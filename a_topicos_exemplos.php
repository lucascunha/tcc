<?php


global $conteudo, $cursosmat, $cursosflag, $id_usuario;

$conteudo = $_SESSION['conteudo']; 

$Excluir = $_POST['Excluir'];
$Enviar = $_POST['Enviar'];
$cNOMEEXEMP= $_POST['cNOMEEXEMP'];
$cDESCEXEMP= $_POST['cDESCEXEMP'];
$cKEYEXEMP= $_POST['cKEYEXEMP'];
$cCOMPEXEMP= $_POST['cCOMPEXEMP'];

$MatExcluir= $_POST['MatExcluir'];
$MatExcluirSeq= $_POST['MatExcluirSeq'];

$CodigoDisciplina = $_GET['CodigoDisciplina'];
$numtopico = $_GET['numtopico'];


unset($_SESSION['cursosmat']);
unset($_SESSION['cursosflag']);  
$cursosmat = $_SESSION['cursosmat'];
$cursosflag = $_SESSION['cursosflag'];

$CEXEMP = $_POST['CEXEMP'];
$VEXEMP = $_POST['VEXEMP'];
$mensagem = '';
$mensagem_ex = '';
$qt_erros = 0;
$qt_erros_ex=0;


   // **************************************************************************
  // * EXCLUI EXERC�CIO                                                        *
  // **************************************************************************
 
 // o usuario 2 foi utilizado para demonstra��o da disciplina e n�o pode incluir, excluir, alterar dados  
 
 if  ($id_usuario <> 2)
 { 
	  	if (isset($Excluir))
	    {        
	     	if (count($MatExcluir) > 0)
	     	{     			 		     	
		        $posicao = EncontraTopico($numtopico);
		        
		        // ----- Flag para excluir itens de material complementar que foram selecionados 	       	          
		        $ExemItemExcluir = array(); 
		        for($x=0; $x < count($conteudo[$posicao]['EXEMPLO']); $x++)
		        {
		           if ($MatExcluir[$x] == "on")	
		              $ExemItemExcluir[$x]= "S";	              
		           else   
		              $ExemItemExcluir[$x]= "N";	              
		        }
	     	    	
	     		// ----- Cria uma matriz tempor�ria dos itens que n�o foram excluidos
	     		$cont = 0; 
	     		$ExemploAux = array();        	
				for($x=0; $x < count($conteudo[$posicao]['EXEMPLO']); $x++) 
				{						
		   	   		if ($ExemItemExcluir[$x] == "N")	   	      	   
		      	    {	      	        	
			        	$ExemploAux[$cont] = $conteudo[$posicao]['EXEMPLO'][$x];
			        	$cont++;
		      	    }	   
				}
		
				// exclui a matriz de exerc�cios indicado por $posicao e adiciona $ExemploAux
				//array_splice($conteudo[$posicao]['EXEMPLO'],0	,count($conteudo[$posicao]['EXEMPLO']),$ExemploAux);  		  
		     	unset($conteudo[$posicao]['EXEMPLO']);
			 	$conteudo[$posicao]['EXEMPLO'] = $ExemploAux;		   			      					   
				GravaMatriz($CodigoDisciplina);	
		    }
		    else
		    {
		   		$qt_erros_ex++;
	  			$mensagem_ex[$qt_erros_ex] = "Nenhum item foi selecionado para exclus�o.";
	  			
	  		}
	    }

}
else
{
     echo "<p class=\"texto5\">\n";
     echo A_LANG_DEMO;
     echo "</p>\n";
}

//=================
 
 // **************************************************************************
 // *  CRIA MATRIZ ORELHA                                                    *
 // **************************************************************************

  $orelha = array();  
  $orelha = array(
		array(   
   		   "LABEL" => 'Selecionar Disciplina',  
     		   "LINK" => "a_index.php?opcao=EntradaTopicos",  
     		   "ESTADO" =>"OFF"
   		   ),    	
  		array(   
   		  	"LABEL" => 'Estruturar Disciplina', 
     		   "LINK" => "a_index.php?opcao=Topicos&CodigoDisciplina=".$CodigoDisciplina,  
     		   "ESTADO" =>"ON"
   		   ) ,		   		   
  		   
   		  ); 

  MontaOrelha($orelha);  

  // echo $CodigoDisciplina;


  ?>

<!--  ************************************************************************** -->
<!--  * BOT�O DE OP��O / FORMUL�RIO DE EXERC�CIO                               * -->
<!--  ************************************************************************** --> 

<form action="a_index.php?opcao=TopicosExemplos&numtopico=<? echo $numtopico."&CodigoDisciplina=".$CodigoDisciplina; ?>" method="post" enctype="multipart/form-data" name="upimagens">   
<table CELLSPACING=1 CELLPADDING=1 width="100%"  border =0  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> >
<tr valign="top">
<td>      
	<table CELLSPACING=0 CELLPADDING=0 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; border-left:3px; border-top:3px; border-bottom:3px; border-right:3px; border-style:solid; border-color:#eeeeee;" >	
	<tr>
		<td>
<?
  
  $orelha = array();  
  $orelha = array(
/*  		array(   
   		   "LABEL" => A_LANG_TOPIC_LIST , 
     		   "LINK" => "a_topicos_verifica.php?CodigoDisciplina=".$CodigoDisciplina, 
     		   "ESTADO" =>"OFF"
   		   ), 	
   		   	   */
		array(   
   		   "LABEL" => 'Estruturar Menu',  
     		   "LINK" => "a_index.php?opcao=TopicosEstruturarArvore&numtopico=".$numtopico."&CodigoDisciplina=".$CodigoDisciplina,  
     		   "ESTADO" =>"OFF"
   		   )  ,    
  		array(   
   		   "LABEL" => A_LANG_TOPIC." N� ".$numtopico, 
     		   "LINK" => "a_index.php?opcao=TopicosAltera&numtopico=".$numtopico."&CodigoDisciplina=".$CodigoDisciplina, 
     		   "ESTADO" =>"OFF"
   		   ),   		   
  		array(   
   		   "LABEL" => A_LANG_TOPIC_EXEMPLES, 
     		   "LINK" => "a_index.php?opcao=TopicosExemplos&numtopico=".$numtopico."&CodigoDisciplina=".$CodigoDisciplina, 
     		   "ESTADO" =>"ON"
   		   ), 
  		array(   
   		   "LABEL" => A_LANG_TOPIC_EXERCISES, 
   		   "LINK" => "a_index.php?opcao=TopicosExercicios&numtopico=".$numtopico."&CodigoDisciplina=".$CodigoDisciplina, 
     		   "ESTADO" =>"OFF"
   		   ),   		   
  		array(   
   		   "LABEL" => "Materiais", 
     		   "LINK" => "a_index.php?opcao=TopicosComplementar&numtopico=".$numtopico."&CodigoDisciplina=".$CodigoDisciplina, 
     		   "ESTADO" =>"OFF"
   		   ),  
  		       		   		   
   		  ); 
 
  //require_once "avaliacao/funcao/geral.func.php";
  //$orelha = orelha_avaliacao($orelha, $numtopico, $CodigoDisciplina);

  MontaOrelha($orelha);
  ?>

 		</td>
	</tr>

<tr>
	<td> 
		<br>
		

		<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">
    	  	<tr>
		    			
				<td colspan=2>
					<div class="bola" style='width:98%; margin:0 0 0 10px;'>
      				<h2 style="padding-top:10px; padding-left:20px;">
      					<?	//echo "<a href=a_index.php?opcao=Topicos&CodigoDisciplina=$CodigoDisciplina border=0>";
							echo  strtoupper(" ".ObterNomeDisciplina($CodigoDisciplina));
							//echo "</a>";
							echo " &nbsp;&nbsp;&nbsp;&raquo;&nbsp;&nbsp;&nbsp; ";
      					?>
      					Exemplos para o Conceito N�
      					<? 
      					    $top=EncontraTopico($numtopico);      				    
      				            echo $numtopico; 
	      				?>
  	      			</h2>
  	      			</div>
	    		</td>
	        	
	       	</tr> 	
    	  	<tr>
		    			
				<td colspan=2>

	    		</td>
	        	
	       	</tr> 		       	


	  		<tr>
	    			<td  align="right" valign="top" width="170">
	    		    		<? echo "Nome"; ?>:
	    			</td>
	    		
	    			<td align="left">
						<textarea  class="button"  rows="3" name="cNOMEEXEMP" id="cNOMEEXEMP" style="width:580px;" ><?php if ($cNOMEEXEMP){echo trim($cNOMEEXEMP);}?></textarea>
    				</td>
		  	</tr>	  				

			<tr>
	    			<td  align="right" valign="top">
	    		    		<? echo "Descri��o:<br>(Como aparecer� ao aluno)"; ?>
	    			</td>
	    		
	    			<td align="left">
						<textarea  class="button"  rows="3" name="cDESCEXEMP" id="cDESCEXEMP" style="width:580px;" ><?php if ($cDESCEXEMP){echo trim($cDESCEXEMP);}?></textarea>
    				</td>
		  	</tr>	  				

	  		<tr>
	    			<td  align="right" valign="top">
	    		    		<? echo A_LANG_TOPIC_WORDS_KEY; ?>:	 
	    			</td>
	    		
	    			<td align="left">
						<textarea  class="button"  rows="3" name="cKEYEXEMP" id="cKEYEXEMP" style="width:580px;" ><?php if ($cKEYEXEMP){echo trim($cKEYEXEMP);}?></textarea>
    				</td>
		  	</tr>	  				


    		<tr>  
		   		<td align="right" valign="top">
		   		   <? echo A_LANG_TENVIRONMENT_EXAMPLES_LEVEL; ?>
   				</td>
   				
   		   		<td align="left">
   		   			<div class="styled-select">			
					<SELECT NAME="cCOMPEXEMP" style="width:370px;" >
					<OPTION VALUE="Facil"><? echo A_LANG_TENVIRONMENT_EXAMPLES_EASY; ?>
					<OPTION VALUE="Medio"><? echo A_LANG_TENVIRONMENT_EXAMPLES_MEDIUM; ?>
					<OPTION VALUE="Complexo"><? echo A_LANG_TENVIRONMENT_EXAMPLES_COMPLEX; ?>
					</SELECT>
				</div><br>
     			</td>
   			</tr>	  		
	  		

   			<tr>
      
	    					<td align="right" valign="top">
	    		    				<? echo A_LANG_MNU_COURSE; ?>:
	    					</td>
	    		
	    					<td align="left">    		
	    					        <?	
	    					        	echo "<div class='tabela_redonda' style=\"width:570px; padding:10px 0 10px 10px; background: #D6E7EF; border: solid 1px #AFC4D5;\">";

	    					        	$cursos = $_SESSION['cursos'];
	    					        	$TotalPossiveisMarcados=0;
	    		    		        	//primeiro acha a posi��o t onde esta o topico no array
										for($t=0; $conteudo[$t]['NUMTOP']<>$numtopico; $t++);
									    $TotalPossiveis = count($conteudo[$t]['CURSO']);
										for($j=0; $j<$TotalPossiveis; $j++)
										{
											$curso_atual = $conteudo[$t]['CURSO'][$j]['ID_CURSO'];
											echo "<input type=\"hidden\" name=\"CEXEMP[".$j."]\" value=".$curso_atual.">\n";


											if ($conteudo[$t]['CURSO'][$j]['STATUSCUR']=='TRUE')
											{   $TotalPossiveisMarcados++;
												if ($VEXEMP[$j] == "on"){ $marcado = "checked";}
												echo "<input class=\"checkbox\" type=\"checkbox\" name=\"VEXEMP[".$j."]\" id=\"VEXEMP[".$j."]\" $marcado>".ObterNomeCurso($conteudo[$t]['CURSO'][$j]['ID_CURSO'])."<br>\n";
												$marcado = "";
											}

										}
										if ($TotalPossiveisMarcados ==0) 
										{ 
											echo "N�o existem cursos vinculados ao conceito - Verifique na aba Conceito."; 	
	   										$qt_erros++;
	   										$mensagem[$qt_erros] = "N�o existem cursos vinculados ao conceito - Verifique na aba Conceito.";
										}
										echo "</div>";
	             			        ?>
             			        
		    				<br><br></td>  
	  		</tr>  	  		
	  		
	
			<tr>	    		
	    			<td align="right" valign="top">
	    		    		<? echo A_LANG_TOPIC_FILE1; ?>:
	    			</td>
				 	<td>	    			    									
					   <?			
					 	while ((strpos($img_name[0], " ")))
							$img_name[0][(strpos($img_name[0], " "))]="_";	

							$eh_html = FALSE;
							$qt_checked = 0;

	  					    if ((trim($cNOMEEXEMP)) == "" || (trim($cDESCEXEMP))  == ""  || (trim($cKEYEXEMP))  == "") 
	  					    {
	  					    		if (trim($cNOMEEXEMP) == "")
	  					    		{ 
	  					    			$qt_erros++;
	  					    			$mensagem[$qt_erros] = "O campo 'Nome' deve ser preenchido.";
	  					    		}
	  					    		if (trim($cDESCEXEMP) == "")
	  					    		{ 
	  					    			$qt_erros++;
	  					    			$mensagem[$qt_erros] = "O campo 'Descri��o' deve ser preenchido.";
	  					    		}
	  					    		if (trim($cKEYEXEMP) == "")
	  					    		{ 
	  					    			$qt_erros++;
	  					    			$mensagem[$qt_erros] = "O campo 'Palavras-Chave' deve ser preenchido.";
	  					    		}
	  					    }

			  				if ($j > 0)
			  				{
			            		for ($checked=0; $checked < $j; $checked++)
			           			{
			           				if ($VEXEMP[$checked] == 'on')
			           				{
			           					$qt_checked++;
			           				}
			           			}
			           			if ($qt_checked ==0)
			           			{
	   								$qt_erros++;
	   								$mensagem[$qt_erros] = "Selecione pelo menos um curso vinculado.";
			           			}	
			           		}

			           		$extensao = '';
							$extensao = strtolower(end(explode('.', $_FILES['img']['name'])));
			           		
			           		if (($qt_erros ==0) || ($extensao == 'jpg') || ($extensao == 'gif') || ($extensao == 'png') || ($extensao == 'bmp') || ($extensao == 'svg') ) 
			           		{
								if(($_FILES['img']['name']))
								{ 
											$extensao = strtolower(end(explode('.', $_FILES['img']['name'])));
											$_UP['pasta'] = 'disciplinas/'.$id_usuario.'/'.$CodigoDisciplina.'/';
											$_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
											$_UP['extensoes'] = array('bmp', 'svg', 'jpg', 'png', 'gif', 'html', 'htm', 'php');
											$_UP['renomeia'] = false;
											$_UP['erros'][0] = 'N�o houve erro';
											$_UP['erros'][1] = 'O arquivo no upload � maior do que o limite do PHP';
											$_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especificado no HTML';
											$_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
											$_UP['erros'][4] = 'N�o foi feito o upload do arquivo';
											if ($_FILES['img']['error'] != 0) 
											{
												$qt_erros++;
												$mensagem[$qt_erros] = "N�o foi poss�vel fazer o upload. Erro: ".($_UP['erros'][$_FILES['img']['error']]);
											}
											else
											if (array_search($extensao, $_UP['extensoes']) === false) 
											{
												$qt_erros++;
												$mensagem[$qt_erros] = "Este tipo de arquivo n�o � permitido.";
											}
											else
											if ($_UP['tamanho'] < $_FILES['img']['size'])
											{
												$qt_erros++;
												$mensagem[$qt_erros] = "O arquivo enviado � muito grande, envie arquivos de at� 2Mb.";
											}
											else
											{	
											 
												if ($_UP['renomeia'] == true) 
												{
													$nome_final = time().'.'.$extensao;
												} 
												else 
												{
													$nome_final = $_FILES['img']['name'];
												}
												
												if (move_uploaded_file($_FILES['img']['tmp_name'], $_UP['pasta'] . $nome_final)) 
												{
													//echo $nome_final." - Upload efetuado com sucesso!";
												}
					
										 		$explode_nome_final = explode(".",$nome_final);
												$explode_nome_final[1] = (($explode_nome_final[1]=="HTM") || ($explode_nome_final[1]=="HTML"))?"html":$explode_nome_final[1];
												
												$img_name[0] = implode(".", $explode_nome_final);
												if($img_name[0] ==".") $img_name[0] = "";
				
										 		$eh_html = (($explode_nome_final[1]=="htm") || ($explode_nome_final[1]=="html"));
            									
												if (!$nfile) $nfile++;
								
												if ($eh_html || $extensao=="htm" || $extensao=="html" || $extensao=="php")
												{
													$caminho_file = $_UP['pasta'].$nome_final;
													$fp = fopen($caminho_file, 'r') or die('stdin');
													$s = fread($fp, filesize ($caminho_file));
													$h = parseHtml($s);
													print_r($h[SRC]);
													$VetArq = array();
													get_array_elems($h['IMG']);										
													InsereEmConteudo2($nome_final, $conteudo, $h['IMG'], $top, "EXEMPLO", $CodigoDisciplina, $cNOMEEXEMP, $cDESCEXEMP, $cKEYEXEMP, $CEXEMP, $VEXEMP, $cCOMPEXEMP);												
							
													fclose($fp);	
												} // if
												else // caso imagens
												{
													$qt_erros=0;
  					    						
												}	
											}	
								}		
								else
								{
	  					    			$qt_erros++;
	  					    			$mensagem[$qt_erros] = "Por favor, selecione o arquivo com o exemplo.";
								}
							}

							?>

							<input type="text" id="fileName" class="file_input_textbox" readonly="readonly">

							<div class="file_input_div">
							<input type="button" value="Selecionar Arquivo" class="buttonBig" /> 
							<input type="file" class="file_input_hidden" name="img" onchange="javascript: document.getElementById('fileName').value = this.value" onmouseover="JavaScript:this.style.cursor='pointer' "  />
							</div>

					</td>										  

  	  		</tr>

			<tr>
						<td>
						</td>		
						<td>
							<table width='585' CELLSPACING=0 CELLPADDING=0 border="0" >	
								<tr>
									<td align='right'>

										<script type="text/javascript">
											$(
												function() 
												{
												$("#customDialog").easyconfirm
												(
													{	locale: 
														{
															title: '',
															text: '<br>Confirma o envio ?<br>',
															button: ['N�o',' Sim'],
															closeText: 'Fechar'
										
														}, dialog: $("#question")
													}
													
												);
													$("#customDialog").click(function() 
													{
														$('#dvLoading').fadeIn(200);
													}
													);
										
												}
											);
										</script>
										<input type="button" class="buttonBig" onClick="location.href='a_index.php?opcao=TopicosEstruturarArvore&CodigoDisciplina=<? echo $CodigoDisciplina ?>'" value="Voltar para Estruturar Menu">

										<input class="buttonBigBig-01" type="submit" name="Enviar"  value="<? echo 'Enviar' ?>" id="customDialog"> 
				
											<div class="dialog" id="question">
												<table>
													<tr>
														<td width="50">
															<img src="imagens/icones/notice.png" alt="" />
														</td>
														<td valign="center">
															Confirma o envio ?
														</td>
													</tr>
												</table>	
											</div>
										<br><br>
									</td>
								</tr>	
								</table>
						</td>
			</tr>

  	  		

			</table> 	
				<!--  ************************************************************************** -->
  				<!--  * SE EXISTIR EXEMPLO - CRIA TABELA     		                       * -->                                     
  				<!--  ************************************************************************** -->
			

				<? if (count($conteudo[$top]['EXEMPLO']) > 0)					
				      {			  
				 ?> 
			<br>
			<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 25px; " class="tabela_redonda">
				<tr>
				<td colspan=2>

					<br>
					<center>	    
					<table CELLSPACING=0 CELLPADDING=0 BORDER=0 WIDTH="100%"  border="0" >  		  				    
  	  				<tr >
  	  				
  	  		 		    		<td class="redondo_inicio" style="background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;"  align="left" valign="middle" width="5%">
		    		    				<? echo A_LANG_COURSE_DELETE; ?>
			    				</td>

	    		 		    		<td width="25%"  style="background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;" align="left" valign="middle">
		    		    				<? echo A_LANG_TENVIRONMENT_EXAMPLES_DSC; ?>
			    				 </td>			    		
	    		 		    		<td width="15%" style="background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;"  align="left" valign="middle">
		    		    				<? echo A_LANG_TENVIRONMENT_EXAMPLES_LVL; ?>
			    				 </td>			    					    				 			    				 		
			    				<td width="15%" style="background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;"  align="left" valign="middle">
		    		    				<? echo A_LANG_MNU_COURSE; ?>
			    				 </td>	
	    		 		    		<td   width="20%" style="background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;"  align="left" valign="middle">
		    		    				<? echo A_LANG_TOPIC_FILE1; ?>
			    				</td>
			    				 
     		 		    			<td  class="redondo_final" width="15%" style="background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff; padding-right:10px;"  width="20%" align="right" valign="middle">
		    		    				<? echo A_LANG_TOPIC_MAIN_ASSOCIATED_FILE; ?>
			    				</td>		
					</tr>					
					<?
					// ---------------------------- impress�o dos exemplo  ----------------------------
					$nCor = 0;					
					for($x=0; $x < count($conteudo[$top]['EXEMPLO']); $x++) 
					{
															
						if ($nCor == 0) 
						    {
						       $nCor = 1;
						       $cCor = "eeeeee";
						    }
				 		  else	
				 		    {
							$nCor = 0;
						 	$cCor = "dddddd";						
						    }				 		  	 												
					?>
					
					<TR BGCOLOR=<?= $cCor ;?>  >							
															
						        <!-- ---------------- checkbox - excluir ---------------- --> 
							<td valign="middle" align="left" class="redondo_inicio">
							    <?
							    echo "<input class=\"checkbox\" type=\"checkbox\" name=\"MatExcluir[".$x."]\">\n";  	    					                	    					             	    					             	    		             	    					             						             							   
							    echo "<input type=\"hidden\" name=\"MatExcluirSeq[".$x."]\" value=".$x.">\n";	    					        	 		     	        	     				     	        	     				     	        	     				      	        		     				     						   	        		     				     		        		     				     		        		     	     							    
							    ?>
							</td>
							
							<!-- ---------------- descri��o do exemplo ---------------- --> 
							<td width="100" align="left" valign="middle">
							   <?							   
							    echo $conteudo[$top]['EXEMPLO'][$x]['DESCEXEMP'];
							   ?> 
							</td>

							<!-- ---------------- N�vel de complexidade ---------------- -->
							<td width="70" align="left" valign="middle">
							   <?
							      echo $conteudo[$top]['EXEMPLO'][$x]['COMPEXEMP'];
							   ?> 
							</td>
							
							<!-- ---------------- cursos selecionados para o exerc�cio ---------------- --> 
																				
			    				<td width="70" align="left" valign="middle">
			    				     <?
			    				     
			    				        $TotalCursoMat = count($conteudo[$top]['EXEMPLO'][$x]['CURSOEXEMPLO']);			    				        
			    				    	for($j=0; $j < $TotalCursoMat; $j++) 
			    					{
		    		    				 if ($conteudo[$top]['EXEMPLO'][$x]['CURSOEXEMPLO'][$j]['STATUSCUREXEMP'] == "TRUE")	
											{
		    		    				    echo ObterNomeCurso($conteudo[$top]['EXEMPLO'][$x]['CURSOEXEMPLO'][$j]['ID_CURSOEXEMP']);
		    		    				    echo "<br>\n";
											}
		    		    			       }
		    		    			     ?>		
			    				</td>								
							
	    		 		    		<!-- ---------------- nome do arquivo referente ao exemplo ---------------- --> 
	    		 		    		<td width="100" align="left" valign="middle">
	    		 		    		        <?
		    		    				echo $conteudo[$top]['EXEMPLO'][$x]['ARQEXEMP'];  
		    		    				?>
			    				 </td>
			    				 
			    				 <!-- ---------------- arquivos associados  ---------------- --> 			    				 			    				 	
			    				 <td class="redondo_final" align="right" valign="middle">	
			    				 <table border="0" style="padding:5px 10px 5px 0;">    				 				    				 
								<?		
                                                // alterado por Douglas
								VerificaStatusArqAssoc($conteudo, $CodigoDisciplina, "exemplo");	
								if (count($conteudo[$top]['EXEMPLO'][$x]['EXEMPLOASSOC']) == 0)
								{
   								      	echo "<tr><td>";
   								  		echo " - &nbsp;&nbsp;";									
   								  		echo "</td><td><tooltip class='tooltip' title='N�o possui arquivo associado'><img src=\"imagens/icones/success.png\" border=\"0\" width=\"20\" align=middle></tooltip></td></tr>";  

								}				
								for($j=0; $j < count($conteudo[$top]['EXEMPLO'][$x]['EXEMPLOASSOC']); $j++) 
								{								  							 	   	
								    //print_r($conteudo[$top]['EXEMPLO'][$x]['EXEMPLOASSOC'][$j]['ARQEXEMPASSOC']); 
								   					   								  								   								   								  								   
								    if ($conteudo[$top][EXEMPLO][$x]['EXEMPLOASSOC'][$j][STATUSEXEMP] == "1")
								    {						   								  								   
   								      	echo "<tr><td>";
   								  		print_r($conteudo[$top]['EXEMPLO'][$x]['EXEMPLOASSOC'][$j]['ARQEXEMPASSOC']); 	
   								  		echo $value."&nbsp;&nbsp;";
   								  		echo "</td><td><tooltip class='tooltip' title='Arquivo enviado ao servidor'><img src=\"imagens/icones/success.png\" border=\"0\" width=\"20\" align=middle></tooltip></td></tr>";  
    								    }
   								    else   
   								    {
								      	echo "<tr><td>";
								      	print_r($conteudo[$top]['EXEMPLO'][$x]['EXEMPLOASSOC'][$j]['ARQEXEMPASSOC']); 	
								      	echo $value;
								      	echo "</td><td><tooltip class='tooltip' title='Arquivo n�o encontrado no servidor'><img src=\"imagens/icones/error.png\" border=\"0\" width=\"20\" align=middle></tooltip></td></tr>";                      					              		 
										$qt_erros++;
	   									$mensagem[1] = "Existem pend�ncias nos ARQUIVOS ASSOCIADOS ao exemplo. Por favor, envie para o servidor.";
	   									?>
	   									<script type="text/javascript">
   										$('body,html').animate({ scrollTop: $('body').height() }, 100);
   										</script>
	   									<?	
	   									$Enviar = True;
									}	
									  //echo "<br>\n";			              		  		   								   								 
								}										
			    				        ?>
			    				 </table>          
			    				 </td>			    		 

					</tr>
					
					<?
					} // fechar for x$
					?>
					

					</TABLE>   <!-- fecha a tabela de arquivos -->			   
					</center>		
			<table cellspacing=0 cellpading=0>
			<tr>		
				<td><br>
					<script type="text/javascript">
						$(
							function() 
							{
							$("#customDialog01").easyconfirm
							(
								{	locale: 
									{
										title: '',
										text: '<br>Confirma a exclus�o dos itens selecionados ?<br>',
										button: ['N�o',' Sim'],
										closeText: 'Fechar'
					
									}, dialog: $("#question01")
								}
								
							);
								$("#customDialog01").click(function() 
								{
							
								}
								);
					
							}
						);
					</script>
					
					<input class="button" type="submit" value="<? echo A_LANG_COURSE_DELETE; ?> os exemplos selecionados" name="Excluir" id="customDialog01">										
						<div class="dialog" id="question01">
							<table>
								<tr>
									<td width="50">
										<img src="imagens/icones/notice.png" alt="" />
									</td>
									<td valign="center">
										Confirma a exclus�o dos itens selecionados ?
									</td>
								</tr>
							</table>	
						</div>
					<br>
				</td>

			</tr>
			</table>

			       <?
			       } // final if (material complementar)
			       ?>	
			       <br><br>		 
			    </td>
			</tr>
		     	
   	    	</table> <!-- fecha a tabela de campos -->	
   	    	

   	    <table border="0" width="98%">
			<tr>
			    <td>
			    <? //echo A_LANG_TENVIRONMENT_ASSOCIATED_FILES; ?><br>
			    </td>										  
  	  		</tr>
  	  	</table>
   
   </td>
   </tr>   
</TABLE><br>
</td>
</tr>         	
</table> <!-- fecha a tabela de cores do fundo do formul�rio -->
</form>


<?
if ($Enviar)
{
		if (($qt_erros == 0)) 
		{
			$mensagem[1]="Arquivo enviado com sucesso!";
	   		$tipo_msg = "success";
	   		$icone = "<img src='imagens/icones/success.png' border='0' />";
	   		?>
	   		<script type="text/javascript">
	   			document.getElementById("cNOMEEXEMP").value = "";
	   			document.getElementById("cDESCEXEMP").value = "";
	   			document.getElementById("cKEYEXEMP").value = "";
				<?php
					for($j=0; $j<$TotalPossiveis; $j++)
					{
						echo "document.getElementById(\"VEXEMP[$j]\").checked = false;";
					}
				?>
	   		</script>
	   	<?php
		}
		if (($qt_erros > 0)) 
	   	{	
	   		$tipo_msg = "error";
			$icone = "<img src='imagens/icones/error.png' border='0' />";				
		}
		?>
				<script type="text/javascript">
 	   				showNotification({
 	       			message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td valign=middle class=mensagem_shadow>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $mensagem[1]; ?></td></tr></table>",
 	      			type: "<? echo $tipo_msg; ?>",
 	       			autoClose: true,
 	       			duration: 8                                        
 	   				});
 	   				//error //success //warning
 				</script>			
<?php 
} 

if ($Excluir)
{ 
		if (($qt_erros_ex == 0)) 
		{
			$mensagem_ex[1]="Arquivos selecionados exclu�dos com sucesso!";
	   		$tipo_msg = "success";
	   		$icone = "<img src='imagens/icones/success.png' border='0' />";

		}
		if (($qt_erros_ex > 0)) 
	   	{	
	   		$tipo_msg = "error";
			$icone = "<img src='imagens/icones/error.png' border='0' />";				
		}
		?>
				<script type="text/javascript">
 	   				showNotification({
 	       			message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td valign=middle class=mensagem_shadow>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $mensagem_ex[1]; ?></td></tr></table>",
 	      			type: "<? echo $tipo_msg; ?>",
 	       			autoClose: true,
 	       			duration: 8                                        
 	   				});
 	   				//error //success //warning
 				</script>			
<?php 
} 


// **************************************************************************
// *  ATIVA DEBUG                                                           *
// **************************************************************************
  
if ($DEBUG)
   get_array_elems($conteudo); 
   
?>
