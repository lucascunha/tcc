<?php  

//Include the code
include("include/phplot/phplot.php");
include("config/configuracoes.php");
include('include/adodb/adodb.inc.php'); 
include('include/adodb/tohtml.inc.php');

//Define the object
	$graph = new PHPlot;

	$graph->SetPlotType("lines");

  	// Conex�o com o banco de dados
  	$conn = &ADONewConnection($A_DB_TYPE); 
	$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

        // Atualiza o conte�do da disciplina em manuten��o
  	$sql = "SELECT date_format(data_hora_inicio,'%d/%m/%Y') as data, count(*) as qt FROM `acesso` GROUP BY date_format(data_hora_inicio,'%d/%m/%Y');";
//  	$sql = "SELECT date_format(data_hora_inicio,'%d/%m/%Y') as data, '1' as qt FROM `acesso`;";
  	$rs = $conn->Execute($sql);
  	
  	if ($rs === false) die("N�o foi poss�vel obter dados no Banco de Dados");  
  	
  	$dados = $rs->GetArray();
  	
	$graph->SetDataValues($dados);	
	$graph->SetYLabel("Logins");	
	$graph->SetXDataLabelAngle(90);				
	
	//Draw it
	$graph->DrawGraph();  	
  	
  	$rs->Close();  	
  	 
?> 