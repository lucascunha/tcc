<?

$opcao = $_GET['opcao'];

session_start();
$logado 		=	$_SESSION['logado'];
$id_usuario		=	$_SESSION['id_usuario'];
$email_usuario 	=	$_SESSION['email_usuario'];
$A_LANG_IDIOMA_USER=$_SESSION['A_LANG_IDIOMA_USER'];
$tipo_usuario	=	$_SESSION['tipo_usuario'];
$status_usuario	=	$_SESSION['status_usuario'];
$id_aluno		=	$_SESSION['id_aluno'];
$tipo   		=	$_SESSION['tipo'];

include "config/configuracoes.php"; 
include "include/funcoes.php"; 

global $logado, $id_usuario, $email_usuario, $A_LANG_IDIOMA_USER, $tipo_usuario, $id_aluno, $tipo;

if(!$logado && $tipo_usuario !='aluno')
{
  header("Location: index.php");
}

// Troca de idioma
if (isset($newlang))
   $A_LANG_IDIOMA_USER=trocalang($newlang, $id_usuario);
   
 if ($A_LANG_IDIOMA_USER == "")
    include "idioma/".$A_LANG_IDIOMA."/geral.php";
 else   
   include "idioma/".$A_LANG_IDIOMA_USER."/geral.php"; 
 
//header ("Content-type: text/html; Charset=".A_LANG_CHACTERSET."\""); 
 
?>

<?

// solicita login para as demais op��es 
if ( $opcao <> "Login" && 
     $opcao <> "Apresentacao" && 
     $opcao <> "Logout" && 
     $opcao <> "NovoUsuario" && 
     $opcao <> "")
     {
 	   AutenticacaoNavega($opcao);
     }
 	
?>

<!--

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<META http-equiv=Content-Type content="text/html; charset=<? echo A_LANG_CHACTERSET; ?>">
<META http-equiv=expires content="0">
<META http-equiv=Content-Language content="<? echo A_LANG_CODE; ?>">
<META name=KEYWORDS content="Adpta��o, Ensino, Dist�ncia">
<META name=DESCRIPTION content="Modulo de Adaptabilidade do Sistema AdaptWeb">
<META name=ROBOTS content="INDEX,FOLLOW">
<META name=resource-type content="document">
<META name=AUTHOR content="Veronice de Freitas">
<META name=COPYRIGHT content="Copyright (c) 2001 by Veronice de Freitas">
<META name=revisit-after content="1 days">
<META name=distribution content="Global">
<META name=GENERATOR content="Windows NotePade">
<META name=rating content="General">
<META name=MS.LOCALE content="<? echo A_LANG_CODE; ?>">
-->

<?
switch ($opcao)
{						
	case "Matricula":
		Contexto(A_LANG_MNU_SUBSCRIBE, 2);
		$inc = "n_matricula.php";
		break;								
	case "Navega":
		$inc =  "n_navega.php";
		break;				
	case "NavegaAutor":
		$inc = "a_navega.php";  //Alterado por Laisi Corsani (UDESC) para Mural Recados 25/07/2008
		break;	
// Criado por Laisi Corsani (UDESC) para Mural Recados 25/07/2008
	case "MuralRecados":
		Contexto("Mural de Recados", 3);
		if ($tipo_usuario == "aluno")
		$inc = "mural_recados/n_mural.php";
		else
		$inc = "mural_recados/a_mural.php";	
		break;


	case "CriaRecados":
		if ($tipo_usuario == "aluno")
		$inc = "mural_recados/n_cria_recado.php";
		else
		$inc = "mural_recados/a_cria_recado.php";	
		break;
		
	case "EnviaRecados":
		if ($tipo_usuario == "aluno")
		$inc = "mural_recados/n_envia_recado.php";
		else
		$inc = "mural_recados/a_envia_recado.php";	

		break;




	case "MuralRecadosAutor":
		Contexto("Mural de Recados", 3);
		$inc = "mural_recados/a_mural.php";
		break;
	case "CriaRecadosAutor":
		$inc = "mural_recados/a_cria_recado.php";
		break;
	case "EnviaRecadosAutor":
		$inc = "mural_recados/a_envia_recado.php";
		break;
// Fim Mural Recados


//Criado por Claudia da Rosa (UDESC) para F�rum de Discuss�o 09/09/08
	case "ForumDiscussao":
		Contexto("F�rum de Discuss�o", 3);
		if ($tipo_usuario == "aluno")
		$inc = "forum_discussao/n_forum.php";
		else
		$inc = "forum_discussao/a_forum.php";	
		break;
		
	case "EnviarTopico":
		if ($tipo_usuario == "aluno")
		$inc = "forum_discussao/n_enviar_topico_discussao.php";
		else
		$inc = "forum_discussao/a_enviar_topico_discussao.php";	
		break;
		
	case "CriaTopicoDiscussao":
		if ($tipo_usuario == "aluno")
		$inc = "forum_discussao/n_cria_topico_discussao.php";
		else
		$inc = "forum_discussao/a_cria_topico_discussao.php";
		break;
	
	case "ParticiparTopicoDiscussao":
		if ($tipo_usuario == "aluno")
		$inc = "forum_discussao/n_participar_topico_discussao.php";
		else
		$inc = "forum_discussao/a_participar_topico_discussao.php";
		break;
		
	case "EnviarParticipacao":
		if ($tipo_usuario == "aluno")
		$inc = "forum_discussao/n_enviar_participacao.php";
		else
		$inc = "forum_discussao/a_enviar_participacao.php";
		break;
		
	case "ForumDiscussaoAutor":
		Contexto("F�rum de Discuss�o", 3);
		$inc = "forum_discussao/a_forum.php";
		break;


	
	case "EnviaTopicoAutor":
		$inc = "forum_discussao/a_enviar_topico_discussao.php";
		break;
		
	case "CriaTopicoDiscussaoAutor":
		$inc = "forum_discussao/a_cria_topico_discussao.php";
		break;	
		
	case "ParticiparTopicoDiscussaoAutor":
		$inc = "forum_discussao/a_participar_topico_discussao.php";
		break;	
	
	case "EnviarParticipacaoAutor":
		$inc = "forum_discussao/a_enviar_participacao.php";
		break;
			
	case "AvaliarParticipacaoTopicoAutor":
		$inc = "forum_discussao/a_avaliar_participacao_topico.php";
		break;
		
	case "AvaliarAlunosAutor":
		$inc = "forum_discussao/a_avaliar_alunos.php";
		break;		
		
	case "VisualizarAlunoAutor":
		$inc = "forum_discussao/a_visualizar_aluno.php";
		break;
		
	case "ExcluirTopicoParticipacao":
		$inc = "forum_discussao/a_excluir_topico_participacao.php";
		break;
	
	case "ExcluirParticipacao":
		$inc = "forum_discussao/a_excluir_participacao.php";
		break;
		
// Fim F�rum de Discuss�o



//	case "Login":
//		include "n_login.php";
//		break;				
//	case "Logout":
//		include "n_logout.php";
//		break;	
//	case "EnvioTrabalho":
//		Contexto(A_LANG_MNU_UPLOAD_FILES, 2);
//		$inc = "n_envio_de_trabalho.php";
//		break;	
	case "AssistirDisciplinas":
		Contexto(A_LANG_MNU_DISCIPLINES_RELEASED, 2);
		$inc = "n_cursos.php";
		break;	
	/**
	 * Op��es para avalia��o do aluno
	 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
	 * Como Trabalho de Conclus�o de Curso.
	 * Orientadora: Isabela Gasparini
	 * Co-orientadora: Avanilde Kemczinski
	 * @since 10/02/2008
	 * @author Claudiomar Desanti
	 */
	
	case "Avaliacao":
	    Contexto(A_LANG_MNU_AVS,2);
	    $inc = "avaliacao/n_index.php";
	    break;

	case "Mural":
	    Contexto(A_LANG_MNU_AVS,2);
	    $inc = "avaliacao/n_mural.php";
	    break;

	case "AguardandoMatricula":
		Contexto(A_LANG_MNU_WAIT, 2);
		$inc = "n_cursos_aguardando_matricula.php";
		break;					
	case "DisciplinasAutor":
		$inc = "n_cursos_autor.php";
		break;																																																

//	case "TopicosEdit":
//		include "a_topicos_edit_conceito.php";
//		break;	
//	case "AlteraCadastro":
//		include "n_altera.php";
//		break;															
	default:
		Contexto(A_LANG_MNU_NAVIGATION, 1);
		$inc = "n_entrada_navegacao.php";											
		break;
}

?>

<title><? echo $opcao." - AdaptWeb" ?></title>
<LINK REL="StyleSheet" HREF="css/geral-x.css" TYPE="text/css">
<link href="avaliacao/css/ff.css" rel="stylesheet" type="text/css" media="screen" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/blitzer/jquery-ui.css" type="text/css" />
<script type="text/javascript" src="src/jquery.floatingmessage.js"></script>
<script language="JavaScript" type="text/JavaScript" src="javascript/ajaxlib.js" ></script>
<script src="javascript/jquery.easy-confirm-dialog.js"></script> 
<script type="text/javascript" src="javascript/jquery_notification_v.1.js"></script>
<link href="javascript/jquery_notification.css" type="text/css" rel="stylesheet"/>	
<link rel="stylesheet" href="javascript/colorbox.css" />
<script src="javascript/jquery.colorbox.js"></script>

<link href="javascript/tiptip/tipTip.css" rel="stylesheet">
<script src="javascript/tiptip/jquery.tipTip.js"></script>
<script src="javascript/tiptip/jquery.tipTip.minified.js"></script>
<script>
$(function(){
$(".tooltip").tipTip({maxWidth: "auto", defaultPosition:"right",  edgeOffset: 10, delay: 150});
});
</script>



	
	<?
		include $inc;
	?>						
																								
</html>

<script type="text/javascript">
    $(document).ready(function() {
            $("body").css("display", "none");
            $("body").fadeIn(100);
    });
</script>