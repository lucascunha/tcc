<?php
$p = pdf_new();
pdf_open_file($p);

pdf_begin_page($p,595,842);
    // desenha duas linhas e uma curva
    pdf_moveto($p,400,700);
    pdf_lineto($p,200,750);
    pdf_lineto($p,400,800);
    pdf_curveto($p,100,800, 100,750, 100,700);
    pdf_stroke($p);
    
    // desenha mesma figura, s� que preenchida
    pdf_setcolor($p,"fill","rgb", 0, 1, 0);
    pdf_moveto($p,400,550);
    pdf_lineto($p,200,600);
    pdf_lineto($p,400,650);
    pdf_curveto($p,100,650, 100,600, 100,550);
    pdf_closepath($p);
    pdf_fill_stroke($p);
pdf_end_page($p);

pdf_close($p);
$buf = pdf_get_buffer($p);
$tamanho = strlen($buf);
header("Content-type:application/pdf");
header("Content-Length:$tamanho");
header("Content-Disposition:inline; filename=linhas.pdf");
echo $buf;
pdf_delete($p);
?>
