<?php
$p = pdf_new();
pdf_open_file($p);
pdf_set_info($p,"Creator","coordenadas2.php");
pdf_set_info($p,"Author","Juliano Niederauer");
pdf_set_info($p,"Title","Sistema de coordenadas");

pdf_begin_page($p,595,842);
    // Move a origem
    pdf_translate($p,0,842);
    pdf_scale($p, 1, -1);
    pdf_set_value($p,"horizscaling",-100);

    $font = pdf_findfont($p,"Helvetica-Bold","host",0);
    pdf_setfont($p,$font,-38.0);
    pdf_show_xy($p, "Superior esquerdo", 10, 40);
pdf_end_page($p);

pdf_set_parameter($p, "openaction", "fitpage");
pdf_close($p);
$buf = pdf_get_buffer($p);
$tamanho = strlen($buf);
header("Content-type:application/pdf");
header("Content-Length:$tamanho");
header("Content-Disposition:inline; filename=coordenadas.pdf");
echo $buf;
pdf_delete($p);
?>
