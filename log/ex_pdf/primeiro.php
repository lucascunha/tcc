<?php
$arquivo = 'c:\apache\pdf\teste.pdf';

$pdf = pdf_new();
pdf_open_file($pdf, $arquivo);

pdf_set_info($pdf, "Author", "Juliano Niederauer");
pdf_set_info($pdf, "Title", "Documento teste");
pdf_set_info($pdf, "Creator", "Juliano Niederauer");
pdf_set_info($pdf, "Subject", "Meu primeiro documento PDF");

pdf_begin_page($pdf, 595, 842);
    $fonte = pdf_findfont($pdf, "Times New Roman", "winansi", 1);
    pdf_setfont($pdf, $fonte, 16);
    pdf_show_xy($pdf, "Este � o meu primeiro documento PDF!!!", 150, 750);
pdf_end_page($pdf);

pdf_close($pdf);
pdf_delete($pdf);
echo "Arquivo <b>$arquivo</b> criado!";
echo "<br><a href=$arquivo>Clique aqui para acess�-lo</a>";
?>

