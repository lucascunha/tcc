<?php
$p = pdf_new();
pdf_open_file($p);

pdf_begin_page($p,595,842);
    // adiciona um thumbnail
    $im = pdf_open_image_file ($p, "jpeg", "gremio.jpg");
    pdf_add_thumbnail($p, $im);

    // adiciona um bookmark
    $topico = pdf_add_bookmark($p, "Clubes");
    pdf_add_bookmark($p, "Gr�mio", $topico);
    
    // adiciona um weblink
    pdf_place_image($p, $im, 250, 700, 1);
    $largura = pdf_get_value ($p, "imagewidth", $im);
    $altura = pdf_get_value ($p, "imageheight", $im);
    pdf_add_weblink ($p, 250, 700, 250+$largura, 700+$altura, "http://www.gremio.net");
    pdf_close_image($p,$im);

    $font = pdf_findfont($p,"Helvetica-Bold","host",0);
    pdf_setfont($p, $font, 20);
    pdf_show_xy($p, "P�gina sobre o Gr�mio", 40, 800);
pdf_end_page($p);

pdf_begin_page($p,595,842);
    // adiciona um thumbnail
    $im = pdf_open_image_file ($p, "jpeg", "flamengo.jpg");
    pdf_add_thumbnail($p, $im);

    // adiciona um weblink
    pdf_place_image($p, $im, 250, 700, 1);
    $largura = pdf_get_value ($p, "imagewidth", $im);
    $altura = pdf_get_value ($p, "imageheight", $im);
    pdf_add_weblink ($p, 250, 700, 250+$largura, 700+$altura, "http://www.flamengo.com.br");
    pdf_close_image($p,$im);

    // adiciona um bookmark
    pdf_add_bookmark($p, "Flamengo", $topico);

    // conte�do da p�gina
    pdf_setfont($p, $font, 20);
    pdf_show_xy($p, "P�gina sobre o Flamengo", 40, 800);
pdf_end_page($p);

pdf_close($p);
$buf = pdf_get_buffer($p);
$tamanho = strlen($buf);
header("Content-type:application/pdf");
header("Content-Length:$tamanho");
header("Content-Disposition:inline; filename=referencias.pdf");
echo $buf;
pdf_delete($p);
?>
