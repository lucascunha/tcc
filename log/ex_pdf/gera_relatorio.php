<?php
// ********* CONFIGURA��ES DO PROGRAMA *********
// documento
$largura = 842;
$altura = 595;
$margem_vertical = 30;
$margem_horizontal = 30;
$tamanho_fonte = 14;
$tamanho_fonte_titulo = 20;
$titulo = "Lista de Pre�os da Novatec Editora";

// banco de dados
$servidor = "localhost";
$usuario = "juliano";
$senha = "12345";
$banco = "test";

// consulta SQL que ir� gerar o relat�rio
$consulta = "select * from livros order by titulo";
$colunas_resultantes = array ("isbn", "titulo", "autor", "preco");

// tabela a ser gerada no PDF
$texto_colunas = array ("ISBN", "T�tulo", "Autor", "Pre�o");
$largura_coluna = array (90, 360, 280, 150);

// ********* N�O ALTERE DAQUI EM DIANTE *********

// executa a consulta
$con = mysql_connect($servidor, $usuario, $senha);
mysql_select_db ($banco);
$result = mysql_query($consulta);
$total = mysql_numrows($result);

if($total==0)
{
    mysql_close($con);
    echo "O relat�rio n�o foi gerado porque a consulta n�o retornou registros!";
    exit;
}

// cria o documento PDF
$p = pdf_new();
pdf_open_file($p);

// c�lculos
$altura_celula = $tamanho_fonte+3;
$altura_titulo = $tamanho_fonte_titulo+3;
$altura_tabela = $altura - 2*$margem_vertical;
$linhas_por_pagina = intval (($altura_tabela-$altura_titulo)/$altura_celula)-1; // tirar 1 devido ao cabe�alho
$num_paginas  = ceil($total/$linhas_por_pagina);
$linha_atual = 0;

// gera as p�ginas
for($i=0; $i<$num_paginas; $i++)
{
    // cria nova p�gina
    pdf_begin_page($p,$largura,$altura);

    // t�tulo do relat�rio
    $font = pdf_findfont($p,"Times-Bold","winansi",0);
    pdf_setfont($p, $font, $tamanho_fonte_titulo);
    $posy = $altura - $margem_vertical;
    $posx = $margem_horizontal;
    $pag_atual = $i+1;
    pdf_show_xy($p, $titulo." (p�gina $pag_atual)", $posx, $posy);

    // cria o cabe�alho da tabela em negrito
    $font = pdf_findfont($p,"Times-Bold","winansi",0);
    pdf_setfont($p,$font,$tamanho_fonte);
    $posy -= $altura_titulo;
    $posx = $margem_horizontal;

    pdf_moveto($p, $posx, $posy-3);
    pdf_lineto($p, $largura-$margem_horizontal, $posy-3);
    pdf_stroke($p);

    for($k=0; $k<sizeof($texto_colunas); $k++)
    {
        pdf_show_xy($p, $texto_colunas[$k], $posx, $posy);
        $posx += $largura_coluna[$k];
    }

    // tira o negrito da fonte
    $font = pdf_findfont($p,"Times-Roman","winansi",0);
    pdf_setfont($p,$font,$tamanho_fonte);

    // escreve os registros
    $inicio = $linha_atual;
    $fim = $linha_atual + $linhas_por_pagina;
    if($fim > $total)
        $fim = $total;

    for($j=$inicio; $j<$fim; $j++)
    {
        $linha_atual = $j;
        $posx = $margem_horizontal;
        $posy -= $altura_celula;
        
        for($k=0; $k<sizeof($colunas_resultantes); $k++)
        {
            $valor = mysql_result($result, $linha_atual, $colunas_resultantes[$k]);
            pdf_show_xy($p, $valor, $posx, $posy);
            $posx += $largura_coluna[$k];
        }
        $linha_atual++;
    }

    // encerra a p�gina
    pdf_end_page($p);

}
mysql_close($con);

// encerra o documento PDF
pdf_set_parameter($p, "openaction", "fitpage");
pdf_close($p);

$buf = pdf_get_buffer($p);
$tamanho = strlen($buf);
header("Content-type:application/pdf");
header("Content-Length:$tamanho");
header("Content-Disposition:inline; filename=relatorio.pdf");
echo $buf;
pdf_delete($p);
?>
