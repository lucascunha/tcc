<?php
$p = pdf_new();
pdf_open_file($p);
pdf_begin_page($p,595,842);
$font = pdf_findfont($p,"Helvetica-Bold","host",0);
pdf_setfont($p,$font,38.0);

$texto = "Estou escrevendo um texto dentro de uma caixa de texto! Que bacana!";

pdf_show_boxed($p, $texto, 50, 630, 500, 180, "left");
pdf_rect($p,50,630,500,180);
pdf_stroke($p);

pdf_show_boxed($p, $texto, 50, 420, 500, 180, "right");
pdf_rect($p,50,420,500,180);
pdf_stroke($p);

pdf_show_boxed($p, $texto, 50, 210, 500, 180, "justify");
pdf_rect($p,50,210,500,180);
pdf_stroke($p);

pdf_show_boxed($p, $texto, 50, 0, 500, 180, "center");
pdf_rect($p,50,0,500,180);
pdf_stroke($p);

pdf_end_page($p);
pdf_set_parameter($p, "openaction", "fitpage");
pdf_close($p);

$buf = pdf_get_buffer($p);
$len = strlen($buf);
header("Content-type:application/pdf");
header("Content-Length:$len");
header("Content-Disposition:inline; filename=caixatexto.pdf");
echo $buf;
pdf_delete($p);
?>
