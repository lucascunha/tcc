<?php
$p = pdf_new();
pdf_open_file($p);

// cria o modelo
$im = pdf_open_image_file ($p, "jpeg", "postgresql.jpg");
$template = pdf_begin_template($p,595,842);
    pdf_save($p);
    pdf_place_image($p, $im, 4, 795, 0.15);
    pdf_place_image($p, $im, 565, 795, 0.15);
    pdf_moveto($p,0,793);
    pdf_lineto($p,595,793);
    pdf_stroke($p);
    $font = pdf_findfont($p,"Times-Bold","host",0);
    pdf_setfont($p,$font,30);
    pdf_show_xy($p,"Exemplo de modelo PDF",130,810);
    pdf_restore($p);
pdf_end_template($p);
pdf_close_image ($p,$im);

// Primeira p�gina
pdf_begin_page($p,595,842);
pdf_place_image($p, $template, 0, 0, 1);
pdf_end_page($p);

// Segunda p�gina
pdf_begin_page($p,595,842);
pdf_place_image($p, $template, 0, 0, 1);
pdf_end_page($p);

pdf_close($p);
$buf = pdf_get_buffer($p);
$tamanho = strlen($buf);
Header("Content-type:application/pdf");
Header("Content-Length:$tamanho");
Header("Content-Disposition:inline; filename=modelo.pdf");
echo $buf;
pdf_delete($p);
?>
