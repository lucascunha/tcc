<?php
$p = pdf_new();
pdf_open_file($p);

pdf_begin_page($p,595,842);
    // C�rculo
    pdf_setcolor($p,"fill","rgb", 0, 0.8, 0);
    pdf_circle($p,150,700,100);
    pdf_fill_stroke($p);
    // Arco
    pdf_setcolor($p,"fill","rgb", 1, 1, 0);
    pdf_arc($p,400,700,100,0,180);
    pdf_closepath($p);
    pdf_fill_stroke($p);
pdf_end_page($p);
pdf_close($p);

$buf = pdf_get_buffer($p);
$tamanho = strlen($buf);
header("Content-type:application/pdf");
header("Content-Length:$tamanho");
header("Content-Disposition:inline; filename=circulos.pdf");
echo $buf;
pdf_delete($p);
?>
