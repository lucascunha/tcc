<?php
$p = pdf_new();
pdf_open_file($p);
pdf_begin_page($p,595,842);
$font = pdf_findfont($p,"Helvetica-Bold","host",0);
pdf_setfont($p,$font,32.0);

pdf_set_parameter($p, "overline", "true");
pdf_show_xy($p, "Linha sobre o texto", 50, 780);
pdf_set_parameter($p, "overline", "false");

pdf_set_parameter($p, "underline", "true");
pdf_continue_text($p, "Texto sublinhado");
pdf_set_parameter($p, "underline","false");

pdf_set_parameter($p, "strikeout", "true");
pdf_continue_text($p, "Texto tachado");
pdf_set_parameter($p, "strikeout","false");

pdf_setcolor($p,"fill","rgb", 0, 0, 1);
pdf_continue_text($p, "Texto azul");

pdf_set_value($p,"textrendering",1);
pdf_setcolor($p,"stroke","rgb", 1, 0, 0);
pdf_continue_text($p, "Texto contornado em vermelho");

pdf_set_value($p,"textrendering",2);
pdf_setcolor($p,"fill","rgb", 0, 0, 1);
pdf_setlinewidth($p,2);
pdf_continue_text($p, "Texto azul, contorno vermelho");

pdf_end_page($p);
pdf_close($p);

$buf = pdf_get_buffer($p);
$tamanho = strlen($buf);
header("Content-type:application/pdf");
header("Content-Length:$tamanho");
header("Content-Disposition:inline; filename=texto.pdf");
echo $buf;
pdf_delete($p);
?>
