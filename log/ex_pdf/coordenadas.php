<?php
$p = pdf_new();
pdf_open_file($p);
pdf_set_info($p,"Creator","coordenadas.php");
pdf_set_info($p,"Author","Juliano Niederauer");
pdf_set_info($p,"Title","Sistema de coordenadas");

pdf_begin_page($p,595,842);
    $font = pdf_findfont($p,"Helvetica-Bold","host",0);
    pdf_setfont($p,$font,28.0);
    pdf_show_xy($p, "Inferior esquerdo", 10, 10);
    pdf_show_xy($p, "Inferior direito", 400, 10);
    pdf_show_xy($p, "Superior esquerdo", 10, 802);
    pdf_show_xy($p, "Superior direito", 370, 802);
    pdf_show_xy($p, "Centro",595/2-50,842/2-14);
pdf_end_page($p);

pdf_set_parameter($p, "openaction", "fitpage");
pdf_close($p);
$buf = pdf_get_buffer($p);
$tamanho = strlen($buf);
header("Content-type:application/pdf");
header("Content-Length:$tamanho");
header("Content-Disposition:inline; filename=coordenadas.pdf");
echo $buf;
pdf_delete($p);
?>
