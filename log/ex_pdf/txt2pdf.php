<?php
$arquivo_txt = "teste.txt";

if(file_exists($arquivo_txt))
    $texto = file_get_contents ($arquivo_txt);
else
{
    echo "Arquivo $arquivo_txt n�o encontrado!";
    exit;
}
    
$margem_vertical = 60;
$margem_horizontal = 40;

$p = pdf_new();
pdf_open_file($p);
pdf_set_info($p,"Creator","txt2pdf.php");
pdf_set_info($p,"Author","Juliano Niederauer");
pdf_set_info($p,"Title","Conversor TXT para PDF");

// cria um modelo
$im = pdf_open_image_file ($p, "jpeg", "matematica.jpg");
$modelo = pdf_begin_template($p,595,842);
    pdf_save($p);
    pdf_place_image($p, $im, 4, 791, 1);
    pdf_moveto($p,0,789);
    pdf_lineto($p,595,789);
    pdf_stroke($p);
    $font = pdf_findfont($p,"Times-Bold","host",0);
    pdf_setfont($p,$font,20);
    pdf_show_xy($p,"S� Matem�tica - www.somatematica.com.br",115,810);
    pdf_restore($p);
pdf_end_template($p);
pdf_close_image ($p,$im);

$continua=true;
while ($continua)
{
    pdf_begin_page($p,595,842);
        pdf_place_image($p, $modelo, 0, 0, 1);  // modelo
        $font = pdf_findfont($p,"Helvetica-Bold","host",0);
        pdf_setfont($p,$font,12);
        $num = pdf_show_boxed($p, $texto, $margem_horizontal, $margem_vertical, 595-2*$margem_horizontal, 842-2*$margem_vertical, "justify");
        $texto = substr($texto, strlen($texto)-$num);
    pdf_end_page($p);

    if($num==0)
        $continua=false;
}

PDF_set_parameter($p, "openaction", "fitpage");
pdf_close($p);
$buf = pdf_get_buffer($p);
$tamanho = strlen($buf);
header("Content-type:application/pdf");
header("Content-Length:$tamanho");
header("Content-Disposition:inline; filename=txt2pdf.pdf");
echo $buf;
pdf_delete($p);
?>
