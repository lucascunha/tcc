<?php 

/***********************************************************************/
/* AREA DE CONFIGURACOES DE CORES                        	       */
/***********************************************************************/

$cor_padrao  = "$cor"               ;
$cor_fonte   = "#000000"            ;

?>


<html>
<head>
<title>	
	Sistema de Ajuda 
</title>
</head>

<body bgcolor="#FFFFFF" text="#000000" vlink="#0000FF" link="#0000FF" alink="#FF0000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<a NAME="topo"></a>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#FFFFFF">
    <td width="6%">
      <img src="imagens/loginho.gif" width="29" height="31">
    </td>
    <td width="84%" >
    </td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" height="25" bgcolor="<?php  echo $cor_padrao; ?>" >
    <center>
	<font size="2" face="Arial, Helvetica, sans-serif"><b>
	Ajuda sobre a An�lise de Log
	</b></font>
	</center>
    </td>
  </tr>
</table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
	    <td bgcolor="eeeeee" width="100%" height="25">
	      <div align="left">
		<font size="1" face="verdana, Arial, Helvetica, sans-serif">

Manual de Utiliza��o da An�lise de Log
		</font>
	      </div>
	    </td>
	  </tr>
	<tr>
	<td background="imagens/pontilhado.gif"  height="2"></td>
  	</tr>
</table>
<br>
<blockquote>
  <p>
  	<font face="Arial, Helvetica, sans-serif"><font size=-1>
  		<a href="#1. Introducao">
			1. Introdu&ccedil;&atilde;o a Analise de Log
		</a>
	</font></font><br>
    <font face="Arial, Helvetica, sans-serif"><font size=-1>
		<a href="#2. Periodo">
			2. Defini&ccedil;&atilde;o do Per&iacute;odo da An&aacute;lise
      	</a>
	 </font></font><br>
    <font face="Arial, Helvetica, sans-serif"><font size=-1>
		<a href="#3. Exibicao">
			3. Escolha do modo de Exibi&ccedil;&atilde;o dos Dados
      	</a>
	</font></font><br>
    <font face="Arial, Helvetica, sans-serif"><font size=-1>
		<a href="#4. Curso_Disciplina">
			4. Escolha do Curso e da Disciplina
      	</a>
	</font></font><br>
	 <font face="Arial, Helvetica, sans-serif"><font size=-1>
		<a href="#5. Analise ">
			5. Escolha do Tipo de An&aacute;lise    	
		</a>
	</font></font><br>
	 <font face="Arial, Helvetica, sans-serif"><font size=-1>
		<a href="#6. Grafico">
			6. Escolha do Tipo de Gr&aacute;fico		
		</a>
	</font></font><br>
		 <font face="Arial, Helvetica, sans-serif"><font size=-1>
		<a href="#7. Imprimir">
			7. Imprimir e Gerar PDF    	
		</a>
	</font></font><br>
  </p>
	<p><a NAME="1. Introducao"></a><b><font face="Arial, Helvetica, sans-serif"><font size=-1>
  		1.Introdu&ccedil;&atilde;o a Analise de Log&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (<a href="#topo">^topo</a>)</font></font></b>
  	<p><font face="Arial, Helvetica, sans-serif"><font size=-1>
  		texto sobre a an�lise de log
	</font></font></p>

	<p><a NAME="2. Periodo"></a><b><font face="Arial, Helvetica, sans-serif"><font size=-1>
  2. Defini&ccedil;&atilde;o do Per&iacute;odo da An&aacute;lise&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<a href="#topo">^topo</a>)</font></font></b>    
  <p>O <em>per�odo</em> � composto por dois campos de entrada de dados que servir�o para delimitar o tempo em que ser� realizada a an�lise. O primeiro campo � definido como <em>data inicia</em>l e o segundo como <em>data final</em>. Como valor padr�o para o campo data inicial ser� feita uma busca no banco de dados para encontrar a data mais antiga de acesso de algum aluno matriculado em alguma disciplina do professor que est� utilizando a An�lise de Log. Como data final, o padr�o fica sendo a data atual. 
  <p><a NAME="3. Exibicao"></a><b><font face="Arial, Helvetica, sans-serif"><font size=-1>3.
Escolha do modo de Exibi&ccedil;&atilde;o dos Dados&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(<a href="#topo">^topo</a>)</font></font></b>
<p><font face="Arial, Helvetica, sans-serif"><font size=-1>
A maneira de <em>exibi��o dos dados </em>� onde o usu�rio escolhe-se como ser�o exibidos os dados, s�o poss�veis op��es:
<dd>
<dd><li><b>Dias da semana:</b> a exibi��o � feita agrupando os dados resultantes de acordo com os dias da semana e o gr�fico resultante indica o dia da semana e a respectiva quantidade de acessos. A Figura 1 exemplifica como pode ser exibido o resultado.</li>
		<center>
		<p><img SRC="imagens/dias_semana.bmp" ><br>
		Figura 1: Gr�fico exibi��o dos resultados � Op��o dias da semana <br><br>
		</center></dd>
	<dd><li><b>Dias do m�s:</b> a exibi��o � feita agrupando os dados resultantes de acordo com os dias do m�s e o gr�fico resultante indicar� o dia do m�s e a respectiva quantidade de acessos. A Figura 2 exemplifica como pode ser exibido o resultado.</li>
		<center>
		<p><img SRC="imagens/dias_mes.bmp"><br>
		Figura 2: &nbsp;Gr&aacute;fico  exibi&ccedil;&atilde;o dos resultados &ndash; Op&ccedil;&atilde;o dias do m&ecirc;s <br>
		<br>
		</center></dd>
	<dd><li><b>Meses do ano:</b> a exibi��o � feita agrupando os dados resultantes de acordo com os meses do ano e o gr�fico resultante indica o m�s do ano e a respectiva quantidade de acessos. A Figura 3 exemplifica como pode ser exibido o resultado.</li>
		<center>
		<p><img SRC="imagens/mes_ano.bmp"><br>
		Figura 3: &nbsp;Gr&aacute;fico exibi&ccedil;&atilde;o dos resultados &ndash; Op&ccedil;&atilde;o meses do  ano <br>
		<br>
		</center></dd>
	<dd><li><b>Horas do dia:</b> a exibi��o � feita agrupando os dados resultantes de acordo com as horas do dia e o gr�fico resultante indica a hora do dia e a respectiva quantidade de acessos. A Figura 4 exemplifica como pode ser exibido o resultado.</li>
		<center>
		<p><img SRC="imagens/horas_dia.bmp"><br>
		Figura 4: &nbsp;Gr&aacute;fico  exibi&ccedil;&atilde;o dos resultados &ndash; Op&ccedil;&atilde;o horas do dia <br>
		<br>
		</center></dd>
	<dd><li><b>Agrupar por semana:</b> a exibi��o � feita agrupando os dados resultantes de acordo com a semana que ocorreram e o gr�fico resultante indica a data inicial e final da semana e a respectiva quantidade de acessos. A Figura 5 mostra como seriam exibidos os resultados se o per�odo escolhido fosse de 01/09/2006 a 30/09/2006 e a op��o escolhida fosse a agrupar por semana.</li>
		<center>
		<p><img SRC="imagens/por_semana.bmp"><br>
		Figura 5: &nbsp;Gr&aacute;fico exibi&ccedil;&atilde;o dos resultados &ndash; Op&ccedil;&atilde;o agrupar  por semana <br>
		<br>
		</center></dd>
	<dd><li><b>Agrupar por m�s:</b> a exibi��o � feita agrupando os dados resultantes de acordo com o m�s que ocorreram e o gr�fico resultante indica o m�s e a respectiva quantidade de acessos. A Figura 6 mostra como seriam exibidos os resultados se o per�odo escolhido fosse de 01/01/2005 a 31/03/2006 e a op��o escolhida fosse a agrupar por m�s. � importante destacar que essa op��o difere da op��o meses do ano, uma vez que, se tivesse sido essa a op��o selecionada, o resultado agruparia as quantidades de acesso que ocorreram em janeiro de 2005 com as que ocorreram em janeiro de 2006, e assim por diante em todos os meses iguais.</li>
		<center>
		<p><img SRC="imagens/por_mes.bmp"><br>
		Figura 6: &nbsp;Gr&aacute;fico exibi&ccedil;&atilde;o dos resultados &ndash; Op&ccedil;&atilde;o agrupar  por m&ecirc;s <br>
		<br>
		</center></dd>
	<dd><li><b>Agrupar por ano:</b> a exibi��o � feita agrupando os dados resultantes de acordo com o ano que ocorreram e o gr�fico resultante indica o ano e a respectiva quantidade de acessos. A Figura 7 mostra como seriam exibidos os resultados se o per�odo escolhido fosse de 01/09/2004 a 30/11/2010 e a op��o escolhida fosse a agrupar por ano.</li>
		<center>
		<p><img SRC="imagens/por_ano.bmp"><br>
		Figura 7: &nbsp;Gr&aacute;fico exibi&ccedil;&atilde;o dos resultados &ndash; Op&ccedil;&atilde;o agrupar  por ano<br>
		<br>
		</center></dd>
</font></font>
<p><a NAME="4. Curso_Disciplina"></a><b><font face="Arial, Helvetica, sans-serif"><font size=-1>4.
Escolha do Curso e da Disciplina&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(<a href="#topo">^topo</a>)</font></font></b>
<p><font face="Arial, Helvetica, sans-serif"><font size=-1>Os itens <em>curso</em> e <em>disciplina</em> s�o op��es geradas a partir do banco de dados onde s�o selecionadas todas os cursos por ele cadastrados, bem como as disciplinas. A op��o <em>curso</em> possibilita a sele��o de uma curso espec�fico criado pelo professor ou mesmo a an�lise de todos os cursos em conjunto, sendo que essa �ltima � definida como padr�o. A escolha da <em>disciplina</em> permite que o professor analise uma turma espec�fica, se for escolhida o <em>curso</em> no item <em>curso</em> e a <em>disciplina</em> espec�fica nesse item, e permite a an�lise de todas as disciplinas de um <em>curso</em>, sendo esta definida como an�lise padr�o, caso na op��o <em>disciplina</em> tenha sido escolhida a op��o todas as disciplinas. A escolha da disciplina e do curso combinadas geram op��es variadas para filtragem dos dados. Cabe ao professor escolher aquela que atende melhor a sua necessidade.
</font></font>
<br>
<br>
<a NAME="5. Analise"></a><b><font face="Arial, Helvetica, sans-serif"><font size=-1>5.
Escolha do Tipo de An&aacute;lise&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(<a href="#topo">^topo</a>)</font></font></b>
<p>Como op&ccedil;&otilde;es de <em>tipo de an&aacute;lise</em> s&atilde;o disponibilizadas as  seguintes:

<dd><li><b>Quantidade de acessos:</b> com essa an�lise, ser�o exibidas as quantidades de acessos de acordo com as especifica��es anteriormente definidas. Todas as figuras utilizadas para exemplificar as op��es exibi��o dos dados utilizavam este <em>tipo de an�lise</em>.</li>
<dd><li><b>Tipo de navega&ccedil;&atilde;o: </b> exibe a quantidade de escolhas de cada tipo  de navega&ccedil;&atilde;o, lembrando que s&atilde;o dois os tipos poss&iacute;veis, tutorial e livre. Esse  item possibilita apenas a exibi&ccedil;&atilde;o atrav&eacute;s do gr&aacute;fico do tipo colunas e tipo  linhas. A Figura 8 exemplifica esse <em>tipo  de an&aacute;lise</em>.</li>
		<center>
		<p><img SRC="imagens/navegacao.bmp"><br>
		Figura 8: &nbsp;Gr&aacute;fico  exibi&ccedil;&atilde;o dos resultados &ndash; Tipo de navega&ccedil;&atilde;o<br>
		<br>
		</center>
<dd><li><b>Utiliza&ccedil;&atilde;o de ajuda: </b>mostra a quantidade de vezes  que os alunos utilizaram a ajuda.</li>
<dd><li><b>Utiliza&ccedil;&atilde;o de busca:</b> mostra a quantidade de  vezes que os alunos utilizaram a busca.</li>
<dd><li><b>Utiliza&ccedil;&atilde;o de mapa:</b> mostra a quantidade de  vezes que os alunos utilizaram a mapa.</li>
<dd><li><b>Conceito com mais acesso:</b> mostra os cinco conceitos  mais acessados pelos usu&aacute;rios. Essa op&ccedil;&atilde;o, futuramente, pode ser adaptada para  permitir que o professor delimite a quantidade de conceitos a serem  visualizados.</li>
  <bb>
  <li><b>Conceito com menos acesso: </b>mostra os cinco conceitos  menos acessados pelos usu&aacute;rios. Essa op&ccedil;&atilde;o, futuramente, pode ser adaptada para  permitir que o professor delimite a quantidade de conceitos a serem visualizados.</li>
<br><br>
  <a NAME="6. Grafico"></a><b><font face="Arial, Helvetica, sans-serif"><font size=-1>6.
Escolha do Tipo de Gr&aacute;fico&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(<a href="#topo">^topo</a>)</font></font></b>
<p>Os tipos de gr&aacute;ficos dispon&iacute;veis s&atilde;o tr&ecirc;s: <em>pizza, colunas </em>e<em> linha</em>. A op&ccedil;&atilde;o escolha do <em>tipo  do gr&aacute;fico</em> &eacute; o &uacute;ltimo item a ser escolhido pelo usu&aacute;rio, uma vez que  depende dos par&acirc;metros escolhidos pode n&atilde;o ser poss&iacute;vel determinado tipo de  gr&aacute;fico. A Figura 9 mostra cada um desses gr&aacute;ficos, a escolha do  tipo do gr&aacute;fico &eacute; do usu&aacute;rio, sendo que o padr&atilde;o ser&aacute; o tipo <em>colunas</em>.
  <center>
	<p><img SRC="imagens/tipo_grafico.bmp"><br>
	  Figura 9: &nbsp;Exemplos de tipos de gr&aacute;ficos<br>
	  <br>
  </center>

 <a NAME="7. Imprimir"></a><b><font face="Arial, Helvetica, sans-serif"><font size=-1>7.
Imprimir e Gerar PDF&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
(<a href="#topo">^topo</a>)</font></font></b>
<p>O professor tem ainda a disposi&ccedil;&atilde;o uma <em>ajuda</em> espec&iacute;fica explicando os  resultados obtidos com cada an&aacute;lise, a op&ccedil;&atilde;o de <em>imprimir</em> e de <em>gerar PDF</em> <em>(Portable Document Format) </em>do resultado  da an&aacute;lise.
<center>
<br><br><br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td background="imagens/pontilhado.gif"  height="2"></td>
  	</tr>

	  <tr>
	    <td bgcolor="eeeeee" width="100%">
	      <div align="center">
		<center><font size="1" face="Arial, Helvetica, sans-serif"><a href="javascript:close();">Fechar</a></font><center>
	      </div>	    </td>
	  </tr>
</table>
<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
	  <tr>
	    <td bgcolor=<?php  echo $cor_padrao; ?> width="100%">
	      <div align="right">
	        <font size="1" face="Arial, Helvetica, sans-serif"><?php  echo " &nbsp; &nbsp;".A_LANG_SYSTEM_COPYRIGHT."";?> </font>	      </div>	    </td>
	  </tr>
</table>
</body>
</html>


