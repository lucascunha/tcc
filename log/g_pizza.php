<?php
header("Content-type: image/png");
include "conecta.php"; 

// ------ configura��es do gr�fico ----------
$largura = 450;
$altura = 300;

// ------ configura��es do c�rculo ----------
$centrox = 150;
$centroy = 150;
$diametro = 210;
$angulo = 0;

// ------ configura��es da legenda ----------
$exibir_legenda = "sim";
$fonte = 2;
$largura_fonte = 6; // largura em pixels (2=6,3=8,4=10)
$altura_fonte = 8; // altura em pixels (2=8,3=10,4=12)
$espaco_entre_linhas = 10;
$margem_vertical = 6;

// canto superior direito da legenda
$lx = 445;
$ly = 5;

// cria imagem e define as cores
$imagem = ImageCreate($largura, $altura);
$fundo = ImageColorAllocate($imagem, 255, 255, 255);
$preto = ImageColorAllocate($imagem, 0, 0, 0);
$azul = ImageColorAllocate($imagem, 100, 149, 237);
$verde = ImageColorAllocate($imagem, 84, 255, 159);
$vermelho = ImageColorAllocate($imagem, 255, 0, 0);
$amarelo = ImageColorAllocate($imagem, 255, 246, 143);
$rosa = ImageColorAllocate($imagem, 255, 105, 180);
$lilas = ImageColorAllocate($imagem, 160, 32, 240);
$laranja = ImageColorAllocate($imagem, 255, 165, 0);

// ------ defini��o dos dados ----------
//$dados = array ("Dom","Seg", "Ter", "Qua", "Qui", "Sex", "S�b");
//$valores = array (0, 0, 13, 300, 5, 15, 25);
$cores = array ($azul, $verde, $vermelho, $amarelo, $rosa, $lilas, $laranja);

    //$numero_times = '1';
    $texto_linha = array("Branco");   
	$numero_anos = '7';
    $vl_coluna = array(1,2,3,4,5,5,7);
	$dados = array("Dom","Seg","Ter","Qua","Qui","Sex","S�b");
    for($j=0 ; $j<$numero_anos; $j++){
    	$ano = $vl_coluna[$j];
		$res = mysql_query("select count(*), dayofweek(data_acesso) as data from log_usuario where dayofweek(data_acesso)=$ano group by data");
        $achou = mysql_numrows($res);
        if($achou)	$valores[] = mysql_result($res,0,0);
        else	$valores[] = 0;
      }
	mysql_close($con);
	// --------------------------------------------------


$numero_linhas = sizeof($texto_linha);
$numero_colunas = sizeof($texto_coluna);
$numero_valores = sizeof($valores);

// ------ c�lculo do total ----------
$total = 0;
$num_linhas = sizeof($dados);
for($i=0 ; $i<$num_linhas; $i++)
    $total += $valores[$i];

// ------ desenha o gr�fico ----------
ImageEllipse ($imagem, $centrox, $centroy, $diametro, $diametro, $preto);
ImageString($imagem, 5, 50, 10, "Gr�fico Resultante da An�lise de Log", $preto);

$raio = $diametro/2;
for($i=0 ; $i<$num_linhas; $i++)
{
    $percentual = ($valores[$i] / $total) * 100;
    $percentual = number_format($percentual, 2);
    $percentual .= "%";
    
    $val = 360 * ($valores[$i] / $total);
    $angulo += $val;
    $angulo_meio = $angulo - ($val / 2);
    
    $x_final = $centrox + $raio * cos(deg2rad($angulo));
    $y_final = $centroy + (- $raio * sin(deg2rad($angulo)));

    $x_meio = $centrox + ($raio/2 * cos(deg2rad($angulo_meio))) ;
    $y_meio = $centroy + (- $raio/2 * sin(deg2rad($angulo_meio)));

    $x_texto = ($centrox + ($raio * cos(deg2rad($angulo_meio))) * 1.2)-15;
    $y_texto = $centroy + (- $raio * sin(deg2rad($angulo_meio))) * 1.2;

    ImageLine($imagem, $centrox, $centroy, $x_final, $y_final, $preto);
    ImageFillToBorder($imagem, $x_meio, $y_meio, $preto, $cores[$i]);
    ImageString($imagem, 2, $x_texto, $y_texto, $percentual, $preto);
}


// ------ CRIA��O DA LEGENDA ----------
if($exibir_legenda=="sim")
{
    // acha a maior string
    $maior_tamanho = 0;
    for($i=0 ; $i<$num_linhas; $i++)
        if(strlen($dados[$i])>$maior_tamanho)
            $maior_tamanho = strlen($dados[$i]);

    // calcula os pontos de in�cio e fim do quadrado
    $x_inicio_legenda = $lx - $largura_fonte * ($maior_tamanho+4);
    $y_inicio_legenda = $ly;

    $x_fim_legenda = $lx;
    $y_fim_legenda = $ly + $num_linhas * ($altura_fonte + $espaco_entre_linhas) + 2*$margem_vertical;
    ImageRectangle($imagem,	$x_inicio_legenda, $y_inicio_legenda,$x_fim_legenda, $y_fim_legenda, $preto);

    // come�a a desenhar os dados
    for($i=0 ; $i<$num_linhas; $i++)
    {
        $x_pos = $x_inicio_legenda + $largura_fonte*3;
        $y_pos = $y_inicio_legenda + $i * ($altura_fonte + $espaco_entre_linhas) + $margem_vertical;

        ImageString($imagem, $fonte, $x_pos, $y_pos, $dados[$i], $preto);
        ImageFilledRectangle ($imagem, $x_pos-2*$largura_fonte, $y_pos, $x_pos-$largura_fonte, $y_pos+$altura_fonte, $cores[$i]);
        ImageRectangle ($imagem, $x_pos-2*$largura_fonte, $y_pos, $x_pos-$largura_fonte, $y_pos+$altura_fonte, $preto);
    }
}

ImagePng($imagem);
ImageDestroy($imagem);
?>
