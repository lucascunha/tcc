<?php
header("Content-type: image/png");
include "conecta.php"; 

// inclui o arquivo com as configura��es
/*Configura��es padr�o do gr�fico*/
	$titulo = "Resultado da An�lise de log";
	$largura = 450;
	$altura = 300;
	$largura_eixo_x = 390;
	$largura_eixo_y = 200;
	$inicio_grafico_x = 50;
	$inicio_grafico_y = 250;

	// ------ configura��es da legenda ----------
	/*$exibir_legenda = "sim";
	$fonte = 3;
	$largura_fonte = 8; // largura em pixels (2=6,3=8,4=10)
	$altura_fonte = 10; // altura em pixels (2=8,3=10,4=12)
	$espaco_entre_linhas = 10;
	$margem_vertical = 5;*/
	
	// canto superior direito da legenda
	$lx = 660;
	$ly = 30;
	// cria imagem e define as cores
	$imagem = ImageCreate($largura, $altura);
	$fundo = ImageColorAllocate($imagem, 255, 255, 255);
	$preto = ImageColorAllocate($imagem, 0, 0, 0);
	$azul = ImageColorAllocate($imagem, 100, 149, 237);
	$verde = ImageColorAllocate($imagem, 84, 255, 159);
	$vermelho = ImageColorAllocate($imagem, 255, 0, 0);
	$amarelo = ImageColorAllocate($imagem, 255, 246, 143);
	$rosa = ImageColorAllocate($imagem, 255, 105, 180);
	$lilas = ImageColorAllocate($imagem, 160, 32, 240);
	$laranja = ImageColorAllocate($imagem, 255, 165, 0);

	$cores_linha = array ($azul, $verde, $vermelho, $amarelo, $magenta);

// ------ defini��o dos dados ----------
    $numero_times = '1';
    $texto_linha = array("Branco");   
	$numero_anos = '7';
    $vl_coluna = array(1,2,3,4,5,5,7);
	$texto_coluna = array("Dom","Seg","Ter","Qua","Qui","Sex","S�b");
    for($j=0 ; $j<$numero_anos; $j++){
    	$ano = $vl_coluna[$j];
		$res = mysql_query("select count(*), dayofweek(data_acesso) as data from log_usuario where dayofweek(data_acesso)=$ano group by data");
        $achou = mysql_numrows($res);
        if($achou)	$valores[] = mysql_result($res,0,0);
        else	$valores[] = 0;
      }
	mysql_close($con);
	// --------------------------------------------------



$numero_linhas = sizeof($texto_linha);
$numero_colunas = sizeof($texto_coluna);
$numero_valores = sizeof($valores);

// ------ obt�m os valores m�nimo e m�ximo de y ----------
$y_maximo = 0;
for($i=0 ; $i<$numero_valores; $i++)
    if($valores[$i]>$y_maximo)
       $y_maximo = $valores[$i];

// ------ calcula o intervalo de varia��o entre os pontos de y ----------
$fator = pow (10, strlen(intval($y_maximo))-1);

if($y_maximo<1)
    $variacao=0.1;
elseif($y_maximo<10)
    $variacao=1;
elseif($y_maximo<2*$fator)
    $variacao=$fator/5;
elseif($y_maximo<5*$fator)
    $variacao=$fator/2;
elseif($y_maximo<10*$fator)
    $variacao=$fator;

// ------ calcula o n�mero de pontos no eixo y ----------
$num_pontos_eixo_y = 0;
$valor = 0;
while ($y_maximo>=$valor)
{
    $valor+=$variacao;
    $num_pontos_eixo_y++;
}

$valor_topo = $valor;
$dist_entre_pontos = $largura_eixo_y / $num_pontos_eixo_y;

// ------- Titulo ---------
ImageString($imagem, 5, 50, 10, "Gr�fico Resultante da An�lise de Log", $preto);

// ------- Eixos x e y ---------
ImageLine($imagem, $inicio_grafico_x, $inicio_grafico_y, $inicio_grafico_x+$largura_eixo_x, $inicio_grafico_y, $preto);
ImageLine($imagem, $inicio_grafico_x, $inicio_grafico_y, $inicio_grafico_x, $inicio_grafico_y-$largura_eixo_y, $preto);

// ------- Pontos no eixo y ---------
$posy = $inicio_grafico_y;
$valor = 0;

for($i=0 ; $i<=$num_pontos_eixo_y; $i++)
{
    $posx = $inicio_grafico_x - (strlen($valor)+2)*6; // 6 da largura da fonte + 2 espa�os

    ImageString($imagem, 2, $posx, $posy-7, $valor, $preto);
    ImageLine($imagem, $inicio_grafico_x-6, $posy, $inicio_grafico_x+$largura_eixo_x, $posy, $preto);
    $valor += $variacao;
    $posy -= $dist_entre_pontos;
}

// ------- Imprime os labels ---------
$largura_coluna = floor($largura_eixo_x / $numero_colunas);
$posx = $inicio_grafico_x;

for($i=0 ; $i<$numero_colunas; $i++)
{
    $pos_label_x = $posx + ($largura_coluna/2) - (strlen($texto_coluna[$i])*6/2);
    $pos_label_y = $inicio_grafico_y+10;
    ImageString($imagem, 2, $pos_label_x, $pos_label_y, $texto_coluna[$i], $preto);
    $posx += $largura_coluna;
}

// ------- Desenha as linhas ---------
for($i=0 ; $i<$numero_linhas; $i++)
{
    $posx = $inicio_grafico_x + $largura_coluna/2;
    $primeiro_ponto = true;
    $indice_j = $i * $numero_colunas;

    for($j=$indice_j ; $j<$indice_j+$numero_colunas; $j++)
    {
        if($j>$indice_j)
            $primeiro_ponto=false;

        $posy = $valores[$j]/$valor_topo * $largura_eixo_y;
        $posy = $inicio_grafico_y - $posy;

        if(!$primeiro_ponto)
            ImageLine($imagem, $ponto_inicio_x, $ponto_inicio_y, $posx, $posy, $cores_linha[$i]);

        // desenha o ponto (de acordo com o formato definido)
        if($formatos[$i]=="losango")
        {
            $pontos = array($posx-4, $posy, $posx, $posy-4, $posx+4, $posy, $posx, $posy+4);
            ImageFilledPolygon($imagem, $pontos, 4, $cores_linha[$i]);
        }
        elseif($formatos[$i]=="quadrado")
        {
            ImageFilledRectangle($imagem, $posx-4, $posy-4, $posx+4, $posy+4, $cores_linha[$i]);
        }
        elseif($formatos[$i]=="triangulo")
        {
            $pontos = array($posx-4, $posy+4, $posx, $posy-4, $posx+4, $posy+4);
            ImageFilledPolygon($imagem, $pontos, 3, $cores_linha[$i]);
        }

        $ponto_inicio_x = $posx;
        $ponto_inicio_y = $posy;

        $posx += $largura_coluna;
    }
}

// ********* CRIA��O DA LEGENDA *********
if($exibir_legenda=="sim")
{
    // acha a maior string
    $maior_tamanho = 0;
    for($i=0 ; $i<$numero_linhas; $i++)
        if(strlen($texto_linha[$i])>$maior_tamanho)
            $maior_tamanho = strlen($texto_linha[$i]);

    // calcula os pontos de in�cio e fim do quadrado
    $x_inicio_legenda = $lx - $largura_fonte * ($maior_tamanho+4);
    $y_inicio_legenda = $ly;

    $x_fim_legenda = $lx;
    $y_fim_legenda = $ly + $numero_linhas * ($altura_fonte + $espaco_entre_linhas) + 2*$margem_vertical;
    ImageRectangle($imagem,	$x_inicio_legenda, $y_inicio_legenda,$x_fim_legenda, $y_fim_legenda, $preto);

    // come�a a desenhar os dados
    for($i=0 ; $i<$numero_linhas; $i++)
    {
        $x_pos = $x_inicio_legenda + $largura_fonte*3;
        $y_pos = $y_inicio_legenda + $i * ($altura_fonte + $espaco_entre_linhas) + $margem_vertical;

        ImageString($imagem, $fonte, $x_pos, $y_pos, $texto_linha[$i], $preto);
        ImageFilledRectangle ($imagem, $x_pos-2*$largura_fonte, $y_pos, $x_pos-$largura_fonte, $y_pos+$altura_fonte, $cores_linha[$i]);
        ImageRectangle ($imagem, $x_pos-2*$largura_fonte, $y_pos, $x_pos-$largura_fonte, $y_pos+$altura_fonte, $preto);
    }
}

ImagePng($imagem);
ImageDestroy($imagem);
?>
