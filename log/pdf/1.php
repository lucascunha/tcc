<?php
define('FPDF_FONTPATH','font/');
include('fpdf.php');

class PDF extends FPDF
{
//Page header
function Header()
{
	$this->Image('logo.jpg',20,20,50);
	$this->Image('logo.jpg',150,70,50);
	$this->SetFont('Arial','B',16);
	$this->Cell(200,35,'Ambiente AdaptWeb - An�lise de Log',0,1,'C'); 
	$this->SetX(21);
	$this->Cell('','10','Resultado da An�lise de Log',0,1,'');
	$this->SetFont('Arial','',12);
	$this->SetX(21);
	$this->Cell('','15','Professor: Nome do Professor',0,1,'');
	$this->SetX(21);
	$this->Cell('','15','Per�odo de: Data_inicio at� Data_final',0,1,''); 
	$this->SetX(21);
	$this->Cell('','15','Curso: Nome(s) do(s) Curso(s) Selecionado(s)',0,1,''); 
	$this->SetX(21);
	$this->Cell('','15','Disciplina: Nome(s) da(s) Disciplina(s) Selecionada(s)',0,1,''); 
	$this->SetX(21);
	$this->Cell('','15','Tipo de An�lise: Tipo da An�lise selecionada',0,1,'');  
	$this->SetX(21);
	$this->Cell('','15','Tipo de Gr�fico: Tipo do Gr�fico Selecionado',0,1,''); 
	$this->SetX(21);
	$this->Cell('','15','Exibi��o dos Dados: Por dia da semana',0,1,''); 
}
//Page footer
function Footer()
{
    $this->SetY(180);
	$this->SetFont('Times','',12);
    $this->Cell(0,2,'Data e Hor�rio que foi gerado o PDF',0,0,'C');
}
}

//Instanciation of inherited class
$pdf=new PDF('L','mm','A4');
$pdf->SetAuthor('Raquel Weirich');
$pdf->SetTitle('Resultado da An�lise de Log');
$pdf->SetMargins(30, 30, 20);
$pdf->Open();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Output();
?>
