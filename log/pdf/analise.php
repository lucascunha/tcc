<?php 
/* -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Inform�tica  
 *       UEL - Departamento de Computa��o
 			UDESC - Universidade do Estado de Santa Catarina
 * -----------------------------------------------------------------------
 *     @package Apresenta��o do Ambiente de Autoria
 *     @subpakage Apresenta��o do Ambiente de Autoria
 *     @file analise.php
 *     @desciption An�lise de Log 
 *     @since 15/09/2006
 *     @author Raquel Weirich (raqueludesc@gmail.com)
 * -----------------------------------------------------------------------         
 */  
?>
<?php 
	include ("./javascript/menu.js");
	$nome_link9       = "Ajuda sobre a An�lise de Log";
	$link9            = "log/ajuda.php?cor=$cor_padrao";
	
	
	/*Estabelecendo conex�o com o servidor*/
	$conn = &ADONewConnection($A_DB_TYPE); 
	$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

if (isset($_POST['action']) && $_POST['action'] == 'submitted') {
	$data_i = $_POST["data_i"];
	$data_f = $_POST["data_f"];
	$exibe = $_POST["exibe"];
	$curso = $_POST["curso"];
	$disciplina = $_POST["disciplina"];
	$tipo_analise = $_POST["tipo_analise"];
	$tipo_grafico = $_POST["tipo_grafico"];
	
	if ($tipo_grafico == '1'){
		$resultado = "<img src=log/grafico.php>";
	}
	else{
		if($tipo_grafico == '2')
			$resultado = "<img src=log/g_pizza.php>";
		else
			$resultado = "<img src=log/g_linha.php>";
	}
	
	//echo "Usuario = $id_usuario <br>";
	//echo "data_i = $data_i <br>";
	//echo "data_f = $data_f <br>";
	//echo "exibe = $exibe <br>";
	//echo "curso = $curso <br>";
	//echo "disciplina = $disciplina <br>";
	//echo "tipo_analise = $tipo_analise <br>";
	//echo "tipo_grafico = $tipo_grafico <br>";	
}
else
	{ 
	$resultado = '�rea destinada para a exi��o dos resultados';

	/********** Definindo as informa��es a serem disponibilizadas no formul�rio *************/
	
	/*Data Inicial - Busca no BD a data de acesso mais antiga de algum aluno matriculado em alguma disciplina do professor*/
		$sql = "select min(l.data_acesso) from log_usuario l, curso c, disciplina d, matricula m where m.id_disc = d.id_disc and m.id_curso=c.id_curso and c.id_usuario = ".$id_usuario." and d.id_usuario = ".$id_usuario." ";
    	$res = mysql_query($sql) or die("Nenhum aluno acessou as suas disciplinas");
        if ($linha = mysql_fetch_array($res)) {
        	$data = $linha[0];
	    }	
		$data_i = substr($data,8,2)."/".substr($data,5,2)."/".substr($data,0,4);
				
	/* Data Final*/
		$hoje = date('Y-m-d');
		$data_f =  substr($hoje,8,2)."/".substr($hoje,5,2)."/".substr($hoje,0,4);

}	//fim do else
				
	/*Seleciona os cursos pelo professor*/
		$c_sql = "SELECT * FROM curso where id_usuario = ".$id_usuario." order by nome_curso";
		$c_rs = $conn->Execute($c_sql);
		if ($c_rs === false) die(A_LANG_COURSE_MSG1);  		  						  
			if (isset($nID_CURSO)) {								  
				$c_sql2 = "SELECT * FROM curso where id_curso='".$nID_CURSO."' and id_usuario=".$id_usuario." ";		  						  		  						  		  						  				 
				$c_rs2 = $conn->Execute($c_sql2);
				$AuxIdCurso = $c_rs2->fields[0];
				$AuxNomeCurso = $c_rs2->fields[1];
				$c_rs2->Close(); 
			}
				
	/*Seleciona as disciplinas cadastardas pelo professor*/
		$d_sql = "SELECT * FROM disciplina where id_usuario = ".$id_usuario." order by nome_disc";
		$d_rs = $conn->Execute($d_sql);
		if ($d_rs === false) die(A_LANG_NOT_DISC);  
  			if (isset($nID_DISC)) {								  
				$d_sql2 = "SELECT * FROM disciplina where id_disc='".$nID_DISC."' and id_usuario=".$id_usuario." ";		  						  		  						  		  						  				 
				$d_rs2 = $conn->Execute($d_sql2);
				$AuxIdDisciplina = $d_rs2->fields[0];
				$AuxNomeDisciplina = $d_rs2->fields[1];
				$d_rs2->Close(); 
			}	
?>

<?php  
  $orelha = array();  
  $orelha = array(
  		array(   
   		   "LABEL" => A_LANG_MNU_LOG , 
     		   "LINK" => "a_index.php", 
     		   "ESTADO" =>"ON"
   		   )   		       		       		   		   
   ); 
  MontaOrelha($orelha);  
?>
 
<table CELLSPACING=5 CELLPADDING=5 border="0" width="100%" height="80%" valign="top" bgcolor=<?php echo $A_COR_FUNDO_ORELHA_ON ?> >
	<tr valign="top">
	  <td>
	  <FORM analise.php method=post>
			<table CELLSPACING=5 CELLPADDING=5 border="0" width="100%" height="80%" valign="top" bgcolor="#BCD2EE">
				<td colspan="2" rowspan="1">Per&iacute;odo de: 
				    <input type="text" class="text" name="data_i" size="14" value='<?php echo "$data_i"?>' maxlength="10" onFocus="this.value='';" onBlur="if( this.value == '' ) this.value='<?php echo "$data_i"?>';"/>
				    &nbsp;&nbsp;&nbsp; at&eacute; &nbsp;&nbsp;&nbsp;
					<input type="text" class="text" name="data_f" size="14" value='<?php echo "$data_f"?>' maxlength="10" onFocus="this.value='';" onBlur="if( this.value == '' ) this.value='<?php echo "$data_f"?>';"/> 
				  </td>
					<td width="63%" bordercolor="#336699" align="right">
						<a href="<?php  echo $link9; ?>" target="_blank" onclick="NewWindow(this.href,'name','750','450','yes');return false;"><font size=2 face="Arial, Helvetica, sans-serif"><?php  echo $nome_link9; ?></font></a>
					</td>
				  </tr>
				  <tr>
				  	<td colspan="2">
						<fieldset style="width:99% "><legend>Exibi��o dos dados</legend>
							<table width="100%">
								<tr>
									<td width="50%"><input type="radio" name="exibe" value="1"/> Dias da semana</td>
									<td><input type="radio" name="exibe" value="5" > Agrupar por semana </td>
								</tr>
								<tr>
									<td width="50%"><input type="radio" name="exibe" value="2" /> Dias do m�s</td>
									<td><input type="radio" name="exibe" value="6" > Agrupar por m�s </td>
								</tr>
								<tr>
									<td><input type="radio" name="exibe" value="3" /> Meses do ano</td>
									<td><input type="radio" name="exibe" value="7" > Agrupar por ano </td>
								</tr>
								<tr>
									<td ><input type="radio" name="exibe" value="4" /> Horas do dia</td>
								</tr>
						</table>
						</fieldset>
					</td>
					<td width="63%" rowspan="8" bordercolor="#336699" bgcolor="#FFFFFF">
						<div align="center"><?php echo "$resultado";?></div>
					</td>
				</tr>
				<tr>
					<td>Curso: </td>
					<td>
						<select class="select" size="1" name="curso" >
							<option value="0">Todos os cursos --------------</option>
								<?php  
									while (!$c_rs->EOF){					 	         									                								                							
										echo "<option value=\"".$c_rs->fields[0]."\">".$c_rs->fields[1]."</option>";
										$c_rs->MoveNext();
									}
									$c_rs->Close(); 
								?>      	
		 			 </select>							
			  	  </td>
				</tr>
				<tr>
					<td width="15%">Disciplina: </td>
					<td width="22%">
						<select name="disciplina" size="1" class="select">
							<option value="0">Todos as disciplinas ----------</option>
								<?php 
									while (!$d_rs->EOF){	
	         							echo "<option value=\"".$d_rs->fields[0]."\">".$d_rs->fields[1]."</option>";
	                					$d_rs->MoveNext();
	             					}
	             					$d_rs->Close(); 
	         					?>      	
					   </select>							
				  </td>		
				</tr>
				<tr>
					<td>Tipo de An&aacute;lise: </td>
					<td><select name=tipo_analise>
							<option value="1" selected>Quantidade de acessos -----</option>
							<option value="2">Tipo de navega��o</option>
							<option value="3">Utiliza��o da ajuda</option>
							<option value="4">Utiliza��o da busca</option>
							<option value="5">Utiliza��o do mapa</option>
							<option value="6">Conceito mais acessado</option>
							<option value="7">Conceito menos acessado</option>
						</select>
					</td>
				</tr>				
				<tr>
					<td>Tipo de Gr&aacute;fico: </td>
					<td><select name=tipo_grafico>
							<option value="1" selected>Coluna -------------------------</option>
							<option value="2">Pizza</option>
							<option value="3">Linha</option>
						</select>
					</td>
				</tr>
				<tr align="center">
					<td colspan="2">
		 					<input type="hidden" name="action" value="submitted" />
							<input type="submit" name="submit" value="Gerar An�lise" />
					</td>
				</tr>
				<tr>
					<td colspan="3" bgcolor="#BCD2EE">
						<div align="right"><a href=# onClick="window.print()">Imprimir</a>
						 	 &zwnj;&nbsp;
						    <a href="pdf/resultado.php" target="_blank" onclick="NewWindow(this.href,'name','750','450','yes');return false;">Gerar PDF</a>
					    </div></td>
				</tr>				
			</table>
			<?php /**********************************************************************/?>		
		</form>
		</td>
	</tr>
</table>
