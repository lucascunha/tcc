<?
/** -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa
 *     UFRGS - Instituto de Informática
 *       UEL - Departamento de Computação
 * -----------------------------------------------------------------------
 *         @name p_entrada_principal.php
 *     @abstract Apresentação do Ambiente AdaptWeb
 *        @since 25/06/2003
 *      @package AdaptWeb
 * -----------------------------------------------------------------------
 */
?>

<?php
  // cria matriz de orelhas
  $orelha = array(
  		array(
   		   "LABEL" => A_LANG_SYSTEM ,
     		   "LINK" => "index.php?opcao=Apresentacao",
     		   "ESTADO" =>"ON"
   		   ),
  		array(
   		   "LABEL" => "O Projeto" ,
     		   "LINK" => "index.php?opcao=EntradaInstituicoes",
     		   "ESTADO" =>"OFF"
   		   ),

  		array(
   		   "LABEL" => "Demonstração" ,
     		   "LINK" => "index.php?opcao=Demo",
     		   "ESTADO" =>"OFF"
   		   ),
  	//	array(
   	//	   "LABEL" => A_LANG_RESEARCHES ,
    // 		   "LINK" => "index.php?opcao=EntradaPesquisadores",
    // 		   "ESTADO" =>"OFF"
   //		   ),
  	//	array(
  	//	   "LABEL" => A_LANG_AUTHORS ,
    //		   "LINK" => "index.php?opcao=EntradaAutores",
     //		   "ESTADO" =>"OFF"
   	//	   )
   		  );

  MontaOrelha($orelha);

?>

<table CELLSPACING=5 CELLPADDING=5 border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> height="80%" style="height:380px;" >

	<tr>
	<td valign=top>
	<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; height:340px;" class="tabela_redonda" >

	<tr>
		<td valign=top>

				<div class="bola" style='width:98%; margin:0 0 0 10px;'>
      			<h2 style="padding-top:10px; padding-left:15px;">
			 	   <?php
				     echo A_LANG_SYSTEM_NAME_DESC;
				   ?>
  	      		</h2>
  	      		</div>
				<br>
				<table border="0" width= "98%" style='margin:0 0 0 10px;'>
					<tr>
						<td>
						<p align="justify">
						<?php
							echo A_LANG_INDEX_ABOUT_INTRO;
						?>
						</p>
						</td>
					</tr>
					<tr>
						<td >
                			<br><b><?php echo A_LANG_INDEX_ABOUT_ENVACCESS; ?></b> <br><br>
                		</td>
                	</tr>

		  			<tr>
						<td>
							<table border="0" cellspacing=0 cellpading=0>
								<tr>
									<td width=100 valign=top>
										<?php echo "<b>".A_LANG_REQUEST_ACCESS_TYPE_TEACHES."</b>"; ?>:</td><td><?php echo A_LANG_INDEX_ABOUT_TINFO ?>
									</td>
								</tr>
								<tr>
									<td colspan=2>
										<br>
									</td>
								</tr>
								<tr>
									<td valign=top>
										<?php echo "<b>".A_LANG_REQUEST_ACCESS_TYPE_STUDANT."</b>"; ?>:</td><td><?php echo A_LANG_INDEX_ABOUT_LINFO; ?>
									</td>
								</tr>
							</table>
		     			</td>
		 			</tr>
				</table>
		</td>
	</tr>
</table>
		</td>
	</tr>
</table>
