<HTML>
<?php

//Gerando arquivos XML com sucesso (se processo de autoria correto, caso contrario nao gera XML
//porem emite mensagens de erro)
//ultima atualizacao em 28/11/02
# Onde se encontra o termo t�pico substituir por conceito. Tanto em nome de variaveis como
# em coment�rios e mensagens


#***********************************************************************************************
#Inicio da minha funcao de geracao dos xml - entrada e a matriz conteudo da veronice
#***********************************************************************************************
#***********************************************************************************************
#INICIO DA CRIACAO DOS ARQUIVOS DE ELEMENTOS DO TOPICO. UM XML PARA CADA TOPICO DA ESTRUTURA
#***********************************************************************************************


#***********************************************************************************************
#Funcao para criacao do nome do arquivo xml para cada topico da estrutura
#***********************************************************************************************
function nomearqxml($conteudo, $i)
{
            $nome=$conteudo[$i]["ARQPRINCIPAL"]["ARQTOP"];
            $tampalavra=strlen($nome);

               $fim=strstr($nome,"."); // retorna tudo que vem depois do ponto
                 $igual=strcasecmp($fim,".html");
                     if ($igual==0) 
                 {
                          $pthtml='.html';
                          $ptxml='.xml';
                          $nomesempt=str_replace($pthtml,$ptxml,$nome);
                 }
                 else
             {
                      $pthtml='.htm';
                      $ptxml='.xml';
                      $nomesempt=str_replace($pthtml,$ptxml,$nome);
             }

			//Samir debug
            //echo "<BR><PRE>"; 
			//print_r($nomesempt);
            //echo"<BR></PRE>"; 
            //________________________ 
             return($nomesempt);

}


#***********************************************************************************************
#Funcao para abertura de arquivo
#***********************************************************************************************
function abrearqesc($arq, &$f)
{
        $f=fopen($arq,"w+",1);
}

#**********************************************************************************************
#Funcao para montar a tag conceito completa de um topico da tabela conceito
#**********************************************************************************************

function conceitoelemento($conteudo, $tam, $fp, $i)
{
      $arquivoxml=$conteudo[$i]["ARQPRINCIPAL"]["ARQTOP"];
      fwrite($fp,"<conceito arquivo=\"$arquivoxml\"> \r\n",400);
      fwrite($fp,"<conteudo> \r\n",400);

      #Monta uma tabela com os arquivos e seus ambientes tecnologicos para o conceito
      $tamarqassocconceito=sizeof($conteudo[$i]["ARQPRINCIPAL"]["ARQPRINCASSOC"]);
      for ($narq=0;$narq<$tamarqassocconceito;$narq++)
      {
          for($col=0;$col<2;$col++)
          {
             $arqelem=$conteudo[$i]["ARQPRINCIPAL"]["ARQPRINCASSOC"][$narq]["ARQTOPASSOC"];
             $ambitec=$conteudo[$i]["ARQPRINCIPAL"]["ARQPRINCASSOC"][$narq]["FLAGTOP"];
             if($col==0)
             {
                  $tabarqelem[$narq][$col]=$arqelem;
             }
             if($col==1)
             {
                  $tabarqelem[$narq][$col]=$ambitec;
             }
          }
      }
	if ($tamarqassocconceito!=0)
	{
      	$tamtabarqelem=sizeof($tabarqelem);
	}
	else
	{
		$tamtabarqelem=0;
	}
      for ($narq=0;$narq<$tamtabarqelem;$narq++)
      {
            $arqelem=$tabarqelem[$narq][0];
            $ambitec=$tabarqelem[$narq][1];
            fwrite($fp,"<elementoconteudo arqelem=\"$arqelem\" ambitec=\"$ambitec\"/> \r\n",400);

      }
      fwrite($fp,"</conteudo> \r\n",400);

        #Para gravar os cursos

      $tamcurso=sizeof($conteudo[$i]["CURSO"]);
      for ($cur=0;$cur<$tamcurso;$cur++)
      {
           for($col=0;$col<2;$col++)
           {
                 $identcurso=$conteudo[$i]["CURSO"][$cur]["ID_CURSO"];
                 $statuscurso=$conteudo[$i]["CURSO"][$cur]["STATUSCUR"];
                 if($col==0)
                 {
                         $tabcur[$cur][$col]=$identcurso;
                 }
                 if($col==1)
                 {
                         $tabcur[$cur][$col]=$statuscurso;
                 }
            }
      }

      for ($cur=0;$cur<$tamcurso;$cur++)
      {
          $identcurso=$tabcur[$cur][0];
          $statuscurso=$tabcur[$cur][1];
          if ($statuscurso=='TRUE')
          {
                  fwrite($fp," <curso identcurso=\"$identcurso\"/>\r\n",400);
          }
      }
      fwrite($fp," </conceito>\r\n",400);
}

#**********************************************************************************************
#Funcao para montar a tag exercicio completa de um topico da tabela conceito
#**********************************************************************************************

function exercicioelemento($conteudo, $tam, $fp, $i)
{
        $tamexerc=sizeof($conteudo[$i]["EXERCICIO"]);
        for ($e=0;$e<$tamexerc;$e++)
        {
                        $idexerc=$conteudo[$i]["EXERCICIO"][$e]["IDEXERC"];
                        $nomeexerc=$conteudo[$i]["EXERCICIO"][$e]["NOMEEXERC"];
						$descexerc=$conteudo[$i]["EXERCICIO"][$e]["DESCEXERC"];
						$palchaveexerc=$conteudo[$i]["EXERCICIO"][$e]["PALCHAVEEXERC"];
                        $arqexerc=$conteudo[$i]["EXERCICIO"][$e]["ARQEXER"];
                        $compexerc=$conteudo[$i]["EXERCICIO"][$e]["COMPEXER"];
					
                      fwrite($fp,"<exercicio idexerc=\"$idexerc\" nomeexerc=\"$nomeexerc\" descexerc=\"$descexerc\" palchaveexerc=\"$palchaveexerc\" arqexerc=\"$arqexerc\" compexerc=\"$compexerc\" >\r\n",400);
                      fwrite($fp,"<conteudo>\r\n",400);

                        #Monta uma tabela com os arquivos e seus ambientes tecnologicos para o conceito
                        $tamarqassocexerc=sizeof($conteudo[$i]["EXERCICIO"][$e]["EXERCICIOASSOC"]);
                      for ($narq=0;$narq<$tamarqassocexerc;$narq++)
                      {
                                  for($col=0;$col<2;$col++)
                                  {
                                     $arqelem=$conteudo[$i]["EXERCICIO"][$e]["EXERCICIOASSOC"][$narq]["ARQEXERASSOC"];
                                    $ambitec=$conteudo[$i]["EXERCICIO"][$e]["EXERCICIOASSOC"][$narq]["FLAGEXER"];
                                     if($col==0)
                                     {
                                          $tabarqelem[$narq][$col]=$arqelem;
                                     }
                                     if($col==1)
                                     {
                                          $tabarqelem[$narq][$col]=$ambitec;
                                     }
                                  }
                      }

#****************

			    if ($tamarqassocexerc!=0)
				{
			      	$tamtabarqelem=sizeof($tabarqelem);
				}
				else
				{
					$tamtabarqelem=0;
				}

                        for ($narq=0;$narq<$tamtabarqelem;$narq++)
                        {
                            $arqelem=$tabarqelem[$narq][0];
                            $ambitec=$tabarqelem[$narq][1];
                              fwrite($fp,"<elementoconteudo arqelem=\"$arqelem\" ambitec=\"$ambitec\" />\r\n",400);

                        }
                      fwrite($fp,"</conteudo> \r\n",400);

                        #Para gravar os cursos

                        $tamcurso=sizeof($conteudo[$i]["EXERCICIO"][$e]["CURSOEXER"]);
                      for ($cur=0;$cur<$tamcurso;$cur++)
                      {
                                   for($col=0;$col<2;$col++)
                                   {
                                         $identcurso=$conteudo[$i]["EXERCICIO"][$e]["CURSOEXER"][$cur]["ID_CURSOEXER"];
                                         $statuscurso=$conteudo[$i]["EXERCICIO"][$e]["CURSOEXER"][$cur]["STATUSCUREXER"];
                                         if($col==0)
                                         {
                                         $tabcur[$cur][$col]=$identcurso;
                                         }
                                         if($col==1)
                                         {
                                         $tabcur[$cur][$col]=$statuscurso;
                                         }
                            }
                        }

                        for ($cur=0;$cur<$tamcurso;$cur++)
                      {
                                  $identcurso=$tabcur[$cur][0];
                            $statuscurso=$tabcur[$cur][1];
                            if ($statuscurso=='TRUE')
                            {
                                  fwrite($fp," <curso identcurso=\"$identcurso\"/>\r\n",400);
                            }
                        }
                        fwrite($fp," </exercicio>\r\n",400);
        }
}

#**********************************************************************************************
#Funcao para montar a tag exemplo completa de um topico da tabela conceito
#**********************************************************************************************

function exemploelemento($conteudo, $tam, $fp, $i)
{
        $tamexemp=sizeof($conteudo[$i]["EXEMPLO"]);
        for ($e=0;$e<$tamexemp;$e++)
        {

                $idexerc=$conteudo[$i]["EXEMPLO"][$e]["IDEXEMP"];
				$nomeexerc=$conteudo[$i]["EXEMPLO"][$e]["NOMEEXEMP"];
                $descexerc=$conteudo[$i]["EXEMPLO"][$e]["DESCEXEMP"];
				$palchaveexerc=$conteudo[$i]["EXEMPLO"][$e]["PALCHAVEEXEMP"];
                $arqexerc=$conteudo[$i]["EXEMPLO"][$e]["ARQEXEMP"];
				
                $compexerc=$conteudo[$i]["EXEMPLO"][$e]["COMPEXEMP"];
				
				/*echo "<pre>";
				print_r ($compexerc);
				echo "</pre>";*/
              
			  fwrite($fp,"<exemplo idexemp=\"$idexerc\" nomeexemp=\"$nomeexerc\" descexemp=\"$descexerc\" palchaveexemp=\"$palchaveexerc\" arqexemp=\"$arqexerc\" compexemp=\"$compexerc\" >\r\n",400);
              fwrite($fp,"<conteudo>\r\n",400);

                  #Monta uma tabela com os arquivos e seus ambientes tecnologicos para o conceito
                  $tamarqassocexerc=sizeof($conteudo[$i]["EXEMPLO"][$e]["EXEMPLOASSOC"]);
                for ($narq=0;$narq<$tamarqassocexerc;$narq++)
                {
                            for($col=0;$col<2;$col++)
                          {
                            $arqelem=$conteudo[$i]["EXEMPLO"][$e]["EXEMPLOASSOC"][$narq]["ARQEXEMPASSOC"];
                            $ambitec=$conteudo[$i]["EXEMPLO"][$e]["EXEMPLOASSOC"][$narq]["FLAGEXEMP"];
                            if($col==0)
                                     {
                                          $tabarqelem[$narq][$col]=$arqelem;
                                     }
                                     if($col==1)
                                     {
                                          $tabarqelem[$narq][$col]=$ambitec;
                                     }
                                  }
                      }


#****************

			    if ($tamarqassocexerc!=0)
				{
			      	$tamtabarqelem=sizeof($tabarqelem);
				}
				else
				{
					$tamtabarqelem=0;
				}

                       for ($narq=0;$narq<$tamtabarqelem;$narq++)
                        {
                            $arqelem=$tabarqelem[$narq][0];
                            $ambitec=$tabarqelem[$narq][1];
                              fwrite($fp,"<elementoconteudo arqelem=\"$arqelem\" ambitec=\"$ambitec\" />\r\n",400);

                        }
                      fwrite($fp,"</conteudo> \r\n",400);

                        #Para gravar os cursos

                        $tamcurso=sizeof($conteudo[$i]["EXEMPLO"][$e]["CURSOEXEMPLO"]);
                      for ($cur=0;$cur<$tamcurso;$cur++)
                      {
                                   for($col=0;$col<2;$col++)
                                   {
                                         $identcurso=$conteudo[$i]["EXEMPLO"][$e]["CURSOEXEMPLO"][$cur]["ID_CURSOEXEMP"];
                                         $statuscurso=$conteudo[$i]["EXEMPLO"][$e]["CURSOEXEMPLO"][$cur]["STATUSCUREXEMP"];
                                         if($col==0)
                                         {
                                         $tabcur[$cur][$col]=$identcurso;
                                         }
                                         if($col==1)
                                         {
                                         $tabcur[$cur][$col]=$statuscurso;
                                         }
                            }
                        }

                        for ($cur=0;$cur<$tamcurso;$cur++)
                      {
                                  $identcurso=$tabcur[$cur][0];
                            $statuscurso=$tabcur[$cur][1];
                            if ($statuscurso=='TRUE')
                            {
                                  fwrite($fp," <curso identcurso=\"$identcurso\"/>\r\n",400);
                            }
                        }
                        fwrite($fp," </exemplo>\r\n",400);
        }
}



#**********************************************************************************************
#Funcao para montar a tag material complementar completa de um topico da tabela conceito
#**********************************************************************************************

function matcompelemento($conteudo, $tam, $fp, $i)
{
          $tammatcomp=sizeof($conteudo[$i]["MATCOMP"]);
        for ($e=0;$e<$tammatcomp;$e++)
        {

            $idmatcomp=$conteudo[$i]["MATCOMP"][$e]["IDMATCOMP"];
            $nomematcomp=$conteudo[$i]["MATCOMP"][$e]["NOMEMATCOMP"];
			$descmatcomp=$conteudo[$i]["MATCOMP"][$e]["DESCMATCOMP"];
			$palchavematcomp=$conteudo[$i]["MATCOMP"][$e]["PALCHAVEMATCOMP"];
            $arquivo=$conteudo[$i]["MATCOMP"][$e]["ARQMATCOMP"];
            fwrite($fp,"<matcomp idmatcomp=\"$idmatcomp\" nomematcomp=\"$nomematcomp\" descmatcomp=\"$descmatcomp\"  palchavematcomp=\"$palchavematcomp\" arqmatcomp=\"$arquivo\" >\r\n",400);
            fwrite($fp,"<conteudo>\r\n",400);

                  #Monta uma tabela com os arquivos e seus ambientes tecnologicos para o conceito
                  $tamarqassocmt=sizeof($conteudo[$i]["MATCOMP"][$e]["MATCOMPASSOC"]);
                for ($narq=0;$narq<$tamarqassocmt;$narq++)
                {
                            for($col=0;$col<2;$col++)
                          {

                                $arqelem=$conteudo[$i]["MATCOMP"][$e]["MATCOMPASSOC"][$narq]["ARQASSOCMAT"];
                            $ambitec=$conteudo[$i]["MATCOMP"][$e]["MATCOMPASSOC"][$narq]["FLAGMAT"];
                            if($col==0)
                                     {
                                          $tabarqelem[$narq][$col]=$arqelem;
                                     }
                                     if($col==1)
                                     {
                                          $tabarqelem[$narq][$col]=$ambitec;
                                     }
                                  }
                      }


#****************

			    if ($tamarqassocmt!=0)
				{
			      	$tamtabarqelem=sizeof($tabarqelem);
				}
				else
				{
					$tamtabarqelem=0;
				}

                        for ($narq=0;$narq<$tamtabarqelem;$narq++)
                        {
                            $arqelem=$tabarqelem[$narq][0];
                            $ambitec=$tabarqelem[$narq][1];
                              fwrite($fp,"<elementoconteudo arqelem=\"$arqelem\" ambitec=\"$ambitec\" />\r\n",400);

                        }
                      fwrite($fp,"</conteudo> \r\n",400);

                        #Para gravar os cursos

                        $tamcurso=sizeof($conteudo[$i]["MATCOMP"][$e]["CURSOMAT"]);
                      for ($cur=0;$cur<$tamcurso;$cur++)
                      {
                                   for($col=0;$col<2;$col++)
                                   {
                                         $identcurso=$conteudo[$i]["MATCOMP"][$e]["CURSOMAT"][$cur]["ID_CURSOMAT"];
                                         $statuscurso=$conteudo[$i]["MATCOMP"][$e]["CURSOMAT"][$cur]["STATUSCURMAT"];
                                         if($col==0)
                                         {
                                         $tabcur[$cur][$col]=$identcurso;
                                         }
                                         if($col==1)
                                         {
                                         $tabcur[$cur][$col]=$statuscurso;
                                         }
                            }
                        }

                        for ($cur=0;$cur<$tamcurso;$cur++)
                      {
                                  $identcurso=$tabcur[$cur][0];
                            $statuscurso=$tabcur[$cur][1];
                            if ($statuscurso=='TRUE')
                            {
                                  fwrite($fp," <curso identcurso=\"$identcurso\"/>\r\n",400);
                            }
                        }
                        fwrite($fp," </matcomp>\r\n",400);
        }
}

#***********************************************************************************************
#***********************************************************************************************
#***********************************************************************************************
#Funcao principal para geracao dos XML de elementos (um para cada topico)
#***********************************************************************************************
#***********************************************************************************************
#***********************************************************************************************

// alterada por Veronice
function geraxml($conteudo,$CodigoDisciplina,$id_usuario, $DOCUMENT_ROOT)
{
        $tam=sizeof($conteudo);
        for($i=0; $i<$tam; $i++)
        {

                $nome=nomearqxml($conteudo,$i); 

		//caminho do arquivo 
                //Samir Mudou//$nome2=$DOCUMENT_ROOT."/Disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$nome; 
				$nome2=$DOCUMENT_ROOT."/disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$nome;
                abrearqesc($nome2,$fp);


                #Gravacao do cabecalho XML no arquivo que ja possui nome
                
                // Alterado por Veronice
                $h = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"no\"?>";
                $h .= "\n<!DOCTYPE textomaterial SYSTEM \"";
                $h .= "/dtd/Elementos_Topico.dtd\">\r\n";                
                fwrite($fp,$h,400);
                
                $numtop=$conteudo[$i]["NUMTOP"];
                $disciplina=$CodigoDisciplina;
                fwrite($fp,"<textomaterial disciplina=\"$disciplina\" numtop=\"$numtop\">\r\n",400);

            	conceitoelemento($conteudo, $tam, $fp, $i);
            	exercicioelemento($conteudo, $tam, $fp, $i);
            	exemploelemento($conteudo, $tam, $fp, $i);
                matcompelemento($conteudo, $tam, $fp, $i);


                fwrite($fp,"        </textomaterial>\r\n",400);
                fclose($fp);
        }
        echo "<BR>\n";
        echo(A_LANG_GENERATION_END);
        echo "<BR>\n";
        echo(A_LANG_GENERATION_CREATED);
        echo($i);
        echo(A_LANG_GENERATION_XML);
        echo "<BR>\n";
}

#***********************************************************************************************
#INICIO DA MANIPULACAO DA ED PARA CRIACAO DO XML ESTRUTURA_TOPICOS.XML
#***********************************************************************************************

#***********************************************************************************************
#Funcao para contar o numero de pontos no numtop
#***********************************************************************************************
function contaponto($tampalavra, $palavra)
{
        $qtdpt=0;
        for($p=0;$p<$tampalavra;$p++)
        {
                if ($palavra[$p]=='.')
                {
                        $qtdpt=$qtdpt+1;
                }
        }
        return($qtdpt);
}

#***********************************************************************************************
#Funcao para pegar a string at� o primeiro ponto (topico para comparar pai e filho)
#***********************************************************************************************
function pegatopico($palavra,$tampalavra,$numpt_atual)
{
        $pts=0;
        $p=0;
        while (($pts<$numpt_atual)&&($p<$tampalavra))
        {
                $palresult[$p]=$palavra[$p];
                if ($palavra[$p]=='.')
                {
                        $pts=$pts+1;
                }
                $p++;
        }
        if ($p==0)
                $aux=0;
        else
                $aux=$palresult;
              
        return($aux);
}

#***********************************************************************************************
#Funcao para montar um topico da estrutura de topicos
#***********************************************************************************************
function montatopico($conteudo, $tam, $fp, $i)
{
                $numtop=$conteudo[$i]["NUMTOP"];
                fwrite($fp,"    <topico numtop=\"$numtop\"\r\n",400);

                $desctop=$conteudo[$i]["DESCTOP"];
                fwrite($fp,"    desctop=\"$desctop\"\r\n",400);

                $abreviacao=$conteudo[$i]["ABREVIACAO"];
                fwrite($fp,"    abreviacao=\"$abreviacao\"\r\n",400);


                $nomearqtag=nomearqxml($conteudo,$i); 
		$arquivoxml=$nomearqtag;
                fwrite($fp,"    arquivoxml=\"$arquivoxml\"\r\n",400);


                $palchave=$conteudo[$i]["PALCHAVE"];
                fwrite($fp,"    palchave=\"$palchave\">\r\n",400);


		#Armazenamento dos Pre-requisitos - Retirando da Tabela de Pre-requisitos

                $tamprereq=sizeof($conteudo[$i]["PREREQUISITO"]);
                for ($pre=0;$pre<$tamprereq;$pre++)
                {
                        for($col=0;$col<2;$col++)
                        {
                                $identprereq=$conteudo[$i]["PREREQUISITO"][$pre]["PREREQ"];
                                $statusprereq=$conteudo[$i]["PREREQUISITO"][$pre]["STATUS"];
                                if($col==0)
                                {
                                        $tabpre[$pre][$col]=$identprereq;
                                }
                                if($col==1)
                                {
                                        $tabpre[$pre][$col]=$statusprereq;
                                }
                        }

                }


                for ($pre=0;$pre<$tamprereq;$pre++)
                {
                        $identpre=$tabpre[$pre][0];
                        $statuspre=$tabpre[$pre][1];
                        if ($statuspre=='TRUE')
                        {
                                fwrite($fp," <prereq identprereq=\"$identpre\"/>\r\n",400);
                        }
                }

		#Fim dos pre-requisitos



		#Armazena cursos relacionados ao t�pico com id e status na tabcur
		#Mesmo que o curso nao esteja relacionado estara na tabcur com status FALSE

                $tamcurso=sizeof($conteudo[$i]["CURSO"]);
                for ($cur=0;$cur<$tamcurso;$cur++)
                {
                        for($col=0;$col<2;$col++)
                        {
                                $identcurso=$conteudo[$i]["CURSO"][$cur]["ID_CURSO"];
                                $statuscurso=$conteudo[$i]["CURSO"][$cur]["STATUSCUR"];
                                if($col==0)
                                {
                                        $tabcur[$cur][$col]=$identcurso;
                                }
                                if($col==1)
                                {
                                        $tabcur[$cur][$col]=$statuscurso;
                                }
                        }

                }


                for ($cur=0;$cur<$tamcurso;$cur++)
                {
                        $identcurso=$tabcur[$cur][0];
			$statuscur=$tabcur[$cur][1];
			if ($statuscur=="TRUE")
			{
                        	fwrite($fp," <curso identcurso=\"$identcurso\">\r\n",400);
                        	fwrite($fp," <elementos>\r\n",400);
			}

			#Verificacao do possui exemplo
			#Verifica se cursos com status TRUE possuem exemplo

                        $tamexemplo=sizeof($conteudo[$i]["EXEMPLO"]);
                        $verificaexemplo=0;
                        for($e=0;$e<$tamexemplo;$e++)
                        {
                                $tamcursoexemplo=sizeof($conteudo[$i]["EXEMPLO"][$e]["CURSOEXEMPLO"]);
                                for($ce=0;$ce<$tamcursoexemplo;$ce++)
                                {
                                        if (($conteudo[$i]["EXEMPLO"][$e]["CURSOEXEMPLO"][$ce]["ID_CURSOEXEMP"]==$tabcur[$cur][0])&&($conteudo[$i]["EXEMPLO"][$e]["CURSOEXEMPLO"][$ce]["STATUSCUREXEMP"]=="TRUE")&&($verificaexemplo==0))
                                        {
                                                fwrite($fp," <exemplo possuiexemp=\"sim\"/>\r\n",400);
                                                $verificaexemplo=1;
                                        }
                                }
                        }


			#Verificacao do possui exercicio
			#Verifica se cursos com status TRUE possuem exercicios

                        $tamexercicio=sizeof($conteudo[$i]["EXERCICIO"]);
                        $verexerc=0;
                        for($e=0;$e<$tamexercicio;$e++)
                        {
                                $tamcursoexercicio=sizeof($conteudo[$i]["EXERCICIO"][$e]["CURSOEXER"]);
                                for($ce=0;$ce<$tamcursoexercicio;$ce++)
                                {
                                        if (($conteudo[$i]["EXERCICIO"][$e]["CURSOEXER"][$ce]["ID_CURSOEXER"]==$tabcur[$cur][0])&&($conteudo[$i]["EXERCICIO"][$e]["CURSOEXER"][$ce]["STATUSCUREXER"]=="TRUE")&&($verexerc==0))
                                        {
                                                fwrite($fp," <exercicio possuiexerc=\"sim\"/>\r\n",400);
                                                                $verexerc=1;
                                        }
                                }
                        }

			#Verificacao do possui material complementar
			#Verifica se cursos com status TRUE possuem material complementar

                        $tammatcomp=sizeof($conteudo[$i]["MATCOMP"]);
                        $verificamatcomp=0;
                        for($e=0;$e<$tammatcomp;$e++)
                        {
                                $tamcursomatcomp=sizeof($conteudo[$i]["MATCOMP"][$e]["CURSOMAT"]);
                                for($ce=0;$ce<$tamcursomatcomp;$ce++)
                                {
                                        if (($conteudo[$i]["MATCOMP"][$e]["CURSOMAT"][$ce]["ID_CURSOMAT"]==$tabcur[$cur][0])&&($conteudo[$i]["MATCOMP"][$e]["CURSOMAT"][$ce]["STATUSCURMAT"]=="TRUE")&&($verificamatcomp==0))
                                        {
                                                fwrite($fp," <matcomp possuimatcomp=\"sim\"/>\r\n",400);
                                                                $verificamatcomp=1;
                                        }
                                }
                        }


			#Finaliza tag de elementos do curso e tag curso (Isto para cada curso relacionado)
			if ($statuscur=="TRUE")
			{
                        	fwrite($fp," </elementos>\r\n",400);
                        	fwrite($fp," </curso>\r\n",400);
			}
                }

#A linha de baixo e o fim da funcao monta topico
}

#***********************************************************************************************
#***********************************************************************************************
#***********************************************************************************************
#Funcao para criacao do xml de estrutura de topico
#***********************************************************************************************
#***********************************************************************************************
#***********************************************************************************************
// Alterado por Veronice
function xmlesttop($conteudo,$CodigoDisciplina,$id_usuario,$DOCUMENT_ROOT)
{
	#A variavel tam guarda o n�mero de linhas (ou o tamanho) da matriz conteudo da Veronice
        $tam=sizeof($conteudo);
	
	#A variavel nome guarda o nome do arquivo XML

        $nome=$DOCUMENT_ROOT."/disciplinas/".$id_usuario."/".$CodigoDisciplina."/estrutura_topico.xml";

        abrearqesc($nome,$fp);

	#Gravacao do cabecalho XML no arquivo que ja possui nome
        fwrite($fp,"<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"no\"?>
        <!DOCTYPE material SYSTEM \"c:\AdaptWeb\dtd\Estrutura_Topico.dtd\">\r\n",400);

        fwrite($fp,"<material disciplina=\"$CodigoDisciplina\">\r\n",400);

	#Laco indicando repeticao da montagem de topico ate o fim da tabela conteudo

	$i=0;
	$contatag=1;
        montatopico($conteudo, $tam, $fp,$i);
        while($i<$tam)
        {
                $controle=0;
                if ($i+1 <= $tam)
                {
                        $palavra=$conteudo[$i]["NUMTOP"];
                        $tampalavra=strlen($palavra);
                        $numpt_atual=contaponto($tampalavra,$palavra);
                        $palavra_atual=$palavra;

                        if ($i+1<$tam)
                                  $palavra=$conteudo[$i+1]["NUMTOP"];
                        else
                                  $palavra=" ";
                        $tampalavra=strlen($palavra);
                        $numpt_proxima=contaponto($tampalavra,$palavra);



                        $palavra_proxima=pegatopico($palavra,$tampalavra,$numpt_proxima);

                        $t=sizeof($palavra_proxima);
			#t guarda o tamanho da proxima palavra para a comparacao

                        for($j=0; $j<($t-1); $j++)
                        {
                                if ($palavra_atual[$j]==$palavra_proxima[$j])
                                {
                                        $controle=$controle+1;
                                }
                        }

			#A linha de baixo e o fim do if i+1<tam
                }


		#Se a variavel controle for igual a 0 entao a palavra_atual e diferente da palavra_proxima
		#Se a variavel controle for diferente de 0 entao as duas palavras (atual e proxima) sao iguais
                $i++;

                if (($numpt_proxima > $numpt_atual) && ($i < $tam))
                {
                        montatopico($conteudo, $tam, $fp,$i);
                        $contatag++;
                }
                else
                {
                        if (($numpt_proxima==$numpt_atual) && ($i < $tam))
                        {
                                fwrite($fp,"        </topico>\r\n",400);
                                $contatag--;
                                montatopico($conteudo, $tam, $fp,$i);
                                $contatag++;
                        }
                        else
                        {
                                 if ($controle==0)
                                 {
                                          $aux1=$contatag;
                                 }
                                 else
                                 {
                                                $aux1=$numpt_atual-$numpt_proxima+1;
                                  }

                                $aux=1;
                                while($aux<=$aux1)
                                {
                                        fwrite($fp,"        </topico>\r\n",400);
                                        $contatag--;
                                        $aux++;
                                }
                                if ($i < $tam)
                                {
                                        montatopico($conteudo, $tam, $fp,$i);
                                        $contatag++;
                                }

                        }
                }


	#A linha de baixo e o fim do while fim da matriz conteudo
        }
        echo "<BR>\n";
        echo(A_LANG_GENERATION_END_STRUCT);
        echo "<BR>\n";
        fwrite($fp,"        </material>\r\n",400);
        fclose($fp);
}

#************************************************************************************************
#************************************************************************************************
# Funcao para verificacao de elementos em branco na matriz da autoria
# Esses elementos que nao podem estar em branco sao os requeridos dentro do documento XML
# e da DTD
#************************************************************************************************
#************************************************************************************************
function verificaerroconceito($conteudo)
{
//alterado por Douglas
         
    $tam=sizeof($conteudo);
    $erroconceito=0;
    echo "<center>";
    echo "<table CELLSPACING=1 CELLPADDING=1 BORDER=0 BGCOLOR='#009ACD' WIDTH='100%'  border='0' >";
    echo "    <TR BGCOLOR=#ffffff>";
    echo "       <td colspan=7>";
    echo "           <p align=center style='font-size:22px;'> ".A_LANG_GENERATION_TOPIC." </p>";
    echo "       </td>";
    echo "    </tr>";
    echo "    <tr BGCOLOR='#009ACD'>";
    echo " 	    <td align='center' valign='top' width='5%'>";
    echo "             ".A_LANG_GENERATION_TOPIC_NUMBER."";
    echo "	    </td>";
    echo "	    <td width='15%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_TOPIC_NAME."";
    echo "          </td>";		
    echo "          <td width='15%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_DESCRIPTION."";
    echo "          </td>";			    					    				 			    				 		
    echo "	    <td width='15%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_KEY_WORD."";
    echo "          </td>";	
    echo "          <td  align='center' valign='top' width='15%'>";
    echo "             ".A_LANG_GENERATION_COURSE."";
    echo "	    </td>";
    echo "          <td width='15%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_PRINC_FILE."";
    echo "	    </td>";			    				 
    echo "          <td width='20%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_ASSOC_FILE."";
    echo "	    </td>";			    				 
    echo "    </tr>";		
    for ($i=0; $i<$tam; $i++) 
	{
		echo "<tr BGCOLOR='#BCD2EE'>";
		echo "<td>";
		if (trim($conteudo[$i]["NUMTOP"])=="")
		{
			echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
			$erroconceito++;
		}
		else 
		{
		        echo "<p align=center> ".$conteudo[$i]["NUMTOP"]." </p>";
		}	
		echo "</td>";
		echo "<td>";
		if (trim($conteudo[$i]["DESCTOP"])=="")
		{
			echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
			$erroconceito++;
		}
		else 
		{
		        echo $conteudo[$i]["DESCTOP"];
		}	
		echo "</td>";
		echo "<td>";
		if (trim($conteudo[$i]["ABREVIACAO"])=="")
		{
			echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
			$erroconceito++;
		}
		else 
		{
		        echo "<p align=center> ".A_LANG_GENERATION_NO_PROBLEM." </p>";
		}	
		echo "</td>";
		echo "<td>";
		if (trim($conteudo[$i]["PALCHAVE"])=="")
		{
			echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
			$erroconceito++;
		}
		else 
		{
		        echo "<p align=center> ".A_LANG_GENERATION_NO_PROBLEM." </p>";
		}	
		echo "</td>";
		echo "<td>";
		#Para gravar os cursos
		$tamcurso=sizeof($conteudo[$i]["CURSO"]);
		$cont = 0;
		for ($cur=0;$cur<$tamcurso;$cur++)
		{
			if (trim($conteudo[$i]["CURSO"][$cur]["STATUSCUR"])=="FALSE")
			{
				$cont++;
			}
		}
		if ( ($cont == $tamcurso) or ($tamcurso==0)) 
		// Nenhum curso selecionado para este t�pico
		{
			echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
			$erroconceito++;
			
		}
		else 
		{
		        echo "<p align=center> ".A_LANG_GENERATION_NO_PROBLEM." </p>";
		}	
		echo "</td>";
		echo "<td>";
		if (trim($conteudo[$i]["ARQPRINCIPAL"]["ARQTOP"])=="")
		{
			echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
			$erroconceito++;
		}
		else 
		{
		        echo "<p align=center> ".A_LANG_GENERATION_NO_PROBLEM." </p>";
		}	
		echo "</td>";
		echo "<td>";
        if (count($conteudo[$i]['ARQPRINCIPAL']['ARQPRINCASSOC'])==0) 
		{
		    echo "<p align=center> ".A_LANG_GENERATION_NOT_EXIST." </p>";
		}
		for($j=0; $j < count($conteudo[$i]['ARQPRINCIPAL']['ARQPRINCASSOC']); $j++)
		{
            print_r($conteudo[$i]['ARQPRINCIPAL']['ARQPRINCASSOC'][$j]['ARQTOPASSOC']);		
	        if ($conteudo[$i]['ARQPRINCIPAL']['ARQPRINCASSOC'][$j]['CARREGADO'] == "0")
			{
				$erroconceito++;
                echo " <font color=#ff0000> (".A_LANG_GENERATION_MISSING.") </font><br>";
			}
			else 
			{
		        echo " (".A_LANG_GENERATION_NO_PROBLEM.") <br>";
		    }
		}
		echo "   </td>";
		echo "</tr>";
	} // end for i		
	echo "</table>";
    return($erroconceito);
}

#************************************************************************************************
# FUNCAO QUE VERIFICA ERRO NAS TAGS DE EXERCICIOS
#************************************************************************************************
function verificaerroexercicio($conteudo)
{
//alterado por Douglas
    echo "<table CELLSPACING=1 CELLPADDING=1 BORDER=0 BGCOLOR='#009ACD' WIDTH='100%'  border='0' >";
    echo "    <TR BGCOLOR=#ffffff>";
    echo "       <td colspan=8>";
    echo "           <p align=center style='font-size:22px; '> ".A_LANG_GENERATION_EXERCISE." </p>";
    echo "       </td>";
    echo "    </tr>";
    echo "    <tr BGCOLOR='#009ACD'>";
    echo " 	    <td align='center' valign='top' width='5%'>";
    echo "             ".A_LANG_GENERATION_TOPIC_NUMBER."";
    echo "	    </td>";
    echo "	    <td width='15%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_TOPIC_NAME."";
    echo "          </td>";		
    echo " 	    <td align='center' valign='top' width='5%'>";
    echo "             ".A_LANG_GENERATION_NUMBER."";
    echo "	    </td>";
	echo "          <td width='15%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_DESCRIPTION."";
    echo "          </td>";			    					    				 			    				 		
	echo "	    <td width='10%' align='center' valign='top'>";
	echo "             ".A_LANG_GENERATION_COMPLEXITY."";
    echo "          </td>";	
    echo "          <td  align='center' valign='top' width='15%'>";
    echo "             ".A_LANG_GENERATION_COURSE."";
    echo "	    </td>";
    echo "          <td width='15%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_PRINC_FILE."";
    echo "	    </td>";			    				 
    echo "          <td width='20%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_ASSOC_FILE."";
    echo "	    </td>";			    				 
    echo "    </tr>";		
	$tam=sizeof($conteudo);
        $erroexercicio=0;
        for ($i=0; $i<$tam; $i++) 
	{
		$tamexerc=sizeof($conteudo[$i]["EXERCICIO"]);
		echo "<tr BGCOLOR='#BCD2EE'>";
		echo "<td rowspan=".$tamexerc.">";
		if (trim($conteudo[$i]["NUMTOP"])=="")
		{
			echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
		}
		else 
		{
		        echo "<p align=center> ".$conteudo[$i]["NUMTOP"]." </p>";
		}	
		echo "</td>";
		echo "<td rowspan=".$tamexerc.">";
		if (trim($conteudo[$i]["DESCTOP"])=="")
		{
			echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
		}
		else 
		{
		        echo $conteudo[$i]["DESCTOP"];
		}	
		echo "</td>";
		if ($tamexerc==0) // Caso n�o tenha nenhum exerc�cio
		{
			echo "<td colspan=6><p align=center> N�o possui </p></td>";
		}
		for ($e=0;$e<$tamexerc;$e++)
		{
		        echo "<td>";
				echo "<p align=center> ".($e+1)." </p>";
			echo "</td>";
			echo "<td>";
			if (trim($conteudo[$i]["EXERCICIO"][$e]["DESCEXERC"])=="")
			{
				echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
				$erroexercicio++;
			}
			else 
			{
				echo $conteudo[$i]["EXERCICIO"][$e]["DESCEXERC"];
			}	
			echo "</td>";
			echo "<td>";
			if ((trim($conteudo[$i]["EXERCICIO"][$e]["COMPEXER"])=="")or
			(trim($conteudo[$i]["EXERCICIO"][$e]["COMPEXER"])=="Sem classifica��o"))
			{
				echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
				$erroexercicio++;
			}
			else 
			{
				echo "<p align=center> ".A_LANG_GENERATION_NO_PROBLEM." </p>";
			}	
			echo "</td>";
			$tamcurso=sizeof($conteudo[$i]["EXERCICIO"][$e]["CURSOEXER"]);
			$cont = 0;
			for ($cur=0;$cur<$tamcurso;$cur++)
			{
				if (trim($conteudo[$i]["EXERCICIO"][$e]["CURSOEXER"][$cur]["STATUSCUREXER"])=="FALSE")
				{
					$cont++;
				}
			}
		        echo "<td>";
			if ($cont == $tamcurso) // Nenhum curso selecionado
			{
				echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
				$erroexercicio++;
			}
			else 
			{
				echo "<p align=center> ".A_LANG_GENERATION_NO_PROBLEM." </p>";
			}	
			echo "</td>";
			echo "<td>";
			if (trim($conteudo[$i]["EXERCICIO"][$e]["ARQEXER"])=="")
			{
				echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
				$erroexercicio++;
			}
			else 
			{
				echo "<p align=center> ".A_LANG_GENERATION_NO_PROBLEM." </p>";
			}	
			echo "</td>";
			echo "<td>";
			if (count($conteudo[$i]['EXERCICIO'][$e]['EXERCICIOASSOC'])==0)
			{
			    echo "<p align=center> ".A_LANG_GENERATION_NOT_EXIST." </p>";
			}
			for($j=0; $j < count($conteudo[$i]['EXERCICIO'][$e]['EXERCICIOASSOC']); $j++)
			{
		        print_r($conteudo[$i]['EXERCICIO'][$e]['EXERCICIOASSOC'][$j]['ARQEXERASSOC']); 
				if ($conteudo[$i]['EXERCICIO'][$e]['EXERCICIOASSOC'][$j]['FLAGEXER']=="0") 
				{
					$erroexercicio++;
					echo " <font color=#ff0000> (".A_LANG_GENERATION_MISSING.") </font><br>";
				}
				else {
					echo " (".A_LANG_GENERATION_NO_PROBLEM.") <br>";
				}
			}
			echo "   </td>";
	        echo "</tr>"; 
			echo "<tr BGCOLOR='#BCD2EE'>";
		} // end for e
	  } // end for i	
      echo "</tr>";	
	  echo "</table>";	  
      return($erroexercicio);
}

#************************************************************************************************
# FUNCAO QUE VERIFICA ERRO NAS TAGS DE EXEMPLOS
#************************************************************************************************
//alterado por Douglas
function verificaerroexemplo($conteudo)
{
	echo "<table CELLSPACING=1 CELLPADDING=1 BORDER=0 BGCOLOR='#009ACD' WIDTH='100%'  border='0' >";
	echo "    <TR BGCOLOR=#ffffff>";
	echo "       <td colspan=8>";
	echo "           <p align=center style='font-size:22px;'> ".A_LANG_GENERATION_EXAMPLE." </p>";
	echo "       </td>";
	echo "    </tr>";
	echo "    <tr BGCOLOR='#009ACD'>";
    echo " 	    <td align='center' valign='top' width='5%'>";
    echo "             ".A_LANG_GENERATION_TOPIC_NUMBER."";
    echo "	    </td>";
    echo "	    <td width='15%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_TOPIC_NAME."";
    echo "          </td>";		
    echo " 	    <td align='center' valign='top' width='5%'>";
    echo "             ".A_LANG_GENERATION_NUMBER."";
    echo "	    </td>";
	echo "          <td width='15%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_DESCRIPTION."";
    echo "          </td>";			    					    				 			    				 		
	echo "	    <td width='10%' align='center' valign='top'>";
	echo "             ".A_LANG_GENERATION_COMPLEXITY."";
    echo "          </td>";	
    echo "          <td  align='center' valign='top' width='15%'>";
    echo "             ".A_LANG_GENERATION_COURSE."";
    echo "	    </td>";
    echo "          <td width='15%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_PRINC_FILE."";
    echo "	    </td>";			    				 
    echo "          <td width='20%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_ASSOC_FILE."";
    echo "	    </td>";			    				 
	echo "    </tr>";		
	$tam=sizeof($conteudo);
	$erroexemplo=0;
	for ($i=0; $i<$tam; $i++) 
	{
		$tamexemp=sizeof($conteudo[$i]["EXEMPLO"]);
		echo "<tr BGCOLOR='#BCD2EE'>";
		echo "<td rowspan=".$tamexemp.">";
		if (trim($conteudo[$i]["NUMTOP"])=="")
		{
			echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
		}
		else 
		{
		        echo "<p align=center> ".$conteudo[$i]["NUMTOP"]." </p>";
		}	
		echo "</td>";
		echo "<td rowspan=".$tamexemp.">";
		if (trim($conteudo[$i]["DESCTOP"])=="")
		{
			echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
		}
		else 
		{
		        echo $conteudo[$i]["DESCTOP"];
		}	
		echo "</td>";
		if ($tamexemp==0) // Caso n�o tenha nenhum exemplo
		{
			echo "<td colspan=6><p align=center> N�o possui </p></td>";
		}
		for ($e=0;$e<$tamexemp;$e++)
		{
		        echo "<td>";
				echo "<p align=center> ".($e+1)." </p>";
			echo "</td>";
			echo "<td>";
			if (trim($conteudo[$i]["EXEMPLO"][$e]["DESCEXEMP"])=="")
			{
				echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
				$erroexemplo++;
			}
			else 
			{
				echo $conteudo[$i]["EXEMPLO"][$e]["DESCEXEMP"];
			}	
			echo "</td>";
			echo "<td>";
			if((trim($conteudo[$i]["EXEMPLO"][$e]["COMPEXEMP"]) == "")or
			(trim($conteudo[$i]["EXEMPLO"][$e]["COMPEXEMP"]) == "Sem classifica��o"))
			{
				echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
				$erroexemplo++;
			}
			else 
			{
				echo "<p align=center> ".A_LANG_GENERATION_NO_PROBLEM." </p>";
			}	
			echo "</td>";
			echo "<td>";
			$tamcurso=sizeof($conteudo[$i]["EXEMPLO"][$e]["CURSOEXEMPLO"]);
			$cont = 0;
			for ($cur=0;$cur<$tamcurso;$cur++)
			{
				if(trim($conteudo[$i]["EXEMPLO"][$e]["CURSOEXEMPLO"][$cur]["STATUSCUREXEMP"])== "FALSE")
				{
					$cont++;
				}
			}
			if ($cont == $tamcurso) // nenhum curso selecionado
			{
				echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
				$erroexemplo++;
			}
			else 
			{
				echo "<p align=center> ".A_LANG_GENERATION_NO_PROBLEM." </p>";
			}	
			echo "</td>";
			echo "<td>";
			if (trim($conteudo[$i]["EXEMPLO"][$e]["ARQEXEMP"]) == "")
			{
				echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
				$erroexemplo++;
			}
			else 
			{
				echo "<p align=center> ".A_LANG_GENERATION_NO_PROBLEM." </p>";
			}	
			echo "</td>";
			echo "<td>";
			if (count($conteudo[$i]['EXEMPLO'][$e]['EXEMPLOASSOC'])==0)
			{
			    echo "<p align=center> ".A_LANG_GENERATION_NOT_EXIST." </p>";
			}
			for($j=0; $j < count($conteudo[$i]['EXEMPLO'][$e]['EXEMPLOASSOC']); $j++)
			{
				print_r($conteudo[$i]['EXEMPLO'][$e]['EXEMPLOASSOC'][$j]['ARQEXEMPASSOC']); 
				if ($conteudo[$i][EXEMPLO][$e]['EXEMPLOASSOC'][$j]['STATUSEXEMP'] == "0") 
				{
					$erroexemplo++;
					echo " <font color=#ff0000> (".A_LANG_GENERATION_MISSING.") </font><br>";
				}
				else {
					echo " (".A_LANG_GENERATION_NO_PROBLEM.") <br>";
				}
			}
			echo "   </td>";
		    echo "</tr>"; 
			echo "<tr BGCOLOR='#BCD2EE'>";
		} // end for e
	} // end for i	
	echo "</tr>";	
	echo "</table>";	  
	return($erroexemplo);
}
#************************************************************************************************
# FUNCAO QUE VERIFICA ERRO NAS TAGS DE MATERIAL COMPLEMENTAR
#************************************************************************************************
//alterado por Douglas
function verificaerromatcomp($conteudo)
{
	echo "<table CELLSPACING=1 CELLPADDING=1 BORDER=0 BGCOLOR='#009ACD' WIDTH='100%'  border='0' >";
	echo "    <TR BGCOLOR=#ffffff>";
	echo "       <td colspan=8>";
	echo "           <p align=center style='font-size:22px;'> ".A_LANG_GENERATION_COMPLEMENTARY." </p>";
	echo "       </td>";
	echo "    </tr>";
	echo "    <tr BGCOLOR='#009ACD'>";
    echo " 	    <td align='center' valign='top' width='5%'>";
    echo "             ".A_LANG_GENERATION_TOPIC_NUMBER."";
    echo "	    </td>";
    echo "	    <td width='15%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_TOPIC_NAME."";
    echo "          </td>";		
	echo " 	    <td align='center' valign='top' width='10%'>";
	echo "             ".A_LANG_GENERATION_NUMBER."";
	echo "	    </td>";
	echo "          <td width='20%' align='center' valign='top'>";
	echo "             ".A_LANG_GENERATION_DESCRIPTION."";
	echo "          </td>";			    					    				 			    				 		
    echo "          <td  align='center' valign='top' width='15%'>";
    echo "             ".A_LANG_GENERATION_COURSE."";
    echo "	    </td>";
    echo "          <td width='15%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_PRINC_FILE."";
    echo "	    </td>";			    				 
    echo "          <td width='20%' align='center' valign='top'>";
    echo "             ".A_LANG_GENERATION_ASSOC_FILE."";
    echo "	    </td>";			    				 
	echo "    </tr>";		
    $tam=sizeof($conteudo);
    $erromatcomp=0;
    for ($i=0; $i<$tam; $i++) 
    {
		$tammatcomp=sizeof($conteudo[$i]["MATCOMP"]);
		echo "<tr BGCOLOR='#BCD2EE'>";
		echo "<td rowspan=".$tammatcomp.">";
		if (trim($conteudo[$i]["NUMTOP"])=="")
		{
			echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
		}
		else 
		{
	        echo "<p align=center> ".$conteudo[$i]["NUMTOP"]." </p>";
		}	
		echo "</td>";
		echo "<td rowspan=".$tammatcomp.">";
		if (trim($conteudo[$i]["DESCTOP"])=="")
		{
			echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
		}
		else 
		{
	        echo $conteudo[$i]["DESCTOP"];
		}	
		echo "</td>";
		if ($tammatcomp==0) // Caso n�o tenha nenhum exemplo
		{
			echo "<td colspan=5><p align=center> N�o possui </p></td>";
		}
		for ($e=0;$e<$tammatcomp;$e++)
		{
	        echo "<td>";
			echo "<p align=center> ".($e+1)." </p>";
			echo "</td>";
			echo "<td>";
			if(trim($conteudo[$i]["MATCOMP"][$e]["DESCMATCOMP"]) == "")
			{
                    echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
					$erromatcomp++;
			}
			else
			{
			        echo $conteudo[$i]["MATCOMP"][$e]["DESCMATCOMP"];
			}		
			echo "</td>";
			echo "<td>";
			$tamcurso=sizeof($conteudo[$i]["MATCOMP"][$e]["CURSOMAT"]);
			$cont = 0;
			for ($cur=0;$cur<$tamcurso;$cur++)
			{
					if(trim($conteudo[$i]["MATCOMP"][$e]["CURSOMAT"][$cur]["STATUSCURMAT"]) == "FALSE")
						{
							$cont++;
						}
			}
			if ($cont == $tamcurso) // nenhum curso selecionado
			{
					echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
					$erromatcomp++;
			}
			else 
			{
				echo "<p align=center> ".A_LANG_GENERATION_NO_PROBLEM." </p>";
			}	
			echo "</td>";
			echo "<td>";
			if(trim($conteudo[$i]["MATCOMP"][$e]["ARQMATCOMP"]) == "")
			{
					echo "<p align=center> <font color=FF0000> ".A_LANG_GENERATION_MISSING." </font> </p>";
					$erromatcomp++;
			}
			else 
			{
				echo "<p align=center> ".A_LANG_GENERATION_NO_PROBLEM." </p>";
			}	
			echo "</td>";
			echo "<td>";
			if (count($conteudo[$i]["MATCOMP"][$e]['MATCOMPASSOC'])==0)
			{
			    echo "<p align=center> ".A_LANG_GENERATION_NOT_EXIST." </p>";
			}
			for($j=0; $j < count($conteudo[$i]["MATCOMP"][$e]['MATCOMPASSOC']); $j++)
			{
					print_r ($conteudo[$i]["MATCOMP"][$e]['MATCOMPASSOC'][$j]['ARQASSOCMAT']);
					if ($conteudo[$i]["MATCOMP"][$e]['MATCOMPASSOC'][$j]['STATUSMAT'] == "0") 
					{
        				$erromatcomp++;
						echo " <font color=#ff0000> (".A_LANG_GENERATION_MISSING.") </font><br>";
					}
					else 
					{
						echo " (".A_LANG_GENERATION_NO_PROBLEM.") <br>";
					}
			}
			echo "   </td>";
			echo "</tr>"; 
			echo "<tr BGCOLOR='#BCD2EE'>";
		} // end for e
	} // end for i
	echo "</tr>";	
	echo "</table>";	  
    return($erromatcomp);
}
#************************************************************************************************
# FUNCAO PRINCIPAL DE VERIFICACAO DE ERROS
# ESTA FUNCAO CHAMA AS DEFINIDAS ACIMA
#************************************************************************************************
//alterado por Douglas
function verificamatriz($conteudo,$CodigoDisciplina)
{
  /*
     $errogeral=0;
     echo "<BR>\n";
     echo(A_LANG_GENERATION_VERIFY);
     echo "<br><br>";
     if ($CodigoDisciplina==" ")
     {
             echo "<BR>\n";
             echo(A_LANG_GENERATION_PROBLEMS);
     }
     $conceito=verificaerroconceito($conteudo);
     $exemplo=verificaerroexemplo($conteudo);
     $exercicio=verificaerroexercicio($conteudo);
     $matcomp=verificaerromatcomp($conteudo);
     if (($conceito!=0) || ($exercicio!=0) || ($exemplo!=0) || ($matcomp!=0))
     {
            $errogeral++;
     }
     return($errogeral);
     */
}


?>
</HTML>
