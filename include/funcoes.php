<?

/* -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Informática  
 *       UEL - Departamento de Computação
 * -----------------------------------------------------------------------
 *       @package Autoria
 *     @subpakage Funcoes
 *          @file funcoes.php
 *    @desciption Agrupar funcoes genericas ao sistema
 *         @since 01/06/2003
 *        @author Veronice de Freitas (veronice@jr.eti.br)
 * -----------------------------------------------------------------------         
 */

   include('include/adodb/adodb.inc.php'); 
   include('include/adodb/tohtml.inc.php');


/** -----------------------------------------------------------------------
 *         @name GerarConteudo($id_disc)
 *   @desciption Carregar matriz de conteudo, feita especialmente para
 *                armazenamento XML em uma nova sessao
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param int $id_disc Codigo da disciplina
 *       @return array $conteudo Matriz da estrutura de conteudo
 * -----------------------------------------------------------------------
 */
function GerarConteudo($id_disc)
{ 
  global $conteudo;
  
  LeMatriz($id_disc);     
  
  return $conteudo;	
} // GerarConteudo  	


/** -----------------------------------------------------------------------
 *         @name InsereRoot()
 *   @desciption Insere um usuário com status ROOT
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *       @return array $conteudo Matriz da estrutura de conteudo
 * -----------------------------------------------------------------------
 */
function InsereRoot()
{ 

  global $conteudo,$A_DB_TYPE, $A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB;
  // incluir o usuário ROOT
  $conn = &ADONewConnection($A_DB_TYPE); 
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
  
  $sql = "SELECT * FROM usuario";
  $rs = $conn->Execute($sql);	  

  // se o banco estiver vazio insere root e um professor com identificação 2 
  // (o professor 2 foi utilizado para navegar livremente na disciplina para demonstração. 
  // (o usuário com a identificação 2 não consegue inserir / excluir / alterar dados
  //  da disciplina de demonstração.
   
  if ($rs->RecordCount() < 2)
  {     
     // insere o utuário root 	
     $sql = "INSERT INTO usuario (nome_usuario,senha_usuario,email_usuario,instituicao_ensino,observacao, Idioma, tipo_usuario, status_usuario) values ('root','123','root@inf.ufrgs.br','','','pt-BR','root','autorizado')";												
		
     $rs = $conn->Execute($sql);		
		
     if ($rs === false) die(A_LANG_NO_ROOT);  				     
     
     // insere o usuário professor (neste caso não será utilizado) -- o banco foi instalado vazio (precisa deste usuário 
     // porque em todo programa existe teste para o usuário 2 não efetuar alteração na disciplina de demonstração     
     $sql = "INSERT INTO usuario (nome_usuario,senha_usuario,email_usuario,instituicao_ensino,observacao, Idioma, tipo_usuario, status_usuario) values ('Demonstração do Ambiente','123','demo@inf.ufrgs.br','','','pt-BR','professor','autorizado')";												
		
     $rs = $conn->Execute($sql);		
		
     if ($rs === false) die(A_LANG_NO_ROOT);  				          
  }
  
  $rs->Close(); 
  
} // InsereRoot   


/** -----------------------------------------------------------------------
 *         @name VerificaBanco()
 *   @desciption False se o banco está fora do ar, True caso contrario
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *       @return boolean $retorno False se o banco está fora do ar, True caso contrario
 * -----------------------------------------------------------------------
 */
function VerificaBanco($v)
{ 

  global $conteudo,$A_DB_TYPE, $A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB;
  $conn = &ADONewConnection($A_DB_TYPE); 
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);  
  $sql = "SELECT * FROM versao";
  $rs = $conn->Execute($sql);
  if ($rs === false)
  {
  	return false;
  }
  else
  {
	  if ($rs->fields['versao'] < $v)
	  {
	  	echo "<font color=#0000ff size=3><b>".A_LANG_MIN_VERSION.$v.A_LANG_INSTALLED_VERSION.$rs->fields['versao']."]</b></font>";
	  }  	
  	return true;
  }
  $rs->close();  
  $conn->close();

  
} // VerificaBanco


/** -----------------------------------------------------------------------
 *         @name GetAccessInformation()
 *   @desciption Obtem informações de local e hora de acesso dos usuários
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *       @return int $accessIP Número do IP 
 * -----------------------------------------------------------------------
 */
function GetAccessInformation()
{
	$strUserAddr  = getenv('REMOTE_ADDR');
	$strUserName  = getenv('REMOTE_HOST');
	$strUserForw  = getenv('HTTP_X_FORWARDED_FOR');
	$strUserCook  = getenv('HTTP_COOKIE');
	$strUserIntr  = getenv('HTTP_CLIENT-IP');

	//echo $strUserAddr." - ", $strUserName." - ", $strUserForw." - ", $strUserCook." - ", $strUserIntr." - ";

	If ($strUserIntr == ""){
		If ($strUserForw == ""){
			$strIPUser = $strUserAddr;
		}
		else {
			$strIPUser = $strUserForw;
		}
	}
	else {
		$strIPUser = $strUserIntr;
	}

	// A variável $accessIP tem o IP do usuario
	$accessIP = $strIPUser;

	// A variável $accessHostName tem o Nome do Host do usuario
	// Samir e Bruno tiraram provisóriamente porque gerava bugs em alguns servidores
  $accessHostName = "";
  //$accessHostName = gethostbyaddr($strIPUser);

        return $accessIP;

} // GetAccessInformation


/** -----------------------------------------------------------------------
 *         @name CriaPasta($Usuario, $Disciplina)
 *   @desciption Cria a estrutura de pastas para armazenar as disciplinas
 *               de cada autor
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 * -----------------------------------------------------------------------
 */
Function CriaPasta($Usuario, $Disciplina)
{	
	
    // criar a pasta do professor	
   if (!is_dir("disciplinas/".$Usuario)) 							   								  
       {
         mkdir("disciplinas/".$Usuario, 0777);
       } 
       
    // criar a pasta da disciplina para o professor		
    if (!is_dir("disciplinas/".$Usuario."/".$Disciplina)) 							   								  
       {
        mkdir("disciplinas/".$Usuario."/".$Disciplina, 0777);
       } 
}			

// ################################  verificar
/**  
 ** @name NomeDisciplina($Disciplina, $Opcao)
 ** @author Veronice de Freitas (veronice@jr.eti.br)
 ** @version 1.0
 ** 
 **/
Function NomeDisciplina($Disciplina, $Opcao)
{		
	
	if ($Opcao == "Topicos")
	   {			
		$Disciplina = $NomeDisciplina;		        				        			
		for($i=0; $i <= strlen($Disciplina); $i++)		
  	   	{ 	  
           		if (substr($Disciplina,$i,1) == "-")
              		{
	                $posicao = $i;	
        	     	break ; 
              		}	
        	}	
          				
       		$Disciplina = " ".trim(substr($Disciplina, $posicao + 2));    
					
		return $Disciplina;  								
	 }		 
} // NomeDisciplina			


/** -----------------------------------------------------------------------
 *         @name Parserhtml($topico)
 *   @desciption  
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param  
 *       @return array $conteudo Matriz da estrutura de conteudo
 * -----------------------------------------------------------------------
 */
 function parseHtml( $s_str )  
{  
            $i_indicatorL = 0;  
            $i_indicatorR = 0;  
            $s_tagOption = "";  
            $i_arrayCounter = 0;  
            $a_html = array();  
            // Search for a tag in string 
            while( is_int(($i_indicatorL=strpos($s_str,"<",$i_indicatorR))) ) {  
                    // Get everything into tag... 
                    $i_indicatorL++;  
                    $i_indicatorR = strpos($s_str,">", $i_indicatorL);  
                    $s_temp = substr($s_str, $i_indicatorL, ($i_indicatorR-$i_indicatorL) );  
                    $a_tag = explode( ' ', $s_temp );  
                    // Here we get the tag's name 
                    list( ,$s_tagName,, ) = each($a_tag);  
                    $s_tagName = trim(strtoupper($s_tagName));  
                    // Well, I am not interesting in <br>, </font> or anything else like that... 
                    // So, this is false for tags without options. 
                    $b_boolOptions = is_array(($s_tagOption=each($a_tag))) && $s_tagOption[1];  
                    if( $b_boolOptions ) {  
                            // Without this, we will mess up the array 
                            $i_arrayCounter = (int)count($a_html[$s_tagName]);  
                            // get the tag options, like src="htt://". Here, s_tagTokOption is 'src' and s_tagTokValue is '"http://"' 

                            do {  
                              $s_tagTokOption = strtoupper(strtok($s_tagOption[1], "="));  
                              $s_tagTokValue  = trim(strtok("="));  
                              $a_html[$s_tagName][$i_arrayCounter][$s_tagTokOption] = $s_tagTokValue;  
                              $b_boolOptions = is_array(($s_tagOption=each($a_tag))) && $s_tagOption[1];  
                            } while( $b_boolOptions );  
                    }  
            }  
            return $a_html;  
} //  Parserhtml


/** -----------------------------------------------------------------------
 *         @name get_array_elems$topico)
 *   @desciption 
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param  
 *       @return  
 * -----------------------------------------------------------------------
*/
function get_array_elems($arrResult, $where="array", $tipo="*")
{ 
	while(list($key,$value)=each($arrResult))
	{ 
		if (is_array($value))
		   { 
		     get_array_elems($value, $where."[$key]", $tipo); 
		   } 
		else 
		  { 
			for ($i=0; $i<count($value);$i++)
			{ 
				if ($tipo=="*" or $tipo==$key)
			     		$where."[$key]=".$value."<BR>\n"; 
			} // for 	
		  } 
	} // while
	
  } // get_array_elems

// ############################ APAGAR
/**  
 ** @name ListaDownload ListaDownload
 ** @author Veronice de Freitas (veronice@jr.eti.br)
 ** @version 1.0
 ** 
 **/

/*
function ListaDownload($arrResult, $where="array", $tipo="*")
{ 
 while(list($key,$value)=each($arrResult))
 { 
         if (is_array($value)){ 
            ListaDownload($value, $where."[$key]", $tipo); 
         } 
         else {              
               for ($i=0; $i<count($value);$i++){ 
               		if ($tipo=="*" or $tipo==$key)
               		{           
                     		echo $value. " (<font color=\"#FF0000\"> carregada para o servidor </font>)";                      		
                     		
                     		echo "<br>\n";                            		
               		}      
               } 
         } 
  } // while     
} // ListaDownload

*/


/** -----------------------------------------------------------------------
 *         @name RemoverDuplicados($vet)
 *   @desciption Remove arquivos associados duplicados
 *               pelo do parser
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param array $vet - vetor de arquivos associados duplicados
 *       @return array $vetUnique - vetor de arquivos associados sem duplicação  
 * -----------------------------------------------------------------------
*/
function RemoverDuplicados($vet)
{ 	           		   	 	 		
	// remove duplicados
	$vetUniqueAux = array_unique($vet);
	
	// arruma indice
	$ind = 0;
	for($j=0; $j < count($vet); $j++) 
	{
	    if (strlen($vetUniqueAux[$j]) > 0)
	       {	    	
	 	  $vetUnique[$ind]  = $vetUniqueAux[$j]; 	   	
	    	  $ind++;
	       }	
        }
		        	        
	return $vetUnique;
	
}// Remove duplicados

/** -----------------------------------------------------------------------
 *         @name RemoverAspas($matriz)
 *   @desciption Remove aspas da matriz de arquivos associados obtidas 
 *               pelo do parser
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param array $$matriz Matriz de arquivos associados com aspas
 *       @return array $$matriz Matriz de arquivos associados sem aspas
 * -----------------------------------------------------------------------
*/
function RemoverAspas($matriz)
{ 	           		   	 	 		
	for($j=0; $j < count($matriz); $j++) 
	{		
		// tirar aspas (")
		$NomeArquivoMat = $matriz[$j][SRC];
		$NomeArquivo = "";	
		for($k=0; $k < strlen($NomeArquivoMat); $k++)
		{ 	    	
			if (substr($NomeArquivoMat,$k,1) <> "\"")	
			   $NomeArquivo .= substr($NomeArquivoMat,$k,1); 
		} // final tirar "
		
		//$matriz[$j][SRC] =$NomeArquivo; 
		$vet[$j] = $NomeArquivo; 
	}
	        
	return $vet;
}// RemoverAspas

/** -----------------------------------------------------------------------
 *         @name VerificaStatusArqAssoc($arquivo, $conteudo, $CodigoDisciplina)
 *   @desciption Verifica se os arquivos associados foram carregados.
 *        @since 07/05/2004
 *       @author Douglas T. do Vale (dtvale@dc.uel.br)
 *        @param array  $conteudo -  matriz com toda estrutura do conteúdo,
 *               int    $CodigoDisciplina - Codigo da disciplina em manutencao no estruturador de conteudo
 * -----------------------------------------------------------------------
*/

function VerificaStatusArqAssoc($conteudo, $CodigoDisciplina, $tipo)
{
   global $conteudo;

   $NomeArquivo = "";
   $tam = count ($conteudo);
   if ($tipo == "principal") {
	for($i=0; $i < $tam; $i++)
 	{
           // Arquivos associados do arquivo principal (conceito)
           $numarq = count ($conteudo[$i]['ARQPRINCIPAL']['ARQPRINCASSOC']);
           if ($numarq > 0) {
               for ($j=0; $j<$numarq; $j++) {
                   $arquivo = $conteudo[$i]['ARQPRINCIPAL']['ARQPRINCASSOC'][$j]['ARQTOPASSOC'];
	           $conteudo[$i]['ARQPRINCIPAL']['ARQPRINCASSOC'][$j]['CARREGADO']=VerificaStatus($arquivo, $CodigoDisciplina);
               } // for j
           } // if
 	} // for i
   }
   else if ($tipo == "exemplo") {
	for($i=0; $i < $tam; $i++)
        {
           // Idem para exemplos
           $numexem = count ($conteudo[$i]['EXEMPLO']);
           if ($numexem > 0) {
               for ($k=0;$k<$numexem;$k++) {
                   $numarq = count ($conteudo[$i]['EXEMPLO'][$k]['EXEMPLOASSOC']);
                   if ($numarq > 0) {
                       for ($j=0; $j<$numarq; $j++) {
                           $arquivo = $conteudo[$i]['EXEMPLO'][$k]['EXEMPLOASSOC'][$j]['ARQEXEMPASSOC'];
    	                   $conteudo[$i]['EXEMPLO'][$k]['EXEMPLOASSOC'][$j]['STATUSEXEMP']=VerificaStatus($arquivo, $CodigoDisciplina);
                       } // for j
                   } // if
               } // for k
           } // if
 	} // for i
   }
   else if ($tipo == "exercicio") {
	for($i=0; $i < $tam; $i++)
        {
           // Idem para exercícios'
           $numexem = count ($conteudo[$i]['EXERCICIO']);
           if ($numexem > 0) {
               for ($k=0;$k<$numexem;$k++) {
                   $numarq = count ($conteudo[$i]['EXERCICIO'][$k]['EXERCICIOASSOC']);
                   if ($numarq > 0) {
                       for ($j=0; $j<$numarq; $j++) {
                           $arquivo = $conteudo[$i]['EXERCICIO'][$k]['EXERCICIOASSOC'][$j]['ARQEXERASSOC'];
    	                   $conteudo[$i]['EXERCICIO'][$k]['EXERCICIOASSOC'][$j]['FLAGEXER']=VerificaStatus($arquivo, $CodigoDisciplina);
                       } // for j
                   } // if
               } // for k
           } // if
 	} // for i
   }
   else if ($tipo == "matcomp") {
	for($i=0; $i < $tam; $i++)
        {
           // Idem para material complementar
           $numexem = count ($conteudo[$i]['MATCOMP']);
           if ($numexem > 0) {
               for ($k=0;$k<$numexem;$k++) {
                   $numarq = count ($conteudo[$i]['MATCOMP'][$k]['MATCOMPASSOC']);
                   if ($numarq > 0) {
                       for ($j=0; $j<$numarq; $j++) {
                           $arquivo = $conteudo[$i]['MATCOMP'][$k]['MATCOMPASSOC'][$j]['ARQASSOCMAT'];
    	                   $conteudo[$i]['MATCOMP'][$k]['MATCOMPASSOC'][$j]['STATUSMAT']=VerificaStatus($arquivo, $CodigoDisciplina);
                       } // for j
                   } // if
               } // for k
           } // if
 	} // for i
   }
   // Atualiza informações na tabela $conteudo
   GravaMatriz($CodigoDisciplina);
} // VerificaStatusArqAssoc

/** -----------------------------------------------------------------------
 *         @name VerificaStatus($arquivo, $conteudo, $CodigoDisciplina)
 *   @desciption Verifica se os arquivos associados foram carregados.
 *        @since 07/05/2004
 *       @author Douglas T. do Vale (dtvale@dc.uel.br)
 *        @param texto  $arquivo -  nome do arquivo associado,
 *               int    $CodigoDisciplina - Codigo da disciplina em manutencao no estruturador de conteudo
 * -----------------------------------------------------------------------
*/

function VerificaStatus($arquivo, $CodigoDisciplina)
{
 	global $id_usuario;

 	$NomeArquivo = "";								 
	for($b=0; $b < strlen($arquivo); $b++)
 	{ 
 	   if (substr($arquivo,$b,1) <> "\"")
 	      $NomeArquivo .= substr($arquivo,$b,1); 
 	}
 	if (file_exists("disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$NomeArquivo))
 	{
 	    return (1); //carregado							   			 
 	}
	else
 	{
 	    return (0); //não carregado							   			 
 	}
} // VerificaStatus
	
/** -----------------------------------------------------------------------
 *         @name InsereEmConteudo($arquivo, $conteudo, $matriz, $posicao, $tipo, $CodigoDisciplina)
 *   @desciption Insere informações relacionado ao conceito na matriz conteúdo 
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param texto  $arquivo -  nome do arquivo de conceito, 
 *               array  $conteudo -  matriz com toda estrutura do conteúdo,
 *               array  $matriz - nome dos arquivos associados em $matriz[$j][SRC], 
 *               int    $posicao - posição do conceito que esta em manutencao,
 *               string $tipo - Tipo de manutenção (seria para colocar conceito, exemplos, exercicio, matcomp) --> nao foi utilizado nesta funcao,
 *               int    $CodigoDisciplina - Codigo da disciplina em manutencao no estruturador de conteudo
 * -----------------------------------------------------------------------
*/

function InsereEmConteudo($arquivo, $conteudo, $matriz, $posicao, $tipo, $CodigoDisciplina)
{
 global $conteudo, $id_usuario;
 
 // o usuario 2 foi utilizado para demonstração da disciplina e não pode incluir, excluir, alterar dados  
 
 if  ($id_usuario <> 2)
 { 
	 if ($tipo=="ARQASOC_ARQTOP")
	    {    	
	    	$conteudo[$posicao]['ARQPRINCIPAL']['ARQTOP']= trim($arquivo); 
	    	
		// ============== arquivos associados ===================			
		// se existir arquivos associados - exclui arquivos
		if (count($conteudo[$posicao]['ARQPRINCIPAL']['ARQPRINCASSOC']) > 0)
		   array_splice($conteudo[$posicao]['ARQPRINCIPAL']['ARQPRINCASSOC'],0,count($conteudo[$posicao]['ARQPRINCIPAL']['ARQPRINCASSOC']));  
		           		   	 	 	
		// remove aspas dos arquivos que estão na matriz (arquivos associados) 
		$vet = RemoverAspas($matriz);
	
		// remove arquivos associados que estão duplicados
		if (count($vet) > 0)
		   $vetUnique = RemoverDuplicados($vet);
	
		// atribui os novos arquivos associados ($matriz[$j][SRC]) na matriz conteúdo 	
		for($j=0; $j < count($vetUnique); $j++) 
		{						
		  $conteudo[$posicao]['ARQPRINCIPAL']['ARQPRINCASSOC'][$j]['ARQTOPASSOC'] = $vetUnique[$j];
		  $conteudo[$posicao]['ARQPRINCIPAL']['ARQPRINCASSOC'][$j]['FLAGTOP'] = "1"; 
		  $conteudo[$posicao]['ARQPRINCIPAL']['ARQPRINCASSOC'][$j]['CARREGADO'] = "verificar status"; 	 	  
		} // for
			
	    } // if
	 
	   GravaMatriz($CodigoDisciplina);	
 } // isuario <> 2
	   
} // InsereEmConteudo

/** -----------------------------------------------------------------------
 *         @name InsereEmConteudo2($arquivo,$conteudo, $matriz, $posicao, $tipo, $CodigoDisciplina, $cDescricao, $IDCursos, $Status, $NivelComp)
 *   @desciption Insere informações relacionado ao tipo de material na matriz conteúdo 
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param texto  $arquivo -  nome do arquivo de conceito, 
 *               array  $conteudo -  matriz com toda estrutura do conteúdo
 *               array  $matriz - nome dos arquivos associados em $matriz[$j][SRC] 
 *               int    $posicao - posição do conceito que esta em manutencao
 *               string $tipo - Tipo de conteudo em manutenção:  exemplos, exercicio, material compelementar 
 *               int    $CodigoDisciplina - Codigo da disciplina em manutencao no estruturador de conteudo
 * 		 string $cDescricao - Descricao do material em manutencao (Exemplo, Exercício, Material Comlementar)
 * 		 array  $IDCursos - matriz contendo a identificação dos cursos  
 *               array  $Status -  Matriz de status (TRUE - curso selecionado) ####################################################### 
 *               string $NivelComp - Nivel de complexidade
 * -----------------------------------------------------------------------
 */
function InsereEmConteudo2($arquivo,$conteudo, $matriz, $posicao, $tipo, $CodigoDisciplina, $cNome, $cDescricao, $cKey, $IDCursos, $Status, $NivelComp)
{ 

 global $conteudo, $id_usuario;

 // o usuario 2 foi utilizado para demonstração da disciplina e não pode incluir, excluir, alterar dados  
 
 if  ($id_usuario <> 2)
 { 
	 // Insere - Material Complementar 
	 if ($tipo=="ARQMAT_COMPLEMENTAR")
	    {      
	    	         	    	
	        // Obter o número do indice da matriz  para inserir o material complementar 
	        $PosMatComp = count($conteudo[$posicao][MATCOMP]);                                
	                
	        // identificação do material complementar (nao foi utilizado)                 
	        $conteudo[$posicao]['MATCOMP'][$PosMatComp]['IDMATCOMP']= 1; 
	        
	        // Descrição do Material Complementar
	        $conteudo[$posicao]['MATCOMP'][$PosMatComp]['NOMEMATCOMP']= $cNome;         
	        $conteudo[$posicao]['MATCOMP'][$PosMatComp]['DESCMATCOMP']= $cDescricao; 
	        $conteudo[$posicao]['MATCOMP'][$PosMatComp]['PALCHAVEMATCOMP']= $cKey; 
	        // Nome do arquivo referente ao material complementar que foi enviado para o servidor
	    	$conteudo[$posicao]['MATCOMP'][$PosMatComp]['ARQMATCOMP']= trim($arquivo); 
	               
	        // Insere na matris os cursos selecionados para o material complementar   		   	 	 	   	
	  	for($j=0; $j < count($IDCursos); $j++) 
		{ //samir 05/02/2004 			    		  	           	       
			   $conteudo[$posicao]['MATCOMP'][$PosMatComp]['CURSOMAT'][$j]['ID_CURSOMAT']   =  $IDCursos[$j];
		       $conteudo[$posicao]['MATCOMP'][$PosMatComp]['CURSOMAT'][$j]['NOME_CURSOMAT'] =  ObterNomeCurso($IDCursos[$j]); 
		       if ($Status[$j] == "on") 
		          {	         
		            $conteudo[$posicao]['MATCOMP'][$PosMatComp]['CURSOMAT'][$j]['STATUSCURMAT']  =  "TRUE"; 
		          }
			    else
				  {
				    $conteudo[$posicao]['MATCOMP'][$PosMatComp]['CURSOMAT'][$j]['STATUSCURMAT']  =  "FALSE"; 
				  }
	       	}	    	
			
		// Remove aspas dos arquivos associados ($matriz[$j]['SRC'])
		$vet = RemoverAspas($matriz);       		   	 	 	
		
		// remove arquivos associados que estão duplicados
		if (count($vet) > 0)
		   $vetUnique = RemoverDuplicados($vet);
		
		// Insere na matriz os arquivos associado para o material complementar 
		for($j=0; $j < count($vetUnique); $j++) 	
		{
		   $conteudo[$posicao]['MATCOMP'][$PosMatComp]['MATCOMPASSOC'][$j]['ARQASSOCMAT'] =  $vetUnique[$j];   
		   $conteudo[$posicao]['MATCOMP'][$PosMatComp]['MATCOMPASSOC'][$j]['FLAGMAT']     = "1"; 
		   $conteudo[$posicao]['MATCOMP'][$PosMatComp]['MATCOMPASSOC'][$j]['STATUSMAT']   = "verificar status";
		}			
	    }  
	   elseif ($tipo=="EXEMPLO")
	    {      
	  	         	    	  	    	
		// Obter o número do indice da matriz  para inserir o exemplo    	
	        $PosExemplo = count($conteudo[$posicao]['EXEMPLO']);                                
	         
	        // identificação do exemplo (nao foi utilizado)                  
	        $conteudo[$posicao]['EXEMPLO'][$PosExemplo]['IDEXEMP']= 1;  
	        
	        //Nome do Exemplo
			$conteudo[$posicao]['EXEMPLO'][$PosExemplo]['NOMEEXEMP']= $cNome;         
			// Descrição do exemplo
	        $conteudo[$posicao]['EXEMPLO'][$PosExemplo]['DESCEXEMP']= $cDescricao;         
			//Palavras-Chave
			$conteudo[$posicao]['EXEMPLO'][$PosExemplo]['PALCHAVEEXEMP']= $cKey;         
	         
	        // Nome do arquivo referente ao exemplo que foi enviado para o servidor
	    	$conteudo[$posicao]['EXEMPLO'][$PosExemplo]['ARQEXEMP']= trim($arquivo); 
	    	
	    	// Nível de Complexidade
	        $conteudo[$posicao]['EXEMPLO'][$PosExemplo]['COMPEXEMP']= $NivelComp;        
	        
	   	// Insere na matriz os cursos selecionados para o exemplo
		//$PosCur = 0;//Samir colocou 04/02/2004
	  	for($j=0; $j < count($IDCursos); $j++) 
		{  
				$conteudo[$posicao]['EXEMPLO'][$PosExemplo]['CURSOEXEMPLO'][$j]['ID_CURSOEXEMP']   =  $IDCursos[$j];
		        $conteudo[$posicao]['EXEMPLO'][$PosExemplo]['CURSOEXEMPLO'][$j]['NOME_CURSOEXEMP'] =  ObterNomeCurso($IDCursos[$j]);//Samir Mudou 03/02/2004 
		       if ($Status[$j] == "on") 
		          {	         
		          $conteudo[$posicao]['EXEMPLO'][$PosExemplo]['CURSOEXEMPLO'][$j]['STATUSCUREXEMP']  =  "TRUE"; 
		          }
			  else
				{
				$conteudo[$posicao]['EXEMPLO'][$PosExemplo]['CURSOEXEMPLO'][$j]['STATUSCUREXEMP']  =  "FALSE"; 
				}

	       	} // for	    	
		
		// Remove aspas dos arquivos associados ($matriz[$j]['SRC'])
		$vet  = RemoverAspas($matriz);
		
		// remove arquivos associados que estão duplicados
		if (count($vet) > 0)
		   $vetUnique = RemoverDuplicados($vet);
		
		// Insere na matriz os arquivos associado para o exemplo         		   	 	 	
		for($j=0; $j < count($vetUnique); $j++) 		
		{
		   $conteudo[$posicao]['EXEMPLO'][$PosExemplo]['EXEMPLOASSOC'][$j]['ARQEXEMPASSOC'] = $vetUnique[$j]; 
		   $conteudo[$posicao]['EXEMPLO'][$PosExemplo]['EXEMPLOASSOC'][$j]['FLAGEXEMP']     = "1"; 
		   $conteudo[$posicao]['EXEMPLO'][$PosExemplo]['EXEMPLOASSOC'][$j]['STATUSEXEMP']   = "verificar status";
		}		
	    }             
	   elseif ($tipo=="EXERCICIO")
	    {          	 	         	    	
		
		// Obter o número do indice da matriz  para inserir o exercício    	  	    	
	        $PosExercicio = count($conteudo[$posicao]['EXERCICIO']);                                
	         
		// identificação do exercício (nao foi utilizado)                           
	        $conteudo[$posicao]['EXERCICIO'][$PosExercicio]['IDEXERC']= 1;  
	        
	        // Nome do exercício
	        $conteudo[$posicao]['EXERCICIO'][$PosExercicio]['NOMEEXERC']= $cNome;
	        // Descrição do exercício
	        $conteudo[$posicao]['EXERCICIO'][$PosExercicio]['DESCEXERC']= $cDescricao; 
	        // Palavras-Chave do exercício
	        $conteudo[$posicao]['EXERCICIO'][$PosExercicio]['PALCHAVEEXERC']= $cKey;
	        
	        // Nível de Complexidade        
	    	$conteudo[$posicao]['EXERCICIO'][$PosExercicio]['ARQEXER']= trim($arquivo); 
	        $conteudo[$posicao]['EXERCICIO'][$PosExercicio]['COMPEXER']= $NivelComp;        
	        
	   	// Insere na matriz os cursos selecionados para o exercício 
	  	for($j=0; $j < count($IDCursos); $j++) 
		{ //Samir Mudou em 05/02/2004
			   $conteudo[$posicao]['EXERCICIO'][$PosExercicio]['CURSOEXER'][$j]['ID_CURSOEXER']   =  $IDCursos[$j];
		       $conteudo[$posicao]['EXERCICIO'][$PosExercicio]['CURSOEXER'][$j]['NOME_CURSOEXER'] =  ObterNomeCurso($IDCursos[$j]); 
		       if ($Status[$j] == "on") 
		          {	        
		            $conteudo[$posicao]['EXERCICIO'][$PosExercicio]['CURSOEXER'][$j]['STATUSCUREXER']  =  "TRUE"; 
		          }
				else
				{
				  $conteudo[$posicao]['EXERCICIO'][$PosExercicio]['CURSOEXER'][$j]['STATUSCUREXER']  =  "FALSE"; 
				}
	       	}	    	
		
		// Remove aspas dos arquivos associados ($matriz[$j]['SRC'])
		$vet = RemoverAspas($matriz);
		
		// remove arquivos associados que estão duplicados
		if (count($vet) > 0)
		   $vetUnique = RemoverDuplicados($vet);
		   
		// Insere na matriz os arquivos associado para o exercício        		   	 	 	
		for($j=0; $j < count($vetUnique); $j++) 
		{
		   $conteudo[$posicao]['EXERCICIO'][$PosExercicio]['EXERCICIOASSOC'][$j]['ARQEXERASSOC'] = $vetUnique[$j]; 
		   $conteudo[$posicao]['EXERCICIO'][$PosExercicio]['EXERCICIOASSOC'][$j]['FLAGEXEMP']     = "1"; 
		   $conteudo[$posicao]['EXERCICIO'][$PosExercicio]['EXERCICIOASSOC'][$j]['FLAGEXER']   = "verificar status";
		}		
	    }             
	            
	    GravaMatriz($CodigoDisciplina);	
}
  	    
} // InsereEmConteudo2


/** -----------------------------------------------------------------------
 *         @name MontaOrelha($orelha)
 *   @desciption Cria as orelhas definidas na matriz   
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param array $orelha Matriz com especificação as orelhas 
 * -----------------------------------------------------------------------
 */
function MontaOrelha($orelha, $nivel)
{	
// Se possuir somente uma orelha (guia)
If (count($orelha)==1)
{

echo "<! --------------- cria UMA orelhas ------------------- -->\n";
echo "<TABLE cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">\n";
echo "  <TR style=\"PADDING-TOP: 6px\">\n";
echo "    <TD>\n";
echo "      <TABLE cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">\n";
echo "        <TBODY>\n";
echo "        <TR>\n";
       
echo "          <!-- Orelha 01 -->\n";
If ($orelha[0]['ESTADO']=="ON") {
	echo "          <TD vAlign=\"top\" width=\"60\"><IMG src=\"imagens/tab/lgo_hm_129x36.".$orelha[0]['ESTADO'].".gif\" align=top width=\"60\" height=\"54\"></TD>\n";

	echo "          <TD noWrap background=\"imagens/tab/menu.ON.bg.gif\">\n";
	echo "          	<font class=\"menu\"><b>".$orelha[0]['LABEL']."</b></font>&nbsp;\n";
	echo "          </TD>\n";
	echo "          <TD><IMG src=\"imagens/tab/menu.".$orelha[0]['ESTADO'].".end.gif\" width=\"51\" height=\"54\"></TD>\n";
}	
else {
	echo "          <TD vAlign=\"top\" width=\"60\">\n";
	echo "          	<IMG src=\"imagens/lgo_hm_129x36.".$orelha[0]['ESTADO'].".gif\" align=top width=\"60\" height=\"54\">\n";
	echo "          </TD>\n";
	echo "          <TD noWrap background=\"imagens/tab/menu.".$orelha[0]['ESTADO'].".bg.gif\">\n";
	
	if  ($orelha[0]['LINK'] == "")	
 		echo "          	 <font class=\"menu\"><b>".$orelha[0]['LABEL']."</b></font>&nbsp;\n";	
	else
		echo "          	<A class=menu tabIndex=120 href=\"".$orelha[0]['LINK']."\"><font class=\"menu\">".$orelha[0]['LABEL']."</font></A>&nbsp;\n";
		
	echo "          </TD>\n";
	echo "          <TD><IMG src=\"imagens/tab/menu.".$orelha[0]['ESTADO'].".end.gif\" width=\"51\" height=\"54\"></TD>\n";
}
echo "          <!-- Orelha Fim -->\n";
echo "          <TD noWrap align=middle background=\"imagens/tab/menu.end.bg.gif\" bgColor=#eeeeee>\n";
echo "          	&nbsp;\n";
echo "          </TD>\n";
echo "          <TD noWrap align=middle background=\"imagens/tab/menu.end.bg.gif\" bgColor=#eeeeee>\n";
echo "          	&nbsp;\n";
echo "          </TD>\n";
echo "          <TD noWrap align=right width=\"100%\" background=\"imagens/tab/menu.end.bg.gif\" bgColor=#eeeeee>&nbsp;</TD>\n";
echo "		</TR>\n";
echo "       </TBODY>\n";
echo "      </TABLE>\n";
echo "    </TD>\n";
echo "  </TR>\n";
echo "</TABLE>\n";

}
else
{
 
echo "<! --------------- cria as orelhas ------------------- -->\n";
echo "<TABLE cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">\n";
echo "  <TR style=\"PADDING-TOP: 6px\">\n";
echo "    <TD>\n";
echo "      <TABLE cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">\n";
echo "        <TBODY>\n";
echo "        <TR>\n";
       
echo "          <!-- Orelha 01 -->\n";
If ($orelha[0]['ESTADO']=="ON") {
	echo "          <TD vAlign=\"top\" width=\"60\"><IMG src=\"imagens/tab/lgo_hm_129x36.".$orelha[0]['ESTADO'].".gif\" align=top width=\"60\" height=\"54\"></TD>\n";

	echo "          <TD noWrap background=\"imagens/tab/menu.ON.bg.gif\">\n";
	echo "          	<font class=\"menu\"><b>".$orelha[0]['LABEL']."</b></font>&nbsp;\n";
	echo "          </TD>\n";
	echo "          <TD><IMG src=\"imagens/tab/menu.".$orelha[0]['ESTADO'].".".$orelha[1]['ESTADO'].".separator.gif\" width=\"63\" height=\"54\"></TD>\n";
}	
else {
	echo "          <TD vAlign=\"top\" width=\"60\">\n";
	echo "          	<IMG src=\"imagens/tab/lgo_hm_129x36.".$orelha[0]['ESTADO'].".gif\" align=top width=\"60\" height=\"54\">\n";
	echo "          </TD>\n";
	echo "          <TD noWrap background=\"imagens/tab/menu.".$orelha[0]['ESTADO'].".bg.gif\">\n";
	
	if  ($orelha[0]['LINK'] == "")	
 		echo "          	 <font class=\"menu\"><b>".$orelha[0]['LABEL']."</b></font>&nbsp;\n";	
	else
		echo "          	<A class=menu tabIndex=120 href=\"".$orelha[0]['LINK']."\"><font class=\"menu\">".$orelha[0]['LABEL']."</font></A>&nbsp;\n";
		
	echo "          </TD>\n";
	echo "          <TD><IMG src=\"imagens/tab/menu.".$orelha[0]['ESTADO'].".".$orelha[1]['ESTADO'].".separator.gif\" width=\"63\" height=\"54\"></TD>\n";
}	

for($i=1; $i < count($orelha)-1; $i++){  		
	if (($orelha[$i]['ESTADO']=="ON")){
		echo "          <!-- Orelha".$i."-->\n";
		echo "          <TD noWrap background=\"imagens/tab/menu.".$orelha[$i]['ESTADO'].".bg.gif\">\n";
		echo "          	<font class=\"menu\"><b>".$orelha[$i]['LABEL']."</b></font>&nbsp;\n";
		echo "          </TD>\n";
		echo "          <TD><IMG src=\"imagens/tab/menu.".$orelha[$i]['ESTADO'].".".$orelha[$i+1]['ESTADO'].".separator.gif\" width=\"63\" height=\"54\"></TD>\n";				
	}
	else{
		echo "          <!-- Orelha".$i."-->\n";
		echo "          <TD noWrap background=\"imagens/tab/menu.".$orelha[$i]['ESTADO'].".bg.gif\">\n";
		
 		if  ($orelha[$i]['LINK'] == "")
 		    	echo "          	 <font class=\"menu\">".$orelha[$i]['LABEL']."</font>&nbsp;\n";
		else 		 
			echo "          	<A class=menu tabIndex=120 href=\"".$orelha[$i]['LINK']."\"><font class=\"menu\">".$orelha[$i]['LABEL']."</font></A>&nbsp;\n";
		    
		echo "          </TD>\n";
		echo "          <TD><IMG src=\"imagens/tab/menu.".$orelha[$i]['ESTADO'].".".$orelha[$i+1]['ESTADO'].".separator.gif\" width=\"63\" height=\"54\"></TD>\n";	
	}  	
}


echo "          <!-- Orelha 05 -->\n";
If ($orelha[$i]['ESTADO']=="ON") {
	echo "          <TD noWrap background=\"imagens/tab/menu.".$orelha[$i]['ESTADO'].".bg.gif\">\n";
	echo "          	<font class=\"menu\"><b>".$orelha[$i]['LABEL']."</b></font>&nbsp;\n";
	echo "          </TD>\n";
	echo "          <TD><IMG src=\"imagens/tab/menu.".$orelha[$i]['ESTADO'].".end.gif\" width=\"51\" height=\"54\"></TD>\n";
}	
else {
	echo "          <TD noWrap background=\"imagens/tab/menu.".$orelha[$i]['ESTADO'].".bg.gif\">\n";
 	if  ($orelha[$i]['LINK'] == "")	
 		echo "          	 <font class=\"menu\"><b>".$orelha[$i]['LABEL']."</b></font>&nbsp;\n";
 	else
		echo "          	<A class=menu tabIndex=120 href=\"".$orelha[$i]['LINK']."\"><font class=\"menu\">".$orelha[$i]['LABEL']."</font></A>&nbsp;\n";
		
	echo "          </TD>\n";
	echo "          <TD><IMG src=\"imagens/tab/menu.".$orelha[$i]['ESTADO'].".end.gif\" width=\"51\" height=\"54\"></TD>\n";
}  	          
  	            	                    
echo "          <!-- Orelha Fim -->\n";
if ($nivel == 2)
{
  $fundo =  "imagens/tab/menu.end.bg.branco.gif";

}
else
{
  $fundo =  "imagens/tab/menu.end.bg.gif";
}
echo "          <TD noWrap align=middle background=\"$fundo\" bgColor=#eeeeee>\n";
echo "          	&nbsp;\n";
echo "          </TD>\n";
echo "          <TD noWrap align=middle background=\"$fundo\" bgColor=#eeeeee>\n";
echo "          	&nbsp;\n";
echo "          </TD>\n";
echo "          <TD noWrap align=right width=\"100%\" background=\"$fundo\" bgColor=#eeeeee>&nbsp;</TD>\n";
echo "		</TR>\n";
echo "       </TBODY>\n";
echo "      </TABLE>\n";
echo "    </TD>\n";
echo "  </TR>\n";
echo "</TABLE>\n";	
 }

} // Orelha


/** -----------------------------------------------------------------------
 *         @name Autenticacao($origem)
 *   @desciption Verifica se o usuário nao esta logado e redireciona para
 *               o formulário de login (ambiente de autoria)
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param string $origem - opcao de oriegem
 * -----------------------------------------------------------------------
 */
function Autenticacao($origem)
{
   global $logado, $id_usuario;

   if (!$logado) 
      header("Location: a_index.php?opcao=Login&origem=".$origem);
} // Autenticacao





/** -----------------------------------------------------------------------
 *         @name AutenticacaoNavega($origem)
 *   @desciption Verifica se o usuário nao esta logado e redireciona para
 *               o formulário de login (ambiente de navegação)
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param string $origem - opcao de oriegem
 * -----------------------------------------------------------------------
*/
function AutenticacaoNavega($origem)
{
   global $logado, $id_usuario;

   if (!$logado) 
      header("Location: n_index_navegacao.php?opcao=Login&origem=".$origem);
} // AutenticacaoNavega






/** -----------------------------------------------------------------------
 *         @name Menu($opcao)
 *   @desciption Menu do ambiente de AUTORIA
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param string $opcao - item selecionado
 * -----------------------------------------------------------------------
 */
function Menu($opcao)

{
global $logado, $id_usuario, $tipo_usuario;

MenuEntrada($opcao);

 if (
      ($opcao <> 'AmbienteAdmin') &&
      ($opcao <> 'AutorizarAcessoAutor') &&
      ($opcao <> 'QuestionarioResult') &&
      ($opcao <> 'ToDo') 
    )  

     
 {    


echo "<p>\n";
echo "<TABLE cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">\n";

// ----------------> Linha - separar itens de menu
echo "<tr class=mnuGroup>\n";
echo "<td colspan=2 class=mnuTitulo>\n";
echo "<b>".A_LANG_MNU_AUTHORING."</b>";
echo "</td>\n";
echo "</tr>\n";

  echo "<tr class=mnuBreak>\n";
  echo "<td>";
  echo "</td>\n";
  echo "</tr>\n";


// ----------------> Cadastro de curso
echo "<tr class=mnuItem>\n";
echo "<td  valign=\"top\">\n";
if ($opcao == "CadastroCurso" || $opcao == "AlteracaoCurso" || $opcao == "ExclusaoCurso")
   {
   echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"a_index.php?opcao=CadastroCurso\"><b>".A_LANG_MNU_COURSE."</b></a>&nbsp;\n";
   }
else
   {
   echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"a_index.php?opcao=CadastroCurso\">".A_LANG_MNU_COURSE."</a>&nbsp;\n";
   }
echo "</td>\n";
echo "</tr>\n";

// ----------------> Cadastro de Disciplina
echo "<tr class=mnuItem>\n";
echo "<td valign=\"top\">\n";
if ($opcao == "CadastroDisciplina" || $opcao == "AlteracaoDisciplina" || $opcao == "ExclusaoDisciplina")
   {
    echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"a_index.php?opcao=CadastroDisciplina\"><b>".A_LANG_MNU_DISCIPLINES."</b></a>\n";
   }
else
   {
    echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"a_index.php?opcao=CadastroDisciplina\">".A_LANG_MNU_DISCIPLINES."</a>\n";
   } 
echo "</td>\n";
echo "</tr>\n";

// ----------------> Cadastro de Disciplina / Curso
echo "<tr class=mnuItem>\n";
echo "<td valign=\"top\">\n";
if ($opcao == "CadastroDisciplinaCurso")
    {
    echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"a_index.php?opcao=CadastroDisciplinaCurso\"><b>".A_LANG_MNU_DISCIPLINES_COURSE."</b></a>\n";
    } 
else
    {
    echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"a_index.php?opcao=CadastroDisciplinaCurso\">".A_LANG_MNU_DISCIPLINES_COURSE."</a>\n";
    }
echo "</td>\n";
echo "</tr>\n";

// ----------------> Estruturador de conteúdo
echo "<tr class=mnuItem>\n";
echo "<td valign=\"top\">\n";
if ($opcao == "EntradaTopicos" || $opcao == "Topicos" || $opcao == "TopicosEstruturarArvore" || $opcao == "TopicosAltera" || $opcao == "TopicosExemplos" || $opcao == "TopicosExercicios" || $opcao == "TopicosComplementar" || $opcao == "TopicoAvaliacao"  || $opcao == "TopicosCadastro1"  )
   {
   //echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"a_index.php?opcao=EntradaTopicos\"><b>".A_LANG_MNU_STRUTURALIZE_TOPICS."</b></a>\n";
    echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"a_index.php?opcao=EntradaTopicos\"><b>Estruturar Disciplina</b></a>\n";
   }
else 
   {
   //echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"a_index.php?opcao=EntradaTopicos\">".A_LANG_MNU_STRUTURALIZE_TOPICS."</a>\n";
    echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"a_index.php?opcao=EntradaTopicos\">Estruturar Disciplina</a>\n";
   }
echo "</td>\n";

// ---------------->  Avaliação Somativa (Claudiomar Desanti - 26/10/2007)

echo "<tr class=mnuItem>\n";
echo "<td valign=\"top\">\n";
if ($opcao == "FuncaoAvaliacao"  || $opcao == "FuncaoAvaliacao"  || $opcao == "FuncaoIndex"  || $opcao == "FuncaoMedia"  || $opcao == "FuncaoLiberar"  || $opcao == "FuncaoAjuste"  || $opcao == "FuncaoDivulgar"  || $opcao == "FuncaoRelatorio"  || $opcao == "FuncaoBackup"  )
    {
    echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"a_index.php?opcao=FuncaoAvaliacao\"><b>".A_LANG_MNU_AVS."</b></a>\n";
    }
else
    {
    echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"a_index.php?opcao=FuncaoAvaliacao\">".A_LANG_MNU_AVS."</a>\n";
    }
echo "</td>\n";
echo "</tr>\n";


// ----------------> Liberar Disciplina
echo "<tr class=mnuItem>\n";
echo "<td valign=\"top\">\n";
if ($opcao == "LiberarDisciplina")
    {
    echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"a_index.php?opcao=LiberarDisciplina\"><b>".A_LANG_MNU_LIBERATE_DISCIPLINES."</b></a>\n";
    }
else
    {
    echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"a_index.php?opcao=LiberarDisciplina\">".A_LANG_MNU_LIBERATE_DISCIPLINES."</a>\n";
    }
echo "</td>\n";
echo "</tr>\n";


// ----------------> Autorizar acesso
echo "<tr class=mnuItem>\n";

echo "<td valign=\"top\">\n";
if ($opcao == "AutorizarAcesso")
   {
   echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"a_index.php?opcao=AutorizarAcesso\"><b>".A_LANG_MNU_ACCESS_LIBERATE."</b></a> \n";
   }
else
   {
   echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"a_index.php?opcao=AutorizarAcesso\">".A_LANG_MNU_ACCESS_LIBERATE."</a> \n";
   }
echo "</td>\n";
echo "</tr>\n";

// ----------------> Repositório
echo "<tr class=mnuItem>\n";

echo "<td valign=\"top\">\n";
if ($opcao == "Repositorio" || $opcao == "Export" || $opcao == "Import" || $opcao == "Pesq" || $opcao == "Gerenciar")
   {
   echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"a_index.php?opcao=Repositorio\"><b>".A_LANG_MNU_REPOSITORY."</b></a> \n";
   }
else
   {
   echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"a_index.php?opcao=Repositorio\">".A_LANG_MNU_REPOSITORY."</a> \n";
   }
echo "</td>\n";
echo "</tr>\n";

// ---------------->  Análise de Log
echo "<tr class=mnuItem>\n";
echo "<td  valign=\"top\">\n";
if ($opcao == "AnaliseLog")
   {
   echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"a_index.php?opcao=AnaliseLog\"><b>".A_LANG_MNU_LOG."</b></a>&nbsp;\n";
   }
else
   {
   echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"a_index.php?opcao=AnaliseLog\">".A_LANG_MNU_LOG."</a>&nbsp;\n";
   }

echo "</td>\n";
echo "</tr>\n";

echo '<tr class="mnuItem">';
echo '<td valign="top">';
if($opcao == 'AnaliseInterativa') {
	echo '<img src="imagens/xtree/openfoldericon.png"></td><td><a href="a_index.php?opcao=AnaliseInterativa">', A_LANG_ANA_INT, '</a>&nbsp;';
} else {
	echo '<img src="imagens/xtree/foldericon.png"></td><td><a href="a_index.php?opcao=AnaliseInterativa">', A_LANG_ANA_INT, '</a>&nbsp;';
}
echo '</td>';
echo '</tr>';

// ----------------> Questionário -04/06/07 
echo "<tr class=mnuItem>\n";
echo "<td  valign=\"top\">\n";
if ($opcao == "Questionario")
   {
   echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"a_index.php?opcao=Questionario\"><b>".QUESTIONARIO."</b></a>&nbsp;\n";
   }
else
   {
   echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"a_index.php?opcao=Questionario\">".QUESTIONARIO."</a>&nbsp;\n";
   }
echo "</td>\n";
echo "</tr>\n";

echo "</table>\n";
echo "</p>\n";

} 

}
/** -----------------------------------------------------------------------
 *         @name MenuEntrada($opcao)
 *   @desciption Menu do ambiente AdaptWeb
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param string $opcao - item selecionado
 * -----------------------------------------------------------------------
 */


function MenuEntrada($opcao)
{

if ($opcao == 'Principal' || $opcao == 'Apresentacao'  || $opcao == 'EntradaInstituicoes'  || $opcao == 'Demo')
{
  $opcao = 'Home';
}

global $logado, $id_usuario, $tipo_usuario, $status_usuario;

echo "<p>\n";
echo "<TABLE cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">\n";

//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************// 

if ($logado)
{
	// ----------------> Linha - separar itens de menu
	echo "<tr class=mnuGroup>\n";
	echo "<td colspan=2 class=mnuTitulo>\n";
	echo "<b>".A_LANG_MNU_MY_ACCOUNT."</b>";
	echo "</td>\n";
	echo "</tr>\n";


  echo "<tr class=mnuBreak>\n";
  echo "<td>";
  echo "</td>\n";
  echo "</tr>\n";


	// ---------------- > home
	echo "<tr class=mnuItem>\n";
	echo "<td valign=\"top\">\n";
	if ($opcao == "Home" || $opcao=="SolicitaAcesso" )
	   {
	   echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"index.php?opcao=Home\"><b>".A_LANG_MNU_HOME."</b></a>&nbsp;\n";
	   }
	else
	   {
	   echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"index.php?opcao=Home\">".A_LANG_MNU_HOME."</a>&nbsp;\n";
	   }
	echo "</td>\n";
	echo "</tr>\n";

	// ----------------> Acesso ao ambiente de nevegação
	echo "<tr class=mnuItem>\n";
	echo "<td valign=\"top\">\n";
	if ($opcao == "AmbienteAluno" || $opcao == "AssistirDisciplinas" || $opcao == "Avaliacao" || $opcao == "Matricula" || $opcao == "AguardandoMatricula" || $opcao =="DisciplinasAutor"  || $opcao =="Navega" || $opcao =="MuralRecados" || $opcao =="CriaRecados" || $opcao =="ForumDiscussao"  || $opcao =="CriaTopicoDiscussao"  || $opcao =="AvaliarAlunosAutor"  || $opcao =="AvaliarAlunosAutorFormativa"  || $opcao =="ParticiparTopicoDiscussaoAutor"  || $opcao == "Mural"   )
	   {
	   echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"n_index_navegacao.php?opcao=AmbienteAluno\"><b>".A_LANG_MNU_NAVIGATION."</b></a>&nbsp;\n";
	   }
	else
	  {
	   echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"n_index_navegacao.php?opcao=AmbienteAluno\">".A_LANG_MNU_NAVIGATION."</a>&nbsp;\n";
	  } 
	echo "</td>\n";
	echo "</tr>\n";


//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************// 

  if ($tipo_usuario == "root" || $tipo_usuario == "professor" )  
	{
		echo "<tr class=mnuItem>\n";
		echo "<td  valign=\"top\">\n";	
  
		if ($status_usuario == "autorizado" || $tipo_usuario == "root" ) 
		    {    
			if ($opcao == "Autoria" or $opcao == "AmbienteProfessor"  || $opcao == "CadastroCurso"|| $opcao == "AlteracaoCurso"|| $opcao == "ExclusaoCurso"|| $opcao == "CadastroDisciplina"|| $opcao == "AlteracaoDisciplina"|| $opcao == "ExclusaoDisciplina"|| $opcao == "CadastroDisciplinaCurso"|| $opcao == "EntradaTopicos"|| $opcao == "Topicos" || $opcao == "TopicosEstruturarArvore" || $opcao == "TopicosAltera" || $opcao == "TopicosExemplos" || $opcao == "TopicosExercicios" || $opcao == "TopicosComplementar" || $opcao == "TopicoAvaliacao"  || $opcao == "TopicosCadastro1"  || $opcao == "FuncaoAvaliacao"  || $opcao == "FuncaoIndex"  || $opcao == "FuncaoMedia"  || $opcao == "FuncaoLiberar"  || $opcao == "FuncaoAjuste"  || $opcao == "FuncaoDivulgar"  || $opcao == "FuncaoRelatorio"  || $opcao == "FuncaoBackup"  || $opcao == "LiberarDisciplina"  || $opcao == "AutorizarAcesso"  || $opcao == "Repositorio"  || $opcao == "Export"  || $opcao == "Import"  || $opcao == "Gerenciar"  || $opcao == "Pesq"  || $opcao == "AnaliseLog"  || $opcao == "AmbienteProfessor"  || $opcao == "Questionario"  )
			   {
			      echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"a_index.php?opcao=AmbienteProfessor\"><b>".A_LANG_MNU_AUTHORING."</b></a>&nbsp;\n";
			   }   
			else
			   { 
			      echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"a_index.php?opcao=AmbienteProfessor\">".A_LANG_MNU_AUTHORING."</a>&nbsp;\n";
			   }
		    }            
		else
		{     	        	       
			if ($opcao == "Autoria")
			   {
			      echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"index.php?opcao=Autoria\"><b>".A_LANG_MNU_AUTHORING."</b></a>&nbsp;\n";
			   }   
			else
			   { 
			      echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"index.php?opcao=Autoria\">".A_LANG_MNU_AUTHORING."</a>&nbsp;\n";
			   }
	
		}
		
		echo "</td>\n";
		echo "</tr>\n";
	}

//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************// 

  if ($tipo_usuario == "root" )  
  {
    echo "<tr class=mnuItem>\n";
    echo "<td  valign=\"top\">\n";  
  
   
      if ($opcao == "AutorizarAcessoAutor" or $opcao == "QuestionarioResult"  || $opcao == "ToDo"  || $opcao == "AmbienteAdmin" )
         {
            echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"a_index.php?opcao=AmbienteAdmin\"><b>".A_LANG_MNU_MANAGE."</b></a>&nbsp;\n";
         }   
      else
         { 
            echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"a_index.php?opcao=AmbienteAdmin\">".A_LANG_MNU_MANAGE."</a>&nbsp;\n";
         }
    
    echo "</td>\n";
    echo "</tr>\n";
  }  


//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************// 

	echo "<tr class=mnuItem>\n";
	echo "<td valign=\"top\">\n";
	if ($logado) 
	   if ($opcao == "AcessoAgenda")
	       echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"index.php?opcao=AcessoAgenda\"><b>".A_LANG_MNU_AGENDA."</b></a> <br><br>\n";
	   else
	       echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"index.php?opcao=AcessoAgenda\">".A_LANG_MNU_AGENDA."</a> <br><br>\n";
	else
	   
	   
	echo "</td>\n";
	echo "</tr>\n";	

//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************// 	

}
else
{ 

//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//
//************************************************************************************************************************************************************//  

	// ----------------> Linha - separar itens de menu
	echo "<tr class=mnuGroup>\n";
	echo "<td colspan=2 class=mnuTitulo>\n";
	echo "<b>".A_LANG_MNU_MY_ACCOUNT."</b>";
	echo "</td>\n";
	echo "</tr>\n";


  echo "<tr class=mnuBreak>\n";
  echo "<td>";
  echo "</td>\n";
  echo "</tr>\n";

	// ---------------- > home
	echo "<tr class=mnuItem>\n";
	echo "<td valign=\"top\">\n";
	if ($opcao == "Home")
	   {
	   echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"index.php?opcao=Home\"><b>".A_LANG_MNU_HOME."</b></a>&nbsp;\n";
	   }
	else
	   {
	   echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"index.php?opcao=Home\">".A_LANG_MNU_HOME."</a>&nbsp;\n";
	   }
	echo "</td>\n";
	echo "</tr>\n";
	
	// ----------------> Login
	echo "<tr class=mnuItem>\n";
	echo "<td valign=\"top\">\n";
	if ($opcao == "Login")
   		echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"index.php?opcao=Login\"><b>".A_LANG_MNU_LOGIN."</b></a> \n";
	else
   		echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"index.php?opcao=Login\">".A_LANG_MNU_LOGIN."</a> \n";

	echo "</td>\n";
	echo "</tr>\n";

	// ----------------> Solicitar acesso ou alterar cadastro
	echo "<tr class=mnuItem>\n";
	echo "<td valign=\"top\">\n";
	if ($logado) 
	   if ($opcao == "SolicitaAcesso")
	       echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"index.php?opcao=SolicitaAcesso\"><b>".A_LANG_MNU_UPDATE_USER."</b></a> \n";
	   else
	       echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"index.php?opcao=SolicitaAcesso\">".A_LANG_MNU_UPDATE_USER."</a> \n";
	else
	  if ($opcao == "SolicitaAcesso")
	      echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"index.php?opcao=SolicitaAcesso\"><b>".A_LANG_MNU_NEW_USER."</b></a> \n";
	  else
	      echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"index.php?opcao=SolicitaAcesso\">".A_LANG_MNU_NEW_USER."</a> \n"; 
	   
	   
	echo "</td>\n";
	echo "</tr>\n";	

}	

echo "</table>\n";
echo "<p>\n";


 if ($tipo_usuario == "root")
 {

  if (
      (($opcao == 'AmbienteAdmin')) ||
      (($opcao == 'AutorizarAcessoAutor')) ||
       (($opcao == 'QuestionarioResult')) ||
        (($opcao == 'ToDo')) 
     )
  {


echo "<p>\n";
echo "<TABLE cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">\n";

// ----------------> Linha - separar itens de menu
echo "<tr class=mnuGroup>\n";
echo "<td colspan=2 class=mnuTitulo>\n";
echo "<b>".A_LANG_MNU_MANAGE."</b>";
echo "</td>\n";
echo "</tr>\n";

  echo "<tr class=mnuBreak>\n";
  echo "<td>";
  echo "</td>\n";
  echo "</tr>\n";


 
    // ----------------> Autorizar acesso - autor
    echo "<tr class=mnuItem>\n";
    echo "<td valign=\"top\">\n";
  
    if ($opcao == "AutorizarAcessoAutor")
         {
            echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"index.php?opcao=AutorizarAcessoAutor\"><b>".A_LANG_MNU_RELEASE_AUTHORING."</b></a> \n";
         }
    else
         {
            echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"index.php?opcao=AutorizarAcessoAutor\">".A_LANG_MNU_RELEASE_AUTHORING."</a> \n";
         }
    echo "</td>\n";
    echo "</tr>\n";
    
    // ----------------> QUESTIONÁRIO -19/06/2007
    echo "<tr class=mnuItem>\n";
  
    echo "<td valign=\"top\">\n";
  
    if ($opcao == "QuestionarioResult")
         {
        echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"index.php?opcao=QuestionarioResult\"><b>".QS_ROOT."</b></a> \n";
         }
    else
         {
             echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"index.php?opcao=QuestionarioResult\">".QS_ROOT."</a> \n";
         }
    echo "</td>\n";
    echo "</tr>\n";

    // ----------------> Backup
    echo "<tr class=mnuItem>\n";
  
    echo "<td valign=\"top\">\n";
  
    if ($opcao == "ToDo")
         {
        echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"index.php?opcao=ToDo\"><b>".A_LANG_MNU_BAKCUP."</b></a> \n";
         }
    else
         {
             echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"index.php?opcao=ToDo\">".A_LANG_MNU_BACKUP."</a> \n";
         }
    echo "</td>\n";
    echo "</tr>\n";   
  
    // ----------------> Gráficos
    echo "<tr class=mnuItem>\n";
  
    echo "<td valign=\"top\">\n";
  
    if ($opcao == "GraficoAcessoDisciplina")
         {
        echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"index.php?opcao=GraficoAcessoDisciplina\">".A_LANG_MNU_GRAPH."</a> \n";
         }
    else
         {
             echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"index.php?opcao=GraficoAcessoDisciplina\">".A_LANG_MNU_GRAPH."</a> \n";
         }
    echo "</td>\n";
    echo "</tr>\n";

echo "</table>\n";
echo "</p>\n";
    
}
   } // root - autorizar acesso


} // Menu inicial do ambiente adaptWeb


/** -----------------------------------------------------------------------
 *         @name MenuNavegacao($opcao)
 *   @desciption Menu do ambiente de navegação
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param string $opcao - item selecionado
 * -----------------------------------------------------------------------
 */
function MenuNavegacao($opcao) 

{
global $logado, $id_usuario, $tipo_usuario;

MenuEntrada($opcao);

echo "<p>\n";
echo "<TABLE cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">\n";


// ----------------> Linha - separar itens de menu
echo "<tr class=mnuGroup>\n";
echo "<td colspan=2 class=mnuTitulo>\n";
echo "<b>".A_LANG_MNU_NAVIGATION."</b>";
echo "</td>\n";
echo "</tr>\n";


  echo "<tr class=mnuBreak>\n";
  echo "<td>";
  echo "</td>\n";
  echo "</tr>\n";

// ---------------->  Disciplina(s) disponível
echo "<tr class=mnuItem>\n";
echo "<td  valign=\"top\">\n";
if ($opcao == "AssistirDisciplinas" || $opcao =="DisciplinasAutor" || $opcao=="NavegaAutor" || $opcao =="MuralRecados" || $opcao == "CriaRecados" || $opcao =="ForumDiscussao" || $opcao =="CriaTopicoDiscussao" || $opcao =="AvaliarAlunosAutor" || $opcao =="AvaliarAlunosAutorFormativa" || $opcao == "Navega"  || $opcao == "ParticiparTopicoDiscussaoAutor")
   {
   echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"n_index_navegacao.php?opcao=AssistirDisciplinas\"><b>".A_LANG_MNU_DISCIPLINES_RELEASED."</b></a>&nbsp;\n";
   }
else
   {
   echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"n_index_navegacao.php?opcao=AssistirDisciplinas\">".A_LANG_MNU_DISCIPLINES_RELEASED."</a>&nbsp;\n";
   }
echo "</td>\n";
echo "</tr>\n";

// ----------------> Avaliacao
echo "<tr class=mnuItem>\n";
echo "<td  valign=\"top\">\n";
if ($opcao == "Avaliacao" || $opcao == "Mural")
   {
   echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"n_index_navegacao.php?opcao=Avaliacao\"><b>".A_LANG_MNU_AVS_NAV."</b></a>&nbsp;\n";
   }
else
   {
   echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"n_index_navegacao.php?opcao=Avaliacao\">".A_LANG_MNU_AVS_NAV."</a>&nbsp;\n";
   }
echo "</td>\n";
echo "</tr>\n";

// ----------------> Matricula
echo "<tr class=mnuItem>\n";
echo "<td valign=\"top\">\n";
if ($opcao == "Matricula")
   {
    echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"n_index_navegacao.php?opcao=Matricula\"><b>".A_LANG_MNU_SUBSCRIBE."</b></a>\n";
   }
else
   {
    echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"n_index_navegacao.php?opcao=Matricula\">".A_LANG_MNU_SUBSCRIBE."</a>\n";
   }
echo "</td>\n";
echo "</tr>\n";

// ----------------> Aguardando Matricula
echo "<tr class=mnuItem>\n";
echo "<td valign=\"top\">\n";
if ($opcao == "AguardandoMatricula")
   {
    echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"n_index_navegacao.php?opcao=AguardandoMatricula\"><b>".A_LANG_MNU_WAIT."</b></a>\n";
   }
else
   {
    echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"n_index_navegacao.php?opcao=AguardandoMatricula\">".A_LANG_MNU_WAIT."</a>\n";
   }
echo "</td>\n";
echo "</tr>\n";


echo "</table>\n";
echo "</p>\n";
} 

// MenuNavegacao 


/** -----------------------------------------------------------------------
 *         @name GravaMatriz($id_disc)
 *   @desciption Grava matriz conteúdo atual no banco de dados
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param int $id_disc - código da disciplina em manutenção
 * -----------------------------------------------------------------------
 */
function GravaMatriz($id_disc)
{ 	
	global $conteudo,$A_DB_TYPE, $A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB;

  	// Conexão com o banco de dados
  	$conn = &ADONewConnection($A_DB_TYPE); 
	$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// conteúdo da matriz conteúdo para ser armazenado no Banco de Dados
  	$conteudo_db=$conn->qstr(serialize($conteudo));

        // Atualiza o conteúdo da disciplina em manutenção
  	$sql = "UPDATE disciplina SET topicos ="
		.$conteudo_db." where id_disc=".$id_disc.";";

  	$rs = $conn->Execute($sql);
  	if ($rs === false) die(A_LANG_ERROR_BD);  
  	
  	$rs->Close();  	
  	
  	// Atualiza a sessão 
  	//session_unregister("conteudo");
  	//session_register("conteudo");  	  
      unset($_SESSION['conteudo']);
      $_SESSION['conteudo'] = $conteudo;
}

/** -----------------------------------------------------------------------
 *         @name GravaMatriz($id_disc)
 *   @desciption obter o nome da disciplina em manutenção - Informação 
 *               para as telas dos ambiente (tela principal, Ambiente de
 *               autoria)
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param int $id_disc - código da disciplina em manutenção
 *       @return string $retorno - Nome da disciplina
 * -----------------------------------------------------------------------
 */
function ObterNomeDisciplina($id_disc)
{
  global $conteudo,$A_DB_TYPE, $A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB;
 
   $conn = &ADONewConnection($A_DB_TYPE); 
   $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
     
  $sql = "SELECT nome_disc FROM disciplina where id_disc=".$id_disc.";";
  $rs = $conn->Execute($sql);

  if ($rs->fields['nome_disc'] <> "")
  	$retorno = $rs->fields['nome_disc'];
  else
        $retorno = A_LANG_DISC_NOT_FOUND;
  	

  if ($rs === false) die(A_LANG_ERROR_BD);  

  $rs->Close();
   
  return $retorno;
}// ObterNomeDisciplina


/** -----------------------------------------------------------------------
 *         @name ObterNomeCurso($id_curso)
 *   @desciption Obter o nome do curso que está relacionado com a 
 *               disciplina em manutenção (Curso da curso da guia  
 *               manutenção)
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param array $id_curso Identificação do curso 
 *       @return array $NomeCurso - Nome do curso 
 * -----------------------------------------------------------------------
 */
function ObterNomeCurso($id_curso)
{
  // Parametos
  // $id_curso - identificação do curso
  // retorno
  // $NomeCurso - Nome do curso
  	 
   global $cursos;

   $NomeCurso = "";
   for($x=0; $x < count($cursos) ; $x++) 
   {
    if ($cursos[$x]['ID_CURSO'] == $id_curso)
       {
       $NomeCurso = $cursos[$x]['NOME_CURSO'];
       break;
       }
   } // for	

  return $NomeCurso;
} // ObterNomeCurso


/** -----------------------------------------------------------------------
 *         @name AtualizaCursoDisciplina($id_disc)
 *   @desciption Atualizar a matriz de cursos relacionado a disciplina que
 *               estiver em manutenção. A  matriz  de cursos é criada toda
 *               vez que o autor acessa o estruturador de conteúdo 
 *               (atualizada de acordo com o banco de dados). Incialmente 
 *               todos os cursos são adicionados na matriz Curos com o 
 *               STATUS falso. Durante a atualização do tópicos o status é
 *               alterado de acordo com o que foi selecionado na matriz 
 *               conteúdo.  
 *       @since 01/06/2003
 *      @author Veronice de Freitas (veronice@jr.eti.br)
 *       @param array $id_curso Identificação do curso 
 *      @return array $NomeCurso - Nome do curso 
 * -----------------------------------------------------------------------
 */
function AtualizaCursoDisciplina($id_disc)
{	
  global $cursos, $A_DB_TYPE, $A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB;
  $cursos = array();

  //session_unregister("cursos");
  unset($_SESSION['cursos']);

  $conn = &ADONewConnection($A_DB_TYPE); 
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
   
  $sql = "SELECT curso.id_curso, nome_curso  FROM curso_disc, curso where curso.id_curso = curso_disc.id_curso and id_disc=".$id_disc.";";
  $rs = $conn->Execute($sql);
       					     			 
  while (!$rs->EOF)
  {					 	    
    	$Linha = array(   
   		       "ID_CURSO" => $rs->fields[0],
     		       "NOME_CURSO"=> $rs->fields[1],
     		       "STATUSCUR"=> "FALSE"
     		     );
    	             
       array_push($cursos,$Linha);
       $rs->MoveNext();
   } 
   
   //session_register("cursos");
   $_SESSION['cursos'] = $cursos;

   // insere novos cursos na matriz conteudo (exemplo: autor selecionou um novo curso para a displina)
   
   AtualizaCursoTopicos($id_disc, $cursos);   			                      
}      


/** -----------------------------------------------------------------------
 *         @name AtualizaCursoTopicos($id_disc, $cursos) 
 *   @desciption Insere / Atualiza/ Exclui os curos da matriz conteúdo -
 *               cursos relacionado ao conceito (toda estrutura de conteudo)
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param int   $id_disc - Identificação da disciplina,
 *               array $cursos  - Matriz de cursos 
 * -----------------------------------------------------------------------
 */
function AtualizaCursoTopicos($id_disc, $cursos)            
{      
   global $conteudo ;
	
   // cria uma matriz cursoTop contendo os cursos da matriz $Cursos	
   $cursoTop = $cursos;
	
   // adiciona ou remove cursos / disciplina   	   	
   for($i=0; $i < count($conteudo); $i++) 
   {  	               
	// atualiza a matriz $cursoTop com os cursos existentes na matriz conteúdo para para cada posição
	//  indicada por $i
      	for($x=0; $x < count($conteudo[$i]['CURSO']); $x++) 
      	{      	          	      	 	        
		
		// verifica quais cursos da matriz cursoTop existem na matriz conteúdo e atribui na matriz o status 
		// contido na matriz conteúdo (TRUE - curso selecionado)
		for($j=0; $j <  count($cursoTop); $j++)    
		{            		
		        if ($conteudo[$i]['CURSO'][$x]['ID_CURSO'] == $cursoTop[$j]['ID_CURSO']) 
		   	   {	               				
       				$cursoTop[$j]['STATUSCUR'] = $conteudo[$i]['CURSO'][$x]['STATUSCUR'];
                		break;
			   }
		} //for $j
       
       	} // for $x  		


         // atualiza os cursos contidos na matriz $cursoTop na matriz conteudo
         if (count ($conteudo[$i]['CURSO']) > 0) 
            {
            // se existir cursos para o conceito atual da matriz os cusos que estão na matriz serão substituidos pelos 
            // curos da matriz $cursoTop
            array_splice($conteudo[$i]['CURSO'],0,count($conteudo[$i]['CURSO']),$cursoTop);              		   				
            }
         else
             {
             	//Se não existir cursos para o conceito atual os cursos da matriz $cursoTop serão adicionados a matriz conteuso
		for($j=0; $j < count($cursoTop); $j++) 
	 	{
			$conteudo[$i]['CURSO'][$j]['ID_CURSO'] = $cursoTop[$j]['ID_CURSO'];
			$conteudo[$i]['CURSO'][$j]['NOME_CURSO'] = $cursoTop[$j]['NOME_CURSO']; 
			$conteudo[$i]['CURSO'][$j]['STATUSCUR'] = $cursoTop[$j]['STATUSCUR']; 				
		}	
            } 

         // inicializa todos os cursos com status false para atualizar os cursos do próximo tópico
	 for($x=0; $x < count($cursoTop); $x++) 
	 {
	     $cursoTop[$x]['STATUSCUR'] = "FALSE";	 
	 } 
	
   } // for $i
	 
   // atualiza a matriz de conteúdo	 
   GravaMatriz($id_disc); 
 }
 
 
/** -----------------------------------------------------------------------
 *         @name AtualizaCursoMatComp($id_disc, $cursos)    
 *   @desciption Insere / Atualiza/ Exclui cursos em material complementar 
 *               (toda estrutura de conteúdo)
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param int   $id_disc - Identificação da disciplina,
 *               array $cursos  - Matriz de cursos 
 * -----------------------------------------------------------------------
 */  
function AtualizaCursoMatComp($id_disc, $cursos)            
{               
     global $conteudo;	    
           	 
     // adiciona ou remove cursos em material complementar
     for($i=0; $i < count($conteudo) ; $i++) 
     {   	
     
        // Atribui para matriz cursosEmTop os cursos selecionados no conceito indicado por $i 
        $cursosEmTop = $conteudo[$i]['CURSO'];
   
        // Indica (TRUE - curso selecionado para o material complementar, LIBERAR - Curso será disponibilizado
        // e não foi selecionado pelo autor, NLIBERAR - Curso que não foi selecionado para o conceito 
        for($x=0; $x < count($cursosEmTop); $x++) 
        {
           if ($cursos[$x]['STATUSCUR'] == "TRUE")
              $cursosEmTop[$x]['STATUSCUR'] = "LIBERAR";	 
           else
              $cursosEmTop[$x]['STATUSCUR'] = "NLIBERAR";	 
        }
		
	// Se existir material complementar para o conceito indicado por $i
       	if (count($conteudo[$i]['MATCOMP']) > 0 )
       	   {	      		 	        		
            
             // Atualiza os cursos para para todos material complementar
             for($b=0; $b < count($conteudo[$i]['MATCOMP']); $b++) 	   	       
             {      	       	          	       	   	      		 	   
      	         // Cada material complementar possui uma matriz de cursos para ser atualizado
      	         for($x=0; $x < count($conteudo[$i]['MATCOMP'][$b]['CURSOMAT']); $x++) 	      	    
      	         {      	    
      	      	 	        
		    // Armazena em $cursosEmTop os cursos relacionado ao material complemetar do indice atual $i - $b
		    // Será verificado cada curso da matriz conteúdo referente ao material complementar se o mesmo foi selecionado 
		    // no momento da inserção e informa seu status na matriz $cursosEmTop
		    for($j=0; $j <  count($cursosEmTop); $j++)    
		    {            		
			if  ($conteudo[$i]['MATCOMP'][$b]['CURSOMAT'][$x]['ID_CURSOMAT'] == $cursos[$j]['ID_CURSO']) 
       			    {
               		      $cursosEmTop[$j]['STATUSCUR'] = $conteudo[$i][MATCOMP][$b][CURSOMAT][$x]['STATUSCURMAT'];	               				  
	                      break;
       			     }
     
		    } //for $j
       
                 } // for $x  	
              		
 	         
 	         // Define status de cada curso (TRUE - Curso selecionado, FALSE - Curso não foi selecionado no conceito em manutenção
 	         // LIBERAR - curso foi disponibilizado e nao foi selecionado pelo autor  
 	         for($x=0; $x < count($cursosEmTop); $x++) 
  	         {   	  	
  		     if ($cursosEmTop[$x]['STATUSCUR'] == "TRUE")
  		        {
   			     // se o curso já foi selecionado para o material complementar		       	
    			     $Linha = array(   
	                                    "ID_CURSOMAT" => $cursos[$x]['ID_CURSO'],
		                            "NOME_CURSOMAT"=>  $cursos[$x]['NOME_CURSO'],
	                                    "STATUSCURMAT"=> $cursosEmTop[$x]['STATUSCUR']
	                               );      	  		    
			     array_push($cursosMat,$Linha);       		                               
	                 }               
  		     elseif  ($cursosEmTop[$x]['STATUSCUR'] == "FALSE")
  		           {
  		                // se o curso já foi disponibilizado para o professor e o mesmo não foi selecionado no conceito em manutenção		       	
  		             	$Linha = array(   
	                                "ID_CURSOMAT" => $cursos[$x]['ID_CURSO'],
		                        "NOME_CURSOMAT"=>  $cursos[$x]['NOME_CURSO'],
	                                "STATUSCURMAT"=> $cursosEmTop[$x]['STATUSCUR']
	                              );      	  		    
			        array_push($cursosMat,$Linha);       		                              
	                   } // final if      
  		     elseif  ($cursosEmTop[$x]['STATUSCUR'] == "LIBERAR")
  		          {
  		                // se o curso for um curso selecionado que ainda não estava em material complementar		       	
  		             	$Linha = array(   
	                                "ID_CURSOMAT" => $cursos[$x]['ID_CURSO'],
		                        "NOME_CURSOMAT"=>  $cursos[$x]['NOME_CURSO'],
	                                "STATUSCURMAT"=> "FALSE"
	                              );      	  		    
			        array_push($cursosMat,$Linha);           	  		          	
  		          }
                 } // for $x
  	          
  	            
  	         // atualiza os cursos contidos na matriz $cursoTop na matriz conteudo / material complementar de acordo com os indices $i e $b
                 if (count($conteudo[$i]['MATCOMP'][$b]['CURSOMAT']) > 0)
                    array_splice($conteudo[$i]['MATCOMP'][$b]['CURSOMAT'],0,count($conteudo[$i]['MATCOMP'][$b]['CURSOMAT']),$cursosMat);                    		                                    	                          
                 else
                    {                     		              
			for($j=0; $j < count($cursosMat); $j++) 
	 		{		 			
				$conteudo[$i]['MATCOMP'][$b]['CURSOMAT']['ID_CURSOMAT'] = $cursosMat[$j]['ID_CURSOMAT'];
				$conteudo[$i]['MATCOMP'][$b]['CURSOMAT']['NOME_CURSOMAT'] = $cursosMat[$j]['NOME_CURSOMAT']; 
				$conteudo[$i]['MATCOMP'][$b]['CURSOMAT']['STATUSCURMAT'] = $cursosMat[$j]['STATUSCURMAT']; 				
			}                            	
                    }
                                          	
           } // for $b - matriz de material complementar
           
        } // if   
                                		   				
     } // for $i     	
                               
     GravaMatriz($id_disc);
}

/** -----------------------------------------------------------------------
 *         @name AtualizaCursoSubNivel($posicao, $numtopico)
 *   @desciption ESTRUTURA DE CONCEITO:  Excluir os cursos nos subconceitos 
 *               ao excluir o curso do conceito atual (a função somente é
 *               executada para o conceito atual que sofreu alteração dos
 *               cursos - Exemplo: o autor tirou da selecao um curso que
 *               estava selecionado.  
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param int    $posicao - Posição da matriz
 *       @return string $numtopico - Número do tópico
 * -----------------------------------------------------------------------
 */
function  AtualizaCursoSubNivel($posicao, $numtopico)
{
global $conteudo;
	
	// desabilita os cursos que não estão selecionados no conceito pai			
	for($b=$posicao; $b < count($conteudo) ; $b++) 	   	       
	{   		      		 	   
	// somente se o conceito for de nível 1 
	if (substr($conteudo[$b]['NUMTOP'],0,strlen($numtopico)) == $numtopico) 
	   {	
	   if (Pai($conteudo[$b]['NUMTOP']) <> '') 
	      {
	      	// Encontra o conceito pai e cria uma matriz dos cursos selecionados no conceito pai
	      	$PosTopPai = EncontraTopico(Pai($conteudo[$b]['NUMTOP']));      
	      	$CursosPai = $conteudo[$PosTopPai]['CURSO'];
	              	            	           	      
	      	for($j=0; $j <  count($conteudo[$b]['CURSO']); $j++)         	      	 
	      	{         	 
	    		// Desabilita nos subtopicos os cursos que não foram selecionados no conceito pai 
	    		for($x=0; $x <  count($CursosPai); $x++)    
	    		{       	      	         	      	      	            	      	
                           if  ($conteudo[$b]['CURSO'][$j]['ID_CURSO'] == $CursosPai[$x]['ID_CURSO'])       
                               {	                               	        
                                  if ($CursosPai[$x]['STATUSCUR'] == "FALSE")
                                      $conteudo[$b]['CURSO'][$j]['STATUSCUR'] = "FALSE";
                                      
	                       break;       
	                      }  // if  
		        } //for $x
			     		   	     		      	 	        		 	              
	      	}  // for $j
			  			              
	     } // if
	    }
	  else  
	    {
	     break;	
	    } 
	 } // form $b         
	           	                                            
} // AtualizaCursoSubNivel
 
 
/** -----------------------------------------------------------------------
 *         @name AtualizaCursoMatTopico($conteudo, $posicao, $tipo, 
 *                                      $CodigoDisciplina, $IDCursos,
 *                                      $StatusTop, $NumTopico)
 *   @desciption Atuliza os cursos relacionado ao conceito atual para as
 *               matrizes de exemplos, exercícios e material complementar   
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param array  $conteudo - Matriz conteudo
 *               int    $posicao - Posicao da matriz
 *               string $tipo - Tipo de material - Exemplo, Exercío, Material complementar
 *               int    $CodigoDisciplina - Código da Disciplina
 *               int    $IDCursos - Matriz de identificação dos cursos
 *               // $StatusTop - ################################################################
 * -----------------------------------------------------------------------
 */
function AtualizaCursoMatTopico($conteudo, $posicao, $tipo, $CodigoDisciplina, $IDCursos, $StatusTop)
{
global $conteudo;
     
if ($tipo=="ARQMAT_COMPLEMENTAR")
    {      
    	
	// se o curso não foi nao foi selecionado altera o status do cursos para FALSE	
	for($b=0; $b < count($conteudo[$posicao]['MATCOMP']) ; $b++) 	   	       
   	{   		      		 	   
	    for($x=0; $x < count($conteudo[$posicao]['MATCOMP'][$b]['CURSOMAT']); $x++) 	      	    
      	    {      	      	 	        
    		 for($j=0; $j <  count($IDCursos); $j++)    
     		 {            		
			if  ($conteudo[$posicao]['MATCOMP'][$b]['CURSOMAT'][$x]['ID_CURSOMAT'] == $IDCursos[$j]) 
               		    {
	               		 if ($StatusTop[$j] <> "on") 
	               		    {	               		        
	               		        $conteudo[$posicao]['MATCOMP'][$b]['CURSOMAT'][$x]['STATUSCURMAT'] = "FALSE";	               				  
        	                        break;
        	                    }
               	            }
             
		 } //for $j
             } // for $x  	
         } // form $b     
 
     }             
elseif ($tipo=="EXEMPLO")
    {         	
	// se o curso não foi nao foi selecionado altera o status do cursos para FALSE	
	for($b=0; $b < count($conteudo[$posicao]['EXEMPLO']) ; $b++) 	   	       
   	{   		      		 	   
	    for($x=0; $x < count($conteudo[$posicao]['EXEMPLO'][$b]['CURSOEXEMPLO']); $x++) 	      	    
      	    {      	      	 	        
    		 for($j=0; $j <  count($IDCursos); $j++)    
     		 {            		
			if  ($conteudo[$posicao]['EXEMPLO'][$b]['CURSOEXEMPLO'][$x]['ID_CURSOEXEMP'] == $IDCursos[$j]) 
               		    {
	               		 if ($StatusTop[$j] <> "on") 
	               		    {
	               		        $conteudo[$posicao]['EXEMPLO'][$b]['CURSOEXEMPLO'][$x]['STATUSCUREXEMP'] = "FALSE";	               				  
        	                        break;
        	                    }
               	            }
             
		 } //for $j
             } // for $x  	
         } // form $b            
     }                  
elseif ($tipo=="EXERCICIO")
    {         	
	// se o curso não foi nao foi selecionado altera o status do cursos para FALSE	
	for($b=0; $b < count($conteudo[$posicao]['EXERCICIO']) ; $b++) 	   	       
   	{   		      		 	   
	    for($x=0; $x < count($conteudo[$posicao]['EXERCICIO'][$b]['CURSOEXER']); $x++) 	      	    
      	    {      	      	 	        
    		 for($j=0; $j <  count($IDCursos); $j++)    
     		 {            		
			if  ($conteudo[$posicao]['EXERCICIO'][$b]['CURSOEXER'][$x]['ID_CURSOEXER'] == $IDCursos[$j]) 
               		    {
	               		 if ($StatusTop[$j] <> "on") 
	               		    {
	               		        //echo "ENTREI";
	               		        $conteudo[$posicao]['EXERCICIO'][$b]['CURSOEXER'][$x]['STATUSCUREXER'] = "FALSE";	               				  
        	                        break;
        	                    }
               	            }
              
		 } //for $j
             } // for $x  	
         } // form $b            
     }      
     
     GravaMatriz($CodigoDisciplina);	             
}

/** -----------------------------------------------------------------------
 *         @name LeMatriz($id_disc)
 *   @desciption Carrega para sessão a matriz de conteúdo armazenada no
 *                banco de dados 
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param int $id_disc  - Código da Disciplina
 * -----------------------------------------------------------------------
 */
 
function LeMatriz($id_disc)
{
  global $conteudo,$A_DB_TYPE, $A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB;
  $conteudo = array();

  //session_unregister("conteudo");
  //session_register("conteudo");
  unset($_SESSION['conteudo']);
  $_SESSION['conteudo'] = $conteudo;
 
  $conn = &ADONewConnection($A_DB_TYPE); 
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
  
  // Recupera matriz de conteúdo do banco de dados 
  $sql = "SELECT * FROM disciplina where id_disc=".$id_disc.";";
  $rs = $conn->Execute($sql);

  // Recupera a matriz na sessão 
  if ($rs->fields[2] <> "")
  	$conteudo = unserialize($rs->fields[2]);
  	
  if ($rs === false) die(A_LANG_ERROR_BD);  

  $rs->Close();
      
  // classifica a matriz de conteúdo
  $conteudo = classifica($conteudo);
  
  // Atualiza matriz - banco de dados
  GravaMatriz($id_disc);
}

/** -----------------------------------------------------------------------
 *         @name testaDeveTrocar($primeiro, $segundo)
 *   @desciption Testa se dois tópicos consecutivos devem ser trocados de ordem.
 *        @since 08/03/2004
 *       @author Samir Merode (sgzmerode@inf.ufrgs.br)
 *        @param string $primeiro, $segundo - os campos 'NUMTOP'  
 * -----------------------------------------------------------------------
 */
 function testaDeveTrocar($primeiro, $segundo)
   {
   $sprim = explode('.', $primeiro);
   $sseg  = explode('.', $segundo);
   $menor = (count($sprim)<count($sseg))?count($sprim):count($sseg);
   for($i=0; $i<$menor; $i++)
      {
	  //echo "$sseg[$i]".'<'."$sprim[$i]";
      if((int) $sseg[$i] < (int) $sprim[$i])
		  return TRUE;
      if((int) $sseg[$i] > (int) $sprim[$i])
         return FALSE;
      }
   return FALSE;
   }

/** -----------------------------------------------------------------------
 *         @name classifica($conteudo)
 *   @desciption Classifica a matriz conteúdo
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param array $conteudo - matriz de conteúdo
 *               Modificada por Samir Merode em 08/03/2004
 * -----------------------------------------------------------------------
 */
 function classifica($conteudo)
 {
 $tamanho = count($conteudo);
 do
   {
   $trocou = FALSE;
   for($i=0; $i<$tamanho-1; $i++)
      {
      if(testaDeveTrocar($conteudo[$i]['NUMTOP'], $conteudo[$i+1]['NUMTOP']))
         {
         $trocou = TRUE;
         $temp = $conteudo[$i];
         $conteudo[$i] = $conteudo[$i+1];
         $conteudo[$i+1] = $temp;
         }
      }
   }
 while($trocou);
 return $conteudo;
 }

/** -----------------------------------------------------------------------
 *         @name MontaArvoreTopicos($id_disc)
 *   @desciption Cria arvore da estrutura de conceito
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param int $id_disc - codigo da disciplina  
 *     @creditos Emil A. Eklund - http://webfx.eae.net/       #################################################
 * -----------------------------------------------------------------------
 */
function MontaArvoreTopicos($id_disc)
{
        global $conteudo;

	// Obtem o nome da disciplina
	$Disciplina = " ".ObterNomeDisciplina($id_disc);

	echo "<!-- js file containing the tree content, edit this file to alter the menu, -->\n";
	echo "<script>\n";

	echo "if (document.getElementById){\n";

	echo "var tree = new WebFXTree('<font size=4>$Disciplina</font>');\n";
	echo "tree.setBehavior('classic');\n";

	for($j=0; $j <= 5 ; $j++) // (2) Numero de níveis que tem a matriz (falta contar o numero de niveis) 	
	{
	    for($i=0; $i < count($conteudo) ; $i++) 
	    {    	
		
		// ---- nome do curso
		$CursosConceito = ' &harr; ';
	        for($b=0; $b < count($conteudo[$i]['CURSO']) ; $b++)   
	        {
	          if ($conteudo[$i]['CURSO'][$b]['STATUSCUR'] == 'TRUE')
	             $CursosConceito .= '('.$conteudo[$i]['CURSO'][$b]['NOME_CURSO'].') ';  	 
	        }
	        
	        if ($CursosConceito == ' &harr; ')
	           $CursosConceito = '';	        
	        // final nome do curso
	        
		$nivel = Nivel($conteudo[$i]['NUMTOP']);
		$pai = TiraPonto(Pai($conteudo[$i]['NUMTOP']));

		// adiciona na arvore todos os conceitos do mesmo nível
		if ($nivel == $j) {

			$cTexto = " ".$conteudo[$i]['NUMTOP']." - ".$conteudo[$i]['DESCTOP'].' '.$CursosConceito;
			$cAcao = "a_index.php?opcao=TopicosAltera&CodigoDisciplina=".$id_disc."&numtopico=".$conteudo[$i]['NUMTOP'];  
			$cVar = TiraPonto($conteudo[$i]['NUMTOP']);  

			echo "var a".$cVar." = new WebFXTreeItem(\"".$cTexto."\",\"".$cAcao."\");\n";      

			if ($pai == "") 
				echo "tree.add(a".$cVar.");\n"; 
			else 
				echo "a".$pai.".add(a".$cVar.");\n"; 
		}
   	    }
	}
  

   echo "document.write(tree);\n";
   echo "}\n";
   echo "</script>\n";	
   
}




/** -----------------------------------------------------------------------
 *         @name MontaArvoreTopicos($id_disc)
 *   @desciption Cria arvore da estrutura de conceito
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param int $id_disc - codigo da disciplina  
 *     @creditos Emil A. Eklund - http://webfx.eae.net/       #################################################
 * -----------------------------------------------------------------------
 */

function MontaArvoreTopicos_1($id_disc)
{
  

  global $conteudo;

  // Obtem o nome da disciplina
  $Disciplina = " ".ObterNomeDisciplina($id_disc);

  echo "<script>\n";
  echo "if (document.getElementById){\n";
  echo "var tree = new WebFXTree('$Disciplina');\n";
  echo "tree.setBehavior('classic');\n";

  for($j=0; $j <= 5 ; $j++) // (2) Numero de níveis que tem a matriz (falta contar o numero de niveis)  
  {
    for($i=0; $i < count($conteudo) ; $i++) 
    {     
    
        // ---- nome do curso
        $CursosConceito = ' &harr; ';
        for($b=0; $b < count($conteudo[$i]['CURSO']) ; $b++)   
        {
            if ($conteudo[$i]['CURSO'][$b]['STATUSCUR'] == 'TRUE')
            {
               $CursosConceito .= '('.$conteudo[$i]['CURSO'][$b]['NOME_CURSO'].') ';     
            }
        }    
          
        if ($CursosConceito == ' &harr; ')
        {
            $CursosConceito = '';
        }              
          // final nome do curso
          
        $nivel = Nivel($conteudo[$i]['NUMTOP']);
        $pai = TiraPonto(Pai($conteudo[$i]['NUMTOP']));
        // adiciona na arvore todos os conceitos do mesmo nível
        if ($nivel == $j) 
        {   


            $cTexto = " ".$conteudo[$i]['NUMTOP']." - ".$conteudo[$i]['DESCTOP'].' '.$CursosConceito;
            $cAcao = "a_index.php?opcao=TopicosEstruturarArvore&CodigoDisciplina=".$id_disc."&numtopico=".$conteudo[$i]['NUMTOP'];  
          

            $cVar = TiraPonto($conteudo[$i]['NUMTOP']);  
            echo "var a".$cVar." = new WebFXTreeItem(\"".$cTexto."\",\"".$cAcao."\");\n";      
            
            if ($pai == "")
            { 
                echo "tree.add(a".$cVar.");\n";

            }    
            else 
            {
                echo "a".$pai.".add(a".$cVar.");\n"; 
            }
        }
    }
  } 

    echo "document.write(tree);\n";
    echo "}\n";
    echo "</script>\n";  
    //echo $cFuncao;  
}




/** -----------------------------------------------------------------------
 *         @name Nivel($nvl)
 *   @desciption Nivel do conceito  
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param string $nvl -  Identificador do conceito
 *       @return int $qtnivel - Numero de niveis                ######################################################
 * -----------------------------------------------------------------------
 */
function Nivel($nvl)
{
	$qtnivel = 0;
	
	// obtem o numero de niveis
	for($j=0; $j < strlen($nvl) ; $j++)
	{
		if (substr($nvl,$j,1) == ".")
		    $qtnivel += 1;
	}

	return $qtnivel;
}

/** -----------------------------------------------------------------------
 *         @name TiraPonto($nvl)
 *   @desciption Remove os pontos do identificador do conceito 
 *               (exemplo: 3.1 - retorna 31) 
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param string $nvl -  Identificador do conceito 
 *       @return int $nivelsemponto - Nivel sem ponto
 * -----------------------------------------------------------------------
 */
function TiraPonto($nvl)
{
	$nivelsemponto = "";

	for($j=0; $j < strlen($nvl) ; $j++)
	{
		if (substr($nvl,$j,1) <> ".")
		    $nivelsemponto .= substr($nvl,$j,1);
	}

	return $nivelsemponto;
}

/** -----------------------------------------------------------------------
 *         @name Pai($nvl)
 *   @desciption obtem o identificador do conceito pai (exemplo: se o 
 *               parametro recebe 2.2.1 o pai é o 2.2)
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param string $nvl -  Identificador do conceito 
 *       @return string  Numero do conceito pai
 * -----------------------------------------------------------------------
 */
function Pai($nvl)
{
	$nivelsemponto = "";

	// obtem o identificador do conceito pai  
	for($j=strlen($nvl); $j > 0 ; $j--)
	{
		if (substr($nvl,$j,1) == ".")
		    break;
	}

	return substr($nvl,0,$j);
}


/** -----------------------------------------------------------------------
 *         @name MoveArvoreTopicos($NumTopSel, $NumTopApos)
 *   @desciption  
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param  
 *       @return  
 * -----------------------------------------------------------------------
 */
function MoveArvoreTopicos($NumTopSel, $NumTopApos)
{
        global $conteudo;
        
        echo $NumTopSel;
        echo $NumTopApos;
               
       // terminar a rotina 
        
}

/** -----------------------------------------------------------------------
 *         @name  EncontraTopico($numero)
 *   @desciption  Encontra posicão do conceito na matriz
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param string $numero - Numero do Conceito 
 *       @return int $j - posição da matriz 
 * -----------------------------------------------------------------------
 */
 
function EncontraTopico($numero)
{
   global $conteudo;
   
   for($j=0; $j <= count($conteudo)+1 ; $j++)
   { 	
     if (trim($conteudo[$j]['NUMTOP']) == trim($numero))
  	break;
   }   
   return $j;
}   

/** -----------------------------------------------------------------------
 *         @name  ExisteTopico($numero)
 *   @desciption  Verifica a existencia de conceito na matriz conteudo
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param string $numero - Numero do Conceito 
 *       @return booblean $existe - TRUE -> encontrou, FALSE - não encontrou 
 * -----------------------------------------------------------------------
 */
function ExisteTopico($numero)
{
   global $conteudo;
   $existe = false;
   for($j=0; $j <= count($conteudo) ; $j++)
   { 	
     if (trim($conteudo[$j]['NUMTOP']) == trim($numero))
     {
        $existe = true;
  	break;
     }	
   }   
   return $existe;
}

/** -----------------------------------------------------------------------
 *         @name  InserirTopicoMesmoNivel($numero)
 *   @desciption  Inserir conceito de mesmo nivel 
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param string $numero - Numero do conceito 
 *       @return string $novo - Numero do novo conceito
 * -----------------------------------------------------------------------
 */

function InserirTopicoMesmoNivel($numero)
{
   global $conteudo;
         
  $total = 0;
  $toppai = trim(pai($numero));
   $tamanho_STR = strlen($toppai);

   $TopMesmoNivel = array();

   for($j=0; $j <= count($conteudo) ; $j++) //15
   { 	   
    //  $igualpai = substr($conteudo[$j]['NUMTOP'],0,strlen($toppai));
      $igualpai = substr($conteudo[$j]['NUMTOP'],0,$tamanho_STR);
     
     if ( strlen(trim($conteudo[$j]['NUMTOP'])) == strlen(trim($numero)) )
       {
        
        // adiciona todos os topicos iguais ao pai na matriz $TopMesmoNivel 

        if ($toppai == $igualpai)
            {
                         //  echo "<br>";
             $total++;	  
            // echo "-->";	            
            $TopMesmoNivel[$total]= trim($conteudo[$j]['NUMTOP']); 

            }
        }    
   }     
         
   if ($total > 0);
      {
   
        // atribui o ultimo identificador do conceito cadastrado em $topico
         $topico =  $TopMesmoNivel[$total]; 
           
   	// separa numero do conceito para adicionar o novo tópico 
   	$cont =0;
   	for($j=0; $j <= strlen($topico) ; $j++)
   	{
      
   	   if (substr($topico,$j,1) != ".")
       	      $parte .= substr($topico,$j,1);
       
   	   if (substr(trim($topico),$j,1) == ".")
      	      {
         	$v[$cont] = $parte;
      	        $parte = ""; 
      	        $cont++;
      	      }  
         }

         $v[$cont] = $parte;             	 
	
	 // cria número do novo subtopico do mesmo nivel;
   
	 $v[$cont] =  $v[$cont] + 1;	 

         // concatena o vetor e retorna novo tópico do mesmo nível
         for($j=0; $j <= $cont ; $j++)
         {
   	   $novo .= $v[$j].".";
   	 }

         $novo = substr($novo,0,strlen($novo)-1);
         
  	 return $novo;       
         
     } // fim IF posicao > 0       
}   


function AcharUltimoTopico() //1
{
    global $conteudo;
    $total = 0;

    for($j=0; $j <= count($conteudo) ; $j++) //15
    {     
      $topico=($conteudo[$j]['NUMTOP']);
      if(strpos($topico, '.')===false)
      {
         $total++;
      }
    }
    return $total;  
 
}   

/** -----------------------------------------------------------------------
 *         @name  InserirSubtopico($numero)
 *   @desciption  Inserir subconceito  
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param string $numero - Numero do conceito (2.2)
 *       @return string $sub - Numero do subconceito (2.2.1)
 * -----------------------------------------------------------------------
 */
 function InserirSubtopico($numero)
{
        global $conteudo;           
           
        // Numero do conceito
        $topico =  $numero; 
        
   	// Cria matriz - separa identificador
   	$cont =0;
   	for($j=0; $j <= strlen($topico) ; $j++)
   	{
      
   	   // concatena identificador
   	   if (substr($topico,$j,1) != ".")
       	      $parte .= substr($topico,$j,1);
       
   	   // separa identificador
   	   if (substr(trim($topico),$j,1) == ".")
      	      {
         	$v[$cont] = $parte;
      	        $parte = ""; 
      	        $cont++;
      	      }  
         }

         $v[$cont] = $parte;      
	
	 // cria número o subtopico (sempre será 1 - os demais são criados em Inserir Tópico do Mesmo nível)
	 $cont++;
	 $v[$cont]=1;
	 
         // concatena o vetor e retorna novo tópico do mesmo nível
         for($j=0; $j <= $cont ; $j++)
         {
   	   $sub .= $v[$j].".";
   	 }

         $sub = substr($sub,0,strlen($sub)-1);


	 // se não existir chama a função para inserir tópico do mesmo nível
	 if (ExisteTopico($sub))
	     $sub = InserirTopicoMesmoNivel($sub);
         
  	 return $sub;       
}   

/** -----------------------------------------------------------------------
 *         @name  PreRequisito($posicao, $numtopico)
 *   @desciption  Pré-Requisito 
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param int    $posicao - Posição da matriz
 *       @return string $numtopicob - Número do conceito
 * -----------------------------------------------------------------------
 */
 function PreRequisito($posicao, $numtopico)
 {
	global $conteudo, $PREREQUISITO;	
	
	// ---------------- matriz de topicos - percurso  ----------------
	$Topicos[] = $numtopico;                    						                    			      
	for($j=strlen($numtopico); $j > 0 ; $j--)              			      				      
	{			              			              
	if (substr($numtopico,$j,1) == ".")
	   {
	   $Topicos[] = substr($numtopico,0,$j);			                 
	   } 
	}	                             
	           			                   			                    			                   			      
	// matriz temporaria de possiveis pre-requisitos até o conceito atual destacando os prerequisitos do percurso do conceito atual como automáticos 	
	$PREREQUISITO = array();
	
	$cont = 0;
	for($j=0; $j < count($conteudo); $j++)    	       
	{    				      	   				      	  						         				             				      	        		 	                 	   				      				       				             				            				                                                       
	   // cria uma matriz dos numeros dos coceitos que estão no percurso do conceito atual
	   $RemoverTopico = 'FALSE';     
	   for($b=0; $b < count($Topicos) ; $b++)            			      				      
	   {
	       if ($Topicos[$b] ==  $conteudo[$j]['NUMTOP'])				              		                                           
		  {
		    $RemoverTopico = 'TRUE';
	 	    break;
		  }
	   }
	   
   	   // irá interromper a matriz de pré-requisitos ao encontrar o conceito atual na matriz conteudo
	   if ($conteudo[$j]['NUMTOP'] == $numtopico)
	       {
	           break;
	       }                                                				        	        				        
	         				        	        				        
	   // ---- obtem os nomes dos cursos que foram selecionados para cada conceito
	   $CursosConceito = '--->';
	   for($b=0; $b < count($conteudo[$j]['CURSO']) ; $b++)   
	   {
	       if ($conteudo[$j]['CURSO'][$b]['STATUSCUR'] == 'TRUE')
	          $CursosConceito .= '('.substr($conteudo[$j]['CURSO'][$b]['NOME_CURSO'],0,3).') ';  	 
	   } // for $b
	        
	   if ($CursosConceito == '--->')
	       $CursosConceito = '';	        	       	       
	   // final curso  
	     	  
	   if ($RemoverTopico <> 'TRUE')  
	       {	   	 
	         // marcar como FALSE os demais prerequisitos     			      
	         $PREREQUISITO[$cont]['PREREQ']  = $conteudo[$j]['NUMTOP']; 	         	         
	         $PREREQUISITO[$cont]['DESCTOP'] = $conteudo[$j]['DESCTOP'].$CursosConceito; 	         
	         $PREREQUISITO[$cont]['STATUS']  = 'FALSE'; 
	         $PREREQUISITO[$cont]['POSICAO'] = $j;  	              
	         $cont++;	            	 	        	         
	        }
	   else
	       {	         
	       	 // irá marcar como AUTOMATICO os conceitos que estao no percurso
	         $PREREQUISITO[$cont]['PREREQ']  = $conteudo[$j]['NUMTOP']; 
	         $PREREQUISITO[$cont]['DESCTOP'] = $conteudo[$j]['DESCTOP'].$CursosConceito; 	         
	         $PREREQUISITO[$cont]['STATUS']  = 'AUTOMATICO'; 
	         $PREREQUISITO[$cont]['POSICAO'] = $j;  	              
	         $cont++;
	        }	   
	  	       	   	        
       } // for $j (matriz de conteudo)             			     	    		                  		               
                                  
       // Recupera a selecao do autor
       for($j=0; $j < count($PREREQUISITO); $j++) 	   	       
       {   	     		         	    	         
  	   for($b=0; $b < count($conteudo[$posicao]['PREREQUISITO']); $b++)     
  	   {  	        	      
  	      if  ($PREREQUISITO[$j]['PREREQ'] == $conteudo[$posicao]['PREREQUISITO'][$b]['PREREQ'])  	      
	          {	           	       			       			         	           	           
	            $PREREQUISITO[$j]['STATUS'] = 'TRUE';			       			         	           	           
	           break;
	          } // if  	      
  	   } // for $b  
       } // for $j	              
                   
      // seleciona os pré-requisitos dos pré-requisitos que estão selecionados em cada conceito que estiver selecionado no conceito atual      			          
      for($j=0; $j < count($PREREQUISITO); $j++) 
      {      	 
        if ($PREREQUISITO[$j]['STATUS'] <> 'FALSE')
          {                
             $posprereq =  $PREREQUISITO[$j]['POSICAO'];                                                                                         
             for($b=0; $b < count($conteudo[$posprereq]['PREREQUISITO']); $b++) 
             {
                 for($c=0; $c < count($PREREQUISITO); $c++) 
                 {
                    if  ($PREREQUISITO[$c]['PREREQ']== $conteudo[$posprereq]['PREREQUISITO'][$b]['PREREQ'])  	      
		        {	           
		         $PREREQUISITO[$c]['STATUS'] = 'AUTOMATICO';			       			         	           	           
		         break;
		        }  	   	          				     	
		        		        		        
		  } // c		          				                                        
		                           
             } // for $b
             
           } // if    
                                
       } // for $j            

           
     // Seleciona os pre-requisitos que estão no percurso de cada pre-prerequisito que estiver com o status marcado com true ou automatico
      for($j=0; $j < count($PREREQUISITO); $j++) 
      {      	 
        if ($PREREQUISITO[$j]['STATUS'] <> 'FALSE')
           {   	     
	     $NumTopAutomatico = array();   
	     $NumTopAutomatico = $PREREQUISITO[$j]['PREREQ'];
	     $TopicosPercurso[] = $NumTopAutomatico;                  						                    			      
	     for($b=strlen($NumTopAutomatico); $b > 0 ; $b--)              			      				      
	     {			              			              
	       if (substr($NumTopAutomatico,$b,1) == ".")
		  {
		   $TopicosPercurso[] = substr($NumTopAutomatico,0,$b);			                 
		  } 
	     } // for $b	     
           	           	                                     
             for($b=0; $b < count($TopicosPercurso); $b++) 
             {
                 for($c=0; $c < count($PREREQUISITO); $c++) 
                 {
                    if  ($PREREQUISITO[$c]['PREREQ']== $TopicosPercurso[$b] && $PREREQUISITO[$c]['STATUS'] <> 'TRUE' )  	      
		        {	           
		         $PREREQUISITO[$c]['STATUS'] = 'AUTOMATICO';			       			         	           	           
		         break;
		        }  	   	          				     			        		        		        
		  } // c		          				                                        
		                           
             } // for $b
           } // if    
                      
       } // for $j            
                    
       //session_unregister("PREREQUISITO");
       //session_register("PREREQUISITO");
       unset($_SESSION['PREREQUISITO']);  
       $_SESSION['PREREQUISITO'] = $PREREQUISITO;     
 }	

/** -----------------------------------------------------------------------
 *         @name  ObterCursoPai($numtopico)
 *   @desciption  Obter quais são os cursos selecionados no tópico pai
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param string $numtopico - Número do tópico pai
 *       @return array  $cursosPai - Matriz de cursos selecionados no pai
 * -----------------------------------------------------------------------
 */
 function ObterCursoPai($numtopico)
 {
  global $conteudo;
  
  $cursosPai = array();
  
  // Identifica o conceito pai
  $PaiTopico = pai($numtopico);
  
  // Encontra o conceito pai
  $topPai=EncontraTopico($PaiTopico );    
    
  // cursos selecionado no conceito pai
  for($b=0; $b < count($conteudo[$topPai]['CURSO']); $b++) 
  {
      if ($conteudo[$topPai]['CURSO'][$b]['STATUSCUR'] == "TRUE")
         {
        	$Linha = array(   
   		       "ID_CURSO" =>  $conteudo[$topPai]['CURSO'][$b]['ID_CURSO'],
     		       "NOME_CURSO"=> $conteudo[$topPai]['CURSO'][$b]['NOME_CURSO'],
     		       "STATUSCUR"=>  $conteudo[$topPai]['CURSO'][$b]['STATUSCUR']
     		     );
    	             
                array_push($cursosPai,$Linha);  	    	
       } 
  }
    
  return $cursosPai;	 
  
 } 	


/** -----------------------------------------------------------------------
 *         @name Contexto($opcao)
 *   @desciption Gera o contexto para navegação
 *        @since 01/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *        @param string $op - item selecionado
 * -----------------------------------------------------------------------
 */
function Contexto($op, $nivel)
{
global $ctx;

$ctx_tmp = $ctx;
session_unregister("ctx");	
session_register("ctx");
$ctx = $ctx_tmp;
$nvl = 0;
$ctx_tmp = "";

if ($nivel != -1)
{
	for($i=0; $i <= strlen($ctx); $i++)		
	{ 	  
		$caracter = substr($ctx,$i,1);
		
		if ( $caracter == "-")
		{	
	     		$nvl++; 
		}
	
		if ( $nvl == $nivel )
		{	
	     		break;
		}	
	        $ctx_tmp = $ctx_tmp.$caracter;		
	}
	
	$ctx = $ctx_tmp;
}

if ($nivel == 0 )
{
	
	$ctx = "";
}
else
{
	$ctx = $ctx."&nbsp;<img src='imagens/seta_caminho.png' border='0' class='imagem1'>&nbsp;";
}

$ctx = $ctx.$op;

}

/** -----------------------------------------------------------------------
 *         @name contaQuantosFilhos(&$conteudo, $conceito)
 *   @desciption Contar quantos subconceitos tem um conceito
 *        @since 08/03/2004
 *       @author Samir Merode (sgzmerode@inf.ufrgs.br)
 *        @param array $conteudo, string $conceito
 * -----------------------------------------------------------------------
 */
	function contaQuantosFilhos(&$conteudo, $conceito)
	{
		for($pos=0; $conteudo[$pos]['NUMTOP']<>$conceito; $pos++);
		//pos indica em que posição está o conceito a remover
		$total = count($conteudo);
		$sconceito = explode('.',$conceito);
		$tamanho = count($sconceito);
		$filhos = 0;
		for($i=$pos+1; $i<$total; $i++)
		{
			$atual = explode('.',$conteudo[$i]['NUMTOP']);
			if (count($atual)>$tamanho)
			{
				for($j=0; $j<$tamanho && ($teste = $sconceito[$j]==$atual[$j]); $j++);
				if($teste)
				{
					$filhos++;
				}
			}
			else break;
		}
		return $filhos;
	}//Fim da função contaQuantosFilhos

/** -----------------------------------------------------------------------
 *         @name excluiConteudo(&$conteudo, $conceito, $atualiza='0')
 *   @desciption Exclui $conceito de $conteudo e ajusta os pre-requisitos
 *        @since 08/03/2004
 *       @author Samir Merode (sgzmerode@inf.ufrgs.br)
 *        @param array $conteudo, string $conceito, int $id_usuario, int $CodigoDisciplina, string $atualiza
 * -----------------------------------------------------------------------
 */
    function excluiConteudo(&$conteudo, $conceito, $id_usuario, $CodigoDisciplina, $atualiza='0')
	{
		for($pos=0; $conteudo[$pos]['NUMTOP']<>$conceito; $pos++);
		// $pos é a posição do conceito na matriz

		if($id_usuario){//Samir Colocou 30-11-2004
		//alterado por cassio 29-10-2004:
		$htmls = array($conteudo[$pos][ARQPRINCIPAL][ARQTOP]);
		$figuras = array();

		//Samir Mexeu v
		if($nome_do_arquivo = $conteudo[$pos][ARQPRINCIPAL][ARQTOP])
		{
		$nomedoarquivo = explode(".", $nome_do_arquivo);
		$nomedoxml = $nomedoarquivo[0].".xml";
		unlink("disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$nomedoxml);
		}

		for ($qfigprinc=0; $conteudo[$pos][ARQPRINCIPAL][ARQPRINCASSOC][$qfigprinc]; $qfigprinc++)
			array_push($figuras, $conteudo[$pos][ARQPRINCIPAL][ARQPRINCASSOC][$qfigprinc][ARQTOPASSOC]);
		for ($qhtmlep=0; $conteudo[$pos][EXEMPLO][$qhtmlep]; $qhtmlep++)
		{
			array_push($htmls, $conteudo[$pos][EXEMPLO][$qhtmlep][ARQEXEMP]);
			for ($qfigep=0; $conteudo[$pos][EXEMPLO][$qhtmlep][EXEMPLOASSOC][$qfigep]; $qfigep++)
				array_push($figuras, $conteudo[$pos][EXEMPLO][$qhtmlep][EXEMPLOASSOC][$qfigep][ARQEXEMPASSOC]);
		}
		for ($qhtmlex=0; $conteudo[$pos][EXERCICIO][$qhtmlex]; $qhtmlex++)
		{
			array_push($htmls, $conteudo[$pos][EXERCICIO][$qhtmlex][ARQEXER]);
			for ($qfigex=0; $conteudo[$pos][EXERCICIO][$qhtmlex][EXERCICIOASSOC][$qfigex]; $qfigex++)
				array_push($figuras, $conteudo[$pos][EXERCICIO][$qhtmlex][EXERCICIOASSOC][$qfigex][ARQEXERASSOC]);
		}
		for ($qhtmlmc=0; $conteudo[$pos][MATCOMP][$qhtmlmc]; $qhtmlmc++)
		{
			array_push($htmls, $conteudo[$pos][MATCOMP][$qhtmlmc][ARQMATCOMP]);
			for ($qfigmc=0; $conteudo[$pos][MATCOMP][$qhtmlmc][MATCOMPASSOC][$qfigmc]; $qfigmc++)
				array_push($figuras, $conteudo[$pos][MATCOMP][$qhtmlmc][MATCOMPASSOC][$qfigmc][ARQASSOCMAT]);
		}

		$un_htmls=array();
		$un_fig=array();
		$un_htmls=RemoverDuplicados($htmls);
		$un_fig=RemoverDuplicados($figuras);
		
		for ($conta=0; $conta<count($un_htmls); $conta++)
			unlink("disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$un_htmls[$conta]);
		for ($conta=0; $conta<count($un_fig); $conta++)
			unlink("disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$un_fig[$conta]);
		//fim de alterado por cassio 29-10-2004
		}

		$total = count($conteudo);
		//echo "<h3>São $total posições em ".'$conteudo'." e vamos apagar o conceito $conceito que está na posição $pos ";
		
		for($i=0; $i<$total; $i++) //Apaga os pré-requisitos dos outros
		{
			if(isset($conteudo[$i]['PREREQUISITO']))
				unset($conteudo[$i]['PREREQUISITO']);
		}

		$aexcluir = contaQuantosFilhos($conteudo, $conceito)+1;
		//echo "e ocupa $aexcluir posições na matriz junto com os filhos.";
		
		for($i=$pos; $i<$total-$aexcluir; $i++) //Move as conceitos
			$conteudo[$i] = $conteudo[$i+$aexcluir];

		for($i=$total-$aexcluir; $i<$total; $i++) //Apaga da matriz
			unset($conteudo[$i]);

		$total = $total-$aexcluir;
		$tamanho = count(explode('.',$conceito));
		for($i=$pos; $i<$total && count(explode('.', $conteudo[$i]['NUMTOP']))>=$tamanho; $i++) //Ajusta os números dos tópicos
		{
		   $atual = explode('.', $conteudo[$i]['NUMTOP']);
		   $atual[$tamanho-1] = $atual[$tamanho-1]-1;
           if ($conteudo[$i]['NUMTOP'] == $atualiza)
				 $atualiza = implode('.', $atual);
		   $conteudo[$i]['NUMTOP'] = implode('.', $atual);
		   
		}
	   return $atualiza; //Serve para atualizar o novo número de um conceito

	} //Fim da excluiConteudo

/** -----------------------------------------------------------------------
 *         @name moveConteudo(&$conteudo, $conceito, $destino)
 *   @desciption Move $conceito em $conteudo para depois de $destino
 *        @since 08/03/2004
 *       @author Samir Merode (sgzmerode@inf.ufrgs.br)
 *        @param array $conteudo, string $conceito, $destino
 * -----------------------------------------------------------------------
 */
    function moveConteudo(&$conteudo, $origem, $destino)
	{
		//Testa se pode mover
		if($origem == $destino)
			return;
		$primeiro = explode(".", $origem);
		$segundo = explode(".", $destino);
		$menor = (count($primeiro)<count($segundo))?count($primeiro):count($segundo);
		$teste = TRUE;
		if($menor <> count($segundo))
		{
			for($i=0; $i<$menor; $i++)
				$teste = $teste && ($primeiro[$i] == $segundo[$i]);
			$teste = !$teste;
		}

		if(!$teste)
			return;
		//Se não puder, retorna.
		
		// Coloca a posição da origem em $src
		for($src=0; $conteudo[$src]['NUMTOP']<>$origem; $src++);

		//Faz uma cópia do que vai ser removido
		$aexcluir = contaQuantosFilhos($conteudo, $origem)+1;
		for($i=$src, $j=0; $j<$aexcluir; $j++, $i++) //Copia o a ser movido
		{
			$temp[$j] = $conteudo[$i];
			if (isset($temp[$j]['PREREQUISITO']))
				unset ($temp[$j]['PREREQUISITO']);
		}
		
		//exclui o que vai ser movido
		$destino = excluiConteudo($conteudo, $origem, 0, 0, $destino);

		$total = count($conteudo);
		//Coloca o local a ser inserido em $dst
		for($dst=0; $conteudo[$dst]['NUMTOP']<>$destino && $dst<$total; $dst++);
		$amover = contaQuantosFilhos($conteudo, $destino);
		$dst = $dst + $amover + 1;

		//Ajusta os números dos tópicos abaixo do que vai ser incluído
		$sdestino = explode(".", $destino);
		$tamanho = count($sdestino);
		for($i=$dst; $i<$total && count($atual = explode(".",$conteudo[$i]['NUMTOP']))>=$tamanho; $i++) 
		{
			
            $atual[$tamanho-1] = $atual[$tamanho-1]+1;
			$conteudo[$i]['NUMTOP'] = implode(".", $atual);
		}

		$tantes = count(explode(".",$temp[0]['NUMTOP']));
		$sdestino[$tamanho-1] = $sdestino[$tamanho-1] + 1;		$temp[0]['NUMTOP'] = implode(".", $sdestino);
		$totaltemp = count($temp);
		for($i=1; $i<$totaltemp; $i++)
		{
			$novo = $sdestino;
			$aux = explode(".", $temp[$i]['NUMTOP']);
			for($j=$tantes, $k=count($sdestino); $j<count($aux); $j++, $k++)
			{
				$novo[$k] = $aux[$j];
			}
			$temp[$i]['NUMTOP'] = implode(".", $novo);
		}
		
		for($i=0, $j=count($conteudo); $i<$totaltemp; $i++, $j++)
			$conteudo[$j] = $temp[$i];
		
		$conteudo = classifica($conteudo);

	} //Fim da moveConteudo

/** -----------------------------------------------------------------------
 *         @name trocalang($newlang)
 *   @desciption Troca a linguagem padrão do usuário
 *        @since 01/04/2004
 *       @author Cassio Cons (crcons@inf.ufrgs.br)
 *        @param string $newlang
 * -----------------------------------------------------------------------
 */	

function trocalang($newlang, $id_usuario)
{
	global $logado, $A_DB_TYPE, $A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB;
	
	$A_LANG_IDIOMA_USER=$newlang;
	session_unregister('A_LANG_IDIOMA_USER');
	session_register('A_LANG_IDIOMA_USER');
	if ($logado)
	{
		$conn = &ADONewConnection($A_DB_TYPE); 
		$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
		$sql = "UPDATE usuario 
  		SET idioma = '".$newlang."'
  		WHERE id_usuario=".$id_usuario." ";
		$rs = $conn->Execute($sql);
	}
	return ($A_LANG_IDIOMA_USER);
} //Fim da trocalang

/** -----------------------------------------------------------------------
 *         @name deldir($dir)
 *   @desciption Deleta um diretório com seus arquivos e subdiretórios
 *        @since 18/11/2004
 *       @author Cassio Cons (crcons@inf.ufrgs.br)
 *        @param string $dir
 * -----------------------------------------------------------------------
 */	
 
function deldir($dir) {
	$dh=opendir($dir);
	while ($file=readdir($dh)) {
		if($file!="." && $file!="..") {
   			$fullpath=$dir."/".$file;
   			if(!is_dir($fullpath)) {
       			unlink($fullpath);
   			} else {
       			deldir($fullpath);
   			}
			}
		}

		closedir($dh);

	if(rmdir($dir)) {
			return true;
		} else {
			return false;
		}
}
	
// ------------ desenvolver 
//Falta criar funcoes para exemplo e exercício --> (function AtualizaCursoMatComp($id_disc, $cursos)            


 /**  ------------------------------------------------------------------------------------------ -------
 *   Nome: Carla Cristina Lui Dias.
 *   Orientadora: Isabela Gasparini.
 *     Data: Maio/2008.        
 *   Alterações realizadas no período de Estágio.
 *     Este arquivo foi alterado para incluir uma função para verificar se o e-mail  digitado é válido em p_solicita_acesso.
 *  ------------------------------------------------------------------------------------------ ----------
 */

//--------------------------------- Funçao valida Email  ------------------------------------------------//
function verificar_email($email)
{
   $mail_correcto = 0;
   //verifico umas coisas
   if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) !=  "@") && (substr($email,strlen($email)-1,1) != "@"))
   {
      if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) &&  (!strstr($email,"\$")) && (!strstr($email," ")))
      {
         //vejo se tem caracter .
         if (substr_count($email,".")>= 1)
         {
            //obtenho a terminação do dominio
            $term_dom = substr(strrchr ($email, '.'),1);
            //verifico que a terminação do dominio seja correcta
            if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) )
            {
              //verifico que o de antes do dominio seja correcto
              $antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1);
              $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1);
              if ($caracter_ult != "@" && $caracter_ult != ".")
              {
                 $mail_correcto = 1;
              }
            }
         }
      }
   }

   if ($mail_correcto)
     return 1;
   else
     {
      return 0;
     /*    echo "<table border=0  width=600>";
          echo "<tr>";
          echo "<td>";
            echo "<IMG src=\"imagens/erro.gif\">";
          echo "</td>";
          echo "<td width=800>";
            echo "<p class=\"texto3\">\n";
            echo A_LANG_REQUEST_ACCESS_MSG11;
          echo "</td>";
          echo "</tr>";
        echo "</table>";
       */ 
    }
}



/**************************************************** AGENDA *****************************************************/
 /* Agenda (Autora: Carina Tissa Aihara)
  * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
  * Como Estagio Curricular.
  * Orientadora: Avanilde Kemczinski
  * Supervisora: Isabela Gasparini
  */

function MenuAgenda($opcao)
{
    global $logado, $id_usuario, $tipo_usuario;

	MenuEntrada($opcao);

	echo "<p>\n";
	echo "<TABLE cellSpacing=\"0\" cellPadding=\"0\" width=\"100%\" border=\"0\">\n";

	// ----------------> Linha - separar itens de menu
	echo "<tr class=mnuGroup>\n";
	echo "<td colspan=2>\n";
	echo "<b>".A_LANG_MNU_AGENDA."</b>";
	echo "</td>\n";
	echo "</tr>\n";

	// ---------------->
	echo "<tr class=mnuItem>\n";
	echo "<td valign=\"top\">\n";
	if ($opcao == "Agendamento")
	{
	    echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"n_index_agenda.php?opcao=Evento\">".A_LANG_AGENDA_AGENDAMENTO."</a>\n";
	}
	else
	{
        echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"n_index_agenda.php?opcao=Evento\">".A_LANG_AGENDA_AGENDAMENTO."</a>\n";
	}
	echo "</td>\n";
	echo "</tr>\n";

	// ---------------->
	echo "<tr class=mnuItem>\n";
	echo "<td valign=\"top\">\n";
	if ($opcao == "PesquisaPeriodo")
	{
        echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"n_index_agenda.php?opcao=PesquisaPeriodo\">".A_LANG_AGENDA_PESQUISA_PERIODO."</a>\n";
	}
	else
	{
        echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"n_index_agenda.php?opcao=PesquisaPeriodo\">".A_LANG_AGENDA_PESQUISA_PERIODO."</a>\n";
	}
	echo "</td>\n";
	echo "</tr>\n";


	// ---------------->
	echo "<tr class=mnuItem>\n";
	echo "<td valign=\"top\">\n";
	if ($opcao == "GerenciamentoEventos")
	{
        echo "<img src=\"imagens/xtree/openfoldericon.png\"></td><td><a href=\"n_index_agenda.php?opcao=GerenciamentoEventos\">".A_LANG_AGENDA_GERENCIAMENTO_EVENTO."</a>\n";
	}
	else
	{
        echo "<img src=\"imagens/xtree/foldericon.png\"></td><td><a href=\"n_index_agenda.php?opcao=GerenciamentoEventos\">".A_LANG_AGENDA_GERENCIAMENTO_EVENTO."</a>\n";
	}
	echo "</td>\n";
	echo "</tr>\n";

	echo "</table>\n";
	echo "<p>\n";

} // Menu da agenda


function removerAcento ($string){ 
    $a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ •ó.'; 
    $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr_Ao-'; 
    $string = utf8_decode($string);     
    $string = strtr($string, utf8_decode($a), $b); 
    $string = strtolower($string); 
    return utf8_decode($string); 
    //return ($string); 
} 

function enviaEmail2Admin ($msg){


    require('include/phpmailer/language/phpmailer.lang-en.php');
    require('include/phpmailer/class.phpmailer.php');
    $texto = "Atenção!!! Este é um e-mail automático. Por favor, não responda!<br><br>";
    if ($msg == 1)
    {  
        $texto = utf8_decode("Existe uma nova solicitação de acesso para usuário Professor no ambiente AdaptWeb<br><br>");
        $subject = utf8_decode("Solicitação de acesso");
    }

    $texto .= "Clique no link para logar no sistema <br> http://ead.joinville.udesc.br/adaptweb/index.php?opcao=Login";
    require('adaptweb-admin/inc-mail.php');
    $mail->AddBCC("mhk.cadastro@gmail.com","Marcos Kimura"); 
    $mail->AddAddress("adaptweb@joinville.udesc.br");  
    $mail->WordWrap = 50;                           
    $mail->IsHTML(true);                            
    $mail->Subject  =  "ADAPTWEB - ".$subject;
    $mail->Body     =  $texto;
    $mail->AltBody  =  "This is the text-only body";
    if(!$mail->Send())   // mensagem se der problema
    {
      $email_erro = $mail->ErrorInfo;
      $email_erro = substr($email_erro, 49);
      $ok = 10;
      echo $email_erro;
    }



}

?>
