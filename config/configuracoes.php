<?
	ini_set('session.cache_expire', 1440); // 1440 minutos = 1 dia
	ini_set('session.cache_limiter', 'none');
	ini_set('session.cookie_lifetime', 0); // O indica que morre quando o browser fecha
	ini_set('session.gc_maxlifetime', 86400); // 86400 segundos = 1 dia

  	// ==== Secoes ====
  	//session_name("Topicos");

  //	session_register("cursosmat");     //teste cursos material
  //	session_register("cursosflag");     //teste cursos material - flag

	// ==== variaveis globais ====
  	//global $conteudo, $cursos, $logado, $id_usuario, $email_usuario, $cursosmat, $cursosflag;;
        global $conteudo, $cursos, $cursosmat, $cursosflag, $mensagem;

	// === General ===
  $A_GENERAL_VERSION = "SERVIDOR ADAPT II - 1.0.0 alpha omega pi";
	$A_DATABASE_VERSION = "0.7.06";

	// ==== Idioma ====
   	$A_LANG_IDIOMA = "pt-BR";
	// $A_LANG_IDIOMA = "en-US";
	// $A_LANG_IDIOMA = "es-ES";
	// $A_LANG_IDIOMA = "fr-FR";
	// $A_LANG_IDIOMA = "ja-JP";

  	// ==== cores ====
    $A_COR_FUNDO_MENU = "#F2F5F9";
    $A_COR_FUNDO_TITULO = "#ffffff";
    $A_COR_FUNDO_BARRA = "#3399FF";
    $A_COR_FUNDO_BARRA2 = "#9999CC";
    $A_COR_FUNDO_BARRA3 = "#666699";
    $A_COR_FUNDO_MENSAGEM = "#0000FF";
    $A_COR_FUNDO_ORELHA_ON = "#ffffff";
    $A_COR_FUNDO_ORELHA_OFF = "#BCD2EE";
    $A_COR_FUNDO_TABELA_ARQ = "#D6E7EF";
    $A_COR_FUNDO_MNU_GRUPO = "#FF0000";
    $A_COR_FUNDO_MNU_ITEM = "#00FF00";
    $A_COR_FUNDO_TABELA = "#e7e6ff";

  	// ==== banco de dados ====
  	$A_DB_TYPE = "mysql";
  	$A_DB_HOST = "localhost";
  	$A_DB_USER = "root";
  	$A_DB_PASS = "lfc123";
  	$A_DB_DB = "adaptweb_beta";

  	// ==== erros =====
  	//$DEBUG = true;        // ativa impress�o da matriz conte�do
  	$DEBUG = false;       // desativa

  	 $DEBUG2 = false;      // ativa impress�o da matriz pre-requisito
  	 //$DEBUG2 = true;      // desativa

  	// =======
  	//$DOCUMENT_ROOT ="c:\AdaptWeb";
  	//$DOCUMENT_ROOT ="D:\Dados\Veronice\www\adaptweb\projeto\html";
	 $DOCUMENT_ROOT ="/var/www/adaptweb_beta";
       //$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT']."/adaptweb";

	$DOCUMENT_SITE ="http://localhost/adaptweb_beta";   //sem a barra no final

    //HABILITA WEB ANALYTICS TRACKING CODE (TRUE para Ligado e FALSE para Desligado)
	define('WA_PATH', '/var/www/adaptweb_beta/');
    $WEB_ANALYTICS = "TRUE";

    //C�digo de rastreamento Page Tagging (PIWIK)
    $PIWIK_URL = "localhost/adaptweb_beta/analytics/piwik/";
    $TRACKING_CODE =
    	"<!-- Piwik -->
		<script type='text/javascript'>
		  var _paq = _paq || [];
		  _paq.push(['setCustomVariable', '1','id_usuario','{$_SESSION['id_aluno']}']);
		  _paq.push(['setCustomVariable', '2','id_curso','{$_SESSION['num_curso']}']);
		  _paq.push(['setCustomVariable', '3','id_disc','{$_SESSION['num_disc']}']);
		  _paq.push(['setCustomVariable', '4','id_prof','{$_SESSION['id_prof']}']);
		  _paq.push(['trackPageView']);
		  _paq.push(['enableLinkTracking']);
		  (function() {
		    var u=(('https:' == document.location.protocol) ? 'https' : 'http') + '://{$PIWIK_URL}';
		    _paq.push(['setTrackerUrl', u+'piwik.php']);
		    _paq.push(['setSiteId', 1]);
		    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
		    g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
		  })();

		</script>
		<!-- End Piwik Code -->";

	require_once $DOCUMENT_ROOT.'/include/Mobile_Detect.php';
	$detect = new Mobile_Detect;
	if(($detect->isMobile()) || ($detect->isTablet())){
    	header("Location: ./m");
    	exit;
}

?>
