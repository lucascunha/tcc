<?php

 /*
 * Pagina para inser��o de recados no Mural no Banco de Dados
 * @author Laisi Corsani <laisicorsani@gmail.com>
 * @version 1.0 <25/07/2008>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Estagio Curricular.
 * Orientadora: Isabela Gasparini
 * Suporvisora: Avanilde Kemczinski
 *
 */ 

global $id_aluno,$tipo,$logado,$num_disc,$num_curso,$id_prof,$disciplina, $curso;

$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;

include_once "include/conecta.php";
require('include/phpmailer/language/phpmailer.lang-en.php');
require('include/phpmailer/class.phpmailer.php');

if(isset($enviarecado)){
$dest = $_POST['dest'];
$mail = $_POST['email'];
$msg = $_POST['mensagem'];
$rem = $id_aluno;
$data = date(Y)."-".date(m)."-".date(d);
$hora = date('H:i:s');

if($mail==null){
$mail=0;
}else{
$mail=1;
}

$max = "SELECT MAX(id_recado) as cod FROM mural";
$answer = mysql_query($max,$id);
$num = mysql_fetch_row($answer);
if($num[0] == null){
$cod = 1;
}else{
	for($i=1; $i<=$num[0]; $i++){
		$look = "SELECT * FROM mural WHERE id_recado='$i'";
		$answer = mysql_query($look,$id);
		$conf = mysql_fetch_row($answer);
		if($conf[0]==null){
			$cod=$i;
			$i = $num[0]+1;
		}else{
			if($conf[0]==$i){
				$cod = $i+1;
			}
		}	
	}
}

if(($msg==" ") && ($dest==null)){
$ok=1;
}else if($dest==null){
$ok=2;
}else if($msg==" "){
$ok=3;
}else if($dest == 'professor'){
	$tipo_rec = 'pessoal';
	$status =1;
	$dest = $id_prof;
	$sql1 = "INSERT INTO mural VALUES ('$cod','$mail','$msg','$rem','$tipo_rec','$status')";
	$sql2 = "INSERT INTO destinatario VALUES ('$cod','$dest','$num_curso','$num_disc','$data','$hora')";
	$res1 = mysql_query($sql1,$id);
	$res2 = mysql_query($sql2,$id);
	$destinatario[0] = $dest;
	if($res1 && res2){
		$ok=4;
	}else{
		$ok=5;
	}
}else if($dest == 'grupo'){
	$tipo_rec = 'geral';
	$status =0;
	$sql1 = "INSERT INTO mural VALUES ('$cod','$mail','$msg','$rem','$tipo_rec','$status')";
	$res1 = mysql_query($sql1,$id);
	$grupo = "SELECT id_usuario FROM matricula WHERE id_disc='$num_disc' AND id_curso='$num_curso' AND status_mat='1'";
	$resposta = mysql_query($grupo,$id);
	for($i=0; $i<mysql_num_rows($resposta); $i++){
		$pessoa = mysql_fetch_row($resposta);
		$dest = $pessoa[0];
		if($dest==$id_prof){$ja=1;}
		$sql2 = "INSERT INTO destinatario VALUES ('$cod','$dest','$num_curso','$num_disc','$data','$hora')";
		$res2 = mysql_query($sql2,$id);
		$destinatario[$i] = $dest;
	}
	if($ja!=1){
		$sql2 = "INSERT INTO destinatario VALUES ('$cod','$id_prof','$num_curso','$num_disc','$data','$hora')";
		$res2 = mysql_query($sql2,$id);
	}
	if($res1 && res2){
		$ok=4;
	}else{
		$ok=6;
	}
}


 if($res1 && res2){
    if($mail==1){

        // **************** Monta a mensagem **************************************************************************************************** //
        $disc = "SELECT nome_disc FROM disciplina WHERE id_disc='$num_disc'";
        $resposta_mail = mysql_query($disc,$id);
        $disciplina_mail = mysql_fetch_row($resposta_mail);

        $c = "SELECT nome_curso FROM curso WHERE id_curso='$num_curso'";
        $resp_mail = mysql_query($c,$id);
        $curso_mail = mysql_fetch_row($resp_mail);

        $texto = "Aten��o!!! Este � um e-mail autom�tico. Por favor, n�o responda!<br><br>";
        $texto .= "<b>Existe um novo recado no Mural de Recados da disciplina ". $disciplina_mail[0]." do curso ". $curso_mail[0].".</b><br><br>";
        $texto .= "Clique no link para acessar o Adaptweb - http://ead.joinville.udesc.br/adaptweb/";
        // **************** Fim monta mensagem ************************************************************************************************** //

        $mail = new PHPMailer();
        $mail->IsSMTP(); 
        $mail->SMTPAuth = true; 
        $mail->Host     = "";                 // SMTP servers
        $mail->Username =  "";                  // SMTP username
        $mail->Password =  "";             // SMTP password
        $mail->From     = "";       
        $mail->FromName = "Adaptweb";
        for($i=0; $i<count($dest); $i++){
            $sql = "SELECT email_usuario FROM usuario WHERE id_usuario='$dest[$i]'";
            $res = mysql_query($sql,$id);
            $row = mysql_fetch_row($res);
            $mail -> AddAddress($row[0]);
		//echo $row[0];
        }
       // $mail->AddAddress("mhkimura@gmail.com","Marcos Kimura"); 
       // $mail->AddAddress("isagasp@gmail.com");             // se quiser colocar outro destinatario
	//$mail->AddBCC("mhkimura@gmail.com","Marcos Kimura"); 
	//$mail->AddBCC("mhk.cadastro@gmail.com","Marcos Kimura"); 
	//$mail->AddAddress("mhkimura@gmail.com");  
        $mail->WordWrap = 50;                           
        $mail->IsHTML(true);                            
        $mail->Subject  =  "ADAPTWEB - Um aluno deixou uma nova mensagem no mural de recados";
        $mail->Body     =  $texto;
        $mail->AltBody  =  "This is the text-only body";
        if(!$mail->Send())   // mensagem se der problema
        {
            $ok = 10;
            /*
            $msg_erro = $mail->ErrorInfo;
            if ($msg_erro == "Language string failed to load: recipients_failed".$mail)
            {
                echo "Email Inexistente. <p>";
            }
            */
            //echo "Erro: " . $mail->ErrorInfo;
            //exit;
        }
    }
}
//fim da funcao


}

if ($logado<>"S"){
echo "<META HTTP-EQUIV='REFRESH' CONTENT=\"0; URL='./login.php'\">";
		}
else {
  
 session_register("num_disc");
 session_register("num_curso");
 session_register("curso");
 session_register("id_prof");
 session_register("disciplina");
 
?>

<html>
<head><title>Adaptweb</title></head>
<body bgcolor="#ffffff" link="#0000cd">

<?
 $orelha = array();  
  $orelha = array(
  		array(   
   		   "LABEL" => "<a  class=menu href=n_index_navegacao.php?opcao=Navega&id_prof=".$id_prof."&num_disc=".$num_disc."&num_curso=".$num_curso."&disciplina=".$disciplina."&curso=".$curso.">".A_LANG_LENVIRONMENT_NAVTYPE."</a>", 
     		   "LINK" => "n_index_navegacao.php?opcao=Navega", 
     		   "ESTADO" =>"OFF"
   		    ),
		array(   
   		   "LABEL" => A_LANG_LENVIRONMENT_MURAL,  
     		   "LINK" => "n_index_navegacao.php?opcao=MuralRecados",    		                                 
     		   "ESTADO" => "ON"
   		   ),

		   array(   
   		   "LABEL" => "<a class=menu href=n_index_navegacao.php?opcao=ForumDiscussao&id_prof=".$id_prof."&num_disc=".$num_disc."&num_curso=".$num_curso."&disciplina=".$disciplina."&curso=".urlencode($curso).">".A_LANG_LENVIRONMENT_FORUM."</a>",  
     		   "LINK" => "n_index_navegacao.php?opcao=ForumDiscussao",    		                                 
     		   "ESTADO" => "OFF"
   		   ) 	     				     		       		   		   		       		   		   
   		  );  			     		       		   		   		       		   		   

  MontaOrelha($orelha);  
?>
 <table CELLSPACING=5 CELLPADDING=3 border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> style="height:350px;">
	<tr valign="top">
		<td>
	<table CELLSPACING=0 CELLPADDING=0 width="98%"  border = "0"  bgcolor="#ffffff" style=" margin-left: 10px; margin-top: 10px; border-left:3px; border-top:3px; border-bottom:3px; border-right:3px; border-style:solid; border-color:#eeeeee; " >	 	
	<tr >
		<td>
		<? 
		  $orelha = array();  
		  $orelha = array(
		 
				array(   
		   		   "LABEL" => 'Mural',  
		     		   "LINK" => "n_index_navegacao.php?opcao=MuralRecados",    		                                 
		     		   "ESTADO" => "OFF"
		   		   ), 			     		       		   		   		       		   		   
				   array(   
		   		   "LABEL" => "Escrever Recado",  
		     		   "LINK" => "n_index_navegacao.php?opcao=CriaRecados",    		                                 
		     		   "ESTADO" => "ON"
		   		   ) 	     					     		       		   		   		       		   		   
		   		  ); 
		
		
		  MontaOrelha($orelha);  
		
		?>
		</td>
	</tr>
	<tr>
	<td>
	<table CELLSPACING=0 CELLPADDING=0 width="97%"  border = "0"  bgcolor="#ffffff" style="margin-left: 30px; margin-top: 10px; height:250px;" >	
	<tr>
		<td>
		<? 
		echo "<h2>". A_LANG_MURAL_DISC.$disciplina; echo " ".A_LANG_LENVIRONMENT_FORCOURSE." "; echo $curso."</h2>";
		?>

			<? 
			if($ok==1){
			$icone = "error.png";
			$mensagem = A_LANG_REC_ALERT_ALL;
			}else if($ok==2){
			$icone = "error.png";
			$mensagem = A_LANG_REC_ALERT_DEST;
			}else if($ok==3){
			$icone = "error.png";
			$mensagem = A_LANG_REC_ALERT_MSG;
			}else if($ok==4){
			$icone = "success.png";
			$mensagem = A_LANG_REC_SUSS;
			}else if($ok==5){
			$icone = "error.png";
			$mensagem = A_LANG_REC_INSUSS;
			}else if($ok==6){
			$icone = "error.png";
			$mensagem = A_LANG_REC_INSUSS_ALL;
			}
		?>
	
		</td>
	</tr>
		<td>
				<fieldset style="width:800px;" class=campo>
				<legend align="left">
					<font color="#000000">| <b><? echo A_LANG_MURAL_ENVIARECADO; ?></b> |</font>
				</legend>
				<br>		

			     <table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0">
			           <tr valign="top">
			             <td>  
			         
			               <div class="Mensagem_Ok">
			                 <?
			                   echo "<p class=\"texto1\">\n";     
			                 echo "<table cellspacing=\"0\" cellpadding=\"0\">";
			                 echo "  <tr >";
			                 echo "    <td width=\"50\">";
			                 echo "      <img src=\"imagens/icones/$icone\" alt=\"\" />";
			                 echo "    </td>";
			                 echo "    <td valign=\"center\">";
			                 
			                 echo $mensagem; 
			                 echo "    </td>";
			                 echo "  </tr>";
			                 echo "</table>";  
			                   echo "</p>\n";      
			                 ?>    
			               </div>
			               <br>
			               </td>
			             </tr> 
			             <tr><td>
							<input type="button" class="buttonBig" value="Voltar ao Mural" onclick="window.location = 'n_index_navegacao.php?opcao=MuralRecados'">
							<input type="button" class="buttonBig" value="Enviar outro recado" onclick="window.location = 'n_index_navegacao.php?opcao=CriaRecados'">
			             </td></tr>      

			     </table>  
<br>


	</td>
	</tr>
</table>	
<br>


</td>
	</tr>

</table>
<br>
</td>
	</tr>
</table>

</body>
</html>
 
<?php }?>

