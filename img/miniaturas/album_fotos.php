<?php
$fotos = array(
"gremio.jpg",
"guias.jpg",
"cooler.jpg",
"guias.jpg",
"cooler.jpg",
"gremio.jpg"
);

$fotos_por_linha = 3;

?>

<html>
<head>
<title>Meu �lbum de fotos</title>
</head>
<body>
<h2 align="center">Meu �lbum de fotos</h2>

<div align="center">
  <center>
  <table border="0" cellspacing="5" width="90%">

<?php

// calcula o n�mero de linhas da tabela
$num_fotos = sizeof($fotos);
$num_linhas = intval ($num_fotos/$fotos_por_linha);
if($num_fotos % $fotos_por_linha!=0)
    $num_linhas++;

// imprime as linhas e colunas
$indice = 0;
for($i=0; $i<$num_linhas; $i++)
{
    echo "<tr>";
    for($j=0; $j<$fotos_por_linha; $j++)
    {
        if($indice<$num_fotos)
        {
            $original = $fotos[$indice];
            // monta o nome do arquivo miniatura
            $mini = explode('.', $original);
            $mini = $mini[0]."_mini.jpg";

            echo "<td><a href=$original><img src=$mini border=0></a></td>";
            $indice++;
        }
        else
            echo "<td></td>";
    }
    echo "</tr>";
}
?>
  </table>
  </center>
</div>
</body>
</html>

