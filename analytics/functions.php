<?php

/* ---------------------------------------------------------------------
* Arquivo de fun��es do Web Analytics                                *
* @author Lucas Sim�es de Carvalho <lucas.scarvalho@gmail.com>        *
* @version 1.0 <01/08/2013>                                           *
*                                                                     *
* Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.   *
* Como Trabalho de Conclus�o de Curso                                 *
* Orientadora: Isabela Gasparini                                      *
*                                                                     *
----------------------------------------------------------------------*/

//Estabelecendo conex�o com o servidor
function conectaAdodb()
{
	include WA_PATH . "config/configuracoes.php";
	include WA_PATH . "include/adodb/adodb.inc.php";
	include "../config/configuracoes.php";
	include "../include/adodb/adodb.inc.php";
	// include_once(__DIR__."/../config/configuracoes.php");
	// include_once(__DIR__."/../include/adodb/adodb.inc.php");

	$conn = &ADONewConnection('mysql');
	$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	return ($conn);
}

//Define a data inicial padr�o (90 dias atr�s)
function defaultStartDate() {
	return(date("Y-m-d", strtotime("now - 90 days")));
}

//Define a data final padr�o (Data de hoje)
function defaultEndDate() {
	return(date('Y-m-d'));
}

//Fun��o gen�rica para retornar todas as m�tricas
function wa_query($metric='Visitas_por_aluno', $id_curso='%', $id_disc='%', $id_prof='%', $startdate=NULL, $enddate=NULL, $id_usuario='%', $conceito='%')
{
	if(is_null($id_curso) OR $id_curso == '')
		$id_curso ='%';
	if(is_null($id_disc) OR $id_disc == '')
		$id_disc ='%';
	if(is_null($id_prof) OR $id_prof == '')
		$id_prof ='%';
	if(is_null($startdate) OR $startdate == '')
		$startdate=defaultStartDate();
	if(is_null($enddate) OR $enddate == '')
		$enddate=defaultEndDate();

	/* M�TRICAS - USO GERAL  */

	//METRICA: Visitas por Aluno (Nome do Aluno, Total de Visitas, Tempo m�dio por visita)
	if($metric =='Visitas_por_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT usuario.id_usuario, usuario.nome_usuario,
									count(usuario.id_usuario) as total_visitas,
									SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time))))) as tempo_total,
									SEC_TO_TIME((SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time)))))
									/(count(usuario.id_usuario))) as tempo_medio
									FROM piwik_log_visit
									INNER JOIN usuario on usuario.id_usuario = piwik_log_visit.custom_var_v1
									INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
									INNER JOIN disciplina ON disciplina.id_disc	= piwik_log_visit.custom_var_v3
									WHERE usuario.tipo_usuario = 'aluno'
									AND curso.id_curso like '".$id_curso."'
									AND disciplina.id_disc like '".$id_disc."' AND piwik_log_visit.custom_var_v3 <> ''
									AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
									AND piwik_log_visit.visit_first_action_time >= '".$startdate." 00:00:00' AND piwik_log_visit.visit_last_action_time <= '".$enddate." 23:59:59'
									GROUP BY piwik_log_visit.custom_var_v1
								");

		return ($result);
	}

	//METRICA: Tempo acesso aluno (Nome do aluno, Nome da Disciplina, Tempo m�dio na disciplina, Tempo m�dio geral do aluno
	if($metric =='Tempo_acesso_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT usuario.id_usuario, usuario.nome_usuario, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								count(usuario.id_usuario) as total_visitas,
								SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(t1.visit_last_action_time,t1.visit_first_action_time))))) as tempo_total_disc,
								SEC_TO_TIME((SUM(TIME_TO_SEC((TIMEDIFF(t1.visit_last_action_time,t1.visit_first_action_time)))))
								/(count(usuario.id_usuario))) as tempo_medio_disc,

									(SELECT SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(t2.visit_last_action_time,t2.visit_first_action_time)))))
									FROM piwik_log_visit as t2
									INNER JOIN usuario on usuario.id_usuario = t2.custom_var_v1
									WHERE t2.custom_var_v1 = t1.custom_var_v1
									) as tempo_total,

									(SELECT SEC_TO_TIME((SUM(TIME_TO_SEC((TIMEDIFF(t1.visit_last_action_time,t1.visit_first_action_time)))))
									/(count(usuario.id_usuario)))
									FROM piwik_log_visit as t3
									INNER JOIN usuario on usuario.id_usuario = t3.custom_var_v1
									WHERE t3.custom_var_v1 = t1.custom_var_v1
									) as tempo_medio_total

								FROM piwik_log_visit as t1
								INNER JOIN usuario on usuario.id_usuario = t1.custom_var_v1
								INNER JOIN curso ON curso.id_curso = t1.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc	= t1.custom_var_v3
								WHERE usuario.tipo_usuario = 'aluno'
								AND curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'
								AND t1.custom_var_v4 like '".$id_prof."'
								AND t1.visit_first_action_time >= '".$startdate." 00:00:00' AND t1.visit_last_action_time <= '".$enddate." 23:59:59'
								GROUP BY t1.custom_var_v1, t1.custom_var_v3
				");
				return ($result);
	}

	//METRICA: Tempo acesso aluno geral (Nome do aluno, Nome da Disciplina, Tempo m�dio geral do aluno
	if($metric =='Tempo_acesso_aluno_geral') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT usuario.id_usuario, usuario.nome_usuario,
								count(usuario.id_usuario) as total_visitas,

									(SELECT SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(t2.visit_last_action_time,t2.visit_first_action_time)))))
									FROM piwik_log_visit as t2
									INNER JOIN usuario on usuario.id_usuario = t2.custom_var_v1
									WHERE t2.custom_var_v1 = t1.custom_var_v1
									) as tempo_total,

									(SELECT SEC_TO_TIME((SUM(TIME_TO_SEC((TIMEDIFF(t1.visit_last_action_time,t1.visit_first_action_time)))))
									/(count(usuario.id_usuario)))
									FROM piwik_log_visit as t3
									INNER JOIN usuario on usuario.id_usuario = t3.custom_var_v1
									WHERE t3.custom_var_v1 = t1.custom_var_v1
									) as tempo_medio_total

								FROM piwik_log_visit as t1
								INNER JOIN usuario on usuario.id_usuario = t1.custom_var_v1
								INNER JOIN curso ON curso.id_curso = t1.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc	= t1.custom_var_v3
								WHERE usuario.tipo_usuario = 'aluno'
								AND curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'
								AND t1.custom_var_v4 like '".$id_prof."'
								AND t1.visit_first_action_time >= '".$startdate." 00:00:00' AND t1.visit_last_action_time <= '".$enddate." 23:59:59'
								GROUP BY t1.custom_var_v1, t1.custom_var_v3
				");
		return ($result);
	}

	//METRICA: Frequencia dos acessos por disciplina
	if($metric =='Frequencia_acessos') {
		$conn=conectaAdodb();
		$result['dia'] = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.nome_disc,
								COUNT(piwik_log_visit.custom_var_v1) as total_acessos,
								SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time))))) as tempo_total,
								SEC_TO_TIME((SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time)))))/COUNT(piwik_log_visit.custom_var_v1)) as tempo_medio,
								DATE_FORMAT(DATE(piwik_log_visit.visit_first_action_time),'%d/%m/%Y') as data
								FROM piwik_log_visit
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc	= piwik_log_visit.custom_var_v3
								WHERE piwik_log_visit.custom_var_v1 <> ''
								AND curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'  AND piwik_log_visit.custom_var_v3 <> ''
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND piwik_log_visit.visit_first_action_time >= '".$startdate." 00:00:00' AND piwik_log_visit.visit_last_action_time <= '".$enddate." 23:59:59'
								GROUP BY DATE(piwik_log_visit.visit_first_action_time), disciplina.id_disc
				");
		$result['semana'] = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.nome_disc,
								COUNT(piwik_log_visit.custom_var_v1) as total_acessos,
								SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time))))) as tempo_total,
								SEC_TO_TIME((SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time)))))/COUNT(piwik_log_visit.custom_var_v1)) as tempo_medio,
								WEEK(piwik_log_visit.visit_first_action_time)  as num_semana,
								CONCAT(DATE_FORMAT(DATE(DATE_ADD(visit_first_action_time, INTERVAL(-5-DAYOFWEEK(visit_first_action_time)) DAY)),'%d/%m/%Y'), ' -> ', DATE_FORMAT(DATE(DATE_ADD(visit_first_action_time, INTERVAL(1-DAYOFWEEK(visit_first_action_time)) DAY)),'%d/%m/%Y')) as semana
								FROM piwik_log_visit
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc	= piwik_log_visit.custom_var_v3
								WHERE piwik_log_visit.custom_var_v1 <> ''
								AND curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'  AND piwik_log_visit.custom_var_v3 <> ''
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND piwik_log_visit.visit_first_action_time >= '".$startdate." 00:00:00' AND piwik_log_visit.visit_last_action_time <= '".$enddate." 23:59:59'
								GROUP BY num_semana, disciplina.id_disc
				");
		$result['mes'] = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.nome_disc,
								COUNT(piwik_log_visit.custom_var_v1) as total_acessos,
								SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time))))) as tempo_total,
								SEC_TO_TIME((SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time)))))/COUNT(piwik_log_visit.custom_var_v1)) as tempo_medio,
								CONCAT(MONTH(piwik_log_visit.visit_first_action_time),'/', YEAR(piwik_log_visit.visit_first_action_time))  as mes
								FROM piwik_log_visit
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc	= piwik_log_visit.custom_var_v3
								WHERE piwik_log_visit.custom_var_v1 <> ''
								AND curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."' AND piwik_log_visit.custom_var_v3 <> ''
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND piwik_log_visit.visit_first_action_time >= '".$startdate." 00:00:00' AND piwik_log_visit.visit_last_action_time <= '".$enddate." 23:59:59'
								GROUP BY disciplina.id_disc, mes
				");
		return ($result);
	}


	//METRICA: Frequencia dos acessos geral
	if($metric =='Frequencia_acessos_geral') {
		$conn=conectaAdodb();
		$result['dia'] = $conn->GetAll("SELECT
								DATE_FORMAT(DATE(piwik_log_visit.visit_first_action_time),'%d/%m/%Y') as data,
								COUNT(piwik_log_visit.custom_var_v1) as total_acessos
								FROM piwik_log_visit
								WHERE piwik_log_visit.custom_var_v1 <> ''  AND piwik_log_visit.custom_var_v3 <> ''
								AND piwik_log_visit.visit_first_action_time >= '".$startdate." 00:00:00' AND piwik_log_visit.visit_last_action_time <= '".$enddate." 23:59:59'
								GROUP BY DATE(piwik_log_visit.visit_first_action_time)
				");
		$result['semana'] = $conn->GetAll("SELECT
								WEEK(piwik_log_visit.visit_first_action_time)  as num_semana,
								CONCAT(DATE_FORMAT(DATE(DATE_ADD(visit_first_action_time, INTERVAL(-5-DAYOFWEEK(visit_first_action_time)) DAY)),'%d/%m/%Y'), ' -> ', DATE_FORMAT(DATE(DATE_ADD(visit_first_action_time, INTERVAL(1-DAYOFWEEK(visit_first_action_time)) DAY)),'%d/%m/%Y')) as semana,
								COUNT(piwik_log_visit.custom_var_v1) as total_acessos
								FROM piwik_log_visit
								WHERE piwik_log_visit.custom_var_v1 <> ''  AND piwik_log_visit.custom_var_v3 <> ''
								AND piwik_log_visit.visit_first_action_time >= '".$startdate." 00:00:00' AND piwik_log_visit.visit_last_action_time <= '".$enddate." 23:59:59'
								GROUP BY num_semana
				");
		$result['mes'] = $conn->GetAll("SELECT
								CONCAT(MONTH(piwik_log_visit.visit_first_action_time),'/', YEAR(piwik_log_visit.visit_first_action_time))  as mes,
								COUNT(piwik_log_visit.custom_var_v1) as total_acessos
								FROM piwik_log_visit
								WHERE piwik_log_visit.custom_var_v1 <> '' AND piwik_log_visit.custom_var_v3 <> ''
								AND piwik_log_visit.visit_first_action_time >= '".$startdate." 00:00:00' AND piwik_log_visit.visit_last_action_time <= '".$enddate." 23:59:59'
								GROUP BY mes
				");
		return ($result);
	}

	//METRICA: Dados tecnologicos de acesso por aluno (Browser e vers�o, SO, Resolu��o de Tela e Localiza��o)
	if($metric =='Dados_tecnologicos_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT id_usuario, nome_usuario, piwik_log_visit.location_country,
									CASE config_browser_name
										when 'IE' then 'Internet Explorer'
										when 'FF' then 'Firefox'
										when 'CH' then 'Chrome'
										when 'SF' then 'Safari'
									ELSE config_browser_name END as config_browser_name,
									piwik_log_visit.config_browser_version,
									CASE config_os
										when 'AND' then 'Android'
										when 'LIN' then 'Linux'
										when 'IPH' then 'Iphone'
										when 'MAC' then 'Macintosh'
										when 'W2K' then 'Windows 2000'
										when 'W95' then 'Windows 95'
										when 'W98' then 'Windows 98'
										when 'WI7' then 'Windows 7'
										when 'WI8' then 'Windows 8'
										when 'WME' then 'Windows Millenium'
										when 'WNT' then 'Windows NT'
										when 'WOS' then 'Web Operational System'
										when 'WS3' then 'Windows Server 2003'
										when 'WVI' then 'Windows Vista'
										when 'WXP' then 'Windows XP'
									ELSE config_os END as config_os,
									piwik_log_visit.config_resolution
									FROM piwik_log_visit INNER JOIN usuario ON piwik_log_visit.custom_var_v1 = usuario.id_usuario
									WHERE usuario.tipo_usuario = 'aluno' AND visit_first_action_time >= '".$startdate." 00:00:00' AND visit_last_action_time <= '".$enddate." 23:59:59'
									AND piwik_log_visit.custom_var_v2 like '".$id_curso."' AND piwik_log_visit.custom_var_v3 like '".$id_disc."' AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
									GROUP BY usuario.id_usuario
				");
				return ($result);

	}

	//METRICA: Dados tecnologicos de acesso (Browser e vers�o, SO, Resolu��o de Tela e Localiza��o)
	if($metric =='Dados_tecnologicos') {
		$conn=conectaAdodb();

		$result['browser'] = $conn->GetAll("SELECT DISTINCT
											CASE config_browser_name
												when 'IE' then 'Internet Explorer'
												when 'FF' then 'Firefox'
												when 'CH' then 'Chrome'
												when 'SF' then 'Safari'
											ELSE config_browser_name END as config_browser_name,
											config_browser_version, count(piwik_log_visit.config_browser_name) as total_browser
											FROM piwik_log_visit INNER JOIN usuario ON piwik_log_visit.custom_var_v1 = usuario.id_usuario
											WHERE usuario.tipo_usuario = 'aluno' AND visit_first_action_time >= '".$startdate." 00:00:00' AND visit_last_action_time <= '".$enddate." 23:59:59'
											AND piwik_log_visit.custom_var_v2 like '".$id_curso."' AND piwik_log_visit.custom_var_v3 like '".$id_disc."' AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
											GROUP BY config_browser_name
				");
		$result['os'] = $conn->GetAll("SELECT DISTINCT
				CASE config_os
					when 'AND' then 'Android'
					when 'LIN' then 'Linux'
					when 'IPH' then 'Iphone'
					when 'MAC' then 'Macintosh'
					when 'W2K' then 'Windows 2000'
					when 'W95' then 'Windows 95'
					when 'W98' then 'Windows 98'
					when 'WI7' then 'Windows 7'
					when 'WI8' then 'Windows 8'
					when 'WME' then 'Windows Millenium'
					when 'WNT' then 'Windows NT'
					when 'WOS' then 'Web Operational System'
					when 'WS3' then 'Windows Server 2003'
					when 'WVI' then 'Windows Vista'
					when 'WXP' then 'Windows XP'
				ELSE config_os END as config_os,
				count(piwik_log_visit.config_os) as total_os
				FROM piwik_log_visit INNER JOIN usuario ON piwik_log_visit.custom_var_v1 = usuario.id_usuario
				WHERE usuario.tipo_usuario = 'aluno' AND visit_first_action_time >= '".$startdate." 00:00:00' AND visit_last_action_time <= '".$enddate." 23:59:59'
				AND piwik_log_visit.custom_var_v2 like '".$id_curso."' AND piwik_log_visit.custom_var_v3 like '".$id_disc."' AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
				GROUP BY config_os
				");
		$result['resolution'] = $conn->GetAll("SELECT DISTINCT config_resolution, count(piwik_log_visit.config_resolution) as total_resolution
				FROM piwik_log_visit INNER JOIN usuario ON piwik_log_visit.custom_var_v1 = usuario.id_usuario
				WHERE usuario.tipo_usuario = 'aluno' AND visit_first_action_time >= '".$startdate." 00:00:00' AND visit_last_action_time <= '".$enddate." 23:59:59'
				AND piwik_log_visit.custom_var_v2 like '".$id_curso."' AND piwik_log_visit.custom_var_v3 like '".$id_disc."' AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
				GROUP BY config_resolution
				");
		return ($result);

	}

	//METRICA: Tempo de acesso por se��o do sistema (Nome da se��o, Total de visualiza��es, Tempo m�dio de acesso)
	if($metric =='Tempo_secao') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT piwik_log_visit.custom_var_v2 as id_curso, curso.nome_curso,
								piwik_log_visit.custom_var_v3 as id_disc, disciplina.nome_disc, piwik_log_visit.custom_var_v4 as id_prof,
								'Ambiente Aula - Modo Livre' as secao,
								SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time))))) as tempo_total,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM piwik_log_visit
								LEFT JOIN usuario ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc = piwik_log_visit.custom_var_v3
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%ad-livre-conceito.php?parametro1=%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY piwik_log_visit.custom_var_v3, secao

								UNION

								SELECT piwik_log_visit.custom_var_v2 as id_curso, curso.nome_curso,
								piwik_log_visit.custom_var_v3 as id_disc, disciplina.nome_disc, piwik_log_visit.custom_var_v4 as id_prof,
								'Ambiente Aula - Modo Tutorial' as secao,
								SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time))))) as tempo_total,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM piwik_log_visit
								LEFT JOIN usuario ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc = piwik_log_visit.custom_var_v3
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%ad-tutorial-conceito.php?parametro1=%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY piwik_log_visit.custom_var_v3, secao

								UNION

								SELECT piwik_log_visit.custom_var_v2 as id_curso, curso.nome_curso,
								piwik_log_visit.custom_var_v3 as id_disc, disciplina.nome_disc, piwik_log_visit.custom_var_v4 as id_prof,
								'Mural de Recados' as secao,
								SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time))))) as tempo_total,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM piwik_log_visit
								LEFT JOIN usuario ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc = piwik_log_visit.custom_var_v3
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%n_index_navegacao.php?opcao=MuralRecados%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY piwik_log_visit.custom_var_v3, secao

								UNION

								SELECT piwik_log_visit.custom_var_v2 as id_curso, curso.nome_curso,
								piwik_log_visit.custom_var_v3 as id_disc, disciplina.nome_disc, piwik_log_visit.custom_var_v4 as id_prof,
								'F�rum de Discuss�o' as secao,
								SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time))))) as tempo_total,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM piwik_log_visit
								LEFT JOIN usuario ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc = piwik_log_visit.custom_var_v3
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%n_index_navegacao.php?opcao=ForumDiscussao%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY piwik_log_visit.custom_var_v3, secao

				");
		return ($result);

	}


	//METRICA: Tempo de acesso por se��o do sistema por cada aluno
	if($metric =='Tempo_secao_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT piwik_log_visit.custom_var_v2 as id_curso, curso.nome_curso,
								piwik_log_visit.custom_var_v3 as id_disc, disciplina.nome_disc, piwik_log_visit.custom_var_v4 as id_prof,
								'Ambiente Aula - Modo Livre' as secao,
								SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time))))) as tempo_total,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM usuario INNER JOIN piwik_log_visit ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc = piwik_log_visit.custom_var_v3
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%ad-livre-conceito.php?parametro1=%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY piwik_log_visit.custom_var_v3, usuario.id_usuario

								UNION

								SELECT piwik_log_visit.custom_var_v2 as id_curso, curso.nome_curso,
								piwik_log_visit.custom_var_v3 as id_disc, disciplina.nome_disc, piwik_log_visit.custom_var_v4 as id_prof,
								'Ambiente Aula - Modo Tutorial' as secao,
								SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time))))) as tempo_total,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM usuario INNER JOIN piwik_log_visit ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc = piwik_log_visit.custom_var_v3
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%ad-tutorial-conceito.php?parametro1=%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY piwik_log_visit.custom_var_v3, usuario.id_usuario

								UNION

								SELECT piwik_log_visit.custom_var_v2 as id_curso, curso.nome_curso,
								piwik_log_visit.custom_var_v3 as id_disc, disciplina.nome_disc, piwik_log_visit.custom_var_v4 as id_prof,
								'Mural de Recados' as secao,
								SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time))))) as tempo_total,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM usuario INNER JOIN piwik_log_visit ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc = piwik_log_visit.custom_var_v3
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%n_index_navegacao.php?opcao=MuralRecados%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY piwik_log_visit.custom_var_v3, usuario.id_usuario

								UNION

								SELECT piwik_log_visit.custom_var_v2 as id_curso, curso.nome_curso,
								piwik_log_visit.custom_var_v3 as id_disc, disciplina.nome_disc, piwik_log_visit.custom_var_v4 as id_prof,
								'F�rum de Discuss�o' as secao,
								SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time))))) as tempo_total,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM usuario INNER JOIN piwik_log_visit ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc = piwik_log_visit.custom_var_v3
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%n_index_navegacao.php?opcao=ForumDiscussao%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY piwik_log_visit.custom_var_v3, usuario.id_usuario
				");
				return ($result);

	}

	//METRICA: ACESSO AOS CONCEITOS
	if($metric =='Acesso_conceitos') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT piwik_log_visit.custom_var_v2 as id_curso,
								piwik_log_visit.custom_var_v3 as id_disc, piwik_log_visit.custom_var_v4 as id_prof,
								SUBSTRING_INDEX(SUBSTRING_INDEX(piwik_log_action.name, 'arq_xml=', -1), '&', 1) AS conceito,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM piwik_log_visit
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%ad-livre-conceito.php?arq_xml=%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY conceito, piwik_log_visit.custom_var_v4 ORDER BY TOTAL_VIEWS DESC
				");
		return ($result);
	}

	//METRICA: ACESSO AOS CONCEITOS POR ALUNOS
	if($metric =='Acesso_conceitos_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT usuario.id_usuario, usuario.nome_usuario, piwik_log_visit.custom_var_v2 as id_curso,
								piwik_log_visit.custom_var_v3 as id_disc, piwik_log_visit.custom_var_v4 as id_prof,
								SUBSTRING_INDEX(SUBSTRING_INDEX(piwik_log_action.name, 'arq_xml=', -1), '&', 1) AS conceito,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM usuario INNER JOIN piwik_log_visit ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%ad-livre-conceito.php?arq_xml=%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY usuario.id_usuario, conceito ORDER BY TOTAL_VIEWS DESC
				");
				return ($result);

	}

	//METRICA: ACESSO AOS EXEMPLOS
	if($metric =='Acesso_exemplos') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT piwik_log_visit.custom_var_v2 as id_curso,
								piwik_log_visit.custom_var_v3 as id_disc, piwik_log_visit.custom_var_v4 as id_prof,
								SUBSTRING_INDEX(SUBSTRING_INDEX(piwik_log_action.name, 'nome_arquivo=', -1), '&', 1) AS exemplo,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM piwik_log_visit
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%ad-lib-filtro.php?nome_arquivo=%exemplo'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY exemplo, piwik_log_visit.custom_var_v4 ORDER BY TOTAL_VIEWS DESC
				");
		return ($result);
	}

	//METRICA: ACESSO AOS EXEMPLOS POR ALUNO
	if($metric =='Acesso_exemplos_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT usuario.id_usuario, usuario.nome_usuario, piwik_log_visit.custom_var_v2 as id_curso,
								piwik_log_visit.custom_var_v3 as id_disc, piwik_log_visit.custom_var_v4 as id_prof,
								SUBSTRING_INDEX(SUBSTRING_INDEX(piwik_log_action.name, 'nome_arquivo=', -1), '&', 1) AS exemplo,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM usuario INNER JOIN piwik_log_visit ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%ad-lib-filtro.php?nome_arquivo=%exemplo'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY usuario.id_usuario, exemplo ORDER BY TOTAL_VIEWS DESC
				");
		return ($result);
	}

	//METRICA: ACESSO AOS EXERCICIOS
	if($metric =='Acesso_exercicios') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT piwik_log_visit.custom_var_v2 as id_curso,
								piwik_log_visit.custom_var_v3 as id_disc, piwik_log_visit.custom_var_v4 as id_prof,
								SUBSTRING_INDEX(SUBSTRING_INDEX(piwik_log_action.name, 'nome_arquivo=', -1), '&', 1) AS exercicio,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM piwik_log_visit
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%ad-lib-filtro.php?nome_arquivo=%exercicio'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY exercicio, piwik_log_visit.custom_var_v4 ORDER BY TOTAL_VIEWS DESC
				");
		return ($result);
	}

	//METRICA: ACESSO AOS EXERCICIOS POR ALUNO
	if($metric =='Acesso_exercicios_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT usuario.id_usuario, usuario.nome_usuario, piwik_log_visit.custom_var_v2 as id_curso,
								piwik_log_visit.custom_var_v3 as id_disc, piwik_log_visit.custom_var_v4 as id_prof,
								SUBSTRING_INDEX(SUBSTRING_INDEX(piwik_log_action.name, 'nome_arquivo=', -1), '&', 1) AS exercicio,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM usuario INNER JOIN piwik_log_visit ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%ad-lib-filtro.php?nome_arquivo=%exercicio'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY usuario.id_usuario, exercicio ORDER BY TOTAL_VIEWS DESC
				");
		return ($result);
	}


	//METRICA: ACESSO AOS MATERIAIS
	if($metric =='Acesso_materiais') {
		$conn=conectaAdodb();
		include WA_PATH . "config/configuracoes.php";
		$result = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								piwik_log_visit.custom_var_v4 as id_prof,
								SUBSTRING_INDEX(SUBSTRING_INDEX(piwik_log_action.name, '".$DOCUMENT_SITE."', -1) , '/', -1) AS material,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM piwik_log_visit
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc = piwik_log_visit.custom_var_v3
								WHERE piwik_log_action.type = 3 AND piwik_log_action.`name` LIKE '".$DOCUMENT_SITE."/disciplinas/%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY material ORDER BY TOTAL_VIEWS DESC
				");
		return ($result);
	}

	//METRICA: ACESSO AOS MATERIAIS POR ALUNO
	if($metric =='Acesso_materiais_aluno') {
		$conn=conectaAdodb();
		include WA_PATH . "config/configuracoes.php";
		$result = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								 usuario.id_usuario, usuario.nome_usuario, piwik_log_visit.custom_var_v4 as id_prof,
								SUBSTRING_INDEX(SUBSTRING_INDEX(piwik_log_action.name, '".$DOCUMENT_SITE."', -1) , '/', -1) AS material,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM usuario INNER JOIN piwik_log_visit ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc = piwik_log_visit.custom_var_v3
								WHERE piwik_log_action.type = 3 AND piwik_log_action.`name` LIKE '".$DOCUMENT_SITE."/disciplinas/%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY usuario.id_usuario, material ORDER BY TOTAL_VIEWS DESC
				");
		return ($result);
	}

	//METRICA: USO DO SISTEMA DE BUSCA POR ALUNO (MODO NAVEGA��O LIVRE)
	if($metric =='Total_busca') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								usuario.id_usuario, usuario.nome_usuario, piwik_log_visit.custom_var_v4 as id_prof,
								piwik_log_visit.custom_var_v3 as id_disc,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM usuario INNER JOIN piwik_log_visit ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc = piwik_log_visit.custom_var_v3
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%ad-livre-busca_xml.php?chave=%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY usuario.id_usuario ORDER BY TOTAL_VIEWS DESC
				");
		return ($result);
	}


	//METRICA: USO DO SISTEMA DE BUSCA POR PALAVRA-CHAVE (MODO NAVEGA��O LIVRE)
	if($metric =='Total_busca_keywords') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								piwik_log_visit.custom_var_v3 as id_disc,
								REPLACE(SUBSTRING_INDEX(SUBSTRING_INDEX(piwik_log_action.name, 'chave=', -1), '&', 1), '%20', ' ') AS keyword,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM piwik_log_visit
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc = piwik_log_visit.custom_var_v3
								WHERE piwik_log_action.type = 1 AND piwik_log_action.`name` like '%ad-livre-busca_xml.php?chave=%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY keyword ORDER BY TOTAL_VIEWS DESC
				");
		return ($result);
	}

	//METRICA: FORUM: ACESSO AO TOPICOS
	if($metric =='Forum_topicos') {
		$conn=conectaAdodb();


		$result = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								topico_forum.id_topico, topico_forum.titulo,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM piwik_log_link_visit_action
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN topico_forum ON topico_forum.id_topico = SUBSTRING_INDEX(SUBSTRING_INDEX(piwik_log_action.name, 'id=', -1), '&', 1)
								INNER JOIN curso ON curso.id_curso = topico_forum.id_curso
								INNER JOIN disciplina ON disciplina.id_disc	= topico_forum.id_disciplina
								WHERE piwik_log_action.`name` LIKE '%n_index_navegacao.php?opcao=ParticiparTopicoDiscussao&id=%'
								AND piwik_log_action.type = 1
								AND curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'

								GROUP BY piwik_log_action.idaction
				");
		return ($result);
	}

	//METRICA: FORUM: ACESSO AO TOPICOS POR ALUNO
	if($metric =='Forum_topicos_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								usuario.id_usuario, usuario.nome_usuario,
								topico_forum.id_topico, topico_forum.titulo,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM piwik_log_link_visit_action
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN topico_forum ON topico_forum.id_topico = SUBSTRING_INDEX(SUBSTRING_INDEX(piwik_log_action.name, 'id=', -1), '&', 1)
								INNER JOIN curso ON curso.id_curso = topico_forum.id_curso
								INNER JOIN disciplina ON disciplina.id_disc	= topico_forum.id_disciplina
								INNER JOIN piwik_log_visit ON piwik_log_visit.idvisit = piwik_log_link_visit_action.idvisit
								INNER JOIN usuario ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								WHERE piwik_log_action.`name` LIKE '%n_index_navegacao.php?opcao=ParticiparTopicoDiscussao&id=%'
								AND curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY usuario.id_usuario, topico_forum.id_topico
				");
		return ($result);
	}

	//METRICA: FORUM: CRIACAO TOPICOS POR ALUNO
	if($metric =='Forum_topicos_criacao') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								usuario.id_usuario, usuario.nome_usuario,
								topico_forum.id_topico, topico_forum.titulo, CONCAT(topico_forum.`data`, ' ', topico_forum.hora) as startdate
								FROM topico_forum INNER JOIN usuario ON usuario.id_usuario = topico_forum.participacao
								INNER JOIN curso ON curso.id_curso = topico_forum.id_curso
								INNER JOIN disciplina ON disciplina.id_disc	= topico_forum.id_disciplina
								WHERE curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'
								AND topico_forum.`data` >= '".$startdate."' AND topico_forum.`data` <= '".$enddate."'
				");
		return ($result);
	}


	//METRICA: FORUM: RESPOSTA AOS TOPICOS POR ALUNO
	if($metric =='Forum_topicos_resposta') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								topico_forum.id_topico, topico_forum.titulo, MIN(participacao_topico_forum.`data`) as startdate,
								COUNT(participacao_topico_forum.id_usuario) as TOTAL_PARTICIPACOES
								FROM topico_forum INNER JOIN participacao_topico_forum ON participacao_topico_forum.id_topico = topico_forum.id_topico
								INNER JOIN curso ON curso.id_curso = topico_forum.id_curso
								INNER JOIN disciplina ON disciplina.id_disc	= topico_forum.id_disciplina
								WHERE curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'
								AND participacao_topico_forum.`data` >= '".$startdate."' AND participacao_topico_forum.`data` <= '".$enddate."'
								GROUP BY topico_forum.id_topico
				");
		return ($result);
	}

	//METRICA: MURAL DE RECADOS - Total Acessos
	if($metric =='Recados_acessos') {
		$conn=conectaAdodb();


		$result = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM piwik_log_link_visit_action
								INNER JOIN piwik_log_visit ON piwik_log_visit.idvisit = piwik_log_link_visit_action.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc	= piwik_log_visit.custom_var_v3
								INNER JOIN usuario ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								WHERE piwik_log_action.`name` LIKE '%n_index_navegacao.php?opcao=MuralRecados%'
								AND curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'
								AND usuario.tipo_usuario = 'aluno'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY disciplina.id_disc, piwik_log_visit.custom_var_v4
				");
		return ($result);
	}

	//METRICA: MURAL DE RECADOS - Acesso por aluno
	if($metric =='Recados_acessos_aluno') {
		$conn=conectaAdodb();


		$result = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								usuario.id_usuario, usuario.nome_usuario,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM piwik_log_link_visit_action
								INNER JOIN piwik_log_visit ON piwik_log_visit.idvisit = piwik_log_link_visit_action.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc	= piwik_log_visit.custom_var_v3
								INNER JOIN usuario ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								WHERE piwik_log_action.`name` LIKE '%n_index_navegacao.php?opcao=MuralRecados%'
								AND usuario.tipo_usuario = 'aluno'
								AND curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY usuario.id_usuario, piwik_log_visit.custom_var_v4
				");
		return ($result);
	}

	//METRICA: MURAL DE RECADOS - Envio de recados por tipo recado
	if($metric =='Recados_envio') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT
								CASE mural.tipo_recado
								WHEN 'geral' THEN 'Grupo'
								WHEN 'pessoal' THEN 'Professor'
								END as tipo_recado,
								MIN(dta) as startdate, MAX(dta) as enddate,
								COUNT(DISTINCT mural.id_recado) as total_recados
								FROM mural
								INNER JOIN destinatario ON destinatario.id_recado = mural.id_recado
								INNER JOIN curso ON curso.id_curso = destinatario.id_curso
								INNER JOIN disciplina ON disciplina.id_disc = destinatario.id_disc
								WHERE curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'
								AND destinatario.id_professor like '".$id_prof."'
								AND dta >= '".$startdate." 00:00:00' AND dta <= '".$enddate." 23:59:59'
								GROUP BY mural.tipo_recado
				");
		return ($result);
	}

	//METRICA: MURAL DE RECADOS - Envio de recados por aluno
	if($metric =='Recados_envio_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								usuario.id_usuario, usuario.nome_usuario,
								CASE mural.tipo_recado
								WHEN 'geral' THEN 'Grupo'
								WHEN 'pessoal' THEN 'Professor'
								END as tipo_recado,
								MIN(dta) as startdate, MAX(dta) as enddate,
								COUNT(DISTINCT mural.id_recado) as total_recados
								FROM mural
								INNER JOIN usuario ON usuario.id_usuario = mural.remetente
								INNER JOIN destinatario ON destinatario.id_recado = mural.id_recado
								INNER JOIN curso ON curso.id_curso = destinatario.id_curso
								INNER JOIN disciplina ON disciplina.id_disc = destinatario.id_disc
								WHERE usuario.tipo_usuario = 'aluno'
								AND curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'
								AND destinatario.id_professor like '".$id_prof."'
								AND dta >= '".$startdate." 00:00:00' AND dta <= '".$enddate." 23:59:59'
								GROUP BY usuario.id_usuario, tipo_recado
				");
		return ($result);
	}

	if($metric =='Recados_enviados_por_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								usuario.id_usuario, usuario.nome_usuario,
								CASE mural.tipo_recado
								WHEN 'geral' THEN 'Grupo'
								WHEN 'pessoal' THEN 'Professor'
								END as tipo_recado,
								MIN(dta) as startdate, MAX(dta) as enddate,
								COUNT(DISTINCT mural.id_recado) as total_recados
								FROM mural
								INNER JOIN usuario ON usuario.id_usuario = mural.remetente
								INNER JOIN destinatario ON destinatario.id_recado = mural.id_recado
								INNER JOIN curso ON curso.id_curso = destinatario.id_curso
								INNER JOIN disciplina ON disciplina.id_disc = destinatario.id_disc
								WHERE usuario.tipo_usuario = 'aluno'
								AND curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'
								AND destinatario.id_professor like '".$id_prof."'
								AND usuario.id_usuario = '".$id_usuario."'
								AND dta >= '".$startdate." 00:00:00' AND dta <= '".$enddate." 23:59:59'
								GROUP BY usuario.id_usuario, tipo_recado
				");
		return ($result);
	}

	if($metric =='Acesso_conceitos_por_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT usuario.id_usuario, usuario.nome_usuario, piwik_log_visit.custom_var_v2 as id_curso,
								piwik_log_visit.custom_var_v3 as id_disc, piwik_log_visit.custom_var_v4 as id_prof,
								SUBSTRING_INDEX(SUBSTRING_INDEX(piwik_log_action.name, 'arq_xml=', -1), '&', 1) AS conceito,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM usuario INNER JOIN piwik_log_visit ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								WHERE piwik_log_action.type = 1 AND (piwik_log_action.`name` like '%ad-livre-conceito.php?arq_xml=%' OR piwik_log_action.`name` like '%ad-tutorial-conceito.php?arq_xml=%')
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND usuario.id_usuario = '".$id_usuario."'
								AND piwik_log_action.name REGEXP '.*" . $conceito . ".*'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY usuario.id_usuario, conceito ORDER BY TOTAL_VIEWS DESC
				");
				return ($result);

	}

	if($metric =='Acesso_todos_conceitos_por_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT usuario.id_usuario, usuario.nome_usuario, piwik_log_visit.custom_var_v2 as id_curso,
								piwik_log_visit.custom_var_v3 as id_disc, piwik_log_visit.custom_var_v4 as id_prof,
								SUBSTRING_INDEX(SUBSTRING_INDEX(piwik_log_action.name, 'arq_xml=', -1), '&', 1) AS conceito,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM usuario INNER JOIN piwik_log_visit ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								WHERE piwik_log_action.type = 1 AND (piwik_log_action.`name` like '%ad-livre-conceito.php?arq_xml=%' OR piwik_log_action.`name` like '%ad-tutorial-conceito.php?arq_xml=%')
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND usuario.id_usuario = '".$id_usuario."'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY usuario.id_usuario, conceito ORDER BY TOTAL_VIEWS DESC
				");
				return ($result);

	}

	if($metric =='Acesso_materiais_por_aluno') {
		$conn=conectaAdodb();
		include WA_PATH . "config/configuracoes.php";
		$result = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								 usuario.id_usuario, usuario.nome_usuario, piwik_log_visit.custom_var_v4 as id_prof,
								SUBSTRING_INDEX(SUBSTRING_INDEX(piwik_log_action.name, '".$DOCUMENT_SITE."', -1) , '/', -1) AS material,
								count(piwik_log_link_visit_action.idaction_url) as TOTAL_VIEWS,
								MIN(server_time) as startdate, MAX(server_time) as enddate
								FROM usuario INNER JOIN piwik_log_visit ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								INNER JOIN piwik_log_link_visit_action ON piwik_log_link_visit_action.idvisit = piwik_log_visit.idvisit
								INNER JOIN piwik_log_action ON piwik_log_action.idaction = piwik_log_link_visit_action.idaction_url
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc = piwik_log_visit.custom_var_v3
								WHERE piwik_log_action.type = 3 AND piwik_log_action.`name` LIKE '".$DOCUMENT_SITE."/disciplinas/%'
								AND piwik_log_visit.custom_var_v2 like '".$id_curso."'
								AND piwik_log_visit.custom_var_v3 like '".$id_disc."'
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.tipo_usuario = 'aluno'
								AND usuario.id_usuario = '".$id_usuario."'
								AND piwik_log_action.name LIKE '%$conceito%'
								AND server_time >= '".$startdate." 00:00:00' AND server_time <= '".$enddate." 23:59:59'
								GROUP BY usuario.id_usuario, material ORDER BY TOTAL_VIEWS DESC
				");
		return ($result);
	}

	if($metric =='Forum_topicos_resposta_por_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT usuario.id_usuario, usuario.nome_usuario, curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								topico_forum.id_topico, topico_forum.titulo, MIN(participacao_topico_forum.`data`) as startdate,
								COUNT(participacao_topico_forum.id_usuario) as TOTAL_PARTICIPACOES
								FROM topico_forum INNER JOIN participacao_topico_forum ON participacao_topico_forum.id_topico = topico_forum.id_topico
								INNER JOIN usuario ON usuario.id_usuario = participacao_topico_forum.id_usuario
								INNER JOIN curso ON curso.id_curso = topico_forum.id_curso
								INNER JOIN disciplina ON disciplina.id_disc	= topico_forum.id_disciplina
								WHERE curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'
								AND usuario.id_usuario = '".$id_usuario."'
								AND participacao_topico_forum.`data` >= '".$startdate."' AND participacao_topico_forum.`data` <= '".$enddate."'
								GROUP BY topico_forum.id_topico
				");
		return ($result);
	}

	if($metric =='Forum_topicos_criacao_por_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc,
								usuario.id_usuario, usuario.nome_usuario,
								topico_forum.id_topico, topico_forum.titulo, CONCAT(topico_forum.`data`, ' ', topico_forum.hora) as startdate
								FROM topico_forum INNER JOIN usuario ON usuario.id_usuario = topico_forum.participacao
								INNER JOIN curso ON curso.id_curso = topico_forum.id_curso
								INNER JOIN disciplina ON disciplina.id_disc	= topico_forum.id_disciplina
								WHERE curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'
								AND usuario.id_usuario = '".$id_usuario."'
								AND topico_forum.`data` >= '".$startdate."' AND topico_forum.`data` <= '".$enddate."'
				");
		return ($result);
	}

	if($metric =='Frequencia_acessos_por_aluno') {
		$conn=conectaAdodb();
		$result = $conn->GetAll("SELECT usuario.id_usuario, curso.id_curso, curso.nome_curso, disciplina.nome_disc,
								COUNT(piwik_log_visit.custom_var_v1) as total_acessos,
								SEC_TO_TIME(SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time))))) as tempo_total,
								SEC_TO_TIME((SUM(TIME_TO_SEC((TIMEDIFF(piwik_log_visit.visit_last_action_time,piwik_log_visit.visit_first_action_time)))))/COUNT(piwik_log_visit.custom_var_v1)) as tempo_medio,
								DATE_FORMAT(DATE(piwik_log_visit.visit_first_action_time),'%d/%m/%Y') as data
								FROM piwik_log_visit
								INNER JOIN curso ON curso.id_curso = piwik_log_visit.custom_var_v2
								INNER JOIN disciplina ON disciplina.id_disc	= piwik_log_visit.custom_var_v3
								INNER JOIN usuario ON usuario.id_usuario = piwik_log_visit.custom_var_v1
								WHERE piwik_log_visit.custom_var_v1 <> ''
								AND curso.id_curso like '".$id_curso."'
								AND disciplina.id_disc like '".$id_disc."'  AND piwik_log_visit.custom_var_v3 <> ''
								AND piwik_log_visit.custom_var_v4 like '".$id_prof."'
								AND usuario.id_usuario = '".$id_usuario."'
								AND piwik_log_visit.visit_first_action_time >= '".$startdate." 00:00:00' AND piwik_log_visit.visit_last_action_time <= '".$enddate." 23:59:59'
								GROUP BY DATE(piwik_log_visit.visit_first_action_time), disciplina.id_disc
				");
		return ($result);
	}

	else {
		return("M�trica inv�lida!");
	}

}

?>
