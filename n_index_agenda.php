
<?
ob_start();
$opcao = $_GET["opcao"];
/** -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Inform�tica  
 *       UEL - Departamento de Computa��o
 * -----------------------------------------------------------------------
 *         @name index.php
 *     @abstract P�gina principal do ambiente AdaptWeb
 *        @since 25/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *      @package AdaptWeb  
 * -----------------------------------------------------------------------
 */ 

// sess�o  
session_start();

// inclui arquivo de configura��es
include "config/configuracoes.php"; 

// inclui arquivo de fun��es
include "include/funcoes.php"; 

global $logado, $id_usuario, $email_usuario, $A_LANG_IDIOMA_USER, $tipo_usuario;
 
// Troca de idioma - idioma de configura��o padr�o ou idioma selecionado pelo usu�rio no momento do cadastro
if (isset($newlang))
   $A_LANG_IDIOMA_USER=trocalang($newlang, $id_usuario);
   
 if ($A_LANG_IDIOMA_USER == "")
    include "idioma/".$A_LANG_IDIOMA."/geral.php";
 else   
    include "idioma/".$A_LANG_IDIOMA_USER."/geral.php"; 



header ("Content-type: text/html; Charset=".A_LANG_CHACTERSET."\"");    
  
 ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<META http-equiv=Content-Type content="text/html; charset=<? echo A_LANG_CHACTERSET; ?>">
<META http-equiv=expires content="0">
<META http-equiv=Content-Language content="<? echo A_LANG_CODE; ?>">
<META name=KEYWORDS content="Adpta��o, Ensino, Dist�ncia">
<META name=DESCRIPTION content="Modulo de Adaptabilidade do Sistema AdaptWeb">
<META name=ROBOTS content="INDEX,FOLLOW">
<META name=resource-type content="document">
<META name=AUTHOR content="Veronice de Freitas">
<META name=COPYRIGHT content="Copyright (c) 2001 by Veronice de Freitas">
<META name=revisit-after content="1 days">
<META name=distribution content="Global">
<META name=GENERATOR content="Windows NotePade">
<META name=rating content="General">
<META name=MS.LOCALE content="<? echo A_LANG_CODE; ?>">


<?
 global $opcao, $ctx;
 if (is_null($opcao))
 {
  $opcao="Principal";
  Contexto(A_LANG_SYSTEM,0);
 }; 

switch ($opcao)
{		




    case "AcessoAgenda":
		Contexto (A_LANG_MNU_AGENDA,1);
		$inc = "agenda/agenda.php";
		break;
	case "Avancar":
		Contexto (A_LANG_MNU_AGENDA,1);
		$inc = "agenda/avancar.php";
		break;
	case "Retroceder":
		Contexto (A_LANG_MNU_AGENDA,1);
		$inc = "agenda/retroceder.php";
		break;
    case "Evento":
		Contexto (A_LANG_AGENDA_AGENDAMENTO,2);
		$inc = "agenda/evento.php";
		break;
    case "Destinar":
		Contexto (A_LANG_AGENDA_AGENDAMENTO,2);
		$inc = "agenda/destinar.php";
		break;
    case "Destinatario":
		Contexto (A_LANG_AGENDA_NOVO_EVENTO,2);
		$inc = "agenda/destinatario.php";
		break;
    case "EventoAgendado":
		Contexto (A_LANG_AGENDA_AGENDAMENTO,2);
		$inc = "agenda/eventoagendado.php";
		break;
	case "EventoCadastrado":
		Contexto (A_LANG_AGENDA_AGENDAMENTO,2);
		$inc = "agenda/eventocadastrado.php";
		break;
    case "AgendamentoCadastrado":
		Contexto (A_LANG_AGENDA_AGENDAMENTO,2);
		$inc = "agenda/agendamentocadastrado.php";
		break;
	case "EventoData":
		Contexto (A_LANG_AGENDA_AGENDAMENTO,2);
		$inc = "agenda/eventodata.php";
		break;
    case "DestinarData":
		Contexto (A_LANG_AGENDA_AGENDAMENTO,2);
		$inc = "agenda/destinardata.php";
		break;
    case "DestinatarioData":
		Contexto (A_LANG_AGENDA_NOVO_EVENTO,2);
		$inc = "agenda/destinatariodata.php";
		break;
    case "AgendamentoCadastradoData":
		Contexto (A_LANG_AGENDA_AGENDAMENTO,2);
		$inc = "agenda/agendamentocadastradodata.php";
		break;
    case "GerenciamentoEventos":
		Contexto (A_LANG_AGENDA_GERENCIAMENTO_EVENTO,2);
		$inc = "agenda/gerenciamentoeventos.php";
		break;
	case "VisualizarEvento":
		Contexto (A_LANG_AGENDA_VISUALIZAR_EVENTO,2);
		$inc = "agenda/visualizarevento.php";
		break;
    case "ExcluirEvento":
		Contexto (A_LANG_AGENDA_EXCLUIR_EVENTO,2);
		$inc = "agenda/excluirevento.php";
		break;
    case "OutroAgendamento":
		Contexto (A_LANG_AGENDA_AGENDAMENTO,2);
		$inc = "agenda/outroagendamento.php";
		break;
	case "AlterarAgendamento":
		Contexto (A_LANG_AGENDA_ALTERAR_AGENDAMENTO,2);
		$inc = "agenda/alteraragendamento.php";
		break;
	case "AlterarAgendamentoGerenciamento":
		Contexto (A_LANG_AGENDA_ALTERAR_AGENDAMENTO,2);
		$inc = "agenda/alteraragendamentogerenciamento.php";
		break;
    case "AlterarAgendamentoPesquisa":
		Contexto (A_LANG_AGENDA_ALTERAR_AGENDAMENTO,2);
		$inc = "agenda/alteraragendamentopesquisa.php";
		break;
	case "AgendamentoAlterado":
		Contexto (A_LANG_AGENDA_AGENDAMENTO_ALTERADO,2);
		$inc = "agenda/agendamentoalterado.php";
		break;
	case "EventoAlterado":
		Contexto (A_LANG_AGENDA_EVENTO_ALTERADO,2);
		$inc = "agenda/eventoalterado.php";
		break;
	case "AlterarEvento":
		Contexto (A_LANG_AGENDA_ALTERAR_EVENTO,2);
		$inc = "agenda/alterarevento.php";
		break;
    case "ExcluirEvento":
		Contexto (A_LANG_AGENDA_EXCLUIR_EVENTO,2);
		$inc = "agenda/excluirevento.php";
		break;
	case "AjudaAgenda":
		Contexto (A_LANG_AGENDA_AJUDA,2);
		$inc = "agenda/ajudaagenda.php";
		break;
	case "ExcluirAgendamento":
		Contexto (A_LANG_AGENDA_EXCLUIR_AGENDAMENTO,2);
		$inc = "agenda/excluiragendamento.php";
		break;
	case "PesquisaPeriodo":
		Contexto (A_LANG_AGENDA_PESQUISA_PERIODO,2);
		$inc = "agenda/pesquisaperiodo.php";
		break;

	
	default:
		Contexto(A_LANG_MNU_HOME,1);	
		$inc = "agenda/agenda.php";
		break;
} // switch
?>

<title><? echo $opcao." - AdaptWeb" ?></title>

<LINK REL="StyleSheet" HREF="css/geral.css" TYPE="text/css">

<!-- <link rel="stylesheet" type="text/css" href="lib/jquery-ui.css" />
<script type="text/javascript" src="lib/jquery.min.js"></script>
<script type="text/javascript" src="lib/jquery-ui.min.js"></script>

-->
<script type="text/javascript" src="src/jquery.floatingmessage.js"></script>

<script language="JavaScript" type="text/JavaScript" src="javascript/ajaxlib.js"></script>
<link rel="stylesheet" type="text/css" href="javascript/floatbox/floatbox.css" /> 
<script type="text/javascript" src="javascript/floatbox/floatbox.js"></script> 


	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/blitzer/jquery-ui.css" type="text/css" />
	<script src="javascript/jquery.easy-confirm-dialog.js"></script>

	

</head>

<body>


<?php include 'cabecalho.php'; ?>



<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" >

	<TR>
		<!-- op��es do menu  -->
		
		<TD WIDTH="150" HEIGHT="430" valign="top" bgcolor=<? echo $A_COR_FUNDO_MENU ?>>
		


			<? MenuAgenda($opcao) ?>

		</TD>

		<!-- �rea de conte�do -->
		<TD valign="top" >
	 		 		
	 	 
			<table WIDTH="100%" height="100%" valign="top" CELLPADDING=5 CELLSPACING=5 BORDER=0>
				<tr>
					<td>				         	
	
						<?

							include $inc;


					      	?>
																														
					</td>
				</tr>
			</table>
		
		</td>
	</tr>
</table>

<!-- BARRA3  RODAP� -->

<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" bgcolor=#333366 >
	<tr>
		<td>	
			<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">
			
				<tr HEIGHT="20" bgcolor=<? echo $A_COR_FUNDO_BARRA3 ?>>
					<!-- Nome do sistema -->
					<td valign="center">
     						<p class="texto1">
     						   <?
						      echo "".A_LANG_SYSTEM_NAME." - ".A_LANG_VERSION.": ".$A_GENERAL_VERSION;
						    ?> 
						   
						</p>
					</td>					
					
					<!-- Copyright -->
					<td valign="center">
						<p class="texto2">
						    <? 	 						   
						      echo " &nbsp; &nbsp;".A_LANG_SYSTEM_COPYRIGHT."";
						    ?>
						   
						</p>
					</td>
				</tr>	
			</TABLE>


		</td>
	</tr>	
</TABLE>
<?php
include 'rodape.php';
?>

</body>

</html>
