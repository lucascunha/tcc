<?
/* -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Informática  
 *       UEL - Departamento de Computação
 * -----------------------------------------------------------------------
 *       @package Acesso ao ambiente 
 *     @subpakage Efetua login no ambiente
 *          @file a_faz_login.php
 *    @desciption Efetua login no ambiente
 *         @since 25/06/2003
 *        @author Veronice de Freitas (veronice@jr.eti.br)
 * -----------------------------------------------------------------------         
 */ 

// ===================
// Form foi submetido
// ===================

 echo "<HTML>"; 
 
 echo "<BODY>"; 
 
// echo "ANTES";
// echo "<BR>";
// $caminho="/disciplinas/".$id_prof."/".$num_disc;
// 
// echo "Numero da disciplina: = ".$num_disc;
// echo "<br>";
// echo "Numero do curso : ".$num_curso;
// echo "<br>";
// echo "Numero do professor ".$id_prof;
// echo "<br>";
// echo "Caminho : ".$caminho;
// echo "<br>";
// echo "Nome da Disciplina ".$disciplina;
 
 
 session_start();
 
 global $num_disc, $num_curso, $id_prof, $caminho, $disciplina, $curso;
 
 session_register("num_disc");
 session_register("num_curso");
 session_register("curso");
 session_register("id_prof");
 session_register("caminho");
 session_register("disciplina");
 
// echo "DEPOIS";
// echo "<BR>";
  
 $caminho="/disciplinas/".$id_prof."/".$num_disc;
 
// echo "Numero da disciplina: = ".$num_disc;
// echo "<br>";
// echo "Numero do curso : ".$num_curso;
// echo "<br>";
// echo "Numero do professor ".$id_prof;
// echo "<br>";
// echo "Caminho : ".$caminho;
// echo "<br>";
// echo "Nome da Disciplina ".$disciplina;
// 
// echo "<BR>";
// echo "ENTREI AQUI" ;
//
//die;

$destino="Location: n_index_navegacao.php?opcao=Navega";

header($destino);

echo "</BODY>"; 

echo "</HTML>"; 

?>