<?
/* -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Inform�tica  
 *       UEL - Departamento de Computa��o
 * -----------------------------------------------------------------------
 *       @package Sair  
 *     @subpakage Efetuar logout
 *          @file a_faz_login.php
 *    @desciption Efetua logout 
 *         @since 25/06/2003
 *        @author Veronice de Freitas (veronice@jr.eti.br)
 * -----------------------------------------------------------------------         
 */ 
   	        	       
?>

  <? 
  // cria a matriz de orelhas  
  $orelha = array();  
  $orelha = array(
  		array(   
   		   "LABEL" => A_LANG_TENVIRONMENT_ACCESS , 
     		   "LINK" => "", 
     		   "ESTADO" =>"ON"
   		   ) 		    		       		   		   
   		  ); 

  MontaOrelha($orelha);  

?>
<table CELLSPACING=5 CELLPADDING=5 border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> height="80%" style="height:380px;">
	<tr>
	<td valign=top>
	<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda" >

	<tr>
		<td valign=top>			

       				 <table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0" style="margin-top:10px;">
    
        				      <tr>
        				        <td>  
 							   		           <div class="Mensagem_Ok">
							   		             <?
							   		             echo "<p class=\"texto1\">\n";     
							   		             echo "<table cellspacing=\"0\" cellpadding=\"0\" width=100%>";
							   		             echo "  <tr>";
							   		             echo "    <td width=\"50\">";
							   		             echo "      <img src=\"imagens/icones/notice.png\" alt=\"\" />";
							   		             echo "    </td>";
							   		             echo "    <td valign=\"center\">";
							   		             
												 echo A_LANG_TENVIRONMENT_COMMENT;
												 echo "<br><br>OBS: O Ambiente Aluno pode ser utilizado sem a autoriza��o pr�via do administrador.";
							   		             echo "  </tr>";
							   		             echo "</table>";  
							   		             echo "</p>\n";      
							   		             ?>    
							   		           </div>
							   		      <br>

        				          </td>
        				        </tr>    				
        				</table>  
       				 	<table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0" style="margin-top:10px; margin-bottom:14px;">
        				      <tr>
        				        <td>  
 							   		           <div class="Mensagem_Amarelo">
							   		             <?
							   		             echo "<p class=\"texto1\">\n";     
							   		             echo "<table cellspacing=\"0\" cellpadding=\"0\" width=100%>";
							   		             echo "  <tr>";
							   		             echo "    <td width=\"50\">";
							   		             echo "      <img src=\"imagens/icones/warning.png\" alt=\"\" />";
							   		             echo "    </td>";
							   		             echo "    <td valign=\"center\">";
							   		             
							   		             echo A_LANG_TENVIRONMENT_WAIT;
							   		             echo "    </td>";
							   		             echo "  </tr>";
							   		             echo "</table>";  
							   		             echo "</p>\n";      
							   		             ?>    
							   		           </div>
							   		         

        				          </td>
        				        </tr>   
        				</table>          				

		</td>
	</tr>
	</table>
		</td>
	</tr>
</table>
