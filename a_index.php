<?
ob_start();

$opcao = $_GET['opcao'];
$var = $_GET['var'];

session_start();
$logado 		=	$_SESSION['logado'];
$id_usuario		=	$_SESSION['id_usuario'];
$email_usuario 	=	$_SESSION['email_usuario'];
$A_LANG_IDIOMA_USER=$_SESSION['A_LANG_IDIOMA_USER'];
$tipo_usuario	=	$_SESSION['tipo_usuario'];
$status_usuario	=	$_SESSION['status_usuario'];
$id_aluno		=	$_SESSION['id_aluno'];
$tipo   		=	$_SESSION['tipo'];

include "config/configuracoes.php"; 
include "include/funcoes.php"; 
 
global $logado, $id_usuario, $email_usuario, $A_LANG_IDIOMA_USER, $tipo_usuario;

/**
 * Verifica se o usu�rio est� logado e se ele � professor
 * SE for falso redireciona para p�gina de index.php
 */
if(!$_SESSION['logado'] && $_SESSION['tipo_usuario'] != 'professor')
{
header ("Location: index.php"); 
}

// Troca de idioma
$newlang = $_GET['newlang'];
if (isset($newlang))
   $_SESSION['A_LANG_IDIOMA_USER']=trocalang($newlang, $_SESSION['id_usuario']);
   
 if ($_SESSION['A_LANG_IDIOMA_USER'] == "") {

    include "idioma/".$A_LANG_IDIOMA."/geral.php";

 } else {

   include "idioma/".$_SESSION['A_LANG_IDIOMA_USER']."/geral.php";
 }
   
header ("Content-type: text/html; Charset=".A_LANG_CHACTERSET."\""); 

// solicita login para as demais op��es 
//if ( $opcao <> "SolicitaAcesso" && 
//     $opcao <> "Login" && 
//     $opcao <> "Home" && 
//     $opcao <> "EntradaInstituicoes" && 
//     $opcao <> "EntradaPesquisadores" && 
//     $opcao <> "EntradaAutor" && 
//     $opcao <> "")
//     {
// 	Autenticacao($opcao);
//     } 	

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<META http-equiv=Content-Type content="text/html; charset=<? echo A_LANG_CHACTERSET; ?>">
<META http-equiv=expires content="0">
<META http-equiv=Content-Language content="<? echo A_LANG_CODE; ?>">
<META name=KEYWORDS content="Adpta��o, Ensino, Dist�ncia">
<META name=DESCRIPTION content="Modulo de Adaptabilidade do Sistema AdaptWeb">
<META name=ROBOTS content="INDEX,FOLLOW">
<META name=resource-type content="document">
<META name=AUTHOR content="Veronice de Freitas">
<META name=COPYRIGHT content="Copyright (c) 2001 by Veronice de Freitas">
<META name=revisit-after content="1 days">
<META name=distribution content="Global">
<META name=GENERATOR content="Windows NotePade">
<META name=rating content="General">
<META name=MS.LOCALE content="<? echo A_LANG_CODE; ?>">


<?

// global $opcao, $ctx;
// if (is_null($opcao))
// {
//  $opcao="Principal";
//  Contexto(A_LANG_MNU_AUTHORING,0);
// };

switch ($opcao)
{			
//	case "Root":
//		$inc = "a_em_desenvolvimento.php";
//		break;
	case "AutorizarAcesso":
	       // Contexto(A_LANG_MNU_ACCESS_LIBERATE, 2);		
		$inc = "a_autorizar_matricula.php";
		break;
//	case "AutorizarProfessor":
//	        Contexto(A_LANG_MNU_LIBERATE_DISCIPLINES, 2);	
//		$inc = "a_autorizar_professor.php";
//		break;	
	case "LiberarDisciplina":
	//	Contexto(A_LANG_MNU_LIBERATE_DISCIPLINES, 2);
		$inc = "a_liberar_disciplina.php";
		break;								
//	case "SolicitaAcesso":
//		$inc = "a_solicitacaoacesso.php";
//		break;			
	case "AlteracaoCurso":
	  //      Contexto(A_LANG_MNU_COURSE, 2);	
		$inc = "a_alteracao_de_cursos.php";
		break;		
	case "ExclusaoCurso":
	  //      Contexto(A_LANG_MNU_COURSE, 2);	
		$inc = "a_exclusao_de_cursos.php";
		break;					
	case "CadastroCurso":
	   //     Contexto(A_LANG_MNU_COURSE, 2);	
		$inc = "a_cadastro_de_cursos.php";
		break;
	case "CadastroDisciplina":
		//Contexto(A_LANG_MNU_DISCIPLINES, 2);
		$inc = "a_cadastro_de_disciplinas.php";
		break;	
	case "AlteracaoDisciplina":
		//Contexto(A_LANG_MNU_DISCIPLINES, 2);
		$inc = "a_alteracao_de_disciplinas.php";
		break;		
	case "ExclusaoDisciplina":
		//Contexto(A_LANG_MNU_DISCIPLINES, 2);
		$inc = "a_exclusao_de_disciplinas.php";
		break;					
	case "CadastroDisciplinaCurso":
		//Contexto(A_LANG_MNU_DISCIPLINES_COURSE, 2);	
		$inc = "a_cadastro_de_disciplinas_curso.php";
		break;							
	case "EntradaTopicos":
	     //   Contexto(A_LANG_MNU_STRUTURALIZE_TOPICS, 2);	
		$inc = "a_topicos_entrada.php";
		break;		
	case "Topicos":			   
	    //    Contexto(A_LANG_ENTRANCE_TOPICS_STRUTURALIZE, 3);	
		$inc = "a_topicos.php";
		break;				
	case "TopicosCadastro1":
	    //    Contexto(A_LANG_ENTRANCE_TOPICS_STRUTURALIZE, 3);	
		$inc = "a_topicos_cadastro1.php";
		break;				
	case "TopicosEstruturarArvore":
		$inc = "a_topicos_estruturar_tree.php";
		break;					
	case "TopicosAltera":
		$inc = "a_topicos_altera.php";
		break;		
	case "TopicosExemplos":
		$inc = "a_topicos_exemplos.php";
		break;					
	case "TopicosExercicios":
		$inc = "a_topicos_exercicios.php";
		break;					
	case "TopicosComplementar":
		$inc = "a_topicos_matcomplementar.php";
		break;
	case "Repositorio":
		//Contexto(A_LANG_MNU_REPOSITORY, 2);	
		$inc = "repositorio/repositorio.php";
		break;
	case "AnaliseLog":
	     //   Contexto(A_LANG_MNU_LOG, 2);	
		$inc = "log/analise.php";
		break;
	
	/*
	 * Ferramenta de An�lise Interativa
	 * Inclu�do por Barbara Moissa em 2013-11-02
	 */
	case 'AnaliseInterativa':
		// Contexto(A_LANG_ANALISEINTERATIVA, 2);
		$inc = 'analise-interativa-v3/ferramenta.php';
	break;
	
	case "Questionario":
	     //   Contexto(A_LANG_MNU_LOG, 2);	
		$inc = "questionario/questionario.php";
		break;

/* 
Modifica��es para a avalia��o
Autoria da Avalia��o
Realizado por Claudiomar Desanti
Orientadora: Isabela Gasparini
*/
case "TopicoAvaliacao":
$inc = "avaliacao/avs_topico.php";
break;

case "GrupoParam":
$inc = "avaliacao/avs_grupo.php";
break;

case "NovaAval":
$inc = "avaliacao/avs_nova_aval.php";
break;

case "NovaAvalPresencial":
$inc = "avaliacao/avs_nova_aval_presencial.php";
break;

case "EditarGrupo":
$inc = "avaliacao/avs_grupo.php";
break;
		
case "QuestaoExistente":
$inc = "avaliacao/avs_questao_existente.php";
break;	
		
case "DuplicarAvaliacao":
$inc = "avaliacao/avs_duplicar_aval.php";
break;
		
/*
  Fun��es de Avalia��o
*/
case "FuncaoAvaliacao":
//Contexto(A_LANG_MNU_AVS, 2);
$inc = "avaliacao/f_listar_disc.php";
break;

case "FuncaoIndex":
$inc = "avaliacao/f_index.php";
break;

case "FuncaoMedia":
$inc = "avaliacao/f_media.php";
break;

case "FuncaoAjuste":
$inc = "avaliacao/f_ajuste.php";
break;

case "FuncaoAjustarMedia":
$inc = "avaliacao/f_ajuste_media.php";
break;

case "FuncaoAjustarDissertAval":
$inc = "avaliacao/f_ajuste_dissert_aval.php";
break;

case "FuncaoAjustarDissertAluno":
$inc = "avaliacao/f_ajuste_dissert_aluno.php";
break;
		
case "FuncaoAjustarDissertCorrecao";
$inc = "avaliacao/f_ajuste_dissert_correcao.php";
break;	

case "FuncaoAjustarAnular":
$inc ="avaliacao/f_ajuste_anular.php";
break;

case "FuncaoAjustarAnularDividir":
$inc ="avaliacao/f_ajuste_anular_dividir.php";
break;

case "FuncaoAjustarAnularPeso":
$inc ="avaliacao/f_ajuste_anular_peso.php";
break;

case "FuncaoAjustarAnularQuestao":
$inc = "avaliacao/f_ajuste_anular_questao.php";
break;

case "FuncaoAjustarPresencialGrupo":
$inc = "avaliacao/f_ajuste_presencial_grupo.php";
break;
		
case "FuncaoAjustePresencialAluno":
$inc = "avaliacao/f_ajuste_presencial_alunos.php";
break;
	
// Avalia��o - Liberar Avalia��o
case "FuncaoLiberar":
$inc = "avaliacao/f_liberar_index.php";
break;

case "FuncaoLiberarConceito":
$inc = "avaliacao/f_liberar_conceito.php";
break;

case "FuncaoLiberarAluno":
$inc = "avaliacao/f_liberar_alunos.php";
break;

case "FuncaoLiberarFinal":
$inc = "avaliacao/f_liberar_final.php";
break;

case "FuncaoDivulgar":
$inc = "avaliacao/f_divulgar.php";
break;

case "FuncaoRelatorio":
$inc = "avaliacao/f_relatorio.php";
break;
		
case "FuncaoRelatorioAval":
$inc = "avaliacao/f_relatorio_aval.php";	
break;

case "FuncaoRelatorioAluno":
$inc = "avaliacao/f_relatorio_aluno.php";
break;

case "FuncaoBackup":
$inc = "avaliacao/f_backup.php";
break;

/*** fim da avalia��o **/


	case "Gerenciar":
		//Contexto("Gerenciamento de Disciplinas", 3);	
		$inc = "repositorio/gerenciar/gerenciar.php";
		break;
	
	case "Export":
		//Contexto(A_LANG_EXPORT, 3);	
		$inc = "repositorio/exportacao/export.php";
		break;
	case "Import":
		//Contexto(A_LANG_IMPORT, 3);	
		$inc = "repositorio/importacao/import.php";
		break;
	case "Pesq":
		//Contexto(A_LANG_OBJECT_SEARCH, 3);	
		$inc = "repositorio/pesquisa/pesq.php";
		break;

	case "Trabalhos":
		//Contexto(A_LANG_MNU_WORKS, 2);	
		$inc = "a_trabalhos.php";
		break;										
	default:
	  //      Contexto(A_LANG_MNU_AUTHORING,1);	
		$inc = "a_entrada.php";
		break;
}
				
?>

<title><? echo $opcao." - AdaptWeb" ?></title>
<LINK REL="StyleSheet" HREF="css/geral.css" TYPE="text/css">
<link href="avaliacao/css/ff.css" rel="stylesheet" type="text/css" media="screen" />

<? if($opcao != 'AnaliseInterativa') { ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script> 
<? } else { ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script> 
<? } ?>

<script src="javascript/jquery-ui.min.js"></script>
<link rel="stylesheet" href="javascript/jquery-ui.css" type="text/css" />
<script type="text/javascript" src="src/jquery.floatingmessage.js"></script>
<script language="JavaScript" type="text/JavaScript" src="javascript/ajaxlib.js" ></script>
<script src="javascript/jquery.easy-confirm-dialog.js"></script>

<script type="text/javascript" src="javascript/jquery_notification_v.1.js"></script>
<link href="javascript/jquery_notification.css" type="text/css" rel="stylesheet"/>	

<link rel="stylesheet" href="javascript/colorbox.css" />
<script src="javascript/jquery.colorbox.js"></script>

<link href="javascript/tiptip/tipTip.css" rel="stylesheet">
<script src="javascript/tiptip/jquery.tipTip.js"></script>
<script src="javascript/tiptip/jquery.tipTip.minified.js"></script>
<script>
$(function(){
$(".tooltip").tipTip({maxWidth: "auto", defaultPosition:"right",  edgeOffset: 10, delay: 100});
});
</script>

<style type="text/css">

    .scrollup{
      width:40px;
      height:40px;      
      text-indent:-9999px;
      opacity:0.5;
      position:fixed;
      bottom:50px;
      right:50px;
      display:none;     
      background: url('imagens/icon_top.png') no-repeat;
    }
</style>


   <script type="text/javascript">
      $(document).ready(function(){ 
      
      $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
          $('.scrollup').fadeIn();
        } else {
          $('.scrollup').fadeOut();
        }
      }); 
      
      $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 100);
        return false;
      });


    });
    </script>


<script type="text/javascript">

<?php
if ($opcao =='__AmbienteProfessor')
{
?>

		$(document.body).ready(function(){

               $("<div>Por motivos de seguran�a, a sess�o do professor ir� expirar automaticamente ap�s 30 minutos de inatividade.</div>").floatingMessage({
                    position : "bottom-left",
                    time : 8000, 
                    className : "ui-widget-header",
                    height : 32,
                    width : 380
               });
 		});


<?php
}	
?>
	              	var g_iCount = new Number(); 
                  	var g_iCount = 1800; 
                  	function startCountdown()
                  	{ 

	

                  		if((g_iCount - 1) >= 0)
                  		{ 
                  				g_iCount = g_iCount - 1; 
                  				//numberCountdown.innerText = 'A sess�o se encerrar� automaticamente em ' + g_iCount + ' segundos, caso n�o exista atividade no ambiente professor.' ; 
                  				setTimeout('startCountdown()',2000); 


							//if (g_iCount <= 1800)
							//{ 
								if (g_iCount == 1790)
								{
									$(document.body).ready(function(){

										$("<DIV id='numberCountdown'>A sess�o se encerrar� automaticamente em 60 segundos, caso n�o exista atividade no ambiente professor.</DIV>").
											floatingMessage({
                  							position : "bottom-left",
                    						click : false,
                    						time : 3000, 
                    						className : "ui-state-default",
                    						height : 32,
                    						width : 380
                						});
          							});

								}		
								if (g_iCount < 1790)
							//	{						
								numberCountdown.textContent = 'A sess�o se encerrar� automaticamente em ' + g_iCount + ' segundos, caso n�o exista atividade no ambiente professor.'; 
							//}
							
							if (g_iCount == 0)
							{
								alert ("A sess�o foi encerrada por inatividade");
								window.location = "./p_faz_sair.php"
							}	


                  		}
                  	} 

        </script>



<body onload='startCountdown()'>

<!-- Inicio: Conte�do -->

<?php include 'cabecalho.php'; ?>




<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0">

	<TR>

		<TD WIDTH="155" HEIGHT="430" valign="top" bgcolor=<? echo $A_COR_FUNDO_MENU ?>>
		
			<? menu($opcao) ?>

		</TD>

		<TD valign="top" >
	 		<!-- corpo -->
			<table WIDTH="100%" height="100%" CELLPADDING=5 CELLSPACING=5 BORDER=0>
				<tr valign="top" >
					<td>
				 
						<?
							include $inc;
						?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<?/*
echo "<H3>SESS�O: </H3><pre>";
print_r($_SESSION);
echo "</pre>";
*/?>

<?php
include 'rodape.php';
?>
</body>

</html>

<a href="#" class="scrollup">Para Cima</a>

<div id="dvLoading"></div>
<script>
$(window).load(function(){
  $('#dvLoading').fadeOut(200);
});
</script>

