<?php
// ini_set('display_errors', 1); error_reporting(-1);

global $id_usuario;

$Alterar = $_POST['Alterar'];
$Excluir = $_POST['Excluir'];
$cNome_Disciplina = $_POST['cNome_Disciplina'];
$nID_DISC = $_POST['nID_DISC'];
$quant_ranking = $_POST['quant-ranking'];
$status_gamificacao = $_POST['status-gamificacao'];

$mensagem = '';
$qt_erros_inc = 0;
$qt_erros_alt = 0;
$qt_erros_exc = 0;

// o usuario 2 foi utilizado para demonstração da disciplina e não pode incluir, excluir, alterar dados

if  ($id_usuario <> 2)
{
	if (isset( $Alterar ))
	{
		if(trim($cNome_Disciplina))
		{
			$conn = &ADONewConnection($A_DB_TYPE);
			$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

			$CmdSql = "SELECT nome_disc from disciplina WHERE nome_disc='".$cNome_Disciplina."' and id_usuario=".$id_usuario;
			$res = mysql_query($CmdSql);
			$linhas = array ();
			$linhas = mysql_fetch_row($res);
			if ($linhas[0]=="")
			{
				$sql = "UPDATE disciplina SET nome_disc='".$cNome_Disciplina."' WHERE id_disc='".$nID_DISC."' and id_usuario=".$id_usuario." ";
				$rs = $conn->Execute($sql);
				$mensagem ="Disciplina alterada com Sucesso";
				if ($rs === false) die(A_LANG_DISCIPLINES_MSG2);
				$rs->Close();
			}
			else
			{
				$mensagem ="Este nome já está sendo utilizado. Escolha outro nome.";
				$qt_erros_alt++;
			}
		}
		else
		{
			$mensagem ="Informe o novo nome para a disciplina selecionada";
			$qt_erros_alt++;
		}
		//ATUALIZA AS OPÇÕES DE GAMIFICAÇÃO
		$sql = "SELECT id_disc FROM disciplina WHERE nome_disc='$cNome_Disciplina' and id_usuario='$id_usuario'";
	  $disciplina = $conn->GetAll($sql);
	  $num_disc = $disciplina[0]['id_disc'];

		if(isset($quant_ranking)){
			$result = mysql_query("SELECT * FROM gamificacao_config WHERE id_disc = '$num_disc' ");
			if(mysql_num_rows($result) > 0) {
			  mysql_query("UPDATE gamificacao_config SET ranking = '$quant_ranking' WHERE id_disc = '$num_disc' ");
			} else {
			  mysql_query("INSERT INTO gamificacao_config (id_disc, ranking) VALUES ('$num_disc', '$quant_ranking') ");
			}
		}
		if(isset($status_gamificacao)){
			$result = mysql_query("SELECT * FROM gamificacao_config WHERE id_disc = '$num_disc' ");
			if(mysql_num_rows($result) > 0) {
			  mysql_query("UPDATE gamificacao_config SET status = '$status_gamificacao' WHERE id_disc = '$num_disc' ");
			} else {
			  mysql_query("INSERT INTO gamificacao_config (id_disc, status) VALUES ('$num_disc', '$status_gamificacao') ");
			}
		}
	}
}

if  ($id_usuario == 2)
{

      $mensagem = A_LANG_DEMO;
      $qt_erros_alt++;
      $Alterar = "demo";

}

  $orelha = array();
  $orelha = array(
  		array(
   		   "LABEL" => "Cadastro de Disciplina" ,
     		   "LINK" => "a_index.php?opcao=CadastroDisciplina",
     		   "ESTADO" =>"OFF"
   		   ),
  		array(
   		   "LABEL" => "Alteração de Disciplina" ,
     		   "LINK" => "",
     		   "ESTADO" =>"ON"
   		   ),
  		array(
   		   "LABEL" => "Exclusão de Disciplina" ,
     		   "LINK" => "a_index.php?opcao=ExclusaoDisciplina",
     		   "ESTADO" =>"OFF"
   		   )
   		  );

  MontaOrelha($orelha);

?>
<script language="javascript">
/* Funcao script ------------- ListaCurso() ----------  */
function AlteraDisciplina()
{
    document.Disciplina.submit()
}
</script>


<table border="0" bgcolor="#ffffff" width="100%" height="100%" style="height: 350px;">
<form name = "Disciplina" action="a_index.php?opcao=AlteracaoDisciplina" method="post">


<tr  valign="top">
<td>


<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">

			<?php
			if ($mensagem != '')
			{
				if  ((isset($Alterar)) || (isset($Excluir)))
				{
				   	if (($qt_erros_alt == 0) && ($qt_erros_exc == 0))
				   	{
				   		$tipo_msg = "success";
				   		$icone = "<img src='imagens/icones/success.png' border='0' />";
					}
				   	else
				   	{
				   		$tipo_msg = "error";
						$icone = "<img src='imagens/icones/error.png' border='0' />";
					}
				} ?>
				<script type="text/javascript">
 	   				showNotification({
	       			message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo $mensagem; ?></td></tr></table>", 	      			type: "<? echo $tipo_msg; ?>",
 	       			autoClose: true,
 	       			duration: 8
 	   				});
 	   				//error //success //warning
 				</script>

			<?php } ?>


			<tr>
				<td>

					<table border="0" bgcolor="#ffffff" cellspacing=10 >
						<tr> <td> &nbsp; </td> </tr>

	      				<tr>
						<td width="170" align="right" valign=top>
		 				  	<? echo "Disciplina para alterar" ?>:
						</td>


						<td style="width: 430px">
 							<?

								  $conn = &ADONewConnection($A_DB_TYPE);
		  						  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

		  						  $sql = "SELECT * FROM disciplina where id_usuario = ".$id_usuario." order by nome_disc";

								  $query = mysql_query($sql);
		  						  $qtd = mysql_num_rows($query);

		  						if ($qtd > 0)
		  						{

								  $rs = $conn->Execute($sql);

		  						  if ($rs === false) die(A_LANG_NOT_DISC);

  								  if (isset($nID_DISC))
								     {
								     $sql2 = "SELECT * FROM disciplina where id_disc='".$nID_DISC."' and id_usuario=".$id_usuario." ";
								     $rs2 = $conn->Execute($sql2);
								     $AuxNomeDisciplina = $rs2->fields[1];
								     $rs2->Close();
								     }
								  ?>

								 <div class="styled-select-big">
								  <select size="1" name="nID_DISC" onchange="AlteraDisciplina()" style="width: 455px">

 	 							  <?
	    								while (!$rs->EOF)
		     							{
	                							if ($rs->fields[0] == $nID_DISC)
	         								    	echo "<option selected value=\"".$rs->fields[0]."\">".$rs->fields[1]."</option>";
	         								else
	         							    		echo "<option value=\"".$rs->fields[0]."\">".$rs->fields[1]."</option>";

	                							$rs->MoveNext();


	             							}

	             						        $rs->Close();
	         						  ?>
	         				     </select>
	         				    </div>

						</td>

	      				</tr>

					<tr>
						<td align="right" valign=top>
						       <? echo "Novo nome para a disciplina"; ?>:
						</td>
						<td>
						   <?
 						   echo "<input class=\"text\" type=\"text\" name=\"cNome_Disciplina\"  style=\"width: 420px\" value=\"$AuxNomeDisciplina\">";
					       ?>
						</td>
					</tr>
					</tr>
					<tr>
						<?php
						  if (isset($nID_DISC)) {
				  		 $sql_disciplina = "SELECT id_disc FROM disciplina WHERE nome_disc='$cNome_Disciplina' and id_usuario='$id_usuario'";
				  	   $disciplina = $conn->GetAll($sql_disciplina);
				  	   $num_disc = $disciplina[0]['id_disc'];

					     $sql_gamificacao = "SELECT * FROM gamificacao_config where id_disc='".$nID_DISC."'";
					     $gam_config = $conn->GetAll($sql_gamificacao);
					     $gam_status = $gam_config[0]['status'];
					     $gam_ranking = $gam_config[0]['ranking'];
					     print_r($gam_config);
					     echo $nID_DISC;
				      }
						?>
						<td align="right">
							Gamificação:
						</td>
						<td>
						<div align="left">
							<input type="radio" name="status-gamificacao" value="ativada" <?php echo ($gam_status === 'ativada' ? 'checked' : ''); ?>> Ativada
							<input type="radio" name="status-gamificacao" value="desativada" <?php echo ($gam_status === 'desativada' ? 'checked' : ''); ?>> Desativada
						</div>
						</td>
					</tr>
					<tr>
						<td align="right">
							Posições no Ranking:
						</td>
						<td>
							<select name="quant-ranking">
								<?php
									for($i = 5; $i <= 10; $i++){ ?>
									<option value="<?php echo $i; ?>" <?php echo ($gam_ranking == $i ? 'selected' : '');?>><?php echo $i;?></option>
									<?php }
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td align="left">
						</td>


						<td align=right><br>


        					<table border="0">

								<tr>
									<td>

										<script type="text/javascript">
											$(
												function()
												{
												$("#customDialog01").easyconfirm
												(
													{	locale:
														{
															title: '',
															text: '<br>Confirma a alteração da disciplina ?<br>',
															button: ['Não',' Sim'],
															closeText: 'Fechar'

														}, dialog: $("#question01")
													}

												);
													$("#customDialog01").click(function()
													{

													}
													);

												}
											);
										</script>
										<input type="button" class="buttonBig" onClick="history.go(-1)" value="Voltar">
										&nbsp;&nbsp;
										<input class="buttonBig" type="submit" value=<? echo A_LANG_DISCIPLINES_UPDATE; ?> name="Alterar" id="customDialog01">
											<div class="dialog01" id="question01">
												<table>
													<tr>
														<td width="50">
															<img src="imagens/icones/notice.png" alt="" />
														</td>
														<td valign="center">
															Confirma a alteração da disciplina ?
														</td>
													</tr>
												</table>
											</div>

									</td>

							</table>

								<?php }
									else
						             {
						         	?>

			         					<table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0">
			            				  <tr>
			            				    <td>
			 							   		           <div class="Mensagem_Amarelo">
										   		             <?
										   		             echo "<p class=\"texto1\">\n";
										   		             echo "<table cellspacing=\"0\" cellpadding=\"0\" width=100%>";
										   		             echo "  <tr>";
										   		             echo "    <td width=\"50\">";
										   		             echo "      <img src=\"imagens/icones/warning.png\" alt=\"\" />";
										   		             echo "    </td>";
										   		             echo "    <td valign=\"center\">";

										   		             echo "Não existem disciplinas para alterar.";
										   		             echo "    </td>";
										   		             echo "  </tr>";
										   		             echo "</table>";
										   		             echo "</p>\n";
										   		             ?>
										   		           </div>
										   		           <br>
			            				      </td>
			            				    </tr>
			       						 </table>
			       		 			<?php }?>




						</td>

					</tr>
				   </table>
			</td>
		</tr>

		</table>
	</td>
</tr>
</form>
</table>


