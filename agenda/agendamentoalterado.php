<?

/* ---------------------------------------------------------------------
 * P�gina de visualiza��o do agendamento alterado                      *
 * @author Carina Tissa Aihara <tissa.aihara@gmail.com>                *
 * @version 1.0 <01/11/2009>                                           *
 *                                                                     *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.   *
 * Como Estagio Curricular.                                            *
 * Orientadora: Avanilde Kemczinski                                    *
 * Suporvisora: Edino Lopes Fernandes                                  *
 *                                                                     *
 ----------------------------------------------------------------------*/


/*** Cria matriz de orelha ***/
$orelha = array();
$orelha = array
(
    array
    (
        "LABEL" => A_LANG_AGENDA_AGENDAMENTO,
        "LINK" => "index.php",
        "ESTADO" =>"ON"
    )
);

MontaOrelha($orelha);


?>
<!--- Monta tabela de fundo --->
<table CELLSPACING="0" CELLPADDING="0" border="0" width="100%" bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> height="80%">
    <tr valign="top">
        <td>
            <br>
              <table border="0">
                 <br>
                   <td valign="top">


<?
/*** Inciciando a session ***/
session_start();
session_register("id_event");
session_register("idagendamento");
session_register("destino_evento");



/*** Estabelecer uma conex�o com o banco de dados ***/
include "config/configuracoes.php";
$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;
include "include/conecta.php";



/*** Tabela mostrando o evento cadastrado ***/
echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";



/*** Evento ***/
echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";



/*** Espa�o ***/
//echo "<tr class=zebraA><td colspan=2>&nbsp</td></tr>";


/*** Tipo do evento ***/
$sql="select id_tipo_evento
from evento_agenda
where evento_agenda.id_evento_agenda='$id_event'";
$resp=mysql_query($sql);
$linha = mysql_fetch_row($resp);
if($linha[0]==1)
{
    $sql1="select tipo_evento_outros
    from evento_agenda
    where evento_agenda.id_evento_agenda='$id_event'";
    $resp1=mysql_query($sql1);
    $linha1 = mysql_fetch_row($resp1);
    echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
}
else
{
    $sql="select nome_tipo_evento
    from evento_agenda, tipo_evento_agenda
    where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
    and evento_agenda.id_evento_agenda='$id_event'";
    $resp=mysql_query($sql);
    $linha = mysql_fetch_row($resp);
    echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
}


/*** Tipo do assunto ***/
$sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
from evento_agenda, tipo_assunto_agenda
where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
and evento_agenda.id_evento_agenda='$id_event'";
$resp=mysql_query($sql);
$linha1 = mysql_fetch_row($resp);
echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";


//Assunto
$sql="select assunto_evento
from evento_agenda
where evento_agenda.id_evento_agenda='$id_event'";
$resp=mysql_query($sql);
$linha = mysql_fetch_row($resp);
echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";


/*** Nome do evento ***/
$sql="select nome_evento
from evento_agenda
where evento_agenda.id_evento_agenda='$id_event'";
$resp=mysql_query($sql);
$linha = mysql_fetch_row($resp);
echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";


/*** Descri��o do evento ***/
$sql="select descricao_evento
from evento_agenda
where evento_agenda.id_evento_agenda='$id_event'";
$resp=mysql_query($sql);
$linha = mysql_fetch_row($resp);
echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";
        
        
/*** Data de cria��o do evento ***/
$sql="select data_criacao_evento
from evento_agenda
where evento_agenda.id_evento_agenda='$id_event'";
$resp=mysql_query($sql);
$linha = mysql_fetch_row($resp);
echo "<tr class=zebraB><td>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td><td>$linha[0]</td></tr>";


/*** Espa�o ***/
echo "<tr><td colspan=2>&nbsp</td></tr>";

echo"</table></center>";
        
        
echo"<center><table border=0 cellspacing=1 cellpadding=5 align=center>";
$sql="select DISTINCT destinatario_agendamento_agenda.grupo_destinatario
from destinatario_agendamento_agenda
where destinatario_agendamento_agenda.id_agendamento_evento='$idagendamento'";
$resp=mysql_query($sql);
$linha = mysql_fetch_row($resp);
echo "<tr class=zebraB><td colspan=2 align=center><b>$linha[0]</b></td></tr>";
        
        
/*** Data do evento ***/
$sql="select data_evento
from agendamento_evento_agenda
where agendamento_evento_agenda.id_agendamento_evento='$idagendamento'";
$resp=mysql_query($sql);
$linha = mysql_fetch_row($resp);
$data_event=$linha[0];
$num_letras = strlen($data_event);
$letra=0;
do
{
    if ($data_event[$letra] == "-")
    {
        $num_primeiro_simbolo = $letra;
    }
    $letra=$letra+1;
}while($letra<=4);
for ($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
{
    if ($data_event[$letra]=="-")
    {
        $num_segundo_simbolo = $letra;
    }
}
for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
{
    if($letra==0)
    {
        $ano_data=$data_event[$letra];
    }
    else
    {
        $ano_data=$ano_data.$data_event[$letra];
    }
}
for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
{
    if($letra==$num_primeiro_simbolo + 1)
    {
        $mes_data=$data_event[$letra];
    }
    else
    {
        $mes_data=$mes_data.$data_event[$letra];
    }
}
for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
{
    if($letra==$num_segundo_simbolo + 1)
    {
        $dia_data=$data_event[$letra];
    }
    else
    {
        $dia_data=$dia_data.$data_event[$letra];
    }
}
$data_formatada=$dia_data."-".$mes_data."-".$ano_data;
echo "<tr class=zebraA><td>".A_LANG_AGENDA_DATA_EVENTO.":</td><td>$data_formatada</td></tr>";


/*** Hora in�cio ***/
$sql="select hora_inicio_evento
from agendamento_evento_agenda
where agendamento_evento_agenda.id_agendamento_evento='$idagendamento'";
$resp=mysql_query($sql);
$linha = mysql_fetch_row($resp);
echo "<tr class=zebraB><td>".A_LANG_AGENDA_HORA_INICIAL.":</td><td>$linha[0]</td></tr>";


/*** Hora fim ***/
$sql="select hora_fim_evento
from agendamento_evento_agenda
where agendamento_evento_agenda.id_agendamento_evento='$idagendamento'";
$resp=mysql_query($sql);
$linha = mysql_fetch_row($resp);
echo "<tr class=zebraA><td>".A_LANG_AGENDA_HORA_FINAL.":</td><td>$linha[0]</td></tr>";


/*** Local do evento ***/
$sql="select local_evento
from agendamento_evento_agenda
where agendamento_evento_agenda.id_agendamento_evento='$idagendamento'";
$resp=mysql_query($sql);
$linha = mysql_fetch_row($resp);
echo "<tr class=zebraB><td>".A_LANG_AGENDA_LOCAL_EVENTO.":</td><td>$linha[0]</td></tr>";

                
/*** Autor do Evento***/
$sql="select evento_agenda.id_usuario
from evento_agenda
where evento_agenda.id_evento_agenda='$id_event'
and evento_agenda.id_usuario='$id_usuario'";
$resp=mysql_query($sql);
$linha = mysql_num_rows($resp);

if($linha>0)
{
    $sql="select DISTINCT destinatario_agendamento_agenda.grupo_destinatario
    from destinatario_agendamento_agenda
    where destinatario_agendamento_agenda.id_agendamento_evento='$idagendamento'";
    $resp=mysql_query($sql);
    $linha = mysql_fetch_row($resp);
    $grupo=$linha[0];
    if($grupo != "Minha Agenda")
    {
        /*** Destinatario ***/
        $sql="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario
        from destinatario_agendamento_agenda, usuario
        where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
        and destinatario_agendamento_agenda.id_agendamento_evento='$idagendamento'
        and destinatario_agendamento_agenda.grupo_destinatario='$grupo'";
        $resp=mysql_query($sql);
        echo "<tr class=zebraB><td colspan=2>".A_LANG_AGENDA_DESTINATARIOS.": <select class=select name=dest[] MULTIPLE size=2>";
        while ($array = mysql_fetch_array($resp))
        {
            echo "<option value=\"".$array[0]."\">".$array[1]."</option>";
        }
        echo"</select>";

    }

}
echo"</table></center>";



/*** Espa�o ***/
echo "<tr><td colspan=2>&nbsp</td></tr>";
echo "<tr><td colspan=2 align=left><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR_AGENDA."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";



?>

   </td>
  </table>
  </td>
  </tr>
</table>
