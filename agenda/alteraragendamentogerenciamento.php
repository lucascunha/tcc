<?

/* ---------------------------------------------------------------------
 * P�gina de altera��o de agendamento do gerenciamento de eventos      *
 * @author Carina Tissa Aihara <tissa.aihara@gmail.com>                *
 * @version 1.0 <01/11/2009>                                           *
 *                                                                     *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.   *
 * Como Estagio Curricular.                                            *
 * Orientadora: Avanilde Kemczinski                                    *
 * Suporvisora: Edino Lopes Fernandes                                  *
 *                                                                     *
 ----------------------------------------------------------------------*/
 
?>
<?php include ('_caminho_agenda.php'); ?>
</script>
<script language="javascript" type="text/javascript" src="agenda/datetimepicker.js"></script>

<?
/*** Cria matriz de orelha ***/
$orelha = array();
$orelha = array
(
    array
    (
        "LABEL" => A_LANG_AGENDA_ALTERAR_AGENDAMENTO,
        "LINK" => "index.php",
        "ESTADO" =>"ON"
    )
);

MontaOrelha($orelha);


?>
<!--- Monta tabela de fundo --->
<table CELLSPACING="0" CELLPADDING="0" border="0" width="100%" bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> height="80%">
    <tr valign="top">
        <td>
            <br>
              <table border="0">
                 <br>
                   <td valign="top">
	 
	 
<?
/*** Estabelecer uma conex�o com o banco de dados ***/
include "config/configuracoes.php";
$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;
include "include/conecta.php";

if (isset ($_POST["alterarminhaagenda"]))
{
    $local_evento=$_POST["localevento"];
    $data_evento=$_POST["dataevento"];
    $hora_inicio=$_POST["horainicio"];
    $hora_fim=$_POST["horafim"];

    /*** Formatar a data ***/
    $num_letras = strlen($data_evento);

    $letra=0;
    do
    {
        if ($data_evento[$letra] == "-")
        {
            $num_primeiro_simbolo = $letra;
        }
        $letra=$letra+1;
    }while($letra<=2);

    for ($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
    {
        if ($data_evento[$letra]=="-")
        {
            $num_segundo_simbolo = $letra;
        }
    }
    for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
    {
        if($letra==0)
        {
            $dia_data=$data_evento[$letra];
        }
        else
        {
            $dia_data=$dia_data.$data_evento[$letra];
        }
    }
    for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
    {
        if($letra==$num_primeiro_simbolo + 1)
        {
            $mes_data=$data_evento[$letra];
        }
        else
        {
            $mes_data=$mes_data.$data_evento[$letra];
        }
    }
    for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
    {
        if($letra==$num_segundo_simbolo + 1)
        {
            $ano_data=$data_evento[$letra];
        }
        else
        {
            $ano_data=$ano_data.$data_evento[$letra];
        }
    }
    $data_formatada=$ano_data."-".$mes_data."-".$dia_data;

    if($data_evento!=NULL && $hora_inicio!=NULL && $hora_fim!=NULL)
    {
        if(checkdate($mes_data, $dia_data, $ano_data))
        {
            $quant_i= strlen($hora_inicio);
            $quant_f= strlen($hora_fim);
            if($quant_i==5 && $quant_f==5)
            {
                $aux=0;
                list($hora_i,$minuto_i) = explode(':',$hora_inicio);
                list($hora_f,$minuto_f) = explode(':',$hora_fim);

                for($cont=0;$cont<2;$cont++)
                {
                    if($hora_i[$cont]!="0" && $hora_i[$cont]!="1" && $hora_i[$cont]!="2" && $hora_i[$cont]!="3" && $hora_i[$cont]!="4" && $hora_i[$cont]!="5" && $hora_i[$cont]!="6" && $hora_i[$cont]!="7" && $hora_i[$cont]!="8" && $hora_i[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($hora_f[$cont]!="0" && $hora_f[$cont]!="1" && $hora_f[$cont]!="2" && $hora_f[$cont]!="3" && $hora_f[$cont]!="4" && $hora_f[$cont]!="5" && $hora_f[$cont]!="6" && $hora_f[$cont]!="7" && $hora_f[$cont]!="8" && $hora_f[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($minuto_i[$cont]!="0" && $minuto_i[$cont]!="1" && $minuto_i[$cont]!="2" && $minuto_i[$cont]!="3" && $minuto_i[$cont]!="4" && $minuto_i[$cont]!="5" && $minuto_i[$cont]!="6" && $minuto_i[$cont]!="7" && $minuto_i[$cont]!="8" && $minuto_i[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($minuto_f[$cont]!="0" && $minuto_f[$cont]!="1" && $minuto_f[$cont]!="2" && $minuto_f[$cont]!="3" && $minuto_f[$cont]!="4" && $minuto_f[$cont]!="5" && $minuto_f[$cont]!="6" && $minuto_f[$cont]!="7" && $minuto_f[$cont]!="8" && $minuto_f[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                }
                if($aux==0)
                {
                    $sql="select destinatario_agendamento_agenda.id_agendamento_evento
                    from agendamento_evento_agenda, destinatario_agendamento_agenda
                    where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                    and agendamento_evento_agenda.id_evento_agenda='$even'
                    and agendamento_evento_agenda.data_evento='$data_formatada'
                    and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                    and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                    and destinatario_agendamento_agenda.grupo_destinatario='Minha Agenda'
                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'";
                    $resp=mysql_query($sql);
                    $linha = mysql_num_rows($resp);
                    if ($linha>0)
                    {
                        echo "<table border=0 width=100%>";
                        echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_EXISTE_AGENDAMENTO_IGUAL."</td></tr>";
                        echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AlterarAgendamentoGerenciamento&even=$even&agen=$agen';\"></input>";
                        echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                        echo "</table>";
                        exit;
                    }
                    else
                    {
                        $dia_hoje = date ("d");
                        $mes_hoje = date ("m");
                        $ano_hoje = date ("Y");
                        $data_hoje_formatada=$ano_hoje."-".$mes_hoje."-".$dia_hoje;

                        $dataescolhida=strtotime($data_formatada);
                        $datadehoje=strtotime($data_hoje_formatada);

                        if($dataescolhida < $datadehoje)
                        {
                            echo "<table border=0 width=100%>";
                            echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_ERRO_DATA."</td></tr>";
                            echo "<tr><td colspan=2>&nbsp</td></tr>";
                            echo"</table>";
                        }
                        else
                        {
                            list($hora_i,$minuto_i) = explode(':',$hora_inicio);
                            if ($hora_i < 0 || $hora_i > 23 || $minuto_i < 0 || $minuto_i > 59)
                            {
                                echo "<table border=0 width=100%>";
                                echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_ERRO_HORARIO_INICIO."</td></tr>";
                                echo "<tr><td colspan=2>&nbsp</td></tr>";
                                echo"</table>";
                            }
                            else
                            {
                                list($hora_f,$minuto_f) = explode(':',$hora_fim);

                                if ($hora_f < 0 || $hora_f > 23 || $minuto_f < 0 || $minuto_f > 59)
                                {
                                    echo "<table border=0 width=100%>";
                                    echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_ERRO_HORARIO_FIM."</td></tr>";
                                    echo "<tr><td colspan=2>&nbsp</td></tr>";
                                    echo"</table>";
                                }
                                else
                                {
                                    $horai_timestamp=mktime($hora_i,$minuto_i,00);
                                    $horaf_timestamp=mktime($hora_f,$minuto_f,00);

                                    if($horaf_timestamp < $horai_timestamp)
                                    {
                                        echo "<table border=0 width=100%>";
                                        echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_ERRO_HORARIO."</td></tr>";
                                        echo "<tr><td colspan=2>&nbsp</td></tr>";
                                        echo"</table>";
                                    }
                                    else
                                    {
                                        $conn = &ADONewConnection($A_DB_TYPE);
                                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                        $sql="update agendamento_evento_agenda set
                                        data_evento='$data_formatada',
                                        hora_inicio_evento='$hora_inicio',
                                        hora_fim_evento='$hora_fim',
                                        local_evento='$local_evento',
                                        data_atualizacao_agendamento=NOW()
                                        where agendamento_evento_agenda.id_agendamento_evento='$agen'";
                                        $rs = $conn->Execute($sql);
                                        if ($rs == false)
                                        {
                                            echo "<table border=0 width=100%>";
                                            echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                            echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=VisualizarEvento&eve=$even';\"></input>";
                                            echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                            echo"</table>";
                                            exit;
                                        }
                                        else
                                        {
                                            ?>
                                            <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=AgendamentoAlterado">
                                            <?
                                            //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=AgendamentoAlterado">

                                            echo"<b>Processando...</b>";
                   
                                            $id_event=$even;
                                            $idagendamento=$agen;
                                            /*** Espa�o ***/
                                            echo "<tr><td colspan=2>&nbsp</td></tr>";

                                            /*** Tabela mostrando o evento cadastrado ***/
                                            echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

                                            /*** Evento ***/
                                            echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

                                            /*** Espa�o ***/
                                            //echo "<tr class=zebraA><td colspan=2>&nbsp</td></tr>";


                                            /*** Tipo do evento ***/
                                            $sql="select id_tipo_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            if($linha[0]==1)
                                            {
                                                $sql1="select tipo_evento_outros
                                                from evento_agenda
                                                where evento_agenda.id_evento_agenda='$even'";
                                                $resp1=mysql_query($sql1);
                                                $linha1 = mysql_fetch_row($resp1);
                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                                            }
                                            else
                                            {
                                                $sql="select nome_tipo_evento
                                                from evento_agenda, tipo_evento_agenda
                                                where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                                                and evento_agenda.id_evento_agenda='$even'";
                                                $resp=mysql_query($sql);
                                                $linha = mysql_fetch_row($resp);
                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
                                            }

                                            /*** Tipo do assunto ***/
                                            $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                                            from evento_agenda, tipo_assunto_agenda
                                            where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                                            and evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha1 = mysql_fetch_row($resp);
                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                                            //Assunto
                                            $sql="select assunto_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";



                                            /*** Nome do evento ***/
                                            $sql="select nome_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";


                                            /*** Descri��o do evento ***/
                                            $sql="select descricao_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                                            /*** Data de cria��o do evento ***/
                                            $sql="select data_criacao_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td><td>$linha[0]</td></tr>";

                                            /*** Espa�o ***/
                                            echo "<tr><td colspan=2>&nbsp</td></tr>";

                                            echo"</table></center>";

                                            exit;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    echo "<table border=0 width=100%>";
                    echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_HORA_INVALIDO."</td></tr>";
                    echo "</table>";
                }
            }
            else
            {
                echo "<table border=0 width=100%>";
                echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_HORA_HH_MM."</td></tr>";
                echo "</table>";
            }
        }
        else
        {
            echo "<table border=0 width=100%>";
            echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_DATA_INVALIDA."</td></tr>";
            echo "</table>";
        }
    }
    else
    {
        $cont=0;
        if($data_evento==NULL)
        {
            $cont=$cont+1;
        }
        if($hora_inicio==NULL)
        {
            $cont=$cont+1;
        }
        if($hora_fim==NULL)
        {
            $cont=$cont+1;
        }

        echo "<table border=0 width=100%>";
        echo "<tr><td><img src=\"imagens/erro.gif\"></td>";
        if($cont==1)
        {
            echo "<td>".A_LANG_AGENDA_ERRO_UM_CAMPO."";
            if($data_evento==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_DATA_EVENTO."";
            }
            if($hora_inicio==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_HORA_INICIO."";
            }
            if($hora_fim==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_HORA_FIM."";
            }
            echo "".A_LANG_AGENDA_ERRO_UM."</td></tr>";
        }
        else
        {
            echo "<td>".A_LANG_AGENDA_ERRO_VARIOS."<br>";
            if($data_evento==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_DATA_EVENTO."<br>";
            }
            if($hora_inicio==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_HORA_INICIO."<br>";
            }
            if($hora_fim==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_HORA_FIM."<br>";
            }
            echo "</td></tr>";
        }
        echo "<tr><td colspan=2>&nbsp</td></tr>";
    }
}
    

    

if(isset($_POST["alterarroot"]))
{
    $data_evento=$_POST["dataevento"];
    $hora_inicio=$_POST["horainicio"];
    $hora_fim=$_POST["horafim"];
    $local_evento=$_POST["localevento"];

    /*** Formatar a data ***/
    $num_letras = strlen($data_evento);

    $letra=0;
    do
    {
        if ($data_evento[$letra] == "-")
        {
            $num_primeiro_simbolo = $letra;
        }
        $letra=$letra+1;
    }while($letra<=2);

    for ($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
    {
        if ($data_evento[$letra]=="-")
        {
            $num_segundo_simbolo = $letra;
        }
    }
    for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
    {
        if($letra==0)
        {
            $dia_data=$data_evento[$letra];
        }
        else
        {
            $dia_data=$dia_data.$data_evento[$letra];
        }
    }
    for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
    {
        if($letra==$num_primeiro_simbolo + 1)
        {
            $mes_data=$data_evento[$letra];
        }
        else
        {
            $mes_data=$mes_data.$data_evento[$letra];
        }
    }
    for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
    {
        if($letra==$num_segundo_simbolo + 1)
        {
            $ano_data=$data_evento[$letra];
        }
        else
        {
            $ano_data=$ano_data.$data_evento[$letra];
        }
    }
    $data_formatada=$ano_data."-".$mes_data."-".$dia_data;

    if($data_evento!=NULL && $hora_inicio!=NULL && $hora_fim!=NULL)
    {
        if(checkdate($mes_data, $dia_data, $ano_data))
        {
            $quant_i= strlen($hora_inicio);
            $quant_f= strlen($hora_fim);
            if($quant_i==5 && $quant_f==5)
            {
                $aux=0;
                list($hora_i,$minuto_i) = explode(':',$hora_inicio);
                list($hora_f,$minuto_f) = explode(':',$hora_fim);

                for($cont=0;$cont<2;$cont++)
                {
                    if($hora_i[$cont]!="0" && $hora_i[$cont]!="1" && $hora_i[$cont]!="2" && $hora_i[$cont]!="3" && $hora_i[$cont]!="4" && $hora_i[$cont]!="5" && $hora_i[$cont]!="6" && $hora_i[$cont]!="7" && $hora_i[$cont]!="8" && $hora_i[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($hora_f[$cont]!="0" && $hora_f[$cont]!="1" && $hora_f[$cont]!="2" && $hora_f[$cont]!="3" && $hora_f[$cont]!="4" && $hora_f[$cont]!="5" && $hora_f[$cont]!="6" && $hora_f[$cont]!="7" && $hora_f[$cont]!="8" && $hora_f[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($minuto_i[$cont]!="0" && $minuto_i[$cont]!="1" && $minuto_i[$cont]!="2" && $minuto_i[$cont]!="3" && $minuto_i[$cont]!="4" && $minuto_i[$cont]!="5" && $minuto_i[$cont]!="6" && $minuto_i[$cont]!="7" && $minuto_i[$cont]!="8" && $minuto_i[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($minuto_f[$cont]!="0" && $minuto_f[$cont]!="1" && $minuto_f[$cont]!="2" && $minuto_f[$cont]!="3" && $minuto_f[$cont]!="4" && $minuto_f[$cont]!="5" && $minuto_f[$cont]!="6" && $minuto_f[$cont]!="7" && $minuto_f[$cont]!="8" && $minuto_f[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                }
                if($aux==0)
                {
                    $sql="select destinatario_agendamento_agenda.id_agendamento_evento
                    from agendamento_evento_agenda, destinatario_agendamento_agenda
                    where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                    and agendamento_evento_agenda.id_evento_agenda='$id_event'
                    and agendamento_evento_agenda.data_evento='$data_formatada'
                    and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                    and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                    and destinatario_agendamento_agenda.grupo_destinatario='$grupo_escolhido'
                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'";
                    $resp=mysql_query($sql);
                    $linha = mysql_num_rows($resp);

                    $sql1="select destinatario_agendamento_agenda.id_agendamento_evento
                    from agendamento_evento_agenda, destinatario_agendamento_agenda
                    where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                    and agendamento_evento_agenda.id_evento_agenda='$id_event'
                    and agendamento_evento_agenda.data_evento='$data_formatada'
                    and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                    and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                    and destinatario_agendamento_agenda.grupo_destinatario='$grupo_escolhido'
                    and destinatario_agendamento_agenda.id_usuario='1'";
                    $resp1=mysql_query($sql1);
                    $linha1 = mysql_num_rows($resp1);

                    if ($linha>0 && $linha1>0)
                    {
                        echo "<table border=0 width=100%>";
                        echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_EXISTE_AGENDAMENTO_IGUAL."</td></tr>";
                        echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AlterarAgendamentoGerenciamento&even=$even&agen=$agen';\"></input>";
                        echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                        echo"</table>";
                        exit;
                    }
                    else
                    {
                        $dia_hoje = date ("d");
                        $mes_hoje = date ("m");
                        $ano_hoje = date ("Y");
                        $data_hoje_formatada=$ano_hoje."-".$mes_hoje."-".$dia_hoje;

                        $dataescolhida=strtotime($data_formatada);
                        $datadehoje=strtotime($data_hoje_formatada);

                        if($dataescolhida < $datadehoje)
                        {
                            echo "<table border=0 width=100%>";
                            echo "<tr><td><img src=\"imagens/erro.gif\">";
                            echo "".A_LANG_AGENDA_ERRO_DATA."</td></tr>";
                            echo"</table>";
                        }
                        else
                        {
                            list($hora_i,$minuto_i) = explode(':',$hora_inicio);
                            if ($hora_i < 0 || $hora_i > 23 || $minuto_i < 0 || $minuto_i > 59)
                            {
                                echo "<table border=0 width=100%>";
                                echo "<tr><td><img src=\"imagens/erro.gif\">";
                                echo "".A_LANG_AGENDA_ERRO_HORARIO_INICIO."</td></tr>";
                                echo"</table>";
                            }
                            else
                            {
                                list($hora_f,$minuto_f) = explode(':',$hora_fim);

                                if ($hora_f < 0 || $hora_f > 23 || $minuto_f < 0 || $minuto_f > 59)
                                {
                                    echo "<table border=0>";
                                    echo "<tr><td><img src=\"imagens/erro.gif\">".A_LANG_AGENDA_ERRO_HORARIO_FIM."</td></tr>";
                                    echo"</table>";
                                }
                                else
                                {
                                    $horai_timestamp=mktime($hora_i,$minuto_i,00);
                                    $horaf_timestamp=mktime($hora_f,$minuto_f,00);

                                    if($horaf_timestamp < $horai_timestamp)
                                    {
                                        echo "<table border=0 width=100%>";
                                        echo "<tr><td><img src=\"imagens/erro.gif\">";
                                        echo "".A_LANG_AGENDA_ERRO_HORARIO."</td></tr>";
                                        echo"</table>";
                                    }
                                    else
                                    {
                                        $conn = &ADONewConnection($A_DB_TYPE);
                                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                        $sql="update agendamento_evento_agenda set
                                        data_evento='$data_formatada',
                                        hora_inicio_evento='$hora_inicio',
                                        hora_fim_evento='$hora_fim',
                                        local_evento='$local_evento',
                                        data_atualizacao_agendamento=NOW()
                                        where agendamento_evento_agenda.id_agendamento_evento='$agen'";
                                        $rs = $conn->Execute($sql);
                                        if ($rs == false)
                                        {
                                            echo "<table border=0 width=100%>";
                                            echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                            echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=VisualizarEvento&eve=$even';\"></input>";
                                            echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                            echo"</table>";
                                            exit;
                                        }
                                        else
                                        {   ?>
                                            <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=AgendamentoAlterado">
                                            <?
                                            //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=AgendamentoAlterado">

                                            echo"<b>Processando...</b>";

                                            $id_event=$even;
                                            $idagendamento=$agen;
                                            /*** Espa�o ***/
                                            echo "<tr><td colspan=2>&nbsp</td></tr>";

                                            /*** Tabela mostrando o evento cadastrado ***/
                                            echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

                                            /*** Evento ***/
                                            echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

                                            /*** Espa�o ***/
                                            //echo "<tr class=zebraA><td colspan=2>&nbsp</td></tr>";


                                            /*** Tipo do evento ***/
                                            $sql="select id_tipo_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            if($linha[0]==1)
                                            {
                                                $sql1="select tipo_evento_outros
                                                from evento_agenda
                                                where evento_agenda.id_evento_agenda='$even'";
                                                $resp1=mysql_query($sql1);
                                                $linha1 = mysql_fetch_row($resp1);
                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                                            }
                                            else
                                            {
                                                $sql="select nome_tipo_evento
                                                from evento_agenda, tipo_evento_agenda
                                                where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                                                and evento_agenda.id_evento_agenda='$even'";
                                                $resp=mysql_query($sql);
                                                $linha = mysql_fetch_row($resp);
                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
                                            }

                                            /*** Tipo do assunto ***/
                                            $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                                            from evento_agenda, tipo_assunto_agenda
                                            where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                                            and evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha1 = mysql_fetch_row($resp);
                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                                            //Assunto
                                            $sql="select assunto_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";



                                            /*** Nome do evento ***/
                                            $sql="select nome_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";


                                            /*** Descri��o do evento ***/
                                            $sql="select descricao_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                                            /*** Data de cria��o do evento ***/
                                            $sql="select data_criacao_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td><td>$linha[0]</td></tr>";

                                            /*** Espa�o ***/
                                            echo "<tr><td colspan=2>&nbsp</td></tr>";

                                            echo"</table></center>";
                                            
                                            exit;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    echo "<table border=0 width=100%>";
                    echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_HORA_INVALIDO."</td></tr>";
                    echo "<tr><td colspan=2>&nbsp</td></tr>";
                    echo "</table>";
                }
            }
            else
            {
                echo "<table border=0 width=100%>";
                echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_HORA_HH_MM."</td></tr>";
                echo "<tr><td colspan=2>&nbsp</td></tr>";
                echo "</table>";
            }
        }
        else
        {
            echo "<table border=0 width=100%>";
            echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_DATA_INVALIDA."</td></tr>";
            echo "<tr><td colspan=2>&nbsp</td></tr>";
            echo "</table>";
        }
    }
    else
    {
        $cont=0;
        if($data_evento==NULL)
        {
            $cont=$cont+1;
        }
        if($hora_inicio==NULL)
        {
            $cont=$cont+1;
        }
        if($hora_fim==NULL)
        {
            $cont=$cont+1;
        }
        echo "<table border=0 width=100%>";
        echo "<tr><td><img src=\"imagens/erro.gif\">&nbsp";
        if($cont==1)
        {
            echo "".A_LANG_AGENDA_ERRO_UM_CAMPO."";
            if($data_evento==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_DATA_EVENTO."";
            }
            if($hora_inicio==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_HORA_INICIO."";
            }
            if($hora_fim==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_HORA_FIM."";
            }
            echo "".A_LANG_AGENDA_ERRO_UM."</td></tr>";
        }
        else
        {
            echo "<td>".A_LANG_AGENDA_ERRO_VARIOS."<br>";
            if($data_evento==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_DATA_EVENTO."<br>";
            }
            if($hora_inicio==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_HORA_INICIO."<br>";
            }
            if($hora_fim==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_HORA_FIM."<br>";
            }
            echo "</td></tr>";
        }
        echo "<tr><td colspan=2>&nbsp</td></tr>";
    }
}




if(isset($_POST["alteraroutros"]))
{

    $data_evento=$_POST["dataevento"];
    $hora_inicio=$_POST["horainicio"];
    $hora_fim=$_POST["horafim"];
    $local_evento=$_POST["localevento"];

    /*** Destino do evento ***/
    $sql1="select DISTINCT destinatario_agendamento_agenda.grupo_destinatario
    from destinatario_agendamento_agenda
    where destinatario_agendamento_agenda.id_agendamento_evento='$agen'";
    $resp1=mysql_query($sql1);
    $linha1 = mysql_fetch_row($resp1);
    $grupo_destinatario=$linha1[0];
    
    $sql="select destinatario_agendamento_agenda.id_usuario
    from destinatario_agendamento_agenda
    where destinatario_agendamento_agenda.id_agendamento_evento='$agen'
    and destinatario_agendamento_agenda.grupo_destinatario='$grupo_destinatario'";
    $resp=mysql_query($sql);
    $quantidade=mysql_num_rows($resp);
    while($linha = mysql_fetch_array($resp))
    {
        $dest[]=$linha[0];
    }

    /*** Formatar a data ***/
    $num_letras = strlen($data_evento);

    $letra=0;
    do
    {
        if ($data_evento[$letra] == "-")
        {
            $num_primeiro_simbolo = $letra;
        }
        $letra=$letra+1;
    }while($letra<=2);

    for ($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
    {
        if ($data_evento[$letra]=="-")
        {
            $num_segundo_simbolo = $letra;
        }
    }
    for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
    {
        if($letra==0)
        {
            $dia_data=$data_evento[$letra];
        }
        else
        {
            $dia_data=$dia_data.$data_evento[$letra];
        }
    }
    for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
    {
        if($letra==$num_primeiro_simbolo + 1)
        {
            $mes_data=$data_evento[$letra];
        }
        else
        {
            $mes_data=$mes_data.$data_evento[$letra];
        }
    }
    for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
    {
        if($letra==$num_segundo_simbolo + 1)
        {
            $ano_data=$data_evento[$letra];
        }
        else
        {
            $ano_data=$ano_data.$data_evento[$letra];
        }
    }
    $data_formatada=$ano_data."-".$mes_data."-".$dia_data;

    if($data_evento!=NULL && $hora_inicio!=NULL && $hora_fim!=NULL)
    {
        if(checkdate($mes_data, $dia_data, $ano_data))
        {
            $quant_i= strlen($hora_inicio);
            $quant_f= strlen($hora_fim);
            if($quant_i==5 && $quant_f==5)
            {
                $aux=0;
                list($hora_i,$minuto_i) = explode(':',$hora_inicio);
                list($hora_f,$minuto_f) = explode(':',$hora_fim);

                for($cont=0;$cont<2;$cont++)
                {
                    if($hora_i[$cont]!="0" && $hora_i[$cont]!="1" && $hora_i[$cont]!="2" && $hora_i[$cont]!="3" && $hora_i[$cont]!="4" && $hora_i[$cont]!="5" && $hora_i[$cont]!="6" && $hora_i[$cont]!="7" && $hora_i[$cont]!="8" && $hora_i[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($hora_f[$cont]!="0" && $hora_f[$cont]!="1" && $hora_f[$cont]!="2" && $hora_f[$cont]!="3" && $hora_f[$cont]!="4" && $hora_f[$cont]!="5" && $hora_f[$cont]!="6" && $hora_f[$cont]!="7" && $hora_f[$cont]!="8" && $hora_f[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($minuto_i[$cont]!="0" && $minuto_i[$cont]!="1" && $minuto_i[$cont]!="2" && $minuto_i[$cont]!="3" && $minuto_i[$cont]!="4" && $minuto_i[$cont]!="5" && $minuto_i[$cont]!="6" && $minuto_i[$cont]!="7" && $minuto_i[$cont]!="8" && $minuto_i[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($minuto_f[$cont]!="0" && $minuto_f[$cont]!="1" && $minuto_f[$cont]!="2" && $minuto_f[$cont]!="3" && $minuto_f[$cont]!="4" && $minuto_f[$cont]!="5" && $minuto_f[$cont]!="6" && $minuto_f[$cont]!="7" && $minuto_f[$cont]!="8" && $minuto_f[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                }
                if($aux==0)
                {
                    $dia_hoje = date ("d");
                    $mes_hoje = date ("m");
                    $ano_hoje = date ("Y");
                    $data_hoje_formatada=$ano_hoje."-".$mes_hoje."-".$dia_hoje;

                    $dataescolhida=strtotime($data_formatada);
                    $datadehoje=strtotime($data_hoje_formatada);

                    if($dataescolhida >= $datadehoje)
                    {
                        list($hora_i,$minuto_i) = explode(':',$hora_inicio);
                        if ($hora_i >= 0 && $hora_i <= 23 && $minuto_i >= 0 && $minuto_i <= 59)
                        {
                            list($hora_f,$minuto_f) = explode(':',$hora_fim);
                            if ($hora_f >= 0 && $hora_f <= 23 && $minuto_f >= 0 && $minuto_f <= 59)
                            {
                                $horai_timestamp=mktime($hora_i,$minuto_i,00);
                                $horaf_timestamp=mktime($hora_f,$minuto_f,00);
                                if($horaf_timestamp >= $horai_timestamp)
                                {
                                    for($cont=0; $cont <$quantidade; $cont++)
                                    {
                                        $sql1="select destinatario_agendamento_agenda.id_usuario
                                        from agendamento_evento_agenda, destinatario_agendamento_agenda
                                        where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                        and agendamento_evento_agenda.id_evento_agenda='$even'
                                        and agendamento_evento_agenda.data_evento='$data_formatada'
                                        and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                        and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                        and destinatario_agendamento_agenda.grupo_destinatario='$grupo_destinatario'
                                        and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'";
                                        $resp1=mysql_query($sql1);
                                        $linha1 = mysql_num_rows($resp1);
                                        if($linha1!=0)
                                        {
                                            $aux1[$cont]=mysql_fetch_row($resp1);
                                        }
                                    }
                                    $quant1=count($aux1);

                                    if($quant1==0)
                                    {
                                        $conn = &ADONewConnection($A_DB_TYPE);
                                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                        $sql="update agendamento_evento_agenda set
                                        data_evento='$data_formatada',
                                        hora_inicio_evento='$hora_inicio',
                                        hora_fim_evento='$hora_fim',
                                        local_evento='$local_evento',
                                        data_atualizacao_agendamento=NOW()
                                        where agendamento_evento_agenda.id_agendamento_evento='$agen'";
                                        $rs = $conn->Execute($sql);
                                        if ($rs == false)
                                        {
                                            echo "<table border=0 width=100%>";
                                            echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                            echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=VisualizarEvento&eve=$even';\"></input>";
                                            echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                            echo"</table>";
                                            exit;
                                        }
                                        else
                                        {
                                            ?>
                                            <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=AgendamentoAlterado">
                                            <?
                                            //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=AgendamentoAlterado">

                                            echo"<b>Processando...</b>";

                                            $id_event=$even;
                                            $idagendamento=$agen;
                                            /*** Espa�o ***/
                                            echo "<tr><td colspan=2>&nbsp</td></tr>";

                                            /*** Tabela mostrando o evento cadastrado ***/
                                            echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

                                            /*** Evento ***/
                                            echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

                                            /*** Espa�o ***/
                                            //echo "<tr class=zebraA><td colspan=2>&nbsp</td></tr>";


                                            /*** Tipo do evento ***/
                                            $sql="select id_tipo_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            if($linha[0]==1)
                                            {
                                                $sql1="select tipo_evento_outros
                                                from evento_agenda
                                                where evento_agenda.id_evento_agenda='$even'";
                                                $resp1=mysql_query($sql1);
                                                $linha1 = mysql_fetch_row($resp1);
                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                                            }
                                            else
                                            {
                                                $sql="select nome_tipo_evento
                                                from evento_agenda, tipo_evento_agenda
                                                where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                                                and evento_agenda.id_evento_agenda='$even'";
                                                $resp=mysql_query($sql);
                                                $linha = mysql_fetch_row($resp);
                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
                                            }

                                            /*** Tipo do assunto ***/
                                            $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                                            from evento_agenda, tipo_assunto_agenda
                                            where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                                            and evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha1 = mysql_fetch_row($resp);
                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                                            //Assunto
                                            $sql="select assunto_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";



                                            /*** Nome do evento ***/
                                            $sql="select nome_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";


                                            /*** Descri��o do evento ***/
                                            $sql="select descricao_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                                            /*** Data de cria��o do evento ***/
                                            $sql="select data_criacao_evento
                                            from evento_agenda
                                            where evento_agenda.id_evento_agenda='$even'";
                                            $resp=mysql_query($sql);
                                            $linha = mysql_fetch_row($resp);
                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td><td>$linha[0]</td></tr>";

                                            /*** Espa�o ***/
                                            echo "<tr><td colspan=2>&nbsp</td></tr>";

                                            echo"</table></center>";

                                            exit;
                                        }
                                    }
                                    else
                                    {
                                        if ($quant1==1)
                                        {
                                            echo"<tr><td><img src=\"imagens/erro.gif\">&nbsp".A_LANG_AGENDA_JA_AGENDADO_PARA_ESTE_DESTINATARIO."</td></tr>";
                                            for($cont=0; $cont <$quantidade; $cont++)
                                            {
                                                $sql3="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario, usuario.email_usuario
                                                from destinatario_agendamento_agenda, usuario, agendamento_evento_agenda
                                                where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                                                and agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                and agendamento_evento_agenda.id_evento_agenda='$even'
                                                and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'
                                                and agendamento_evento_agenda.data_evento='$data_formatada'
                                                and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                and destinatario_agendamento_agenda.grupo_destinatario='$grupo_destinatario'";
                                                $resp3=mysql_query($sql3);
                                                while($aux= mysql_fetch_array($resp3))
                                                {
                                                    echo"<tr><td>- $aux[1] ($aux[2])</td></tr>";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if($quant1>1)
                                            {
                                                echo"<tr><td><img src=\"imagens/erro.gif\">&nbsp".A_LANG_AGENDA_JA_AGENDADO_PARA_ESTES_DESTINATARIOS."</td></tr>";
                                                for($cont=0; $cont <$quantidade; $cont++)
                                                {
                                                    $sql3="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario, usuario.email_usuario
                                                    from destinatario_agendamento_agenda, usuario, agendamento_evento_agenda
                                                    where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                                                    and agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                    and agendamento_evento_agenda.id_evento_agenda='$even'
                                                    and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'
                                                    and agendamento_evento_agenda.data_evento='$data_formatada'
                                                    and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                    and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                    and destinatario_agendamento_agenda.grupo_destinatario='$grupo_destinatario'";
                                                    $resp3=mysql_query($sql3);
                                                    while($aux= mysql_fetch_array($resp3))
                                                    {
                                                        echo"<tr><td>- $aux[1] ($aux[2])</td></tr>";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    echo "<table border=0 width=100%>";
                                    echo "<tr><td><img src=\"imagens/erro.gif\">";
                                    echo "".A_LANG_AGENDA_ERRO_HORARIO."</td></tr>";
                                    echo "<tr><td colspan=2>&nbsp</td></tr>";
                                    echo"</table>";
                                }
                            }
                            else
                            {
                                echo "<table border=0>";
                                echo "<tr><td><img src=\"imagens/erro.gif\">".A_LANG_AGENDA_ERRO_HORARIO_FIM."</td></tr>";
                                echo "<tr><td colspan=2>&nbsp</td></tr>";
                                echo"</table>";
                            }
                        }
                        else
                        {
                            echo "<table border=0 width=100%>";
                            echo "<tr><td><img src=\"imagens/erro.gif\">";
                            echo "".A_LANG_AGENDA_ERRO_HORARIO_INICIO."</td></tr>";
                            echo "<tr><td colspan=2>&nbsp</td></tr>";
                            echo"</table>";
                        }
                    }
                    else
                    {
                        echo "<table border=0 width=100%>";
                        echo "<tr><td><img src=\"imagens/erro.gif\">";
                        echo "".A_LANG_AGENDA_ERRO_DATA."</td></tr>";
                        echo "<tr><td colspan=2>&nbsp</td></tr>";
                        echo"</table>";
                    }
                }
                else
                {
                    echo "<table border=0 width=100%>";
                    echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_HORA_INVALIDO."</td></tr>";
                    echo "<tr><td colspan=2>&nbsp</td></tr>";
                    echo "</table>";
                }
            }
            else
            {
                echo "<table border=0 width=100%>";
                echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_HORA_HH_MM."</td></tr>";
                echo "<tr><td colspan=2>&nbsp</td></tr>";
                echo "</table>";
            }
        }
        else
        {
            echo "<table border=0 width=100%>";
            echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_DATA_INVALIDA."</td></tr>";
            echo "<tr><td colspan=2>&nbsp</td></tr>";
            echo "</table>";
        }
    }
    else
    {
        $cont=0;
        if($data_evento==NULL)
        {
            $cont=$cont+1;
        }
        if($hora_inicio==NULL)
        {
            $cont=$cont+1;
        }
        if($hora_fim==NULL)
        {
            $cont=$cont+1;
        }

        echo "<table border=0 width=100%>";
        echo "<tr><td><img src=\"imagens/erro.gif\">&nbsp";
        if($cont==1)
        {
            echo "".A_LANG_AGENDA_ERRO_UM_CAMPO."";
            if($data_evento==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_DATA_EVENTO."";
            }
            if($hora_inicio==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_HORA_INICIO."";
            }
            if($hora_fim==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_HORA_FIM."";
            }
            echo "".A_LANG_AGENDA_ERRO_UM."</td></tr>";
        }
        else
        {
            echo "<td>".A_LANG_AGENDA_ERRO_VARIOS."<br>";
            if($data_evento==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_DATA_EVENTO."<br>";
            }
            if($hora_inicio==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_HORA_INICIO."<br>";
            }
            if($hora_fim==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_HORA_FIM."<br>";
            }
            echo "</td></tr>";
        }
        echo "<tr><td colspan=2>&nbsp</td></tr>";
    }
}



/*** Tabela mostrando o evento cadastrado ***/
echo "<center><table border=0 cellspacing=1 cellpadding=5>";


/*** Evento ***/
echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

/*** Espa�o ***/
//echo "<tr><td colspan=2>&nbsp</td></tr>";


/*** Tipo do evento ***/
$sql="select nome_tipo_evento
from evento_agenda, tipo_evento_agenda
where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
and evento_agenda.id_evento_agenda='$even'";
$resp=mysql_query($sql);
$linha = mysql_fetch_row($resp);
echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";


/*** Tipo do assunto ***/
$sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
from evento_agenda, tipo_assunto_agenda
where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
and evento_agenda.id_evento_agenda='$even'";
$resp=mysql_query($sql);
$linha1 = mysql_fetch_row($resp);
echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

//Assunto
$sql="select assunto_evento
from evento_agenda
where evento_agenda.id_evento_agenda='$even'";
$resp=mysql_query($sql);
$linha = mysql_fetch_row($resp);
echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";


/*** Nome do evento ***/
$sql="select nome_evento
from evento_agenda
where evento_agenda.id_evento_agenda='$even'";
$resp=mysql_query($sql);
$linha = mysql_fetch_row($resp);
echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";


/*** Descri��o do evento ***/
$sql="select descricao_evento
from evento_agenda
where evento_agenda.id_evento_agenda='$even'";
$resp=mysql_query($sql);
$linha = mysql_fetch_row($resp);
echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";


echo "</table>";//Encerra tabela mostrando o evento cadastrado




/*** Destino do evento ***/
$sql1="select DISTINCT destinatario_agendamento_agenda.grupo_destinatario
from destinatario_agendamento_agenda
where destinatario_agendamento_agenda.id_agendamento_evento='$agen'";
$resp1=mysql_query($sql1);
$linha1 = mysql_fetch_row($resp1);
$gr=$linha1[0];




if($gr=="Minha Agenda")
{
    echo"<center><table>";
    /*** Espa�o ***/
    echo "<tr><td colspan=2>&nbsp</td></tr>";
    echo "<tr><td colspan=2>&nbsp</td></tr>";
    /*** Minha Agenda ***/
    echo "<tr><td colspan=2><b>".$gr."</b></td></tr>";
    echo"</table></center>";
    
    ?>
    <!--- Inicia formul�rio --->
    <form name="formulariominhaagenda" enctype="multipart/form-data" action="n_index_agenda.php?opcao=AlterarAgendamentoGerenciamento&even=<? echo $even;?>&agen=<? echo $agen; ?>" method="post">
    <?


    /*** Tabela de cadastro ***/
    echo "<table border=0>";
    
    /*** Espa�os ***/
    //echo "<tr><td colspan=2>&nbsp</td></tr>";

    /*** Data do evento ***/
    $sql1="select agendamento_evento_agenda.data_evento
    from agendamento_evento_agenda
    where agendamento_evento_agenda.id_evento_agenda='$even'
    and agendamento_evento_agenda.id_agendamento_evento='$agen'";
    $resp1=mysql_query($sql1);
    $linha1 = mysql_fetch_row($resp1);

    $data_event=$linha1[0];
    $num_letras = strlen($data_event);

    $letra=0;
    do
    {
        if ($data_event[$letra] == "-")
        {
            $num_primeiro_simbolo = $letra;
        }
        $letra=$letra+1;
    }while($letra<=4);

    for ($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
    {
        if ($data_event[$letra]=="-")
        {
            $num_segundo_simbolo = $letra;
        }
    }

    for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
    {
        if($letra==0)
        {
            $ano_data=$data_event[$letra];
        }
        else
        {
            $ano_data=$ano_data.$data_event[$letra];
        }
    }
    for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
    {
        if($letra==$num_primeiro_simbolo + 1)
        {
            $mes_data=$data_event[$letra];
        }
        else
        {
            $mes_data=$mes_data.$data_event[$letra];
        }
    }
    for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
    {
        if($letra==$num_segundo_simbolo + 1)
        {
            $dia_data=$data_event[$letra];
        }
        else
        {
            $dia_data=$dia_data.$data_event[$letra];
        }
    }
    $data_formatada=$dia_data."-".$mes_data."-".$ano_data;

    echo "<tr><td>".A_LANG_AGENDA_DATA_EVENTO."*:</td>";
    if(isset($_POST["alterarminhaagenda"]))
    {
        $aux_data_evento=$data_evento;
    }
    else
    {
        $aux_data_evento=$data_formatada;
    }
    echo "<td><input class=text type=text size=9 name=dataevento id=demo1 value=\"".$aux_data_evento."\"><a href=\"javascript:NewCal('demo1','ddmmyyyy')\">";
    echo "<img src='agenda/cal.gif' width=16 height=16 border=0 alt='Escolher a data'></a></td></tr>";

    ?>
    <script>
    function horaConta(c)
    {
        if(c.value.length==2)
        {
            c.value += ':';
        }
    }
    </script>
    <?


    /*** Hora in�cio ***/
    $sql1="select agendamento_evento_agenda.hora_inicio_evento
    from agendamento_evento_agenda
    where agendamento_evento_agenda.id_evento_agenda='$even'
    and agendamento_evento_agenda.id_agendamento_evento='$agen'";
    $resp1=mysql_query($sql1);
    $linha1 = mysql_fetch_row($resp1);
    $hori=$linha1[0];
    for ($letra=0; $letra < 5; $letra++)
    {
        if($letra==0)
        {
            $horaini=$hori[$letra];
        }
        else
        {
            $horaini=$horaini.$hori[$letra];
        }
    }

    if(isset($_POST["alterarminhaagenda"]))
    {
        $aux_hora_inicio=$hora_inicio;
    }
    else
    {
        $aux_hora_inicio=$horaini;
    }
    /*** Hora in�cio ***/
    echo"<tr><td>".A_LANG_AGENDA_HORA_INICIO."*:</td><td><input class=text type=text size=3 name=horainicio onkeyup=horaConta(this); value=\"".$aux_hora_inicio."\"></td></tr>";




    /*** Hora fim ***/
    $sql1="select agendamento_evento_agenda.hora_fim_evento
    from agendamento_evento_agenda
    where agendamento_evento_agenda.id_evento_agenda='$even'
    and agendamento_evento_agenda.id_agendamento_evento='$agen'";
    $resp1=mysql_query($sql1);
    $linha1 = mysql_fetch_row($resp1);
    $horf=$linha1[0];
    for ($letra=0; $letra < 5; $letra++)
    {
        if($letra==0)
        {
            $horafi=$horf[$letra];
        }
        else
        {
            $horafi=$horafi.$horf[$letra];
        }
    }

    if(isset($_POST["alterarminhaagenda"]))
    {
        $aux_hora_fim=$hora_fim;
    }
    else
    {
        $aux_hora_fim=$horafi;
    }
    /*** Hora fim ***/
    echo "<tr><td>".A_LANG_AGENDA_HORA_FIM."*:</td><td><input class=text type=text size=3 name=horafim onkeyup=horaConta(this); value=\"".$aux_hora_fim."\">";

    /*** Local Evento ***/
    $sql1="select agendamento_evento_agenda.local_evento
    from agendamento_evento_agenda
    where agendamento_evento_agenda.id_evento_agenda='$even'
    and agendamento_evento_agenda.id_agendamento_evento='$agen'";
    $resp1=mysql_query($sql1);
    $linha1 = mysql_fetch_row($resp1);
    if(isset($_POST["alterarminhaagenda"]))
    {
        $aux_local_evento=$local_evento;
    }
    else
    {
        $aux_local_evento=$linha1[0];
    }
    /*** Local do evento ***/
    echo "<tr><td>".A_LANG_AGENDA_LOCAL_EVENTO.":</td><td><textarea name=localevento class=button cols=70 rows=3>$aux_local_evento</textarea></td></tr>";

    /*** Repeti��o ***/
    echo "<tr><td disabled>".A_LANG_AGENDA_REPETICAO_EVENTO.":</td>";
    echo"<td><input type=radio name=naorepetir value='naorepetir' disabled> ".A_LANG_AGENDA_NAO_REPETIR."";
    echo"<input type=radio name=diariamente value='diariamente' disabled> ".A_LANG_AGENDA_DIARIAMENTE."";
    echo"<input type=radio name=semanalmente value='semanalmente' disabled> ".A_LANG_AGENDA_SEMANALMENTE."";
    echo"<input type=radio name=mensalmente value='mensalmente' disabled> ".A_LANG_AGENDA_MENSALMENTE."";
    echo"<input type=radio name=Anualmente value='anualmente' disabled> ".A_LANG_AGENDA_ANUALMENTE."";
    echo"</input> ";
    echo"<select name=quantidadevezes class=select size=1 disabled>";
    echo "<option selected value=\"0\">".A_LANG_AGENDA_QUANTIDADE_REPETICOES."</option>";
    echo "<option value=\"1\">".A_LANG_AGENDA_1X."</option>";
    echo "<option value=\"2\">".A_LANG_AGENDA_2X."</option>";
    echo "<option value=\"3\">".A_LANG_AGENDA_3X."</option>";
    echo "<option value=\"4\">".A_LANG_AGENDA_4X."</option>";
    echo "<option value=\"5\">".A_LANG_AGENDA_5X."</option>";
    echo "<option value=\"6\">".A_LANG_AGENDA_6X."</option>";
    echo "<option value=\"7\">".A_LANG_AGENDA_7X."</option>";
    echo "<option value=\"8\">".A_LANG_AGENDA_8X."</option>";
    echo "<option value=\"9\">".A_LANG_AGENDA_9X."</option>";
    echo "<option value=\"10\">".A_LANG_AGENDA_10X."</option>";
    echo"</select> ";
    echo"".A_LANG_AGENDA_OU."</td></tr>";

    echo"<tr><td></td><td disabled>".A_LANG_AGENDA_DATA_REPETI��O.": <input class=button size=9 name=datarepeticao id=demo1 value=\"".$data_repeticao."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\" disabled></td></tr>";

    /*** E-mail de Aviso ***/
    echo "<tr><td disabled>".A_LANG_AGENDA_AVISO_EMAIL.":</td>";
    echo"<td><input type=radio name=naoenviar value='naoenviar' disabled> ".A_LANG_AGENDA_NAO_ENVIAR."";
    echo"<input type=radio name=dia value='dia' disabled> ".A_LANG_AGENDA_AVISO_UM_DIA."";
    echo"<input type=radio name=semana value='semana' disabled> ".A_LANG_AGENDA_AVISO_UMA_SEMANA."";
    echo"<input type=radio name=mes value='mes' disabled> ".A_LANG_AGENDA_AVISO_UM_MES."";
    echo"</input> ";
    echo"".A_LANG_AGENDA_OU."</td></tr>";

    echo"<tr><td></td><td disabled>".A_LANG_AGENDA_DATA_ENVIO_AVISO.": <input class=button size=9 name=dataaviso id=demo1 value=\"".$data_aviso."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\" disabled></td></tr>";

    /*** Espa�o ***/
    echo "<tr><td colspan=2>&nbsp</td></tr>";

    echo "</table>";//Encerra tabela de cadastro

    /*** Bot�es cadastrar e cancelar ***/
    echo "<tr><td colspan=2 align=left>";
    echo "<input name=alterarminhaagenda id=bot1 class=button type=submit value=\"".A_LANG_AGENDA_ALTERAR."\"></input>";
    echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=VisualizarEvento&eve=$even';\"></input>";
    echo "</td></tr>";

    echo "</form>";//Encerra formul�rio

    /*** Espa�o ***/
    echo "<tr><td colspan=2>&nbsp</td></tr>";

    /*** Observa��o ***/
    echo"<tr><td colspan=2><b>".A_LANG_AGENDA_OBS_CAMPO_INCOMPLETO."</b></td></tr>";
}
else
{
    if($gr=="Root")
    {
        echo"<center><table>";

        /*** Espa�o ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";
        echo "<tr><td colspan=2>&nbsp</td></tr>";

        /*** Root ***/
        echo "<tr><td colspan=2 align=center><b>$gr</b></td></tr>";


        echo"</table></center>";
    
        ?>
        <!--- Inicia formul�rio --->
        <form name="formularioagendamentoroot" action="n_index_agenda.php?opcao=AlterarAgendamentoGerenciamento&even=<? echo $even;?>&agen=<? echo $agen; ?>" method="post">
        <?

        /*** Tabela de cadastro ***/
        echo "<table border=0>";

        /*** Espa�os ***/
        //echo "<tr><td colspan=2>&nbsp</td></tr>";

        /*** Data do evento ***/
        $sql1="select agendamento_evento_agenda.data_evento
        from agendamento_evento_agenda
        where agendamento_evento_agenda.id_evento_agenda='$even'
        and agendamento_evento_agenda.id_agendamento_evento='$agen'";
        $resp1=mysql_query($sql1);
        $linha1 = mysql_fetch_row($resp1);

        $data_event=$linha1[0];
        $num_letras = strlen($data_event);

        $letra=0;
        do
        {
            if ($data_event[$letra] == "-")
            {
                $num_primeiro_simbolo = $letra;
            }
            $letra=$letra+1;
        }while($letra<=4);

        for ($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
        {
            if ($data_event[$letra]=="-")
            {
                $num_segundo_simbolo = $letra;
            }
        }

        for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
        {
            if($letra==0)
            {
                $ano_data=$data_event[$letra];
            }
            else
            {
                $ano_data=$ano_data.$data_event[$letra];
            }
        }
        for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
        {
            if($letra==$num_primeiro_simbolo + 1)
            {
                $mes_data=$data_event[$letra];
            }
            else
            {
                $mes_data=$mes_data.$data_event[$letra];
            }
        }
        for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
        {
            if($letra==$num_segundo_simbolo + 1)
            {
                $dia_data=$data_event[$letra];
            }
            else
            {
                $dia_data=$dia_data.$data_event[$letra];
            }
        }
        $data_formatada=$dia_data."-".$mes_data."-".$ano_data;

        echo "<tr><td>".A_LANG_AGENDA_DATA_EVENTO."*:</td>";
        if(isset($_POST["alterarroot"]))
        {
            $aux_data_evento=$data_evento;
        }
        else
        {
            $aux_data_evento=$data_formatada;
        }
        echo "<td><input class=text type=text size=9 name=dataevento id=demo1 value=\"".$aux_data_evento."\"><a href=\"javascript:NewCal('demo1','ddmmyyyy')\">";
        echo "<img src='agenda/cal.gif' width=16 height=16 border=0 alt='Escolher a data'></a></td></tr>";

        ?>
        <script>
        function horaConta(c)
        {
            if(c.value.length==2)
            {
                c.value += ':';
            }
        }
        </script>
        <?


        /*** Hora in�cio ***/
        $sql1="select agendamento_evento_agenda.hora_inicio_evento
        from agendamento_evento_agenda
        where agendamento_evento_agenda.id_evento_agenda='$even'
        and agendamento_evento_agenda.id_agendamento_evento='$agen'";
        $resp1=mysql_query($sql1);
        $linha1 = mysql_fetch_row($resp1);
        $hori=$linha1[0];
        for ($letra=0; $letra < 5; $letra++)
        {
            if($letra==0)
            {
                $horaini=$hori[$letra];
            }
            else
            {
                $horaini=$horaini.$hori[$letra];
            }
        }

        if(isset($_POST["alterarroot"]))
        {
            $aux_hora_inicio=$hora_inicio;
        }
        else
        {
            $aux_hora_inicio=$horaini;
        }
        /*** Hora in�cio ***/
        echo"<tr><td>".A_LANG_AGENDA_HORA_INICIO."*:</td><td><input class=text type=text size=3 name=horainicio onkeyup=horaConta(this); value=\"".$aux_hora_inicio."\"></td></tr>";




        /*** Hora fim ***/

        $sql1="select agendamento_evento_agenda.hora_fim_evento
        from agendamento_evento_agenda
        where agendamento_evento_agenda.id_evento_agenda='$even'
        and agendamento_evento_agenda.id_agendamento_evento='$agen'";
        $resp1=mysql_query($sql1);
        $linha1 = mysql_fetch_row($resp1);
        $horf=$linha1[0];
        for ($letra=0; $letra < 5; $letra++)
        {
            if($letra==0)
            {
                $horafi=$horf[$letra];
            }
            else
            {
                $horafi=$horafi.$horf[$letra];
            }
        }

        if(isset($_POST["alterarroot"]))
        {
            $aux_hora_fim=$hora_fim;
        }
        else
        {
            $aux_hora_fim=$horafi;
        }
        /*** Hora fim ***/
        echo "<tr><td>".A_LANG_AGENDA_HORA_FIM."*:</td><td><input class=text type=text size=3 name=horafim onkeyup=horaConta(this); value=\"".$aux_hora_fim."\">";

        /*** Local Evento ***/
        $sql1="select agendamento_evento_agenda.local_evento
        from agendamento_evento_agenda
        where agendamento_evento_agenda.id_evento_agenda='$even'
        and agendamento_evento_agenda.id_agendamento_evento='$agen'";
        $resp1=mysql_query($sql1);
        $linha1 = mysql_fetch_row($resp1);
        if(isset($_POST["alterarroot"]))
        {
            $aux_local_evento=$local_evento;
        }
        else
        {
            $aux_local_evento=$linha1[0];
        }
        /*** Local do evento ***/
        echo "<tr><td>".A_LANG_AGENDA_LOCAL_EVENTO.":</td><td><textarea name=localevento class=button cols=70 rows=3>$aux_local_evento</textarea></td></tr>";

        /*** Repeti��o ***/
        echo "<tr><td disabled>".A_LANG_AGENDA_REPETICAO_EVENTO.":</td>";
        echo"<td><input type=radio name=naorepetir value='naorepetir' disabled> ".A_LANG_AGENDA_NAO_REPETIR."";
        echo"<input type=radio name=diariamente value='diariamente' disabled> ".A_LANG_AGENDA_DIARIAMENTE."";
        echo"<input type=radio name=semanalmente value='semanalmente' disabled> ".A_LANG_AGENDA_SEMANALMENTE."";
        echo"<input type=radio name=mensalmente value='mensalmente' disabled> ".A_LANG_AGENDA_MENSALMENTE."";
        echo"<input type=radio name=Anualmente value='anualmente' disabled> ".A_LANG_AGENDA_ANUALMENTE."";
        echo"</input> ";
        echo"<select name=quantidadevezes class=select size=1 disabled>";
        echo "<option selected value=\"0\">".A_LANG_AGENDA_QUANTIDADE_REPETICOES."</option>";
        echo "<option value=\"1\">".A_LANG_AGENDA_1X."</option>";
        echo "<option value=\"2\">".A_LANG_AGENDA_2X."</option>";
        echo "<option value=\"3\">".A_LANG_AGENDA_3X."</option>";
        echo "<option value=\"4\">".A_LANG_AGENDA_4X."</option>";
        echo "<option value=\"5\">".A_LANG_AGENDA_5X."</option>";
        echo "<option value=\"6\">".A_LANG_AGENDA_6X."</option>";
        echo "<option value=\"7\">".A_LANG_AGENDA_7X."</option>";
        echo "<option value=\"8\">".A_LANG_AGENDA_8X."</option>";
        echo "<option value=\"9\">".A_LANG_AGENDA_9X."</option>";
        echo "<option value=\"10\">".A_LANG_AGENDA_10X."</option>";
        echo"</select> ";
        echo"".A_LANG_AGENDA_OU."</td></tr>";

        echo"<tr><td></td><td disabled>".A_LANG_AGENDA_DATA_REPETI��O.": <input class=button size=9 name=datarepeticao id=demo1 value=\"".$data_repeticao."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\" disabled></td></tr>";

        /*** E-mail de Aviso ***/
        echo "<tr><td disabled>".A_LANG_AGENDA_AVISO_EMAIL.":</td>";
        echo"<td><input type=radio name=naoenviar value='naoenviar' disabled> ".A_LANG_AGENDA_NAO_ENVIAR."";
        echo"<input type=radio name=dia value='dia' disabled> ".A_LANG_AGENDA_AVISO_UM_DIA."";
        echo"<input type=radio name=semana value='semana' disabled> ".A_LANG_AGENDA_AVISO_UMA_SEMANA."";
        echo"<input type=radio name=mes value='mes' disabled> ".A_LANG_AGENDA_AVISO_UM_MES."";
        echo"</input> ";
        echo"".A_LANG_AGENDA_OU."</td></tr>";

        echo"<tr><td></td><td disabled>".A_LANG_AGENDA_DATA_ENVIO_AVISO.": <input class=button size=9 name=dataaviso id=demo1 value=\"".$data_aviso."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\" disabled></td></tr>";

        /*** Espa�o ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";

        echo "</table>";//Encerra tabela de cadastro

        /*** Bot�es cadastrar e cancelar ***/
        echo "<tr><td colspan=2 align=left>";
        echo "<input name=alterarroot id=bot1 class=button type=submit value=\"".A_LANG_AGENDA_ALTERAR."\"></input>";
        echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=VisualizarEvento&eve=$even';\"></input>";
        echo "</td></tr>";

        echo "</form>";//Encerra formul�rio

        /*** Espa�o ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";


        /*** Observa��o ***/
        echo"<tr><td colspan=2><b>".A_LANG_AGENDA_OBS_CAMPO_INCOMPLETO."</b></td></tr>";
    
    }
    else
    {
        if($gr!="Minha Agenda" && $gr!="Root")
        {
       
            echo"<center><table border=0 cellspacing=1 cellpadding=5>";

            /*** Espa�o ***/
            echo "<tr><td colspan=2>&nbsp</td></tr>";
            /*** Espa�o ***/
            echo "<tr><td colspan=2>&nbsp</td></tr>";

            /*** Outros ***/
            echo "<tr class=zebraB><td colspan=2 align=center><b>$gr</b></td></tr>";
        
            $sql9="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario
            from usuario, destinatario_agendamento_agenda
            where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
            and destinatario_agendamento_agenda.id_agendamento_evento='$agen'
            and destinatario_agendamento_agenda.grupo_destinatario='$gr'
            order by nome_usuario";
            $resp9=mysql_query($sql9);
            while ($array = mysql_fetch_array($resp9))
            {
                echo "<tr class=zebraA><td>- ".$array[1]."</td></tr>";
            }

            echo"</table></center>";
        
            ?>
            <!--- Inicia formul�rio --->
            <form name="formularioagendamentooutros" action="n_index_agenda.php?opcao=AlterarAgendamentoGerenciamento&even=<? echo $even;?>&agen=<? echo $agen; ?>" method="post">
            <?

            /*** Tabela de cadastro ***/
            echo "<table border=0>";

            /*** Espa�os ***/
            //echo "<tr><td colspan=2>&nbsp</td></tr>";

            /*** Data do evento ***/
            $sql1="select agendamento_evento_agenda.data_evento
            from agendamento_evento_agenda
            where agendamento_evento_agenda.id_evento_agenda='$even'
            and agendamento_evento_agenda.id_agendamento_evento='$agen'";
            $resp1=mysql_query($sql1);
            $linha1 = mysql_fetch_row($resp1);

            $data_event=$linha1[0];
            $num_letras = strlen($data_event);

            $letra=0;
            do
            {
                if ($data_event[$letra] == "-")
                {
                    $num_primeiro_simbolo = $letra;
                }
                $letra=$letra+1;
            }while($letra<=4);

            for ($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
            {
                if ($data_event[$letra]=="-")
                {
                    $num_segundo_simbolo = $letra;
                }
            }

            for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
            {
                if($letra==0)
                {
                    $ano_data=$data_event[$letra];
                }
                else
                {
                    $ano_data=$ano_data.$data_event[$letra];
                }
            }
            for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
            {
                if($letra==$num_primeiro_simbolo + 1)
                {
                    $mes_data=$data_event[$letra];
                }
                else
                {
                    $mes_data=$mes_data.$data_event[$letra];
                }
            }
            for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
            {
                if($letra==$num_segundo_simbolo + 1)
                {
                    $dia_data=$data_event[$letra];
                }
                else
                {
                    $dia_data=$dia_data.$data_event[$letra];
                }
            }
            $data_formatada=$dia_data."-".$mes_data."-".$ano_data;

            echo "<tr><td>".A_LANG_AGENDA_DATA_EVENTO."*:</td>";
            if(isset($_POST["alteraroutros"]))
            {
                $aux_data_evento=$data_evento;
            }
            else
            {
                $aux_data_evento=$data_formatada;
            }
            echo "<td><input class=text type=text size=9 name=dataevento id=demo1 value=\"".$aux_data_evento."\"><a href=\"javascript:NewCal('demo1','ddmmyyyy')\">";
            echo "<img src='agenda/cal.gif' width=16 height=16 border=0 alt='Escolher a data'></a></td></tr>";

            ?>
            <script>
            function horaConta(c)
            {
                if(c.value.length==2)
                {
                    c.value += ':';
                }
            }
            </script>
            <?


            /*** Hora in�cio ***/
            $sql1="select agendamento_evento_agenda.hora_inicio_evento
            from agendamento_evento_agenda
            where agendamento_evento_agenda.id_evento_agenda='$even'
            and agendamento_evento_agenda.id_agendamento_evento='$agen'";
            $resp1=mysql_query($sql1);
            $linha1 = mysql_fetch_row($resp1);
            $hori=$linha1[0];
            for ($letra=0; $letra < 5; $letra++)
            {
                if($letra==0)
                {
                    $horaini=$hori[$letra];
                }
                else
                {
                    $horaini=$horaini.$hori[$letra];
                }
            }

            if(isset($_POST["alteraroutros"]))
            {
                $aux_hora_inicio=$hora_inicio;
            }
            else
            {
                $aux_hora_inicio=$horaini;
            }
            /*** Hora in�cio ***/
            echo"<tr><td>".A_LANG_AGENDA_HORA_INICIO."*:</td><td><input class=text type=text size=3 name=horainicio onkeyup=horaConta(this); value=\"".$aux_hora_inicio."\"></td></tr>";




            /*** Hora fim ***/
            $sql1="select agendamento_evento_agenda.hora_fim_evento
            from agendamento_evento_agenda
            where agendamento_evento_agenda.id_evento_agenda='$even'
            and agendamento_evento_agenda.id_agendamento_evento='$agen'";
            $resp1=mysql_query($sql1);
            $linha1 = mysql_fetch_row($resp1);
            $horf=$linha1[0];
            for ($letra=0; $letra < 5; $letra++)
            {
                if($letra==0)
                {
                    $horafi=$horf[$letra];
                }
                else
                {
                    $horafi=$horafi.$horf[$letra];
                }
            }

            if(isset($_POST["alteraroutros"]))
            {
                $aux_hora_fim=$hora_fim;
            }
            else
            {
                $aux_hora_fim=$horafi;
            }
            /*** Hora fim ***/
            echo "<tr><td>".A_LANG_AGENDA_HORA_FIM."*:</td><td><input class=text type=text size=3 name=horafim onkeyup=horaConta(this); value=\"".$aux_hora_fim."\">";

            /*** Local Evento ***/
            $sql1="select agendamento_evento_agenda.local_evento
            from agendamento_evento_agenda
            where agendamento_evento_agenda.id_evento_agenda='$even'
            and agendamento_evento_agenda.id_agendamento_evento='$agen'";
            $resp1=mysql_query($sql1);
            $linha1 = mysql_fetch_row($resp1);
            if(isset($_POST["alteraroutros"]))
            {
                $aux_local_evento=$local_evento;
            }
            else
            {
                $aux_local_evento=$linha1[0];
            }
            /*** Local do evento ***/
            echo "<tr><td>".A_LANG_AGENDA_LOCAL_EVENTO.":</td><td><textarea name=localevento class=button cols=70 rows=3>$aux_local_evento</textarea></td></tr>";

            /*** Repeti��o ***/
            echo "<tr><td disabled>".A_LANG_AGENDA_REPETICAO_EVENTO.":</td>";
            echo"<td><input type=radio name=naorepetir value='naorepetir' disabled> ".A_LANG_AGENDA_NAO_REPETIR."";
            echo"<input type=radio name=diariamente value='diariamente' disabled> ".A_LANG_AGENDA_DIARIAMENTE."";
            echo"<input type=radio name=semanalmente value='semanalmente' disabled> ".A_LANG_AGENDA_SEMANALMENTE."";
            echo"<input type=radio name=mensalmente value='mensalmente' disabled> ".A_LANG_AGENDA_MENSALMENTE."";
            echo"<input type=radio name=Anualmente value='anualmente' disabled> ".A_LANG_AGENDA_ANUALMENTE."";
            echo"</input> ";
            echo"<select name=quantidadevezes class=select size=1 disabled>";
            echo "<option selected value=\"0\">".A_LANG_AGENDA_QUANTIDADE_REPETICOES."</option>";
            echo "<option value=\"1\">".A_LANG_AGENDA_1X."</option>";
            echo "<option value=\"2\">".A_LANG_AGENDA_2X."</option>";
            echo "<option value=\"3\">".A_LANG_AGENDA_3X."</option>";
            echo "<option value=\"4\">".A_LANG_AGENDA_4X."</option>";
            echo "<option value=\"5\">".A_LANG_AGENDA_5X."</option>";
            echo "<option value=\"6\">".A_LANG_AGENDA_6X."</option>";
            echo "<option value=\"7\">".A_LANG_AGENDA_7X."</option>";
            echo "<option value=\"8\">".A_LANG_AGENDA_8X."</option>";
            echo "<option value=\"9\">".A_LANG_AGENDA_9X."</option>";
            echo "<option value=\"10\">".A_LANG_AGENDA_10X."</option>";
            echo"</select> ";
            echo"".A_LANG_AGENDA_OU."</td></tr>";

            echo"<tr><td></td><td disabled>".A_LANG_AGENDA_DATA_REPETI��O.": <input class=button size=9 name=datarepeticao id=demo1 value=\"".$data_repeticao."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\" disabled></td></tr>";

            /*** E-mail de Aviso ***/
            echo "<tr><td disabled>".A_LANG_AGENDA_AVISO_EMAIL.":</td>";
            echo"<td><input type=radio name=naoenviar value='naoenviar' disabled> ".A_LANG_AGENDA_NAO_ENVIAR."";
            echo"<input type=radio name=dia value='dia' disabled> ".A_LANG_AGENDA_AVISO_UM_DIA."";
            echo"<input type=radio name=semana value='semana' disabled> ".A_LANG_AGENDA_AVISO_UMA_SEMANA."";
            echo"<input type=radio name=mes value='mes' disabled> ".A_LANG_AGENDA_AVISO_UM_MES."";
            echo"</input> ";
            echo"".A_LANG_AGENDA_OU."</td></tr>";

            echo"<tr><td></td><td disabled>".A_LANG_AGENDA_DATA_ENVIO_AVISO.": <input class=button size=9 name=dataaviso id=demo1 value=\"".$data_aviso."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\" disabled></td></tr>";

            /*** Espa�o ***/
            echo "<tr><td colspan=2>&nbsp</td></tr>";

            echo "</table>";//Encerra tabela de cadastro

            /*** Bot�es cadastrar e cancelar ***/
            echo "<tr><td colspan=2 align=left>";
            echo "<input name=alteraroutros id=bot1 class=button type=submit value=\"".A_LANG_AGENDA_ALTERAR."\"></input>";
            echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=VisualizarEvento&eve=$even';\"></input>";
            echo "</td></tr>";

            echo "</form>";//Encerra formul�rio

            /*** Espa�o ***/
            echo "<tr><td colspan=2>&nbsp</td></tr>";


            /*** Observa��o ***/
            echo"<tr><td colspan=2><b>".A_LANG_AGENDA_OBS_CAMPO_INCOMPLETO."</b></td></tr>";
        }
    }
}

?>
       
   </td>
  </table>
  </td>
  </tr>
</table>
