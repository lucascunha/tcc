<?

/* ---------------------------------------------------------------------
 * P�gina destinat�rio do agendamento                                  *
 * @author Carina Tissa Aihara <tissa.aihara@gmail.com>                *
 * @version 1.0 <01/11/2009>                                           *
 *                                                                     *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.   *
 * Como Estagio Curricular.                                            *
 * Orientadora: Avanilde Kemczinski                                    *
 * Suporvisora: Edino Lopes Fernandes                                  *
 *                                                                     *
 ----------------------------------------------------------------------*/

?>

<?php include ('_caminho_agenda.php'); ?>

<!-- C�digo para selecionar todos os destinat�rios -->
<html>
<head><title>Adaptweb</title></head>
<body bgcolor="#ffffff" link="#0000cd">
<script language="JavaScript">
function selecionar(elemento)
{
    set = document.getElementById(elemento);
    for(i=0;i<set.length;i++)
    {
	    set.options[i].selected=true;
    }
}
</script>

<!-- Calend�rio Popup -->
<script language="javascript" type="text/javascript" src="agenda/datetimepicker.js"></script>
<?

/*** Cria matriz de orelha ***/
$orelha = array();
$orelha = array
(
    array
    (
        "LABEL" => A_LANG_AGENDA_AGENDAMENTO,
        "LINK" => "index.php",
        "ESTADO" =>"ON"
    )
);

MontaOrelha($orelha);


?>
<!--- Monta tabela de fundo --->
<table CELLSPACING="0" CELLPADDING="0" border="0" width="100%" bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> height="80%">
    <tr valign="top">
        <td>
            <br>
              <table border="0">
                 <br>
                   <td valign="top">


<?

/*** Inciciando a session ***/
session_start();
session_register("id_event");
session_register("idagendamento");
session_register("destino_evento");
session_register("tipogrupo_dest");
session_register("grupo_esco");
session_register("id_disciplin_grup");
session_register("id_cur_grup");
session_register("id_professo_grup");
session_register("disciplin_grup");
session_register("cur_grup");
session_register("disciplincur_grup");


/*** Estabelecer uma conex�o com o banco de dados ***/
include "config/configuracoes.php";
$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;
include "include/conecta.php";




if(isset($_POST["agendarroot"]))
{
    $data_evento=$_POST["dataevento"];
    $hora_inicio=$_POST["horainicio"];
    $hora_fim=$_POST["horafim"];
    $local_evento=$_POST["localevento"];
    $tipogrupo_destinatario=$tipogrupo_dest;
    $grupo_escolhido=$grupo_esco;

    /*** Formatar a data ***/
    $num_letras = strlen($data_evento);

    $letra=0;
    do
    {
        if ($data_evento[$letra] == "-")
        {
            $num_primeiro_simbolo = $letra;
        }
        $letra=$letra+1;
    }while($letra<=2);

    for ($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
    {
        if ($data_evento[$letra]=="-")
        {
            $num_segundo_simbolo = $letra;
        }
    }
    for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
    {
        if($letra==0)
        {
            $dia_data=$data_evento[$letra];
        }
        else
        {
            $dia_data=$dia_data.$data_evento[$letra];
        }
    }
    for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
    {
        if($letra==$num_primeiro_simbolo + 1)
        {
            $mes_data=$data_evento[$letra];
        }
        else
        {
            $mes_data=$mes_data.$data_evento[$letra];
        }
    }
    for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
    {
        if($letra==$num_segundo_simbolo + 1)
        {
            $ano_data=$data_evento[$letra];
        }
        else
        {
            $ano_data=$ano_data.$data_evento[$letra];
        }
    }
    $data_formatada=$ano_data."-".$mes_data."-".$dia_data;

    if($data_evento!=NULL && $hora_inicio!=NULL && $hora_fim!=NULL)
    {
        if(checkdate($mes_data, $dia_data, $ano_data))
        {
            $quant_i= strlen($hora_inicio);
            $quant_f= strlen($hora_fim);
            if($quant_i==5 && $quant_f==5)
            {
                $aux=0;
                list($hora_i,$minuto_i) = explode(':',$hora_inicio);
                list($hora_f,$minuto_f) = explode(':',$hora_fim);

                for($cont=0;$cont<2;$cont++)
                {
                    if($hora_i[$cont]!="0" && $hora_i[$cont]!="1" && $hora_i[$cont]!="2" && $hora_i[$cont]!="3" && $hora_i[$cont]!="4" && $hora_i[$cont]!="5" && $hora_i[$cont]!="6" && $hora_i[$cont]!="7" && $hora_i[$cont]!="8" && $hora_i[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($hora_f[$cont]!="0" && $hora_f[$cont]!="1" && $hora_f[$cont]!="2" && $hora_f[$cont]!="3" && $hora_f[$cont]!="4" && $hora_f[$cont]!="5" && $hora_f[$cont]!="6" && $hora_f[$cont]!="7" && $hora_f[$cont]!="8" && $hora_f[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($minuto_i[$cont]!="0" && $minuto_i[$cont]!="1" && $minuto_i[$cont]!="2" && $minuto_i[$cont]!="3" && $minuto_i[$cont]!="4" && $minuto_i[$cont]!="5" && $minuto_i[$cont]!="6" && $minuto_i[$cont]!="7" && $minuto_i[$cont]!="8" && $minuto_i[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($minuto_f[$cont]!="0" && $minuto_f[$cont]!="1" && $minuto_f[$cont]!="2" && $minuto_f[$cont]!="3" && $minuto_f[$cont]!="4" && $minuto_f[$cont]!="5" && $minuto_f[$cont]!="6" && $minuto_f[$cont]!="7" && $minuto_f[$cont]!="8" && $minuto_f[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                }
                if($aux==0)
                {
                    $sql="select destinatario_agendamento_agenda.id_agendamento_evento
                    from agendamento_evento_agenda, destinatario_agendamento_agenda
                    where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                    and agendamento_evento_agenda.id_evento_agenda='$id_event'
                    and agendamento_evento_agenda.data_evento='$data_formatada'
                    and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                    and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                    and destinatario_agendamento_agenda.grupo_destinatario='$grupo_escolhido'
                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'";
                    $resp=mysql_query($sql);
                    $linha = mysql_num_rows($resp);

                    $sql1="select destinatario_agendamento_agenda.id_agendamento_evento
                    from agendamento_evento_agenda, destinatario_agendamento_agenda
                    where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                    and agendamento_evento_agenda.id_evento_agenda='$id_event'
                    and agendamento_evento_agenda.data_evento='$data_formatada'
                    and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                    and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                    and destinatario_agendamento_agenda.grupo_destinatario='$grupo_escolhido'
                    and destinatario_agendamento_agenda.id_usuario='1'";
                    $resp1=mysql_query($sql1);
                    $linha1 = mysql_num_rows($resp1);

                    if ($linha>0 && $linha1>0)
                    {
                        echo "<table border=0 width=100%>";
                        echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_EXISTE_AGENDAMENTO_IGUAL."</td></tr>";
                        echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\"></input>";
                        echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                        echo"</table>";
                        exit;
                    }
                    else
                    {
                        $dia_hoje = date ("d");
                        $mes_hoje = date ("m");
                        $ano_hoje = date ("Y");
                        $data_hoje_formatada=$ano_hoje."-".$mes_hoje."-".$dia_hoje;

                        $dataescolhida=strtotime($data_formatada);
                        $datadehoje=strtotime($data_hoje_formatada);

                        if($dataescolhida < $datadehoje)
                        {
                            echo "<table border=0 width=100%>";
                            echo "<tr><td><img src=\"imagens/erro.gif\">";
                            echo "".A_LANG_AGENDA_ERRO_DATA."</td></tr>";
                            echo"</table>";
                        }
                        else
                        {
                            list($hora_i,$minuto_i) = explode(':',$hora_inicio);
                            if ($hora_i < 0 || $hora_i > 23 || $minuto_i < 0 || $minuto_i > 59)
                            {
                                echo "<table border=0 width=100%>";
                                echo "<tr><td><img src=\"imagens/erro.gif\">";
                                echo "".A_LANG_AGENDA_ERRO_HORARIO_INICIO."</td></tr>";
                                echo"</table>";
                            }
                            else
                            {
                                list($hora_f,$minuto_f) = explode(':',$hora_fim);

                                if ($hora_f < 0 || $hora_f > 23 || $minuto_f < 0 || $minuto_f > 59)
                                {
                                    echo "<table border=0>";
                                    echo "<tr><td><img src=\"imagens/erro.gif\">".A_LANG_AGENDA_ERRO_HORARIO_FIM."</td></tr>";
                                    echo"</table>";
                                }
                                else
                                {
                                    $horai_timestamp=mktime($hora_i,$minuto_i,00);
                                    $horaf_timestamp=mktime($hora_f,$minuto_f,00);

                                    if($horaf_timestamp < $horai_timestamp)
                                    {
                                        echo "<table border=0 width=100%>";
                                        echo "<tr><td><img src=\"imagens/erro.gif\">";
                                        echo "".A_LANG_AGENDA_ERRO_HORARIO."</td></tr>";
                                        echo"</table>";
                                    }
                                    else
                                    {
                                        $conn = &ADONewConnection($A_DB_TYPE);
                                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                        $sql="INSERT INTO agendamento_evento_agenda (
                                        id_evento_agenda,
                                        data_evento,
                                        hora_inicio_evento,
                                        hora_fim_evento,
                                        local_evento,
                                        data_atualizacao_agendamento
                                        ) VALUES (
                                        '$id_event',
                                        '$data_formatada',
                                        '$hora_inicio',
                                        '$hora_fim',
                                        '$local_evento',
                                        'NULL')";
                                        $rs = $conn->Execute($sql);
                                        if ($rs == false)
                                        {
                                            echo "<table border=0 width=100%>";
                                            echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                            echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\"></input>";
                                            echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                            echo"</table>";
                                            exit;
                                        }
                                        else
                                        {
                                            $idagendamento=mysql_insert_id();
                                            $conn = &ADONewConnection($A_DB_TYPE);
                                            $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                            $sql="INSERT INTO destinatario_agendamento_agenda (
                                            id_agendamento_evento,
                                            id_usuario,
                                            grupo_destinatario,
                                            destino_evento,
                                            id_tipo_grupo_destinatario,
                                            grupo_destinatario_id_disc,
                                            grupo_destinatario_id_curso,
                                            grupo_destinatario_id_professor,
                                            data_envio
                                            ) VALUES (
                                            '$idagendamento',
                                            '$id_usuario',
                                            '$grupo_escolhido',
                                            '$destino_evento',
                                            '$tipogrupo_destinatario',
                                            'NULL',
                                            'NULL',
                                            'NULL',
                                            NOW())";
                                            $rs = $conn->Execute($sql);
                                            if ($rs == false)
                                            {
                                                $conn = &ADONewConnection($A_DB_TYPE);
                                                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                $sql2="delete
                                                from agendamento_evento_agenda
                                                where agendamento_evento_agenda.id_agendamento_evento='$idagendamento'";
                                                $rs2 = $conn->Execute($sql2);
                                                if($rs2 == false)
                                                {
                                                    echo "<table border=0 width=100%>";
                                                    echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                    echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\"></input></td></tr>";
                                                    echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                    echo"</table>";
                                                    exit;
                                                }
                                                else
                                                {
                                                    echo "<table border=0 width=100%>";
                                                    echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                    echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\"></input>";
                                                    echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                    echo"</table>";
                                                    exit;
                                                }
                                            }
                                            else
                                            {
                                                $conn = &ADONewConnection($A_DB_TYPE);
                                                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                $sql="INSERT INTO destinatario_agendamento_agenda (
                                                id_agendamento_evento,
                                                id_usuario,
                                                grupo_destinatario,
                                                destino_evento,
                                                id_tipo_grupo_destinatario,
                                                grupo_destinatario_id_disc,
                                                grupo_destinatario_id_curso,
                                                grupo_destinatario_id_professor,
                                                data_envio
                                                ) VALUES (
                                                '$idagendamento',
                                                '1',
                                                '$grupo_escolhido',
                                                '$destino_evento',
                                                '$tipogrupo_destinatario',
                                                'NULL',
                                                'NULL',
                                                'NULL',
                                                NOW())";
                                                $rs = $conn->Execute($sql);
                                                if ($rs == false)
                                                {
                                                    $conn = &ADONewConnection($A_DB_TYPE);
                                                    $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                    $sql2="delete
                                                    from agendamento_evento_agenda
                                                    where agendamento_evento_agenda.id_agendamento_evento='$idagendamento'";
                                                    $rs2 = $conn->Execute($sql2);
                                                    $conn = &ADONewConnection($A_DB_TYPE);
                                                    $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                    $sql3="delete
                                                    from destinatario_agendamento_agenda
                                                    where destinatario_agendamento_agenda.id_agendamento_evento='$idagendamento'
                                                    and destinatario_agendamento_agenda.grupo_destinatario='$grupo_escolhido'
                                                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'";
                                                    $rs3 = $conn->Execute($sql3);
                                                    if($rs2 == false || $r3==false)
                                                    {
                                                        echo "<table border=0 width=100%>";
                                                        echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                        echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\"></input></td></tr>";
                                                        echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                        echo"</table>";
                                                        exit;
                                                    }
                                                    else
                                                    {
                                                        echo "<table border=0 width=100%>";
                                                        echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                        echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\"></input>";
                                                        echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                        echo"</table>";
                                                        exit;
                                                    }
                                                }
                                                else
                                                {
                                                    ?>
                                                    <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=AgendamentoCadastrado">
                                                    <?
                                                    //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=AgendamentoCadastrado">


                                                    echo"<b>Processando...</b>";

                                                    /*** Espa�o ***/
                                                    echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                    /*** Tabela mostrando o evento cadastrado ***/
                                                    echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

                                                    /*** Evento ***/
                                                    echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

                                                    /*** Espa�o ***/
                                                    //echo "<tr class=zebraA><td colspan=2>&nbsp</td></tr>";


                                                    /*** Tipo do evento ***/
                                                    $sql="select id_tipo_evento
                                                    from evento_agenda
                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                    $resp=mysql_query($sql);
                                                    $linha = mysql_fetch_row($resp);
                                                    if($linha[0]==1)
                                                    {
                                                        $sql1="select tipo_evento_outros
                                                        from evento_agenda
                                                        where evento_agenda.id_evento_agenda='$id_event'";
                                                        $resp1=mysql_query($sql1);
                                                        $linha1 = mysql_fetch_row($resp1);
                                                        echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                                                    }
                                                    else
                                                    {
                                                        $sql="select nome_tipo_evento
                                                        from evento_agenda, tipo_evento_agenda
                                                        where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                                                        and evento_agenda.id_evento_agenda='$id_event'";
                                                        $resp=mysql_query($sql);
                                                        $linha = mysql_fetch_row($resp);
                                                        echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
                                                    }

                                                    /*** Tipo do assunto ***/
                                                    $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                                                    from evento_agenda, tipo_assunto_agenda
                                                    where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                                                    and evento_agenda.id_evento_agenda='$id_event'";
                                                    $resp=mysql_query($sql);
                                                    $linha1 = mysql_fetch_row($resp);
                                                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                                                    //Assunto
                                                    $sql="select assunto_evento
                                                    from evento_agenda
                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                    $resp=mysql_query($sql);
                                                    $linha = mysql_fetch_row($resp);
                                                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";



                                                    /*** Nome do evento ***/
                                                    $sql="select nome_evento
                                                    from evento_agenda
                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                    $resp=mysql_query($sql);
                                                    $linha = mysql_fetch_row($resp);
                                                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";


                                                    /*** Descri��o do evento ***/
                                                    $sql="select descricao_evento
                                                    from evento_agenda
                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                    $resp=mysql_query($sql);
                                                    $linha = mysql_fetch_row($resp);
                                                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                                                    /*** Data de cria��o do evento ***/
                                                    $sql="select data_criacao_evento
                                                    from evento_agenda
                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                    $resp=mysql_query($sql);
                                                    $linha = mysql_fetch_row($resp);
                                                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td><td>$linha[0]</td></tr>";

                                                    /*** Espa�o ***/
                                                    echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                    echo"</table></center>";
                                                    
                                                    exit;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    echo "<table border=0 width=100%>";
                    echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_HORA_INVALIDO."</td></tr>";
                    echo "<tr><td colspan=2>&nbsp</td></tr>";
                    echo "</table>";
                }
            }
            else
            {
                echo "<table border=0 width=100%>";
                echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_HORA_HH_MM."</td></tr>";
                echo "<tr><td colspan=2>&nbsp</td></tr>";
                echo "</table>";
            }
        }
        else
        {
            echo "<table border=0 width=100%>";
            echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_DATA_INVALIDA."</td></tr>";
            echo "<tr><td colspan=2>&nbsp</td></tr>";
            echo "</table>";
        }
    }
    else
    {
        $cont=0;
        if($data_evento==NULL)
        {
            $cont=$cont+1;
        }
        if($hora_inicio==NULL)
        {
            $cont=$cont+1;
        }
        if($hora_fim==NULL)
        {
            $cont=$cont+1;
        }

        echo "<table border=0 width=100%>";
        echo "<tr><td><img src=\"imagens/erro.gif\">&nbsp";
        if($cont==1)
        {
            echo "".A_LANG_AGENDA_ERRO_UM_CAMPO."";
            if($data_evento==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_DATA_EVENTO."";
            }

            if($hora_inicio==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_HORA_INICIO."";
            }
            if($hora_fim==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_HORA_FIM."";
            }
            echo "".A_LANG_AGENDA_ERRO_UM."</td></tr>";
        }
        else
        {
            echo "<td>".A_LANG_AGENDA_ERRO_VARIOS."<br>";
            if($data_evento==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_DATA_EVENTO."<br>";
            }
            if($hora_inicio==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_HORA_INICIO."<br>";
            }
            if($hora_fim==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_HORA_FIM."<br>";
            }
            echo "</td></tr>";
        }
        echo "<tr><td colspan=2>&nbsp</td></tr>";
    }
}


if(isset($_POST["agendaroutros"]))
{
    $id_disciplina_grupo=$id_disciplin_grup;
    $id_curso_grupo=$id_cur_grup;
    $id_professor_grupo=$id_professo_grup;
    $disciplina_grupo=$disciplin_grup;
    $curso_grupo=$cur_grup;
    $disccurso_grupo=$disciplincur_grup;
    $tipogrupo_destinatario=$tipogrupo_dest;
    $grupo_escolhido=$grupo_esco;
    $data_evento=$_POST["dataevento"];
    $hora_inicio=$_POST["horainicio"];
    $hora_fim=$_POST["horafim"];
    $local_evento=$_POST["localevento"];
    if(isset ($_POST["usugrupo"]))
    {
        $tipogrupo_destinatario=$tipogrupo_dest;
        $grupo_escolhido=$grupo_esco;
        $id_disciplina_grupo=$id_disciplin_grup;
        $id_curso_grupo=$id_cur_grup;
        $id_professor_grupo=$id_professo_grup;
        $disciplina_grupo=$disciplin_grup;
        $curso_grupo=$cur_grup;
        $disccurso_grupo=$disciplincur_grup;

        $dest = $usugrupo;
        $quantidade=count($dest);
    

    
        /*** Formatar a data ***/
        $num_letras = strlen($data_evento);

        $letra=0;
        do
        {
            if ($data_evento[$letra] == "-")
            {
                $num_primeiro_simbolo = $letra;
            }
            $letra=$letra+1;
        }while($letra<=2);

        for ($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
        {
            if ($data_evento[$letra]=="-")
            {
                $num_segundo_simbolo = $letra;
            }
        }
        for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
        {
            if($letra==0)
            {
                $dia_data=$data_evento[$letra];
            }
            else
            {
                $dia_data=$dia_data.$data_evento[$letra];
            }
        }
        for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
        {
            if($letra==$num_primeiro_simbolo + 1)
            {
                $mes_data=$data_evento[$letra];
            }
            else
            {
                $mes_data=$mes_data.$data_evento[$letra];
            }
        }
        for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
        {
            if($letra==$num_segundo_simbolo + 1)
            {
                $ano_data=$data_evento[$letra];
            }
            else
            {
                $ano_data=$ano_data.$data_evento[$letra];
            }
        }
        $data_formatada=$ano_data."-".$mes_data."-".$dia_data;
    
    

        if($data_evento!=NULL && $hora_inicio!=NULL && $hora_fim!=NULL)
        {
            if(checkdate($mes_data, $dia_data, $ano_data))
            {
                $quant_i= strlen($hora_inicio);
                $quant_f= strlen($hora_fim);
                if($quant_i==5 && $quant_f==5)
                {
                    $aux=0;
                    list($hora_i,$minuto_i) = explode(':',$hora_inicio);
                    list($hora_f,$minuto_f) = explode(':',$hora_fim);

                    for($cont=0;$cont<2;$cont++)
                    {
                        if($hora_i[$cont]!="0" && $hora_i[$cont]!="1" && $hora_i[$cont]!="2" && $hora_i[$cont]!="3" && $hora_i[$cont]!="4" && $hora_i[$cont]!="5" && $hora_i[$cont]!="6" && $hora_i[$cont]!="7" && $hora_i[$cont]!="8" && $hora_i[$cont]!="9")
                        {
                            $aux=1;
                            break;
                        }
                        if($hora_f[$cont]!="0" && $hora_f[$cont]!="1" && $hora_f[$cont]!="2" && $hora_f[$cont]!="3" && $hora_f[$cont]!="4" && $hora_f[$cont]!="5" && $hora_f[$cont]!="6" && $hora_f[$cont]!="7" && $hora_f[$cont]!="8" && $hora_f[$cont]!="9")
                        {
                            $aux=1;
                            break;
                        }
                        if($minuto_i[$cont]!="0" && $minuto_i[$cont]!="1" && $minuto_i[$cont]!="2" && $minuto_i[$cont]!="3" && $minuto_i[$cont]!="4" && $minuto_i[$cont]!="5" && $minuto_i[$cont]!="6" && $minuto_i[$cont]!="7" && $minuto_i[$cont]!="8" && $minuto_i[$cont]!="9")
                        {
                            $aux=1;
                            break;
                        }
                        if($minuto_f[$cont]!="0" && $minuto_f[$cont]!="1" && $minuto_f[$cont]!="2" && $minuto_f[$cont]!="3" && $minuto_f[$cont]!="4" && $minuto_f[$cont]!="5" && $minuto_f[$cont]!="6" && $minuto_f[$cont]!="7" && $minuto_f[$cont]!="8" && $minuto_f[$cont]!="9")
                        {
                            $aux=1;
                            break;
                        }
                    }
                    if($aux==0)
                    {
                        $dia_hoje = date ("d");
                        $mes_hoje = date ("m");
                        $ano_hoje = date ("Y");
                        $data_hoje_formatada=$ano_hoje."-".$mes_hoje."-".$dia_hoje;

                        $dataescolhida=strtotime($data_formatada);
                        $datadehoje=strtotime($data_hoje_formatada);
                        
                        if($dataescolhida >= $datadehoje)
                        {
                            list($hora_i,$minuto_i) = explode(':',$hora_inicio);
                            if ($hora_i >= 0 && $hora_i <= 23 && $minuto_i >= 0 && $minuto_i <= 59)
                            {
                                list($hora_f,$minuto_f) = explode(':',$hora_fim);
                                if ($hora_f >= 0 && $hora_f <= 23 && $minuto_f >= 0 && $minuto_f <= 59)
                                {
                                    $horai_timestamp=mktime($hora_i,$minuto_i,00);
                                    $horaf_timestamp=mktime($hora_f,$minuto_f,00);
                                    if($horaf_timestamp >= $horai_timestamp)
                                    {
                                        if($tipogrupo_destinatario==1)
                                        {
                                            $sql="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario
                                            from agendamento_evento_agenda, destinatario_agendamento_agenda, usuario
                                            where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                            and destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                                            and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                            and agendamento_evento_agenda.data_evento='$data_formatada'
                                            and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                            and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                            and destinatario_agendamento_agenda.grupo_destinatario='$disciplina_grupo'
                                            and destinatario_agendamento_agenda.id_usuario='$id_usuario'";
                                            $resp=mysql_query($sql);
                                            $linha33 = mysql_num_rows($resp);
                                            
                                            for($cont=0; $cont <$quantidade; $cont++)
                                            {
                                                $sql1="select destinatario_agendamento_agenda.id_usuario
                                                from agendamento_evento_agenda, destinatario_agendamento_agenda
                                                where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                and agendamento_evento_agenda.data_evento='$data_formatada'
                                                and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                and destinatario_agendamento_agenda.grupo_destinatario='$disciplina_grupo'
                                                and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'";
                                                $resp1=mysql_query($sql1);
                                                $linha1 = mysql_num_rows($resp1);
                                                if($linha1!=0)
                                                {
                                                    $aux1[$cont]=mysql_fetch_row($resp1);
                                                }
                                            }
                                            $quant1=count($aux1);
                                            
                                            if($quant1==0)
                                            {
                                                $conn = &ADONewConnection($A_DB_TYPE);
                                                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                $sql="INSERT INTO agendamento_evento_agenda (
                                                id_evento_agenda,
                                                data_evento,
                                                hora_inicio_evento,
                                                hora_fim_evento,
                                                local_evento,
                                                data_atualizacao_agendamento
                                                ) VALUES (
                                                '$id_event',
                                                '$data_formatada',
                                                '$hora_inicio',
                                                '$hora_fim',
                                                '$local_evento',
                                                'NULL')";
                                                $rs = $conn->Execute($sql);
                                                if ($rs == false)
                                                {
                                                    echo "<table border=0 width=100%>";
                                                    echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                    echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                    echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                    echo"</table>";
                                                    exit;
                                                }
                                                else
                                                {
                                                    $idagendamento=mysql_insert_id();
                                                    if($linha33==0 && $quant1==0)
                                                    {
                                                        $conn = &ADONewConnection($A_DB_TYPE);
                                                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                        $sql="INSERT INTO destinatario_agendamento_agenda (
                                                        id_agendamento_evento,
                                                        id_usuario,
                                                        grupo_destinatario,
                                                        destino_evento,
                                                        id_tipo_grupo_destinatario,
                                                        grupo_destinatario_id_disc,
                                                        grupo_destinatario_id_curso,
                                                        grupo_destinatario_id_professor,
                                                        data_envio
                                                        ) VALUES (
                                                        '$idagendamento',
                                                        '$id_usuario',
                                                        '$disciplina_grupo',
                                                        '$destino_evento',
                                                        '$tipogrupo_destinatario',
                                                        '$id_disciplina_grupo',
                                                        'NULL',
                                                        '$id_professor_grupo',
                                                        NOW())";
                                                        $rs = $conn->Execute($sql);
                                                        if ($rs == false)
                                                        {
                                                            echo "<table border=0 width=100%>";
                                                            echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                            echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                            echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                            echo"</table>";
                                                            exit;
                                                        }
                                                        else
                                                        {
                                                            for($cont=0; $cont <$quantidade; $cont++)
                                                            {
                                                                $sql="INSERT INTO destinatario_agendamento_agenda (
                                                                id_agendamento_evento,
                                                                id_usuario,
                                                                grupo_destinatario,
                                                                destino_evento,
                                                                id_tipo_grupo_destinatario,
                                                                grupo_destinatario_id_disc,
                                                                grupo_destinatario_id_curso,
                                                                grupo_destinatario_id_professor,
                                                                data_envio
                                                                ) VALUES (
                                                                '$idagendamento',
                                                                '$dest[$cont]',
                                                                '$disciplina_grupo',
                                                                '$destino_evento',
                                                                '$tipogrupo_destinatario',
                                                                '$id_disciplina_grupo',
                                                                'NULL',
                                                                '$id_professor_grupo',
                                                                NOW())";
                                                                $rs11 = $conn->Execute($sql);
                                                            }
                                                            if ($rs11 == false)
                                                            {
                                                                $sql2="delete
                                                                from destinatario_agendamento_agenda
                                                                where destinatario_agendamento_agenda.id_agendamento_evento='$idagendamento'
                                                                and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                                                and destinatario_agendamento_agenda.grupo_destinatario='$disciplina_grupo'";
                                                                $rs2 = $conn->Execute($sql2);
                                                                if($rs2 == true)
                                                                {
                                                                    echo "<table border=0 width=100%>";
                                                                    echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                    echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                    echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                    echo"</table>";
                                                                    exit;
                                                                }
                                                                else
                                                                {
                                                                    echo "<table border=0 width=100%>";
                                                                    echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                    echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                    echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                    echo"</table>";
                                                                    exit;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ?>
                                                                <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=AgendamentoCadastrado">
                                                                <?
                                                                //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=AgendamentoCadastrado">


                                                                echo"<b>Processando...</b>";
                                                                
                                                                /*** Espa�o ***/
                                                                echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                /*** Tabela mostrando o evento cadastrado ***/
                                                                echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

                                                                /*** Evento ***/
                                                                echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

                                                                /*** Tipo do evento ***/
                                                                $sql="select id_tipo_evento
                                                                from evento_agenda
                                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                                $resp=mysql_query($sql);
                                                                $linha = mysql_fetch_row($resp);
                                                                if($linha[0]==1)
                                                                {
                                                                    $sql1="select tipo_evento_outros
                                                                    from evento_agenda
                                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp1=mysql_query($sql1);
                                                                    $linha1 = mysql_fetch_row($resp1);
                                                                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                                                                }
                                                                else
                                                                {
                                                                    $sql="select nome_tipo_evento
                                                                    from evento_agenda, tipo_evento_agenda
                                                                    where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                                                                    and evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp=mysql_query($sql);
                                                                    $linha = mysql_fetch_row($resp);
                                                                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
                                                                }

                                                                /*** Tipo do assunto ***/
                                                                $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                                                                from evento_agenda, tipo_assunto_agenda
                                                                where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                                                                and evento_agenda.id_evento_agenda='$id_event'";
                                                                $resp=mysql_query($sql);
                                                                $linha1 = mysql_fetch_row($resp);
                                                                echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                                                                //Assunto
                                                                $sql="select assunto_evento
                                                                from evento_agenda
                                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                                $resp=mysql_query($sql);
                                                                $linha = mysql_fetch_row($resp);
                                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";

                                                                /*** Nome do evento ***/
                                                                $sql="select nome_evento
                                                                from evento_agenda
                                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                                $resp=mysql_query($sql);
                                                                $linha = mysql_fetch_row($resp);
                                                                echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";

                                                                /*** Descri��o do evento ***/
                                                                $sql="select descricao_evento
                                                                from evento_agenda
                                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                                $resp=mysql_query($sql);
                                                                $linha = mysql_fetch_row($resp);
                                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                                                                /*** Data de cria��o do evento ***/
                                                                $sql="select data_criacao_evento
                                                                from evento_agenda
                                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                                $resp=mysql_query($sql);
                                                                $linha = mysql_fetch_row($resp);
                                                                echo "<tr class=zebraB><td>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td><td>$linha[0]</td></tr>";

                                                                /*** Espa�o ***/
                                                                echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                echo"</table></center>";
                                                                
                                                                exit;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if($linha33>0 && $quant1==0)
                                                        {
                                                            $conn = &ADONewConnection($A_DB_TYPE);
                                                            $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                            for($cont=0; $cont <$quantidade; $cont++)
                                                            {
                                                                $sql="INSERT INTO destinatario_agendamento_agenda (
                                                                id_agendamento_evento,
                                                                id_usuario,
                                                                grupo_destinatario,
                                                                destino_evento,
                                                                id_tipo_grupo_destinatario,
                                                                grupo_destinatario_id_disc,
                                                                grupo_destinatario_id_curso,
                                                                grupo_destinatario_id_professor,
                                                                data_envio
                                                                ) VALUES (
                                                                '$idagendamento',
                                                                '$dest[$cont]',
                                                                '$disciplina_grupo',
                                                                '$destino_evento',
                                                                '$tipogrupo_destinatario',
                                                                '$id_disciplina_grupo',
                                                                'NULL',
                                                                '$id_professor_grupo',
                                                                NOW())";
                                                                $rs11 = $conn->Execute($sql);
                                                            }
                                                            if ($rs11 == false)
                                                            {
                                                                echo "<table border=0 width=100%>";
                                                                echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                echo"</table>";
                                                                exit;
                                                            }
                                                            else
                                                            {
                                                                ?>
                                                                <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=AgendamentoCadastrado">
                                                                <?
                                                                //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=AgendamentoCadastrado">


                                                                echo"<b>Processando...</b>";

                                                                /*** Espa�o ***/
                                                                echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                /*** Tabela mostrando o evento cadastrado ***/
                                                                echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

                                                                /*** Evento ***/
                                                                echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

                                                                /*** Tipo do evento ***/
                                                                $sql="select id_tipo_evento
                                                                from evento_agenda
                                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                                $resp=mysql_query($sql);
                                                                $linha = mysql_fetch_row($resp);
                                                                if($linha[0]==1)
                                                                {
                                                                    $sql1="select tipo_evento_outros
                                                                    from evento_agenda
                                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp1=mysql_query($sql1);
                                                                    $linha1 = mysql_fetch_row($resp1);
                                                                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                                                                }
                                                                else
                                                                {
                                                                    $sql="select nome_tipo_evento
                                                                    from evento_agenda, tipo_evento_agenda
                                                                    where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                                                                    and evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp=mysql_query($sql);
                                                                    $linha = mysql_fetch_row($resp);
                                                                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
                                                                }

                                                                /*** Tipo do assunto ***/
                                                                $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                                                                from evento_agenda, tipo_assunto_agenda
                                                                where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                                                                and evento_agenda.id_evento_agenda='$id_event'";
                                                                $resp=mysql_query($sql);
                                                                $linha1 = mysql_fetch_row($resp);
                                                                echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                                                                //Assunto
                                                                $sql="select assunto_evento
                                                                from evento_agenda
                                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                                $resp=mysql_query($sql);
                                                                $linha = mysql_fetch_row($resp);
                                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";

                                                                /*** Nome do evento ***/
                                                                $sql="select nome_evento
                                                                from evento_agenda
                                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                                $resp=mysql_query($sql);
                                                                $linha = mysql_fetch_row($resp);
                                                                echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";

                                                                /*** Descri��o do evento ***/
                                                                $sql="select descricao_evento
                                                                from evento_agenda
                                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                                $resp=mysql_query($sql);
                                                                $linha = mysql_fetch_row($resp);
                                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                                                                /*** Data de cria��o do evento ***/
                                                                $sql="select data_criacao_evento
                                                                from evento_agenda
                                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                                $resp=mysql_query($sql);
                                                                $linha = mysql_fetch_row($resp);
                                                                echo "<tr class=zebraB><td>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td><td>$linha[0]</td></tr>";

                                                                /*** Espa�o ***/
                                                                echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                echo"</table></center>";

                                                                exit;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if ($quant1==1)
                                                {
                                                    echo"<tr><td><img src=\"imagens/erro.gif\">&nbsp".A_LANG_AGENDA_DESTINATARIO_RECEBEU_ESTE_EVENTO."</td></tr>";
                                                    for($cont=0; $cont <$quantidade; $cont++)
                                                    {
                                                        $sql3="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario, usuario.email_usuario
                                                        from destinatario_agendamento_agenda, usuario, agendamento_evento_agenda
                                                        where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                                                        and agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                        and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                        and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'
                                                        and agendamento_evento_agenda.data_evento='$data_formatada'
                                                        and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                        and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                        and destinatario_agendamento_agenda.grupo_destinatario='$disciplina_grupo'";
                                                        $resp3=mysql_query($sql3);
                                                        while($aux= mysql_fetch_array($resp3))
                                                        {
                                                            echo"<tr><td>- $aux[1] ($aux[2])</td></tr>";
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if($quant1>1)
                                                    {
                                                        echo"<tr><td><img src=\"imagens/erro.gif\">&nbsp".A_LANG_AGENDA_DESTINATARIOS_RECEBERAM_ESTE_EVENTO."</td></tr>";
                                                        for($cont=0; $cont <$quantidade; $cont++)
                                                        {
                                                            $sql3="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario, usuario.email_usuario
                                                            from destinatario_agendamento_agenda, usuario, agendamento_evento_agenda
                                                            where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                                                            and agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                            and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                            and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'
                                                            and agendamento_evento_agenda.data_evento='$data_formatada'
                                                            and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                            and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                            and destinatario_agendamento_agenda.grupo_destinatario='$disciplina_grupo'";
                                                            $resp3=mysql_query($sql3);
                                                            while($aux= mysql_fetch_array($resp3))
                                                            {
                                                                echo"<tr><td>- $aux[1] ($aux[2])</td></tr>";
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if($tipogrupo_destinatario==2)
                                            {
                                                $sql="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario
                                                from agendamento_evento_agenda, destinatario_agendamento_agenda, usuario
                                                where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                and destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                                                and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                and agendamento_evento_agenda.data_evento='$data_formatada'
                                                and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                and destinatario_agendamento_agenda.grupo_destinatario='$curso_grupo'
                                                and destinatario_agendamento_agenda.id_usuario='$id_usuario'";
                                                $resp=mysql_query($sql);
                                                $linha33 = mysql_num_rows($resp);

                                                for($cont=0; $cont <$quantidade; $cont++)
                                                {
                                                    $sql1="select destinatario_agendamento_agenda.id_usuario
                                                    from agendamento_evento_agenda, destinatario_agendamento_agenda
                                                    where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                    and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                    and agendamento_evento_agenda.data_evento='$data_formatada'
                                                    and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                    and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                    and destinatario_agendamento_agenda.grupo_destinatario='$curso_grupo'
                                                    and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'";
                                                    $resp1=mysql_query($sql1);
                                                    $linha1 = mysql_num_rows($resp1);
                                                    if($linha1!=0)
                                                    {
                                                        $aux1[$cont]=mysql_fetch_row($resp1);
                                                    }
                                                }
                                                $quant1=count($aux1);
                                                
                                                if($quant1==0)
                                                {
                                                    $conn = &ADONewConnection($A_DB_TYPE);
                                                    $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                    $sql="INSERT INTO agendamento_evento_agenda (
                                                    id_evento_agenda,
                                                    data_evento,
                                                    hora_inicio_evento,
                                                    hora_fim_evento,
                                                    local_evento,
                                                    data_atualizacao_agendamento
                                                    ) VALUES (
                                                    '$id_event',
                                                    '$data_formatada',
                                                    '$hora_inicio',
                                                    '$hora_fim',
                                                    '$local_evento',
                                                    'NULL')";
                                                    $rs = $conn->Execute($sql);
                                                    if ($rs == false)
                                                    {
                                                        echo "<table border=0 width=100%>";
                                                        echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                        echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                        echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                        echo"</table>";
                                                        exit;
                                                    }
                                                    else
                                                    {
                                                        $idagendamento=mysql_insert_id();
                                                        if($linha33==0 && $quant1==0)
                                                        {
                                                            $conn = &ADONewConnection($A_DB_TYPE);
                                                            $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                            $sql="INSERT INTO destinatario_agendamento_agenda (
                                                            id_agendamento_evento,
                                                            id_usuario,
                                                            grupo_destinatario,
                                                            destino_evento,
                                                            id_tipo_grupo_destinatario,
                                                            grupo_destinatario_id_disc,
                                                            grupo_destinatario_id_curso,
                                                            grupo_destinatario_id_professor,
                                                            data_envio
                                                            ) VALUES (
                                                            '$idagendamento',
                                                            '$id_usuario',
                                                            '$curso_grupo',
                                                            '$destino_evento',
                                                            '$tipogrupo_destinatario',
                                                            'NULL',
                                                            '$id_curso_grupo',
                                                            '$id_professor_grupo',
                                                            NOW())";
                                                            $rs = $conn->Execute($sql);
                                                            if ($rs == false)
                                                            {
                                                                echo "<table border=0 width=100%>";
                                                                echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                echo"</table>";
                                                                exit;
                                                            }
                                                            else
                                                            {
                                                                for($cont=0; $cont <$quantidade; $cont++)
                                                                {
                                                                    $sql="INSERT INTO destinatario_agendamento_agenda (
                                                                    id_agendamento_evento,
                                                                    id_usuario,
                                                                    grupo_destinatario,
                                                                    destino_evento,
                                                                    id_tipo_grupo_destinatario,
                                                                    grupo_destinatario_id_disc,
                                                                    grupo_destinatario_id_curso,
                                                                    grupo_destinatario_id_professor,
                                                                    data_envio
                                                                    ) VALUES (
                                                                    '$idagendamento',
                                                                    '$dest[$cont]',
                                                                    '$curso_grupo',
                                                                    '$destino_evento',
                                                                    '$tipogrupo_destinatario',
                                                                    'NULL',
                                                                    '$id_curso_grupo',
                                                                    '$id_professor_grupo',
                                                                    NOW())";
                                                                    $rs11 = $conn->Execute($sql);
                                                                }
                                                                if ($rs11 == false)
                                                                {
                                                                    $sql2="delete
                                                                    from destinatario_agendamento_agenda
                                                                    where destinatario_agendamento_agenda.id_agendamento_evento='$idagendamento'
                                                                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                                                    and destinatario_agendamento_agenda.grupo_destinatario='$curso_grupo'";
                                                                    $rs2 = $conn->Execute($sql2);
                                                                    if($rs2 == true)
                                                                    {
                                                                        echo "<table border=0 width=100%>";
                                                                        echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                        echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                        echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                        echo"</table>";
                                                                        exit;
                                                                    }
                                                                    else
                                                                    {
                                                                        echo "<table border=0 width=100%>";
                                                                        echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                        echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                        echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                        echo"</table>";
                                                                        exit;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                    <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=AgendamentoCadastrado">
                                                                    <?
                                                                    //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=AgendamentoCadastrado">


                                                                    echo"<b>Processando...</b>";

                                                                    /*** Espa�o ***/
                                                                    echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                    /*** Tabela mostrando o evento cadastrado ***/
                                                                    echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

                                                                    /*** Evento ***/
                                                                    echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

                                                                    /*** Tipo do evento ***/
                                                                    $sql="select id_tipo_evento
                                                                    from evento_agenda
                                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp=mysql_query($sql);
                                                                    $linha = mysql_fetch_row($resp);
                                                                    if($linha[0]==1)
                                                                    {
                                                                        $sql1="select tipo_evento_outros
                                                                        from evento_agenda
                                                                        where evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp1=mysql_query($sql1);
                                                                        $linha1 = mysql_fetch_row($resp1);
                                                                        echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                                                                    }
                                                                    else
                                                                    {
                                                                        $sql="select nome_tipo_evento
                                                                        from evento_agenda, tipo_evento_agenda
                                                                        where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                                                                        and evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp=mysql_query($sql);
                                                                        $linha = mysql_fetch_row($resp);
                                                                        echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";

                                                                    }

                                                                    /*** Tipo do assunto ***/
                                                                    $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                                                                    from evento_agenda, tipo_assunto_agenda
                                                                    where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                                                                    and evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp=mysql_query($sql);
                                                                    $linha1 = mysql_fetch_row($resp);
                                                                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                                                                    //Assunto
                                                                    $sql="select assunto_evento
                                                                    from evento_agenda
                                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp=mysql_query($sql);
                                                                    $linha = mysql_fetch_row($resp);
                                                                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";

                                                                    /*** Nome do evento ***/
                                                                    $sql="select nome_evento
                                                                    from evento_agenda
                                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp=mysql_query($sql);
                                                                    $linha = mysql_fetch_row($resp);
                                                                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";

                                                                    /*** Descri��o do evento ***/
                                                                    $sql="select descricao_evento
                                                                    from evento_agenda
                                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp=mysql_query($sql);
                                                                    $linha = mysql_fetch_row($resp);
                                                                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                                                                    /*** Data de cria��o do evento ***/
                                                                    $sql="select data_criacao_evento
                                                                    from evento_agenda
                                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp=mysql_query($sql);
                                                                    $linha = mysql_fetch_row($resp);
                                                                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td><td>$linha[0]</td></tr>";

                                                                    /*** Espa�o ***/
                                                                    echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                    echo"</table></center>";

                                                                    exit;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if($linha33>0 && $quant1==0)
                                                            {
                                                                $conn = &ADONewConnection($A_DB_TYPE);
                                                                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                                for($cont=0; $cont <$quantidade; $cont++)
                                                                {
                                                                    $sql="INSERT INTO destinatario_agendamento_agenda (
                                                                    id_agendamento_evento,
                                                                    id_usuario,
                                                                    grupo_destinatario,
                                                                    destino_evento,
                                                                    id_tipo_grupo_destinatario,
                                                                    grupo_destinatario_id_disc,
                                                                    grupo_destinatario_id_curso,
                                                                    grupo_destinatario_id_professor,
                                                                    data_envio
                                                                    ) VALUES (
                                                                    '$idagendamento',
                                                                    '$dest[$cont]',
                                                                    '$curso_grupo',
                                                                    '$destino_evento',
                                                                    '$tipogrupo_destinatario',
                                                                    'NULL',
                                                                    '$id_curso_grupo',
                                                                    '$id_professor_grupo',
                                                                    NOW())";
                                                                    $rs11 = $conn->Execute($sql);
                                                                }
                                                                if ($rs11 == false)
                                                                {
                                                                    echo "<table border=0 width=100%>";
                                                                    echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                    echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                    echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                    echo"</table>";
                                                                    exit;
                                                                }
                                                                else
                                                                {
                                                                    ?>
                                                                    <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=AgendamentoCadastrado">
                                                                    <?
                                                                    //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=AgendamentoCadastrado">


                                                                    echo"<b>Processando...</b>";

                                                                    /*** Espa�o ***/
                                                                    echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                    /*** Tabela mostrando o evento cadastrado ***/
                                                                    echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

                                                                    /*** Evento ***/
                                                                    echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

                                                                    /*** Tipo do evento ***/
                                                                    $sql="select id_tipo_evento
                                                                    from evento_agenda
                                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp=mysql_query($sql);
                                                                    $linha = mysql_fetch_row($resp);
                                                                    if($linha[0]==1)
                                                                    {
                                                                        $sql1="select tipo_evento_outros
                                                                        from evento_agenda
                                                                        where evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp1=mysql_query($sql1);
                                                                        $linha1 = mysql_fetch_row($resp1);
                                                                        echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                                                                    }
                                                                    else
                                                                    {
                                                                        $sql="select nome_tipo_evento
                                                                        from evento_agenda, tipo_evento_agenda
                                                                        where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                                                                        and evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp=mysql_query($sql);
                                                                        $linha = mysql_fetch_row($resp);
                                                                        echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
                                                                    }

                                                                    /*** Tipo do assunto ***/
                                                                    $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                                                                    from evento_agenda, tipo_assunto_agenda
                                                                    where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                                                                    and evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp=mysql_query($sql);
                                                                    $linha1 = mysql_fetch_row($resp);
                                                                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                                                                    //Assunto
                                                                    $sql="select assunto_evento
                                                                    from evento_agenda
                                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp=mysql_query($sql);
                                                                    $linha = mysql_fetch_row($resp);
                                                                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";

                                                                    /*** Nome do evento ***/
                                                                    $sql="select nome_evento
                                                                    from evento_agenda
                                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp=mysql_query($sql);
                                                                    $linha = mysql_fetch_row($resp);
                                                                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";

                                                                    /*** Descri��o do evento ***/
                                                                    $sql="select descricao_evento
                                                                    from evento_agenda
                                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp=mysql_query($sql);
                                                                    $linha = mysql_fetch_row($resp);
                                                                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                                                                    /*** Data de cria��o do evento ***/
                                                                    $sql="select data_criacao_evento
                                                                    from evento_agenda
                                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                                    $resp=mysql_query($sql);
                                                                    $linha = mysql_fetch_row($resp);
                                                                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td><td>$linha[0]</td></tr>";

                                                                    /*** Espa�o ***/
                                                                    echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                    echo"</table></center>";

                                                                    exit;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if ($quant1==1)
                                                    {
                                                        echo"<tr><td><img src=\"imagens/erro.gif\">&nbsp".A_LANG_AGENDA_DESTINATARIO_RECEBEU_ESTE_EVENTO."</td></tr>";
                                                        for($cont=0; $cont <$quantidade; $cont++)
                                                        {
                                                            $sql3="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario, usuario.email_usuario
                                                            from destinatario_agendamento_agenda, usuario, agendamento_evento_agenda
                                                            where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                                                            and agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                            and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                            and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'
                                                            and agendamento_evento_agenda.data_evento='$data_formatada'
                                                            and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                            and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                            and destinatario_agendamento_agenda.grupo_destinatario='$curso_grupo'";
                                                            $resp3=mysql_query($sql3);
                                                            while($aux= mysql_fetch_array($resp3))
                                                            {
                                                                echo"<tr><td>- $aux[1] ($aux[2])</td></tr>";
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if($quant1>1)
                                                        {
                                                            echo"<tr><td><img src=\"imagens/erro.gif\">&nbsp".A_LANG_AGENDA_DESTINATARIOS_RECEBERAM_ESTE_EVENTO."</td></tr>";
                                                            for($cont=0; $cont <$quantidade; $cont++)
                                                            {
                                                                $sql3="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario, usuario.email_usuario
                                                                from destinatario_agendamento_agenda, usuario, agendamento_evento_agenda
                                                                where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                                                                and agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                                and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                                and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'
                                                                and agendamento_evento_agenda.data_evento='$data_formatada'
                                                                and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                                and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                                and destinatario_agendamento_agenda.grupo_destinatario='$curso_grupo'";
                                                                $resp3=mysql_query($sql3);
                                                                while($aux= mysql_fetch_array($resp3))
                                                                {
                                                                    echo"<tr><td>- $aux[1] ($aux[2])</td></tr>";
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if($tipogrupo_destinatario==3)
                                                {
                                                    $sql="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario
                                                    from agendamento_evento_agenda, destinatario_agendamento_agenda, usuario
                                                    where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                    and destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                                                    and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                    and agendamento_evento_agenda.data_evento='$data_formatada'
                                                    and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                    and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                    and destinatario_agendamento_agenda.grupo_destinatario='$disccurso_grupo'
                                                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'";
                                                    $resp=mysql_query($sql);
                                                    $linha33 = mysql_num_rows($resp);

                                                    for($cont=0; $cont <$quantidade; $cont++)
                                                    {
                                                        $sql1="select destinatario_agendamento_agenda.id_usuario
                                                        from agendamento_evento_agenda, destinatario_agendamento_agenda
                                                        where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                        and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                        and agendamento_evento_agenda.data_evento='$data_formatada'
                                                        and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                        and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                        and destinatario_agendamento_agenda.grupo_destinatario='$disccurso_grupo'
                                                        and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'";
                                                        $resp1=mysql_query($sql1);
                                                        $linha1 = mysql_num_rows($resp1);
                                                        if($linha1!=0)
                                                        {
                                                            $aux1[$cont]=mysql_fetch_row($resp1);
                                                        }
                                                    }
                                                    $quant1=count($aux1);
                                                    
                                                    if($quant1==0)
                                                    {
                                                        $conn = &ADONewConnection($A_DB_TYPE);
                                                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                        $sql="INSERT INTO agendamento_evento_agenda (
                                                        id_evento_agenda,
                                                        data_evento,
                                                        hora_inicio_evento,
                                                        hora_fim_evento,
                                                        local_evento,
                                                        data_atualizacao_agendamento
                                                        ) VALUES (
                                                        '$id_event',
                                                        '$data_formatada',
                                                        '$hora_inicio',
                                                        '$hora_fim',
                                                        '$local_evento',
                                                        'NULL')";
                                                        $rs = $conn->Execute($sql);
                                                        if ($rs == false)
                                                        {
                                                            echo "<table border=0 width=100%>";
                                                            echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                            echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                            echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                            echo"</table>";
                                                            exit;
                                                        }
                                                        else
                                                        {
                                                            $idagendamento=mysql_insert_id();
                                                            if($linha33==0 && $quant1==0)
                                                            {
                                                                $conn = &ADONewConnection($A_DB_TYPE);
                                                                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                                $sql="INSERT INTO destinatario_agendamento_agenda (
                                                                id_agendamento_evento,
                                                                id_usuario,
                                                                grupo_destinatario,
                                                                destino_evento,
                                                                id_tipo_grupo_destinatario,
                                                                grupo_destinatario_id_disc,
                                                                grupo_destinatario_id_curso,
                                                                grupo_destinatario_id_professor,
                                                                data_envio
                                                                ) VALUES (
                                                                '$idagendamento',
                                                                '$id_usuario',
                                                                '$disccurso_grupo',
                                                                '$destino_evento',
                                                                '$tipogrupo_destinatario',
                                                                '$id_disciplina_grupo',
                                                                '$id_curso_grupo',
                                                                '$id_professor_grupo',
                                                                NOW())";
                                                                $rs = $conn->Execute($sql);
                                                                if ($rs == false)
                                                                {
                                                                    echo "<table border=0 width=100%>";
                                                                    echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                    echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                    echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                    echo"</table>";
                                                                    exit;
                                                                }
                                                                else
                                                                {
                                                                    for($cont=0; $cont <$quantidade; $cont++)
                                                                    {
                                                                        $sql="INSERT INTO destinatario_agendamento_agenda (
                                                                        id_agendamento_evento,
                                                                        id_usuario,
                                                                        grupo_destinatario,
                                                                        destino_evento,
                                                                        id_tipo_grupo_destinatario,
                                                                        grupo_destinatario_id_disc,
                                                                        grupo_destinatario_id_curso,
                                                                        grupo_destinatario_id_professor,
                                                                        data_envio
                                                                        ) VALUES (
                                                                        '$idagendamento',
                                                                        '$dest[$cont]',
                                                                        '$disccurso_grupo',
                                                                        '$destino_evento',
                                                                        '$tipogrupo_destinatario',
                                                                        '$id_disciplina_grupo',
                                                                        '$id_curso_grupo',
                                                                        '$id_professor_grupo',
                                                                        NOW())";
                                                                        $rs11 = $conn->Execute($sql);
                                                                    }
                                                                    if ($rs11 == false)
                                                                    {
                                                                        $sql2="delete
                                                                        from destinatario_agendamento_agenda
                                                                        where destinatario_agendamento_agenda.id_agendamento_evento='$idagendamento'
                                                                        and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                                                        and destinatario_agendamento_agenda.grupo_destinatario='$disccurso_grupo'";
                                                                        $rs2 = $conn->Execute($sql2);
                                                                        if($rs2 == true)
                                                                        {
                                                                            echo "<table border=0 width=100%>";
                                                                            echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                            echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                            echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                            echo"</table>";
                                                                            exit;
                                                                        }
                                                                        else
                                                                        {
                                                                            echo "<table border=0 width=100%>";
                                                                            echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                            echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                            echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                            echo"</table>";
                                                                            exit;
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        ?>
                                                                        <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=AgendamentoCadastrado">
                                                                        <?
                                                                        //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=AgendamentoCadastrado">


                                                                        echo"<b>Processando...</b>";

                                                                        /*** Espa�o ***/
                                                                        echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                        /*** Tabela mostrando o evento cadastrado ***/
                                                                        echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

                                                                        /*** Evento ***/
                                                                        echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

                                                                        /*** Tipo do evento ***/
                                                                        $sql="select id_tipo_evento
                                                                        from evento_agenda
                                                                        where evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp=mysql_query($sql);
                                                                        $linha = mysql_fetch_row($resp);
                                                                        if($linha[0]==1)
                                                                        {
                                                                            $sql1="select tipo_evento_outros
                                                                            from evento_agenda
                                                                            where evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp1=mysql_query($sql1);
                                                                            $linha1 = mysql_fetch_row($resp1);
                                                                            echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                                                                        }
                                                                        else
                                                                        {
                                                                            $sql="select nome_tipo_evento
                                                                            from evento_agenda, tipo_evento_agenda
                                                                            where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                                                                            and evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp=mysql_query($sql);
                                                                            $linha = mysql_fetch_row($resp);
                                                                            echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
                                                                        }

                                                                        /*** Tipo do assunto ***/
                                                                        $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                                                                        from evento_agenda, tipo_assunto_agenda
                                                                        where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                                                                        and evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp=mysql_query($sql);
                                                                        $linha1 = mysql_fetch_row($resp);
                                                                        echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                                                                        //Assunto
                                                                        $sql="select assunto_evento
                                                                        from evento_agenda
                                                                        where evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp=mysql_query($sql);
                                                                        $linha = mysql_fetch_row($resp);
                                                                        echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";

                                                                        /*** Nome do evento ***/
                                                                        $sql="select nome_evento
                                                                        from evento_agenda
                                                                        where evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp=mysql_query($sql);
                                                                        $linha = mysql_fetch_row($resp);
                                                                        echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";

                                                                        /*** Descri��o do evento ***/
                                                                        $sql="select descricao_evento
                                                                        from evento_agenda
                                                                        where evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp=mysql_query($sql);
                                                                        $linha = mysql_fetch_row($resp);
                                                                        echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                                                                        /*** Data de cria��o do evento ***/
                                                                        $sql="select data_criacao_evento
                                                                        from evento_agenda
                                                                        where evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp=mysql_query($sql);
                                                                        $linha = mysql_fetch_row($resp);
                                                                        echo "<tr class=zebraB><td>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td><td>$linha[0]</td></tr>";

                                                                        /*** Espa�o ***/
                                                                        echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                        echo"</table></center>";

                                                                        exit;
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if($linha33>0 && $quant1==0)
                                                                {
                                                                    $conn = &ADONewConnection($A_DB_TYPE);
                                                                    $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                                    for($cont=0; $cont <$quantidade; $cont++)
                                                                    {
                                                                        $sql="INSERT INTO destinatario_agendamento_agenda (
                                                                        id_agendamento_evento,
                                                                        id_usuario,
                                                                        grupo_destinatario,
                                                                        destino_evento,
                                                                        id_tipo_grupo_destinatario,
                                                                        grupo_destinatario_id_disc,
                                                                        grupo_destinatario_id_curso,
                                                                        grupo_destinatario_id_professor,
                                                                        data_envio
                                                                        ) VALUES (
                                                                        '$idagendamento',
                                                                        '$dest[$cont]',
                                                                        '$disccurso_grupo',
                                                                        '$destino_evento',
                                                                        '$tipogrupo_destinatario',
                                                                        '$id_disciplina_grupo',
                                                                        '$id_curso_grupo',
                                                                        '$id_professor_grupo',
                                                                        NOW())";
                                                                        $rs11 = $conn->Execute($sql);
                                                                    }
                                                                    if ($rs11 == false)
                                                                    {
                                                                        echo "<table border=0 width=100%>";
                                                                        echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                        echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                        echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                        echo"</table>";
                                                                        exit;
                                                                    }
                                                                    else
                                                                    {
                                                                        ?>
                                                                        <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=AgendamentoCadastrado">
                                                                        <?
                                                                        //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=AgendamentoCadastrado">


                                                                        echo"<b>Processando...</b>";

                                                                        /*** Espa�o ***/
                                                                        echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                        /*** Tabela mostrando o evento cadastrado ***/
                                                                        echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

                                                                        /*** Evento ***/
                                                                        echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

                                                                        /*** Tipo do evento ***/
                                                                        $sql="select id_tipo_evento
                                                                        from evento_agenda
                                                                        where evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp=mysql_query($sql);
                                                                        $linha = mysql_fetch_row($resp);
                                                                        if($linha[0]==1)
                                                                        {
                                                                            $sql1="select tipo_evento_outros
                                                                            from evento_agenda
                                                                            where evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp1=mysql_query($sql1);
                                                                            $linha1 = mysql_fetch_row($resp1);
                                                                            echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                                                                        }
                                                                        else
                                                                        {
                                                                            $sql="select nome_tipo_evento
                                                                            from evento_agenda, tipo_evento_agenda
                                                                            where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                                                                            and evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp=mysql_query($sql);
                                                                            $linha = mysql_fetch_row($resp);
                                                                            echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
                                                                        }

                                                                        /*** Tipo do assunto ***/
                                                                        $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                                                                        from evento_agenda, tipo_assunto_agenda
                                                                        where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                                                                        and evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp=mysql_query($sql);
                                                                        $linha1 = mysql_fetch_row($resp);
                                                                        echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                                                                        //Assunto
                                                                        $sql="select assunto_evento
                                                                        from evento_agenda
                                                                        where evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp=mysql_query($sql);
                                                                        $linha = mysql_fetch_row($resp);
                                                                        echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";

                                                                        /*** Nome do evento ***/
                                                                        $sql="select nome_evento
                                                                        from evento_agenda
                                                                        where evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp=mysql_query($sql);
                                                                        $linha = mysql_fetch_row($resp);
                                                                        echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";

                                                                        /*** Descri��o do evento ***/
                                                                        $sql="select descricao_evento
                                                                        from evento_agenda
                                                                        where evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp=mysql_query($sql);
                                                                        $linha = mysql_fetch_row($resp);
                                                                        echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                                                                        /*** Data de cria��o do evento ***/
                                                                        $sql="select data_criacao_evento
                                                                        from evento_agenda
                                                                        where evento_agenda.id_evento_agenda='$id_event'";
                                                                        $resp=mysql_query($sql);
                                                                        $linha = mysql_fetch_row($resp);
                                                                        echo "<tr class=zebraB><td>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td><td>$linha[0]</td></tr>";

                                                                        /*** Espa�o ***/
                                                                        echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                        echo"</table></center>";

                                                                        exit;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if ($quant1==1)
                                                        {
                                                            echo"<tr><td><img src=\"imagens/erro.gif\">&nbsp".A_LANG_AGENDA_DESTINATARIO_RECEBEU_ESTE_EVENTO."</td></tr>";
                                                            for($cont=0; $cont <$quantidade; $cont++)
                                                            {
                                                                $sql3="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario, usuario.email_usuario
                                                                from destinatario_agendamento_agenda, usuario, agendamento_evento_agenda
                                                                where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                                                                and agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                                and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                                and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'
                                                                and agendamento_evento_agenda.data_evento='$data_formatada'
                                                                and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                                and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                                and destinatario_agendamento_agenda.grupo_destinatario='$disccurso_grupo'";
                                                                $resp3=mysql_query($sql3);
                                                                while($aux= mysql_fetch_array($resp3))
                                                                {
                                                                    echo"<tr><td>- $aux[1] ($aux[2])</td></tr>";
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if($quant1>1)
                                                            {
                                                                echo"<tr><td><img src=\"imagens/erro.gif\">&nbsp".A_LANG_AGENDA_DESTINATARIOS_RECEBERAM_ESTE_EVENTO."</td></tr>";
                                                                for($cont=0; $cont <$quantidade; $cont++)
                                                                {
                                                                    $sql3="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario, usuario.email_usuario
                                                                    from destinatario_agendamento_agenda, usuario, agendamento_evento_agenda
                                                                    where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                                                                    and agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                                    and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                                    and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'
                                                                    and agendamento_evento_agenda.data_evento='$data_formatada'
                                                                    and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                                    and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                                    and destinatario_agendamento_agenda.grupo_destinatario='$disccurso_grupo'";
                                                                    $resp3=mysql_query($sql3);
                                                                    while($aux= mysql_fetch_array($resp3))
                                                                    {
                                                                        echo"<tr><td>- $aux[1] ($aux[2])</td></tr>";
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if($tipogrupo_destinatario==5 || $tipogrupo_destinatario==6 || $tipogrupo_destinatario==7 ||$tipogrupo_destinatario==8)
                                                    {
                                                        $sql="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario
                                                        from agendamento_evento_agenda, destinatario_agendamento_agenda, usuario
                                                        where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                        and destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                                                        and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                        and agendamento_evento_agenda.data_evento='$data_formatada'
                                                        and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                        and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                        and destinatario_agendamento_agenda.grupo_destinatario='$grupo_escolhido'
                                                        and destinatario_agendamento_agenda.id_usuario='$id_usuario'";
                                                        $resp=mysql_query($sql);
                                                        $linha33 = mysql_num_rows($resp);

                                                        for($cont=0; $cont <$quantidade; $cont++)
                                                        {
                                                            $sql1="select destinatario_agendamento_agenda.id_usuario
                                                            from agendamento_evento_agenda, destinatario_agendamento_agenda
                                                            where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                            and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                            and agendamento_evento_agenda.data_evento='$data_formatada'
                                                            and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                            and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                            and destinatario_agendamento_agenda.grupo_destinatario='$grupo_escolhido'
                                                            and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'";
                                                            $resp1=mysql_query($sql1);
                                                            $linha1 = mysql_num_rows($resp1);
                                                            if($linha1!=0)
                                                            {
                                                                $aux1[$cont]=mysql_fetch_row($resp1);
                                                            }
                                                        }
                                                        $quant1=count($aux1);
                                                        
                                                        if($quant1==0)
                                                        {
                                                            $conn = &ADONewConnection($A_DB_TYPE);
                                                            $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                            $sql="INSERT INTO agendamento_evento_agenda (
                                                            id_evento_agenda,
                                                            data_evento,
                                                            hora_inicio_evento,
                                                            hora_fim_evento,
                                                            local_evento,
                                                            data_atualizacao_agendamento
                                                            ) VALUES (
                                                            '$id_event',
                                                            '$data_formatada',
                                                            '$hora_inicio',
                                                            '$hora_fim',
                                                            '$local_evento',
                                                            'NULL')";
                                                            $rs = $conn->Execute($sql);
                                                            if ($rs == false)
                                                            {
                                                                echo "<table border=0 width=100%>";
                                                                echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                echo"</table>";
                                                                exit;
                                                            }
                                                            else
                                                            {
                                                                $idagendamento=mysql_insert_id();
                                                                if($linha33==0 && $quant1==0)
                                                                {
                                                                    $conn = &ADONewConnection($A_DB_TYPE);
                                                                    $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                                    $sql="INSERT INTO destinatario_agendamento_agenda (
                                                                    id_agendamento_evento,
                                                                    id_usuario,
                                                                    grupo_destinatario,
                                                                    destino_evento,
                                                                    id_tipo_grupo_destinatario,
                                                                    grupo_destinatario_id_disc,
                                                                    grupo_destinatario_id_curso,
                                                                    grupo_destinatario_id_professor,
                                                                    data_envio
                                                                    ) VALUES (
                                                                    '$idagendamento',
                                                                    '$id_usuario',
                                                                    '$grupo_escolhido',
                                                                    '$destino_evento',
                                                                    '$tipogrupo_destinatario',
                                                                    'NULL',
                                                                    'NULL',
                                                                    'NULL',
                                                                    NOW())";
                                                                    $rs = $conn->Execute($sql);
                                                                    if ($rs == false)
                                                                    {
                                                                        echo "<table border=0 width=100%>";
                                                                        echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                        echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                        echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                        echo"</table>";
                                                                        exit;
                                                                    }
                                                                    else
                                                                    {
                                                                        for($cont=0; $cont <$quantidade; $cont++)
                                                                        {
                                                                            $sql="INSERT INTO destinatario_agendamento_agenda (
                                                                            id_agendamento_evento,
                                                                            id_usuario,
                                                                            grupo_destinatario,
                                                                            destino_evento,
                                                                            id_tipo_grupo_destinatario,
                                                                            grupo_destinatario_id_disc,
                                                                            grupo_destinatario_id_curso,
                                                                            grupo_destinatario_id_professor,
                                                                            data_envio
                                                                            ) VALUES (
                                                                            '$idagendamento',
                                                                            '$dest[$cont]',
                                                                            '$grupo_escolhido',
                                                                            '$destino_evento',
                                                                            '$tipogrupo_destinatario',
                                                                            'NULL',
                                                                            'NULL',
                                                                            'NULL',
                                                                            NOW())";
                                                                            $rs11 = $conn->Execute($sql);
                                                                        }
                                                                        if ($rs11 == false)
                                                                        {
                                                                            $sql2="delete
                                                                            from destinatario_agendamento_agenda
                                                                            where destinatario_agendamento_agenda.id_agendamento_evento='$idagendamento'
                                                                            and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                                                            and destinatario_agendamento_agenda.grupo_destinatario='$grupo_escolhido'";
                                                                            $rs2 = $conn->Execute($sql2);
                                                                            if($rs2 == true)
                                                                            {
                                                                                echo "<table border=0 width=100%>";
                                                                                echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                                echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                                echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                                echo"</table>";
                                                                                exit;
                                                                            }
                                                                            else
                                                                            {
                                                                                echo "<table border=0 width=100%>";
                                                                                echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                                echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                                echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                                echo"</table>";
                                                                                exit;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            ?>
                                                                            <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=AgendamentoCadastrado">
                                                                            <?
                                                                            //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=AgendamentoCadastrado">


                                                                            echo"<b>Processando...</b>";

                                                                            /*** Espa�o ***/
                                                                            echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                            /*** Tabela mostrando o evento cadastrado ***/
                                                                            echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

                                                                            /*** Evento ***/
                                                                            echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

                                                                            /*** Tipo do evento ***/
                                                                            $sql="select id_tipo_evento
                                                                            from evento_agenda
                                                                            where evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp=mysql_query($sql);
                                                                            $linha = mysql_fetch_row($resp);
                                                                            if($linha[0]==1)
                                                                            {
                                                                                $sql1="select tipo_evento_outros
                                                                                from evento_agenda
                                                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                                                $resp1=mysql_query($sql1);
                                                                                $linha1 = mysql_fetch_row($resp1);
                                                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                                                                            }
                                                                            else
                                                                            {
                                                                                $sql="select nome_tipo_evento
                                                                                from evento_agenda, tipo_evento_agenda
                                                                                where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                                                                                and evento_agenda.id_evento_agenda='$id_event'";
                                                                                $resp=mysql_query($sql);
                                                                                $linha = mysql_fetch_row($resp);
                                                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
                                                                            }

                                                                            /*** Tipo do assunto ***/
                                                                            $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                                                                            from evento_agenda, tipo_assunto_agenda
                                                                            where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                                                                            and evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp=mysql_query($sql);
                                                                            $linha1 = mysql_fetch_row($resp);
                                                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                                                                            //Assunto
                                                                            $sql="select assunto_evento
                                                                            from evento_agenda
                                                                            where evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp=mysql_query($sql);
                                                                            $linha = mysql_fetch_row($resp);
                                                                            echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";

                                                                            /*** Nome do evento ***/
                                                                            $sql="select nome_evento
                                                                            from evento_agenda
                                                                            where evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp=mysql_query($sql);
                                                                            $linha = mysql_fetch_row($resp);
                                                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";

                                                                            /*** Descri��o do evento ***/
                                                                            $sql="select descricao_evento
                                                                            from evento_agenda
                                                                            where evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp=mysql_query($sql);
                                                                            $linha = mysql_fetch_row($resp);
                                                                            echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                                                                            /*** Data de cria��o do evento ***/
                                                                            $sql="select data_criacao_evento
                                                                            from evento_agenda
                                                                            where evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp=mysql_query($sql);
                                                                            $linha = mysql_fetch_row($resp);
                                                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td><td>$linha[0]</td></tr>";

                                                                            /*** Espa�o ***/
                                                                            echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                            echo"</table></center>";

                                                                            exit;
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if($linha33>0 && $quant1==0)
                                                                    {
                                                                        $conn = &ADONewConnection($A_DB_TYPE);
                                                                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                                        for($cont=0; $cont <$quantidade; $cont++)
                                                                        {
                                                                            $sql="INSERT INTO destinatario_agendamento_agenda (
                                                                            id_agendamento_evento,
                                                                            id_usuario,
                                                                            grupo_destinatario,
                                                                            destino_evento,
                                                                            id_tipo_grupo_destinatario,
                                                                            grupo_destinatario_id_disc,
                                                                            grupo_destinatario_id_curso,
                                                                            grupo_destinatario_id_professor,
                                                                            data_envio
                                                                            ) VALUES (
                                                                            '$idagendamento',
                                                                            '$dest[$cont]',
                                                                            '$grupo_escolhido',
                                                                            '$destino_evento',
                                                                            '$tipogrupo_destinatario',
                                                                            'NULL',
                                                                            'NULL',
                                                                            'NULL',
                                                                            NOW())";
                                                                            $rs11 = $conn->Execute($sql);
                                                                        }
                                                                        if ($rs11 == false)
                                                                        {
                                                                            echo "<table border=0 width=100%>";
                                                                            echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                                            echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinatario';\"></input>";
                                                                            echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                                            echo"</table>";
                                                                            exit;
                                                                        }
                                                                        else
                                                                        {
                                                                            ?>
                                                                            <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=AgendamentoCadastrado">
                                                                            <?
                                                                            //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=AgendamentoCadastrado">


                                                                            echo"<b>Processando...</b>";

                                                                            /*** Espa�o ***/
                                                                            echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                            /*** Tabela mostrando o evento cadastrado ***/
                                                                            echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

                                                                            /*** Evento ***/
                                                                            echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

                                                                            /*** Tipo do evento ***/
                                                                            $sql="select id_tipo_evento
                                                                            from evento_agenda
                                                                            where evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp=mysql_query($sql);
                                                                            $linha = mysql_fetch_row($resp);
                                                                            if($linha[0]==1)
                                                                            {
                                                                                $sql1="select tipo_evento_outros
                                                                                from evento_agenda
                                                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                                                $resp1=mysql_query($sql1);
                                                                                $linha1 = mysql_fetch_row($resp1);
                                                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                                                                            }
                                                                            else
                                                                            {
                                                                                $sql="select nome_tipo_evento
                                                                                from evento_agenda, tipo_evento_agenda
                                                                                where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                                                                                and evento_agenda.id_evento_agenda='$id_event'";
                                                                                $resp=mysql_query($sql);
                                                                                $linha = mysql_fetch_row($resp);
                                                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
                                                                            }

                                                                            /*** Tipo do assunto ***/
                                                                            $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                                                                            from evento_agenda, tipo_assunto_agenda
                                                                            where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                                                                            and evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp=mysql_query($sql);
                                                                            $linha1 = mysql_fetch_row($resp);
                                                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                                                                            //Assunto
                                                                            $sql="select assunto_evento
                                                                            from evento_agenda
                                                                            where evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp=mysql_query($sql);
                                                                            $linha = mysql_fetch_row($resp);
                                                                            echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";

                                                                            /*** Nome do evento ***/
                                                                            $sql="select nome_evento
                                                                            from evento_agenda
                                                                            where evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp=mysql_query($sql);
                                                                            $linha = mysql_fetch_row($resp);
                                                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";

                                                                            /*** Descri��o do evento ***/
                                                                            $sql="select descricao_evento
                                                                            from evento_agenda
                                                                            where evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp=mysql_query($sql);
                                                                            $linha = mysql_fetch_row($resp);
                                                                            echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                                                                            /*** Data de cria��o do evento ***/
                                                                            $sql="select data_criacao_evento
                                                                            from evento_agenda
                                                                            where evento_agenda.id_evento_agenda='$id_event'";
                                                                            $resp=mysql_query($sql);
                                                                            $linha = mysql_fetch_row($resp);
                                                                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td><td>$linha[0]</td></tr>";

                                                                            /*** Espa�o ***/
                                                                            echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                                            echo"</table></center>";

                                                                            exit;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if ($quant1==1)
                                                            {
                                                                echo"<tr><td><img src=\"imagens/erro.gif\">&nbsp".A_LANG_AGENDA_DESTINATARIO_RECEBEU_ESTE_EVENTO."</td></tr>";
                                                                for($cont=0; $cont <$quantidade; $cont++)
                                                                {
                                                                    $sql3="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario, usuario.email_usuario
                                                                    from destinatario_agendamento_agenda, usuario, agendamento_evento_agenda
                                                                    where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                                                                    and agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                                    and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                                    and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'
                                                                    and agendamento_evento_agenda.data_evento='$data_formatada'
                                                                    and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                                    and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                                    and destinatario_agendamento_agenda.grupo_destinatario='$grupo_escolhido'";
                                                                    $resp3=mysql_query($sql3);
                                                                    while($aux= mysql_fetch_array($resp3))
                                                                    {
                                                                        echo"<tr><td>- $aux[1] ($aux[2])</td></tr>";
                                                                    }

                                                                }
                                                            }
                                                            else
                                                            {
                                                                if($quant1>1)
                                                                {
                                                                    echo"<tr><td><img src=\"imagens/erro.gif\">&nbsp".A_LANG_AGENDA_DESTINATARIOS_RECEBERAM_ESTE_EVENTO."</td></tr>";
                                                                    for($cont=0; $cont <$quantidade; $cont++)
                                                                    {
                                                                        $sql3="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario, usuario.email_usuario
                                                                        from destinatario_agendamento_agenda, usuario, agendamento_evento_agenda
                                                                        where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                                                                        and agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                                                                        and agendamento_evento_agenda.id_evento_agenda='$id_event'
                                                                        and destinatario_agendamento_agenda.id_usuario='$dest[$cont]'
                                                                        and agendamento_evento_agenda.data_evento='$data_formatada'
                                                                        and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                                                                        and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                                                                        and destinatario_agendamento_agenda.grupo_destinatario='$grupo_escolhido'";
                                                                        $resp3=mysql_query($sql3);
                                                                        while($aux= mysql_fetch_array($resp3))
                                                                        {
                                                                            echo"<tr><td>- $aux[1] ($aux[2])</td></tr>";
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        echo "<table border=0 width=100%>";
                                        echo "<tr><td><img src=\"imagens/erro.gif\">";
                                        echo "".A_LANG_AGENDA_ERRO_HORARIO."</td></tr>";
                                        echo "<tr><td colspan=2>&nbsp</td></tr>";
                                        echo"</table>";
                                    }
                                }
                                else
                                {
                                    echo "<table border=0>";
                                    echo "<tr><td><img src=\"imagens/erro.gif\">".A_LANG_AGENDA_ERRO_HORARIO_FIM."</td></tr>";
                                    echo "<tr><td colspan=2>&nbsp</td></tr>";
                                    echo"</table>";
                                }
                            }
                            else
                            {
                                echo "<table border=0 width=100%>";
                                echo "<tr><td><img src=\"imagens/erro.gif\">";
                                echo "".A_LANG_AGENDA_ERRO_HORARIO_INICIO."</td></tr>";
                                echo "<tr><td colspan=2>&nbsp</td></tr>";
                                echo"</table>";
                            }
                        }
                        else
                        {
                            echo "<table border=0 width=100%>";
                            echo "<tr><td><img src=\"imagens/erro.gif\">";
                            echo "".A_LANG_AGENDA_ERRO_DATA."</td></tr>";
                            echo "<tr><td colspan=2>&nbsp</td></tr>";
                            echo"</table>";
                        }
                    }
                    else
                    {
                        echo "<table border=0 width=100%>";
                        echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_HORA_INVALIDO."</td></tr>";
                        echo "<tr><td colspan=2>&nbsp</td></tr>";
                        echo "</table>";
                    }
                }
                else
                {
                    echo "<table border=0 width=100%>";
                    echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_HORA_HH_MM."</td></tr>";
                    echo "<tr><td colspan=2>&nbsp</td></tr>";
                    echo "</table>";
                }
            }
            else
            {
                echo "<table border=0 width=100%>";
                echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_DATA_INVALIDA."</td></tr>";
                echo "<tr><td colspan=2>&nbsp</td></tr>";
                echo "</table>";
            }
        }
        else
        {
            $cont=0;
            if($data_evento==NULL)
            {
                $cont=$cont+1;
            }
            if($hora_inicio==NULL)
            {
                $cont=$cont+1;
            }
            if($hora_fim==NULL)
            {
                $cont=$cont+1;
            }

            echo "<table border=0 width=100%>";
            echo "<tr><td><img src=\"imagens/erro.gif\">&nbsp";
            if($cont==1)
            {
                echo "".A_LANG_AGENDA_ERRO_UM_CAMPO."";
                if($data_evento==NULL)
                {
                    echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_DATA_EVENTO."";
                }
                if($hora_inicio==NULL)
                {
                    echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_HORA_INICIO."";
                }
                if($hora_fim==NULL)
                {
                    echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_HORA_FIM."";
                }
                echo "".A_LANG_AGENDA_ERRO_UM."</td></tr>";
            }
            else
            {
                echo "<td>".A_LANG_AGENDA_ERRO_VARIOS."<br>";
                if($data_evento==NULL)
                {
                    echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_DATA_EVENTO."<br>";
                }
                if($hora_inicio==NULL)
                {
                    echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_HORA_INICIO."<br>";
                }
                if($hora_fim==NULL)
                {
                    echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_HORA_FIM."<br>";
                }
                echo "</td></tr>";
            }
            echo "<tr><td colspan=2>&nbsp</td></tr>";
        }
    }
    else
    {
        echo "<table border=0 width=100%>";
        echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_NAO_SELECIONADO_NENHUM_USUARIO."</td></tr>";
        echo "<tr><td colspan=2>&nbsp</td></tr>";
        echo"</table>";
    }
}





if (isset ($_POST["tipogrupodestinatario"]))
{
    $tipogrupo_destinatario=$_POST["tipogrupodestinatario"];
}

if (isset ($_POST["tipogrupodestinatario"]) || isset($tipogrupo_destinatario))
{
    $id_disciplina_grupo=$id_disciplin_grup;
    $id_curso_grupo=$id_cur_grup;
    $id_professor_grupo=$id_professo_grup;
    $disciplina_grupo=$disciplin_grup;
    $curso_grupo=$cur_grup;
    $disccurso_grupo=$disciplincur_grup;
    //$tipogrupo_destinatario=$_POST["tipogrupodestinatario"];
    if(isset($_POST["grupodisc"]) || isset($_POST["grupocurs"]) || isset($_POST["grupodisccurs"]) || isset($_POST["gruporoot"]) || isset($_POST["grupoalu"]) || isset($_POST["grupoprof"]) || isset($_POST["grupotod"]) || isset($_POST["grupoinst"]))
    {
        $grupo_disc=$_POST["grupodisc"];
        $grupo_curs=$_POST["grupocurs"];
        $grupo_disccurs=$_POST["grupodisccurs"];
        $grupo_root=$_POST["gruporoot"];
        $grupo_alu=$_POST["grupoalu"];
        $grupo_prof=$_POST["grupoprof"];
        $grupo_tod=$_POST["grupotod"];
        $grupo_inst=$_POST["grupoinst"];

        if($tipogrupo_destinatario == "1")
        {
            $grupo_escolhido=$grupo_disc;
        }
        else
        {
            if($tipogrupo_destinatario == "2")
            {
                $grupo_escolhido=$grupo_curs;
            }
            else
            {
                if($tipogrupo_destinatario == "3")
                {
                    $grupo_escolhido=$grupo_disccurs;
                }
                else
                {
                    if($tipogrupo_destinatario == "4")
                    {
                        $grupo_escolhido=$grupo_root;
                    }
                    else
                    {
                        if($tipogrupo_destinatario == "5")
                        {
                            $grupo_escolhido=$grupo_alu;
                        }
                        else
                        {
                            if($tipogrupo_destinatario == "6")
                            {
                                $grupo_escolhido=$grupo_prof;
                            }
                            else
                            {
                                if($tipogrupo_destinatario == "7")
                                {
                                    $grupo_escolhido=$grupo_tod;
                                }
                                else
                                {
                                    if($tipogrupo_destinatario == "8")
                                    {
                                        $grupo_escolhido=$grupo_inst;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        $tipogrupo_destinatario=$tipogrupo_dest;
        $grupo_escolhido=$grupo_esco;
    }
    
    if($tipogrupo_destinatario=="4")
    {
        /*** Tabela mostrando o evento cadastrado ***/
        echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

        /*** Evento ***/
        echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

        /*** Espa�o ***/
        //echo "<tr class=zebraA><td colspan=2>&nbsp</td></tr>";


        /*** Tipo do evento ***/
        $sql="select id_tipo_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        if($linha[0]==1)
        {
            $sql1="select tipo_evento_outros
            from evento_agenda
            where evento_agenda.id_evento_agenda='$id_event'";
            $resp1=mysql_query($sql1);
            $linha1 = mysql_fetch_row($resp1);
            echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
        }
        else
        {
            $sql="select nome_tipo_evento
            from evento_agenda, tipo_evento_agenda
            where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
            and evento_agenda.id_evento_agenda='$id_event'";
            $resp=mysql_query($sql);
            $linha = mysql_fetch_row($resp);
            echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
        }

        /*** Tipo do assunto ***/
        $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
        from evento_agenda, tipo_assunto_agenda
        where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
        and evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha1 = mysql_fetch_row($resp);
        echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

        //Assunto
        $sql="select assunto_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";

        /*** Nome do evento ***/
        $sql="select nome_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";

        /*** Descri��o do evento ***/
        $sql="select descricao_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";
        /*** Espa�o ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";
        
        echo"</table></center>";
        
        echo"<center><table>";

        /*** Espa�o ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";

        /*** Root ***/
        echo "<tr><td colspan=2 align=center><b>$grupo_escolhido</b></td></tr>";


        echo"</table></center>";
        

        ?>
        <!--- Inicia formul�rio --->
        <form name="formularioagendamentoroot" action="n_index_agenda.php?opcao=Destinatario" method="post">
        <?

        /*** Tabela do agendamento do evento ***/
        echo "<table border=0 width=100%>";
        
        /*** Data do evento ***/
        echo "<tr><td>".A_LANG_AGENDA_DATA_EVENTO."*:</td>";
        echo "<td><input class=button size=9 name=dataevento id=demo1 value=\"".$data_evento."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\">";

        ?>
        <script>
        function horaConta(c)
        {
            if(c.value.length==2)
            {
                c.value += ':';
            }
        }
        </script>
        <?

        /*** Hora in�cio ***/
        echo"<tr><td>".A_LANG_AGENDA_HORA_INICIO."*:</td><td><input class=text type=text size=3 name=horainicio onkeyup=horaConta(this); value=\"".$hora_inicio."\"></td></tr>";

        /*** Hora fim ***/
        echo "<tr><td>".A_LANG_AGENDA_HORA_FIM."*:</td><td><input class=text type=text size=3 name=horafim onkeyup=horaConta(this); value=\"".$hora_fim."\"></td></tr>";

        /*** Local do evento ***/
        echo "<tr><td>".A_LANG_AGENDA_LOCAL_EVENTO.":</td><td><textarea name=localevento class=button cols=70 rows=3>$local_evento</textarea></td></tr>";

        /*** Repeti��o ***/
        echo "<tr><td disabled>".A_LANG_AGENDA_REPETICAO_EVENTO.":</td>";
        echo"<td><input type=radio name=naorepetir value='naorepetir' disabled> ".A_LANG_AGENDA_NAO_REPETIR."";
        echo"<input type=radio name=diariamente value='diariamente' disabled> ".A_LANG_AGENDA_DIARIAMENTE."";
        echo"<input type=radio name=semanalmente value='semanalmente' disabled> ".A_LANG_AGENDA_SEMANALMENTE."";
        echo"<input type=radio name=mensalmente value='mensalmente' disabled> ".A_LANG_AGENDA_MENSALMENTE."";
        echo"<input type=radio name=Anualmente value='anualmente' disabled> ".A_LANG_AGENDA_ANUALMENTE."";
        echo"</input> ";
        echo"<select name=quantidadevezes class=select size=1 disabled>";
        echo "<option selected value=\"0\">".A_LANG_AGENDA_QUANTIDADE_REPETICOES."</option>";
        echo "<option value=\"1\">".A_LANG_AGENDA_1X."</option>";
        echo "<option value=\"2\">".A_LANG_AGENDA_2X."</option>";
        echo "<option value=\"3\">".A_LANG_AGENDA_3X."</option>";
        echo "<option value=\"4\">".A_LANG_AGENDA_4X."</option>";
        echo "<option value=\"5\">".A_LANG_AGENDA_5X."</option>";
        echo "<option value=\"6\">".A_LANG_AGENDA_6X."</option>";
        echo "<option value=\"7\">".A_LANG_AGENDA_7X."</option>";
        echo "<option value=\"8\">".A_LANG_AGENDA_8X."</option>";
        echo "<option value=\"9\">".A_LANG_AGENDA_9X."</option>";
        echo "<option value=\"10\">".A_LANG_AGENDA_10X."</option>";
        echo"</select> ";
        echo"".A_LANG_AGENDA_OU."</td></tr>";

        echo"<tr><td></td><td disabled>".A_LANG_AGENDA_DATA_REPETI��O.": <input class=button size=9 name=datarepeticao id=demo1 value=\"".$data_repeticao."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\" disabled></td></tr>";

        /*** E-mail de Aviso ***/
        echo "<tr><td disabled>".A_LANG_AGENDA_AVISO_EMAIL.":</td>";
        echo"<td><input type=radio name=naoenviar value='naoenviar' disabled> ".A_LANG_AGENDA_NAO_ENVIAR."";
        echo"<input type=radio name=dia value='dia' disabled> ".A_LANG_AGENDA_AVISO_UM_DIA."";
        echo"<input type=radio name=semana value='semana' disabled> ".A_LANG_AGENDA_AVISO_UMA_SEMANA."";
        echo"<input type=radio name=mes value='mes' disabled> ".A_LANG_AGENDA_AVISO_UM_MES."";
        echo"</input> ";
        echo"".A_LANG_AGENDA_OU."</td></tr>";

        echo"<tr><td></td><td disabled>".A_LANG_AGENDA_DATA_ENVIO_AVISO.": <input class=button size=9 name=dataaviso id=demo1 value=\"".$data_aviso."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\" disabled></td></tr>";

        /*** Espa�o ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";

        /*** Bot�es cadastrar e cancelar ***/
        echo "<tr valign=top><td colspan=2 align=left>";
        echo "<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_MUDAR_GRUPO_DESTINATARIO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\"></input>";
        echo "&nbsp<input name=agendarroot id=bot1 class=button type=submit value=\"".A_LANG_AGENDA_AGENDAR."\"></input>";
        echo "&nbsp<inputid=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input>";
        echo "</td></tr>";
        echo"</table>";
        echo"</form>";
        
        //Espa�os
        echo "<tr><td colspan=2>&nbsp</td></tr>";

        echo"<table border=0 align=left>";
        
        /*** Espa�os ***/
        //echo "<tr><td colspan=2>&nbsp</td></tr>";

        /*** Observa��o ***/
        echo"<tr><td colspan=2><b>".A_LANG_AGENDA_OBS_CAMPO_INCOMPLETO."</b></td></tr>";
        echo"</table>";
        $tipogrupo_dest=$tipogrupo_destinatario;
        $grupo_esco=$grupo_escolhido;
    }
    if($tipogrupo_destinatario!="0" && $tipogrupo_destinatario!="4")
    {

        /*** Tabela mostrando o evento cadastrado ***/
        echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

        /*** Evento ***/
        echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

        /*** Espa�o ***/
        //echo "<tr class=zebraA><td colspan=2>&nbsp</td></tr>";


        /*** Tipo do evento ***/
        $sql="select id_tipo_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        if($linha[0]==1)
        {
            $sql1="select tipo_evento_outros
            from evento_agenda
            where evento_agenda.id_evento_agenda='$id_event'";
            $resp1=mysql_query($sql1);
            $linha1 = mysql_fetch_row($resp1);
            echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
        }
        else
        {
            $sql="select nome_tipo_evento
            from evento_agenda, tipo_evento_agenda
            where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
            and evento_agenda.id_evento_agenda='$id_event'";
            $resp=mysql_query($sql);
            $linha = mysql_fetch_row($resp);
            echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
        }

        /*** Tipo do assunto ***/
        $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
        from evento_agenda, tipo_assunto_agenda
        where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
        and evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha1 = mysql_fetch_row($resp);
        echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

        //Assunto
        $sql="select assunto_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";

        /*** Nome do evento ***/
        $sql="select nome_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";

        /*** Descri��o do evento ***/
        $sql="select descricao_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";
        
        /*** Espa�o ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";
        
        echo"</table></center>";
        
        echo"<center><table border=0>";
        
        /*** Espa�o ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";

        /*** Grupo destinat�rio ***/
        if($tipogrupo_destinatario=="1")
        {
            $num_letras = strlen($grupo_escolhido);

            for ($letra=0; $letra <= $num_letras; $letra++)
            {
                if ($grupo_escolhido[$letra]=="-")
                {
                    $num_primeiro_simbolo = $letra;
                }
                if ($grupo_escolhido[$letra]=="*")
                {
                    $num_segundo_simbolo = $letra;
                    break;
                }
            }
            for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
            {
                if($letra==0)
                {
                    $id_disciplina_grupo=$grupo_escolhido[$letra];
                }
                else
                {
                    $id_disciplina_grupo=$id_disciplina_grupo.$grupo_escolhido[$letra];
                }
            }
            for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo -1; $letra++)
            {
                if($letra==$num_primeiro_simbolo + 1)
                {
                    $id_professor_grupo=$grupo_escolhido[$letra];
                }
                else
                {
                    $id_professor_grupo=$professor_grupo.$id_grupo_escolhido[$letra];
                }
            }
            for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
            {
                if($letra==$num_segundo_simbolo + 1)
                {
                    $disciplina_grupo=$grupo_escolhido[$letra];
                }
                else
                {
                    $disciplina_grupo=$disciplina_grupo.$grupo_escolhido[$letra];
                }
            }

            $id_disciplin_grup=$id_disciplina_grupo;
            $id_professo_grup=$id_professor_grupo;
            $disciplin_grup=$disciplina_grupo;


            //Grupo do destinat�rio
            $sql1="select disciplina.nome_disc from disciplina where disciplina.id_disc='$id_disciplina_grupo'";
            $resp1=mysql_query($sql1);
            $linha1 = mysql_fetch_row($resp1);
            $sql2="select usuario.nome_usuario from usuario where usuario.id_usuario='$id_professor_grupo'";
            $resp2=mysql_query($sql2);
            $linha2 = mysql_fetch_row($resp2);
            echo "<tr><td colspan=2 align=center><b>$linha1[0] - $linha2[0]</b></td></tr>";

            /*** Espa�o ***/
            //echo "<tr><td colspan=2>&nbsp</td></tr>";
        }
        else
        {
            if($tipogrupo_destinatario=="2")
            {
                $num_letras = strlen($grupo_escolhido);

                for ($letra=0; $letra <= $num_letras; $letra++)
                {
                    if ($grupo_escolhido[$letra]=="-")
                    {
                        $num_primeiro_simbolo = $letra;
                    }
                    if ($grupo_escolhido[$letra]=="*")
                    {
                        $num_segundo_simbolo = $letra;
                        break;
                    }
                }
                for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
                {
                    if($letra==0)
                    {
                        $id_curso_grupo=$grupo_escolhido[$letra];
                    }
                    else
                    {
                        $id_curso_grupo=$id_curso_grupo.$grupo_escolhido[$letra];
                    }
                }
                for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
                {
                    if($letra==$num_primeiro_simbolo + 1)
                    {
                        $id_professor_grupo=$grupo_escolhido[$letra];
                    }
                    else
                    {
                        $id_professor_grupo=$id_professor_grupo.$grupo_escolhido[$letra];
                    }
                }
                for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
                {
                    if($letra==$num_segundo_simbolo + 1)
                    {
                        $curso_grupo=$grupo_escolhido[$letra];
                    }
                    else
                    {
                        $curso_grupo=$curso_grupo.$grupo_escolhido[$letra];
                    }
                }

                $id_cur_grup=$id_curso_grupo;
                $id_professo_grup=$id_professor_grupo;
                $cur_grup=$curso_grupo;

                //Grupo do destinat�rio
                $sql1="select curso.nome_curso from curso where curso.id_curso='$id_curso_grupo'";
                $resp1=mysql_query($sql1);
                $linha1 = mysql_fetch_row($resp1);
                $sql2="select usuario.nome_usuario from usuario where usuario.id_usuario='$id_professor_grupo'";
                $resp2=mysql_query($sql2);
                $linha2 = mysql_fetch_row($resp2);
                echo "<tr><td colspan=2 align=center><b>$linha1[0] - $linha2[0]</b></td></tr>";

                /*** Espa�o ***/
                //echo "<tr><td colspan=2>&nbsp</td></tr>";
            }
            else
            {
                if($tipogrupo_destinatario=="3")
                {
                    $num_letras = strlen($grupo_escolhido);

                    for ($letra=0; $letra <= $num_letras; $letra++)
                    {
                        if ($grupo_escolhido[$letra]=="/")
                        {
                            $num_primeiro_simbolo = $letra;
                        }
                        if ($grupo_escolhido[$letra]=="-")
                        {
                            $num_segundo_simbolo = $letra;
                        }
                        if ($grupo_escolhido[$letra]=="*")
                        {
                            $num_terceiro_simbolo = $letra;
                            break;
                        }
                    }
                    for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
                    {
                        if($letra==0)
                        {
                            $id_disciplina_grupo=$grupo_escolhido[$letra];
                        }
                        else
                        {
                            $id_disciplina_grupo=$id_disciplina_grupo.$grupo_escolhido[$letra];
                        }
                    }
                    for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
                    {
                        if($letra==$num_primeiro_simbolo + 1)
                        {
                            $id_curso_grupo=$grupo_escolhido[$letra];
                        }
                        else
                        {
                            $id_curso_grupo=$id_curso_grupo.$grupo_escolhido[$letra];
                        }
                    }
                    for ($letra=$num_segundo_simbolo + 1; $letra <= $num_terceiro_simbolo - 1; $letra++)
                    {
                        if($letra==$num_segundo_simbolo + 1)
                        {
                            $id_professor_grupo=$grupo_escolhido[$letra];
                        }
                        else
                        {
                            $id_professor_grupo=$id_professor_grupo.$grupo_escolhido[$letra];
                        }
                    }
                    for ($letra=$num_terceiro_simbolo + 1; $letra <= $num_letras; $letra++)
                    {
                        if($letra==$num_terceiro_simbolo + 1)
                        {
                            $disccurso_grupo=$grupo_escolhido[$letra];
                        }
                        else
                        {
                            $disccurso_grupo=$disccurso_grupo.$grupo_escolhido[$letra];
                        }
                    }

                    $id_disciplin_grup=$id_disciplina_grupo;
                    $id_cur_grup=$id_curso_grupo;
                    $id_professo_grup=$id_professor_grupo;
                    $disciplincur_grup=$disccurso_grupo;

                    //Grupo do destinat�rio
                    $sql1="select disciplina.nome_disc from disciplina where disciplina.id_disc='$id_disciplina_grupo'";
                    $resp1=mysql_query($sql1);
                    $linha1 = mysql_fetch_row($resp1);
                    $sql2="select curso.nome_curso from curso where curso.id_curso='$id_curso_grupo'";
                    $resp2=mysql_query($sql2);
                    $linha2 = mysql_fetch_row($resp2);
                    $sql3="select usuario.nome_usuario from usuario where usuario.id_usuario='$id_professor_grupo'";
                    $resp3=mysql_query($sql3);
                    $linha3 = mysql_fetch_row($resp3);

                    echo "<tr><td colspan=2 align=center><b>$linha1[0] / $linha2[0] - $linha3[0]</b></td></tr>";

                    /*** Espa�o ***/
                    //echo "<tr><td colspan=2>&nbsp</td></tr>";

                }
                else
                {
                    echo "<tr><td colspan=2 align=center><b>$grupo_escolhido</b></td></tr>";

                    /*** Espa�o ***/
                    //echo "<tr><td colspan=2>&nbsp</td></tr>";
                }
            }
        }
        echo"</table></center>";

        ?>
        <!--- Inicia formul�rio --->
        <form name="formulariodestagend" action="n_index_agenda.php?opcao=Destinatario" method="post">
        <?

        /*** Tabela dos destinat�rios ***/
        echo "<table border=0>";

        echo "<tr><td valign=top align=left>".A_LANG_AGENDA_USUARIOS_GRUPO.":</td>";
        echo "<td valign=top><center><table border=0 cellpadding=0 cellspacing=0></td>";

        $conn = &ADONewConnection($A_DB_TYPE);
        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

        if($tipo_usuario=="root")
        {
            if($tipogrupo_destinatario==1)
            {
                $sql2="select disciplina.id_usuario, usuario.tipo_usuario, usuario.nome_usuario, usuario.email_usuario
                from disciplina, usuario
                where disciplina.id_usuario=usuario.id_usuario
                and disciplina.id_disc='$id_disciplina_grupo'
                and disciplina.id_usuario='$id_professor_grupo'";
                $res2 = mysql_query($sql2);
                $row2 = mysql_fetch_row($res2);

                $sql="select matricula.id_usuario, usuario.nome_usuario
                from matricula, usuario
                where matricula.id_usuario=usuario.id_usuario
                and matricula.id_disc='$id_disciplina_grupo'
                and matricula.id_usuario<>'$id_usuario'
                and matricula.id_usuario<>'$row2[0]'
                and status_mat =1
                order by nome_usuario";
                $res = mysql_query($sql);
                $quant= mysql_num_rows($res);

                if($quant==0)
                {
                    echo "<td valign=top colspan=2><font color=red>".A_LANG_AGENDA_NENHUM_USUARIO_DISCIPLINA."</td></tr>";
                    echo "<tr><td colspan=2>&nbsp</td></tr>";
                    echo "<tr><td><input class=button type=button value=\"".A_LANG_AGENDA_MUDAR_GRUPO_DESTINATARIO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\">&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_VOLTAR_AGENDA."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></td></tr>";
                    exit;
                }

                echo "<td align=left height=35>";
                echo"<select id=usugrup class=button NAME=usugrupo[] MULTIPLE size=20 maxsize=15>";

                $aux=$row2[0];
                $i=0;
                for($c=0; $c< $quantidade; $c++)
                {
                    if($aux==$dest[$c])
                    {
                        $i=1;
                        echo "<option selected id=opcao value=\"".$row2[0]."\">".$row2[1]." - ".$row2[2]."&nbsp;(".$row2[3].")"."</option>";
                    }
                }
                if($i==0)
                {
                    echo "<option id=opcao value=\"".$row2[0]."\">".$row2[1]." - ".$row2[2]."&nbsp;(".$row2[3].")"."</option>";
                }

                for ($cont=0; $cont< mysql_num_rows($res); $cont++)
                {
                    $row = mysql_fetch_row($res);
                    $aux=$row[0];
                    $i=0;
                    for($c=0; $c< $quantidade; $c++)
                    {
                        if($aux==$dest[$c])
                        {
                            $i=1;
                            $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$aux'";
                            $res1 = mysql_query($sql1);
                            $row1 = mysql_fetch_row($res1);
                            echo "<option selected id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";

                        }
                    }
                    if($i==0)
                    {
                        $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$row[0]'";
                        $res1 = mysql_query($sql1);
                        $row1 = mysql_fetch_row($res1);
                        echo "<option id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                    }
                }
            }
            else
            {
                if($tipogrupo_destinatario==2)
                {
                    $sql2="select curso.id_usuario, usuario.tipo_usuario, usuario.nome_usuario, usuario.email_usuario
                    from curso, usuario
                    where curso.id_usuario=usuario.id_usuario
                    and curso.id_curso='$id_curso_grupo'
                    and curso.id_usuario='$id_professor_grupo'";
                    $res2 = mysql_query($sql2);
                    $row2 = mysql_fetch_row($res2);

                    $sql="select matricula.id_usuario, usuario.nome_usuario
                    from matricula, usuario
                    where matricula.id_usuario=usuario.id_usuario
                    and matricula.id_curso='$id_curso_grupo'
                    and matricula.id_usuario<>'$id_usuario'
                    and matricula.id_usuario<>'$row2[0]'
                    and status_mat =1";
                    $res = mysql_query($sql);
                    $quant= mysql_num_rows($res);
                    if($quant==0)
                    {
                        echo "<td valign=top><font color=red>".A_LANG_AGENDA_NENHUM_USUARIO_CURSO."</td></tr>";
                        echo "<tr><td colspan=2>&nbsp</td></tr>";
                        echo "<tr><td><input class=button type=button value=\"".A_LANG_AGENDA_MUDAR_GRUPO_DESTINATARIO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\">&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_VOLTAR_AGENDA."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></td></tr>";
                        exit;
                    }
                    echo "<td align=left height=35>";
                    echo"<select id=usugrup class=button NAME=usugrupo[] MULTIPLE size=20 maxsize=15>";
                    $aux=$row2[0];
                    $i=0;
                    for($c=0; $c< $quantidade; $c++)
                    {
                        if($aux==$dest[$c])
                        {
                            $i=1;
                            echo "<option selected id=opcao value=\"".$row2[0]."\">".$row2[1]." - ".$row2[2]."&nbsp;(".$row2[3].")"."</option>";
                        }
                    }
                    if($i==0)
                    {
                        echo "<option id=opcao value=\"".$row2[0]."\">".$row2[1]." - ".$row2[2]."&nbsp;(".$row2[3].")"."</option>";
                    }

                    for ($cont=0; $cont< mysql_num_rows($res); $cont++)
                    {
                        $row = mysql_fetch_row($res);
                        $aux=$row[0];
                        $i=0;
                        for($c=0; $c< $quantidade; $c++)
                        {
                            if($aux==$dest[$c])
                            {
                                $i=1;
                                $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$aux'";
                                $res1 = mysql_query($sql1);
                                $row1 = mysql_fetch_row($res1);
                                echo "<option selected id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                            }
                        }
                        if($i==0)
                        {
                            $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$row[0]'";
                            $res1 = mysql_query($sql1);
                            $row1 = mysql_fetch_row($res1);
                            echo "<option id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                        }
                    }
                }
                else
                {
                    if($tipogrupo_destinatario==3)
                    {
                        $sql2="select curso_disc.id_usuario, usuario.tipo_usuario, usuario.nome_usuario, usuario.email_usuario
                        from curso_disc, usuario
                        where curso_disc.id_usuario=usuario.id_usuario
                        and curso_disc.id_disc='$id_disciplina_grupo'
                        and curso_disc.id_curso='$id_curso_grupo'
                        and curso_disc.id_usuario='$id_professor_grupo'";
                        $res2 = mysql_query($sql2);
                        $row2 = mysql_fetch_row($res2);

                        $sql="select matricula.id_usuario, usuario.nome_usuario
                        from matricula, usuario
                        where matricula.id_usuario=usuario.id_usuario
                        and matricula.id_disc='$id_disciplina_grupo'
                        and matricula.id_curso='$id_curso_grupo'
                        and matricula.id_usuario<>'$id_usuario'
                        and matricula.id_usuario<>'$row2[0]'
                        and status_mat =1
                        order by nome_usuario";
                        $res = mysql_query($sql);
                        $quant= mysql_num_rows($res);

                        if($quant==0)
                        {
                            echo "<td valign=top><font color=red>".A_LANG_AGENDA_NENHUM_USUARIO_DISCIPLINA_CURSO."</td></tr>";
                            echo "<tr><td colspan=2>&nbsp</td></tr>";
                            echo "<tr><td><input class=button type=button value=\"".A_LANG_AGENDA_MUDAR_GRUPO_DESTINATARIO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\">&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_VOLTAR_AGENDA."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></td></tr>";
                            exit;
                        }

                        echo "<td align=left height=35>";
                        echo"<select id=usugrup class=button name=usugrupo[] MULTIPLE size=20 maxsize=15>";
                        $aux=$row2[0];
                        $i=0;
                        for($c=0; $c< $quantidade; $c++)
                        {
                            if($aux==$dest[$c])
                            {
                                $i=1;
                                echo "<option selected id=opcao value=\"".$row2[0]."\">".$row2[1]." - ".$row2[2]."&nbsp;(".$row2[3].")"."</option>";
                            }
                        }
                        if($i==0)
                        {
                            echo "<option id=opcao value=\"".$row2[0]."\">".$row2[1]." - ".$row2[2]."&nbsp;(".$row2[3].")"."</option>";
                        }

                        for ($cont=0; $cont< mysql_num_rows($res); $cont++)
                        {
                            $row = mysql_fetch_row($res);
                            $aux=$row[0];
                            $i=0;
                            for($c=0; $c< $quantidade; $c++)
                            {
                                if($aux==$dest[$c])
                                {
                                    $i=1;
                                    $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$aux'";
				                    $res1 = mysql_query($sql1);
				                    $row1 = mysql_fetch_row($res1);
				                    echo "<option selected id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                                }
                            }
                            if($i==0)
                            {
                                $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$row[0]'";
				                $res1 = mysql_query($sql1);
				                $row1 = mysql_fetch_row($res1);
				                echo "<option id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                            }
                        }
                    }
                    else
                    {
                        if($tipogrupo_destinatario==5)
                        {
                            $sql="select usuario.id_usuario, usuario.nome_usuario
                            from usuario
                            where usuario.tipo_usuario='aluno'
                            order by nome_usuario";
                            $res = mysql_query($sql);
                            $quant= mysql_num_rows($res);
                            if($quant==0)
                            {
                                echo "<td valign=top><font color=red>".A_LANG_AGENDA_NENHUM_ALUNO_ADAPTWEB."</td></tr>";
                                echo "<tr><td colspan=2>&nbsp</td></tr>";
                                echo "<tr><td><input class=button type=button value=\"".A_LANG_AGENDA_MUDAR_GRUPO_DESTINATARIO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\">&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_VOLTAR_AGENDA."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></td></tr>";
                                exit;
                            }
                            echo "<td align=left height=35>";
                            echo"<select id=usugrup class=button name=usugrupo[] MULTIPLE size=20 maxsize=15>";
                            for ($cont=0; $cont< mysql_num_rows($res); $cont++)
                            {
                                $row = mysql_fetch_row($res);
                                $aux=$row[0];
                                $i=0;
                                for($c=0; $c< $quantidade; $c++)
                                {
                                    if($aux==$dest[$c])
                                    {
                                        $i=1;
                                        $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$aux'";
				                        $res1 = mysql_query($sql1);
				                        $row1 = mysql_fetch_row($res1);
				                        echo "<option selected id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                                    }
                                }
                                if($i==0)
                                {
                                    $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$row[0]'";
				                    $res1 = mysql_query($sql1);
				                    $row1 = mysql_fetch_row($res1);
				                    echo "<option id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                                }
                            }
                        }
                        else
                        {
                            if($tipogrupo_destinatario==6)
                            {
                                $sql="select usuario.id_usuario, usuario.nome_usuario
                                from usuario
                                where usuario.tipo_usuario='professor'
                                order by nome_usuario";
                                $res = mysql_query($sql);
                                $quant= mysql_num_rows($res);
                                if($quant==0)
                                {
                                    echo "<td valign=top><font color=red>".A_LANG_AGENDA_NENHUM_PROFESSOR_ADAPTWEB."</td></tr>";
                                    echo "<tr><td colspan=2>&nbsp</td></tr>";
                                    echo "<tr><td><input class=button type=button value=\"".A_LANG_AGENDA_MUDAR_GRUPO_DESTINATARIO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\">&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_VOLTAR_AGENDA."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></td></tr>";
                                    exit;
                                }
                                echo "<td align=left height=35>";
                                echo"<select id=usugrup class=button name=usugrupo[] MULTIPLE size=20 maxsize=15>";
                                for ($cont=0; $cont< mysql_num_rows($res); $cont++)
                                {
                                    $row = mysql_fetch_row($res);
                                    $aux=$row[0];
                                    $i=0;
                                    for($c=0; $c< $quantidade; $c++)
                                    {
                                        if($aux==$dest[$c])
                                        {
                                            $i=1;
                                            $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$aux'";
				                            $res1 = mysql_query($sql1);
				                            $row1 = mysql_fetch_row($res1);
				                            echo "<option selected id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                                        }
                                    }
                                    if($i==0)
                                    {
                                        $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$row[0]'";
				                        $res1 = mysql_query($sql1);
				                        $row1 = mysql_fetch_row($res1);
				                        echo "<option id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                                    }
                                }
                            }
                            else
                            {
                                if($tipogrupo_destinatario==7)
                                {
                                    $sql="select usuario.id_usuario, usuario.nome_usuario
                                    from usuario
                                    where usuario.tipo_usuario='professor'
                                    and usuario.nome_usuario <> ' '
                                    union
                                    select usuario.id_usuario, usuario.nome_usuario
                                    from usuario
                                    where usuario.tipo_usuario='aluno'
                                    and usuario.nome_usuario <> ' '
                                    order by nome_usuario";
                                    $res = mysql_query($sql);
                                    $quant= mysql_num_rows($res);
                                    if($quant==0)
                                    {
                                        echo "<td valign=top><font color=red>".A_LANG_AGENDA_NENHUM_USUARIO_ADAPTWEB."</td></tr>";
                                        echo "<tr><td colspan=2>&nbsp</td></tr>";
                                        echo "<tr><td><input class=button type=button value=\"".A_LANG_AGENDA_MUDAR_GRUPO_DESTINATARIO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\">&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_VOLTAR_AGENDA."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></td></tr>";
                                        exit;
                                    }
                                    echo "<td align=left height=35>";
                                    echo"<select id=usugrup class=button name=usugrupo[] MULTIPLE size=20 maxsize=15>";
                                    for ($cont=0; $cont< mysql_num_rows($res); $cont++)
                                    {
                                        $row = mysql_fetch_row($res);
                                        $aux=$row[0];
                                        $i=0;
                                        for($c=0; $c< $quantidade; $c++)
                                        {
                                            if($aux==$dest[$c])
                                            {
                                                $i=1;
                                                $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$aux'";
				                                $res1 = mysql_query($sql1);
				                                $row1 = mysql_fetch_row($res1);
				                                echo "<option selected id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                                            }
                                        }
                                        if($i==0)
                                        {
                                            $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$row[0]'";
				                            $res1 = mysql_query($sql1);
				                            $row1 = mysql_fetch_row($res1);
				                            echo "<option id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                                        }
                                    }
                                }
                                else
                                {
                                    if($tipogrupo_destinatario==8)
                                    {
                                        $sql="select usuario.id_usuario, usuario.nome_usuario
                                        from usuario
                                        where usuario.instituicao_ensino='$grupo_escolhido'
                                        order by nome_usuario";
                                        $res = mysql_query($sql);
                                        $quant= mysql_num_rows($res);
                                        if($quant==0)
                                        {
                                            echo "<td valign=top><font color=red>".A_LANG_AGENDA_NENHUM_USUARIO_DISCIPLINA_CURSO."</td></tr>";
                                            echo "<tr><td colspan=2>&nbsp</td></tr>";
                                            echo "<tr><td><input class=button type=button value=\"".A_LANG_AGENDA_MUDAR_GRUPO_DESTINATARIO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\">&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_VOLTAR_AGENDA."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></td></tr>";
                                            exit;
                                        }
                                        echo "<td align=left height=35>";
                                        echo"<select id=usugrup class=button name=usugrupo[] MULTIPLE size=20 maxsize=15>";
                                        for ($cont=0; $cont< mysql_num_rows($res); $cont++)
                                        {
                                            $row = mysql_fetch_row($res);
                                            $aux=$row[0];
                                            $i=0;
                                            for($c=0; $c< $quantidade; $c++)
                                            {
                                                if($aux==$dest[$c])
                                                {
                                                    $i=1;
                                                    $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$aux'";
				                                    $res1 = mysql_query($sql1);
				                                    $row1 = mysql_fetch_row($res1);
				                                    echo "<option selected id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                                                }
                                            }
                                            if($i==0)
                                            {
                                                $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$row[0]'";
				                                $res1 = mysql_query($sql1);
				                                $row1 = mysql_fetch_row($res1);
				                                echo "<option id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if($tipo_usuario=="professor")
        {
            if($tipogrupo_destinatario==1)
            {
                $sql="select matricula.id_usuario, usuario.nome_usuario
                from matricula, usuario
                where matricula.id_usuario=usuario.id_usuario
                and matricula.id_disc='$id_disciplina_grupo'
                and matricula.id_usuario<>'$id_usuario'
                and status_mat =1
                order by nome_usuario";

                //$res = $conn->Execute($sql);
                $res = mysql_query($sql);
                $quant= mysql_num_rows($res);
                if($quant==0)
                {
                    echo "<td valign=top colspan=2><font color=red>".A_LANG_AGENDA_NENHUM_USUARIO_DISCIPLINA."</td></tr>";
                    echo "<tr><td colspan=2>&nbsp</td></tr>";
                    echo "<tr><td><input class=button type=button value=\"".A_LANG_AGENDA_MUDAR_GRUPO_DESTINATARIO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\">&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_VOLTAR_AGENDA."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></td></tr>";
                    exit;
                }
                echo "<td align=left height=35>";
                echo"<select id=usugrup class=button NAME=usugrupo[] MULTIPLE size=20 maxsize=15>";
                for ($cont=0; $cont< mysql_num_rows($res); $cont++)
                {
                    $row = mysql_fetch_row($res);
                    $aux=$row[0];
                    $i=0;
                    for($c=0; $c< $quantidade; $c++)
                    {
                        if($aux==$dest[$c])
                        {
                            $i=1;
                            $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$aux'";
                            $res1 = mysql_query($sql1);
                            $row1 = mysql_fetch_row($res1);
                            echo "<option selected id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                        }
                    }
                    if($i==0)
                    {
                        $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$row[0]'";
                        $res1 = mysql_query($sql1);
                        $row1 = mysql_fetch_row($res1);
                        echo "<option id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                    }
                }
            }
            else
            {
                if($tipogrupo_destinatario==2)
                {
                    $sql="select matricula.id_usuario, usuario.nome_usuario
                    from matricula, usuario
                    where matricula.id_usuario=usuario.id_usuario
                    and matricula.id_curso='$id_curso_grupo'
                    and matricula.id_usuario<>'$id_usuario'
                    and status_mat =1
                    order by nome_usuario";
                    $res = mysql_query($sql);
                    $quant= mysql_num_rows($res);
                    if($quant==0)
                    {
                        echo "<td valign=top><font color=red>".A_LANG_AGENDA_NENHUM_USUARIO_CURSO."</td></tr>";
                        echo "<tr><td colspan=2>&nbsp</td></tr>";
                        echo "<tr><td><input class=button type=button value=\"".A_LANG_AGENDA_MUDAR_GRUPO_DESTINATARIO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\">&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_VOLTAR_AGENDA."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></td></tr>";
                        exit;
                    }
                    echo "<td align=left height=35>";
                    echo"<select id=usugrup class=button NAME=usugrupo[] MULTIPLE size=20 maxsize=15>";
                    for ($cont=0; $cont< mysql_num_rows($res); $cont++)
                    {
                        $row = mysql_fetch_row($res);
                        $aux=$row[0];
                        $i=0;
                        for($c=0; $c< $quantidade; $c++)
                        {
                            if($aux==$dest[$c])
                            {
                                $i=1;
                                $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$aux'";
                                $res1 = mysql_query($sql1);
                                $row1 = mysql_fetch_row($res1);
                                echo "<option selected id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                            }
                        }
                        if($i==0)
                        {
                            $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$row[0]'";
                            $res1 = mysql_query($sql1);
                            $row1 = mysql_fetch_row($res1);
                            echo "<option id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                        }
                    }
                }
                else
                {
                    if($tipogrupo_destinatario==3)
                    {
                        $sql="select curso_disc.id_usuario, nome_usuario
                        from curso_disc, usuario
                        where curso_disc.id_usuario=usuario.id_usuario
                        and curso_disc.id_disc='$id_disciplina_grupo'
                        and curso_disc.id_curso='$id_curso_grupo'
                        and curso_disc.id_usuario<>'$id_usuario'
                        union
                        select matricula.id_usuario, usuario.nome_usuario
                        from matricula, usuario
                        where matricula.id_usuario=usuario.id_usuario
                        and matricula.id_disc='$id_disciplina_grupo'
                        and matricula.id_curso='$id_curso_grupo'
                        and matricula.id_usuario<>'$id_usuario'
                        and status_mat =1
                        order by nome_usuario";
                        $res = mysql_query($sql);
                        $quant= mysql_num_rows($res);
                        if($quant==0)
                        {
                            echo "<td valign=top><font color=red>".A_LANG_AGENDA_NENHUM_USUARIO_DISCIPLINA_CURSO."</td></tr>";
                            echo "<tr><td colspan=2>&nbsp</td></tr>";
                            echo "<tr><td><input class=button type=button value=\"".A_LANG_AGENDA_MUDAR_GRUPO_DESTINATARIO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\">&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_VOLTAR_AGENDA."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></td></tr>";
                            exit;
                        }
                        echo "<td align=left height=35>";
                        echo"<select id=usugrup class=button name=usugrupo[] MULTIPLE size=20 maxsize=15>";

                        for ($cont=0; $cont< mysql_num_rows($res); $cont++)
                        {
                            $row = mysql_fetch_row($res);
                            $la=$row[0];
                            $i=0;
                            for($c=0; $c< $quantidade; $c++)
                            {
                                if($la==$dest[$c])
                                {
                                    $i=1;
                                    $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$la'";
				                    $res1 = mysql_query($sql1);
				                    $row1 = mysql_fetch_row($res1);
				                    echo "<option selected id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                                }
                            }
                            if($i==0)
                            {
                                $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$row[0]'";
				                $res1 = mysql_query($sql1);
				                $row1 = mysql_fetch_row($res1);
				                echo "<option id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                            }
                        }
                    }
                }
            }
        }

        if($tipo_usuario=="aluno")
        {
            $sql="select matricula.id_usuario, usuario.nome_usuario
            from matricula, usuario
            where matricula.id_usuario=usuario.id_usuario
            and matricula.id_disc='$id_disciplina_grupo'
            and matricula.id_curso='$id_curso_grupo'
            and matricula.id_usuario<>'$id_usuario'
            and status_mat =1
            order by nome_usuario";
            $res = mysql_query($sql);
            $quant= mysql_num_rows($res);
            $quant= mysql_num_rows($res);
            if($quant==0)
            {
                echo "<td valign=top><font color=red>".A_LANG_AGENDA_NENHUM_USUARIO_DISCIPLINA_CURSO."</td></tr>";
                echo "<tr><td colspan=2>&nbsp</td></tr>";
                echo "<tr><td><input class=button type=button value=\"".A_LANG_AGENDA_MUDAR_GRUPO_DESTINATARIO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\">&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_VOLTAR_AGENDA."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></td></tr>";
                exit;
            }
            echo "<td align=left height=35>";
            echo"<select id=usugrup class=button name=usugrupo[] MULTIPLE size=20 maxsize=15>";
            for ($cont=0; $cont< mysql_num_rows($res); $cont++)
            {
                $row = mysql_fetch_row($res);
                $aux=$row[0];
                $i=0;
                for($c=0; $c< $quantidade; $c++)
                {
                    if($aux==$dest[$c])
                    {
                        $i=1;
                        $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$aux'";
                        $res1 = mysql_query($sql1);
                        $row1 = mysql_fetch_row($res1);
                        echo "<option selected id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                    }
                }
                if($i==0)
                {
                    $sql1 = "SELECT nome_usuario, email_usuario FROM usuario WHERE id_usuario='$row[0]'";
                    $res1 = mysql_query($sql1);
                    $row1 = mysql_fetch_row($res1);
                    echo "<option id=opcao value=\"".$row[0]."\">".$row1[0]."&nbsp;(".$row1[1].")"."</option>";
                }
            }
        }

        echo "</select><td align=center><table border=0>";
        echo"<tr><td align=center>".A_LANG_AGENDA_SELECIONAR_USUARIO_GRUPO."</td></tr>";

        echo"<tr><td align=center><input class=button type=button value=\"".A_LANG_AGENDA_SELECIONAR_TODOS_USUARIOS."\" onClick=selecionar('usugrup')></td></tr></tr>";

        echo"</table></table></table>";

        echo"<table border=0>";

        //Espa�os
        echo "<tr><td colspan=2>&nbsp</td></tr>";

        /*** Data do evento ***/
        echo "<tr><td>".A_LANG_AGENDA_DATA_EVENTO."*:</td>";
        echo "<td><input class=button size=9 name=dataevento id=demo1 value=\"".$data_evento."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\">";

        ?>
        <script>
        function horaConta(c)
        {
            if(c.value.length==2)
            {
                c.value += ':';
            }
        }
        </script>
        <?

        /*** Hora in�cio ***/
        echo"<tr><td>".A_LANG_AGENDA_HORA_INICIO."*:</td><td><input class=text type=text size=3 name=horainicio onkeyup=horaConta(this); value=\"".$hora_inicio."\"></td></tr>";

        /*** Hora fim ***/
        echo "<tr><td>".A_LANG_AGENDA_HORA_FIM."*:</td><td><input class=text type=text size=3 name=horafim onkeyup=horaConta(this); value=\"".$hora_fim."\">";

        /*** Local do evento ***/
        echo "<tr><td>".A_LANG_AGENDA_LOCAL_EVENTO.":</td><td><textarea name=localevento class=button cols=70 rows=3>$local_evento</textarea></td></tr>";

        /*** Repeti��o ***/
        echo "<tr><td disabled>".A_LANG_AGENDA_REPETICAO_EVENTO.":</td>";
        echo"<td><input type=radio name=naorepetir value='naorepetir' disabled> ".A_LANG_AGENDA_NAO_REPETIR."";
        echo"<input type=radio name=diariamente value='diariamente' disabled> ".A_LANG_AGENDA_DIARIAMENTE."";
        echo"<input type=radio name=semanalmente value='semanalmente' disabled> ".A_LANG_AGENDA_SEMANALMENTE."";
        echo"<input type=radio name=mensalmente value='mensalmente' disabled> ".A_LANG_AGENDA_MENSALMENTE."";
        echo"<input type=radio name=Anualmente value='anualmente' disabled> ".A_LANG_AGENDA_ANUALMENTE."";
        echo"</input> ";
        echo"<select name=quantidadevezes class=select size=1 disabled>";
        echo "<option selected value=\"0\">".A_LANG_AGENDA_QUANTIDADE_REPETICOES."</option>";
        echo "<option value=\"1\">".A_LANG_AGENDA_1X."</option>";
        echo "<option value=\"2\">".A_LANG_AGENDA_2X."</option>";
        echo "<option value=\"3\">".A_LANG_AGENDA_3X."</option>";
        echo "<option value=\"4\">".A_LANG_AGENDA_4X."</option>";
        echo "<option value=\"5\">".A_LANG_AGENDA_5X."</option>";
        echo "<option value=\"6\">".A_LANG_AGENDA_6X."</option>";
        echo "<option value=\"7\">".A_LANG_AGENDA_7X."</option>";
        echo "<option value=\"8\">".A_LANG_AGENDA_8X."</option>";
        echo "<option value=\"9\">".A_LANG_AGENDA_9X."</option>";
        echo "<option value=\"10\">".A_LANG_AGENDA_10X."</option>";
        echo"</select> ";
        echo"".A_LANG_AGENDA_OU."</td></tr>";

        echo"<tr><td></td><td disabled>".A_LANG_AGENDA_DATA_REPETI��O.": <input class=button size=9 name=datarepeticao id=demo1 value=\"".$data_repeticao."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\" disabled></td></tr>";

        /*** E-mail de Aviso ***/
        echo "<tr><td disabled>".A_LANG_AGENDA_AVISO_EMAIL.":</td>";
        echo"<td><input type=radio name=naoenviar value='naoenviar' disabled> ".A_LANG_AGENDA_NAO_ENVIAR."";
        echo"<input type=radio name=dia value='dia' disabled> ".A_LANG_AGENDA_AVISO_UM_DIA."";
        echo"<input type=radio name=semana value='semana' disabled> ".A_LANG_AGENDA_AVISO_UMA_SEMANA."";
        echo"<input type=radio name=mes value='mes' disabled> ".A_LANG_AGENDA_AVISO_UM_MES."";
        echo"</input> ";
        echo"".A_LANG_AGENDA_OU."</td></tr>";

        echo"<tr><td></td><td disabled>".A_LANG_AGENDA_DATA_ENVIO_AVISO.": <input class=button size=9 name=dataaviso id=demo1 value=\"".$data_aviso."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\" disabled></td></tr>";

        echo"</table>";

        //Bot�es cadastrar e cancelar
        echo "<table border=0 align=left>";

        //Espa�os
        echo "<tr><td colspan=2>&nbsp</td></tr>";

        echo "<tr valign=top><td colspan=2>";
        echo "<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_MUDAR_GRUPO_DESTINATARIO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\"></input>";
        echo "&nbsp<input name=agendaroutros id=bot1 class=button type=submit value=\"".A_LANG_AGENDA_AGENDAR."\"></input>";
        echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input>";
        echo "</td></tr>";
        echo"</form>";
        //Espa�os
        echo "<tr><td colspan=2>&nbsp</td></tr>";

        /*** Observa��o ***/
        echo"<tr><td colspan=2><b>".A_LANG_AGENDA_OBS_CAMPO_INCOMPLETO."</b></td></tr>";
        echo"</table>";
        $tipogrupo_dest=$tipogrupo_destinatario;
        $grupo_esco=$grupo_escolhido;
    }
}

?>

   </td>
  </table>
  </td>
  </tr>
</table>
