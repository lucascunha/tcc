<?

/* ---------------------------------------------------------------------
 * Pagina inicial da agenda                                            *
 * @author Carina Tissa Aihara <tissa.aihara@gmail.com>                *
 * @version 1.0 <01/11/2009>                                           *
 *                                                                     *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.   *
 * Como Estagio Curricular.                                            *
 * Orientadora: Avanilde Kemczinski                                    *
 * Suporvisora: Edino Lopes Fernandes                                  *
 *                                                                     *
 ----------------------------------------------------------------------*/

?>
<script language="javascript">
<!--- Aparece o campo de vis�o --->
function ApareceTipoVisao()
{
    document.formulariotipovisao.submit()
}
<!--- Aparece o calend�io de acordo com o tipo da vis�o e a vis�o escolhida --->
function ApareceVisao()
{
    document.formulariovisao.submit()
}
</script>

<?

/*** Estabelecer conex�o com o banco de dados ***/
include "config/configuracoes.php";
$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;
include "include/conecta.php";



/*** O usu�rio 2 � utilizado para demonstra��o do ambiente Adaptweb,
n�o podendo incluir, excluir ou alterar dados ***/
if  ($id_usuario == 2)
{
    echo "<p class=\"texto5\">\n";
    echo A_LANG_AGENDA_DEMO;
    echo "</p>\n";
}



/*** Cria matriz de orelha ***/
$orelha = array();
$orelha = array
(
    array
    (
        "LABEL" => A_LANG_MNU_AGENDA,
        "LINK" => "index.php",
        "ESTADO" =>"ON"
    )
);

MontaOrelha($orelha);



?>

<!--- Tirar o sublinhado dos links -->
<style type="text/css">
<!--
a:link {
	text-decoration: none;
}
a:visited {
	text-decoration: none;
}
a:hover {
	text-decoration: none;
}
a:active {
	text-decoration: none;
}
-->
</style>



<!--- Tabela do fundo -->
<table CELLSPACING="0" CELLPADDING="0" border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> height="80%">
 <tr valign="top">
  <td>
    <br>
     <table border="0">
       <br>
         <td valign="top" width="21%">

<?

/*** Inciciando a session ***/
session_start();
session_register("mes");
session_register("ano");
session_register("posicao_primeiro_dia_mes");
session_register("posicao_ultimo_dia_mes");
session_register("tip_vis");
session_register("vis");



/*** Pegar o dia, o m�s e ano atual, e colocar nas vari�veis correspondentes ***/
$dia_hoje = date ("d");
$mes_hoje = date ("m");
$ano_hoje = date ("Y");



/*** Copiando o m�s e o ano atual para as vari�veis de tipo session m�s e ano ***/
$mes = $mes_hoje;
$ano = $ano_hoje;


/*** Obter o dia da semana correspondente ao primeiro dia do m�s ***/
$dia_da_semana = mktime (0,0,0,$mes_hoje, 1, $ano_hoje);
$semana_primeiro_dia_mes = date('w', $dia_da_semana);



/*** Verificar qual � o �ltimo dia do m�s ***/
$ultimo_dia_mes_hoje = 28;
for ($ultimo_dia_mes = $ultimo_dia_mes_hoje; $ultimo_dia_mes < 32; $ultimo_dia_mes++)
{
	if (checkdate ($mes_hoje, $ultimo_dia_mes, $ano_hoje))
	{
		$ultimo_dia_mes_hoje = $ultimo_dia_mes;
	}
}



/*** Descobrir a posi��o do primeiro e do �ltimo dia do m�s ***/
if($ultimo_dia_mes_hoje == 28)
{
	if($semana_primeiro_dia_mes == 0)
	{
        $posicao_primeiro_dia_mes = $semana_primeiro_dia_mes;
		$posicao_ultimo_dia_mes = 6;
	}
	else
	{
	    $posicao_primeiro_dia_mes = $semana_primeiro_dia_mes;
		$posicao_ultimo_dia_mes = $semana_primeiro_dia_mes - 1;
	}
}
if($ultimo_dia_mes_hoje == 29)
{
	$posicao_primeiro_dia_mes = $semana_primeiro_dia_mes;
	$posicao_ultimo_dia_mes = $semana_primeiro_dia_mes;
}
if($ultimo_dia_mes_hoje == 30)
{
	if($semana_primeiro_dia_mes == 6)
	{
	    $posicao_primeiro_dia_mes = $semana_primeiro_dia_mes;
		$posicao_ultimo_dia_mes = 0;
	}
	else
	{
	    $posicao_primeiro_dia_mes = $semana_primeiro_dia_mes;
		$posicao_ultimo_dia_mes = $semana_primeiro_dia_mes + 	1;
	}
}
if($ultimo_dia_mes_hoje == 31)
{
	if($semana_primeiro_dia_mes == 5)
	{
	    $posicao_primeiro_dia_mes = $semana_primeiro_dia_mes;
		$posicao_ultimo_dia_mes = 0;
	}
	else
	{
		if($semana_primeiro_dia_mes == 6)
		{
			$posicao_primeiro_dia_mes = $semana_primeiro_dia_mes;
			$posicao_ultimo_dia_mes = 1;
		}
		else
		{
			$posicao_primeiro_dia_mes = $semana_primeiro_dia_mes;
			$posicao_ultimo_dia_mes = $semana_primeiro_dia_mes + 2;
		}
	}
}



/*** Pegando o n�mero referente ao m�s e transformando em seu respectivo nome ***/
if (strcasecmp ($mes_hoje, '01') == 0)
{
	$nome_mes_hoje = "Janeiro";
}
if (strcasecmp ($mes_hoje, '02') == 0)
{
	$nome_mes_hoje = "Fevereiro";
}
if (strcasecmp ($mes_hoje, '03') == 0)
{
	$nome_mes_hoje = "Mar�o";
}
if (strcasecmp ($mes_hoje, '04') == 0)
{
	$nome_mes_hoje = "Abril";
}
if (strcasecmp ($mes_hoje, '05') == 0)
{
	$nome_mes_hoje = "Maio";
}
if (strcasecmp ($mes_hoje, '06') == 0)
{
	$nome_mes_hoje = "Junho";
}
if (strcasecmp ($mes_hoje, '07') == 0)
{
	$nome_mes_hoje = "Julho";
}
if (strcasecmp ($mes_hoje, '08') == 0)
{
	$nome_mes_hoje = "Agosto";
}
if (strcasecmp ($mes_hoje, '09') == 0)
{
	$nome_mes_hoje = "Setembro";
}
if (strcasecmp ($mes_hoje, '10') == 0)
{
	$nome_mes_hoje = "Outubro";
}
if (strcasecmp ($mes_hoje, '11') == 0)
{
	$nome_mes_hoje = "Novembro";
}
if (strcasecmp ($mes_hoje, '12') == 0)
{
	$nome_mes_hoje = "Dezembro";
}



/*** Montando o p�gina do calend�rio ***/

/*** Tabela para montar a p�gina da agenda ***/
echo"<table border=0 width=100%>";



/*** Ajuda sobre a agenda ***/
//echo"<right><tr><td align=right><b><a href='n_index_agenda.php?opcao=AjudaAgenda'>".A_LANG_AGENDA_AJUDA."</a></b></td></tr></right>";



/*** Linha de espa�o ***/
//echo"<tr><td colspan=2>&nbsp</td></tr>";



/*** Campo de busca ***/
//echo"<tr><td align=right>".A_LANG_AGENDA_BUSCA."<input class=\"text\" type=\"text\" name=busca size=20 disabled>&nbsp<input class=button type=submit value=\"".A_LANG_AGENDA_OK."\" disabled></td></tr>";



/*** Colocar o tipo visao selecionado na vari�vel $tipo_visao ***/
if(isset($_POST["tipovisao"]))
{
    $tipo_visao=$_POST["tipovisao"];
}



/*** Deixar o tipo da vis�o selecionado ***/
if(isset($tipo_visao))
{
    if($tipo_visao==0)
    {
        $aux_seleciona_tipo_visao="selected";
        $tip_vis=$tipo_visao;
    }
    if($tipo_visao==1)
    {
        $aux_tipo_visao_partic="selected";
        $tip_vis=$tipo_visao;
    }
    if($tipo_visao==2)
    {
        $aux_tipo_visao_disc="selected";
        $tip_vis=$tipo_visao;
    }
    if($tipo_visao==3)
    {
        $aux_tipo_visao_curso="selected";
        $tip_vis=$tipo_visao;
    }
    if($tipo_visao==4)
    {
        $aux_tipo_visao_disc_curso="selected";
        $tip_vis=$tipo_visao;
    }
    if($tipo_visao==5)
    {
        $aux_tipo_visao_adapt="selected";
        $tip_vis=$tipo_visao;
    }
}



/*** Form para o tipo da vis�o do calend�rio ***/
//echo"<form name=formulariotipovisao action='n_index_agenda.php?opcao=AcessoAgenda&mes=$mes_hoje&ano=$ano_hoje' method=post>";



/*** Tipo de vis�es do calend�rio ***/
//echo"<tr><td align=center>".A_LANG_AGENDA_TIPO_VISAO.":&nbsp<select name=tipovisao class=select size=1 onchange=ApareceTipoVisao()>";
//echo "<option $aux_seleciona_tipo_visao value=\"0\">".A_LANG_AGENDA_TODAS_VISOES."</option>";
//if($tipo_usuario=="aluno")
//{
//    echo "<option $aux_tipo_visao_partic value=\"1\">".A_LANG_AGENDA_PARTICULAR."</option>";
//    echo "<option $aux_tipo_visao_curso value=\"3\">".A_LANG_AGENDA_CURSO."</option>";
//    echo "<option $aux_tipo_visao_disc_curso value=\"4\">".A_LANG_AGENDA_DISCIPLINA_CURSO."</option>";
//    echo "<option $aux_tipo_visao_adapt value=\"5\">".A_LANG_AGENDA_AMBIENTE_ADAPTWEB."</option>";
//
//}
//else
//{
//    echo "<option $aux_tipo_visao_partic value=\"1\">".A_LANG_AGENDA_PARTICULAR."</option>";
//    echo "<option $aux_tipo_visao_disc value=\"2\">".A_LANG_AGENDA_DISCIPLINA."</option>";
//    echo "<option $aux_tipo_visao_curso value=\"3\">".A_LANG_AGENDA_CURSO."</option>";
//    echo "<option $aux_tipo_visao_disc_curso value=\"4\">".A_LANG_AGENDA_DISCIPLINA_CURSO."</option>";
//    echo "<option $aux_tipo_visao_adapt value=\"5\">".A_LANG_AGENDA_AMBIENTE_ADAPTWEB."</option>";
//}
//echo "</select>";
//echo"</td></tr>";



/*** Fim do form para o tipo da vis�o do calend�rio ***/
//echo"</form>";










/*** Se existir o tipo da vis�o do calend�rio ***/
if(isset ($tipo_visao))
{

    /*** Se o tipo da vis�o for igual a 1 ***/
    if($tipo_visao==1)
    {
        echo"<form name=formulariovisao action='n_index_agenda.php?opcao=AcessoAgenda&mes=$mes_hoje&ano=$ano_hoje&tipo_visao=$tipo_visao' method=post>";


        $sql1="select DISTINCT evento_agenda.id_tipo_assunto, evento_agenda.assunto_evento
	    from evento_agenda, destinatario_agendamento_agenda, agendamento_evento_agenda
	    where evento_agenda.id_evento_agenda = agendamento_evento_agenda.id_evento_agenda
	    and agendamento_evento_agenda.id_agendamento_evento = destinatario_agendamento_agenda.id_agendamento_evento
	    and destinatario_agendamento_agenda.id_usuario='$id_usuario'
	    and evento_agenda.id_tipo_assunto='1'";
	    $resp1=mysql_query($sql1);
	    $linha1=mysql_num_rows($resp1);
	    echo"<tr><td align=center>".A_LANG_AGENDA_VISOES_CALENDARIO."&nbsp";
        echo "<select name=visoes class=select size=1 onchange=ApareceVisao()>";
        echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONAR_VISAO."</option>";
        while ($array = mysql_fetch_array($resp1))
        {
	        if($vis==$array[1] || $visoes==$array[1])
            {
           	    echo "<option selected value=\"".$array[1]."\">".$array[1]."</option>";
           	}
           	else
           	{
           	    echo "<option value=\"".$array[1]."\">".$array[1]."</option>";
           	}
        }
   		echo "</select>";

        echo"</form>";

        if(isset ($visoes))
        {
            /*** Monta calend�rio com a vis�o escolhida ***/
       	    if($visoes != NULL && $visoes!="0")
           	{
           	    /*** Espa�o ***/
               	echo"<tr><td colspan=2>&nbsp</td></tr>";


                /*** Tabela do calend�rio ***/
               	echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


                /*** Primeira linha do calend�rio ***/
                echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
               	<td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
               	<td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                /*** Linha dos dias da semana do calend�rio ***/
               	echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
               	<td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";

                /*** Dias do calend�rio ***/
               	$contador = 0;
               	$dia = 1;
               	echo "<tr class=zebraA>";
               	if ($contador == 0)
               	{
               	    for ($cont = 0; $cont < $semana_primeiro_dia_mes; $cont++)
               	    {
               	        echo "<td>&nbsp</td>";
      	            }
              		for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
              		{
              		    $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                  		$conn = &ADONewConnection($A_DB_TYPE);
                  		$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                        $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                        from destinatario_agendamento_agenda,
                        agendamento_evento_agenda, evento_agenda
                        where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                        and evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
                        and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                        and agendamento_evento_agenda.data_evento='$dtevento'
                        and evento_agenda.assunto_evento='$visoes'";
                        $resp2=mysql_query($sql2);
                        $linha2=mysql_num_rows($resp2);

                        if($linha2>0)
                        {
                            if($dia==$dia_hoje)
                            {
                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                <font color='purple'>$dia</font></a></strong></center></td>";
                                $dia = $dia +1;
                                $contador = $contador + 1;
                            }
                            else
                            {
                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                <font color='red'>$dia</font></a></strong></center></td>";
                                $dia = $dia +1;
                                $contador = $contador + 1;
                            }
                        }
                        else
                        {
                            if ($dia == $dia_hoje)
                            {
                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                <font color='blue'>$dia</font></a></strong></center></td>";
                                $dia = $dia +1;
                                $contador = $contador + 1;
                            }
                            else
                            {
                                echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                $dia = $dia +1;
                                $contador = $contador + 1;
                            }
                        }
                    }
                }
               	if($contador != 0)
               	{
               	    do
               	    {
               	        echo "</tr>";
               	        echo "<tr class=zebraA>";
                        for ($cont= 0; $cont < 7; $cont++)
          		        {
     		        	    $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                 			$conn = &ADONewConnection($A_DB_TYPE);
                 			$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                            $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                            from destinatario_agendamento_agenda,
                            agendamento_evento_agenda, evento_agenda
                            where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                            and evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
                            and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                            and agendamento_evento_agenda.data_evento='$dtevento'
                            and evento_agenda.assunto_evento='$visoes'";
                            $resp2=mysql_query($sql2);
                            $linha2=mysql_num_rows($resp2);

                            if($linha2>0)
                            {
                                if($dia==$dia_hoje)
                                {
                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                    <font color='purple'>$dia</font></a></strong></center></td>";
                                    $dia = $dia +1;
                                    $contador = $contador + 1;
                                }
                                else
                                {
                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                    <font color='red'>$dia</font></a></strong></center></td>";
                                    $dia = $dia +1;
                                    $contador = $contador + 1;
                                }
                                if ($dia > $ultimo_dia_mes_hoje )
                                {
                                    if ($posicao_ultimo_dia_mes != 6)
                                    {
                                        for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
     			                        {
				                             echo "<td>&nbsp</td>";
				                        }
                                    }
				                    break;
                                }
                            }
                            else
                            {
                                if ($dia == $dia_hoje)
                                {
                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                    <font color='blue'>$dia</font></a></strong></center></td>";
                                    $dia = $dia +1;
                                }
                                else
                                {
                                    echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                    $dia = $dia +1;
                                }
                                if ($dia > $ultimo_dia_mes_hoje )
                                {
                                    if ($posicao_ultimo_dia_mes != 6)
                                    {
                                        for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
     			                        {
				                            echo "<td>&nbsp</td>";
				                        }
                                    }
				                    break;
                                }
                            }
                        }
                        echo "</tr>";
                    }while ($dia <= $ultimo_dia_mes_hoje);
                }
                echo "</table>";//Encerrando a tabela do calend�rio


                /*** Espa�os ***/
                echo "<tr><td colspan=2>&nbsp</td></tr>";
                echo "<tr><td colspan=2>&nbsp</td></tr>";


                echo "</table>";//Encerra tabela para montar a p�gina da agenda
            }
            else
            {
                /*** Monta o calend�rio com todas as vis�es ***/
                if($visoes=="0")
                {
                    /*** Espa�o ***/
                    echo"<tr><td colspan=2>&nbsp</td></tr>";


                    /*** Tabela do calend�rio ***/
                    echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


                    /*** Primeira linha do calend�rio ***/
                    echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
                    <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
                    <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                    /*** Linha dos dias da semana do calend�rio ***/
                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
                    <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                    <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


                    /*** Dias do calend�rio ***/
                    $contador = 0;
                    $dia = 1;
                    echo "<tr class=zebraA>";
                    if ($contador == 0)
                    {
	                    for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	                    {
		                    echo "<td>&nbsp</td>";
                        }
                        for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                        {
                            $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                            $conn = &ADONewConnection($A_DB_TYPE);
                            $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                            $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                            from destinatario_agendamento_agenda,
                            agendamento_evento_agenda
                            where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                            and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                            and agendamento_evento_agenda.data_evento='$dtevento'";
                            $resp2=mysql_query($sql2);
                            $linha2=mysql_num_rows($resp2);

                            if($linha2>0)
                            {
                                if($dia==$dia_hoje)
                                {
                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                    <font color='purple'>$dia</font></a></strong></center></td>";
                                    $dia = $dia +1;
                                    $contador = $contador + 1;
                                }
                                else
                                {
                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                    <font color='red'>$dia</font></a></strong></center></td>";
                                    $dia = $dia +1;
                                    $contador = $contador + 1;
                                }
                            }
                            else
                            {
                                if ($dia == $dia_hoje)
                                {
                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                    <font color='blue'>$dia</font></a></strong></center></td>";
                                    $dia = $dia +1;
                                    $contador = $contador + 1;
                                }
                                else
                                {
                                    echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                    $dia = $dia +1;
                                    $contador = $contador + 1;
                                }
                            }
	                    }
                    }
                    if($contador != 0)
                    {
	                    do
	                    {
		                    echo "</tr>";
		                    echo "<tr class=zebraA>";
                            for ($cont= 0; $cont < 7; $cont++)
                            {
                                $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                $conn = &ADONewConnection($A_DB_TYPE);
                                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                from destinatario_agendamento_agenda,
                                agendamento_evento_agenda
                                where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                and agendamento_evento_agenda.data_evento='$dtevento'";
                                $resp2=mysql_query($sql2);
                                $linha2=mysql_num_rows($resp2);

                                if($linha2>0)
                                {
                                    if($dia==$dia_hoje)
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='purple'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                    else
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='red'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                    if ($dia > $ultimo_dia_mes_hoje )
                                    {
                                        if ($posicao_ultimo_dia_mes != 6)
                                        {
                                            for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                {
                                                echo "<td>&nbsp</td>";
                                            }
                                        }
				                        break;
                                    }
                                }
                                else
                                {
                                    if ($dia == $dia_hoje)
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='blue'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                    }
                                    else
                                    {
                                        echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                        $dia = $dia +1;
                                    }
                                    if ($dia > $ultimo_dia_mes_hoje )
                                    {
                                        if ($posicao_ultimo_dia_mes != 6)
                                        {
                                            for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
     			                            {
				                                echo "<td>&nbsp</td>";
				                            }
                                        }
				                        break;
                                    }
                                }
                            }
                            echo "</tr>";
                        }while ($dia <= $ultimo_dia_mes_hoje);
                    }
                    echo "</table>";//Encerrando a tabela do calend�rio


                    /*** Espa�os ***/
                    echo "<tr><td colspan=2>&nbsp</td></tr>";
                    echo "<tr><td colspan=2>&nbsp</td></tr>";


                    echo "</table>";//Encerra tabela para montar a p�gina da agenda
                }
            }
            if(isset($visoes))
            {
                $vis=$visoes;
            }
        }
        else
        {
            /*** Espa�o ***/
            echo"<tr><td colspan=2>&nbsp</td></tr>";


            /*** Tabela do calend�rio ***/
            echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


            /*** Primeira linha do calend�rio ***/
            echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
            <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
            <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


            /*** Linha dos dias da semana do calend�rio ***/
            echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
            <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
            <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


            /*** Dias do calend�rio ***/
            $contador = 0;
            $dia = 1;
            echo "<tr class=zebraA>";
            if ($contador == 0)
            {
	            for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	            {
		            echo "<td>&nbsp</td>";
	            }
                for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                {
                    $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                    $conn = &ADONewConnection($A_DB_TYPE);
                    $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                    $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                    from destinatario_agendamento_agenda,
                    agendamento_evento_agenda
                    where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                    and agendamento_evento_agenda.data_evento='$dtevento'";
                    $resp2=mysql_query($sql2);
                    $linha2=mysql_num_rows($resp2);

                    if($linha2>0)
                    {
                        if($dia==$dia_hoje)
                        {
                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                            <font color='purple'>$dia</font></a></strong></center></td>";
                            $dia = $dia +1;
                            $contador = $contador + 1;
                        }
                        else
                        {
                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                            <font color='red'>$dia</font></a></strong></center></td>";
                            $dia = $dia +1;
                            $contador = $contador + 1;
                        }
                    }
                    else
                    {
                        if ($dia == $dia_hoje)
                        {
                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                            <font color='blue'>$dia</font></a></strong></center></td>";
                            $dia = $dia +1;
                            $contador = $contador + 1;
                        }
                        else
                        {
                            echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                            $dia = $dia +1;
                            $contador = $contador + 1;
                        }
                    }
	            }
            }
            if($contador != 0)
            {
	            do
	            {
		            echo "</tr>";
		            echo "<tr class=zebraA>";
                    for ($cont= 0; $cont < 7; $cont++)
                    {
                        $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                        $conn = &ADONewConnection($A_DB_TYPE);
                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                        $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                        from destinatario_agendamento_agenda,
                        agendamento_evento_agenda
                        where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                        and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                        and agendamento_evento_agenda.data_evento='$dtevento'";
                        $resp2=mysql_query($sql2);
                        $linha2=mysql_num_rows($resp2);

                        if($linha2>0)
                        {
                            if($dia==$dia_hoje)
                            {
                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                <font color='purple'>$dia</font></a></strong></center></td>";
                                $dia = $dia +1;
                                $contador = $contador + 1;
                            }
                            else
                            {
                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                <font color='red'>$dia</font></a></strong></center></td>";
                                $dia = $dia +1;
                                $contador = $contador + 1;
                            }
                            if ($dia > $ultimo_dia_mes_hoje )
                            {
                                if ($posicao_ultimo_dia_mes != 6)
                                {
                                    for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                        {
                                        echo "<td>&nbsp</td>";
                                    }
                                }
				                break;
                            }
                        }
                        else
                        {
                            if ($dia == $dia_hoje)
                            {
                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                <font color='blue'>$dia</font></a></strong></center></td>";
                                $dia = $dia +1;
                            }
                            else
                            {
                                echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                $dia = $dia +1;
                            }
                            if ($dia > $ultimo_dia_mes_hoje )
                            {
                                if ($posicao_ultimo_dia_mes != 6)
                                {
                                    for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
     			                    {
				                        echo "<td>&nbsp</td>";
				                    }
                                }
				                break;
                            }
                        }
                    }
                    echo "</tr>";
                }while ($dia <= $ultimo_dia_mes_hoje);
            }
            echo "</table>";//Encerrando a tabela do calend�rio


            /*** Espa�os ***/
            echo "<tr><td colspan=2>&nbsp</td></tr>";
            echo "<tr><td colspan=2>&nbsp</td></tr>";


            echo "</table>";//Encerra tabela para montar a p�gina da agenda
        }
    }
    else
    {

/*****************************************************************************************************************/

        /*** Se o tipo da vis�o for igual a 2 ***/
        if($tipo_visao==2)
        {
            echo"<form name=formulariovisao action='n_index_agenda.php?opcao=AcessoAgenda&mes=$mes_hoje&ano=$ano_hoje&tipo_visao=$tipo_visao' method=post>";

            $sql2="select DISTINCT evento_agenda.id_tipo_assunto, evento_agenda.assunto_evento
            from evento_agenda, destinatario_agendamento_agenda, agendamento_evento_agenda
            where evento_agenda.id_evento_agenda = agendamento_evento_agenda.id_evento_agenda
            and agendamento_evento_agenda.id_agendamento_evento = destinatario_agendamento_agenda.id_agendamento_evento
            and destinatario_agendamento_agenda.id_usuario='$id_usuario'
            and evento_agenda.id_tipo_assunto='2'";
            $resp2=mysql_query($sql2);
            $linha2=mysql_num_rows($resp2);
            echo"<tr><td align=center>".A_LANG_AGENDA_VISOES_CALENDARIO."&nbsp";
            echo "<select name=visoes class=select size=1 onchange=ApareceVisao()>";
            echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONAR_VISAO."</option>";
            while ($array = mysql_fetch_array($resp2))
            {
	            if($vis==$array[1] || $visoes==$array[1])
           	    {
           	        echo "<option selected value=\"".$array[1]."\">".$array[1]."</option>";
           	    }
           	    else
           	    {
           	        echo "<option value=\"".$array[1]."\">".$array[1]."</option>";
           	    }
        	}
            echo "</select>";

            echo"</form>";


            if(isset ($visoes))
            {
                if($visoes != NULL && $visoes!="0")
                {
                    /*** Espa�o ***/
                    echo"<tr><td colspan=2>&nbsp</td></tr>";


                    /*** Tabela do calend�rio ***/
                    echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


                    /*** Primeira linha do calend�rio ***/
                    echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
                    <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
                    <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                    /*** Linha dos dias da semana do calend�rio ***/
                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
                    <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                    <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


                    /*** Dias do calend�rio ***/
                    $contador = 0;
                    $dia = 1;
                    echo "<tr class=zebraA>";
                    if ($contador == 0)
                    {
	                    for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	                    {
		                    echo "<td>&nbsp</td>";
	                    }
                        for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                        {
                            $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                            $conn = &ADONewConnection($A_DB_TYPE);
                            $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                            $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                            from destinatario_agendamento_agenda,
                            agendamento_evento_agenda, evento_agenda
                            where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                            and evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
                            and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                            and agendamento_evento_agenda.data_evento='$dtevento'
                            and evento_agenda.assunto_evento='$visoes'";
                            $resp2=mysql_query($sql2);
                            $linha2=mysql_num_rows($resp2);

                            if($linha2>0)
                            {
                                if($dia==$dia_hoje)
                                {
                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                    <font color='purple'>$dia</font></a></strong></center></td>";
                                    $dia = $dia +1;
                                    $contador = $contador + 1;
                                }
                                else
                                {
                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                    <font color='red'>$dia</font></a></strong></center></td>";
                                    $dia = $dia +1;
                                    $contador = $contador + 1;
                                }
                            }
                            else
                            {
                                if ($dia == $dia_hoje)
                                {
                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                    <font color='blue'>$dia</font></a></strong></center></td>";
                                    $dia = $dia +1;
                                    $contador = $contador + 1;
                                }
                                else
                                {
                                    echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                    $dia = $dia +1;
                                    $contador = $contador + 1;
                                }
                            }
	                    }
                    }
                    if($contador != 0)
                    {
	                    do
	                    {
		                    echo "</tr>";
		                    echo "<tr class=zebraA>";
                            for ($cont= 0; $cont < 7; $cont++)
                            {
                                $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                $conn = &ADONewConnection($A_DB_TYPE);
                                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                from destinatario_agendamento_agenda,
                                agendamento_evento_agenda, evento_agenda
                                where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                and evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
                                and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                and agendamento_evento_agenda.data_evento='$dtevento'
                                and evento_agenda.assunto_evento='$visoes'";
                                $resp2=mysql_query($sql2);
                                $linha2=mysql_num_rows($resp2);

                                if($linha2>0)
                                {
                                    if($dia==$dia_hoje)
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='purple'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                    else
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='red'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                    if ($dia > $ultimo_dia_mes_hoje )
                                    {
                                        if ($posicao_ultimo_dia_mes != 6)
                                        {
                                            for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                {
                                                echo "<td>&nbsp</td>";
                                            }
                                        }
				                        break;
                                    }
                                }
                                else
                                {
                                    if ($dia == $dia_hoje)
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='blue'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                    }
                                    else
                                    {
                                        echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                        $dia = $dia +1;
                                    }
                                    if ($dia > $ultimo_dia_mes_hoje )
                                    {
                                        if ($posicao_ultimo_dia_mes != 6)
                                        {
                                            for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                {
                                                echo "<td>&nbsp</td>";
                                            }
                                        }
				                        break;
                                    }
                                }
                            }
                            echo "</tr>";
                        }while ($dia <= $ultimo_dia_mes_hoje);
                    }
                    echo "</table>";//Encerrando a tabela do calend�rio


                    /*** Espa�os ***/
                    echo "<tr><td colspan=2>&nbsp</td></tr>";
                    echo "<tr><td colspan=2>&nbsp</td></tr>";

                    echo "</table>";//Encerra tabela para montar a p�gina da agenda
                }
                else
                {
                    if($visoes=="0" || $vis=="0")
                    {
                        /*** Espa�o ***/
                        echo"<tr><td colspan=2>&nbsp</td></tr>";


                        /*** Tabela do calend�rio ***/
                        echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


                        /*** Primeira linha do calend�rio ***/
                        echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
                        <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
                        <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                        /*** Linha dos dias da semana do calend�rio ***/
                        echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
                        <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                        <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


                        /*** Dias do calend�rio ***/
                        $contador = 0;
                        $dia = 1;
                        echo "<tr class=zebraA>";
                        if ($contador == 0)
                        {
	                        for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	                        {
		                        echo "<td>&nbsp</td>";
	                        }
                            for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                            {
                                $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                $conn = &ADONewConnection($A_DB_TYPE);
                                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                from destinatario_agendamento_agenda,
                                agendamento_evento_agenda
                                where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                and agendamento_evento_agenda.data_evento='$dtevento'";
                                $resp2=mysql_query($sql2);
                                $linha2=mysql_num_rows($resp2);

                                if($linha2>0)
                                {
                                    if($dia==$dia_hoje)
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='purple'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                    else
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='red'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                }
                                else
                                {
                                    if ($dia == $dia_hoje)
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='blue'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                    else
                                    {
                                        echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                }
	                        }
                        }
                        if($contador != 0)
                        {
                            do
	                        {
		                        echo "</tr>";
		                        echo "<tr class=zebraA>";
                                for ($cont= 0; $cont < 7; $cont++)
                                {
                                    $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                    $conn = &ADONewConnection($A_DB_TYPE);
                                    $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                    $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                    from destinatario_agendamento_agenda,
                                    agendamento_evento_agenda
                                    where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                    and agendamento_evento_agenda.data_evento='$dtevento'";
                                    $resp2=mysql_query($sql2);
                                    $linha2=mysql_num_rows($resp2);

                                    if($linha2>0)
                                    {
                                        if($dia==$dia_hoje)
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='purple'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                            $contador = $contador + 1;
                                        }
                                        else
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='red'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                            $contador = $contador + 1;
                                        }
                                        if ($dia > $ultimo_dia_mes_hoje )
                                        {
                                            if ($posicao_ultimo_dia_mes != 6)
                                            {
                                                for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                    {
                                                    echo "<td>&nbsp</td>";
                                    			}
                                            }
				                            break;
                                        }
                                    }
                                    else
                                    {
                                        if ($dia == $dia_hoje)
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='blue'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                        }
                                        else
                                        {
                                            echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                            $dia = $dia +1;
                                        }
                                        if ($dia > $ultimo_dia_mes_hoje )
                                        {
                                            if ($posicao_ultimo_dia_mes != 6)
                                            {
                                                for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
     			                                {
				                                    echo "<td>&nbsp</td>";
				                                }
                                            }
				                            break;
                                        }
                                    }
                                }
                                echo "</tr>";
                            }while ($dia <= $ultimo_dia_mes_hoje);
                        }
                        echo "</table>";//Encerrando a tabela do calend�rio


                        /*** Espa�os ***/
                        echo "<tr><td colspan=2>&nbsp</td></tr>";
                        echo "<tr><td colspan=2>&nbsp</td></tr>";


                        echo "</table>";//Encerra tabela para montar a p�gina da agenda
                    }
                }
                if(isset($visoes))
                {
                    $vis=$visoes;
                }
            }
            else
            {
                /*** Espa�o ***/
                echo"<tr><td colspan=2>&nbsp</td></tr>";


                /*** Tabela do calend�rio ***/
                echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


                /*** Primeira linha do calend�rio ***/
                echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
                <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
                <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                /*** Linha dos dias da semana do calend�rio ***/
                echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
                <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


                /*** Dias do calend�rio ***/
                $contador = 0;
                $dia = 1;
                echo "<tr class=zebraA>";
                if ($contador == 0)
                {
	                for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	                {
		                echo "<td>&nbsp</td>";
	                }
                    for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                    {
                        $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                        $conn = &ADONewConnection($A_DB_TYPE);
                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                        $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                        from destinatario_agendamento_agenda,
                        agendamento_evento_agenda
                        where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                        and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                        and agendamento_evento_agenda.data_evento='$dtevento'";
                        $resp2=mysql_query($sql2);
                        $linha2=mysql_num_rows($resp2);

                        if($linha2>0)
                        {
                            if($dia==$dia_hoje)
                            {
                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                <font color='purple'>$dia</font></a></strong></center></td>";
                                $dia = $dia +1;
                                $contador = $contador + 1;
                            }
                            else
                            {
                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                <font color='red'>$dia</font></a></strong></center></td>";
                                $dia = $dia +1;
                                $contador = $contador + 1;
                            }
                        }
                        else
                        {
                            if ($dia == $dia_hoje)
                            {
                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                <font color='blue'>$dia</font></a></strong></center></td>";
                                $dia = $dia +1;
                                $contador = $contador + 1;
                            }
                            else
                            {
                                echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                $dia = $dia +1;
                                $contador = $contador + 1;
                            }
                        }
	                }
                }
                if($contador != 0)
                {
	                do
	                {
		                echo "</tr>";
		                echo "<tr class=zebraA>";
                        for ($cont= 0; $cont < 7; $cont++)
                        {
                            $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                            $conn = &ADONewConnection($A_DB_TYPE);
                            $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                            $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                            from destinatario_agendamento_agenda,
                            agendamento_evento_agenda
                            where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                            and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                            and agendamento_evento_agenda.data_evento='$dtevento'";
                            $resp2=mysql_query($sql2);
                            $linha2=mysql_num_rows($resp2);

                            if($linha2>0)
                            {
                                if($dia==$dia_hoje)
                                {
                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                    <font color='purple'>$dia</font></a></strong></center></td>";
                                    $dia = $dia +1;
                                    $contador = $contador + 1;
                                }
                                else
                                {
                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                    <font color='red'>$dia</font></a></strong></center></td>";
                                    $dia = $dia +1;
                                    $contador = $contador + 1;
                                }
                                if ($dia > $ultimo_dia_mes_hoje )
                                {
                                    if ($posicao_ultimo_dia_mes != 6)
                                    {
                                        for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                            {
                                            echo "<td>&nbsp</td>";
                                        }
                                    }
				                    break;
                                }
                            }
                            else
                            {
                                if ($dia == $dia_hoje)
                                {
                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                    <font color='blue'>$dia</font></a></strong></center></td>";
                                    $dia = $dia +1;
                                }
                                else
                                {
                                    echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                    $dia = $dia +1;
                                }
                                if ($dia > $ultimo_dia_mes_hoje )
                                {
                                    if ($posicao_ultimo_dia_mes != 6)
                                    {
                                        for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
     			                        {
				                            echo "<td>&nbsp</td>";
				                        }
                                    }
				                    break;
                                }
                            }
                        }
                        echo "</tr>";
                    }while ($dia <= $ultimo_dia_mes_hoje);
                }
                echo "</table>";//Encerrando a tabela do calend�rio


                /*** Espa�os ***/
                echo "<tr><td colspan=2>&nbsp</td></tr>";
                echo "<tr><td colspan=2>&nbsp</td></tr>";


                echo "</table>";//Encerra tabela para montar a p�gina da agenda
             }
         }
         else
         {

/******************************************************************************************************************/

             if($tipo_visao==3)
             {
                 echo"<form name=formulariovisao action='n_index_agenda.php?opcao=AcessoAgenda&mes=$mes_hoje&ano=$ano_hoje&tipo_visao=$tipo_visao' method=post>";

                 $sql3="select DISTINCT evento_agenda.id_tipo_assunto, evento_agenda.assunto_evento
                 from evento_agenda, destinatario_agendamento_agenda, agendamento_evento_agenda
                 where evento_agenda.id_evento_agenda = agendamento_evento_agenda.id_evento_agenda
                 and agendamento_evento_agenda.id_agendamento_evento = destinatario_agendamento_agenda.id_agendamento_evento
                 and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                 and evento_agenda.id_tipo_assunto='3'";
                 $resp3=mysql_query($sql3);
                 $linha3=mysql_num_rows($resp3);
                 echo"<tr><td align=center>".A_LANG_AGENDA_VISOES_CALENDARIO."&nbsp";
                 echo "<select name=visoes class=select size=1 onchange=ApareceVisao()>";
                 echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONAR_VISAO."</option>";
                 while ($array = mysql_fetch_array($resp3))
        		 {
	        	     if($vis==$array[1] || $visoes==$array[1])
           		     {
           		         echo "<option selected value=\"".$array[1]."\">".$array[1]."</option>";
           		     }
           		     else
           		     {
           		         echo "<option value=\"".$array[1]."\">".$array[1]."</option>";
           		     }
        		 }
                 echo "</select>";

                 echo"</form>";

                 if(isset ($visoes))
                 {
                     if($visoes != NULL && $visoes!="0")
                     {
                         /*** Espa�o ***/
                         echo"<tr><td colspan=2>&nbsp</td></tr>";


                         /*** Tabela do calend�rio ***/
                         echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


                         /*** Primeira linha do calend�rio ***/
                         echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
                         <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
                         <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                         /*** Linha dos dias da semana do calend�rio ***/
                         echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
                         <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                         <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


                         /*** Dias do calend�rio ***/
                         $contador = 0;
                         $dia = 1;
                         echo "<tr class=zebraA>";
                         if ($contador == 0)
                         {
	                         for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	                         {
		                         echo "<td>&nbsp</td>";
	                         }
                             for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                             {
                                 $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                 $conn = &ADONewConnection($A_DB_TYPE);
                                 $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                 $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                 from destinatario_agendamento_agenda,
                                 agendamento_evento_agenda, evento_agenda
                                 where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                 and evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
                                 and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                 and agendamento_evento_agenda.data_evento='$dtevento'
                                 and evento_agenda.assunto_evento='$visoes'";
                                 $resp2=mysql_query($sql2);
                                 $linha2=mysql_num_rows($resp2);

                                 if($linha2>0)
                                 {
                                     if($dia==$dia_hoje)
                                     {
                                         echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                         <font color='purple'>$dia</font></a></strong></center></td>";
                                         $dia = $dia +1;
                                         $contador = $contador + 1;
                                     }
                                     else
                                     {
                                         echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                         <font color='red'>$dia</font></a></strong></center></td>";
                                         $dia = $dia +1;
                                         $contador = $contador + 1;
                                     }
                                 }
                                 else
                                 {
                                     if ($dia == $dia_hoje)
                                     {
                                         echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                         <font color='blue'>$dia</font></a></strong></center></td>";
                                         $dia = $dia +1;
                                         $contador = $contador + 1;
                                     }
                                     else
                                     {
                                         echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                         $dia = $dia +1;
                                         $contador = $contador + 1;
                                     }
                                 }
	                         }
                         }
                         if($contador != 0)
                         {
	                         do
	                         {
		                         echo "</tr>";
		                         echo "<tr class=zebraA>";
                                 for ($cont= 0; $cont < 7; $cont++)
                                 {
                                     $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                     $conn = &ADONewConnection($A_DB_TYPE);
                                     $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                     $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                     from destinatario_agendamento_agenda,
                                     agendamento_evento_agenda, evento_agenda
                                     where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                     and evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
                                     and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                     and agendamento_evento_agenda.data_evento='$dtevento'
                                     and evento_agenda.assunto_evento='$visoes'";
                                     $resp2=mysql_query($sql2);
                                     $linha2=mysql_num_rows($resp2);

                                     if($linha2>0)
                                     {
                                         if($dia==$dia_hoje)
                                         {
                                             echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                             <font color='purple'>$dia</font></a></strong></center></td>";
                                             $dia = $dia +1;
                                             $contador = $contador + 1;
                                         }
                                         else
                                         {
                                             echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                             <font color='red'>$dia</font></a></strong></center></td>";
                                             $dia = $dia +1;
                                             $contador = $contador + 1;
                                         }
                                         if ($dia > $ultimo_dia_mes_hoje )
                                         {
                                             if ($posicao_ultimo_dia_mes != 6)
                                             {
                                                 for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                     {
                                                     echo "<td>&nbsp</td>";
                                                 }
                                             }
				                             break;
                                         }
                                     }
                                     else
                                     {
                                         if ($dia == $dia_hoje)
                                         {
                                             echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                             <font color='blue'>$dia</font></a></strong></center></td>";
                                             $dia = $dia +1;
                                         }
                                         else
                                         {
                                             echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                             $dia = $dia +1;
                                         }
                                         if ($dia > $ultimo_dia_mes_hoje )
                                         {
                                             if ($posicao_ultimo_dia_mes != 6)
                                             {
                                                 for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
     			                                 {
				                                     echo "<td>&nbsp</td>";
				                                 }
                                             }
				                             break;
                                         }
                                     }
                                 }
                                 echo "</tr>";
                             }while ($dia <= $ultimo_dia_mes_hoje);
                         }
                         echo "</table>";//Encerrando a tabela do calend�rio


                         /*** Espa�os ***/
                         echo "<tr><td colspan=2>&nbsp</td></tr>";
                         echo "<tr><td colspan=2>&nbsp</td></tr>";

                         echo "</table>";//Encerra tabela para montar a p�gina da agenda
                     }
                     else
                     {
                         if($visoes=="0")
                         {
                             /*** Espa�o ***/
                             echo"<tr><td colspan=2>&nbsp</td></tr>";


                             /*** Tabela do calend�rio ***/
                             echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


                             /*** Primeira linha do calend�rio ***/
                             echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
                             <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
                             <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                             /*** Linha dos dias da semana do calend�rio ***/
                             echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
                             <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                             <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


                             /*** Dias do calend�rio ***/
                             $contador = 0;
                             $dia = 1;
                             echo "<tr class=zebraA>";
                             if ($contador == 0)
                             {
	                             for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	                             {
		                             echo "<td>&nbsp</td>";
	                             }
                                 for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                                 {
                                     $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                     $conn = &ADONewConnection($A_DB_TYPE);
                                     $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                     $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                     from destinatario_agendamento_agenda,
                                     agendamento_evento_agenda
                                     where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                     and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                     and agendamento_evento_agenda.data_evento='$dtevento'";
                                     $resp2=mysql_query($sql2);
                                     $linha2=mysql_num_rows($resp2);

                                     if($linha2>0)
                                     {
                                         if($dia==$dia_hoje)
                                         {
                                             echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                             <font color='purple'>$dia</font></a></strong></center></td>";
                                             $dia = $dia +1;
                                             $contador = $contador + 1;
                                         }
                                         else
                                         {
                                             echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                             <font color='red'>$dia</font></a></strong></center></td>";
                                             $dia = $dia +1;
                                             $contador = $contador + 1;
                                         }
                                     }
                                     else
                                     {
                                         if ($dia == $dia_hoje)
                                         {
                                             echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                             <font color='blue'>$dia</font></a></strong></center></td>";
                                             $dia = $dia +1;
                                             $contador = $contador + 1;
                                         }
                                         else
                                         {
                                             echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                             $dia = $dia +1;
                                             $contador = $contador + 1;
                                         }
                                     }
	                             }
                             }
                             if($contador != 0)
                             {
	                             do
	                             {
		                             echo "</tr>";
		                             echo "<tr class=zebraA>";
                                     for ($cont= 0; $cont < 7; $cont++)
                                     {
                                         $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                         $conn = &ADONewConnection($A_DB_TYPE);
                                         $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                         $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                         from destinatario_agendamento_agenda,
                                         agendamento_evento_agenda
                                         where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                         and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                         and agendamento_evento_agenda.data_evento='$dtevento'";
                                         $resp2=mysql_query($sql2);
                                         $linha2=mysql_num_rows($resp2);

                                         if($linha2>0)
                                         {
                                             if($dia==$dia_hoje)
                                             {
                                                 echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                 <font color='purple'>$dia</font></a></strong></center></td>";
                                                 $dia = $dia +1;
                                                 $contador = $contador + 1;
                                             }
                                             else
                                             {
                                                 echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                 <font color='red'>$dia</font></a></strong></center></td>";
                                                 $dia = $dia +1;
                                                 $contador = $contador + 1;
                                             }
                                             if ($dia > $ultimo_dia_mes_hoje )
                                             {
                                                 if ($posicao_ultimo_dia_mes != 6)
                                                 {
                                                     for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                         {
                                                         echo "<td>&nbsp</td>";
                                                     }
                                                 }
                                                 break;
                                             }
                                         }
                                         else
                                         {
                                             if ($dia == $dia_hoje)
                                             {
                                                 echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                 <font color='blue'>$dia</font></a></strong></center></td>";
                                                 $dia = $dia +1;
                                             }
                                             else
                                             {
                                                 echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                                 $dia = $dia +1;
                                             }
                                             if ($dia > $ultimo_dia_mes_hoje )
                                             {
                                                 if ($posicao_ultimo_dia_mes != 6)
                                                 {
                                                     for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                         {
                                                         echo "<td>&nbsp</td>";
                                                     }
                                                 }
				                                 break;
                                             }
                                         }
                                     }
                                     echo "</tr>";
                                 }while ($dia <= $ultimo_dia_mes_hoje);
                             }
                             echo "</table>";//Encerrando a tabela do calend�rio


                             /*** Espa�os ***/
                             echo "<tr><td colspan=2>&nbsp</td></tr>";
                             echo "<tr><td colspan=2>&nbsp</td></tr>";

                             echo "</table>";//Encerra tabela para montar a p�gina da agenda
                         }
                     }
                     if(isset($visoes))
                     {
                         $vis=$visoes;
                     }
                 }
                 else
                 {
                     /*** Espa�o ***/
                     echo"<tr><td colspan=2>&nbsp</td></tr>";


                     /*** Tabela do calend�rio ***/
                     echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


                     /*** Primeira linha do calend�rio ***/
                     echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
                     <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
                     <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                     /*** Linha dos dias da semana do calend�rio ***/
                     echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
                     <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                     <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


                     /*** Dias do calend�rio ***/
                     $contador = 0;
                     $dia = 1;
                     echo "<tr class=zebraA>";
                     if ($contador == 0)
                     {
	                     for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	                     {
		                     echo "<td>&nbsp</td>";
	                     }
                         for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                         {
                             $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                             $conn = &ADONewConnection($A_DB_TYPE);
                             $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                             $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                             from destinatario_agendamento_agenda,
                             agendamento_evento_agenda
                             where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                             and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                             and agendamento_evento_agenda.data_evento='$dtevento'";
                             $resp2=mysql_query($sql2);
                             $linha2=mysql_num_rows($resp2);

                             if($linha2>0)
                             {
                                 if($dia==$dia_hoje)
                                 {
                                     echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                     <font color='purple'>$dia</font></a></strong></center></td>";
                                     $dia = $dia +1;
                                     $contador = $contador + 1;
                                 }
                                 else
                                 {
                                     echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                     <font color='red'>$dia</font></a></strong></center></td>";
                                     $dia = $dia +1;
                                     $contador = $contador + 1;
                                 }
                             }
                             else
                             {
                                 if ($dia == $dia_hoje)
                                 {
                                     echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                     <font color='blue'>$dia</font></a></strong></center></td>";
                                     $dia = $dia +1;
                                     $contador = $contador + 1;
                                 }
                                 else
                                 {
                                     echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                     $dia = $dia +1;
                                     $contador = $contador + 1;
                                 }
                             }
	                     }
                     }
                     if($contador != 0)
                     {
	                     do
	                     {
		                     echo "</tr>";
		                     echo "<tr class=zebraA>";
                             for ($cont= 0; $cont < 7; $cont++)
                             {
                                 $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                 $conn = &ADONewConnection($A_DB_TYPE);
                                 $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                 $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                 from destinatario_agendamento_agenda,
                                 agendamento_evento_agenda
                                 where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                 and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                 and agendamento_evento_agenda.data_evento='$dtevento'";
                                 $resp2=mysql_query($sql2);
                                 $linha2=mysql_num_rows($resp2);

                                 if($linha2>0)
                                 {
                                     if($dia==$dia_hoje)
                                     {
                                         echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                         <font color='purple'>$dia</font></a></strong></center></td>";
                                         $dia = $dia +1;
                                         $contador = $contador + 1;
                                     }
                                     else
                                     {
                                         echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                         <font color='red'>$dia</font></a></strong></center></td>";
                                         $dia = $dia +1;
                                         $contador = $contador + 1;
                                     }
                                     if ($dia > $ultimo_dia_mes_hoje )
                                     {
                                         if ($posicao_ultimo_dia_mes != 6)
                                         {
                                             for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                 {
                                                 echo "<td>&nbsp</td>";
                                             }
                                         }
				                         break;
                                     }
                                 }
                                 else
                                 {
                                     if ($dia == $dia_hoje)
                                     {
                                         echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                         <font color='blue'>$dia</font></a></strong></center></td>";
                                         $dia = $dia +1;
                                     }
                                     else
                                     {
                                         echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                         $dia = $dia +1;
                                     }
                                     if ($dia > $ultimo_dia_mes_hoje )
                                     {
                                         if ($posicao_ultimo_dia_mes != 6)
                                         {
                                             for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                 {
                                                 echo "<td>&nbsp</td>";
                                             }
                                         }
				                         break;
                                     }
                                 }
                             }
                             echo "</tr>";
                         }while ($dia <= $ultimo_dia_mes_hoje);
                     }
                     echo "</table>";//Encerrando a tabela do calend�rio


                     /*** Espa�os ***/
                     echo "<tr><td colspan=2>&nbsp</td></tr>";
                     echo "<tr><td colspan=2>&nbsp</td></tr>";


                     echo "</table>";//Encerra tabela para montar a p�gina da agenda
                 }
            }
            else
            {

/*****************************************************************************************************************/

                if($tipo_visao==4 || $tip_vis==4)
                {
                    echo"<form name=formulariovisao action='n_index_agenda.php?opcao=AcessoAgenda&mes=$mes_hoje&ano=$ano_hoje&tipo_visao=$tipo_visao' method=post>";
                    $sql4="select DISTINCT evento_agenda.id_tipo_assunto, evento_agenda.assunto_evento
                    from evento_agenda, destinatario_agendamento_agenda, agendamento_evento_agenda
                    where evento_agenda.id_evento_agenda = agendamento_evento_agenda.id_evento_agenda
                    and agendamento_evento_agenda.id_agendamento_evento = destinatario_agendamento_agenda.id_agendamento_evento
                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                    and evento_agenda.id_tipo_assunto='4'";
                    $resp4=mysql_query($sql4);
                    $linha4=mysql_num_rows($resp4);
                    echo"<tr><td align=center>".A_LANG_AGENDA_VISOES_CALENDARIO."&nbsp";
                    echo "<select name=visoes class=select size=1 onchange=ApareceVisao()>";
                    echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONAR_VISAO."</option>";
                    while ($array = mysql_fetch_array($resp4))
                    {
                        if($vis==$array[1] || $visoes==$array[1])
                        {
                            echo "<option selected value=\"".$array[1]."\">".$array[1]."</option>";
                        }
                        else
                        {
                            echo "<option value=\"".$array[1]."\">".$array[1]."</option>";
                        }
                    }
                    echo "</select>";

                    echo"</form>";

                    if(isset ($visoes))
                    {
                        if($visoes != NULL && $visoes!="0")
                        {
                            /*** Espa�o ***/
                            echo"<tr><td colspan=2>&nbsp</td></tr>";


                            /*** Tabela do calend�rio ***/
                            echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


                            /*** Primeira linha do calend�rio ***/
                            echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
                            <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
                            <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                            /*** Linha dos dias da semana do calend�rio ***/
                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
                            <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                            <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


                            /*** Dias do calend�rio ***/
                            $contador = 0;
                            $dia = 1;
                            echo "<tr class=zebraA>";
                            if ($contador == 0)
                            {
	                            for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	                            {
		                            echo "<td>&nbsp</td>";
	                            }
                                for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                                {
                                    $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                    $conn = &ADONewConnection($A_DB_TYPE);
                                    $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                    $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                    from destinatario_agendamento_agenda,
                                    agendamento_evento_agenda, evento_agenda
                                    where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                    and evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
                                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                    and agendamento_evento_agenda.data_evento='$dtevento'
                                    and evento_agenda.assunto_evento='$visoes'";
                                    $resp2=mysql_query($sql2);
                                    $linha2=mysql_num_rows($resp2);

                                    if($linha2>0)
                                    {
                                        if($dia==$dia_hoje)
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='purple'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                            $contador = $contador + 1;
                                        }
                                        else
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='red'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                            $contador = $contador + 1;
                                        }
                                    }
                                    else
                                    {
                                        if ($dia == $dia_hoje)
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='blue'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                            $contador = $contador + 1;
                                        }
                                        else
                                        {
                                            echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                            $dia = $dia +1;
                                            $contador = $contador + 1;
                                        }
                                    }
	                            }
                            }
                            if($contador != 0)
                            {
	                            do
	                            {
		                            echo "</tr>";
		                            echo "<tr class=zebraA>";
                                    for ($cont= 0; $cont < 7; $cont++)
                                    {
                                        $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                        $conn = &ADONewConnection($A_DB_TYPE);
                                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                        $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                        from destinatario_agendamento_agenda,
                                        agendamento_evento_agenda, evento_agenda
                                        where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                        and evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
                                        and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                        and agendamento_evento_agenda.data_evento='$dtevento'
                                        and evento_agenda.assunto_evento='$visoes'";
                                        $resp2=mysql_query($sql2);
                                        $linha2=mysql_num_rows($resp2);

                                        if($linha2>0)
                                        {
                                            if($dia==$dia_hoje)
                                            {
                                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                <font color='purple'>$dia</font></a></strong></center></td>";
                                                $dia = $dia +1;
                                                $contador = $contador + 1;
                                            }
                                            else
                                            {
                                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                <font color='red'>$dia</font></a></strong></center></td>";
                                                $dia = $dia +1;
                                                $contador = $contador + 1;
                                            }
                                            if ($dia > $ultimo_dia_mes_hoje )
                                            {
                                                if ($posicao_ultimo_dia_mes != 6)
                                                {
                                                    for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                        {
                                                        echo "<td>&nbsp</td>";
				                                    }
                                                }
				                                break;
                                            }
                                        }
                                        else
                                        {
                                            if ($dia == $dia_hoje)
                                            {
                                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                <font color='blue'>$dia</font></a></strong></center></td>";
                                                $dia = $dia +1;
                                            }
                                            else
                                            {
                                                echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                                $dia = $dia +1;
                                            }
                                            if ($dia > $ultimo_dia_mes_hoje )
                                            {
                                                if ($posicao_ultimo_dia_mes != 6)
                                                {
                                                    for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                        {
                                                        echo "<td>&nbsp</td>";
                                                    }
                                                }
				                                break;
                                            }
                                        }
                                    }
                                    echo "</tr>";
                                }while ($dia <= $ultimo_dia_mes_hoje);
                            }
                            echo "</table>";//Encerrando a tabela do calend�rio


                            /*** Espa�os ***/
                            echo "<tr><td colspan=2>&nbsp</td></tr>";
                            echo "<tr><td colspan=2>&nbsp</td></tr>";

                            echo "</table>";//Encerra tabela para montar a p�gina da agenda
                        }
                        else
                        {
                            if($visoes=="0")
                            {
                                /*** Espa�o ***/
                                echo"<tr><td colspan=2>&nbsp</td></tr>";


                                /*** Tabela do calend�rio ***/
                                echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


                                /*** Primeira linha do calend�rio ***/
                                echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
                                <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
                                <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                                /*** Linha dos dias da semana do calend�rio ***/
                                echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
                                <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                                <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


                                /*** Dias do calend�rio ***/
                                $contador = 0;
                                $dia = 1;
                                echo "<tr class=zebraA>";
                                if ($contador == 0)
                                {
	                                for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	                                {
		                                echo "<td>&nbsp</td>";
	                                }
                                    for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                                    {
                                        $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                        $conn = &ADONewConnection($A_DB_TYPE);
                                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                        $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                        from destinatario_agendamento_agenda,
                                        agendamento_evento_agenda
                                        where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                        and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                        and agendamento_evento_agenda.data_evento='$dtevento'";
                                        $resp2=mysql_query($sql2);
                                        $linha2=mysql_num_rows($resp2);

                                        if($linha2>0)
                                        {
                                            if($dia==$dia_hoje)
                                            {
                                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                <font color='purple'>$dia</font></a></strong></center></td>";
                                                $dia = $dia +1;
                                                $contador = $contador + 1;
                                            }
                                            else
                                            {
                                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                <font color='red'>$dia</font></a></strong></center></td>";
                                                $dia = $dia +1;
                                                $contador = $contador + 1;
                                            }
                                        }
                                        else
                                        {
                                            if ($dia == $dia_hoje)
                                            {
                                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                <font color='blue'>$dia</font></a></strong></center></td>";
                                                $dia = $dia +1;
                                                $contador = $contador + 1;
                                            }
                                            else
                                            {
                                                echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                                $dia = $dia +1;
                                                $contador = $contador + 1;
                                            }
                                        }
	                                }
                                }
                                if($contador != 0)
                                {
	                                do
	                                {
		                                echo "</tr>";
		                                echo "<tr class=zebraA>";
                                        for ($cont= 0; $cont < 7; $cont++)
                                        {
                                            $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                            $conn = &ADONewConnection($A_DB_TYPE);
                                            $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                            $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                            from destinatario_agendamento_agenda,
                                            agendamento_evento_agenda
                                            where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                            and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                            and agendamento_evento_agenda.data_evento='$dtevento'";
                                            $resp2=mysql_query($sql2);
                                            $linha2=mysql_num_rows($resp2);

                                            if($linha2>0)
                                            {
                                                if($dia==$dia_hoje)
                                                {
                                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                    <font color='purple'>$dia</font></a></strong></center></td>";
                                                    $dia = $dia +1;
                                                    $contador = $contador + 1;
                                                }
                                                else
                                                {
                                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                    <font color='red'>$dia</font></a></strong></center></td>";
                                                    $dia = $dia +1;
                                                    $contador = $contador + 1;
                                                }
                                                if ($dia > $ultimo_dia_mes_hoje )
                                                {
                                                    if ($posicao_ultimo_dia_mes != 6)
                                                    {
                                                        for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                            {
                                                            echo "<td>&nbsp</td>";
				                                        }
                                                    }
				                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if ($dia == $dia_hoje)
                                                {
                                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                    <font color='blue'>$dia</font></a></strong></center></td>";
                                                    $dia = $dia +1;
                                                }
                                                else
                                                {
                                                    echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                                    $dia = $dia +1;
                                                }
                                                if ($dia > $ultimo_dia_mes_hoje )
                                                {
                                                    if ($posicao_ultimo_dia_mes != 6)
                                                    {
                                                        for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                            {
				                                            echo "<td>&nbsp</td>";
                                                        }
                                                    }
				                                    break;
                                                }
                                            }
                                        }
                                        echo "</tr>";
                                    }while ($dia <= $ultimo_dia_mes_hoje);
                                }
                                echo "</table>";//Encerrando a tabela do calend�rio


                                /*** Espa�os ***/
                                echo "<tr><td colspan=2>&nbsp</td></tr>";
                                echo "<tr><td colspan=2>&nbsp</td></tr>";

                                echo "</table>";//Encerra tabela para montar a p�gina da agenda
                            }
                        }
                        if(isset($visoes))
                        {
                        $vis=$visoes;
                        }
                    }
                    else
                    {
                        /*** Espa�o ***/
                        echo"<tr><td colspan=2>&nbsp</td></tr>";


                        /*** Tabela do calend�rio ***/
                        echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


                        /*** Primeira linha do calend�rio ***/
                        echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
                        <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
                        <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                        /*** Linha dos dias da semana do calend�rio ***/
                        echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
                        <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                        <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


                        /*** Dias do calend�rio ***/
                        $contador = 0;
                        $dia = 1;
                        echo "<tr class=zebraA>";
                        if ($contador == 0)
                        {
	                        for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	                        {
		                        echo "<td>&nbsp</td>";
	                        }
                            for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                            {
                                $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                $conn = &ADONewConnection($A_DB_TYPE);
                                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                from destinatario_agendamento_agenda,
                                agendamento_evento_agenda
                                where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                and agendamento_evento_agenda.data_evento='$dtevento'";
                                $resp2=mysql_query($sql2);
                                $linha2=mysql_num_rows($resp2);

                                if($linha2>0)
                                {
                                    if($dia==$dia_hoje)
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='purple'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                    else
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='red'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                }
                                else
                                {
                                    if ($dia == $dia_hoje)
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='blue'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                    else
                                    {
                                        echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                }
	                        }
                        }
                        if($contador != 0)
                        {
	                        do
	                        {
		                        echo "</tr>";
		                        echo "<tr class=zebraA>";
                                for ($cont= 0; $cont < 7; $cont++)
                                {
                                    $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                    $conn = &ADONewConnection($A_DB_TYPE);
                                    $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                    $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                    from destinatario_agendamento_agenda,
                                    agendamento_evento_agenda
                                    where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                    and agendamento_evento_agenda.data_evento='$dtevento'";
                                    $resp2=mysql_query($sql2);
                                    $linha2=mysql_num_rows($resp2);

                                    if($linha2>0)
                                    {
                                        if($dia==$dia_hoje)
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='purple'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                            $contador = $contador + 1;
                                        }
                                        else
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='red'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                            $contador = $contador + 1;
                                        }
                                        if ($dia > $ultimo_dia_mes_hoje )
                                        {
                                            if ($posicao_ultimo_dia_mes != 6)
                                            {
                                                for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                    {
				                                    echo "<td>&nbsp</td>";
				                                }
                                            }
				                            break;
                                        }
                                    }
                                    else
                                    {
                                        if ($dia == $dia_hoje)
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='blue'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                        }
                                        else
                                        {
                                            echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                            $dia = $dia +1;
                                        }
                                        if ($dia > $ultimo_dia_mes_hoje )
                                        {
                                            if ($posicao_ultimo_dia_mes != 6)
                                            {
                                                for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                    {
				                                    echo "<td>&nbsp</td>";
                                                }
                                            }
				                            break;
                                        }
                                    }
                                }
                                echo "</tr>";
                            }while ($dia <= $ultimo_dia_mes_hoje);
                        }
                        echo "</table>";//Encerrando a tabela do calend�rio


                        /*** Espa�os ***/
                        echo "<tr><td colspan=2>&nbsp</td></tr>";
                        echo "<tr><td colspan=2>&nbsp</td></tr>";

                        echo "</table>";//Encerra tabela para montar a p�gina da agenda
                    }
                }
                else
                {

/******************************************************************************************************************/

                    if($tipo_visao==5 || $tip_vis==5)
                    {
                        echo"<form name=formulariovisao action='n_index_agenda.php?opcao=AcessoAgenda&mes=$mes_hoje&ano=$ano_hoje&tipo_visao=$tipo_visao' method=post>";
                        $sql5="select DISTINCT evento_agenda.id_tipo_assunto, evento_agenda.assunto_evento
                        from evento_agenda, destinatario_agendamento_agenda, agendamento_evento_agenda
                        where evento_agenda.id_evento_agenda = agendamento_evento_agenda.id_evento_agenda
                        and agendamento_evento_agenda.id_agendamento_evento = destinatario_agendamento_agenda.id_agendamento_evento
                        and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                        and evento_agenda.id_tipo_assunto='5'";
                        $resp5=mysql_query($sql5);
                        $linha5=mysql_num_rows($resp5);
                        echo"<tr><td align=center>".A_LANG_AGENDA_VISOES_CALENDARIO."&nbsp";
                        echo "<select name=visoes class=select size=1 onchange=ApareceVisao()>";
                        echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONAR_VISAO."</option>";
                        while ($array = mysql_fetch_array($resp5))
                        {
                            if($vis==$array[1] || $visoes==$array[1])
                            {
                                echo "<option selected value=\"".$array[1]."\">".$array[1]."</option>";
                            }
                            else
                            {
                                echo "<option value=\"".$array[1]."\">".$array[1]."</option>";
                            }
                        }
                        echo "</select>";

                        echo"</form>";

                        if(isset ($visoes))
                        {
                            if($visoes !=NULL && $visoes!="0")
                            {
                                /*** Espa�o ***/
                                echo"<tr><td colspan=2>&nbsp</td></tr>";


                                /*** Tabela do calend�rio ***/
                                echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


                                /*** Primeira linha do calend�rio ***/
                                echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
                                <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
                                <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                                /*** Linha dos dias da semana do calend�rio ***/
                                echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
                                <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                                <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


                                /*** Dias do calend�rio ***/
                                $contador = 0;
                                $dia = 1;
                                echo "<tr class=zebraA>";
                                if ($contador == 0)
                                {
	                                for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	                                {
		                                echo "<td>&nbsp</td>";
	                                }
                                    for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                                    {
                                        $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                        $conn = &ADONewConnection($A_DB_TYPE);
                                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                        $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                        from destinatario_agendamento_agenda,
                                        agendamento_evento_agenda, evento_agenda
                                        where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                        and evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
                                        and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                        and agendamento_evento_agenda.data_evento='$dtevento'
                                        and evento_agenda.assunto_evento='$visoes'";
                                        $resp2=mysql_query($sql2);
                                        $linha2=mysql_num_rows($resp2);

                                        if($linha2>0)
                                        {
                                            if($dia==$dia_hoje)
                                            {
                                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                <font color='purple'>$dia</font></a></strong></center></td>";
                                                $dia = $dia +1;
                                                $contador = $contador + 1;
                                            }
                                            else
                                            {
                                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                <font color='red'>$dia</font></a></strong></center></td>";
                                                $dia = $dia +1;
                                                $contador = $contador + 1;
                                            }
                                        }
                                        else
                                        {
                                            if ($dia == $dia_hoje)
                                            {
                                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                <font color='blue'>$dia</font></a></strong></center></td>";
                                                $dia = $dia +1;
                                                $contador = $contador + 1;
                                            }
                                            else
                                            {
                                                echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                                $dia = $dia +1;
                                                $contador = $contador + 1;
                                            }
                                        }
	                                }
                                }
                                if($contador != 0)
                                {
	                                do
	                                {
		                                echo "</tr>";
		                                echo "<tr class=zebraA>";
                                        for ($cont= 0; $cont < 7; $cont++)
                                        {
                                            $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                            $conn = &ADONewConnection($A_DB_TYPE);
                                            $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                            $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                            from destinatario_agendamento_agenda,
                                            agendamento_evento_agenda, evento_agenda
                                            where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                            and evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
                                            and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                            and agendamento_evento_agenda.data_evento='$dtevento'
                                            and evento_agenda.assunto_evento='$visoes'";
                                            $resp2=mysql_query($sql2);
                                            $linha2=mysql_num_rows($resp2);

                                            if($linha2>0)
                                            {
                                                if($dia==$dia_hoje)
                                                {
                                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                    <font color='purple'>$dia</font></a></strong></center></td>";
                                                    $dia = $dia +1;
                                                    $contador = $contador + 1;
                                                }
                                                else
                                                {
                                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                    <font color='red'>$dia</font></a></strong></center></td>";
                                                    $dia = $dia +1;
                                                    $contador = $contador + 1;
                                                }
                                                if ($dia > $ultimo_dia_mes_hoje )
                                                {
                                                    if ($posicao_ultimo_dia_mes != 6)
                                                    {
                                                        for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                            {
                                                            echo "<td>&nbsp</td>";
                                                        }
                                                    }
				                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if ($dia == $dia_hoje)
                                                {
                                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                    <font color='blue'>$dia</font></a></strong></center></td>";
                                                    $dia = $dia +1;
                                                }
                                                else
                                                {
                                                    echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                                    $dia = $dia +1;
                                                }
                                                if ($dia > $ultimo_dia_mes_hoje )
                                                {
                                                    if ($posicao_ultimo_dia_mes != 6)
                                                    {
                                                        for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                            {
                                                            echo "<td>&nbsp</td>";
                                                        }
                                                    }
				                                    break;
                                                }
                                            }
                                        }
                                        echo "</tr>";
                                    }while ($dia <= $ultimo_dia_mes_hoje);
                                }
                                echo "</table>";//Encerrando a tabela do calend�rio


                                /*** Espa�os ***/
                                echo "<tr><td colspan=2>&nbsp</td></tr>";
                                echo "<tr><td colspan=2>&nbsp</td></tr>";

                                echo "</table>";//Encerra tabela para montar a p�gina da agenda
                            }
                            else
                            {
                                if($visoes=="0")
                                {
                                    /*** Espa�o ***/
                                    echo"<tr><td colspan=2>&nbsp</td></tr>";

                                    /*** Tabela do calend�rio ***/
                                    echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";

                                    /*** Primeira linha do calend�rio ***/
                                    echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
                                    <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
                                    <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                                    /*** Linha dos dias da semana do calend�rio ***/
                                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
                                    <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                                    <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


                                    /*** Dias do calend�rio ***/
                                    $contador = 0;
                                    $dia = 1;
                                    echo "<tr class=zebraA>";
                                    if ($contador == 0)
                                    {
	                                    for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	                                    {
		                                    echo "<td>&nbsp</td>";
	                                    }
                                        for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                                        {
                                            $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                            $conn = &ADONewConnection($A_DB_TYPE);
                                            $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                            $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                            from destinatario_agendamento_agenda,
                                            agendamento_evento_agenda
                                            where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                            and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                            and agendamento_evento_agenda.data_evento='$dtevento'";
                                            $resp2=mysql_query($sql2);
                                            $linha2=mysql_num_rows($resp2);

                                            if($linha2>0)
                                            {
                                                if($dia==$dia_hoje)
                                                {
                                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                    <font color='purple'>$dia</font></a></strong></center></td>";
                                                    $dia = $dia +1;
                                                    $contador = $contador + 1;
                                                }
                                                else
                                                {
                                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                    <font color='red'>$dia</font></a></strong></center></td>";
                                                    $dia = $dia +1;
                                                    $contador = $contador + 1;
                                                }
                                            }
                                            else
                                            {
                                                if ($dia == $dia_hoje)
                                                {
                                                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                    <font color='blue'>$dia</font></a></strong></center></td>";
                                                    $dia = $dia +1;
                                                    $contador = $contador + 1;
                                                }
                                                else
                                                {
                                                    echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                                    $dia = $dia +1;
                                                    $contador = $contador + 1;
                                                }
                                            }
	                                    }
                                    }
                                    if($contador != 0)
                                    {
	                                    do
	                                    {
		                                    echo "</tr>";
		                                    echo "<tr class=zebraA>";
                                            for ($cont= 0; $cont < 7; $cont++)
                                            {
                                                $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                                $conn = &ADONewConnection($A_DB_TYPE);
                                                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                                from destinatario_agendamento_agenda,
                                                agendamento_evento_agenda
                                                where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                                and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                                and agendamento_evento_agenda.data_evento='$dtevento'";
                                                $resp2=mysql_query($sql2);
                                                $linha2=mysql_num_rows($resp2);

                                                if($linha2>0)
                                                {
                                                    if($dia==$dia_hoje)
                                                    {
                                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                        <font color='purple'>$dia</font></a></strong></center></td>";
                                                        $dia = $dia +1;
                                                        $contador = $contador + 1;
                                                    }
                                                    else
                                                    {
                                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                        <font color='red'>$dia</font></a></strong></center></td>";
                                                        $dia = $dia +1;
                                                        $contador = $contador + 1;
                                                    }
                                                    if ($dia > $ultimo_dia_mes_hoje )
                                                    {
                                                        if ($posicao_ultimo_dia_mes != 6)
                                                        {
                                                            for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                                {
                                                                echo "<td>&nbsp</td>";
                                                            }
                                                        }
				                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    if ($dia == $dia_hoje)
                                                    {
                                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                        <font color='blue'>$dia</font></a></strong></center></td>";
                                                        $dia = $dia +1;
                                                    }
                                                    else
                                                    {
                                                        echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                                        $dia = $dia +1;
                                                    }
                                                    if ($dia > $ultimo_dia_mes_hoje )
                                                    {
                                                        if ($posicao_ultimo_dia_mes != 6)
                                                        {
                                                            for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                                {
                                                                echo "<td>&nbsp</td>";
				                                            }
                                                        }
				                                        break;
                                                    }
                                                }
                                            }
                                            echo "</tr>";
                                        }while ($dia <= $ultimo_dia_mes_hoje);
                                    }
                                    echo "</table>";//Encerrando a tabela do calend�rio


                                    /*** Espa�os ***/
                                    echo "<tr><td colspan=2>&nbsp</td></tr>";
                                    echo "<tr><td colspan=2>&nbsp</td></tr>";

                                    echo "</table>";//Encerra tabela para montar a p�gina da agenda
                                }
                            }
                            if(isset($visoes))
                            {
                                $vis=$visoes;
                            }
                        }
                        else
                        {
                            /*** Espa�o ***/
                            echo"<tr><td colspan=2>&nbsp</td></tr>";


                            /*** Tabela do calend�rio ***/
                            echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


                            /*** Primeira linha do calend�rio ***/
                            echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
                            <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
                            <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                            /*** Linha dos dias da semana do calend�rio ***/
                            echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
                            <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                            <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


                            /*** Dias do calend�rio ***/
                            $contador = 0;
                            $dia = 1;
                            echo "<tr class=zebraA>";
                            if ($contador == 0)
                            {
	                            for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	                            {
		                            echo "<td>&nbsp</td>";
	                            }
                                for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                                {
                                    $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                    $conn = &ADONewConnection($A_DB_TYPE);
                                    $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                    $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                    from destinatario_agendamento_agenda,
                                    agendamento_evento_agenda
                                    where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                    and agendamento_evento_agenda.data_evento='$dtevento'";
                                    $resp2=mysql_query($sql2);
                                    $linha2=mysql_num_rows($resp2);

                                    if($linha2>0)
                                    {
                                        if($dia==$dia_hoje)
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='purple'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                            $contador = $contador + 1;
                                        }
                                        else
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='red'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                            $contador = $contador + 1;
                                        }
                                    }
                                    else
                                    {
                                        if ($dia == $dia_hoje)
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='blue'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                            $contador = $contador + 1;
                                        }
                                        else
                                        {
                                            echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                            $dia = $dia +1;
                                            $contador = $contador + 1;
                                        }
                                    }
	                            }
                            }
                            if($contador != 0)
                            {
	                            do
	                            {
		                            echo "</tr>";
		                            echo "<tr class=zebraA>";
                                    for ($cont= 0; $cont < 7; $cont++)
                                    {
                                        $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                        $conn = &ADONewConnection($A_DB_TYPE);
                                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                        $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                        from destinatario_agendamento_agenda,
                                        agendamento_evento_agenda
                                        where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                        and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                        and agendamento_evento_agenda.data_evento='$dtevento'";
                                        $resp2=mysql_query($sql2);
                                        $linha2=mysql_num_rows($resp2);

                                        if($linha2>0)
                                        {
                                            if($dia==$dia_hoje)
                                            {
                                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                <font color='purple'>$dia</font></a></strong></center></td>";
                                                $dia = $dia +1;
                                                $contador = $contador + 1;
                                            }
                                            else
                                            {
                                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                <font color='red'>$dia</font></a></strong></center></td>";
                                                $dia = $dia +1;
                                                $contador = $contador + 1;
                                            }
                                            if ($dia > $ultimo_dia_mes_hoje )
                                            {
                                                if ($posicao_ultimo_dia_mes != 6)
                                                {
                                                    for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                        {
				                                        echo "<td>&nbsp</td>";
				                                    }
                                                }
				                                break;
                                            }
                                        }
                                        else
                                        {
                                            if ($dia == $dia_hoje)
                                            {
                                                echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                                <font color='blue'>$dia</font></a></strong></center></td>";
                                                $dia = $dia +1;
                                            }
                                            else
                                            {
                                                echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                                $dia = $dia +1;
                                            }
                                            if ($dia > $ultimo_dia_mes_hoje )
                                            {
                                                if ($posicao_ultimo_dia_mes != 6)
                                                {
                                                    for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                        {
                                                        echo "<td>&nbsp</td>";
                                                    }
                                                }
				                                break;
                                            }
                                        }
                                    }
                                    echo "</tr>";
                                }while ($dia <= $ultimo_dia_mes_hoje);
                            }
                            echo "</table>";//Encerrando a tabela do calend�rio


                            /*** Espa�os ***/
                            echo "<tr><td colspan=2>&nbsp</td></tr>";
                            echo "<tr><td colspan=2>&nbsp</td></tr>";


                            echo "</table>";//Encerra tabela para montar a p�gina da agenda
                        }
                    }
                    else
                    {

/******************************************************************************************************************/

                        $visoes="0";
                        $vis="0";
                        $tipo_visao="0";
                        $tip_vis="0";

                        /*** Espa�o ***/
                        echo"<tr><td colspan=2>&nbsp</td></tr>";


                        /*** Tabela do calend�rio ***/
                        echo "<tr><td colspan=2><table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD></td></tr>";


                        /*** Primeira linha do calend�rio ***/
                        echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
                        <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
                        <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


                        /*** Linha dos dias da semana do calend�rio ***/
                        echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
                        <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
                        <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


                        /*** Dias do calend�rio ***/
                        $contador = 0;
                        $dia = 1;
                        echo "<tr class=zebraA>";
                        if ($contador == 0)
                        {
	                        for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	                        {
		                        echo "<td>&nbsp</td>";
	                        }
                            for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
                            {
                                $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                $conn = &ADONewConnection($A_DB_TYPE);
                                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                from destinatario_agendamento_agenda,
                                agendamento_evento_agenda
                                where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                and agendamento_evento_agenda.data_evento='$dtevento'";
                                $resp2=mysql_query($sql2);
                                $linha2=mysql_num_rows($resp2);

                                if($linha2>0)
                                {
                                    if($dia==$dia_hoje)
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='purple'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                    else
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='red'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                }
                                else
                                {
                                    if ($dia == $dia_hoje)
                                    {
                                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                        <font color='blue'>$dia</font></a></strong></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                    else
                                    {
                                        echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                        $dia = $dia +1;
                                        $contador = $contador + 1;
                                    }
                                }
	                        }
                        }
                        if($contador != 0)
                        {
	                        do
	                        {
		                        echo "</tr>";
		                        echo "<tr class=zebraA>";
                                for ($cont= 0; $cont < 7; $cont++)
                                {
                                    $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                                    $conn = &ADONewConnection($A_DB_TYPE);
                                    $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                    $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                                    from destinatario_agendamento_agenda,
                                    agendamento_evento_agenda
                                    where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                                    and agendamento_evento_agenda.data_evento='$dtevento'";
                                    $resp2=mysql_query($sql2);
                                    $linha2=mysql_num_rows($resp2);

                                    if($linha2>0)
                                    {
                                        if($dia==$dia_hoje)
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='purple'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                            $contador = $contador + 1;
                                        }
                                        else
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='red'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                            $contador = $contador + 1;
                                        }
                                        if ($dia > $ultimo_dia_mes_hoje )
                                        {
                                            if ($posicao_ultimo_dia_mes != 6)
                                            {
                                                for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                    {
				                                    echo "<td>&nbsp</td>";
				                                }
                                            }
				                            break;
                                        }
                                    }
                                    else
                                    {
                                        if ($dia == $dia_hoje)
                                        {
                                            echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                                            <font color='blue'>$dia</font></a></strong></center></td>";
                                            $dia = $dia +1;
                                        }
                                        else
                                        {
                                            echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                                            $dia = $dia +1;
                                        }
                                        if ($dia > $ultimo_dia_mes_hoje )
                                        {
                                            if ($posicao_ultimo_dia_mes != 6)
                                            {
                                                for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                                    {
                                                    echo "<td>&nbsp</td>";
                                                }
                                            }
				                            break;
                                        }
                                    }
                                }
                                echo "</tr>";
                            }while ($dia <= $ultimo_dia_mes_hoje);
                        }
                        echo "</table>";//Encerrando a tabela do calend�rio


                        /*** Espa�os ***/
                        echo "<tr><td colspan=2>&nbsp</td></tr>";
                        echo "<tr><td colspan=2>&nbsp</td></tr>";

                        echo "</table>";//Encerra tabela para montar a p�gina da agenda
                        
                        if(isset($tipo_visao))
                        {
                            $tip_vis=$tipo_visao;
                        }
                    }
                }
            }
        }
    }
}
else
{

/******************************************************************************************************************/

    /*** Espa�o ***/
    //echo"<tr><td colspan=2>&nbsp</td></tr>";


    /*** Tabela do calend�rio ***/
    echo "<tr><td colspan=2><table align=left border=0 cellspacing=1 cellpadding=10 bgColor=#009ACD></td></tr>";


    /*** Primeira linha do calend�rio ***/
    echo "<tr><td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Retroceder><<</a></center></td>
    <td bgColor=#009ACD colspan=5><center><b>$nome_mes_hoje $ano_hoje</b></center></td>
    <td bgColor=#009ACD colspan=1><center><a href=n_index_agenda.php?opcao=Avancar>>></a></center></td></tr>";


    /*** Linha dos dias da semana do calend�rio ***/
    echo "<tr class=zebraB><td>".A_LANG_AGENDA_DOMINGO."</td><td>".A_LANG_AGENDA_SEGUNDA."</td>
    <td>".A_LANG_AGENDA_TERCA."</td><td>".A_LANG_AGENDA_QUARTA."</td><td>".A_LANG_AGENDA_QUINTA."</td>
    <td>".A_LANG_AGENDA_SEXTA."</td><td>".A_LANG_AGENDA_SABADO."</td></tr>";


    /*** Dias do calend�rio ***/
    $contador = 0;
    $dia = 1;
    echo "<tr class=zebraA>";
    if ($contador == 0)
    {
	    for ($cont= 0; $cont < $semana_primeiro_dia_mes; $cont++)
	    {
		    echo "<td>&nbsp</td>";
	    }
        for ($cont = $semana_primeiro_dia_mes; $cont < 7; $cont ++)
        {
            $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
            $conn = &ADONewConnection($A_DB_TYPE);
            $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
            $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
            from destinatario_agendamento_agenda,
            agendamento_evento_agenda
            where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
            and destinatario_agendamento_agenda.id_usuario='$id_usuario'
            and agendamento_evento_agenda.data_evento='$dtevento'";
            $resp2=mysql_query($sql2);
            $linha2=mysql_num_rows($resp2);

            if($linha2>0)
            {
                if($dia==$dia_hoje)
                {
                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                    <font color='purple'>$dia</font></a></strong></center></td>";
                    $dia = $dia +1;
                    $contador = $contador + 1;
                }
                else
                {
                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                    <font color='red'>$dia</font></a></strong></center></td>";
                    $dia = $dia +1;
                    $contador = $contador + 1;
                }
            }
            else
            {
                if ($dia == $dia_hoje)
                {
                    echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                    <font color='blue'>$dia</font></a></strong></center></td>";
                    $dia = $dia +1;
                    $contador = $contador + 1;
                }
                else
                {
                    echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                    $dia = $dia +1;
                    $contador = $contador + 1;
                }
            }
	    }
    }
    if($contador != 0)
    {
	    do
	    {
		    echo "</tr>";
		    echo "<tr class=zebraA>";
            for ($cont= 0; $cont < 7; $cont++)
            {
                $dtevento=$ano_hoje."-".$mes_hoje."-".$dia;
                $conn = &ADONewConnection($A_DB_TYPE);
                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                $sql2="select destinatario_agendamento_agenda.id_agendamento_evento
                from destinatario_agendamento_agenda,
                agendamento_evento_agenda
                where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                and agendamento_evento_agenda.data_evento='$dtevento'";
                $resp2=mysql_query($sql2);
                $linha2=mysql_num_rows($resp2);

                if($linha2>0)
                {
                    if($dia==$dia_hoje)
                    {
                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                        <font color='purple'>$dia</font></a></strong></center></td>";
                        $dia = $dia +1;
                        $contador = $contador + 1;
                    }
                    else
                    {
                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                        <font color='red'>$dia</font></a></strong></center></td>";
                        $dia = $dia +1;
                        $contador = $contador + 1;
                    }
                    if ($dia > $ultimo_dia_mes_hoje )
                    {
                        if ($posicao_ultimo_dia_mes != 6)
                        {
                            for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                {
                                echo "<td>&nbsp</td>";
                            }
                        }
				        break;
                    }
                }
                else
                {
                    if ($dia == $dia_hoje)
                    {
                        echo "<td><center><strong><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>
                        <font color='blue'>$dia</font></a></strong></center></td>";
                        $dia = $dia +1;
                    }
                    else
                    {
                        echo "<td><center><a href=n_index_agenda.php?opcao=EventoAgendado&diaevento=$dia&mesevento=$mes_hoje&anoevento=$ano_hoje>$dia</a></center></td>";
                        $dia = $dia +1;
                    }
                    if ($dia > $ultimo_dia_mes_hoje )
                    {
                        if ($posicao_ultimo_dia_mes != 6)
                        {
                            for ($cont = $posicao_ultimo_dia_mes + 1; $cont < 7; $cont++)
 			                {
				                echo "<td>&nbsp</td>";
				            }
                        }
				        break;
                    }
                }
            }
            echo "</tr>";
        }while ($dia <= $ultimo_dia_mes_hoje);
    }
    echo "</table>";//Encerrando a tabela do calend�rio


    /*** Espa�os ***/
    echo "<tr><td colspan=2>&nbsp</td></tr>";
    echo "<tr><td colspan=2>&nbsp</td></tr>";


    echo "</table>";//Encerra tabela para montar a p�gina da agenda
}


?>


                 </p>
				</td>
			</tr>
			</table>
		</td>
	</tr>
</table>


