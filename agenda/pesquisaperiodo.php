<?

/* ---------------------------------------------------------------------
 * P�gina de pesquisa por per�odo                                      *
 * @author Carina Tissa Aihara <tissa.aihara@gmail.com>                *
 * @version 1.0 <01/11/2009>                                           *
 *                                                                     *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.   *
 * Como Estagio Curricular.                                            *
 * Orientadora: Avanilde Kemczinski                                    *
 * Suporvisora: Edino Lopes Fernandes                                  *
 *                                                                     *
 ----------------------------------------------------------------------*/

//Cria matriz de orelha
$orelha = array();
$orelha = array
(
    array
    (
        "LABEL" => A_LANG_AGENDA_PESQUISA_PERIODO,
        "LINK" => "index.php",
        "ESTADO" =>"ON"
    )
);

MontaOrelha($orelha);
?>

<!--Monta tabela de fundo-->
<table CELLSPACING="5" CELLPADDING="5" border="0" width="100%" bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> height="80%">
    <tr valign="top">
        <td>
            <br>
                <table border="0"
                    <br>
                        <td valign="top">
                            <td>
                            
<?
/*** Estabelecer uma conex�o com o banco de dados ***/
include "config/configuracoes.php";
$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;
include "include/conecta.php";


session_start();
session_register("inicio");
session_register("final");
session_register("d");

if(isset($_POST["excluir"]))
{
    if(isset ($_POST["dest"]))
    {
        $d=$dest;
        echo"".A_LANG_AGENDA_DESEJA_REALMENTE_EXCLUIR."";
        ?>
        <!--- Inicia formul�rio --->
        <form name="form" action="n_index_agenda.php?opcao=PesquisaPeriodo&result&dataini=<? echo $inicio?>&datafim=<? echo $final?>" method="post">
        <?
        echo "<tr><td><input name=excluir1 class=button type=submit value=\"".A_LANG_AGENDA_SIM."\"></input>";
        echo"&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_NAO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=EventoAgendado&diaevento=$diaevento&mesevento=$mesevento&anoevento=$anoevento';\"></input>";
        echo "</td></tr>";
        exit;
    }
    else
    {
        echo "<table border=0 width=100%>";
        echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>N�o foi selecionado nenhum usu�rio!Por favor selecione!</td></tr>";
    }
}

if(isset($_POST["excluir1"]))
{
    $destinat=$d;
    $quantid=count($destinat);

    for($cont=0; $cont < $quantid; $cont++)
    {
        $num_letras = strlen($destinat[$cont]);

        for($letra=0; $letra < $num_letras; $letra++)
        {
            if ($destinat[$cont][$letra]=="*" && $destinat[$cont][$letra+1]=="*")
            {
                $num_primeiro_simbolo = $letra;
                break;
            }
        }

        for ($letra=$num_primeiro_simbolo+2; $letra <= $num_letras; $letra++)
        {
            if($destinat[$cont][$letra]=="*" && $destinat[$cont][$letra+1]=="*")
            {
                $num_segundo_simbolo = $letra;
                break;
            }
        }
        for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
        {
            if($letra==0)
            {
                $id_dest=$destinat[$cont][$letra];
            }
            else
            {
                $id_dest=$id_dest.$destinat[$cont][$letra];
            }
        }
        for ($letra=$num_primeiro_simbolo + 2; $letra <= $num_segundo_simbolo - 1; $letra++)
        {
            if($letra==$num_primeiro_simbolo + 2)
            {
                $id_agendam=$destinat[$cont][$letra];
            }
            else
            {
                $id_agendam=$id_agendam.$destinat[$cont][$letra];
            }
        }
        for ($letra=$num_segundo_simbolo + 2; $letra <= $num_letras; $letra++)
        {
            if($letra==$num_segundo_simbolo + 2)
            {
                $grup=$destinat[$cont][$letra];
            }
            else
            {
                $grup=$grup.$destinat[$cont][$letra];
            }
        }
        $conn = &ADONewConnection($A_DB_TYPE);
        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
        $sql2="delete
        from destinatario_agendamento_agenda
        where destinatario_agendamento_agenda.id_agendamento_evento='$id_agendam'
        and destinatario_agendamento_agenda.id_usuario='$id_dest'
        and destinatario_agendamento_agenda.grupo_destinatario='$grup'";
        $rs2 = $conn->Execute($sql2);
        if($rs2 == false)
        {
            echo "<table border=0 width=100%>";
            echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
            echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='PesquisaPeriodo&result&dataini=$inicio&datafim=$final';\"></input></td></tr>";
            exit;
        }
        else
        {
            $sql="select destinatario_agendamento_agenda.id_usuario
            from destinatario_agendamento_agenda
            where destinatario_agendamento_agenda.id_agendamento_evento='$id_agendam'
            and destinatario_agendamento_agenda.grupo_destinatario='$grup'";
            $resp=mysql_query($sql);
            $quantidade=mysql_num_rows($resp);
            if($quantidade==0)
            {
                $conn = &ADONewConnection($A_DB_TYPE);
                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                $sql2="delete
                from agendamento_evento_agenda
                where agendamento_evento_agenda.id_agendamento_evento='$id_agendam'";
                $rs2 = $conn->Execute($sql2);
                if($rs2 == false)
                {
                    echo "<table border=0 width=100%>";
                    echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                    echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=PesquisaPeriodo&result&dataini=$inicio&datafim=$final';\"></input></td></tr>";
                    exit;
                }
            }
        }
    }
    echo "<table border=0 width=100%>";
    echo "<tr><td><img src=\"imagens/ok.gif\">".A_LANG_AGENDA_DEST_EXCLUIDO_SUCESSO."</td></tr>";
    echo "<tr><td><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=PesquisaPeriodo&result&dataini=$inicio&datafim=$final';\"></input></td></tr>";
    exit;
}




if(isset($_POST["pesquisar"]))
{
    $visao_pesq=$_POST["visao"];
    $inicio=$_POST["ini"];
    $final=$_POST["fim"];
    
    if($inicio!="" && $final!="")
    {
        $num_letras = strlen($inicio);

        $letra=0;
        do
        {
            if ($inicio[$letra] == "/")
            {
                $num_primeiro_simbolo = $letra;
            }
            $letra=$letra+1;
        }while($letra<=2);

        for ($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
        {
            if ($inicio[$letra]=="/")
            {
                $num_segundo_simbolo = $letra;
            }
        }

        for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
        {
            if($letra==0)
            {
                $dia_datai=$inicio[$letra];
            }
            else
            {
                $dia_datai=$dia_datai.$inicio[$letra];
            }
        }
        for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
        {
            if($letra==$num_primeiro_simbolo + 1)
            {
                $mes_datai=$inicio[$letra];
            }
            else
            {
                $mes_datai=$mes_datai.$inicio[$letra];
            }
        }
        for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
        {
            if($letra==$num_segundo_simbolo + 1)
            {
                $ano_datai=$inicio[$letra];
            }
            else
            {
                $ano_datai=$ano_datai.$inicio[$letra];
            }
        }
    
        $num_letras = strlen($final);

        $letra=0;
        do
        {
            if ($final[$letra] == "/")
            {
                $num_primeiro_simbolo = $letra;
            }
            $letra=$letra+1;
        }while($letra<=2);

        for ($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
        {
            if ($final[$letra]=="/")
            {
                $num_segundo_simbolo = $letra;
            }
        }

        for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
        {
            if($letra==0)
            {
                $dia_dataf=$final[$letra];
            }
            else
            {
                $dia_dataf=$dia_dataf.$final[$letra];
            }
        }
        for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
        {
            if($letra==$num_primeiro_simbolo + 1)
            {
                $mes_dataf=$final[$letra];
            }
            else
            {
                $mes_dataf=$mes_dataf.$final[$letra];
            }
        }
        for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
        {
            if($letra==$num_segundo_simbolo + 1)
            {
                $ano_dataf=$final[$letra];
            }
            else
            {
                $ano_dataf=$ano_dataf.$final[$letra];
            }
        }
 
        if(checkdate($mes_datai, $dia_datai, $ano_datai) && checkdate($mes_dataf, $dia_dataf, $ano_dataf))
        {
            $data_inicio=$ano_datai."-".$mes_datai."-".$dia_datai;
            $data_fim=$ano_dataf."-".$mes_dataf."-".$dia_dataf;
        
            $i=strtotime($data_inicio);
            $f=strtotime($data_fim);
        
            if($f<$i)
            {
                echo "<table border=0 width=100%>";
                echo "<tr><td><img src=\"imagens/erro.gif\">&nbsp".A_LANG_AGENDA_PER�ODO_INVALIDO."</td></tr>";
                echo"<tr><td colspan=2>&nbsp</td></tr>";
            }
            else
            {
                /*** Eventos do dia ***/
                echo "<table align=center border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD>";
                echo "<tr><td bgColor=#009ACD colspan=1><b><center>Eventos de $inicio � $final</center></b></td></tr>";

                /*** Vetores auxiliares ***/
                $auxiliar_agendamento=array();
                $auxiliar_grupo_destinatario=array();
                $auxiliar_evento=array();


                if($visao_pesq=="0")
                {
                    /*** Id do agendamento, grupo do destinat�rio, id do evento da data fornecida ***/
                    $sql="select destinatario_agendamento_agenda.id_agendamento_evento, destinatario_agendamento_agenda.grupo_destinatario, agendamento_evento_agenda.id_evento_agenda
                    from agendamento_evento_agenda, destinatario_agendamento_agenda
                    where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                    and agendamento_evento_agenda.data_evento BETWEEN ('$data_inicio') AND ('$data_fim')
                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                    union
                    select destinatario_agendamento_agenda.id_agendamento_evento, destinatario_agendamento_agenda.grupo_destinatario, agendamento_evento_agenda.id_evento_agenda
                    from agendamento_evento_agenda, destinatario_agendamento_agenda, evento_agenda
                    where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                    and evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
                    and agendamento_evento_agenda.data_evento BETWEEN ('$data_inicio') AND ('$data_fim')
                    and evento_agenda.id_usuario='$id_usuario'";
                    $resp=mysql_query($sql);
                    $quantidade=mysql_num_rows($resp);
                }
                else
                {
                    /*** Id do agendamento, grupo do destinat�rio, id do evento da data fornecida ***/
                    $sql="select destinatario_agendamento_agenda.id_agendamento_evento, destinatario_agendamento_agenda.grupo_destinatario, agendamento_evento_agenda.id_evento_agenda
                    from agendamento_evento_agenda, destinatario_agendamento_agenda, evento_agenda
                    where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                    and evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
                    and agendamento_evento_agenda.data_evento BETWEEN ('$data_inicio') AND ('$data_fim')
                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'
                    and evento_agenda.assunto_evento='$visao_pesq'
                    union
                    select destinatario_agendamento_agenda.id_agendamento_evento, destinatario_agendamento_agenda.grupo_destinatario, agendamento_evento_agenda.id_evento_agenda
                    from agendamento_evento_agenda, destinatario_agendamento_agenda, evento_agenda
                    where destinatario_agendamento_agenda.id_agendamento_evento=agendamento_evento_agenda.id_agendamento_evento
                    and evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
                    and agendamento_evento_agenda.data_evento BETWEEN ('$data_inicio') AND ('$data_fim')
                    and evento_agenda.id_usuario='$id_usuario'
                    and evento_agenda.assunto_evento='$visao_pesq'";
                    $resp=mysql_query($sql);
                    $quantidade=mysql_num_rows($resp);
                }

                /*** Aviso se n�o houver eventos na data fornecida ***/
                if($quantidade==0)
                {
                    echo"<tr class=zebraB><td><center><font color=red>".A_LANG_AGENDA_NENHUM_EVENTO_PERIODO."</td></tr>";
                }

                /*** Passando os resultados do select para os vetores auxiliares ***/
                while($linha = mysql_fetch_array($resp))
                {
                    $auxiliar_agendamento[]=$linha[0];
                    $auxiliar_grupo_destinatario[]=$linha[1];
                    $auxiliar_evento[]=$linha[2];
                }

                for($cont=0; $cont<$quantidade; $cont++)
                {
                    if($cont%2==0)
                    {
                        //Tipo do evento
                        $sql1="select tipo_evento_agenda.nome_tipo_evento
                        from tipo_evento_agenda, evento_agenda
                        where tipo_evento_agenda.id_tipo_evento=evento_agenda.id_tipo_evento
                        and evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                        $resp1=mysql_query($sql1);
                        $linha1 = mysql_fetch_row($resp1);
                        echo"<tr class=zebraB><td><strong><center>$linha1[0]</td></tr>";

                        //Data de recebimento
                        $sql12="select destinatario_agendamento_agenda.data_envio
                        from destinatario_agendamento_agenda
                        where destinatario_agendamento_agenda.id_agendamento_evento='$auxiliar_agendamento[$cont]'
                        and destinatario_agendamento_agenda.grupo_destinatario='$auxiliar_grupo_destinatario[$cont]'";
                        $resp12=mysql_query($sql12);
                        $linha12 = mysql_fetch_row($resp12);
                        echo"<tr class=zebraB><td>".A_LANG_AGENDA_DATA_RECEBIMENTO.": $linha12[0]</td></tr>";

                        //Data de recebimento
                        $sql13="select evento_agenda.data_atualizacao_evento
                        from evento_agenda
                        where evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                        $resp13=mysql_query($sql13);
                        $num_linha13 = mysql_num_rows($resp13);

                        $sql14="select agendamento_evento_agenda.data_atualizacao_agendamento
                        from agendamento_evento_agenda
                        where agendamento_evento_agenda.id_agendamento_evento='$auxiliar_agendamento[$cont]'";
                        $resp14=mysql_query($sql14);
                        $num_linha14 = mysql_num_rows($resp14);

                        if($num_linha13 > 0 && $num_linha14==0)
                        {
                            $linha13=mysql_fetch_row($resp13);
                            echo"<tr class=zebraB><td>".A_LANG_AGENDA_DATA_ULTIMA_ATUALIZACAO.": $linha13[0]</td></tr>";
                        }
                        else
                        {
                            if($num_linha13==0 && $num_linha14 > 0)
                            {
                                $linha14=mysql_fetch_row($resp14);
                                echo"<tr class=zebraB><td>".A_LANG_AGENDA_DATA_ULTIMA_ATUALIZACAO.": $linha14[0]</td></tr>";
                            }
                            else
                            {
                                if($num_linha13 > 0 && $num_linha14 > 0)
                                {
                                    $linha13=mysql_fetch_row($resp13);
                                    $linha14=mysql_fetch_row($resp14);
                                    $aux=strtotime($linha13);
                                    $aux1=strtotime($linha14);
                                    if($aux>$aux1)
                                    {
                                        echo"<tr class=zebraB><td>".A_LANG_AGENDA_DATA_ULTIMA_ATUALIZACAO.": $linha13[0]</td></tr>";
                                    }
                                    else
                                    {
                                        echo"<tr class=zebraB><td>".A_LANG_AGENDA_DATA_ULTIMA_ATUALIZACAO.": $linha14[0]</td></tr>";
                                    }
                                }
                            }
                        }

                        //Nome e E-mail do autor
                        $sql2="select evento_agenda.id_usuario, usuario.nome_usuario, usuario.email_usuario
                        from evento_agenda, usuario
                        where usuario.id_usuario=evento_agenda.id_usuario
                        and evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                        $resp2=mysql_query($sql2);
                        $linha2 = mysql_fetch_row($resp2);
                        echo"<tr class=zebraB><td>".A_LANG_AGENDA_AUTOR_EVENTO.": $linha2[1]</td></tr>";
                        echo"<tr class=zebraB><td>".A_LANG_AGENDA_EMAIL.": $linha2[2]</td></tr>";

                        //Tipo do assunto
                        $sql3="select evento_agenda.id_tipo_assunto, tipo_assunto_agenda.nome_tipo_assunto
                        from tipo_assunto_agenda, evento_agenda
                        where tipo_assunto_agenda.id_tipo_assunto=evento_agenda.id_tipo_assunto
                        and evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                        $resp3=mysql_query($sql3);
                        $linha3= mysql_fetch_row($resp3);
                        echo"<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.": $linha3[1]</td></tr>";

                        //Assunto
                        $sql4="select evento_agenda.assunto_evento
                        from evento_agenda
                        where evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                        $resp4=mysql_query($sql4);
                        $linha4 = mysql_fetch_row($resp4);
                        echo"<tr class=zebraB><td>".A_LANG_AGENDA_ASSUNTO.": $linha4[0]</td></tr>";


                        //Nome do evento e Descri��o do evento
                        $sql5="select evento_agenda.nome_evento, evento_agenda.descricao_evento
                        from evento_agenda
                        where evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                        $resp5=mysql_query($sql5);
                        $linha5= mysql_fetch_row($resp5);
                        echo"<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.": $linha5[0]</td></tr>";
                        echo"<tr class=zebraB><td>".A_LANG_AGENDA_DESCRICAO.": $linha5[1]</td></tr>";
        
                        //Data do evento
                        $sql6="select agendamento_evento_agenda.data_evento
                        from agendamento_evento_agenda
                        where agendamento_evento_agenda.id_agendamento_evento='$auxiliar_agendamento[$cont]'";
                        $resp6=mysql_query($sql6);
                        $linha6 = mysql_fetch_row($resp6);
                        echo"<tr class=zebraB><td>".A_LANG_AGENDA_DATA_EVENTO.": $linha6[0]</td></tr>";
                        $dat=$linha6[0];

                        //Hor�rio do evento
                        $sql6="select agendamento_evento_agenda.hora_inicio_evento, agendamento_evento_agenda.hora_fim_evento
                        from agendamento_evento_agenda
                        where agendamento_evento_agenda.id_agendamento_evento='$auxiliar_agendamento[$cont]'";
                        $resp6=mysql_query($sql6);
                        $linha6 = mysql_fetch_row($resp6);
                        echo"<tr class=zebraB><td>".A_LANG_AGENDA_HORA_INICIO.": $linha6[0]</td></tr>";
                        echo"<tr class=zebraB><td>".A_LANG_AGENDA_HORA_FIM.": $linha6[1]</td></tr>";

                        //Destino do evento
                        $sql7="select DISTINCT destinatario_agendamento_agenda.destino_evento
                        from destinatario_agendamento_agenda
                        where destinatario_agendamento_agenda.id_agendamento_evento='$auxiliar_agendamento[$cont]'
                        and destinatario_agendamento_agenda.grupo_destinatario='$auxiliar_grupo_destinatario[$cont]'";
                        $resp7=mysql_query($sql7);
                        $linha7 = mysql_fetch_row($resp7);



                        //Destinat�rios se houver
                        if($linha7[0]=='Minha Agenda')
                        {
                            $sql8="select evento_agenda.id_usuario
                            from evento_agenda
                            where evento_agenda.id_usuario='$id_usuario'
                            and evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                            $resp8=mysql_query($sql8);
                            $linha8 = mysql_num_rows($resp8);
                            $sql9="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario
                            from usuario, destinatario_agendamento_agenda
                            where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                            and destinatario_agendamento_agenda.id_agendamento_evento='$auxiliar_agendamento[$cont]'
                            and destinatario_agendamento_agenda.grupo_destinatario='$auxiliar_grupo_destinatario[$cont]'
                            order by nome_usuario";
                            $resp9=mysql_query($sql9);
                            if($linha8!=0)
                            {
                                echo"<form name=formularioexclui action='n_index_agenda.php?opcao=PesquisaPeriodo&result&dataini=$inicio&datafim=$final' method=post>";
                                echo "<tr class=zebraB><td>".A_LANG_AGENDA_DESTINATARIOS.": <select class=select name=dest[] MULTIPLE size=2>";
                                while ($array = mysql_fetch_array($resp9))
                                {
                                    echo "<option value=\"".$array[0].'**'.$auxiliar_agendamento[$cont].'**'.$auxiliar_grupo_destinatario[$cont]."\">".$array[1]."</option>";
                                }
                                echo"</select>";
                
                
                                $dia_hoje = date ("d");
                                $mes_hoje = date ("m");
                                $ano_hoje = date ("Y");
                                $data_hoje=$ano_hoje."-".$mes_hoje."-".$dia_hoje;
                                $sql="select DATEDIFF('$dat','$data_hoje')";
                                $resp=mysql_query($sql);
                                $linha = mysql_fetch_row($resp);
                                $resp=$linha[0];
                                if($resp>=0)
                                {
                                    echo"&nbsp<input id=bot2 class=button type=submit name=excluir value=\"".A_LANG_AGENDA_EXCLUIR."\"></input></td></tr>";
                                    echo"<tr class=zebraB><td align=center>".A_LANG_AGENDA_SELECIONAR_USUARIO_GRUPO."</td></tr>";
                                }
                                else
                                {
                                    echo"&nbsp<input id=bot2 class=button type=submit name=excluir value=\"".A_LANG_AGENDA_EXCLUIR."\" disabled></input></td></tr>";
                                    echo"<tr class=zebraB><td align=center>".A_LANG_AGENDA_SELECIONAR_USUARIO_GRUPO."</td></tr>";
                                }
                                echo"</form>";
                            }
                        }

                        if($linha7[0]=='Grupo de Usu�rios')
                        {
                            //Destinat�rios se houver
                            $sql8="select evento_agenda.id_usuario
                            from evento_agenda
                            where evento_agenda.id_usuario='$id_usuario'
                            and evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                            $resp8=mysql_query($sql8);
                            $linha8 = mysql_num_rows($resp8);
                            $sql9="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario
                            from usuario, destinatario_agendamento_agenda
                            where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                            and destinatario_agendamento_agenda.id_agendamento_evento='$auxiliar_agendamento[$cont]'
                            and destinatario_agendamento_agenda.grupo_destinatario='$auxiliar_grupo_destinatario[$cont]'
                            order by nome_usuario";
                            $resp9=mysql_query($sql9);
                            if($linha8!=0)
                            {
                                $sql10="SELECT destinatario_agendamento_agenda.id_tipo_grupo_destinatario, tipo_grupo_destinatario_agenda.nome_tipo_grupo_destinatario
                                FROM destinatario_agendamento_agenda, tipo_grupo_destinatario_agenda
                                WHERE destinatario_agendamento_agenda.id_tipo_grupo_destinatario = tipo_grupo_destinatario_agenda.id_tipo_grupo_destinatario
                                AND destinatario_agendamento_agenda.id_agendamento_evento = '$auxiliar_agendamento[$cont]'
                                and destinatario_agendamento_agenda.grupo_destinatario = '$auxiliar_grupo_destinatario[$cont]'";
                                $resp10=mysql_query($sql10);
                                $linha10 = mysql_fetch_row($resp10);
                                echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_GRUPO_DESTINATARIO.": $linha10[1]</td></tr>";
                                if($linha10[0]==1)
                                {
                                    $sql11="select disciplina.nome_disc, usuario.nome_usuario
                                    from disciplina, usuario, destinatario_agendamento_agenda
                                    where destinatario_agendamento_agenda.grupo_destinatario_id_disc=disciplina.id_disc
                                    and destinatario_agendamento_agenda.grupo_destinatario_id_professor=usuario.id_usuario
                                    and destinatario_agendamento_agenda.id_agendamento_evento = '$auxiliar_agendamento[$cont]'
                                    and destinatario_agendamento_agenda.grupo_destinatario = '$auxiliar_grupo_destinatario[$cont]'";
                                    $resp11=mysql_query($sql11);
                                    $linha11 = mysql_fetch_row($resp11);
                                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_GRUPO_DESTINATARIO.": $linha11[0] - $linha11[1]</td></tr>";
                                }
                                if($linha10[0]==2)
                                {
                                    $sql11="select curso.nome_curso, usuario.nome_usuario
                                    from curso, usuario, destinatario_agendamento_agenda
                                    where destinatario_agendamento_agenda.grupo_destinatario_id_curso=curso.id_curso
                                    and destinatario_agendamento_agenda.grupo_destinatario_id_professor=usuario.id_usuario
                                    and destinatario_agendamento_agenda.id_agendamento_evento = '$auxiliar_agendamento[$cont]'
                                    and destinatario_agendamento_agenda.grupo_destinatario = '$auxiliar_grupo_destinatario[$cont]'";
                                    $resp11=mysql_query($sql11);
                                    $linha11 = mysql_fetch_row($resp11);
                                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_GRUPO_DESTINATARIO.": $linha11[0] - $linha11[1]</td></tr>";
                                }
                                if($linha10[0]==3)
                                {
                                    $sql11="select disciplina.nome_disc, curso.nome_curso, usuario.nome_usuario
                                    from disciplina, curso, usuario, destinatario_agendamento_agenda
                                    where destinatario_agendamento_agenda.grupo_destinatario_id_disc=disciplina.id_disc
                                    and destinatario_agendamento_agenda.grupo_destinatario_id_curso=curso.id_curso
                                    and destinatario_agendamento_agenda.grupo_destinatario_id_professor=usuario.id_usuario
                                    and destinatario_agendamento_agenda.id_agendamento_evento = '$auxiliar_agendamento[$cont]'
                                    and destinatario_agendamento_agenda.grupo_destinatario = '$auxiliar_grupo_destinatario[$cont]'";
                                    $resp11=mysql_query($sql11);
                                    $linha11 = mysql_fetch_row($resp11);
                                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_GRUPO_DESTINATARIO.": $linha11[0] / $linha11[1] - $linha11[2]</td></tr>";
                                }
                                if($linha10[0]==4 || $linha10[0]==5 || $linha10[0]==6 || $linha10[0]==7 || $linha10[0]==8)
                                {
                                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_GRUPO_DESTINATARIO.": $auxiliar_grupo_destinatario[$cont]</td></tr>";
                                }
                                echo"<form name=formularioexclui action='n_index_agenda.php?opcao=PesquisaPeriodo&result&dataini=$inicio&datafim=$final' method=post>";
                                echo "<tr class=zebraB><td>".A_LANG_AGENDA_DESTINATARIOS.": <select class=select name=dest[] MULTIPLE size=2>";
                                while ($array = mysql_fetch_array($resp9))
                                {
                                    echo "<option value=\"".$array[0].'**'.$auxiliar_agendamento[$cont].'**'.$auxiliar_grupo_destinatario[$cont]."\">".$array[1]."</option>";
                                }
                                echo"</select>";
                
                                $dia_hoje = date ("d");
                                $mes_hoje = date ("m");
                                $ano_hoje = date ("Y");
                                $data_hoje=$ano_hoje."-".$mes_hoje."-".$dia_hoje;
                                $sql="select DATEDIFF('$dat','$data_hoje')";
                                $resp=mysql_query($sql);
                                $linha = mysql_fetch_row($resp);
                                $resp=$linha[0];
                                if($resp>=0)
                                {
                                    echo"&nbsp<input id=bot2 class=button type=submit name=excluir value=\"".A_LANG_AGENDA_EXCLUIR."\"></input></td></tr>";
                                    echo"<tr class=zebraB><td align=center>".A_LANG_AGENDA_SELECIONAR_USUARIO_GRUPO."</td></tr>";
                                }
                                else
                                {
                                    echo"&nbsp<input id=bot2 class=button type=submit name=excluir value=\"".A_LANG_AGENDA_EXCLUIR."\" disabled></input></td></tr>";
                                    echo"<tr class=zebraB><td align=center>".A_LANG_AGENDA_SELECIONAR_USUARIO_GRUPO."</td></tr>";
                                }
                                echo"</form>";
                            }
                        }

                        if($linha8!=0)
                        {
                            $dia_hoje = date ("d");
                            $mes_hoje = date ("m");
                            $ano_hoje = date ("Y");

                            $data_hoje=$ano_hoje."-".$mes_hoje."-".$dia_hoje;

                            $sql="select DATEDIFF('$dat','$data_hoje')";
                            $resp=mysql_query($sql);
                            $linha = mysql_fetch_row($resp);
                            $resp=$linha[0];
                            if($resp>=0)
                            {
                                echo"<tr class=zebraB><td><input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_ALTERAR_AGENDAMENTO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AlterarAgendamentoPesquisa&even=$auxiliar_evento[$cont]&agen=$auxiliar_agendamento[$cont]';\"></input></td></tr>";
                            }
                            else
                            {
                                echo"<tr class=zebraB><td><input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_ALTERAR_AGENDAMENTO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AlterarAgendamentoPesquisa&even=$auxiliar_evento[$cont]&agen=$auxiliar_agendamento[$cont]';\" disabled></input></td></tr>";
                            }
                        }
                    }
                    else
                    {
                         //Tipo do evento
                         $sql1="select tipo_evento_agenda.nome_tipo_evento
                         from tipo_evento_agenda, evento_agenda
                         where tipo_evento_agenda.id_tipo_evento=evento_agenda.id_tipo_evento
                         and evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                         $resp1=mysql_query($sql1);
                         $linha1 = mysql_fetch_row($resp1);
                         echo"<tr class=zebraA><td><strong><center>$linha1[0]</td></tr>";


                         //Data de recebimento
                         $sql12="select destinatario_agendamento_agenda.data_envio
                         from destinatario_agendamento_agenda
                         where destinatario_agendamento_agenda.id_agendamento_evento='$auxiliar_agendamento[$cont]'
                         and destinatario_agendamento_agenda.grupo_destinatario='$auxiliar_grupo_destinatario[$cont]'";
                         $resp12=mysql_query($sql12);
                         $linha12 = mysql_fetch_row($resp12);
                         echo"<tr class=zebraA><td>".A_LANG_AGENDA_DATA_RECEBIMENTO.": $linha12[0]</td></tr>";

                         //Data de recebimento
                         $sql13="select evento_agenda.data_atualizacao_evento
                         from evento_agenda
                         where evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                         $resp13=mysql_query($sql13);
                         $num_linha13 = mysql_num_rows($resp13);

                         $sql14="select agendamento_evento_agenda.data_atualizacao_agendamento
                         from agendamento_evento_agenda
                         where agendamento_evento_agenda.id_agendamento_evento='$auxiliar_agendamento[$cont]'";
                         $resp14=mysql_query($sql14);
                         $num_linha14 = mysql_num_rows($resp14);

                         if($num_linha13 > 0 && $num_linha14==0)
                         {
                             $linha13=mysql_fetch_row($resp13);
                             echo"<tr class=zebraA><td>".A_LANG_AGENDA_DATA_ULTIMA_ATUALIZACAO.": $linha13[0]</td></tr>";
                         }
                         else
                         {
                             if($num_linha13==0 && $num_linha14 > 0)
                             {
                                 $linha14=mysql_fetch_row($resp14);
                                 echo"<tr class=zebraA><td>".A_LANG_AGENDA_DATA_ULTIMA_ATUALIZACAO.": $linha14[0]</td></tr>";
                             }
                             else
                             {
                                 if($num_linha13 > 0 && $num_linha14 > 0)
                                 {
                                     $linha13=mysql_fetch_row($resp13);
                                     $linha14=mysql_fetch_row($resp14);
                                     $aux=strtotime($linha13);
                                     $aux1=strtotime($linha14);
                                     if($aux>$aux1)
                                     {
                                         echo"<tr class=zebraA><td>".A_LANG_AGENDA_DATA_ULTIMA_ATUALIZACAO.": $linha13[0]</td></tr>";
                                     }
                                     else
                                     {
                                         echo"<tr class=zebraA><td>".A_LANG_AGENDA_DATA_ULTIMA_ATUALIZACAO.": $linha14[0]</td></tr>";
                                     }
                                 }
                             }
                         }

                         //Nome e E-mail do autor
                         $sql2="select evento_agenda.id_usuario, usuario.nome_usuario, usuario.email_usuario
                         from evento_agenda, usuario
                         where usuario.id_usuario=evento_agenda.id_usuario
                         and evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                         $resp2=mysql_query($sql2);
                         $linha2 = mysql_fetch_row($resp2);
                         echo"<tr class=zebraA><td>".A_LANG_AGENDA_AUTOR_EVENTO.": $linha2[1]</td></tr>";
                         echo"<tr class=zebraA><td>".A_LANG_AGENDA_EMAIL.": $linha2[2]</td></tr>";

                         //Tipo do assunto
                         $sql3="select evento_agenda.id_tipo_assunto, tipo_assunto_agenda.nome_tipo_assunto
                         from tipo_assunto_agenda, evento_agenda
                         where tipo_assunto_agenda.id_tipo_assunto=evento_agenda.id_tipo_assunto
                         and evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                         $resp3=mysql_query($sql3);
                         $linha3 = mysql_fetch_row($resp3);
                         echo"<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_ASSUNTO.": $linha3[1]</td></tr>";

                         //Assunto
                         $sql4="select evento_agenda.assunto_evento
                         from evento_agenda
                         where evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                         $resp4=mysql_query($sql4);
                         $linha4 = mysql_fetch_row($resp4);
                         echo"<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.": $linha4[0]</td></tr>";


                         //Nome do evento e Descri��o do evento
                         $sql5="select evento_agenda.nome_evento, evento_agenda.descricao_evento
                         from evento_agenda
                         where evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                         $resp5=mysql_query($sql5);
                         $linha5 = mysql_fetch_row($resp5);
                         echo"<tr class=zebraA><td>".A_LANG_AGENDA_NOME_EVENTO.": $linha5[0]</td></tr>";
                         echo"<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO.": $linha5[1]</td></tr>";
        
                         //Data do evento
                         $sql6="select agendamento_evento_agenda.data_evento
                         from agendamento_evento_agenda
                         where agendamento_evento_agenda.id_agendamento_evento='$auxiliar_agendamento[$cont]'";
                         $resp6=mysql_query($sql6);
                         $linha6 = mysql_fetch_row($resp6);
                         echo"<tr class=zebraA><td>".A_LANG_AGENDA_DATA_EVENTO.": $linha6[0]</td></tr>";
                         $dat=$linha6[0];

                         //Hor�rio do evento
                         $sql6="select agendamento_evento_agenda.hora_inicio_evento, agendamento_evento_agenda.hora_fim_evento
                         from agendamento_evento_agenda
                         where agendamento_evento_agenda.id_agendamento_evento='$auxiliar_agendamento[$cont]'";
                         $resp6=mysql_query($sql6);
                         $linha6 = mysql_fetch_row($resp6);
                         echo"<tr class=zebraA><td>".A_LANG_AGENDA_HORA_INICIO.": $linha6[0]</td></tr>";
                         echo"<tr class=zebraA><td>".A_LANG_AGENDA_HORA_FIM.": $linha6[1]</td></tr>";

                         //Destino do evento
                         $sql7="select DISTINCT destinatario_agendamento_agenda.destino_evento
                         from destinatario_agendamento_agenda
                         where destinatario_agendamento_agenda.id_agendamento_evento='$auxiliar_agendamento[$cont]'
                         and destinatario_agendamento_agenda.grupo_destinatario='$auxiliar_grupo_destinatario[$cont]'";
                         $resp7=mysql_query($sql7);
                         $linha7 = mysql_fetch_row($resp7);


                         if($linha7[0]=='Minha Agenda')
                         {
                             //Destinat�rios se houver
                             $sql8="select evento_agenda.id_usuario
                             from evento_agenda
                             where evento_agenda.id_usuario='$id_usuario'
                             and evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                             $resp8=mysql_query($sql8);
                             $linha8 = mysql_num_rows($resp8);
                             $sql9="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario
                             from usuario, destinatario_agendamento_agenda
                             where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                             and destinatario_agendamento_agenda.id_agendamento_evento='$auxiliar_agendamento[$cont]'
                             and destinatario_agendamento_agenda.grupo_destinatario='$auxiliar_grupo_destinatario[$cont]'
                             order by nome_usuario";
                             $resp9=mysql_query($sql9);
                             if($linha8!=0)
                             {
                                 echo"<form name=formularioexclui action='n_index_agenda.php?opcao=PesquisaPeriodo&result&dataini=$inicio&datafim=$final' method=post>";
                                 echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESTINATARIOS.": <select class=select name=dest[] MULTIPLE size=2>";
                                 while ($array = mysql_fetch_array($resp9))
                                 {
                                     echo "<option value=\"".$array[0].'**'.$auxiliar_agendamento[$cont].'**'.$auxiliar_grupo_destinatario[$cont]."\">".$array[1]."</option>";
                                 }
                                 echo"</select>";
                
                                 $dia_hoje = date ("d");
                                 $mes_hoje = date ("m");
                                 $ano_hoje = date ("Y");
                                 $data_hoje=$ano_hoje."-".$mes_hoje."-".$dia_hoje;
                                 $sql="select DATEDIFF('$dat','$data_hoje')";
                                 $resp=mysql_query($sql);
                                 $linha = mysql_fetch_row($resp);
                                 $resp=$linha[0];
                                 if($resp>=0)
                                 {
                                     echo"&nbsp<input id=bot2 class=button type=submit name=excluir value=\"".A_LANG_AGENDA_EXCLUIR."\"></input></td></tr>";
                                     echo"<tr class=zebraA><td align=center>".A_LANG_AGENDA_SELECIONAR_USUARIO_GRUPO."</td></tr>";
                                 }
                                 else
                                 {
                                     echo"&nbsp<input id=bot2 class=button type=submit name=excluir value=\"".A_LANG_AGENDA_EXCLUIR."\" disabled></input></td></tr>";
                                     echo"<tr class=zebraA><td align=center>".A_LANG_AGENDA_SELECIONAR_USUARIO_GRUPO."</td></tr>";
                                 }
                                 echo"</form>";
                             }
                         }

                         if($linha7[0]=='Grupo de Usu�rios')
                         {
                             //Destinat�rios se houver
                             $sql8="select evento_agenda.id_usuario
                             from evento_agenda
                             where evento_agenda.id_usuario='$id_usuario'
                             and evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
                             $resp8=mysql_query($sql8);
                             $linha8 = mysql_num_rows($resp8);
                             $sql9="select destinatario_agendamento_agenda.id_usuario, usuario.nome_usuario
                             from usuario, destinatario_agendamento_agenda
                             where destinatario_agendamento_agenda.id_usuario=usuario.id_usuario
                             and destinatario_agendamento_agenda.id_agendamento_evento='$auxiliar_agendamento[$cont]'
                             and destinatario_agendamento_agenda.grupo_destinatario='$auxiliar_grupo_destinatario[$cont]'
                             order by nome_usuario";
                             $resp9=mysql_query($sql9);
                             if($linha8!=0)
                             {
                                 $sql10="SELECT DISTINCT destinatario_agendamento_agenda.id_tipo_grupo_destinatario, tipo_grupo_destinatario_agenda.nome_tipo_grupo_destinatario
                                 FROM destinatario_agendamento_agenda, tipo_grupo_destinatario_agenda
                                 WHERE destinatario_agendamento_agenda.id_tipo_grupo_destinatario = tipo_grupo_destinatario_agenda.id_tipo_grupo_destinatario
                                 AND destinatario_agendamento_agenda.id_agendamento_evento = '$auxiliar_agendamento[$cont]'
                                 and destinatario_agendamento_agenda.grupo_destinatario = '$auxiliar_grupo_destinatario[$cont]'";
                                 $resp10=mysql_query($sql10);
                                 $linha10 = mysql_fetch_row($resp10);
                                 echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_GRUPO_DESTINATARIO.": $linha10[1]</td></tr>";
                                 if($linha10[0]==1)
                                 {
                                     $sql11="select DISTINCT disciplina.nome_disc, usuario.nome_usuario
                                     from disciplina, usuario, destinatario_agendamento_agenda
                                     where destinatario_agendamento_agenda.grupo_destinatario_id_disc=disciplina.id_disc
                                     and destinatario_agendamento_agenda.grupo_destinatario_id_professor=usuario.id_usuario
                                     and destinatario_agendamento_agenda.id_agendamento_evento = '$auxiliar_agendamento[$cont]'
                                     and destinatario_agendamento_agenda.grupo_destinatario = '$auxiliar_grupo_destinatario[$cont]'";
                                     $resp11=mysql_query($sql11);
                                     $linha11 = mysql_fetch_row($resp11);
                                     echo "<tr class=zebraA><td>".A_LANG_AGENDA_GRUPO_DESTINATARIO.": $linha11[0] - $linha11[1]</td></tr>";
                                 }
                                 if($linha10[0]==2)
                                 {
                                     $sql11="select DISTINCT curso.nome_curso, usuario.nome_usuario
                                     from curso, usuario, destinatario_agendamento_agenda
                                     where destinatario_agendamento_agenda.grupo_destinatario_id_curso=curso.id_curso
                                     and destinatario_agendamento_agenda.grupo_destinatario_id_professor=usuario.id_usuario
                                     and destinatario_agendamento_agenda.id_agendamento_evento = '$auxiliar_agendamento[$cont]'
                                     and destinatario_agendamento_agenda.grupo_destinatario = '$auxiliar_grupo_destinatario[$cont]'";
                                     $resp11=mysql_query($sql11);
                                     $linha11 = mysql_fetch_row($resp11);
                                     echo "<tr class=zebraA><td>".A_LANG_AGENDA_GRUPO_DESTINATARIO.": $linha11[0] - $linha11[1]</td></tr>";
                                 }
                                 if($linha10[0]==3)
                                 {
                                     $sql11="select DISTINCT disciplina.nome_disc, curso.nome_curso, usuario.nome_usuario
                                     from disciplina, curso, usuario, destinatario_agendamento_agenda
                                     where destinatario_agendamento_agenda.grupo_destinatario_id_disc=disciplina.id_disc
                                     and destinatario_agendamento_agenda.grupo_destinatario_id_curso=curso.id_curso
                                     and destinatario_agendamento_agenda.grupo_destinatario_id_professor=usuario.id_usuario
                                     and destinatario_agendamento_agenda.id_agendamento_evento = '$auxiliar_agendamento[$cont]'
                                     and destinatario_agendamento_agenda.grupo_destinatario = '$auxiliar_grupo_destinatario[$cont]'";
                                     $resp11=mysql_query($sql11);
                                     $linha11 = mysql_fetch_row($resp11);
                                     echo "<tr class=zebraA><td>".A_LANG_AGENDA_GRUPO_DESTINATARIO.": $linha11[0] / $linha11[1] - $linha11[2]</td></tr>";
                                 }
                                 if($linha10[0]==4 || $linha10[0]==5 || $linha10[0]==6 || $linha10[0]==7 || $linha10[0]==8)
                                 {
                                     echo "<tr class=zebraA><td>".A_LANG_AGENDA_GRUPO_DESTINATARIO.": $auxiliar_grupo_destinatario[$cont]</td></tr>";
                                 }
                                 echo"<form name=formularioexclui action='n_index_agenda.php?opcao=PesquisaPeriodo&result&dataini=$inicio&datafim=$final' method=post>";
                                 echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESTINATARIOS.": <select class=select name=dest[] MULTIPLE size=2>";
                                 while ($array = mysql_fetch_array($resp9))
                                 {
                                     echo "<option value=\"".$array[0].'**'.$auxiliar_agendamento[$cont].'**'.$auxiliar_grupo_destinatario[$cont]."\">".$array[1]."</option>";
                                 }
                                 echo"</select>";
                
                                 $dia_hoje = date ("d");
                                 $mes_hoje = date ("m");
                                 $ano_hoje = date ("Y");
                                 $data_hoje=$ano_hoje."-".$mes_hoje."-".$dia_hoje;
                                 $sql="select DATEDIFF('$dat','$data_hoje')";
                                 $resp=mysql_query($sql);
                                 $linha = mysql_fetch_row($resp);
                                 $resp=$linha[0];
                                 if($resp>=0)
                                 {
                                     echo"&nbsp<input id=bot2 class=button type=submit name=excluir value=\"".A_LANG_AGENDA_EXCLUIR."\"></input></td></tr>";
                                     echo"<tr class=zebraA><td align=center>".A_LANG_AGENDA_SELECIONAR_USUARIO_GRUPO."</td></tr>";
                                 }
                                 else
                                 {
                                     echo"&nbsp<input id=bot2 class=button type=submit name=excluir value=\"".A_LANG_AGENDA_EXCLUIR."\" disabled></input></td></tr>";
                                     echo"<tr class=zebraA><td align=center>".A_LANG_AGENDA_SELECIONAR_USUARIO_GRUPO."</td></tr>";
                                 }
                                 echo"</form>";
                             }
                         }
                         if($linha8!=0)
                         {
                             $dia_hoje = date ("d");
                             $mes_hoje = date ("m");
                             $ano_hoje = date ("Y");

                             $data_hoje=$ano_hoje."-".$mes_hoje."-".$dia_hoje;

                             $sql="select DATEDIFF('$dat','$data_hoje')";
                             $resp=mysql_query($sql);
                             $linha = mysql_fetch_row($resp);
                             $resp=$linha[0];
                             if($resp>=0)
                             {
                                 echo"<tr class=zebraA><td><input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_ALTERAR_AGENDAMENTO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AlterarAgendamentoPesquisa&even=$auxiliar_evento[$cont]&agen=$auxiliar_agendamento[$cont]';\"></input></td></tr>";
                             }
                             else
                             {
                                 echo"<tr class=zebraA><td><input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_ALTERAR_AGENDAMENTO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AlterarAgendamentoPesquisa&even=$auxiliar_evento[$cont]&agen=$auxiliar_agendamento[$cont]';\" disabled></input></td></tr>";
                             }
                         }
                     }
                 }

                 echo"</table>";

                 /*** Espa�o ***/
                 echo "<tr><td colspan=2>&nbsp</td></tr>";

                 /*** Bot�o voltar ***/
                 echo"<tr><td><input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=PesquisaPeriodo';\"></input></td></tr>";
                 exit;
             }
         }
         else
         {
             echo "<table border=0 width=100%>";
             echo "<tr><td><img src=\"imagens/erro.gif\">&nbsp".A_LANG_AGENDA_DATA_INVALIDA."</td></tr>";
             echo"<tr><td colspan=2>&nbsp</td></tr>";
         }
     }
}


?>
<!--- Pesquisa por per�odo --->
<script>
function dataConta(c)
{
    if(c.value.length==2)
    {
      c.value += '/';
    }
    else if(c.value.length==5)
    {
      c.value += '/';
    }
    else if(c.value.length==10)
    {
         document.formpesq.fim.focus();
    }
}
</script>
<?
/*** Pesquisa por per�odo ***/

echo "<form action='n_index_agenda.php?opcao=PesquisaPeriodo&result' name=formpesq method=post>";
echo "<tr><td colspan=2><a><left>".A_LANG_AGENDA_PERIODO_PESQUISA."</left></a></td></tr>";
echo "<tr><td width=70% colspan=2>
<input class=\"text\" type=\"text\" name=ini size=10 maxlength=10 value=\"".$inicio."\" onkeyup=dataConta(this);>
<a> ".A_LANG_AGENDA_ATE." </a>
<input class=\"text\" type=\"text\" name=fim id=fim size=10 maxlength=10 value=\"".$final."\" onkeyup=dataConta(this);>
</td></tr>";


echo "<tr><td colspan=2><a><left>".A_LANG_AGENDA_VISAO_DESEJADA.":</left></a></td></tr>";

/*** Vis�es ***/
$sql1="select DISTINCT evento_agenda.id_tipo_assunto, evento_agenda.assunto_evento
from evento_agenda, destinatario_agendamento_agenda, agendamento_evento_agenda
where evento_agenda.id_evento_agenda = agendamento_evento_agenda.id_evento_agenda
and agendamento_evento_agenda.id_agendamento_evento = destinatario_agendamento_agenda.id_agendamento_evento
and destinatario_agendamento_agenda.id_usuario='$id_usuario'
and evento_agenda.id_tipo_assunto='1'
union
select DISTINCT evento_agenda.id_tipo_assunto, evento_agenda.assunto_evento
from evento_agenda, destinatario_agendamento_agenda, agendamento_evento_agenda
where evento_agenda.id_evento_agenda = agendamento_evento_agenda.id_evento_agenda
and agendamento_evento_agenda.id_agendamento_evento = destinatario_agendamento_agenda.id_agendamento_evento
and destinatario_agendamento_agenda.id_usuario='$id_usuario'
and evento_agenda.id_tipo_assunto='2'
union
select DISTINCT evento_agenda.id_tipo_assunto, evento_agenda.assunto_evento
from evento_agenda, destinatario_agendamento_agenda, agendamento_evento_agenda
where evento_agenda.id_evento_agenda = agendamento_evento_agenda.id_evento_agenda
and agendamento_evento_agenda.id_agendamento_evento = destinatario_agendamento_agenda.id_agendamento_evento
and destinatario_agendamento_agenda.id_usuario='$id_usuario'
and evento_agenda.id_tipo_assunto='3'
union
select DISTINCT evento_agenda.id_tipo_assunto, evento_agenda.assunto_evento
from evento_agenda, destinatario_agendamento_agenda, agendamento_evento_agenda
where evento_agenda.id_evento_agenda = agendamento_evento_agenda.id_evento_agenda
and agendamento_evento_agenda.id_agendamento_evento = destinatario_agendamento_agenda.id_agendamento_evento
and destinatario_agendamento_agenda.id_usuario='$id_usuario'
and evento_agenda.id_tipo_assunto='4'
union
select DISTINCT evento_agenda.id_tipo_assunto, evento_agenda.assunto_evento
from evento_agenda, destinatario_agendamento_agenda, agendamento_evento_agenda
where evento_agenda.id_evento_agenda = agendamento_evento_agenda.id_evento_agenda
and agendamento_evento_agenda.id_agendamento_evento = destinatario_agendamento_agenda.id_agendamento_evento
and destinatario_agendamento_agenda.id_usuario='$id_usuario'
and evento_agenda.id_tipo_assunto='5'";
$resp1=mysql_query($sql1);
$linha1=mysql_num_rows($resp1);

echo"<tr><td><select class=select name=visao size=1>";
echo "<option selected value=\"0\">".A_LANG_AGENDA_TODAS_VISOES."</option>";
while ($array = mysql_fetch_array($resp1))
{
    if($visao_pesq==$array[1])
    {
        echo "<option selected value=\"".$array[1]."\">".$array[1]."</option>";
    }
    else
    {
        echo "<option value=\"".$array[1]."\">".$array[1]."</option>";
    }
}

echo"</select>";
echo"<tr><td><input class=button name=pesquisar type=submit value=\"".A_LANG_AGENDA_PESQUISAR."\"></td></tr>";
echo "</form>";

?>


    </td>
   </td>
  </table>
  </td>
  </tr>
</table>
