<?

/* ---------------------------------------------------------------------
 * P�gina de cadastro do evento                                        *
 * @author Carina Tissa Aihara <tissa.aihara@gmail.com>                *
 * @version 1.0 <01/11/2009>                                           *
 *                                                                     *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.   *
 * Como Estagio Curricular.                                            *
 * Orientadora: Avanilde Kemczinski                                    *
 * Suporvisora: Edino Lopes Fernandes                                  *
 *                                                                     *
 ----------------------------------------------------------------------*/


?>

<?php include ('_caminho_agenda.php'); ?>

<!--- Fun��o em JavaScript para o aparecimento de campos --->
<script language="javascript">
function ApareceAssunto()
{
    var indexSelect = document.getElementById("campotipoassunto");
    if (indexSelect.value == "0")
    {
        document.getElementById("assunto").style.display = "none";
        document.getElementById("campoassuntopartic").style.display = "none";
	    document.getElementById("campoassuntodisc").style.display = "none";
	    document.getElementById("campoassuntocurs").style.display = "none";
	    document.getElementById("campoassuntodisccurs").style.display = "none";
	    document.getElementById("campoassuntoadapt").style.display = "none";
    }
    if (indexSelect.value == "1")
    {
        document.getElementById("assunto").style.display = "block";
	    document.getElementById("campoassuntopartic").style.display = "block";
	    document.getElementById("campoassuntodisc").style.display = "none";
	    document.getElementById("campoassuntocurs").style.display = "none";
	    document.getElementById("campoassuntodisccurs").style.display = "none";
	    document.getElementById("campoassuntoadapt").style.display = "none";
    }
    if (indexSelect.value == "2")
    {
        document.getElementById("assunto").style.display = "block";
        document.getElementById("campoassuntopartic").style.display = "none";
	    document.getElementById("campoassuntodisc").style.display = "block";
	    document.getElementById("campoassuntocurs").style.display = "none";
	    document.getElementById("campoassuntodisccurs").style.display = "none";
	    document.getElementById("campoassuntoadapt").style.display = "none";
    }
    if (indexSelect.value == "3")
    {
        document.getElementById("assunto").style.display = "block";
	    document.getElementById("campoassuntopartic").style.display = "none";
	    document.getElementById("campoassuntodisc").style.display = "none";
	    document.getElementById("campoassuntocurs").style.display = "block";
	    document.getElementById("campoassuntodisccurs").style.display = "none";
	    document.getElementById("campoassuntoadapt").style.display = "none";
    }
    if (indexSelect.value == "4")
    {
        document.getElementById("assunto").style.display = "block";
	    document.getElementById("campoassuntopartic").style.display = "none";
	    document.getElementById("campoassuntodisc").style.display = "none";
	    document.getElementById("campoassuntocurs").style.display = "none";
	    document.getElementById("campoassuntodisccurs").style.display = "block";
	    document.getElementById("campoassuntoadapt").style.display = "none";
    }
    if (indexSelect.value == "5")
    {
        document.getElementById("assunto").style.display = "block";
        document.getElementById("campoassuntopartic").style.display = "none";
	    document.getElementById("campoassuntodisc").style.display = "none";
	    document.getElementById("campoassuntocurs").style.display = "none";
	    document.getElementById("campoassuntodisccurs").style.display = "none";
	    document.getElementById("campoassuntoadapt").style.display = "block";
    }
}
</script>


<script language="javascript">
function ApareceTipoEventoOutros()
{
    var indexSelect = document.getElementById("campotipoevento");
    if (indexSelect.value == "0")
    {
        document.getElementById("tipooutros").style.display = "none";
        document.getElementById("campotipoeventooutros").style.display = "none";
    }
    if (indexSelect.value == "1")
    {
        document.getElementById("tipooutros").style.display = "block";
        document.getElementById("campotipoeventooutros").style.display = "block";
    }
    if (indexSelect.value == "2")
    {
        document.getElementById("tipooutros").style.display = "none";
        document.getElementById("campotipoeventooutros").style.display = "none";
    }
    if (indexSelect.value == "3")
    {
        document.getElementById("tipooutros").style.display = "none";
        document.getElementById("campotipoeventooutros").style.display = "none";
    }
    if (indexSelect.value == "4")
    {
        document.getElementById("tipooutros").style.display = "none";
        document.getElementById("campotipoeventooutros").style.display = "none";
    }
    if (indexSelect.value == "5")
    {
        document.getElementById("tipooutros").style.display = "none";
        document.getElementById("campotipoeventooutros").style.display = "none";
    }
    if (indexSelect.value == "6")
    {
        document.getElementById("tipooutros").style.display = "none";
        document.getElementById("campotipoeventooutros").style.display = "none";
    }
    if (indexSelect.value == "7")
    {
        document.getElementById("tipooutros").style.display = "none";
        document.getElementById("campotipoeventooutros").style.display = "none";
    }
    if (indexSelect.value == "8")
    {
        document.getElementById("tipooutros").style.display = "none";
        document.getElementById("campotipoeventooutros").style.display = "none";
    }
    if (indexSelect.value == "9")
    {
        document.getElementById("tipooutros").style.display = "none";
        document.getElementById("campotipoeventooutros").style.display = "none";
    }
}
</script>

<script language="javascript">
function ApareceDestino()
{
    document.formulariodestino.submit()
}
</script>


<!--- Aparecer assunto depois de apertado cadastrar --->
<body onload="ApareceTipoEventoOutros(); ApareceAssunto();">



<?
/*** Cria matriz de orelha ***/
$orelha = array();
$orelha = array
(
    array
    (
        "LABEL" => A_LANG_AGENDA_EVENTO,
        "LINK" => "index.php",
        "ESTADO" =>"ON"
    )
);

MontaOrelha($orelha);


?>
<!--- Monta tabela de fundo --->
<table CELLSPACING="0" CELLPADDING="0" border="0" width="100%" bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> height="80%">
    <tr valign="top">
        <td>
            <br>
              <table border="0">
                 <br>
                   <td valign="top">


<?
/*** Inciciando a session ***/
session_start();
session_register("id_event");
session_register("destino_evento");


/*** Estabelecer uma conex�o com o banco de dados ***/
include "config/configuracoes.php";
$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;
include "include/conecta.php";


if (isset ($_POST["cadastrar"]))
{
    $tipo_evento=$_POST["tipoevento"];
    $tipoevento_outros=$_POST["tipoeventooutros"];
    $tipo_assunto=$_POST["tipoassunto"];
    $assunto_partic=$_POST["assuntpartic"];
    $assunto_adapt=$_POST["assuntadapt"];
    $assunto_disc=$_POST["assuntdisc"];
    $assunto_curs=$_POST["assuntcurs"];
    $assunto_disccurs=$_POST["assuntdisccurs"];
    $nome_evento=$_POST["nomeevento"];
    $descricao_evento=$_POST["descricaoevento"];
    $arquivo_evento=$_POST["MAX_FILE_SIZE"];
    $file=$_POST["userfile"];
    
    
    /*** Assunto escolhido ***/
    if($tipo_assunto == "1")
    {
        $assunto_escolhido=$assunto_partic;
    }
    else
    {
        if($tipo_assunto == "2")
        {
            $assunto_escolhido=$assunto_disc;

            $num_letras = strlen($assunto_disc);

            for($letra=0; $letra <= $num_letras; $letra++)
            {
                if ($assunto_disc[$letra]=="-")
                {
                    $num_primeiro_simbolo = $letra;
                    break;
                }
            }
            for ($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
            {
                if($assunto_disc[$letra]=="*")
                {
                    $num_segundo_simbolo = $letra;
                    break;
                }
            }
            for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
            {
                if($letra==0)
                {
                    $id_disciplina_assunto=$assunto_disc[$letra];
                }
                else
                {
                    $id_disciplina_assunto=$id_disciplina_assunto.$assunto_disc[$letra];
                }
            }
            for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
            {
                if($letra==$num_primeiro_simbolo + 1)
                {
                    $id_professor_assunto=$assunto_disc[$letra];
                }
                else
                {
                    $id_professor_assunto=$id_professor_assunto.$assunto_disc[$letra];
                }
            }
            for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
            {
                if($letra==$num_segundo_simbolo + 1)
                {
                    $disciplina_assunto=$assunto_disc[$letra];
                }
                else
                {
                    $disciplina_assunto=$disciplina_assunto.$assunto_disc[$letra];
                }
            }
        }
        else
        {
            if($tipo_assunto == "3")
            {
                $assunto_escolhido=$assunto_curs;

                $num_letras = strlen($assunto_curs);

                for($letra=0; $letra <= $num_letras; $letra++)
                {
                    if ($assunto_curs[$letra]=="-")
                    {
                        $num_primeiro_simbolo = $letra;
                        break;
                    }
                }
                for ($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
                {
                    if($assunto_curs[$letra]=="*")
                    {
                        $num_segundo_simbolo = $letra;
                        break;
                    }
                }
                for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
                {
                    if($letra==0)
                    {
                        $id_curso_assunto=$assunto_curs[$letra];
                    }
                    else
                    {
                        $id_curso_assunto=$id_curso_assunto.$assunto_curs[$letra];
                    }
                }
                for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
                {
                    if($letra==$num_primeiro_simbolo + 1)
                    {
                        $id_professor_assunto=$assunto_curs[$letra];
                    }
                    else
                    {
                        $id_professor_assunto=$id_professor_assunto.$assunto_curs[$letra];
                    }
                }
                for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
                {
                    if($letra==$num_segundo_simbolo + 1)
                    {
                        $curso_assunto=$assunto_curs[$letra];
                    }
                    else
                    {
                        $curso_assunto=$curso_assunto.$assunto_curs[$letra];
                    }
                }
            }
            else
            {
                if($tipo_assunto == "4")
                {
                    $assunto_escolhido=$assunto_disccurs;

                    $num_letras = strlen ($assunto_disccurs);

                    for($letra=0; $letra <= $num_letras; $letra++)
                    {
                        if($assunto_disccurs[$letra]=="/")
                        {
                            $num_primeiro_simbolo = $letra;
                            break;
                        }
                    }
                    for($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
                    {
                        if($assunto_disccurs[$letra]=="-")
                        {
                            $num_segundo_simbolo = $letra;
                            break;
                        }
                    }
                    for ($letra=$num_segundo_simbolo+1; $letra <= $num_letras; $letra++)
                    {
                        if($assunto_disccurs[$letra]=="*")
                        {
                            $num_terceiro_simbolo = $letra;
                            break;
                        }
                    }
                    for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
                    {
                        if($letra==0)
                        {
                            $id_disciplina_assunto=$assunto_disccurs[$letra];
                        }
                        else
                        {
                            $id_disciplina_assunto=$id_disciplina_assunto.$assunto_disccurs[$letra];
                        }
                    }
                    for ($letra= $num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
                    {
                        if($letra==$num_primeiro_simbolo+1)
                        {
                            $id_curso_assunto=$assunto_disccurs[$letra];
                        }
                        else
                        {
                            $id_curso_assunto=$id_curso_assunto.$assunto_disccurs[$letra];
                        }
                    }
                    for ($letra= $num_segundo_simbolo + 1; $letra <= $num_terceiro_simbolo - 1; $letra++)
                    {
                        if($letra==$num_segundo_simbolo + 1)
                        {
                            $id_professor_assunto=$assunto_disccurs[$letra];
                        }
                        else
                        {
                            $id_professor_assunto=$id_professor_assunto.$assunto_disccurs[$letra];
                        }
                    }
                    for ($letra=$num_terceiro_simbolo + 1; $letra <= $num_letras; $letra++)
                    {
                        if($letra==$hum_terceiro_simbolo + 1)
                        {
                            $disciplina_curso_assunto=$assunto_disccurs[$letra];
                        }
                        else
                        {
                            $disciplina_curso_assunto=$disciplina_curso_assunto.$assunto_disccurs[$letra];
                        }
                    }
                }
                else
                {
                    if($tipo_assunto == "5")
                    {
                        $assunto_escolhido=$assunto_adapt;
                    }
                }
            }
        }
    }
    
    if($tipo_assunto == "1" || $tipo_assunto == "5")
    {
        if($tipo_evento!=0 && $tipo_assunto!=0 && $assunto_escolhido!=NULL)
        {
            if($tipo_assunto == "1")
            {
                $conn = &ADONewConnection($A_DB_TYPE);
                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                $sql="INSERT INTO evento_agenda (
                id_tipo_evento,
                tipo_evento_outros,
                id_tipo_assunto,
                assunto_evento_id_disc,
                assunto_evento_id_curso,
                assunto_evento_id_professor,
                assunto_evento,
                nome_evento,
                descricao_evento,
                arquivo_upload,
                data_criacao_evento,
                data_atualizacao_evento,
                id_usuario
                ) VALUES (
                '$tipo_evento',
                '$tipoevento_outros',
                '$tipo_assunto',
                'NULL',
                'NULL',
                'NULL',
                '$assunto_partic',
                '$nome_evento',
                '$descricao_evento',
                'NULL',
                NOW(),
                'NULL',
                '$id_usuario')";
                $rs = $conn->Execute($sql);
            }
            else
            {
                if($tipo_assunto == "5")
                {
                    $conn = &ADONewConnection($A_DB_TYPE);
                    $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                    $sql="INSERT INTO evento_agenda (
                    id_tipo_evento,
                    tipo_evento_outros,
                    id_tipo_assunto,
                    assunto_evento_id_disc,
                    assunto_evento_id_curso,
                    assunto_evento_id_professor,
                    assunto_evento,
                    nome_evento,
                    descricao_evento,
                    arquivo_upload,
                    data_criacao_evento,
                    data_atualizacao_evento,
                    id_usuario
                    ) VALUES (
                    '$tipo_evento',
                    '$tipoevento_outros',
                    '$tipo_assunto',
                    'NULL',
                    'NULL',
                    'NULL',
                    '$assunto_adapt',
                    '$nome_evento',
                    '$descricao_evento',
                    'NULL',
                    NOW(),
                    'NULL',
                    '$id_usuario')";
                    $rs = $conn->Execute($sql);
                }
            }
            if ($rs == false)
            {
                echo "<table border=0 width=100%>";
                echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                echo"</table>";
            }
            else
            {
                ?>
                <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=Destinar">
                <?
                //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=Destinar">


                $id_event=mysql_insert_id();
                $destino_evento="0";

                echo"<b>Processando...</b>";

                /*** Espa�o ***/
                echo "<tr><td colspan=2>&nbsp</td></tr>";

                /*** Tabela mostrando o evento cadastrado ***/
                echo "</center><table border=0  cellspacing=1 cellpadding=5>";


                /*** Evento ***/
                echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";


                /*** Espa�o ***/
                //echo "<tr class=zebraA><td colspan=2>&nbsp</td></tr>";


                /*** Tipo do evento ***/
                $sql="select id_tipo_evento
                from evento_agenda
                where evento_agenda.id_evento_agenda='$id_event'";
                $resp=mysql_query($sql);
                $linha = mysql_fetch_row($resp);
                if($linha[0]==1)
                {
                    $sql1="select tipo_evento_outros
                    from evento_agenda
                    where evento_agenda.id_evento_agenda='$id_event'";
                    $resp1=mysql_query($sql1);
                    $linha1 = mysql_fetch_row($resp1);
                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                }
                else
                {
                    $sql="select nome_tipo_evento
                    from evento_agenda, tipo_evento_agenda
                    where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                    and evento_agenda.id_evento_agenda='$id_event'";
                    $resp=mysql_query($sql);
                    $linha = mysql_fetch_row($resp);
                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
                }
                /*** Tipo do assunto ***/
                $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                from evento_agenda, tipo_assunto_agenda
                where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                and evento_agenda.id_evento_agenda='$id_event'";
                $resp=mysql_query($sql);
                $linha1 = mysql_fetch_row($resp);
                echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                //Assunto
                $sql="select assunto_evento
                from evento_agenda
                where evento_agenda.id_evento_agenda='$id_event'";
                $resp=mysql_query($sql);
                $linha = mysql_fetch_row($resp);
                echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";



                /*** Nome do evento ***/
                $sql="select nome_evento
                from evento_agenda
                where evento_agenda.id_evento_agenda='$id_event'";
                $resp=mysql_query($sql);
                $linha = mysql_fetch_row($resp);
                echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";


                /*** Descri��o do evento ***/
                $sql="select descricao_evento
                from evento_agenda
                where evento_agenda.id_evento_agenda='$id_event'";
                $resp=mysql_query($sql);
                $linha = mysql_fetch_row($resp);
                echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                /*** Espa�o ***/
                echo "<tr><td colspan=2>&nbsp</td></tr>";
                
                echo "</table></center>";//Encerra tabela que mostra o agendamento cadastrado
                  
                exit;
            }
        }
        else
        {
            $cont=0;
            if($tipo_evento==0)
            {
                $cont=$cont+1;
            }
            if($tipo_assunto==0)
            {
                $cont=$cont+1;
            }
            if($assunto_escolhido==NULL)
            {
                $cont=$cont+1;
            }
            
            echo "<table border=0 width=100%>";
            echo "<tr><td><img src=\"imagens/erro.gif\"></td>";
            if($cont==1)
            {
                echo "<td>".A_LANG_AGENDA_ERRO_UM_CAMPO."";
                if($tipo_evento==0)
                {
                    echo "".A_LANG_AGENDA_CADASTRO_ERRO_UM_TIPO_EVENTO."";
                }
                if($tipo_assunto==0)
                {
                    echo "".A_LANG_AGENDA_CADASTRO_ERRO_UM_TIPO_ASSUNTO."";
                }
                if($assunto_escolhido==NULL)
                {
                    echo "".A_LANG_AGENDA_CADASTRO_ERRO_UM_ASSUNTO."";
                }
                echo "".A_LANG_AGENDA_ERRO_UM."</td></tr>";
            }
            else
            {
                echo "<td>".A_LANG_AGENDA_ERRO_VARIOS."<br>";
                if($tipo_evento==0)
                {
                    echo "".A_LANG_AGENDA_CADASTRO_ERRO_VARIOS_TIPO_EVENTO."<br>";
                }
                if($tipo_assunto==0)
                {
                    echo "".A_LANG_AGENDA_CADASTRO_ERRO_VARIOS_TIPO_ASSUNTO."<br>";
                }
                if($assunto_escolhido==NULL)
                {
                    echo "".A_LANG_AGENDA_CADASTRO_ERRO_VARIOS_ASSUNTO."<br>";
                }
                echo "</td></tr>";
            }
        }
    }
    else
    {
        if($tipo_assunto == "2" || $tipo_assunto == "3" || $tipo_assunto == "4")
        {
            if($tipo_evento!=0 && $tipo_assunto!=0 && $assunto_escolhido!=0)
            {
                if($tipo_assunto == "2")
                {
                    $conn = &ADONewConnection($A_DB_TYPE);
                    $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                    $sql="INSERT INTO evento_agenda (
                    id_tipo_evento,
                    tipo_evento_outros,
                    id_tipo_assunto,
                    assunto_evento_id_disc,
                    assunto_evento_id_curso,
                    assunto_evento_id_professor,
                    assunto_evento,
                    nome_evento,
                    descricao_evento,
                    arquivo_upload,
                    data_criacao_evento,
                    data_atualizacao_evento,
                    id_usuario
                    ) VALUES (
                    '$tipo_evento',
                    '$tipoevento_outros',
                    '$tipo_assunto',
                    '$id_disciplina_assunto',
                    'NULL',
                    '$id_professor_assunto',
                    '$disciplina_assunto',
                    '$nome_evento',
                    '$descricao_evento',
                    'NULL',
                    NOW(),
                    'NULL',
                    '$id_usuario')";
                    $rs = $conn->Execute($sql);
                
                }
                else
                {
                    if($tipo_assunto == "3")
                    {
                        $conn = &ADONewConnection($A_DB_TYPE);
                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                        $sql="INSERT INTO evento_agenda (
                        id_tipo_evento,
                        tipo_evento_outros,
                        id_tipo_assunto,
                        assunto_evento_id_disc,
                        assunto_evento_id_curso,
                        assunto_evento_id_professor,
                        assunto_evento,
                        nome_evento,
                        descricao_evento,
                        arquivo_upload,
                        data_criacao_evento,
                        data_atualizacao_evento,
                        id_usuario
                        ) VALUES (
                        '$tipo_evento',
                        '$tipoevento_outros',
                        '$tipo_assunto',
                        'NULL',
                        '$id_curso_assunto',
                        '$id_professor_assunto',
                        '$curso_assunto',
                        '$nome_evento',
                        '$descricao_evento',
                        'NULL',
                        NOW(),
                        'NULL',
                        '$id_usuario')";
                        $rs = $conn->Execute($sql);
                    }
                    else
                    {
                        if($tipo_assunto == "4")
                        {
                            $conn = &ADONewConnection($A_DB_TYPE);
                            $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                            $sql="INSERT INTO evento_agenda (
                            id_tipo_evento,
                            tipo_evento_outros,
                            id_tipo_assunto,
                            assunto_evento_id_disc,
                            assunto_evento_id_curso,
                            assunto_evento_id_professor,
                            assunto_evento,
                            nome_evento,
                            descricao_evento,
                            arquivo_upload,
                            data_criacao_evento,
                            data_atualizacao_evento,
                            id_usuario
                            ) VALUES (
                            '$tipo_evento',
                            '$tipoevento_outros',
                            '$tipo_assunto',
                            '$id_disciplina_assunto',
                            '$id_curso_assunto',
                            '$id_professor_assunto',
                            '$disciplina_curso_assunto',
                            '$nome_evento',
                            '$descricao_evento',
                            'NULL',
                            NOW(),
                            'NULL',
                            '$id_usuario')";
                            $rs = $conn->Execute($sql);
                        }
                    }
                }
                if ($rs == false)
                {
                    echo "<table border=0 width=100%>";
                    echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                    echo"</table>";
                }
                else
                {
                    ?>
                    <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=Destinar">
                    <?
                    //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=Destinar">

                    $id_event=mysql_insert_id();
                    $destino_evento="0";

                    echo"<b>Processando...</b>";

                    /*** Espa�o ***/
                    echo "<tr><td colspan=2>&nbsp</td></tr>";

                    /*** Tabela mostrando o evento cadastrado ***/
                    echo "</center><table border=0  cellspacing=1 cellpadding=5>";


                    /*** Evento ***/
                    echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";


                    /*** Espa�o ***/
                    //echo "<tr class=zebraA><td colspan=2>&nbsp</td></tr>";


                    /*** Tipo do evento ***/
                    $sql="select id_tipo_evento
                    from evento_agenda
                    where evento_agenda.id_evento_agenda='$id_event'";
                    $resp=mysql_query($sql);
                    $linha = mysql_fetch_row($resp);
                    if($linha[0]==1)
                    {
                        $sql1="select tipo_evento_outros
                        from evento_agenda
                        where evento_agenda.id_evento_agenda='$id_event'";
                        $resp1=mysql_query($sql1);
                        $linha1 = mysql_fetch_row($resp1);
                        echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                    }
                    else
                    {
                        $sql="select nome_tipo_evento
                        from evento_agenda, tipo_evento_agenda
                        where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                        and evento_agenda.id_evento_agenda='$id_event'";
                        $resp=mysql_query($sql);
                        $linha = mysql_fetch_row($resp);
                        echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
                    }


                    /*** Tipo do assunto ***/
                    $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                    from evento_agenda, tipo_assunto_agenda
                    where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                    and evento_agenda.id_evento_agenda='$id_event'";
                    $resp=mysql_query($sql);
                    $linha1 = mysql_fetch_row($resp);
                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                    //Assunto
                    $sql="select assunto_evento
                    from evento_agenda
                    where evento_agenda.id_evento_agenda='$id_event'";
                    $resp=mysql_query($sql);
                    $linha = mysql_fetch_row($resp);
                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";



                    /*** Nome do evento ***/
                    $sql="select nome_evento
                    from evento_agenda
                    where evento_agenda.id_evento_agenda='$id_event'";
                    $resp=mysql_query($sql);
                    $linha = mysql_fetch_row($resp);
                    echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";


                    /*** Descri��o do evento ***/
                    $sql="select descricao_evento
                    from evento_agenda
                    where evento_agenda.id_evento_agenda='$id_event'";
                    $resp=mysql_query($sql);
                    $linha = mysql_fetch_row($resp);
                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                    /*** Espa�o ***/
                    echo "<tr><td colspan=2>&nbsp</td></tr>";

                    echo "</table></center>";//Encerra tabela que mostra o agendamento cadastrado

                    exit;
                }
            }
            else
            {
                $cont=0;
                if($tipo_evento==0)
                {
                    $cont=$cont+1;
                }
                if($tipo_assunto==0)
                {
                    $cont=$cont+1;
                }
                if($assunto_escolhido==0)
                {
                    $cont=$cont+1;
                }


                echo "<table border=0 width=100%>";
                echo "<tr><td><img src=\"imagens/erro.gif\"></td>";
                if($cont==1)
                {
                    echo "<td>".A_LANG_AGENDA_ERRO_UM_CAMPO."";
                    if($tipo_evento==0)
                    {
                        echo "".A_LANG_AGENDA_CADASTRO_ERRO_UM_TIPO_EVENTO."";
                    }
                    if($tipo_assunto==0)
                    {
                        echo "".A_LANG_AGENDA_CADASTRO_ERRO_UM_TIPO_ASSUNTO."";
                    }
                    if($assunto_escolhido==0)
                    {
                        echo "".A_LANG_AGENDA_CADASTRO_ERRO_UM_ASSUNTO."";
                    }

                    echo "".A_LANG_AGENDA_ERRO_UM."</td></tr>";
                }
                else
                {
                    echo "<td>".A_LANG_AGENDA_ERRO_VARIOS."<br>";
                    if($tipo_evento==0)
                    {
                        echo "".A_LANG_AGENDA_CADASTRO_ERRO_VARIOS_TIPO_EVENTO."<br>";
                    }
                    if($tipo_assunto==0)
                    {
                        echo "".A_LANG_AGENDA_CADASTRO_ERRO_VARIOS_TIPO_ASSUNTO."<br>";
                    }
                    if($assunto_escolhido==0)
                    {
                        echo "".A_LANG_AGENDA_CADASTRO_ERRO_VARIOS_ASSUNTO."<br>";
                    }
                    echo "</td></tr>";
                }
            }
        }
        else
        {
            echo "<table border=0 width=100%>";
            echo "<tr><td><img src=\"imagens/erro.gif\"></td>";
            echo "<td>".A_LANG_AGENDA_ERRO_VARIOS."<br>";
            if($tipo_evento==0)
            {
                echo "".A_LANG_AGENDA_CADASTRO_ERRO_VARIOS_TIPO_EVENTO."<br>";
            }
            if($tipo_assunto==0)
            {
                echo "".A_LANG_AGENDA_CADASTRO_ERRO_VARIOS_TIPO_ASSUNTO."<br>";
            }
            if($assunto_escolhido==0)
            {
                echo "".A_LANG_AGENDA_CADASTRO_ERRO_VARIOS_ASSUNTO."<br>";
            }
            echo "</td></tr>";
            echo"</table>";
        }
    }
}


?>
<!--- Inicia formul�rio --->
<form name="formularioevento" enctype="multipart/form-data" action="n_index_agenda.php?opcao=Evento" method="post">
<?


/*** Tabela do agendamento do evento ***/
echo "<table border=0 width=100%>";



/*** Tipo do evento ***/
$sql="select id_tipo_evento, nome_tipo_evento from tipo_evento_agenda";
$resp=mysql_query($sql);
echo "<tr><td>".A_LANG_AGENDA_TIPO_EVENTO."*:</td>";
echo "<td><select name=tipoevento id=campotipoevento class=select size=1 style='width:223px' onchange=ApareceTipoEventoOutros()>";
echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONA_TIPO_EVENTO."</option>";
while ($array = mysql_fetch_array($resp))
{
    if($tipo_evento==$array[0])
    {
        echo "<option selected value=\"".$array[0]."\">".$array['nome_tipo_evento']."</option>";
    }
    else
    {
        echo "<option value=\"".$array[0]."\">".$array['nome_tipo_evento']."</option>";
    }
}
echo "</select>";
echo "</td>";
echo "</tr>";



/*** Tipo do Evento Outros ***/
echo "<tr><td id=tipooutros valign=top style=display:none colspan=1>".A_LANG_AGENDA_NOME_TIPO_EVENTO_OUTROS.":</td>";



/*** Nome Tipo do Evento Outros ***/
echo "<td colspan=1><input name=tipoeventooutros id=campotipoeventooutros class=text type=text size=70 value=\"".$tipoevento_outros."\" style=display:none></input>";
echo"</td></tr>";



/*** Usu�rio tipo root ***/
if($tipo_usuario == "root")
{
    //Assunto Disciplina
    $sql1="select disciplina.id_disc, disciplina.id_usuario,
    disciplina.nome_disc, usuario.nome_usuario
    from disciplina, usuario
    where disciplina.id_usuario = usuario.id_usuario
    order by nome_disc, nome_usuario";
    $resp1=mysql_query($sql1);
    $linha1=mysql_num_rows($resp1);

    //Assunto Curso
    $sql2="select curso.id_curso, curso.id_usuario,
    curso.nome_curso, usuario.nome_usuario
    from curso, usuario
    where curso.id_usuario = usuario.id_usuario
    order by nome_curso, nome_usuario";
    $resp2=mysql_query($sql2);
    $linha2=mysql_num_rows($resp2);

    //Assunto Disciplina/Curso
    $sql3="select curso_disc.id_disc, curso_disc.id_curso, curso_disc.id_usuario,
    disciplina.nome_disc, curso.nome_curso, usuario.nome_usuario
    from curso_disc, disciplina, curso, usuario
    where curso_disc.id_disc=disciplina.id_disc
    and curso_disc.id_curso=curso.id_curso
    and curso_disc.id_usuario=usuario.id_usuario
    order by nome_disc, nome_curso, nome_usuario";
    $resp3=mysql_query($sql3);
    $linha3=mysql_num_rows($resp3);
}



/*** Usu�rio tipo professor ***/
if($tipo_usuario == "professor")
{
    //Assunto Disciplina (professor)
    $sql1="select disciplina.id_disc, disciplina.id_usuario,
    disciplina.nome_disc, usuario.nome_usuario
    from disciplina, usuario
    where disciplina.id_usuario=usuario.id_usuario
    and disciplina.id_usuario='$id_usuario'
    order by nome_disc, nome_usuario";
    $resp1=mysql_query($sql1);
    $linha1=mysql_num_rows($resp1);

    //Assunto Curso (professor, aluno)
    $sql2="select curso.id_curso, curso.id_usuario,
    curso.nome_curso, usuario.nome_usuario
    from curso, usuario
    where curso.id_usuario=usuario.id_usuario
    and curso.id_usuario='$id_usuario'
    union
    select matricula.id_curso, curso.id_usuario, curso.nome_curso, usuario.nome_usuario
    from curso, usuario, matricula
    where matricula.id_curso=curso.id_curso
    and curso.id_usuario=usuario.id_usuario
    and matricula.id_usuario='$id_usuario'
    and status_mat=1
    order by nome_curso, nome_usuario";
    $resp2=mysql_query($sql2);
    $linha2=mysql_num_rows($resp2);

    //Assunto Disciplina/Curso (professor, aluno)
    $sql3="select curso_disc.id_disc, curso_disc.id_curso, curso_disc.id_usuario,
    disciplina.nome_disc, curso.nome_curso, usuario.nome_usuario
    from curso_disc, usuario, disciplina, curso
    where curso_disc.id_usuario = usuario.id_usuario
    and curso_disc.id_disc = disciplina.id_disc
    and curso_disc.id_curso = curso.id_curso
    and curso_disc.id_usuario = '$id_usuario'
    union
    select curso_disc.id_disc, curso_disc.id_curso, curso_disc.id_usuario,
    disciplina.nome_disc, curso.nome_curso, usuario.nome_usuario
    from curso_disc, disciplina, curso, matricula, usuario
    where curso_disc.id_usuario = usuario.id_usuario
    and curso_disc.id_disc = disciplina.id_disc
    and curso_disc.id_curso = curso.id_curso
    and matricula.id_disc = curso_disc.id_disc
    and matricula.id_curso = curso_disc.id_curso
    and matricula.id_usuario = '$id_usuario'
    and status_mat =1
    order by nome_disc, nome_curso, nome_usuario";
    $resp3=mysql_query($sql3);
    $linha3=mysql_num_rows($resp3);
}



/*** Usu�rio tipo aluno ***/
if($tipo_usuario == "aluno")
{
    //Assunto Curso (aluno)
    $sql2="select matricula.id_curso, curso.id_usuario,
    curso.nome_curso, usuario.nome_usuario
    from curso, usuario, matricula
    where matricula.id_curso=curso.id_curso
    and curso.id_usuario=usuario.id_usuario
    and matricula.id_usuario='$id_usuario'
    and status_mat=1
    order by nome_curso, nome_usuario";
    $resp2=mysql_query($sql2);
    $linha2=mysql_num_rows($resp2);

    //Assunto Disciplina/Curso (aluno)
    $sql3="select curso_disc.id_disc, curso_disc.id_curso, curso_disc.id_usuario,
    disciplina.nome_disc, curso.nome_curso, usuario.nome_usuario
    from curso_disc, disciplina, curso, matricula, usuario
    where curso_disc.id_usuario = usuario.id_usuario
    and curso_disc.id_disc = disciplina.id_disc
    and curso_disc.id_curso = curso.id_curso
    and matricula.id_disc = curso_disc.id_disc
    and matricula.id_curso = curso_disc.id_curso
    and matricula.id_usuario = '$id_usuario'
    and status_mat =1
    order by nome_disc, nome_curso, nome_usuario";
    $resp3=mysql_query($sql3);
    $linha3=mysql_num_rows($resp3);
}



/*** Aparecer o tipo do assunto escolhido mesmo depois que apertar o cadastrar ***/
if(isset($_POST["cadastrar"]))
{
    $tipo_assunto=$_POST["tipoassunto"];
    $aux_seleciona_tipo_assunto="";
    if($tipo_assunto==1) $aux_tipo_assunto_partic="selected";
    if($tipo_assunto==2) $aux_tipo_assunto_disc="selected";
    if($tipo_assunto==3) $aux_tipo_assunto_curso="selected";
    if($tipo_assunto==4) $aux_tipo_assunto_disc_curso="selected";
    if($tipo_assunto==5) $aux_tipo_assunto_adapt="selected";
}
else
{
    $aux_seleciona_tipo_assunto="selected";
    $aux_tipo_assunto_partic="";
    $aux_tipo_assunto_disc="";
    $aux_tipo_assunto_curso="";
    $aux_tipo_assunto_disc_curso="";
    $aux_tipo_assunto_adapt="";
}



/*** Tipo do assunto ***/
echo "<tr><td>".A_LANG_AGENDA_TIPO_ASSUNTO."*:</td>";
echo "<td><select name=tipoassunto id=campotipoassunto class=select size=1 onchange=ApareceAssunto() style='width:223px'>";
    echo "<option $aux_seleciona_tipo_assunto value=\"0\">".A_LANG_AGENDA_SELECIONA_TIPO_ASSUNTO."</option>";
if($tipo_usuario=="aluno")
{
    if($linha2 == 0 && $linha3 == 0)
    {
        echo "<option $aux_tipo_assunto_partic value=\"1\">".A_LANG_AGENDA_PARTICULAR."</option>";
        echo "<option $aux_tipo_assunto_adapt value=\"5\">".A_LANG_AGENDA_AMBIENTE_ADAPTWEB."</option>";
        echo "</select>";
        echo "</td>";
        echo "</tr>";
        echo "".A_LANG_AGENDA_DISCIPLINA_E_CURSO_NAO_CADASTRADO."";
    }
    else
    {
        if($linha2 != 0 && $linha3 != 0)
        {
            echo "<option $aux_tipo_assunto_partic value=\"1\">".A_LANG_AGENDA_PARTICULAR."</option>";
            echo "<option $aux_tipo_assunto_curso value=\"3\">".A_LANG_AGENDA_CURSO."</option>";
            echo "<option $aux_tipo_assunto_disc_curso value=\"4\">".A_LANG_AGENDA_DISCIPLINA_CURSO."</option>";
            echo "<option $aux_tipo_assunto_adapt value=\"5\">".A_LANG_AGENDA_AMBIENTE_ADAPTWEB."</option>";
            echo "</select>";
            echo "</td>";
            echo "</tr>";
        }
    }
}
else
{
    if($linha1 == 0 && $linha2 == 0 && $linha3==0)
    {
        echo "<option $aux_tipo_assunto_partic value=\"1\">".A_LANG_AGENDA_PARTICULAR."</option>";
        echo "<option $aux_tipo_assunto_adapt value=\"5\">".A_LANG_AGENDA_AMBIENTE_ADAPTWEB."</option>";
        echo "</select>";
        echo "</td>";
        echo "</tr>";
        echo "".A_LANG_AGENDA_DISCIPLINA_E_CURSO_NAO_CADASTRADO."";
    }
    else
    {
        if($linha1 != 0 && $linha2==0 && $linha3==0)
        {
            echo "<option $aux_tipo_assunto_partic value=\"1\">".A_LANG_AGENDA_PARTICULAR."</option>";
            echo "<option $aux_tipo_assunto_disc value=\"2\">".A_LANG_AGENDA_DISCIPLINA."</option>";
            echo "<option $aux_tipo_assunto_adapt value=\"5\">".A_LANG_AGENDA_AMBIENTE_ADAPTWEB."</option>";
            echo "</select>";
            echo "</td>";
            echo "</tr>";
            echo "".A_LANG_AGENDA_CURSO_NAO_CADASTRADO."";
        }
        else
        {
            if($linha ==0 && $linha2 !=0 && $linha3==0)
            {
                echo "<option $aux_tipo_assunto_partic value=\"1\">".A_LANG_AGENDA_PARTICULAR."</option>";
                echo "<option $aux_tipo_assunto_curso value=\"3\">".A_LANG_AGENDA_CURSO."</option>";
                echo "<option $aux_tipo_assunto_adapt value=\"5\">".A_LANG_AGENDA_AMBIENTE_ADAPTWEB."</option>";
                echo "</select>";
                echo "</td>";
                echo "</tr>";
                echo "".A_LANG_AGENDA_DISCIPLINA_NAO_CADASTRADA."";
            }
            else
            {
                if($linha !=0 && $linha2 !=0 && $linha3==0)
                {
                    echo "<option $aux_tipo_assunto_partic value=\"1\">".A_LANG_AGENDA_PARTICULAR."</option>";
                    echo "<option $aux_tipo_assunto_disc value=\"2\">".A_LANG_AGENDA_DISCIPLINA."</option>";
                    echo "<option $aux_tipo_assunto_curso value=\"3\">".A_LANG_AGENDA_CURSO."</option>";
                    echo "<option $aux_tipo_assunto_adapt value=\"5\">".A_LANG_AGENDA_AMBIENTE_ADAPTWEB."</option>";
                    echo "</select>";
                    echo "</td>";
                    echo "</tr>";
                    echo "".A_LANG_AGENDA_DISCIPLINA_CURSO_NAO_RELACIONADO."";
                }
                else
                {
                    if($linha1!=0 && $linha2!=0 && $linha3!=0)
                    {
                        echo "<option $aux_tipo_assunto_partic value=\"1\">".A_LANG_AGENDA_PARTICULAR."</option>";
                        echo "<option $aux_tipo_assunto_disc value=\"2\">".A_LANG_AGENDA_DISCIPLINA."</option>";
                        echo "<option $aux_tipo_assunto_curso value=\"3\">".A_LANG_AGENDA_CURSO."</option>";
                        echo "<option $aux_tipo_assunto_disc_curso value=\"4\">".A_LANG_AGENDA_DISCIPLINA_CURSO."</option>";
                        echo "<option $aux_tipo_assunto_adapt value=\"5\">".A_LANG_AGENDA_AMBIENTE_ADAPTWEB."</option>";
                        echo "</select>";
                        echo "</td>";
                        echo "</tr>";
                    }
                }
            }
        }
    }
}



/*** Assunto ***/
echo "<tr><td id=assunto valign=top style=display:none>".A_LANG_AGENDA_ASSUNTO."*:</td><td>";



/*** Assunto Particular ***/
echo "<input name=assuntpartic id=campoassuntopartic class=text type=text size=70 value=\"".$assunto_partic."\" style=display:none></input>";



/*** Assunto Disciplina ***/
echo "<select name=assuntdisc id=campoassuntodisc class=select size=1 style=display:none onchange=ApareceErro()>";
echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONA_ASSUNTO."</option>";
while ($array = mysql_fetch_array($resp1))
{
    if($assunto_disc==$array[0].'-'.$array[1].'*'.$array[2].' - '.$array[3])
    {
        echo "<option selected value=\"".$array[0].'-'.$array[1].'*'.$array[2].' - '.$array[3]."\">".$array['nome_disc']." - ".$array['nome_usuario']."</option>";
    }
    else
    {
        echo "<option value=\"".$array[0].'-'.$array[1].'*'.$array[2].' - '.$array[3]."\">".$array['nome_disc']." - ".$array['nome_usuario']."</option>";
    }
}
echo "</select>";



/*** Assunto Curso ***/
echo "<select name=assuntcurs id=campoassuntocurs class=select size=1 style=display:none>";
echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONA_ASSUNTO."</option>";
while ($array = mysql_fetch_array($resp2))
{
    if($assunto_curs==$array[0].'-'.$array[1].'*'.$array[2].' - '.$array[3])
    {
        echo "<option selected value=\"".$array[0].'-'.$array[1].'*'.$array[2].' - '.$array[3]."\">".$array['nome_curso'].' - '.$array['nome_usuario']."</option>";
    }
    else
    {
        echo "<option value=\"".$array[0].'-'.$array[1].'*'.$array[2].' - '.$array[3]."\">".$array['nome_curso'].' - '.$array['nome_usuario']."</option>";
    }
}
echo "</select>";



/*** Assunto Disciplina/Curso ***/
echo "<select name=assuntdisccurs id=campoassuntodisccurs class=select size=1 style=display:none>";
echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONA_ASSUNTO."</option>";
while ($array = mysql_fetch_array($resp3))
{
    if($assunto_disccurs==$array[0].'/'.$array[1].'-'.$array[2].'*'.$array[3].' / '.$array[4].' - '.$array[5])
    {
        echo "<option selected value=\"".$array[0].'/'.$array[1].'-'.$array[2].'*'.$array[3].' / '.$array[4].' - '.$array[5]."\">".$array['nome_disc'].' / '.$array['nome_curso'].' - '.$array['nome_usuario']."</option>";
    }
    else
    {
        echo "<option value=\"".$array[0].'/'.$array[1].'-'.$array[2].'*'.$array[3].' / '.$array[4].' - '.$array[5]."\">".$array['nome_disc'].' / '.$array['nome_curso'].' - '.$array['nome_usuario']."</option>";
    }
}
echo "</select>";



/*** Assunto Ambiente AdaptWeb ***/
echo "<input name=assuntadapt id=campoassuntoadapt class=text type=text size=70 value=\"".$assunto_adapt."\" style=display:none></input>";



/*** Encerra dado e linha ***/
echo "</td></tr>";



/*** Nome do evento ***/
echo "<tr><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td><input name=nomeevento class=text type=text size=70 value=\"".$nome_evento."\"></td></tr>";



/*** Descri��o do evento ***/
echo "<tr><td>".A_LANG_AGENDA_DESCRICAO.":</td><td><textarea name=descricaoevento class=button cols=70 rows=5>$descricao_evento</textarea></td></tr>";



/*** Arquivo para upload ***/
echo "<tr><td><input name=MAX_FILE_SIZE type=hidden value=5000000 disabled>".A_LANG_AGENDA_ARQUIVO.":</td>&nbsp<td><input name=userfile class=button type=file disabled></td></tr>";




/*** Espa�o ***/
echo "<tr><td colspan=2>&nbsp</td></tr>";



/*** Bot�es cadastrar e cancelar ***/
echo "<tr valign=top><td colspan=2 align=left>";
echo "<input name=cadastrar id=bot1 class=button type=submit value=\"".A_LANG_AGENDA_CADASTRAR."\"></input>";
echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input>";
echo "</td></tr>";



echo "</table>";//Encerra tabela de cadastro
echo "</form>";//Encerra formul�rio

/*** Espa�os ***/
echo "<tr><td colspan=2>&nbsp</td></tr>";



/*** Observa��o ***/
echo"<tr><td colspan=2><b>".A_LANG_AGENDA_OBS_CAMPO_INCOMPLETO."</b></td></tr>";



?>

   </td>
  </table>
  </td>
  </tr>
</table>
