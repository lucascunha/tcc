<?

/* ---------------------------------------------------------------------
 * P�gina de gerenciamento do evento                                   *
 * @author Carina Tissa Aihara <tissa.aihara@gmail.com>                *
 * @version 1.0 <01/11/2009>                                           *
 *                                                                     *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.   *
 * Como Estagio Curricular.                                            *
 * Orientadora: Avanilde Kemczinski                                    *
 * Suporvisora: Edino Lopes Fernandes                                  *
 *                                                                     *
 ----------------------------------------------------------------------*/

//Cria matriz de orelha
$orelha = array();
$orelha = array
(
    array
    (
        "LABEL" => A_LANG_AGENDA_GERENCIAMENTO_EVENTO,
        "LINK" => "index.php",
        "ESTADO" =>"ON"
    )
);

MontaOrelha($orelha);
?>

<!--Monta tabela de fundo-->
<table CELLSPACING="5" CELLPADDING="5" border="0" width="100%" bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> height="80%">
    <tr valign="top">
        <td>
            <br>
                <table border="0"
                    <br>
                        <td valign="top">
                            <td>
                            
<?
/*** Estabelecer uma conex�o com o banco de dados ***/
include "config/configuracoes.php";
$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;
include "include/conecta.php";

/*** Inciciando a session ***/
session_start();
session_register("diaeven");
session_register("meseven");
session_register("anoeven");
session_register("destino_evento");

echo"<center><table border=0 cellspacing=1 cellpadding=5 bgColor=#009ACD width=100% height=100%>";
echo"<tr><center><td bgColor=#009ACD align=center colspan=6><font size=2><b>".A_LANG_AGENDA_EVENTOS."</b></td></center></tr>";
echo"<tr class=zebraB><td align=center>".A_LANG_AGENDA_NUMERO."</td>";
echo"<td align=center>".A_LANG_AGENDA_TIPO_EVENTO."</td>";
echo"<td align=center>".A_LANG_AGENDA_ASSUNTO."</td>";
echo"<td align=center>".A_LANG_AGENDA_NOME_EVENTO."</td>";
echo"<td align=center>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td>";
echo"<td align=center>".A_LANG_AGENDA_NUMERO_AGENDAMENTOS."</td>";
echo"<td align=center>".A_LANG_AGENDA_ACOES."</td>";
echo"</tr>";

$sql="select evento_agenda.id_evento_agenda
from evento_agenda
where evento_agenda.id_usuario='$id_usuario'";
$resp=mysql_query($sql);
$quant = mysql_num_rows($resp);
while($linha = mysql_fetch_array($resp))
{
    $auxiliar_evento[]=$linha[0];
}

/*** Aviso se n�o houver eventos na data fornecida ***/
if($quant==0)
{
    echo"<tr class=zebraA><td colspan=7><center><font color=red>".A_LANG_AGENDA_NENHUM_EVENTO."</td></tr>";
}

for($cont=0; $cont<$quant; $cont++)
{
    if($cont%2==0)
    {
        echo"<tr class=zebraA>";
        
        //Id do Evento
        echo"<td align=center>$auxiliar_evento[$cont]</td>";
        
        //Tipo do Evento
        $sql="select evento_agenda.id_tipo_evento, tipo_evento_agenda.nome_tipo_evento, evento_agenda.tipo_evento_outros
        from evento_agenda, tipo_evento_agenda
        where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
        and evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        if($linha[0]==1)
        {
            echo"<td align=center>$linha[2]</td>";
        }
        else
        {
            echo"<td align=center>$linha[1]</td>";
        }

        //Assunto do Evento
        $sql="select evento_agenda.assunto_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo"<td align=center>$linha[0]</td>";
        
        //Nome do Evento
        $sql="select evento_agenda.nome_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo"<td align=center>$linha[0]</td>";
        
        //Data de Cria��o do Evento
        $sql="select evento_agenda.data_criacao_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo"<td align=center>$linha[0]</td>";
        
        //N�mero de Agendamentos
        $sql="select agendamento_evento_agenda.id_agendamento_evento
        from agendamento_evento_agenda, evento_agenda
        where evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
        and agendamento_evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
        $resp=mysql_query($sql);
        $num=mysql_num_rows($resp);
        echo"<td align=center>$num</td>";
        
        $destino_evento="0";

        $dia_hoje = date ("d");
        $mes_hoje = date ("m");
        $ano_hoje = date ("Y");
        $data_hoje=$ano_hoje."-".$mes_hoje."-".$dia_hoje;
        $aux=0;
        $sql="select distinct agendamento_evento_agenda.data_evento
        from agendamento_evento_agenda
        where agendamento_evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
        $resp=mysql_query($sql);
        $quanti = mysql_num_rows($resp);
        while ($linha = mysql_fetch_array($resp))
        {
            $sql1="select DATEDIFF('$linha[0]','$data_hoje')";
            $resp1=mysql_query($sql1);
            $linha1 = mysql_fetch_row($resp1);
            $respi=$linha1[0];
            if($respi<0)
            {
                $aux=1;
            }
        }
        if($aux<>1)
        {
            echo"<td align=center><input type=image src=\"imagens/visualizar.PNG\" title=\"".A_LANG_AGENDA_VISUALIZAR."\" value=\"".A_LANG_AGENDA_VISUALIZAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=VisualizarEvento&eve=$auxiliar_evento[$cont]';\"></input>
            <input type=image src=\"imagens/editar.PNG\" title=\"".A_LANG_AGENDA_ALTERAR."\" value=\"".A_LANG_AGENDA_ALTERAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AlterarEvento&eve=$auxiliar_evento[$cont]';\"></input>
            <input type=image src=\"imagens/deletar.GIF\" title=\"".A_LANG_AGENDA_EXCLUIR."\" value=\"".A_LANG_AGENDA_EXCLUIR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=ExcluirEvento&eve=$auxiliar_evento[$cont]';\"></input>
            <input type=image src=\"imagens/agendar.PNG\" title=\"".A_LANG_AGENDA_AGENDAMENTO."\" value=\"".A_LANG_AGENDA_AGENDAMENTO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar&eve=$auxiliar_evento[$cont]';\"></input></td>";
            echo"</tr>";
        }
        if($aux==1)
        {
            echo"<td align=center><input type=image src=\"imagens/visualizar.PNG\" title=\"".A_LANG_AGENDA_VISUALIZAR."\" value=\"".A_LANG_AGENDA_VISUALIZAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=VisualizarEvento&eve=$auxiliar_evento[$cont]';\"></input>
            <input type=image src=\"imagens/editar.PNG\" title=\"".A_LANG_AGENDA_ALTERAR."\" value=\"".A_LANG_AGENDA_ALTERAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AlterarEvento&eve=$auxiliar_evento[$cont]';\" disabled></input>
            <input type=image src=\"imagens/deletar.GIF\" title=\"".A_LANG_AGENDA_EXCLUIR."\" value=\"".A_LANG_AGENDA_EXCLUIR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=ExcluirEvento&eve=$auxiliar_evento[$cont]';\" disabled></input>
            <input type=image src=\"imagens/agendar.PNG\" title=\"".A_LANG_AGENDA_AGENDAMENTO."\" value=\"".A_LANG_AGENDA_AGENDAMENTO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar&eve=$auxiliar_evento[$cont]';\"></input></td>";
            echo"</tr>";
        }
    }
    else
    {
        echo"<tr class=zebraB>";
        
        //Id do Evento
        echo"<td align=center>$auxiliar_evento[$cont]</td>";
        
        //Tipo do Evento
        $sql="select evento_agenda.id_tipo_evento, tipo_evento_agenda.nome_tipo_evento, evento_agenda.tipo_evento_outros
        from evento_agenda, tipo_evento_agenda
        where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
        and evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        if($linha[0]==1)
        {
            echo"<td align=center>$linha[2]</td>";
        }
        else
        {
            echo"<td align=center>$linha[1]</td>";
        }
        
        //Assunto do Evento
        $sql="select evento_agenda.assunto_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo"<td align=center>$linha[0]</td>";

        //Nome do Evento
        $sql="select evento_agenda.nome_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo"<td align=center>$linha[0]</td>";
        
        //Data de Cria��o do Evento
        $sql="select evento_agenda.data_criacao_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo"<td align=center>$linha[0]</td>";
        
        //N�meros de Agendamentos
        $sql="select agendamento_evento_agenda.id_agendamento_evento
        from agendamento_evento_agenda, evento_agenda
        where evento_agenda.id_evento_agenda=agendamento_evento_agenda.id_evento_agenda
        and agendamento_evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
        $resp=mysql_query($sql);
        $num=mysql_num_rows($resp);
        echo"<td align=center>$num</td>";
        
        $destino_evento="0";
        
        $dia_hoje = date ("d");
        $mes_hoje = date ("m");
        $ano_hoje = date ("Y");
        $data_hoje=$ano_hoje."-".$mes_hoje."-".$dia_hoje;
        $aux=0;
        $sql="select distinct agendamento_evento_agenda.data_evento
        from agendamento_evento_agenda
        where agendamento_evento_agenda.id_evento_agenda='$auxiliar_evento[$cont]'";
        $resp=mysql_query($sql);
        $quanti = mysql_num_rows($resp);
        while ($linha = mysql_fetch_array($resp))
        {
            $sql1="select DATEDIFF('$linha[0]','$data_hoje')";
            $resp1=mysql_query($sql1);
            $linha1 = mysql_fetch_row($resp1);
            $respi=$linha1[0];
            if($respi<0)
            {
                $aux=1;
            }
        }
        if($aux<>1)
        {
            echo"<td align=center><input type=image src=\"imagens/visualizar.PNG\" title=\"".A_LANG_AGENDA_VISUALIZAR."\" value=\"".A_LANG_AGENDA_VISUALIZAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=VisualizarEvento&eve=$auxiliar_evento[$cont]';\"></input>
            <input type=image src=\"imagens/editar.PNG\" title=\"".A_LANG_AGENDA_ALTERAR."\" value=\"".A_LANG_AGENDA_ALTERAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AlterarEvento&eve=$auxiliar_evento[$cont]';\"></input>
            <input type=image src=\"imagens/deletar.GIF\" title=\"".A_LANG_AGENDA_EXCLUIR."\" value=\"".A_LANG_AGENDA_EXCLUIR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=ExcluirEvento&eve=$auxiliar_evento[$cont]';\"></input>
            <input type=image src=\"imagens/agendar.PNG\" title=\"".A_LANG_AGENDA_AGENDAMENTO."\" value=\"".A_LANG_AGENDA_AGENDAMENTO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar&eve=$auxiliar_evento[$cont]';\"></input></td>";
            echo"</tr>";
        }
        if($aux==1)
        {
            echo"<td align=center><input type=image src=\"imagens/visualizar.PNG\" title=\"".A_LANG_AGENDA_VISUALIZAR."\" value=\"".A_LANG_AGENDA_VISUALIZAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=VisualizarEvento&eve=$auxiliar_evento[$cont]';\"></input>
            <input type=image src=\"imagens/editar.PNG\" title=\"".A_LANG_AGENDA_ALTERAR."\" value=\"".A_LANG_AGENDA_ALTERAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AlterarEvento&eve=$auxiliar_evento[$cont]';\" disabled></input>
            <input type=image src=\"imagens/deletar.GIF\" title=\"".A_LANG_AGENDA_EXCLUIR."\" value=\"".A_LANG_AGENDA_EXCLUIR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=ExcluirEvento&eve=$auxiliar_evento[$cont]';\" disabled></input>
            <input type=image src=\"imagens/agendar.PNG\" title=\"".A_LANG_AGENDA_AGENDAMENTO."\" value=\"".A_LANG_AGENDA_AGENDAMENTO."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar&eve=$auxiliar_evento[$cont]';\"></input></td>";
            echo"</tr>";
        }
    }
}
echo"</table></center>";


?>


    </td>
   </td>
  </table>
  </td>
  </tr>
</table>
