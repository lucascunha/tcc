<?

/* ---------------------------------------------------------------------
 * P�gina destina do agendamento                                       *
 * @author Carina Tissa Aihara <tissa.aihara@gmail.com>                *
 * @version 1.0 <01/11/2009>                                           *
 *                                                                     *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.   *
 * Como Estagio Curricular.                                            *
 * Orientadora: Avanilde Kemczinski                                    *
 * Suporvisora: Edino Lopes Fernandes                                  *
 *                                                                     *
 ----------------------------------------------------------------------*/
 
?>

<?php include ('_caminho_agenda.php'); ?>

<script language="javascript">
function ApareceTipoGrupoDestinatario()
{
    var indexSelect = document.getElementById("campodestinoevento");
    if (indexSelect.value == "0")
    {
        document.getElementById("titulotipogrupodest").style.display = "none";
        document.getElementById("campotipogrupodestinatario").style.display = "none";
        document.getElementById("grupodestinatario").style.display = "none";
        document.getElementById("campogrupodisc").style.display = "none";
	    document.getElementById("campogrupocurs").style.display = "none";
	    document.getElementById("campogrupodisccurs").style.display = "none";
	    document.getElementById("campogruporoot").style.display = "none";
        document.getElementById("campogrupoalu").style.display = "none";
	    document.getElementById("campogrupoprof").style.display = "none";
	    document.getElementById("campogrupotod").style.display = "none";
	    document.getElementById("campogrupoinst").style.display = "none";
    }
    if (indexSelect.value == "Minha Agenda")
    {
        document.getElementById("titulotipogrupodest").style.display = "none";
        document.getElementById("campotipogrupodestinatario").style.display = "none";
        document.getElementById("grupodestinatario").style.display = "none";
        document.getElementById("campogrupodisc").style.display = "none";
	    document.getElementById("campogrupocurs").style.display = "none";
	    document.getElementById("campogrupodisccurs").style.display = "none";
	    document.getElementById("campogruporoot").style.display = "none";
        document.getElementById("campogrupoalu").style.display = "none";
	    document.getElementById("campogrupoprof").style.display = "none";
	    document.getElementById("campogrupotod").style.display = "none";
	    document.getElementById("campogrupoinst").style.display = "none";
    }
    if (indexSelect.value == "Grupo de Usu�rios")
    {
        document.getElementById("titulotipogrupodest").style.display = "block";
        document.getElementById("campotipogrupodestinatario").style.display = "block";
    }
}

function ApareceGrupoDestinatarioRoot()
{
    var indexSelect = document.getElementById("campotipogrupodestinatario");
    if (indexSelect.value == "0")
    {
        document.getElementById("grupodestinatario").style.display = "none";
        document.getElementById("campogrupodisc").style.display = "none";
	    document.getElementById("campogrupocurs").style.display = "none";
	    document.getElementById("campogrupodisccurs").style.display = "none";
        document.getElementById("campogrupoalu").style.display = "none";
	    document.getElementById("campogrupoprof").style.display = "none";
	    document.getElementById("campogrupotod").style.display = "none";
	    document.getElementById("campogrupoinst").style.display = "none";
    }
    if (indexSelect.value == "1")
    {
        document.getElementById("grupodestinatario").style.display = "block";
        document.getElementById("campogrupodisc").style.display = "block";
	    document.getElementById("campogrupocurs").style.display = "none";
	    document.getElementById("campogrupodisccurs").style.display = "none";
        document.getElementById("campogrupoalu").style.display = "none";
	    document.getElementById("campogrupoprof").style.display = "none";
	    document.getElementById("campogrupotod").style.display = "none";
	    document.getElementById("campogrupoinst").style.display = "none";
    }
    if (indexSelect.value == "2")
    {
        document.getElementById("grupodestinatario").style.display = "block";
        document.getElementById("campogrupodisc").style.display = "none";
	    document.getElementById("campogrupocurs").style.display = "block";
	    document.getElementById("campogrupodisccurs").style.display = "none";
        document.getElementById("campogrupoalu").style.display = "none";
	    document.getElementById("campogrupoprof").style.display = "none";
	    document.getElementById("campogrupotod").style.display = "none";
	    document.getElementById("campogrupoinst").style.display = "none";
    }
    if (indexSelect.value == "3")
    {
        document.getElementById("grupodestinatario").style.display = "block";
        document.getElementById("campogrupodisc").style.display = "none";
	    document.getElementById("campogrupocurs").style.display = "none";
	    document.getElementById("campogrupodisccurs").style.display = "block";
        document.getElementById("campogrupoalu").style.display = "none";
	    document.getElementById("campogrupoprof").style.display = "none";
	    document.getElementById("campogrupotod").style.display = "none";
	    document.getElementById("campogrupoinst").style.display = "none";
    }
    if (indexSelect.value == "5")
    {
        document.getElementById("grupodestinatario").style.display = "block";
        document.getElementById("campogrupodisc").style.display = "none";
	    document.getElementById("campogrupocurs").style.display = "none";
	    document.getElementById("campogrupodisccurs").style.display = "none";
	    document.getElementById("campogrupoalu").style.display = "block";
	    document.getElementById("campogrupoprof").style.display = "none";
	    document.getElementById("campogrupotod").style.display = "none";
	    document.getElementById("campogrupoinst").style.display = "none";
    }
    if (indexSelect.value == "6")
    {
        document.getElementById("grupodestinatario").style.display = "block";
        document.getElementById("campogrupodisc").style.display = "none";
	    document.getElementById("campogrupocurs").style.display = "none";
	    document.getElementById("campogrupodisccurs").style.display = "none";
	    document.getElementById("campogrupoalu").style.display = "none";
	    document.getElementById("campogrupoprof").style.display = "block";
	    document.getElementById("campogrupotod").style.display = "none";
	    document.getElementById("campogrupoinst").style.display = "none";
    }
    if (indexSelect.value == "7")
    {
        document.getElementById("grupodestinatario").style.display = "block";
        document.getElementById("campogrupodisc").style.display = "none";
	    document.getElementById("campogrupocurs").style.display = "none";
	    document.getElementById("campogrupodisccurs").style.display = "none";
	    document.getElementById("campogrupoalu").style.display = "none";
	    document.getElementById("campogrupoprof").style.display = "none";
	    document.getElementById("campogrupotod").style.display = "block";
	    document.getElementById("campogrupoinst").style.display = "none";
    }
    if (indexSelect.value == "8")
    {
        document.getElementById("grupodestinatario").style.display = "block";
        document.getElementById("campogrupodisc").style.display = "none";
	    document.getElementById("campogrupocurs").style.display = "none";
	    document.getElementById("campogrupodisccurs").style.display = "none";
	    document.getElementById("campogrupoalu").style.display = "none";
	    document.getElementById("campogrupoprof").style.display = "none";
	    document.getElementById("campogrupotod").style.display = "none";
	    document.getElementById("campogrupoinst").style.display = "block";
    }
}

function ApareceGrupoDestinatarioProfessor()
{
    var indexSelect = document.getElementById("campotipogrupodestinatario");
    if (indexSelect.value == "0")
    {
        document.getElementById("grupodestinatario").style.display = "none";
        document.getElementById("campogrupodisc").style.display = "none";
	    document.getElementById("campogrupocurs").style.display = "none";
	    document.getElementById("campogrupodisccurs").style.display = "none";
	    document.getElementById("campogruporoot").style.display = "none";
    }
    if (indexSelect.value == "1")
    {
        document.getElementById("grupodestinatario").style.display = "block";
        document.getElementById("campogrupodisc").style.display = "block";
	    document.getElementById("campogrupocurs").style.display = "none";
	    document.getElementById("campogrupodisccurs").style.display = "none";
	    document.getElementById("campogruporoot").style.display = "none";
    }
    if (indexSelect.value == "2")
    {
        document.getElementById("grupodestinatario").style.display = "block";
        document.getElementById("campogrupodisc").style.display = "none";
	    document.getElementById("campogrupocurs").style.display = "block";
	    document.getElementById("campogrupodisccurs").style.display = "none";
	    document.getElementById("campogruporoot").style.display = "none";
    }
    if (indexSelect.value == "3")
    {
        document.getElementById("grupodestinatario").style.display = "block";
        document.getElementById("campogrupodisc").style.display = "none";
	    document.getElementById("campogrupocurs").style.display = "none";
	    document.getElementById("campogrupodisccurs").style.display = "block";
	    document.getElementById("campogruporoot").style.display = "none";
    }
    if (indexSelect.value == "4")
    {
        document.getElementById("grupodestinatario").style.display = "block";
        document.getElementById("campogrupodisc").style.display = "none";
	    document.getElementById("campogrupocurs").style.display = "none";
	    document.getElementById("campogrupodisccurs").style.display = "none";
	    document.getElementById("campogruporoot").style.display = "block";
    }
}

function ApareceGrupoDestinatarioAluno()
{
    var indexSelect = document.getElementById("campotipogrupodestinatario");
    if (indexSelect.value == "0")
    {
        document.getElementById("grupodestinatario").style.display = "none";
	    document.getElementById("campogrupodisccurs").style.display = "none";
	    document.getElementById("campogruporoot").style.display = "none";
    }
    if (indexSelect.value == "3")
    {
        document.getElementById("grupodestinatario").style.display = "block";
	    document.getElementById("campogrupodisccurs").style.display = "block";
	    document.getElementById("campogruporoot").style.display = "none";
    }
    if (indexSelect.value == "4")
    {
        document.getElementById("grupodestinatario").style.display = "block";
	    document.getElementById("campogrupodisccurs").style.display = "none";
	    document.getElementById("campogruporoot").style.display = "block";
    }

}
</script>


<script language="javascript">
function ApareceTipoGrupoDestinatario()
{
    document.formulariotipogrupo.submit()
}
</script>

<script language="javascript">
function ApareceDestinatario()
{
    document.formulariodestinatario.submit()
}
</script>



<script language="javascript" type="text/javascript" src="agenda/datetimepicker.js"></script>

<?
if($tipo_usuario=="root")
{
    ?>
    <!--- Aparecer assunto depois de apertado cadastrar --->
    <body onload="ApareceGrupoDestinatarioRoot();">
    <?
}
if($tipo_usuario=="professor")
{
    ?>
    <!--- Aparecer assunto depois de apertado cadastrar --->
    <body onload="ApareceGrupoDestinatarioProfessor();">
    <?
}
if($tipo_usuario=="aluno")
{
    ?>
    <!--- Aparecer assunto depois de apertado cadastrar --->
    <body onload="ApareceGrupoDestinatarioAluno();">
    <?
}


/*** Cria matriz de orelha ***/
$orelha = array();
$orelha = array
(
    array
    (
        "LABEL" => A_LANG_AGENDA_AGENDAMENTO,
        "LINK" => "index.php",
        "ESTADO" =>"ON"
    )
);

MontaOrelha($orelha);


?>
<!--- Monta tabela de fundo --->
<table CELLSPACING="0" CELLPADDING="0" border="0" width="100%" bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> height="80%">
    <tr valign="top">
        <td>
            <br>
              <table border="0">
                 <br>
                   <td valign="top">


<?

/*** Inciciando a session ***/
session_start();
session_register("id_event");
session_register("idagendamento");
session_register("destino_evento");


/*** Estabelecer uma conex�o com o banco de dados ***/
include "config/configuracoes.php";
$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;
include "include/conecta.php";

if (isset ($_POST["agendarminhaagenda"]))
{
    $local_evento=$_POST["localevento"];
    $data_evento=$_POST["dataevento"];
    $hora_inicio=$_POST["horainicio"];
    $hora_fim=$_POST["horafim"];
    
    /*** Formatar a data ***/
    $num_letras = strlen($data_evento);

    $letra=0;
    do
    {
        if ($data_evento[$letra] == "-")
        {
            $num_primeiro_simbolo = $letra;
        }
        $letra=$letra+1;
    }while($letra<=2);

    for ($letra=$num_primeiro_simbolo+1; $letra <= $num_letras; $letra++)
    {
        if ($data_evento[$letra]=="-")
        {
            $num_segundo_simbolo = $letra;
        }
    }
    for ($letra=0; $letra <= $num_primeiro_simbolo - 1; $letra++)
    {
        if($letra==0)
        {
            $dia_data=$data_evento[$letra];
        }
        else
        {
            $dia_data=$dia_data.$data_evento[$letra];
        }
    }
    for ($letra=$num_primeiro_simbolo + 1; $letra <= $num_segundo_simbolo - 1; $letra++)
    {
        if($letra==$num_primeiro_simbolo + 1)
        {
            $mes_data=$data_evento[$letra];
        }
        else
        {
            $mes_data=$mes_data.$data_evento[$letra];
        }
    }
    for ($letra=$num_segundo_simbolo + 1; $letra <= $num_letras; $letra++)
    {
        if($letra==$num_segundo_simbolo + 1)
        {
            $ano_data=$data_evento[$letra];
        }
        else
        {
            $ano_data=$ano_data.$data_evento[$letra];
        }
    }
    $data_formatada=$ano_data."-".$mes_data."-".$dia_data;
    
    
    
    if($data_evento!=NULL && $hora_inicio!=NULL && $hora_fim!=NULL)
    {
    
        if(checkdate($mes_data, $dia_data, $ano_data))
        {
            $quant_i= strlen($hora_inicio);
            $quant_f= strlen($hora_fim);
            if($quant_i==5 && $quant_f==5)
            {
                $aux=0;
                list($hora_i,$minuto_i) = explode(':',$hora_inicio);
                list($hora_f,$minuto_f) = explode(':',$hora_fim);

                for($cont=0;$cont<2;$cont++)
                {
                    if($hora_i[$cont]!="0" && $hora_i[$cont]!="1" && $hora_i[$cont]!="2" && $hora_i[$cont]!="3" && $hora_i[$cont]!="4" && $hora_i[$cont]!="5" && $hora_i[$cont]!="6" && $hora_i[$cont]!="7" && $hora_i[$cont]!="8" && $hora_i[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($hora_f[$cont]!="0" && $hora_f[$cont]!="1" && $hora_f[$cont]!="2" && $hora_f[$cont]!="3" && $hora_f[$cont]!="4" && $hora_f[$cont]!="5" && $hora_f[$cont]!="6" && $hora_f[$cont]!="7" && $hora_f[$cont]!="8" && $hora_f[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($minuto_i[$cont]!="0" && $minuto_i[$cont]!="1" && $minuto_i[$cont]!="2" && $minuto_i[$cont]!="3" && $minuto_i[$cont]!="4" && $minuto_i[$cont]!="5" && $minuto_i[$cont]!="6" && $minuto_i[$cont]!="7" && $minuto_i[$cont]!="8" && $minuto_i[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                    if($minuto_f[$cont]!="0" && $minuto_f[$cont]!="1" && $minuto_f[$cont]!="2" && $minuto_f[$cont]!="3" && $minuto_f[$cont]!="4" && $minuto_f[$cont]!="5" && $minuto_f[$cont]!="6" && $minuto_f[$cont]!="7" && $minuto_f[$cont]!="8" && $minuto_f[$cont]!="9")
                    {
                        $aux=1;
                        break;
                    }
                }
                if($aux==0)
                {
                    $sql="select destinatario_agendamento_agenda.id_agendamento_evento
                    from agendamento_evento_agenda, destinatario_agendamento_agenda
                    where agendamento_evento_agenda.id_agendamento_evento=destinatario_agendamento_agenda.id_agendamento_evento
                    and agendamento_evento_agenda.id_evento_agenda='$id_event'
                    and agendamento_evento_agenda.data_evento='$data_formatada'
                    and agendamento_evento_agenda.hora_inicio_evento='$hora_inicio'
                    and agendamento_evento_agenda.hora_fim_evento='$hora_fim'
                    and destinatario_agendamento_agenda.grupo_destinatario='Minha Agenda'
                    and destinatario_agendamento_agenda.id_usuario='$id_usuario'";
                    $resp=mysql_query($sql);
                    $linha = mysql_num_rows($resp);
                    if ($linha>0)
                    {
                        echo "<table border=0 width=100%>";
                        echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_EXISTE_AGENDAMENTO_IGUAL."</td></tr>";
                        echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\"></input>";
                        echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                        echo "</table>";
                        exit;
                    }
                    else
                    {
                        $dia_hoje = date ("d");
                        $mes_hoje = date ("m");
                        $ano_hoje = date ("Y");
                        $data_hoje_formatada=$ano_hoje."-".$mes_hoje."-".$dia_hoje;

                        $dataescolhida=strtotime($data_formatada);
                        $datadehoje=strtotime($data_hoje_formatada);

                        if($dataescolhida < $datadehoje)
                        {
                            echo "<table border=0 width=100%>";
                            echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_ERRO_DATA."</td></tr>";
                            echo "<tr><td colspan=2>&nbsp</td></tr>";
                            echo"</table>";
                        }
                        else
                        {
                            list($hora_i,$minuto_i) = explode(':',$hora_inicio);
                            if ($hora_i < 0 || $hora_i > 23 || $minuto_i < 0 || $minuto_i > 59)
                            {
                                echo "<table border=0 width=100%>";
                                echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_ERRO_HORARIO_INICIO."</td></tr>";
                                echo "<tr><td colspan=2>&nbsp</td></tr>";
                                echo"</table>";
                            }
                            else
                            {
                                list($hora_f,$minuto_f) = explode(':',$hora_fim);

                                if ($hora_f < 0 || $hora_f > 23 || $minuto_f < 0 || $minuto_f > 59)
                                {
                                    echo "<table border=0 width=100%>";
                                    echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_ERRO_HORARIO_FIM."</td></tr>";
                                    echo "<tr><td colspan=2>&nbsp</td></tr>";
                                    echo"</table>";
                                }
                                else
                                {
                                    $horai_timestamp=mktime($hora_i,$minuto_i,00);
                                    $horaf_timestamp=mktime($hora_f,$minuto_f,00);

                                    if($horaf_timestamp < $horai_timestamp)
                                    {
                                        echo "<table border=0 width=100%>";
                                        echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_ERRO_HORARIO."</td></tr>";
                                        echo "<tr><td colspan=2>&nbsp</td></tr>";
                                        echo"</table>";
                                    }
                                    else
                                    {
                                        $conn = &ADONewConnection($A_DB_TYPE);
                                        $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                        $sql="INSERT INTO agendamento_evento_agenda (
                                        id_evento_agenda,
                                        data_evento,
                                        hora_inicio_evento,
                                        hora_fim_evento,
                                        local_evento,
                                        data_atualizacao_agendamento
                                        ) VALUES (
                                        '$id_event',
                                        '$data_formatada',
                                        '$hora_inicio',
                                        '$hora_fim',
                                        '$local_evento',
                                        'NULL')";
                                        $rs = $conn->Execute($sql);
                                        if ($rs == false)
                                        {
                                            echo "<table border=0 width=100%>";
                                            echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                            echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\"></input>";
                                            echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                            echo"</table>";
                                            exit;
                                        }
                                        else
                                        {
                                            $idagendamento=mysql_insert_id();
                                            $conn = &ADONewConnection($A_DB_TYPE);
                                            $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                            $sql="INSERT INTO destinatario_agendamento_agenda (
                                            id_agendamento_evento,
                                            id_usuario,
                                            grupo_destinatario,
                                            destino_evento,
                                            id_tipo_grupo_destinatario,
                                            grupo_destinatario_id_disc,
                                            grupo_destinatario_id_curso,
                                            grupo_destinatario_id_professor,
                                            data_envio
                                            ) VALUES (
                                            '$idagendamento',
                                            '$id_usuario',
                                            'Minha Agenda',
                                            '$destino_evento',
                                            'NULL',
                                            'NULL',
                                            'NULL',
                                            'NULL',
                                            NOW())";
                                            $rs = $conn->Execute($sql);
                                            if ($rs == false)
                                            {
                                                $conn = &ADONewConnection($A_DB_TYPE);
                                                $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
                                                $sql2="delete
                                                from agendamento_evento_agenda
                                                where agendamento_evento_agenda.id_agendamento_evento='$idagendamento'";
                                                $rs2 = $conn->Execute($sql2);
                                                if($rs2 == false)
                                                {
                                                    echo "<table border=0 width=100%>";
                                                    echo "<tr><td><img src=\"imagens/erro.gif\"></td><td>".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                    echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\"></input>";
                                                    echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                    echo"</table>";
                                                    exit;
                                                }
                                                else
                                                {
                                                    echo "<table border=0 width=100%>";
                                                    echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_ERRO_ENVIO_DADOS."</td></tr>";
                                                    echo "<tr><td colspan=2 align=center><input class=button type=button value=\"".A_LANG_AGENDA_VOLTAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=Destinar';\"></input>";
                                                    echo "&nbsp<input id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input></td></tr>";
                                                    echo"</table>";
                                                    exit;
                                                }
                                            }
                                            else
                                            {
                                                ?>
                                                <meta http-equiv="Refresh" content="0.0001;URL=http://<?php echo $caminho_agenda; ?>/n_index_agenda.php?opcao=AgendamentoCadastrado">
                                                <?
                                                //<meta http-equiv="Refresh" content="0.0001;URL=http://localhost/adaptweb2/n_index_agenda.php?opcao=AgendamentoCadastrado">


                                                echo"<b>Processando...</b>";

                                                /*** Espa�o ***/
                                                echo "<tr><td colspan=2>&nbsp</td></tr>";
                   
                                                /*** Tabela mostrando o evento cadastrado ***/
                                                echo "<center><table border=0 cellspacing=1 cellpadding=5 align=center>";

                                                /*** Evento ***/
                                                echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

                                                /*** Espa�o ***/
                                                //echo "<tr class=zebraA><td colspan=2>&nbsp</td></tr>";


                                                /*** Tipo do evento ***/
                                                $sql="select id_tipo_evento
                                                from evento_agenda
                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                $resp=mysql_query($sql);
                                                $linha = mysql_fetch_row($resp);
                                                if($linha[0]==1)
                                                {
                                                    $sql1="select tipo_evento_outros
                                                    from evento_agenda
                                                    where evento_agenda.id_evento_agenda='$id_event'";
                                                    $resp1=mysql_query($sql1);
                                                    $linha1 = mysql_fetch_row($resp1);
                                                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
                                                }
                                                else
                                                {
                                                    $sql="select nome_tipo_evento
                                                    from evento_agenda, tipo_evento_agenda
                                                    where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
                                                    and evento_agenda.id_evento_agenda='$id_event'";
                                                    $resp=mysql_query($sql);
                                                    $linha = mysql_fetch_row($resp);
                                                    echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
                                                }

                                                /*** Tipo do assunto ***/
                                                $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
                                                from evento_agenda, tipo_assunto_agenda
                                                where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
                                                and evento_agenda.id_evento_agenda='$id_event'";
                                                $resp=mysql_query($sql);
                                                $linha1 = mysql_fetch_row($resp);
                                                echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

                                                //Assunto
                                                $sql="select assunto_evento
                                                from evento_agenda
                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                $resp=mysql_query($sql);
                                                $linha = mysql_fetch_row($resp);
                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";



                                                /*** Nome do evento ***/
                                                $sql="select nome_evento
                                                from evento_agenda
                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                $resp=mysql_query($sql);
                                                $linha = mysql_fetch_row($resp);
                                                echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";


                                                /*** Descri��o do evento ***/
                                                $sql="select descricao_evento
                                                from evento_agenda
                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                $resp=mysql_query($sql);
                                                $linha = mysql_fetch_row($resp);
                                                echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

                                                /*** Data de cria��o do evento ***/
                                                $sql="select data_criacao_evento
                                                from evento_agenda
                                                where evento_agenda.id_evento_agenda='$id_event'";
                                                $resp=mysql_query($sql);
                                                $linha = mysql_fetch_row($resp);
                                                echo "<tr class=zebraB><td>".A_LANG_AGENDA_DATA_CRIACAO_EVENTO."</td><td>$linha[0]</td></tr>";

                                                /*** Espa�o ***/
                                                echo "<tr><td colspan=2>&nbsp</td></tr>";

                                                echo"</table></center>";
                   
                                                exit;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    echo "<table border=0 width=100%>";
                    echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_HORA_INVALIDO."</td></tr>";
                    echo "</table>";
                }
            }
            else
            {
                echo "<table border=0 width=100%>";
                echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_HORA_HH_MM."</td></tr>";
                echo "</table>";
            }
        }
        else
        {
            echo "<table border=0 width=100%>";
            echo "<tr><td><img src=\"imagens/erro.gif\"> ".A_LANG_AGENDA_DATA_INVALIDA."</td></tr>";
            echo "</table>";
        }
    }
    else
    {
        $cont=0;
        if($data_evento==NULL)
        {
            $cont=$cont+1;
        }
        if($hora_inicio==NULL)
        {
            $cont=$cont+1;
        }
        if($hora_fim==NULL)
        {
            $cont=$cont+1;
        }

        echo "<table border=0 width=100%>";
        echo "<tr><td><img src=\"imagens/erro.gif\"></td>";
        if($cont==1)
        {
            echo "<td>".A_LANG_AGENDA_ERRO_UM_CAMPO."";
            if($data_evento==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_DATA_EVENTO."";
            }
            if($hora_inicio==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_HORA_INICIO."";
            }
            if($hora_fim==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_HORA_FIM."";
            }
            echo "".A_LANG_AGENDA_ERRO_UM."</td></tr>";
        }
        else
        {
            echo "<td>".A_LANG_AGENDA_ERRO_VARIOS."<br>";
            if($data_evento==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_DATA_EVENTO."<br>";
            }
            if($hora_inicio==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_HORA_INICIO."<br>";
            }
            if($hora_fim==NULL)
            {
                echo "".A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_HORA_FIM."<br>";
            }
            echo "</td></tr>";
        }
        echo "<tr><td colspan=2>&nbsp</td></tr>";
    }
}
  
  
  

if (isset ($_POST["destinoevento"]))
{
    $destino_evento=$_POST["destinoevento"];
}

if (isset ($_POST["destinoevento"]) || isset($destino_evento))
{
    if(isset($eve))
    {
        $id_event=$eve;
    }
    if($destino_evento=="Minha Agenda")
    {
    
        /*** Tabela mostrando o evento cadastrado ***/
        echo "<center><table border=0 cellspacing=1 cellpadding=5>";

        /*** Evento ***/
        echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

        /*** Espa�o ***/
        //echo "<tr class=zebraA><td colspan=2>&nbsp</td></tr>";


        /*** Tipo do evento ***/
        $sql="select id_tipo_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        if($linha[0]==1)
        {
            $sql1="select tipo_evento_outros
            from evento_agenda
            where evento_agenda.id_evento_agenda='$id_event'";
            $resp1=mysql_query($sql1);
            $linha1 = mysql_fetch_row($resp1);
            echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
        }
        else
        {
            $sql="select nome_tipo_evento
            from evento_agenda, tipo_evento_agenda
            where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
            and evento_agenda.id_evento_agenda='$id_event'";
            $resp=mysql_query($sql);
            $linha = mysql_fetch_row($resp);
            echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
        }

        /*** Tipo do assunto ***/
        $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
        from evento_agenda, tipo_assunto_agenda
        where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
        and evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha1 = mysql_fetch_row($resp);
        echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

        //Assunto
        $sql="select assunto_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";



        /*** Nome do evento ***/
        $sql="select nome_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";


        /*** Descri��o do evento ***/
        $sql="select descricao_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";
        
        /*** Espa�o ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";
        
        echo"</table></center>";
        
        echo"<center><table>";
        /*** Espa�o ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";
        /*** Minha Agenda ***/
        echo "<tr><td colspan=2><b>".A_LANG_AGENDA_MINHA_AGENDA."</b></td></tr>";
        
        echo"</table></center>";
        
        ?>
        <!--- Inicia formul�rio --->
        <form name="formularioagendamento" action="n_index_agenda.php?opcao=Destinar" method="post">
        <?
    
        /*** Tabela do agendamento do evento ***/
        echo "<table border=0 width=100%>";
        
        
        /*** Data do evento ***/
        echo "<tr><td>".A_LANG_AGENDA_DATA_EVENTO."*:</td>";
        echo "<td><input class=button size=9 name=dataevento id=demo1 value=\"".$data_evento."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\">";

        ?>
        <script>
        function horaConta(c)
        {
            if(c.value.length==2)
            {
                c.value += ':';
            }
        }
        </script>
        <?



        /*** Hora in�cio ***/
        echo"<tr><td>".A_LANG_AGENDA_HORA_INICIO."*:</td><td><input class=text type=text size=3 name=horainicio onkeyup=horaConta(this); value=\"".$hora_inicio."\"></td></tr>";

        /*** Hora fim ***/
        echo "<tr><td>".A_LANG_AGENDA_HORA_FIM."*:</td><td><input class=text type=text size=3 name=horafim onkeyup=horaConta(this); value=\"".$hora_fim."\">";
        
        /*** Local do evento ***/
        echo "<tr><td>".A_LANG_AGENDA_LOCAL_EVENTO.":</td><td><textarea name=localevento class=button cols=70 rows=3>$local_evento</textarea></td></tr>";
        
        /*** Repeti��o ***/
        echo "<tr><td disabled>".A_LANG_AGENDA_REPETICAO_EVENTO.":</td>";
        echo"<td><input type=radio name=naorepetir value='naorepetir' disabled> ".A_LANG_AGENDA_NAO_REPETIR."";
        echo"<input type=radio name=diariamente value='diariamente' disabled> ".A_LANG_AGENDA_DIARIAMENTE."";
        echo"<input type=radio name=semanalmente value='semanalmente' disabled> ".A_LANG_AGENDA_SEMANALMENTE."";
        echo"<input type=radio name=mensalmente value='mensalmente' disabled> ".A_LANG_AGENDA_MENSALMENTE."";
        echo"<input type=radio name=Anualmente value='anualmente' disabled> ".A_LANG_AGENDA_ANUALMENTE."";
        echo"</input> ";
        echo"<select name=quantidadevezes class=select size=1 disabled>";
        echo "<option selected value=\"0\">".A_LANG_AGENDA_QUANTIDADE_REPETICOES."</option>";
        echo "<option value=\"1\">".A_LANG_AGENDA_1X."</option>";
        echo "<option value=\"2\">".A_LANG_AGENDA_2X."</option>";
        echo "<option value=\"3\">".A_LANG_AGENDA_3X."</option>";
        echo "<option value=\"4\">".A_LANG_AGENDA_4X."</option>";
        echo "<option value=\"5\">".A_LANG_AGENDA_5X."</option>";
        echo "<option value=\"6\">".A_LANG_AGENDA_6X."</option>";
        echo "<option value=\"7\">".A_LANG_AGENDA_7X."</option>";
        echo "<option value=\"8\">".A_LANG_AGENDA_8X."</option>";
        echo "<option value=\"9\">".A_LANG_AGENDA_9X."</option>";
        echo "<option value=\"10\">".A_LANG_AGENDA_10X."</option>";
        echo"</select> ";
        echo"".A_LANG_AGENDA_OU."</td></tr>";
        
        echo"<tr><td></td><td disabled>".A_LANG_AGENDA_DATA_REPETI��O.": <input class=button size=9 name=datarepeticao id=demo1 value=\"".$data_repeticao."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\" disabled></td></tr>";
        
        /*** E-mail de Aviso ***/
        echo "<tr><td disabled>".A_LANG_AGENDA_AVISO_EMAIL.":</td>";
        echo"<td><input type=radio name=naoenviar value='naoenviar' disabled> ".A_LANG_AGENDA_NAO_ENVIAR."";
        echo"<input type=radio name=dia value='dia' disabled> ".A_LANG_AGENDA_AVISO_UM_DIA."";
        echo"<input type=radio name=semana value='semana' disabled> ".A_LANG_AGENDA_AVISO_UMA_SEMANA."";
        echo"<input type=radio name=mes value='mes' disabled> ".A_LANG_AGENDA_AVISO_UM_MES."";
        echo"</input> ";
        echo"".A_LANG_AGENDA_OU."</td></tr>";

        echo"<tr><td></td><td disabled>".A_LANG_AGENDA_DATA_ENVIO_AVISO.": <input class=button size=9 name=dataaviso id=demo1 value=\"".$data_aviso."\"  onclick=\"javascript:NewCal('demo1','ddmmyyyy');\" onkeyup=\"javascript:NewCal('demo1','ddmmyyyy');\" disabled></td></tr>";
        
        /*** Espa�o ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";
        
        /*** Bot�es cadastrar e cancelar ***/
        echo "<tr valign=top><td colspan=2 align=left>";
        echo "<input name=agendarminhaagenda id=bot1 class=button type=submit value=\"".A_LANG_AGENDA_AGENDAR."\"></input>";
        echo "&nbsp<input name=deletar id=bot2 class=button type=button value=\"".A_LANG_AGENDA_CANCELAR."\" onClick=\"javascript:window.location='n_index_agenda.php?opcao=AcessoAgenda';\"></input>";
        echo "</td></tr>";
        echo"</table>";
        echo"</form>";

        
        /*** Espa�os ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";



        /*** Observa��o ***/
        echo"<tr><td colspan=2><b>".A_LANG_AGENDA_OBS_CAMPO_INCOMPLETO."</b></td></tr>";
    }
    if($destino_evento=="Grupo de Usu�rios")
    {

    
        /*** Tabela mostrando o evento cadastrado ***/
        echo "<center><table border=0 cellspacing=1 cellpadding=5>";

        /*** Evento ***/
        echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";

        /*** Espa�o ***/
        //echo "<tr class=zebraA><td colspan=2>&nbsp</td></tr>";


        /*** Tipo do evento ***/
        $sql="select id_tipo_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        if($linha[0]==1)
        {
            $sql1="select tipo_evento_outros
            from evento_agenda
            where evento_agenda.id_evento_agenda='$id_event'";
            $resp1=mysql_query($sql1);
            $linha1 = mysql_fetch_row($resp1);
            echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
        }
        else
        {
            $sql="select nome_tipo_evento
            from evento_agenda, tipo_evento_agenda
            where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
            and evento_agenda.id_evento_agenda='$id_event'";
            $resp=mysql_query($sql);
            $linha = mysql_fetch_row($resp);
            echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
        }


        /*** Tipo do assunto ***/
        $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
        from evento_agenda, tipo_assunto_agenda
        where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
        and evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha1 = mysql_fetch_row($resp);
        echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

        //Assunto
        $sql="select assunto_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";



        /*** Nome do evento ***/
        $sql="select nome_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";


        /*** Descri��o do evento ***/
        $sql="select descricao_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";
        

        /*** Espa�o ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";

        echo"</table></center>";

        if(isset($destino_evento))
        {
            if($destino_evento==0)
            {
                $aux_seleciona_destino_evento="selected";
                $aux_destino_evento_minha_agenda="";
                $aux_destino_evento_grupo_usuarios="";
                $aux_seleciona_tipogrupo_destinatario="selected";
                $aux_tipogrupo_destinatario_disc="";
                $aux_tipogrupo_destinatario_curso="";
                $aux_tipogrupo_destinatario_disc_curso="";
                $aux_tipogrupo_destinatario_alunos="";
                $aux_tipogrupo_destinatario_professores="";
                $aux_tipogrupo_destinatario_todos="";
                $aux_tipogrupo_destinatario_instituicao="";
                $aux_tipogrupo_destinatario_root="";
            }
            if($destino_evento=="Minha Agenda")
            {
                $aux_seleciona_destino_evento="";
                $aux_destino_evento_minha_agenda="selected";
                $aux_destino_evento_grupo_usuarios="";
            }
            if($destino_evento=="Grupo de Usu�rios")
            {
                $aux_seleciona_destino_evento="";
                $aux_destino_evento_minha_agenda="";
                $aux_destino_evento_grupo_usuarios="selected";
                    
                if($tipogrupo_destinatario==1) $aux_tipogrupo_destinatario_disc="selected";
                if($tipogrupo_destinatario==2) $aux_tipogrupo_destinatario_curso="selected";
                if($tipogrupo_destinatario==3) $aux_tipogrupo_destinatario_disc_curso="selected";
                if($tipogrupo_destinatario==4) $aux_tipogrupo_destinatario_root="selected";
                if($tipogrupo_destinatario==5) $aux_tipogrupo_destinatario_alunos="selected";
                if($tipogrupo_destinatario==6) $aux_tipogrupo_destinatario_professores="selected";
                if($tipogrupo_destinatario==7) $aux_tipogrupo_destinatario_todos="selected";
                if($tipogrupo_destinatario==8) $aux_tipogrupo_destinatario_instituicao="selected";
            }
        }
        ?>
        <!--- Inicia formul�rio --->
        <form name="formulariotipogrupo" action="n_index_agenda.php?opcao=Destinar" method="post">
        <?

        echo"<table border=0>";
            
        /*** Destino do evento ***/
        echo "<tr><td>".A_LANG_AGENDA_DESTINO_EVENTO."*:</td>";
        echo "<td><select class=select size=1 name=destinoevento id=campodestinoevento onchange=ApareceTipoGrupoDestinatario() style='width:223px'>";
        echo "<option $aux_seleciona_destino_evento value=0>".A_LANG_AGENDA_SELECIONA_DESTINO_EVENTO."</options>";
        echo "<option $aux_destino_evento_minha_agenda value='Minha Agenda'>".A_LANG_AGENDA_MINHA_AGENDA."</option>";
        echo "<option $aux_destino_evento_grupo_usuarios value='Grupo de Usu�rios'>".A_LANG_AGENDA_GRUPO_USUARIOS."</options>";
        echo "</select>";
        echo "</td></tr>";
        
        echo"</form>";
        
        ?>
        <!--- Inicia formul�rio --->
        <form name="formulariodestinatario" action="n_index_agenda.php?opcao=Destinatario" method="post">
        <?
            
        /*** Usu�rio tipo root ***/
        if($tipo_usuario == "root")
        {
            //Disciplina
            $sql1="select disciplina.id_disc, disciplina.id_usuario,
            disciplina.nome_disc, usuario.nome_usuario
            from disciplina, usuario
            where disciplina.id_usuario = usuario.id_usuario
            order by nome_disc, nome_usuario";
            $resp1=mysql_query($sql1);
            $linha1=mysql_num_rows($resp1);

            //Curso
            $sql2="select curso.id_curso, curso.id_usuario,
            curso.nome_curso, usuario.nome_usuario
            from curso, usuario
            where curso.id_usuario = usuario.id_usuario
            order by nome_curso, nome_usuario";
            $resp2=mysql_query($sql2);
            $linha2=mysql_num_rows($resp2);

            //Disciplina/Curso
            $sql3="select curso_disc.id_disc, curso_disc.id_curso, curso_disc.id_usuario,
            disciplina.nome_disc, curso.nome_curso, usuario.nome_usuario
            from curso_disc, disciplina, curso, usuario
            where curso_disc.id_disc=disciplina.id_disc
            and curso_disc.id_curso=curso.id_curso
            and curso_disc.id_usuario=usuario.id_usuario
            order by nome_disc, nome_curso, nome_usuario";
            $resp3=mysql_query($sql3);
            $linha3=mysql_num_rows($resp3);

            //Institui��o
            $sql4="select DISTINCT instituicao_ensino
            from usuario
            where instituicao_ensino<>'NULL'
            and instituicao_ensino<>' '
            order by instituicao_ensino";
            $resp4=mysql_query($sql4);
            $linha4=mysql_num_rows($resp4);

            //Tipo do grupo do destinat�rio
            echo "<tr><td id=titulotipogrupodest >".A_LANG_AGENDA_TIPO_GRUPO_DESTINATARIO."*:</td>";
            echo "<td><select class=select size=1 name=tipogrupodestinatario id=campotipogrupodestinatario onchange=ApareceGrupoDestinatarioRoot()>";
            echo "<option $aux_seleciona_tipogrupo_destinatario value=\"0\">".A_LANG_AGENDA_SELECIONA_TIPO_GRUPO_DESTINATARIO."</option>";
            echo "<option $aux_tipogrupo_destinatario_disc value=\"1\">Disciplina</option>";
            echo "<option $aux_tipogrupo_destinatario_curso value=\"2\">Curso</option>";
            echo "<option $aux_tipogrupo_destinatario_disc_curso value=\"3\">Disciplina/Curso</option>";
            echo "<option $aux_tipogrupo_destinatario_alunos value=\"5\">Alunos do AdaptWeb</option>";
            echo "<option $aux_tipogrupo_destinatario_professores value=\"6\">Professores do AdaptWeb</option>";
            echo "<option $aux_tipogrupo_destinatario_todos value=\"7\">Todos os Usu�rios do AdaptWeb</option>";
            echo "<option $aux_tipogrupo_destinatario_instituicao value=\"8\">Institui��o</option>";
            echo "</select>";
            echo "</td></tr>";
        }
        
        /*** Usu�rio tipo professor ***/
        if($tipo_usuario == "professor")
        {
            //Grupo Disciplina
            $sql1="select disciplina.id_disc, disciplina.id_usuario,
            disciplina.nome_disc, usuario.nome_usuario
            from disciplina, usuario
            where disciplina.id_usuario=usuario.id_usuario
            and disciplina.id_usuario='$id_usuario'
            order by nome_disc, nome_usuario";
            $resp1=mysql_query($sql1);
            $linha1=mysql_num_rows($resp1);

            //Grupo Curso
            $sql2="select curso.id_curso, usuario.id_usuario,
            curso.nome_curso, usuario.nome_usuario
            from curso, usuario
            where curso.id_usuario=usuario.id_usuario
            and curso.id_usuario='$id_usuario'
            order by nome_curso, nome_usuario";
            $resp2=mysql_query($sql2);
            $linha2=mysql_num_rows($resp2);

            //Grupo Disciplina/Curso
            $sql3="select curso_disc.id_disc, curso_disc.id_curso, curso_disc.id_usuario,
            disciplina.nome_disc, curso.nome_curso, usuario.nome_usuario
            from curso_disc, usuario, disciplina, curso
            where curso_disc.id_usuario = usuario.id_usuario
            and curso_disc.id_disc = disciplina.id_disc
            and curso_disc.id_curso = curso.id_curso
            and curso_disc.id_usuario = '$id_usuario'
            union
            select curso_disc.id_disc, curso_disc.id_curso, curso_disc.id_usuario,
            disciplina.nome_disc, curso.nome_curso, usuario.nome_usuario
            from curso_disc, disciplina, curso, matricula, usuario
            where curso_disc.id_usuario = usuario.id_usuario
            and curso_disc.id_disc = disciplina.id_disc
            and curso_disc.id_curso = curso.id_curso
            and matricula.id_disc = curso_disc.id_disc
            and matricula.id_curso = curso_disc.id_curso
            and matricula.id_usuario = '$id_usuario'
            and status_mat =1
            order by nome_disc, nome_curso, nome_usuario";
            $resp3=mysql_query($sql3);
            $linha3=mysql_num_rows($resp3);

            //Tipo do grupo do destinat�rio
            echo "<tr><td id=titulotipogrupodest>".A_LANG_AGENDA_TIPO_GRUPO_DESTINATARIO."*:</td>";
            echo "<td><select class=select size=1 name=tipogrupodestinatario id=campotipogrupodestinatario onchange=ApareceGrupoDestinatarioProfessor()>";
            if($linha1 == 0 && $linha2 == 0 && $linha3==0)
            {
                echo "<option $aux_seleciona_tipogrupo_destinatario value=\"0\">".A_LANG_AGENDA_SELECIONA_TIPO_GRUPO_DESTINATARIO."</option>";
                echo "<option $aux_tipogrupo_destinatario_root value=\"4\">Root</option>";
                echo "</select>";
                echo "</td></tr>";
                echo "".A_LANG_AGENDA_DISCIPLINA_E_CURSO_NAO_CADASTRADO."";
            }
            else
            {
                if($linha1 != 0 && $linha2==0 && $linha3==0)
                {
                    echo "<option $aux_seleciona_tipogrupo_destinatario value=\"0\">".A_LANG_AGENDA_SELECIONA_TIPO_GRUPO_DESTINATARIO."</option>";
                    echo "<option $aux_tipogrupo_destinatario_disc value=\"1\">Disciplina</option>";
                    echo "<option $aux_tipogrupo_destinatario_root value=\"4\">Root</option>";
                    echo "</select>";
                    echo "</td></tr>";
                    echo "".A_LANG_AGENDA_CURSO_NAO_CADASTRADO."";
                }
                else
                {
                    if($linha ==0 && $linha2 !=0 && $linha3==0)
                    {
                        echo "<option $aux_seleciona_tipogrupo_destinatario value=\"0\">".A_LANG_AGENDA_SELECIONA_TIPO_GRUPO_DESTINATARIO."</option>";
                        echo "<option $aux_tipogrupo_destinatario_curso value=\"2\">Curso</option>";
                        echo "<option $aux_tipogrupo_destinatario_root value=\"4\">Root</option>";
                        echo "</select>";
                        echo "</td></tr>";
                        echo "".A_LANG_AGENDA_DISCIPLINA_NAO_CADASTRADA."";
                    }
                    else
                    {
                        if($linha !=0 && $linha2 !=0 && $linha3==0)
                        {
                            echo "<option $aux_seleciona_tipogrupo_destinatario value=\"0\">".A_LANG_AGENDA_SELECIONA_TIPO_GRUPO_DESTINATARIO."</option>";
                            echo "<option $aux_tipogrupo_destinatario_disc value=\"1\">Disciplina</option>";
                            echo "<option $aux_tipogrupo_destinatario_curso value=\"2\">Curso</option>";
                            echo "<option $aux_tipogrupo_destinatario_root value=\"4\">Root</option>";
                            echo "</select>";
                            echo "</td></tr>";
                            echo "".A_LANG_AGENDA_DISCIPLINA_CURSO_NAO_RELACIONADO."";
                        }
                        else
                        {
                            if($linha1!=0 && $linha2!=0 && $linha3!=0)
                            {
                                echo "<option $aux_seleciona_tipogrupo_destinatario value=\"0\">".A_LANG_AGENDA_SELECIONA_TIPO_GRUPO_DESTINATARIO."</option>";
                                echo "<option $aux_tipogrupo_destinatario_disc value=\"1\">Disciplina</option>";
                                echo "<option $aux_tipogrupo_destinatario_curso value=\"2\">Curso</option>";
                                echo "<option $aux_tipogrupo_destinatario_disc_curso value=\"3\">Disciplina/Curso</option>";
                                echo "<option $aux_tipogrupo_destinatario_root value=\"4\">Root</option>";
                                echo "</select>";
                                echo "</td></tr>";
                            }
                        }
                    }
                }
            }
        }

        /*** Usu�rio tipo aluno ***/
        if($tipo_usuario == "aluno")
        {
            //Grupo Disciplina/Curso - certo
            $sql3="select curso_disc.id_disc, curso_disc.id_curso, curso_disc.id_usuario,
            disciplina.nome_disc, curso.nome_curso, usuario.nome_usuario
            from curso_disc, disciplina, curso, matricula, usuario
            where curso_disc.id_usuario = usuario.id_usuario
            and curso_disc.id_disc = disciplina.id_disc
            and curso_disc.id_curso = curso.id_curso
            and matricula.id_disc = curso_disc.id_disc
            and matricula.id_curso = curso_disc.id_curso
            and matricula.id_usuario = '$id_usuario'
            and status_mat =1
            order by nome_disc, nome_curso, nome_usuario";
            $resp3=mysql_query($sql3);
            $linha3=mysql_num_rows($resp3);

            //Tipo do grupo do destinat�rio
            echo "<tr><td id=titulotipogrupodest>".A_LANG_AGENDA_TIPO_GRUPO_DESTINATARIO."*:</td>";
            echo "<td><select name=tipogrupodestinatario id=campotipogrupodestinatario class=select size=1 onchange=ApareceGrupoDestinatarioAluno()>";
            if($linha3==0)
            {
                echo "<option $aux_seleciona_tipogrupo_destinatario value=\"0\">".A_LANG_AGENDA_SELECIONA_TIPO_GRUPO_DESTINATARIO."</option>";
                echo "<option $aux_tipogrupo_destinatario_root value=\"4\">Root</option>";
                echo "</select>";
                echo "</td></tr>";
                echo "".A_LANG_AGENDA_DISCIPLINA_CURSO_NAO_RELACIONADO."";//arrumar para aparecer em cima
            }
            else
            {
                if($linha3!=0)
                {
                    echo "<option $aux_seleciona_tipogrupo_destinatario value=\"0\">".A_LANG_AGENDA_SELECIONA_TIPO_GRUPO_DESTINATARIO."</option>";
                    echo "<option $aux_tipogrupo_destinatario_disc_curso value=\"3\">Disciplina/Curso</option>";
                    echo "<option $aux_tipogrupo_destinatario_root value=\"4\">Root</option>";
                    echo "</select>";
                    echo "</td></tr>";
                }
            }
        }

        //Grupo do Destinat�rio
        echo "<tr><td valign=top id=grupodestinatario style=display:none>".A_LANG_AGENDA_GRUPO_DESTINATARIO."*:</td><td>";

        //Grupo Disciplina
        echo "<select class=select size=1 name=grupodisc id=campogrupodisc style=display:none onchange=ApareceDestinatario()>";
        echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONA_GRUPO_DESTINATARIO."</option>";
        while ($array = mysql_fetch_array($resp1))
        {
            if($grupo_disc==$array[0].'-'.$array[1].'*'.$array[2].' - '.$array[3])
            {
                echo "<option selected value=\"".$array[0].'-'.$array[1].'*'.$array[2].' - '.$array[3]."\">".$array['nome_disc']." - ".$array['nome_usuario']."</option>";
            }
            else
            {
                echo "<option value=\"".$array[0].'-'.$array[1].'*'.$array[2].' - '.$array[3]."\">".$array['nome_disc']." - ".$array['nome_usuario']."</option>";
            }
        }
        echo "</select>";

        //Grupo Curso
        echo "<select class=select size=1 name=grupocurs id=campogrupocurs style=display:none onchange=ApareceDestinatario()>";
        echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONA_GRUPO_DESTINATARIO."</option>";
        while ($array = mysql_fetch_array($resp2))
        {
            if($grupo_curs==$array[0].'-'.$array[1].'*'.$array[2].' - '.$array[3])
            {
                echo "<option selected value=\"".$array[0].'-'.$array[1].'*'.$array[2].' - '.$array[3]."\">".$array['nome_curso'].' - '.$array['nome_usuario']."</option>";
            }
            else
            {
                echo "<option value=\"".$array[0].'-'.$array[1].'*'.$array[2].' - '.$array[3]."\">".$array['nome_curso'].' - '.$array['nome_usuario']."</option>";
            }
        }
        echo "</select>";

        //Grupo Disciplina/Curso
        echo "<select class=select size=1 name=grupodisccurs id=campogrupodisccurs style=display:none onchange=ApareceDestinatario()>";
        echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONA_GRUPO_DESTINATARIO."</option>";
        while ($array = mysql_fetch_array($resp3))
        {
            if($grupo_disccurs==$array[0].'/'.$array[1].'-'.$array[2].'*'.$array[3].' / '.$array[4].' - '.$array[5])
            {
                echo "<option selected value=\"".$array[0].'/'.$array[1].'-'.$array[2].'*'.$array[3].' / '.$array[4].' - '.$array[5]."\">".$array['nome_disc'].' / '.$array['nome_curso'].' - '.$array['nome_usuario']."</option>";
            }
            else
            {
                echo "<option value=\"".$array[0].'/'.$array[1].'-'.$array[2].'*'.$array[3].' / '.$array[4].' - '.$array[5]."\">".$array['nome_disc'].' / '.$array['nome_curso'].' - '.$array['nome_usuario']."</option>";
            }
        }
        echo "</select>";

        //Grupo Root
        echo "<select name=gruporoot id=campogruporoot class=submit size=1 style=display:none onchange=ApareceDestinatario()>";
        echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONA_GRUPO_DESTINATARIO."</option>";
        echo "<option value=\"Root\">Root</option>";
        echo "</select>";


        //Grupo Alunos do AdaptWeb
        echo "<select class=select size=1 name=grupoalu id=campogrupoalu style=display:none onchange=ApareceDestinatario()>";
        echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONA_GRUPO_DESTINATARIO."</option>";
        echo "<option value=\"Alunos do AdaptWeb\">".A_LANG_AGENDA_ALUNOS_ADAPTWEB."</option>";
        echo "</select>";

        //Grupo Professores do AdaptWeb
        echo "<select class=select size=1 name=grupoprof id=campogrupoprof style=display:none onchange=ApareceDestinatario()>";
        echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONA_GRUPO_DESTINATARIO."</option>";
        echo "<option value=\"Professores do AdaptWeb\">".A_LANG_AGENDA_PROFESSORES_ADAPTWEB."</option>";
        echo "</select>";

        //Grupo de todos os usu�rios do AdaptWeb
        echo "<select class=select size=1 name=grupotod id=campogrupotod style=display:none onchange=ApareceDestinatario()>";
        echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONA_GRUPO_DESTINATARIO."</option>";
        echo "<option value=\"Todos os Usu�rios do AdaptWeb\">".A_LANG_AGENDA_TODOS_USUARIOS_ADAPTWEB."</option>";
        echo "</select>";

        //Grupo Institui��o
        echo "<select class=select size=1 name=grupoinst id=campogrupoinst style=display:none onchange=ApareceDestinatario()>";
        echo "<option selected value=\"0\">".A_LANG_AGENDA_SELECIONA_GRUPO_DESTINATARIO."</option>";
        while ($array = mysql_fetch_array($resp4))
        {
            if($grupo_inst==$array[0])
            {
                echo "<option selected value=\"".$array[0]."\">".$array['instituicao_ensino']."</option>";
            }
            else
            {
                echo "<option value=\"".$array[0]."\">".$array['instituicao_ensino']."</option>";
            }
        }
        echo "</select>";
            


        echo "</td></tr>";//Encerra dado e linha
        echo"</form>";
         
        /*** Espa�os ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";
        echo "<tr><td colspan=2>&nbsp</td></tr>";

        /*** Observa��o ***/
        //echo"<tr><td colspan=2><b>".A_LANG_AGENDA_OBS_CAMPO_INCOMPLETO."</b></td></tr>";
    }
    if($destino_evento=="0")
    {
        /*** Tabela mostrando o evento cadastrado ***/
        echo "</center><table border=0  cellspacing=1 cellpadding=5>";


        /*** Evento ***/
        echo "<tr class=zebraB><td colspan=2 align=center><b>".A_LANG_AGENDA_EVENTO."</b></td></tr>";


        /*** Espa�o ***/
        //echo "<tr class=zebraA><td colspan=2>&nbsp</td></tr>";


        /*** Tipo do evento ***/
        $sql="select id_tipo_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        if($linha[0]==1)
        {
            $sql1="select tipo_evento_outros
            from evento_agenda
            where evento_agenda.id_evento_agenda='$id_event'";
            $resp1=mysql_query($sql1);
            $linha1 = mysql_fetch_row($resp1);
            echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO_OUTROS.":</td><td>$linha1[0]</td></tr>";
        }
        else
        {
            $sql="select nome_tipo_evento
            from evento_agenda, tipo_evento_agenda
            where evento_agenda.id_tipo_evento=tipo_evento_agenda.id_tipo_evento
            and evento_agenda.id_evento_agenda='$id_event'";
            $resp=mysql_query($sql);
            $linha = mysql_fetch_row($resp);
            echo "<tr class=zebraA><td>".A_LANG_AGENDA_TIPO_EVENTO.":</td><td>$linha[0]</td></tr>";
        }


        /*** Tipo do assunto ***/
        $sql="select evento_agenda.id_tipo_assunto, nome_tipo_assunto
        from evento_agenda, tipo_assunto_agenda
        where evento_agenda.id_tipo_assunto=tipo_assunto_agenda.id_tipo_assunto
        and evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha1 = mysql_fetch_row($resp);
        echo "<tr class=zebraB><td>".A_LANG_AGENDA_TIPO_ASSUNTO.":</td><td>$linha1[1]</td></tr>";

        //Assunto
        $sql="select assunto_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraA><td>".A_LANG_AGENDA_ASSUNTO.":</td><td>$linha[0]</td></tr>";



        /*** Nome do evento ***/
        $sql="select nome_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraB><td>".A_LANG_AGENDA_NOME_EVENTO.":</td><td>$linha[0]</td></tr>";


        /*** Descri��o do evento ***/
        $sql="select descricao_evento
        from evento_agenda
        where evento_agenda.id_evento_agenda='$id_event'";
        $resp=mysql_query($sql);
        $linha = mysql_fetch_row($resp);
        echo "<tr class=zebraA><td>".A_LANG_AGENDA_DESCRICAO."</td><td>$linha[0]</td></tr>";

        /*** Espa�o ***/
        echo "<tr><td colspan=2>&nbsp</td></tr>";


        echo "</table></center>";//Encerra tabela que mostra o agendamento cadastrado

        if(isset($destino_evento))
        {
            //$aux_seleciona_destino_evento="selected";
            //if($destino_evento=="Minha Agenda") $aux_destino_evento_minha_agenda="selected";
            //if($destino_evento=="Grupo de Usu�rios") $aux_destino_evento_grupo_usuarios="selected";
                
            if($destino_evento=="0")
            {
                $aux_seleciona_destino_evento="selected";
                $aux_destino_evento_minha_agenda="";
                $aux_destino_evento_grupo_usuarios="";
                $aux_seleciona_tipogrupo_destinatario="selected";
                $aux_tipogrupo_destinatario_disc="";
                $aux_tipogrupo_destinatario_curso="";
                $aux_tipogrupo_destinatario_disc_curso="";
                $aux_tipogrupo_destinatario_alunos="";
                $aux_tipogrupo_destinatario_professores="";
                $aux_tipogrupo_destinatario_todos="";
                $aux_tipogrupo_destinatario_instituicao="";
                $aux_tipogrupo_destinatario_root="";
            }
            if($destino_evento=="Minha Agenda")
            {
                $aux_seleciona_destino_evento="";
                $aux_destino_evento_minha_agenda="selected";
                $aux_destino_evento_grupo_usuarios="";
            }
            if($destino_evento=="Grupo de Usu�rios")
            {
                $aux_seleciona_destino_evento="";
                $aux_destino_evento_minha_agenda="";
                $aux_destino_evento_grupo_usuarios="selected";

                if($tipogrupo_destinatario==1) $aux_tipogrupo_destinatario_disc="selected";
                if($tipogrupo_destinatario==2) $aux_tipogrupo_destinatario_curso="selected";
                if($tipogrupo_destinatario==3) $aux_tipogrupo_destinatario_disc_curso="selected";
                if($tipogrupo_destinatario==4) $aux_tipogrupo_destinatario_root="selected";
                if($tipogrupo_destinatario==5) $aux_tipogrupo_destinatario_alunos="selected";
                if($tipogrupo_destinatario==6) $aux_tipogrupo_destinatario_professores="selected";
                if($tipogrupo_destinatario==7) $aux_tipogrupo_destinatario_todos="selected";
                if($tipogrupo_destinatario==8) $aux_tipogrupo_destinatario_instituicao="selected";
            }
        }

        ?>
        <!--- Inicia formul�rio --->
        <form name="formulariotipogrupo" action="n_index_agenda.php?opcao=Destinar" method="post">
        <?

        echo"<table border=0>";

        /*** Destino do evento ***/
        echo "<tr><td>".A_LANG_AGENDA_DESTINO_EVENTO."*:</td>";
        echo "<td><select class=select size=1 name=destinoevento id=campodestinoevento onchange=ApareceTipoGrupoDestinatario() style='width:223px'>";
        echo "<option $aux_seleciona_destino_evento value=0>".A_LANG_AGENDA_SELECIONA_DESTINO_EVENTO."</options>";
        echo "<option $aux_destino_evento_minha_agenda value='Minha Agenda'>".A_LANG_AGENDA_MINHA_AGENDA."</option>";
        echo "<option $aux_destino_evento_grupo_usuarios value='Grupo de Usu�rios'>".A_LANG_AGENDA_GRUPO_USUARIOS."</options>";
        echo "</select>";
        echo "</td></tr>";
        echo"</table>";
            
        echo"</form>";
    }
}

?>

   </td>
  </table>
  </td>
  </tr>
</table>
