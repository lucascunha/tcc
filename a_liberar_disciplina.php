<?php
  
global $NomeDisciplina, $conteudo, $tipo_usuario, $id_usuario;

$TirarAcesso = $_POST['TirarAcesso'];
$DiscLib = $_POST['DiscLib'];
$PermitirAcesso = $_POST['PermitirAcesso'];
$DiscNaoLib = $_POST['DiscNaoLib'];

$mensagem = '';
$qt_erros_inc = 0;
$qt_erros_exc = 0;

// o usuario 2 foi utilizado para demonstra��o da disciplina e n�o pode incluir, excluir, alterar dados
if  ($id_usuario <> 2)
{  
	// atualiza status_disciplina como '0'
	if (isset($TirarAcesso) and count($DiscLib) > 0)   
	{		
		$conn = &ADONewConnection($A_DB_TYPE); 
		$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
		
		for($j=0; $j < count($DiscLib); $j++) 
		{		
			$sql = "UPDATE disciplina SET status_disc='0' WHERE id_disc = $DiscLib[$j].";			
			$rs = $conn->Execute($sql);     
			if ($rs === false) die(A_LANG_DISCIPLINES_MSG2);  				     
		}
		$mensagem = 'Todas as disciplinas selecionadas foram bloqueadas';
		$rs->Close(); 		
	}
	else
	{
		if (isset($TirarAcesso)) 
		{
			$mensagem = 'Nenhuma disciplina foi selecionada para bloquear';	
			$qt_erros_exc++; 
		}
	}
	
	
	// atualiza status_disciplina para '1'
	if (isset($PermitirAcesso) and count($DiscNaoLib) > 0)   
	{	
		$conn = &ADONewConnection($A_DB_TYPE); 
		$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
		
		for($j=0; $j < count($DiscNaoLib); $j++) 
		{		
			$sql = "UPDATE disciplina SET status_disc='1' WHERE id_disc=$DiscNaoLib[$j].";	
			$rs = $conn->Execute($sql);     
			
			if ($rs === false) die(A_LANG_DISCIPLINES_MSG2);  				     
		}
		$mensagem = 'Todas as disciplinas selecionadas foram liberadas';
		$rs->Close(); 		
	}
	else
	{
		if (isset($PermitirAcesso))
		{
			$mensagem = 'Nenhuma disciplina foi selecionada para liberar';	
			$qt_erros_inc++;
		}
	}
}

if  ($id_usuario == 2)
{
      
      $mensagem = A_LANG_DEMO;
      $qt_erros_exc++;
      $TirarAcesso = "demo";
      $PermitirAcesso = "demo";
      
}


// **************************************************************************
// *  CRIA MATRIZ ORELHA                                                    *
// **************************************************************************
  
$orelha = array();  

$orelha = array(
  	      array(   
   		      "LABEL" => A_LANG_LIBERATE_DISCIPLINES , 
     		      "LINK" => "",    
     	  	      "ESTADO" => "ON"
   	           )		     		       		   		  
   	       ); 
   	       
MontaOrelha($orelha);  

?>
 
<script type="text/javascript">

	function selectAll(Nome_Select,selectAll, Nome_Input, Nome_Form) {
		
		if ((document.getElementById(Nome_Input).checked == true))
		{
			selectAll = true;
		} else 
		{
			selectAll = false;
		}

		//if (typeof Nome_Select == "string") {
			Nome_Select = document.getElementById(Nome_Select);
		//}
		//alert (typeof Nome_Select);
		//alert (Nome_Select.type);

		if (Nome_Select.type == "select-multiple") {
			for (var i = 0; i < Nome_Select.options.length; i++) {
				Nome_Select.options[i].selected = selectAll;
			}
		}
	}
	</script>



<!-- Modify this file to change the way the tree looks -->
<link type="text/css" rel="stylesheet" href="css/xtree.css">

<table CELLSPACING=1 CELLPADDING=1 width="100%"  border ="0"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> >


	<tr>
    	<td>
			<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">

			<?php
				//if ((!(isset($PermitirAcesso)))&&(!(isset($TirarAcesso))))	
					{ ?>	
						<tr>
	  						<td colspan="3">
	  							<table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0">
									<tr>
										<td>	
	  										<div class="Mensagem_Ok">
				   								<p class="texto1">	   
													<table cellspacing="0" cellpadding="0">
														<tr>
															<td width="50">
																<img src="imagens/icones/notice.png" alt="" />
															</td>
															<td valign="center">	
																<?php echo A_LANG_TENVIRONMENT_LIBERATE_DESCRIPTION ;	 ?>
	   														</td>
														</tr>
													</table>	
	   											</p>	   	
	    									</div>
	    								</td>
	   								</tr>
	   							</table>
	   						</td>	
						</tr>
			<?	} 

			if ((isset($PermitirAcesso)))
			{	
				if ($mensagem != '')
				{

				   	if (($qt_erros_inc == 0))
				   	{ 	
				   		$tipo_msg = "success";
				   		$icone = "<img src='imagens/icones/success.png' border='0' />";
					}
				   	else
				   	{	
				   		$tipo_msg = "error";
						$icone = "<img src='imagens/icones/error.png' border='0' />";				
					}

					?>
					<script type="text/javascript">
 	   				showNotification({
	       			message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo $mensagem; ?></td></tr></table>", 	      			type: "<? echo $tipo_msg; ?>",
 	       			autoClose: true,
 	       			duration: 8                                        
 	   				});
 	   				//error //success //warning
 					</script>			

				<?php 
				} 
			} ?>

	
	<tr>
	  	<td>

	   	</td>	
	</tr>



				
				<tr>							
		    		<td valign="top"  align="right" width="170">
						<?	echo A_LANG_TENVIRONMENT_LIBERATE_LIB;	?>
	  	     		</td>
	  	     		<form name="adicao" action="a_index.php?opcao=LiberarDisciplina" method="post">
		     		<td align="left" height="35" width="410">
									<?			
					  					$conn = &ADONewConnection($A_DB_TYPE); 
					  					$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
									  	$sql="select id_disc, nome_disc from disciplina where id_usuario=$id_usuario and status_xml='1' and status_disc='0' order by nome_disc";
					  					$rs = $conn->Execute($sql);	  
					  				?>
					 				<select class="button" NAME="DiscNaoLib[]" MULTIPLE size="6" maxsize="15"  style="width: 400px" id="DiscNaoLib"> 						 							 
					  				<? 
										while (!$rs->EOF)
										{					 	         									                								                							
						    	   			echo "<option value=\"".$rs->fields[0]."\">".$rs->fields[1]."</option>";
								           	$rs->MoveNext();
     									}
										$rs->Close(); 
						  			?>      	
						  			</select>	
					</td>														
					<td valign="center" align = "left">
					<table>
						<tr>
							<td align="left" width="25">
								<input class="buttonBig" type="checkbox" name="All_Liberar" onclick="selectAll('DiscNaoLib',true, 'All_Liberar', 'adicao')" id="All_Liberar">
							</td>
							<td align="left">
								Selecionar todos
							</td>
						</tr>
						<tr>
							<td colspan=2>	
								
									<script type="text/javascript">
										$(
											function() 
											{
											$("#customDialog").easyconfirm
											(
												{	locale: 
													{
														title: '',
														//text: '<br>Confirma a libera��o das disciplinas selecionadas ?<br>',
														text: '<table><tr><td width=50><img src=imagens/icones/notice.png></td><td valign=center>Confirma a libera��o das disciplinas selecionadas ?</td></tr></table>',
														button: ['N�o',' Sim'],
														closeText: 'Fechar'
									
													}
												}
												
											);
												$("#customDialog").click(function() 
												{
											
												}
												);
									
											}
										);
									</script>
									
									<input class="buttonBig" type="submit" value="Liberar Disciplinas" name="PermitirAcesso" id="customDialog">
							</td>
						</tr>
					</table>
				</td>




	   				</form>
	    		</tr>
	    		<tr><td></td></tr>
	    	</table>		
			<br><br>	
			<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">


			<?php
			if ((isset($TirarAcesso)))
			{	
				if ($mensagem != '')
				{

				   	if (($qt_erros_exc == 0))
				   	{ 	
				   		$tipo_msg = "success";
				   		$icone = "<img src='imagens/icones/success.png' border='0' />";
					}
				   	else
				   	{	
				   		$tipo_msg = "error";
						$icone = "<img src='imagens/icones/error.png' border='0' />";				
					}

					?>
					<script type="text/javascript">
 	   				showNotification({
	       			message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo $mensagem; ?></td></tr></table>", 	      			type: "<? echo $tipo_msg; ?>",
 	       			autoClose: true,
 	       			duration: 8                                        
 	   				});
 	   				//error //success //warning
 					</script>			

				<?php 
				} 
			} ?>			

				<tr><td></td><td></td><td></td></tr>
				<tr>	
					<td valign="top"  align="right" width="170">
						<?	echo A_LANG_TENVIRONMENT_LIBERATE_LIB_DIS; ?>
					</td>	
					<form name="exclusao" action="a_index.php?opcao=LiberarDisciplina" method="post">
					<td align="left" height="35" width="410">
									<?			
				  						$conn = &ADONewConnection($A_DB_TYPE); 
				  						$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
										$sql="select id_disc, nome_disc from disciplina where id_usuario=$id_usuario and status_xml='1' and status_disc='1' order by nome_disc";
				  						$rs = $conn->Execute($sql);	  
				  						$sql2="select matricula.id_disc from disciplina, matricula where disciplina.id_usuario=$id_usuario and status_xml='1' and status_disc='1' and disciplina.id_disc = matricula.id_disc";
				  						$rs2 = $conn->Execute($sql2);	  
				 					?>
				  					<select class="button" name="DiscLib[]" multiple size="15" maxsize="15" style="width: 400px" id="DiscLib"> 						 							 
				  					<? 
									while (!$rs->EOF)
									{
						  			$cont = 0;
						  				// numero de alunos matriculados
						  
						  				$rs2->movefirst();
						  				while (!$rs2->EOF)
						  				{
						    				if  ($rs->fields[0] == $rs2->fields[0]) 	
						         			$cont++;
						         			$rs2->MoveNext();
						  				} // final while	
						   
						  				if (count > 1 ) 
						     			$msg_matricula = 'matricula';
						  				else
						     			$msg_matricula = 'matriculas';    
						     
						  				echo "<option value=\"".$rs->fields[0]."\">".$rs->fields[1].' ('.$cont.' '.$msg_matricula.')'."</option>";
					          			$rs->MoveNext();
									}// final while
	
				        			$rs->Close(); 
				        			$rs2->Close(); 
				  					?>      	
				  					</select>	
					</td>

					<!-- <td valign="center" height="35">
	     				<input class="buttonBig" type="submit" value="Bloquear Disciplinas Selecionadas" name="TirarAcesso">
	    			</td>-->
				<td valign="center" align = "left">
					<table>
						<tr>
							<td align="left" width="25">
								<input class="buttonBig" type="checkbox" name="All_Bloquear" onclick="selectAll('DiscLib',true, 'All_Bloquear', 'exclusao')" id="All_Bloquear">
							</td>
							<td align="left">
								Selecionar todos
							</td>
						</tr>
						<tr>
							<td colspan=2>	
								
									<script type="text/javascript">
										$(
											function() 
											{
											$("#customDialog01").easyconfirm
											(
												{	locale: 
													{
														title: '',
														text: '<table><tr><td width=50><img src=imagens/icones/notice.png></td><td valign=center>Confirma o bloqueio das disciplinas selecionadas ?</td></tr></table>',
														button: ['N�o',' Sim'],
														closeText: 'Fechar'
									
													}
												}
												
											);
												$("#customDialog01").click(function() 
												{
											
												}
												);
									
											}
										);
									</script>
									
									<input class="buttonBig" type="submit" value="Bloquear Disciplinas" name="TirarAcesso" id="customDialog01">
		
							</td>
						</tr>
					</table>
				</td>	    							

					</form>
				</tr>
				<tr><td></td></tr>	
			</table><br><br>
		</td>	
   	</tr>
</TABLE>