
<?php

/*
 * Pagina de t�picos criados do ambiente professor
 
 * @version 1.0 <04/10/2008>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como TCC - Trabalho de Conclus�o de Curso
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */ 

global $id_aluno,$tipo,$logado,$num_disc,$num_curso,$id_prof,$disciplina, $curso;

$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;

include_once "include/conecta.php";


if ($logado<>"S"){
echo "<META HTTP-EQUIV='REFRESH' CONTENT=\"0; URL='./login.php'\">";
		}
else {
  
 //session_register("num_disc");
 //session_register("num_curso");
 //session_register("curso");
 //session_register("id_prof");
 //session_register("disciplina");
$num_disc       = $_SESSION['num_disc'];   
$num_curso      = $_SESSION['num_curso'];  
$curso          = $_SESSION['curso'];      
$id_prof        = $_SESSION['id_prof'];    
$disciplina     = $_SESSION['disciplina']; 


/******************/
/* atualizar avaliacao formativa */

$msg = 	$_GET['msg'];
$ok = 	$_GET[ "ok" ];

if ($_GET[ "ok" ])
{
			if ($ok==1)
			$msg = "Avalia��o Individual realizada com sucesso.";
            if ($ok==2)
            $msg = "Problemas na Avalia��o Individual.";

			if ($msg != '')
			{
			 
			      if (($ok == 1))
			      {   
			        $tipo_msg = "success";
			        $icone = "<img src='imagens/icones/success.png' border='0' />";
			      }
			      else
			      { 
			        $tipo_msg = "error";
			        $icone = "<img src='imagens/icones/error.png' border='0' />";       
			      }
			   ?>
			  <script type="text/javascript">
			      showNotification({
			        message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo $msg; ?></td></tr></table>",              
			        type: "<? echo $tipo_msg; ?>",
			        autoClose: true,
			        duration: 8                                        
			      });
			      //error //success //warning
			  </script>     
			<?php
			}
}			
/*******************/
?>

<html>
<head>
	<title>Adaptweb</title>
	<style type="text/css">
		#fixedtipdiv{
			position:absolute;
			padding: 2px;
			border:1px solid black;
			font:normal 11px Arial, Helvetica, sans-serif;
			line-height:18px;
			z-index:100;
		}

	</style>
</head>
<body bgcolor="#ffffff" link="#0000cd">


<? 
  $orelha = array();  
  $orelha = array(
  		array(   
   		   "LABEL" => "<a  class=menu href=n_index_navegacao.php?opcao=Navega&id_prof=".$id_prof."&num_disc=".$num_disc."&num_curso=".$num_curso."&disciplina=".$disciplina."&curso=".urlencode($curso).">".A_LANG_LENVIRONMENT_NAVTYPE."</a>", 
     		   "LINK" => "n_index_navegacao.php?opcao=Navega", 
     		   "ESTADO" =>"OFF"
   		    ),
		array(   
   		   "LABEL" => "<a  class=menu href=n_index_navegacao.php?opcao=MuralRecados&id_prof=".$id_prof."&num_disc=".$num_disc."&num_curso=".$num_curso."&disciplina=".$disciplina."&curso=".urlencode($curso).">".A_LANG_LENVIRONMENT_MURAL."</a>",  
     		   "LINK" => "n_index_navegacao.php?opcao=MuralRecados",    		                                 
     		   "ESTADO" => "OFF"
   		   ),
//Alteracao Claudia da Rosa (UDESC) em 13/09/2008 para F�rum de Discuss�o
		   array(   
   		   "LABEL" => A_LANG_LENVIRONMENT_FORUM,  
     		   "LINK" => "n_index_navegacao.php?opcao=ForumDiscussao",    		                                 
     		   "ESTADO" => "ON"
   		   ) 	     					     		       		   		   		       		   		   
   		  ); 

  MontaOrelha($orelha);  

?>


<table CELLSPACING=5 CELLPADDING=3 border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> style="height:350px;">
	<tr valign="top">
		<td>
	<table CELLSPACING=0 CELLPADDING=0 width="98%"  border = "0"  bgcolor="#ffffff" style=" margin-left: 10px; margin-top: 10px; border-left:3px; border-top:3px; border-bottom:3px; border-right:3px; border-style:solid; border-color:#eeeeee;" >	
	<tr>
		<td>
		<? 
		  $orelha = array();  
		  $orelha = array(
		 
				array(   
		   		   "LABEL" => 'F�rum',  
		     		   "LINK" => "n_index_navegacao.php?opcao=ForumDiscussao",    		                                 
		     		   "ESTADO" => "OFF"
		   		   ), 			     		       		   		   		       		   		   
				   array(   
		   		   "LABEL" => "Criar T�pico de Discuss�o",  
		     		   "LINK" => "n_index_navegacao.php?opcao=CriaTopicoDiscussao",    		                                 
		     		   "ESTADO" => "OFF"
		   		   ),
				array(   
		   		   "LABEL" => 'Avalia��o Individual do Aluno',  
		     		   "LINK" => "n_index_navegacao.php?opcao=AvaliarAlunosAutor",    		                                 
		     		   "ESTADO" => "ON"
		   		   ), 			     		       		   		   		       		   		   
				   array(   
		   		   "LABEL" => "Avalia��o Formativa do Aluno",  
		     		   "LINK" => "n_index_navegacao.php?opcao=AvaliarAlunosAutorFormativa",    		                                 
		     		   "ESTADO" => "OFF"
		   		   )	     					    	     					     		       		   		   		       		   		   
		   		  ); 
		
		
		  MontaOrelha($orelha);  
		
		?>
		</td>
	</tr>
	<tr>
	<td>
 <table CELLSPACING=0 CELLPADDING=25 width="100%"  border = "0"  bgcolor="#ffffff" >
  <tr>
      <td>
	<table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0"  bgcolor="#ffffff">	
	<tr>
		<td>

		<?php
		if ($msg == '')	{ ?>
			   		           <div class="Mensagem_Ok">
			   		             <?
			   		             echo "<p class=\"texto1\">\n";     
			   		             echo "<table cellspacing=\"0\" cellpadding=\"0\">";
			   		             echo "  <tr>";
			   		             echo "    <td width=\"50\">";
			   		             echo "      <img src=\"imagens/icones/notice.png\" alt=\"\" />";
			   		             echo "    </td>";
			   		             echo "    <td valign=\"center\">";
			   		             
			   		             echo "	Para realizar a avalia��o formativa, voc� deve PRIMEIRO avaliar a participa��o individual do aluno em cada t�pico de discuss�o para definir a qualidade da informa��o postada pelo aluno.";
			   		             echo "    </td>";
			   		             echo "  </tr>";
			   		             echo "</table>";  
			   		             echo "</p>\n";      
			   		             ?>    
			   		           </div><br><br>
		<?php } ?>
<div class="Mensagem_Gray">
  <h2 style="padding-top:0px; padding-left:10px;">
		<? 
		echo "Avalia��o Individual - ".$disciplina; 
		echo " ".A_LANG_LENVIRONMENT_FORCOURSE." "; 
		echo $curso;		
		?>
  </h2>
</div>			
		<br>

	<tr>
		<td>



			<fieldset class="campo">
				<legend align="left">
					<font color="#000000">| <b><? echo "T�picos para avalia��o individual do aluno"; ?></b> |</font>
				</legend>
			
		
				<?
				$sql = mysql_query ("SELECT topico_forum.titulo,topico_forum.descricao,topico_forum.data,topico_forum.id_disciplina,topico_forum.id_curso,
				topico_forum.hora,topico_forum.avaliacao,topico_forum.id_topico,usuario.nome_usuario,usuario.tipo_usuario,usuario.id_usuario,
				Count(participacao_topico_forum.id_participacao) AS participacoes_de_um_topico
				FROM topico_forum LEFT JOIN participacao_topico_forum USING (id_topico)
				INNER JOIN usuario ON (topico_forum.participacao = usuario.id_usuario)
				WHERE topico_forum.id_curso = '$num_curso' AND topico_forum.id_disciplina ='$num_disc'  and topico_forum.avaliacao = 1
				Group By participacao_topico_forum.id_topico, topico_forum.titulo,topico_forum.descricao,topico_forum.data,
				topico_forum.hora,topico_forum.avaliacao, topico_forum.id_topico ORDER BY data DESC, hora DESC");
				
				$qtd = mysql_num_rows($sql); // verifica o n�mero total de resultados




				if($qtd <= 0 )
				{ ?>
								<br>
			   		           <div class="Mensagem_Amarelo">
			   		             <?
			   		             echo "<p class=\"texto1\">\n";     
			   		             echo "<table cellspacing=\"0\" cellpadding=\"0\">";
			   		             echo "  <tr>";
			   		             echo "    <td width=\"50\">";
			   		             echo "      <img src=\"imagens/icones/warning.png\" alt=\"\" />";
			   		             echo "    </td>";
			   		             echo "    <td valign=\"center\">";
			   		             
			   		             echo "N�o existem t�picos para avalia��o do aluno!";
			   		             echo "    </td>";
			   		             echo "  </tr>";
			   		             echo "</table>";  
			   		             echo "</p>\n";      
			   		             ?>    
			   		           </div>
			   		    <?       					
						
				}
				else 
				
				{
				echo "<br>";	
				echo "<table CELLSPACING=\"0\" CELLPADDING=\"5\" WIDTH=\"100%\" border=\"0\">"; 
				echo"<tr >";
				echo"<td class=\"redondo_inicio\" style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;\" >Titulo do T�pico (N� Participantes)</td>";
				echo"<td style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;\" >Autor</td>";
				echo"<td style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;\" ><center>Data</center></td>";
		        echo"<td style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;\" ><center>Hora</center></td>";
				echo"<td class=\"redondo_final\" style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;\"><center>Pend�ncias de Avalia��o Individual</center></td>";
				
				echo"</tr>";
			    			  
				  $cStyle = "zebraB";
				  $aux_mostrar = 0;
				  while ($pesq = mysql_fetch_array($sql))
				  {		
							echo "<tr class=".$cStyle.">";	
					        if ((strlen($pesq['titulo'])) > 70)
					        {
								$pesq['titulo'] = substr($pesq['titulo'], 0, 60). " ...";

					        }  	
					    	if ((strlen($pesq['nome_usuario'])) > 11)
					        {
								$pesq['nome_usuario'] = substr($pesq['nome_usuario'], 0, 11). " ...";

					        }  	    


					   		//echo "<td align=left> <a href=\"n_index_navegacao.php?opcao=ParticiparTopicoDiscussaoAutor&id=".$pesq['id_topico']."\">".$pesq['titulo']."&nbsp;(".$pesq['participacoes_de_um_topico'].")</a></td>";
							
							echo "<td class=\"redondo_inicio\" align=left>".$pesq['titulo']."&nbsp;(".$pesq['participacoes_de_um_topico'].")</td>";
							
							/*Verificar se existem pendencias na avalia��o*/
							$pendencias_aval = mysql_query ("SELECT * FROM participacao_topico_forum where id_topico =".$pesq['id_topico']." and qualidade_informacao = 0 and id_usuario <> $id_prof");
							$numero_pendencias = mysql_num_rows($pendencias_aval);
							/*Verificar se existem pendencias na avalia��o*/


							/*XXXXXXXXXXXX TRABALHANDO O NEGRITO DO PROFESSOR XXXXXXXXXXXXXXXXXXXXXX*/
							$id = $pesq['id_usuario'];
							if( $id == $id_prof)
							{
								echo "<td align=left><b>".$pesq['nome_usuario']."</b></th></a></td>";}
							else
							{
								echo "<td  align=left>".$pesq['nome_usuario']."</th></a></td>";}
							/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
								
							/*XXXXXXXXXXXXXXXXXX TRABALHANDO A DATAXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
							$altera_data = explode('-',$pesq['data']);   
							$data= $altera_data[2]."/".$altera_data[1]."/".$altera_data[0];
							echo "<td align=center>".$data."</a></td>"; 
							/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
							
							echo "<td align=center>".$pesq['hora']."</a></td>";
							
							/*XXXXXXXXXXXXXXX TRABALHANDO A OP��O DE AVALIA��O XXXXXXXXXXXXXXXXXXX*/ 
							if($pesq['avaliacao']==1)
							{
								if ($pesq['participacoes_de_um_topico']<=0)
								{						
									echo "<td class=\"redondo_final\" align=center width=13% >".A_LANG_FORUM_NAO."</td>";
								}
								else
								{
									$avaliarAluno=1;
									if ($numero_pendencias > 0)
									{
										echo "<td class=\"redondo_final\" style=\"text-decoration:none\"; align=center> <a class='iframe' href=\"x_index_navegacao.php?opcao=AvaliarParticipacaoTopicoAutor&id=".$pesq['id_topico']."\"><b title=\"Avaliar Participa��o do Aluno\"  class='tooltip'><font color=red >".A_LANG_FORUM_SIM."</font></b></a></td>";
										$aux_mostrar++;
									}
									else
									{	
										echo "<td class=\"redondo_final\" style=\"text-decoration:none;\"; align=center> <a class='iframe' href=\"x_index_navegacao.php?opcao=AvaliarParticipacaoTopicoAutor&id=".$pesq['id_topico']."\"><b title=\"Avaliar Participa��o do Aluno\"  class='tooltip'>".A_LANG_FORUM_NAO."</b></a></td>";
									}
								}
							 }
							 else
							 {
									echo "<td class=\"redondo_final\" align=center width=13%>".A_LANG_FORUM_NAO."</a></td>";}
							  /*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
													   
						   
						   
						   
						   $cStyle = $cStyle=="zebraA"?"zebraB":"zebraA";
						   echo "</tr>";
	   
				}//Fecha while
			echo "</table>";}
			?>
				<br>
			 	</fieldset> 

<br>
<?php
if ($aux_mostrar ==0 && $qtd > 0)
{
?>
<br>
	 <div class="Mensagem_xxx">
	   <?
	   echo "<p class=\"texto1\">\n";     
	   echo "<table cellspacing=\"0\" cellpadding=\"0\" border=0 width=100%>";
	   echo "  <tr>";
	   echo "    <td valign=\"center\" align=\"right\">";
	   echo "<input type=\"button\" class=\"buttonBigBig-01\" value=\"Avalia��o Formativa\" onclick=\"window.location='n_index_navegacao.php?opcao=AvaliarAlunosAutorFormativa'\">";
	   echo "    </td>";
	   echo "  </tr>";
	   echo "</table>";  
	   echo "</p>\n";      
	   ?>    
	 </div><br>

<?php
}
?>			
		</td>
	</tr>
</table>
		
<br>
</td>
</tr>
</table>		
<br><br>
</td>
</tr>
</table>
<br>
</td>
</tr>
</table>

</td>
</tr>
</table>

</body>
</html>
<?php } ?>



		<script>
			$(document).ready(function(){
/*
$("#cboxTopLeft").hide();
$("#cboxTopRight").hide();
$("#cboxBottomLeft").hide();
$("#cboxBottomRight").hide();
$("#cboxMiddleLeft").hide();
$("#cboxMiddleRight").hide();
$("#cboxTopCenter").hide();
$("#cboxBottomCenter").hide();	
*/

				//Examples of how to assign the ColorBox event to elements
				$(".group1").colorbox({rel:'group1'});
				$(".group2").colorbox({rel:'group2', transition:"fade"});
				$(".group3").colorbox({rel:'group3', transition:"none", width:"75%", height:"75%"});
				$(".group4").colorbox({rel:'group4', slideshow:true});
				$(".ajax").colorbox();
				$(".youtube").colorbox({iframe:true, innerWidth:425, innerHeight:344});
				$(".vimeo").colorbox({iframe:true, innerWidth:500, innerHeight:409});
				$(".iframe").colorbox({iframe:true, initialWidth:"70px", initialHeight:"70px", width:"100%", height:"90%"});
				$(".inline").colorbox({width:"98%"});
				$(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});

				$('.non-retina').colorbox({rel:'group5', transition:'none'})
				$('.retina').colorbox({rel:'group5', transition:'none', retinaImage:true, retinaUrl:true});
				
				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){ 
					$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
			});
		</script>
