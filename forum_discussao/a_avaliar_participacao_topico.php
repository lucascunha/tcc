<?php

 /*
 * Pagina de tópicos criados do ambiente aluno
 * @author Claudia da Rosa <clau.udesc@gmail.com>
 * @version 1.0 <30/09/2008>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como TCC - Trabalho de Conclusão de Curso
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */ 


global $id_aluno,$tipo,$logado,$num_disc,$num_curso,$id_prof,$disciplina, $curso;

$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;


include_once "include/conecta.php";

if ($logado<>"S"){
echo "<META HTTP-EQUIV='REFRESH' CONTENT=\"0; URL='./login.php'\">";
		}
else 
{
  
 //session_register("num_disc");
 //session_register("num_curso");
 //session_register("curso");
 //session_register("id_prof");
 //session_register("disciplina");
$num_disc       = $_SESSION['num_disc'];   
$num_curso      = $_SESSION['num_curso'];  
$curso          = $_SESSION['curso'];      
$id_prof        = $_SESSION['id_prof'];    
$disciplina     = $_SESSION['disciplina']; 

?>



<html>
<head><title>Adaptweb</title>

<script>
function formSubmit()
	{
		document.getElementById("aform1").submit();
	}
</script>
</head>

<body bgcolor="#ffffff" >


<?
	
if(($_POST['salvaravaliacao'] ) && ($_POST[ "id" ]))
{
	echo "<div id=\"loading\" > <br><br><br><center><img src=\"imagens/indicator.gif\" border=\"0\"><br>Aguarde...</center></div>";	
	$id_topico = $_POST["id"];
	
	$sql = mysql_query("SELECT MAX(id_participacao) AS maximo, MIN(id_participacao) AS minimo
						FROM participacao_topico_forum
						INNER JOIN usuario ON (participacao_topico_forum.id_usuario = usuario.id_usuario)
						WHERE id_topico = '$id_topico'");

	$valores = mysql_fetch_array($sql);
	
	for($i = $valores['minimo']; $i <= $valores['maximo']; $i++){
	
		if($_POST['qual_'.$i])
		{
		  $qual = $_POST['qual_'.$i];
		  $vetor = explode('_',$_POST["qual_".$i]);
		  // $vetor[0] -> id da participacao  	
		  // $vetor[1] -> valor da qualidade  
		  $StrSQL = "UPDATE participacao_topico_forum SET qualidade_informacao = ".$vetor[1]." WHERE id_participacao = ".$vetor[0]." ";
		  if(mysql_query($StrSQL))
		  {
			$ok=1;
			//$msg = "Avaliação Individual realizada com sucesso.";
		  }
		  else
		  {
            $ok=2;
            //$msg = "Problemas na Avaliação Individual.";
		  }
		}
	}
?>

<script>
<!--
top.location.replace('n_index_navegacao.php?opcao=AvaliarAlunosAutor&msg=<?php echo $msg; ?>&ok=<?php echo $ok;?>');

//-->
</script>

<?php
}
else
{



$id = $_GET[ "id" ];
$buscaTopico = mysql_query("SELECT topico_forum.titulo,usuario.nome_usuario,topico_forum.data,topico_forum.hora,topico_forum.descricao,topico_forum.id_topico FROM topico_forum INNER JOIN usuario ON (topico_forum.participacao = usuario.id_usuario) WHERE id_topico = '$id' ");
$t = mysql_fetch_array($buscaTopico);
$buscaParticipacao = mysql_query("SELECT usuario.id_usuario, usuario.nome_usuario,participacao_topico_forum.data,participacao_topico_forum.hora,participacao_topico_forum.resposta_topico,participacao_topico_forum.id_participacao,participacao_topico_forum.qualidade_informacao FROM participacao_topico_forum  INNER JOIN usuario ON (participacao_topico_forum.id_usuario = usuario.id_usuario) WHERE (id_topico = '$id') order by participacao_topico_forum.data,participacao_topico_forum.hora   ");
$numer = mysql_num_rows($buscaParticipacao); // verifica o número total de resultados de participações do tópico em questão.
//$p = mysql_fetch_array($buscaParticipacao);



?>

<table CELLSPACING=5 CELLPADDING=3 border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> >

	<tr valign=top>
	<td>
	<table CELLSPACING=0 CELLPADDING=15 width="100%"  border = "0"  bgcolor="#ffffff"  style="height:100%;">

	
<form method="post" name="aform1" id="aform1"  action="x_index_navegacao.php?opcao=AvaliarParticipacaoTopicoAutor"> 
<input type="hidden" name="id" value="<?php echo $id; ?>">
<input type="hidden" name="salvaravaliacao" value="teste">
	<tr valign=top>
		<td>
		<?
			$altera_data = explode('-',$t['data']);   
			$data= $altera_data[2]."/".$altera_data[1]."/".$altera_data[0];

			echo "<div align=\"left\">";	
			echo "<table CELLSPACING=\"0\" CELLPADDING=\"0\"  WIDTH=\"100%\" border=\"0\" class=\"tabela_redonda_topo\" >"; 
			echo "	<tr >";
			echo "		<td align=left class=\"redondo_final\" style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; font-size:20px; color:white; padding:10px 15px 10px 15px;\"  colspan=5>";
			echo 			$t['titulo'];
			echo "		</td>";
			echo "	</tr>";


			echo "<tr><td colspan=5><table border =0 cellspacing=0 cellpading=0>";
			echo "	<tr>";
			echo "		<td bgcolor=\"#ffffff\" valign=top >";
			echo "			<img src=\"imagens/avatar.png\" border=0 width=65>";
			echo "		</td>";			
			echo "		<td valign=top class=\"redondo_final\" style=\"background: #ffffff; border-width:0 1px 1px 0; border-color: #ffffff; border-style : solid; font-size:14px; color:black; padding:0 0 0 15px;\" ><br>";
			echo 			$t['descricao'];
			echo "		</td>";
			echo "	</tr>";
			echo "</table></td></tr>";

			echo "	<tr>";
			echo "		<td align=right bgcolor=\"#ffffff\" colspan=5>";
			echo "			<b><font color=black >Criado por ".strtoupper($t['nome_usuario'])."</b>&nbsp;( ".$data."&nbsp;- ".$t['hora']."h )&nbsp;&nbsp; </font>";
			echo "		</td>";
			echo "	</tr>";

			?>
						
			 <tr>
				<td colspan="5">&nbsp;</td>
			 </tr>
			 
			 
			 <tr>
			 <td>

			 <?
			 //Fazer while para imprimir as participações, ao lado de cada participação um campo para a qualidade de informação 

             if($numer <= 0 )
				{
				   		echo"<br>";
						echo "<table border=0  width=94%>"; 
					 	echo "<tr>";
						echo "<td rowspan=2><IMG src=\"imagens/erro.gif\"></td>";
						echo "<td><p class=\"texto3\">Não há participações para este tópico!</td>";
					  	echo "</tr>";
				   		echo "</table>";
				}
				else
				{
				      echo"<tr>";
                      echo utf8_decode("<td valign = middle width = 15% align=left class=\"redondo_inicio\" style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; font-size:14px; color:white; padding:10px 0 10px 10px;\" >Participante</td>");
                      echo utf8_decode("<td valign = middle width = 8% align=left style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; font-size:14px; color:white; padding:10px 0 10px 0;\" >Data</td>");
                      echo utf8_decode("<td valign = middle width = 8% align=left style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; font-size:14px; color:white; padding:10px 0 10px 0;\" >Hora</td>");
                      echo utf8_decode("<td valign = middle width = 40% align=left style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; font-size:14px; color:white; padding:10px 0 10px 0;\" >Resposta</td>");
					  echo utf8_decode("<td valign = middle width = 29% align=right class=\"redondo_final\" style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; font-size:14px; color:white; padding:10px 10px 10px 0;\">Qualidade da Resposta Publicada</td>");
					  echo"</tr>";
					  
					  
					  $cStyle = "zebraB";				 
					  while ($p = mysql_fetch_array($buscaParticipacao))
					  {
						   $idu = $p['id_usuario'];
						   if( $idu <> $id_prof)
						   {
						   		echo "<tr class=".$cStyle.">";
								$participacao==0;
						   		$participacao ++;
								/*XXXXXXXXXXXXXXXXXXTRABALHANDO A DATAXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
								$altera_data = explode('-',$p['data']);   
								$data= $altera_data[2]."/".$altera_data[1]."/".$altera_data[0];
								/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
								if ($p['qualidade_informacao'] == 1) $qual1 = "checked"; else
								if ($p['qualidade_informacao'] == 2) $qual2 = "checked"; else
								if ($p['qualidade_informacao'] == 3) $qual3 = "checked"; else
								if ($p['qualidade_informacao'] == 4) $qual4 = "checked";


								if ((strlen($p['resposta_topico'])) > 70)
								{
								$p['resposta_topico'] = substr($p['resposta_topico'], 0, 70). " ...";

								}  			

								echo ("<td class=\"redondo_inicio\"  align=left>".$p['nome_usuario']."</td><td>".$data."</td><td>".$p['hora']."</td><td>".$p['resposta_topico']."&nbsp; </td>");							
								echo "<td class=\"redondo_final\" align=right style='padding:10px 10px 10px 0;'>
								<input type=\"radio\" name=\"qual_".$p['id_participacao']."\" value=\"".$p['id_participacao']."_1\" $qual1> ".A_LANG_FORUM_QUAL_INS."
								<input type=\"radio\" name=\"qual_".$p['id_participacao']."\" value=\"".$p['id_participacao']."_2\" $qual2> ".A_LANG_FORUM_QUAL_REG."
								<input type=\"radio\" name=\"qual_".$p['id_participacao']."\" value=\"".$p['id_participacao']."_3\" $qual3> ".A_LANG_FORUM_QUAL_BOM."
								<input type=\"radio\" name=\"qual_".$p['id_participacao']."\" value=\"".$p['id_participacao']."_4\" $qual4> ".A_LANG_FORUM_QUAL_EXC."</td>";
								
								$qual1 = $qual2 = $qual3 = $qual4 = 0;
								
								$cStyle = $cStyle=="zebraA"?"zebraB":"zebraA";
							
								echo "</tr>";
							}
														
							
														
					  }//Fecha While
					  if( $participacao <= 0)
					  	{		
					  		echo "<tr class=".$cStyle.">";
							echo "<tr>";
							echo "<table CELLSPACING=\"5\" CELLPADDING=\"5\" border=\"0\" width=\"40%\">"; 
							echo "<td rowspan=2><IMG src=\"imagens/erro.gif\"></td>";
							echo "<td><p class=\"texto3\">".A_LANG_FORUM_AVAL_PARTIC."</td>";
							echo "</tr>";
							
							$cStyle = $cStyle=="zebraA"?"zebraB":"zebraA";
							echo "</tr>";
						}
					  

				
				if( $participacao > 0)
				{
				echo "<table CELLSPACING=\"0\" CELLPADDING=\"5\" BGCOLOR= \"#ffffff\" WIDTH=\"100%\" border=\"0\" class=\"tabela_redonda_topo\" >"; 
				?>
				<tr><td align = right>

					<br><br><input type="submit" class="buttonBigBig-01" value="<?php echo utf8_decode("Gravar Alterações");?>" name="gravar">
				</td></tr>
				<?php
				}	
				echo "</table>";			
				echo "</center>";
				echo "</div>";				

				
					  
				}
			 ?>
			 
			 
		</td>
		</tr>
     </table> 
	 </form>  
	 </td>
	</tr>

</td>
</tr>
</table>

</body>
</html>
<?php } }?>