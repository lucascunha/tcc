<?php

/*
 * Pagina de t�picos criados do ambiente professor
 * @author Claudia da Rosa <clau.udesc@gmail.com>
 * @version 1.0 <04/10/2008>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como TCC - Trabalho de Conclus�o de Curso
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */ 
 
 printf("<script type=\"text/javascript\"> 
   function excluir(id_topico){
       var answer = confirm(\"Voc� tem certeza que deseja excluir este T�pico?\")
	if (answer){
	   window.location.href='n_index_navegacao.php?opcao=ExcluirTopicoParticipacao&cod='+id_topico;
	}
   } 
 </script>");
  //window.location.href='n_index_navegacao.php?opcao=ParticiparTopicoDiscussaoAutor&id='+id;  
 

global $id_aluno,$tipo,$logado,$num_disc,$num_curso,$id_prof,$disciplina, $curso;

$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;

include_once "include/conecta.php";


if ($logado<>"S"){
echo "<META HTTP-EQUIV='REFRESH' CONTENT=\"0; URL='./login.php'\">";
		}
else {
  
$num_disc   = $_SESSION['num_disc'];  
$num_curso  = $_SESSION['num_curso'];  
$curso      = $_SESSION['curso'];    
$id_prof    = $_SESSION['id_prof'];     
$disciplina = $_SESSION['disciplina']; 

?> 

<html>
<head>
	<title>Adaptweb</title>
	<style type="text/css">
		#fixedtipdiv{
			position:absolute;
			padding: 2px;
			border:1px solid black;
			font:normal 11px Arial, Helvetica, sans-serif;
			line-height:18px;
			z-index:100;
		}
	</style>
</head>
<body bgcolor="#ffffff" link="#0000cd">


<? 
  $orelha = array();  
  $orelha = array(
  		array(   
   		   "LABEL" => "<a  class=menu href=n_index_navegacao.php?opcao=Navega&id_prof=".$id_prof."&num_disc=".$num_disc."&num_curso=".$num_curso."&disciplina=".$disciplina."&curso=".urlencode($curso).">".A_LANG_LENVIRONMENT_NAVTYPE."</a>", 
     		   "LINK" => "n_index_navegacao.php?opcao=Navega", 
     		   "ESTADO" =>"OFF"
   		    ),
		array(   
   		   "LABEL" => "<a  class=menu href=n_index_navegacao.php?opcao=MuralRecados&id_prof=".$id_prof."&num_disc=".$num_disc."&num_curso=".$num_curso."&disciplina=".$disciplina."&curso=".urlencode($curso).">".A_LANG_LENVIRONMENT_MURAL."</a>",  
     		   "LINK" => "n_index_navegacao.php?opcao=MuralRecados",    		                                 
     		   "ESTADO" => "OFF"
   		   ),
//Alteracao Claudia da Rosa (UDESC) em 13/09/2008 para F�rum de Discuss�o
		   array(   
   		   "LABEL" => A_LANG_LENVIRONMENT_FORUM,  
     		   "LINK" => "n_index_navegacao.php?opcao=ForumDiscussao",    		                                 
     		   "ESTADO" => "ON"
   		   ) 	     					     		       		   		   		       		   		   
   		  ); 

  MontaOrelha($orelha);  




$busca = mysql_query ("SELECT topico_forum.titulo,topico_forum.descricao,topico_forum.data,topico_forum.id_disciplina,topico_forum.id_curso,
topico_forum.hora,topico_forum.avaliacao,topico_forum.id_topico,usuario.nome_usuario,usuario.tipo_usuario,usuario.id_usuario,
Count(participacao_topico_forum.id_participacao) AS participacoes_de_um_topico
FROM topico_forum LEFT JOIN participacao_topico_forum USING (id_topico)
INNER JOIN usuario ON (topico_forum.participacao = usuario.id_usuario)
WHERE topico_forum.id_curso = '$num_curso' AND topico_forum.id_disciplina ='$num_disc' 
Group By participacao_topico_forum.id_topico, topico_forum.titulo,topico_forum.descricao,topico_forum.data,
topico_forum.hora,topico_forum.avaliacao, topico_forum.id_topico ORDER BY data DESC, hora DESC");

$num = mysql_num_rows($busca); // verifica o n�mero total de resultados


 ?>  

<table CELLSPACING=5 CELLPADDING=3 border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?>  style="height:350px;">
<tr valign="top">
<td>      
	<table CELLSPACING=0 CELLPADDING=0 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; border-left:3px; border-top:3px; border-bottom:3px; border-right:3px; border-style:solid; border-color:#eeeeee;" >	
	<tr>
		<td>
		<? 
		  $orelha = array();  
		  $orelha = array(
		 
				array(   
		   		   "LABEL" => 'F�rum',  
		     		   "LINK" => "n_index_navegacao.php?opcao=ForumDiscussao",    		                                 
		     		   "ESTADO" => "ON"
		   		   ), 			     		       		   		   		       		   		   
				   array(   
		   		   "LABEL" => "Criar T�pico de Discuss�o",  
		     		   "LINK" => "n_index_navegacao.php?opcao=CriaTopicoDiscussao",    		                                 
		     		   "ESTADO" => "OFF"
		   		   ),
				array(   
		   		   "LABEL" => 'Avalia��o Individual do Aluno',  
		     		   "LINK" => "n_index_navegacao.php?opcao=AvaliarAlunosAutor",    		                                 
		     		   "ESTADO" => "OFF"
		   		   ), 			     		       		   		   		       		   		   
				   array(   
		   		   "LABEL" => "Avalia��o Formativa do Aluno",  
		     		   "LINK" => "n_index_navegacao.php?opcao=AvaliarAlunosAutorFormativa",    		                                 
		     		   "ESTADO" => "OFF"
		   		   )

		   		  ); 
		
		
		  MontaOrelha($orelha);  
		
		?>
		</td>
	</tr>
	<tr >
	<td >
 <table CELLSPACING=0 CELLPADDING=25 width="100%"  border = "0"  bgcolor="#ffffff" >
  <tr  >
   <td >
	<table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0"  bgcolor="#ffffff" >	
	 <tr >
		<td >
<div class="Mensagem_Gray">
  <h2 style="padding-top:0px; padding-left:10px;">
		<? 
		echo A_LANG_FORUM_DISC.$disciplina; 
		echo " ".A_LANG_LENVIRONMENT_FORCOURSE." "; 
		echo $curso;		
		?>
  </h2>
</div>	
		<br>
			<fieldset class="campo">
				<legend align="left">
					<font color="#000000">| <b><? echo A_LANG_FORUM_LISTTOP; ?></b> |</font>
				</legend>
			
		
				<?
				if($num <= 0 )
				{ ?>
								<br>
			   		           <div class="Mensagem_Amarelo">
			   		             <?
			   		             echo "<p class=\"texto1\">\n";     
			   		             echo "<table cellspacing=\"0\" cellpadding=\"0\">";
			   		             echo "  <tr>";
			   		             echo "    <td width=\"50\">";
			   		             echo "      <img src=\"imagens/icones/warning.png\" alt=\"\" />";
			   		             echo "    </td>";
			   		             echo "    <td valign=\"center\">";
			   		             
			   		             echo "N�o existem t�picos criados nos �ltimos dias!";
			   		             echo "    </td>";
			   		             echo "  </tr>";
			   		             echo "</table>";  
			   		             echo "</p>\n";      
			   		             ?>    
			   		           </div><br>
			   		    <?       					
						
				}
				else 
				
				{
				echo "<br>";	
				echo "<table CELLSPACING=\"0\" CELLPADDING=\"3\" WIDTH=\"100%\" border=\"0\">"; 
				echo"<tr >";
				echo"<td class=\"redondo_inicio\" style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;\" >Titulo do T�pico (N� Participantes)</td>";
				echo"<td style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;\" >Autor</td>";
				echo"<td style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;\" ><center>Data</center></td>";
		        echo"<td style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;\" ><center>Hora</center></td>";
				echo"<td style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;\" ><center>T�pico de Avalia��o</center></td>";
				echo"<td class=\"redondo_final\" style=\"background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; color:#ffffff;\" ><center>Excluir</center></td>";
				echo"</tr>";
			    			  
				  $cStyle = "zebraB";
				  while ($pesq = mysql_fetch_array($busca))
				  {			
							echo "<tr class=".$cStyle.">";	
					        if ((strlen($pesq['titulo'])) > 70)
					        {
								$pesq['titulo'] = substr($pesq['titulo'], 0, 60). " ...";

					        }  	
					    	if ((strlen($pesq['nome_usuario'])) > 11)
					        {
								$pesq['nome_usuario'] = substr($pesq['nome_usuario'], 0, 11). " ...";

					        }  	    


					   		echo "<td class=\"redondo_inicio\" align=left> <a href=\"n_index_navegacao.php?opcao=ParticiparTopicoDiscussaoAutor&id=".$pesq['id_topico']."\"  class='tooltip' title='Acessar o T�pico'>".$pesq['titulo']."&nbsp;(".$pesq['participacoes_de_um_topico'].")</a></td>";
							
							/*XXXXXXXXXXXX TRABALHANDO O NEGRITO DO PROFESSOR XXXXXXXXXXXXXXXXXXXXXX*/
							$id = $pesq['id_usuario'];
							if( $id == $id_prof)
							{
								echo "<td align=left><b>".$pesq['nome_usuario']."</b></th></a></td>";}
							else
							{
								echo "<td align=left>".$pesq['nome_usuario']."</th></a></td>";}
							/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
								
							/*XXXXXXXXXXXXXXXXXX TRABALHANDO A DATAXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
							$altera_data = explode('-',$pesq['data']);   
							$data= $altera_data[2]."/".$altera_data[1]."/".$altera_data[0];
							echo "<td align=center>".$data."</a></td>"; 
							/*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
							
							echo "<td align=center>".$pesq['hora']."</a></td>";
							
							/*XXXXXXXXXXXXXXX TRABALHANDO A OP��O DE AVALIA��O XXXXXXXXXXXXXXXXXXX*/ 
							if($pesq['avaliacao']==1)
							{
								//if ($pesq['participacoes_de_um_topico']<=0)
								//{						
								//	echo "<td align=center width=13% >".A_LANG_FORUM_SIM."</td>";
								//}
								//else
								//{
								//	$avaliarAluno=1;
									echo "<td style=\"text-decoration:none\"; align=center width=13%> <a href=\"n_index_navegacao.php?opcao=AvaliarAlunosAutor\"  title=\"Avaliar Participa��o do Aluno\"  class='tooltip'><b>".A_LANG_FORUM_SIM."</b></a></td>";
									//}
							 }
							 else
							 {
									echo "<td align=center width=13%>".A_LANG_FORUM_NAO."</a></td>";}
							  /*XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX*/
													   
						      echo "<td class=\"redondo_final\" align=\"center\" th width=10%> <a onclick=\"excluir(".$pesq['id_topico'].");  \" style=cursor:pointer;><img  class='tooltip' title=\"Excluir T�pico\" src=\"imagens/excluir24.png\"></th></a> </td>"; 		
						   
						   
						   
						   $cStyle = $cStyle=="zebraA"?"zebraB":"zebraA";
						   echo "</tr>";
						   
				}//Fecha while
			echo "</table>";
			?>
				<br>
			 	</fieldset> 
			 	
			<? } ?> 
		</td>
		</tr>

	</table><br><br><br>
	</td>
</tr>
</table>
	</td>
</tr>
</table><br>
	</td>
</tr>
</table>
</body>
</html>
<?php } ?>