<?php

 /*
 * Pagina de t�picos criados do ambiente professor

 * @version 1.0 <04/10/2008>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como TCC - Trabalho de Conclus�o de Curso
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */ 
 



global $id_aluno,$tipo,$logado,$num_disc,$num_curso,$id_prof,$disciplina, $curso;

$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;

$num_disc   = $_SESSION['num_disc'];  
$num_curso  = $_SESSION['num_curso'];  
$curso      = $_SESSION['curso'];    
$id_prof    = $_SESSION['id_prof'];     
$disciplina = $_SESSION['disciplina']; 

include_once "include/conecta.php";

$criatopicoautor = $_POST['criatopicoautor'];

if(isset($criatopicoautor))
{
$tit = $_POST['titulo'];
$descr = $_POST['descricao'];
$part = $id_prof;
$aval = $_POST['aval'];
$data = date(Y)."-".date(m)."-".date(d);
$hora = date('H:i:s');

$max = "SELECT MAX(id_topico) as cod FROM topico_forum";
$answer = mysql_query($max,$id);
$num = mysql_fetch_row($answer);
if($num[0] == null){
$cod = 1;
}else{
	for($i=1; $i<=$num[0]; $i++){
		$look = "SELECT * FROM topico_forum WHERE id_topico='$i'";
		$answer = mysql_query($look,$id);
		$conf = mysql_fetch_row($answer);
		if($conf[0]==null)
		{
			$cod=$i;
			$i = $num[0]+1;
		}else{
			if($conf[0]==$i){
				$cod = $i+1;
			}
		}	
	}
}

if(($tit=="") && ($descr==" ") &&($aval==null))
{
	$ok=1;
}
else if(($tit=="") && ($descr==" "))
{
	$ok=7;
}
else if(($aval==null) && ($descr==" "))
{
	$ok=8;
}
else if(($aval==null) && ($tit==""))
{
	$ok=9;
}
else if($tit=="")
{
	$ok=2;
}
else if($descr==" ")
{
	$ok=3;
}
else if($aval==null)
{
	$ok=6;
}

else 
{

	$sql1 = "INSERT INTO topico_forum VALUES('$cod','$tit','$descr','$part','$aval','$data','$hora','$num_disc','$num_curso')";
	$res1 = mysql_query($sql1,$id);
	if($res1=!null)
	{
		$ok=4;
	}
	else
	{
	   $ok=5;
	}
}


			if($ok==1){
			$qt_erro++;	
			$mensagem = A_LANG_TOP_ALERT_ALL_PROF;
			}else if($ok==2){
			$qt_erro++;	
			$mensagem = A_LANG_TOP_ALERT_TITULO;
			}else if($ok==3){
			$qt_erro++;	
			$mensagem = A_LANG_TOP_ALERT_DESCR;
			}else if($ok==4){
			$qt_erro=0;	
			$mensagem = A_LANG_TOPICO_SUSS;
			}else if($ok==5){
			$qt_erro++;	
			$mensagem = A_LANG_TOPICO_INSUSS;
			}else if($ok==6){
			$qt_erro++;		
			$mensagem = A_LANG_TOP_ALERT_AVAL;
			}else if($ok==7){
			$qt_erro++;	
			$mensagem = A_LANG_TOP_ALERT_ALL;
			}else if($ok==8){
			$qt_erro++;	
			$mensagem = A_LANG_TOP_ALERT_ALL_AVAL_DESCR;
			}else if($ok==9){
			$qt_erro++;	
			$mensagem = A_LANG_TOP_ALERT_ALL_AVAL_TITULO;
			}

if ($mensagem != '')
{
 
      if (($qt_erro == 0))
      {   
        $tipo_msg = "success";
        $icone = "<img src='imagens/icones/success.png' border='0' />";
      }
      else
      { 
        $tipo_msg = "error";
        $icone = "<img src='imagens/icones/error.png' border='0' />";       
      }
   ?>
  <script type="text/javascript">
      showNotification({
        message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo $mensagem; ?></td></tr></table>",              
        type: "<? echo $tipo_msg; ?>",
        autoClose: true,
        duration: 8                                        
      });
      //error //success //warning
  </script>     

<?php } 

}



if ($logado<>"S"){
echo "<META HTTP-EQUIV='REFRESH' CONTENT=\"0; URL='./login.php'\">";
		}
else {
  
$num_disc   = $_SESSION['num_disc'];  
$num_curso  = $_SESSION['num_curso'];  
$curso      = $_SESSION['curso'];    
$id_prof    = $_SESSION['id_prof'];     
$disciplina = $_SESSION['disciplina']; 
 
 ?>



<html>
<head><title>Adaptweb</title>
<script>
function checaForm()
{ 	
    if ((document.forms[0].aval[0].checked == false) && (document.forms[0].aval[1].checked == false))
    {
    	alert ("Por favor, informe se o t�pico ser� para avalia��o do aluno.");
        return false;
    }    
    if (document.forms[0].titulo.value == '')
    {
    	alert ("Por favor, digite o t�tulo.");
        return false;
    }    
    if (document.forms[0].descricao.value == '')
    {
    	alert ("Por favor, digite a descri��o.");
        return false;
    }        
    document.forms[0].submit();
    return true;
}
</script>

</head>
<body bgcolor="#ffffff" link="#0000cd" onLoad="document.criar_topico.titulo.focus()">

<? 
  $orelha = array();  
  $orelha = array(
  		array(   
   		   "LABEL" => "<a  class=menu href=n_index_navegacao.php?opcao=Navega&id_prof=".$id_prof."&num_disc=".$num_disc."&num_curso=".$num_curso."&disciplina=".$disciplina."&curso=".urlencode($curso).">".A_LANG_LENVIRONMENT_NAVTYPE."</a>", 
     		   "LINK" => "n_index_navegacao.php?opcao=Navega", 
     		   "ESTADO" =>"OFF"
   		    ),
		array(   
   		   "LABEL" => "<a  class=menu href=n_index_navegacao.php?opcao=MuralRecados&id_prof=".$id_prof."&num_disc=".$num_disc."&num_curso=".$num_curso."&disciplina=".$disciplina."&curso=".urlencode($curso).">".A_LANG_LENVIRONMENT_MURAL."</a>",  
     		   "LINK" => "n_index_navegacao.php?opcao=MuralRecados",    		                                 
     		   "ESTADO" => "OFF"
   		   ),

		   array(   
   		   "LABEL" => A_LANG_LENVIRONMENT_FORUM,  
     		   "LINK" => "n_index_navegacao.php?opcao=ForumDiscussao",    		                                 
     		   "ESTADO" => "ON"
   		   ) 	     					     		       		   		   		       		   		   
   		  ); 

  MontaOrelha($orelha);  

?>

<table CELLSPACING=5 CELLPADDING=3 border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?>  style="height:350px;">
	<tr valign="top">
		<td>
	<table CELLSPACING=0 CELLPADDING=0 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; border-left:3px; border-top:3px; border-bottom:3px; border-right:3px; border-style:solid; border-color:#eeeeee;" >	
	<tr>
		<td>
		<? 
		  $orelha = array();  
		  $orelha = array(
		 
				array(   
		   		   "LABEL" => 'F�rum',  
		     		   "LINK" => "n_index_navegacao.php?opcao=ForumDiscussao",    		                                 
		     		   "ESTADO" => "OFF"
		   		   ), 			     		       		   		   		       		   		   
				   array(   
		   		   "LABEL" => "Criar T�pico de Discuss�o",  
		     		   "LINK" => "n_index_navegacao.php?opcao=CriaTopicoDiscussao",    		                                 
		     		   "ESTADO" => "ON"
		   		   ),
				array(   
		   		   "LABEL" => 'Avalia��o Individual do Aluno',  
		     		   "LINK" => "n_index_navegacao.php?opcao=AvaliarAlunosAutor",    		                                 
		     		   "ESTADO" => "OFF"
		   		   ), 			     		       		   		   		       		   		   
				   array(   
		   		   "LABEL" => "Avalia��o Formativa do Aluno",  
		     		   "LINK" => "n_index_navegacao.php?opcao=AvaliarAlunosAutorFormativa",    		                                 
		     		   "ESTADO" => "OFF"
		   		   )	     					  	     					     		       		   		   		       		   		   
		   		  ); 
		
		
		  MontaOrelha($orelha);  
		
		?>
		</td>
	</tr>
	<tr>
	<td>
 <table CELLSPACING=0 CELLPADDING=25 width="100%"  border = "0"  bgcolor="#ffffff" >
  <tr>
      <td>
	<table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0"  bgcolor="#ffffff">	
	<tr>
		<td>
<div class="Mensagem_Gray">
  <h2 style="padding-top:0px; padding-left:10px;">
		<? 
		echo A_LANG_FORUM_DISC.$disciplina; 
		echo " ".A_LANG_LENVIRONMENT_FORCOURSE." "; 
		echo $curso;		
		?>
  </h2>
</div>	
		<br>
			<fieldset class="campo">
				<legend align="left">
					<font color="#000000">| <b><? echo A_LANG_FORUM_CRIARTOPICODISCUSSAO.":"; ?></b> |</font>
				</legend>
				<br>
				
				
					<form name="criar_topico" method="post" name="form1" id="form1" action="n_index_navegacao.php?opcao=CriaTopicoDiscussao" onsubmit="return checaForm()">  
					<table align="left" border="0" width="100%" cellspacing=10> 
					<tr>
						<td style="width:100px" align="left">
							T�pico <? echo A_LANG_FORUM_AVAL?>
						</td>
						<td>
						<input type="radio" name="aval" value="1"><? echo A_LANG_FORUM_SIM; ?>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="aval" value="0"><? echo A_LANG_FORUM_NAO; ?>
					</td>
					
					</tr>
					
					<tr>
						<td  align="left">
							<? echo A_LANG_FORUM_TITULO ?>
						</td>	
						<td>
							<input class= "text" name="titulo" id="titulo" size="42" maxlength="80" style="width:600px"> 
						</td>
					</tr>
					
					<tr>
						<td align="left">
							<? echo A_LANG_FORUM_DESCR ?> 
						</td>
					
						<td>
							<script language="Javascript">
							function Contar(Campo){
							document.getElementById("Qtd").innerText = 7000-document.criar_topico.descricao.value.length  + ' caracteres'
							document.getElementById("Qtd").textContent = 7000-document.criar_topico.descricao.value.length  + ' caracteres'
							if((7000-document.criar_topico.descricao.value.length)<=0)
							alert("Aten��o, voc� atingiu o limite m�ximo de caracteres!");
							}
							</script>	


							<textarea class="button" name="descricao" id="descricao" style="width:600px" rows="10" onKeyUp="this.value = this.value.slice(0,7000); Contar(this)"></textarea>
							<div id="Qtd" align="right" style="width:600px">7000 caracteres</div>		
						</td>
					</tr>
					<tr>
						<td>
						</td>

						<td align="left">
							<INPUT class="buttonBig"  TYPE="reset" value="Limpar Campos" onClick="document.criar_topico.descricao.value=''; Contar(this)">  
					
									<script type="text/javascript">
										$(
											function() 
											{
											$("#customDialog01").easyconfirm
											(
												{	locale: 
													{
														title: '',
														text: '<br>Confirma a cria��o do T�pico de Discuss�o ?<br>',
														button: ['N�o',' Sim'],
														closeText: 'Fechar'
									
													}, dialog: $("#question01")
												}
												
											);
											$("#customDialog01").click(function() { //return checaForm(); 
											});
									
											}
										);
									</script>
									<input type="submit" class="buttonBig" name="criatopicoautor" value="Criar T�pico" id="customDialog01"><br><br>
										<div class="dialog01" id="question01">
											<table>
												<tr>
													<td width="50">
														<img src="imagens/icones/notice.png" alt="" />
													</td>
													<td valign="center">
														Confirma a cria��o do T�pico de Discuss�o ?
													</td>
												</tr>
											</table>	
										</div>							
						</td>
					</tr>
				</table>
				</form>
			</fieldset>
		</td>
	</tr>
</table>		</td>
	</tr>
</table>		</td>
	</tr>
</table><br>
		</td>
	</tr>
</table>
</body>
</html>
<?php } ?>