<?php

 /*
 * Pagina de t�picos criados do ambiente aluno
 * @author Claudia da Rosa <clau.udesc@gmail.com>
 * @version 1.0 <30/09/2008>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como TCC - Trabalho de Conclus�o de Curso
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */ 

global $id_aluno,$tipo,$logado,$num_disc,$num_curso,$id_prof,$disciplina, $curso;

$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;

include_once "include/conecta.php";

if(isset($criatopicoautor))
{
$tit = $_POST['titulo'];
$descr = $_POST['descricao'];
$part = $id_prof;
$aval = $_POST['aval'];
$data = date(Y)."-".date(m)."-".date(d);
$hora = date('H:i:s');

$max = "SELECT MAX(id_topico) as cod FROM topico_forum";
$answer = mysql_query($max,$id);
$num = mysql_fetch_row($answer);
if($num[0] == null){
$cod = 1;
}else{
	for($i=1; $i<=$num[0]; $i++){
		$look = "SELECT * FROM topico_forum WHERE id_topico='$i'";
		$answer = mysql_query($look,$id);
		$conf = mysql_fetch_row($answer);
		if($conf[0]==null)
		{
			$cod=$i;
			$i = $num[0]+1;
		}else{
			if($conf[0]==$i){
				$cod = $i+1;
			}
		}	
	}
}

if(($tit=="") && ($descr==" ") &&($aval==null))
{
	$ok=1;
}
else if(($tit=="") && ($descr==" "))
{
	$ok=7;
}
else if(($aval==null) && ($descr==" "))
{
	$ok=8;
}
else if(($aval==null) && ($tit==""))
{
	$ok=9;
}
else if($tit=="")
{
	$ok=2;
}
else if($descr==" ")
{
	$ok=3;
}
else if($aval==null)
{
	$ok=6;
}

else 
{

	$sql1 = "INSERT INTO topico_forum VALUES('$cod','$tit','$descr','$part','$aval','$data','$hora','$num_disc','$num_curso')";
	$res1 = mysql_query($sql1,$id);
	if($res1=!null)
	{
		$ok=4;
	}
	else
	{
	   $ok=5;
	}
}
}

if ($logado<>"S"){
echo "<META HTTP-EQUIV='REFRESH' CONTENT=\"0; URL='./login.php'\">";
		}
else {
  
 session_register("num_disc");
 session_register("num_curso");
 session_register("curso");
 session_register("id_prof");
 session_register("disciplina");
 
?>

<html>
<head><title>Adaptweb</title></head>
<body bgcolor="#ffffff" link="#0000cd">
<? 
  $orelha = array();  
  $orelha = array(
  		array(   
   		   "LABEL" => "<a  class=menu href=n_index_navegacao.php?opcao=Navega&id_prof=".$id_prof."&num_disc=".$num_disc."&num_curso=".$num_curso."&disciplina=".$disciplina."&curso=".urlencode($curso).">".A_LANG_LENVIRONMENT_NAVTYPE."</a>", 
     		   "LINK" => "n_index_navegacao.php?opcao=Navega", 
     		   "ESTADO" =>"OFF"
   		    ),
		array(   
   		   "LABEL" => "<a  class=menu href=n_index_navegacao.php?opcao=MuralRecados&id_prof=".$id_prof."&num_disc=".$num_disc."&num_curso=".$num_curso."&disciplina=".$disciplina."&curso=".urlencode($curso).">".A_LANG_LENVIRONMENT_MURAL."</a>",  
     		   "LINK" => "n_index_navegacao.php?opcao=MuralRecados",    		                                 
     		   "ESTADO" => "OFF"
   		   ),

		   array(   
   		   "LABEL" => A_LANG_LENVIRONMENT_FORUM,  
     		   "LINK" => "n_index_navegacao.php?opcao=ForumDiscussao",    		                                 
     		   "ESTADO" => "ON"
   		   ) 	     					     		       		   		   		       		   		   
   		  ); 

  MontaOrelha($orelha);  

?>
 <table CELLSPACING=5 CELLPADDING=3 border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> style="height:350px;">
	<tr valign="top">
		<td>
			<table CELLSPACING=0 CELLPADDING=0 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; border-left:3px; border-top:3px; border-bottom:3px; border-right:3px; border-style:solid; border-color:#eeeeee;" >	
			<tr>
				<td>
		<? 
		  $orelha = array();  
		  $orelha = array(
		 
				array(   
		   		   "LABEL" => 'F�rum',  
		     		   "LINK" => "n_index_navegacao.php?opcao=ForumDiscussao",    		                                 
		     		   "ESTADO" => "OFF"
		   		   ), 			     		       		   		   		       		   		   
				   array(   
		   		   "LABEL" => "Criar T�pico de Discuss�o",  
		     		   "LINK" => "n_index_navegacao.php?opcao=CriaTopicoDiscussao",    		                                 
		     		   "ESTADO" => "ON"
		   		   ) 	     					     		       		   		   		       		   		   
		   		  ); 
		
		
		  MontaOrelha($orelha);  
		
		?>
				</td>
			</tr>
			<tr>
			<td>
				<table CELLSPACING=0 CELLPADDING=0 width="97%"  border = "0"  bgcolor="#ffffff" style="margin-left: 30px; margin-top: 10px; " >	
				<tr>
					<td>
						<? echo "<h2>".A_LANG_FORUM_DISC.$disciplina; echo " ".A_LANG_LENVIRONMENT_FORCOURSE." "; echo $curso."</h2>";?>
					<? 
			if($ok==1){
			$icone = "error.png";
			$mensagem = A_LANG_TOP_ALERT_ALL_PROF;
			}else if($ok==2){
			$icone = "error.png";
			$mensagem = A_LANG_TOP_ALERT_TITULO;
			}else if($ok==3){
			$icone = "error.png";	
			$mensagem = A_LANG_TOP_ALERT_DESCR;
			}else if($ok==4){
			$icone = "success.png";	
			$mensagem = A_LANG_TOPICO_SUSS;
			}else if($ok==5){
			$icone = "error.png";	
			$mensagem = A_LANG_TOPICO_INSUSS;
			}else if($ok==6){
			$icone = "error.png";	
			$mensagem = A_LANG_TOP_ALERT_AVAL;
			}else if($ok==7){
			$icone = "error.png";	
			$mensagem = A_LANG_TOP_ALERT_ALL;
			}else if($ok==8){
			$icone = "error.png";	
			$mensagem = A_LANG_TOP_ALERT_ALL_AVAL_DESCR;
			}else if($ok==9){
			$icone = "error.png";	
			$mensagem = A_LANG_TOP_ALERT_ALL_AVAL_TITULO;
			}
			
		?>		<br>
		</td>
	</tr>
	<td>
				<fieldset style="width:800px;" class=campo>
				<legend align="left">
					<font color="#000000">| <b><? echo A_LANG_FORUM_CRIARTOPICODISCUSSAO; ?></b> |</font>
				</legend>
				<br>

			     <table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0">
			           <tr valign="top">
			             <td>  
			         
			               <div class="Mensagem_Ok">
			                 <?
			                   echo "<p class=\"texto1\">\n";     
			                 echo "<table cellspacing=\"0\" cellpadding=\"0\">";
			                 echo "  <tr >";
			                 echo "    <td width=\"50\">";
			                 echo "      <img src=\"imagens/icones/$icone\" alt=\"\" />";
			                 echo "    </td>";
			                 echo "    <td valign=\"center\">";
			                 
			                 echo $mensagem; 
			                 echo "    </td>";
			                 echo "  </tr>";
			                 echo "</table>";  
			                   echo "</p>\n";      
			                 ?>    
			               </div>
			               <br>
			               </td>
			             </tr>  
			             <tr><td>
			             	<input type="button" class="buttonBig" value="Voltar ao F�rum" onclick="window.location = 'n_index_navegacao.php?opcao=ForumDiscussao'">
							<input type="button" class="buttonBig" value="<?echo A_LANG_FORUM_OUTRO_TOPICO; ?>" onclick="window.location = 'n_index_navegacao.php?opcao=CriaTopicoDiscussao'">
						</td></tr>     

			     </table>  
			    </fieldset> 
<br>
			

	</td>
	</tr>
</table>	
<br>


</td>
	</tr>

</table>
<br>
</td>
	</tr>
</table>

</body>
</html>
<?php }?>

