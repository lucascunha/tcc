<?php
	$_PROFESSOR = $_SESSION['id_usuario'];
	$_CURSO = ($_POST['curso'] > 0) ? $_POST['curso'] : '%';
	$_DISCIPLINA = ($_POST['disciplina'] > 0) ? $_POST['disciplina'] : '%';
	$_PER_DE = ($_POST['per_de'] > 0) ? $_POST['per_de'] : date(A_LANG_FOR_DA_DATA, strtotime('3 months ago'));
	$_PER_ATE = ($_POST['per_ate'] > 0) ? $_POST['per_ate'] : date(A_LANG_FOR_DA_DATA);
	$_METRICAS = ($_POST['metricas']) ? $_POST['metricas'] : array('tot_de_vis_por_alu');
?>

<form action="#" method="post">
	<div class="bordas mar_int_10">
		<div class="mar_ext_inf_10">
			<!-- CURSO -->
			<strong><?=A_LANG_CURSO; ?>:</strong>
			<select name="curso" class="slt">
				<option value="0"><?=A_LANG_TOD_OS_CUR; ?></option>
				<?php
				$sql = 'SELECT id_curso AS id, nome_curso AS nome FROM curso WHERE Id_usuario = '.  $_PROFESSOR;
				$cursos = consulta($sql);
				if(tot_res($cursos) > 0) {
					while($curso = objeto($cursos)) {
						$selecionada = ($curso->id == $_CURSO) ? 'selected="selected"' : '';
				?>
				<option <?=$selecionada; ?> value="<?=$curso->id; ?>"><?=$curso->nome; ?></option>
				<?php
					}
				}
				?>
			</select>
			<!-- / CURSO -->
			
			<!-- DISCIPLINA -->
			<strong><?=A_LANG_DISCIPLINA; ?>:</strong>
			<select name="disciplina" class="slt">
				<option value="0"><?=A_LANG_TOD_AS_DIS; ?></option>
				<?php
				$sql = 'SELECT id_disc AS id, nome_disc AS nome FROM disciplina WHERE Id_usuario = '.  $_PROFESSOR;
				$disciplinas = consulta($sql);
				if(tot_res($disciplinas) > 0) {
					while($disciplina = objeto($disciplinas)) {
						$selecionada = ($disciplina->id == $_DISCIPLINA) ? 'selected="selected"' : '';
				?>
				<option <?=$selecionada; ?> value="<?=$disciplina->id; ?>"><?=$disciplina->nome; ?></option>
				<?php
					}
				}
				?>
			</select>
			<!-- / DISCIPLINA -->
			
			<!-- PERÍODO -->
			<strong><?=A_LANG_PERIODO; ?>:</strong>
			<?=A_LANG_PER_DE; ?>
			<input type="text" name="per_de" class="data txt" size="10" maxlength="10" value="<?=$_PER_DE; ?>" />
			<?=A_LANG_PER_ATE; ?>
			<input type="text" name="per_ate" class="data txt" size="10" maxlength="10" value="<?=$_PER_ATE; ?>" />
			<!-- / PERÍODO -->
		</div>
		
		<!-- MÉTRICAS PARA SELEÇÃO -->
		<div id="met_ocu">
			<p class="mar_ext_inf_10"><span class="met_sit"><img src="<?=$_CAMINHO; ?>img/mais.png" title="" width="12" height="12" /> <?=A_LANG_MOS_MET; ?></span>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span id="met_sel">oi</span></p>
		</div>
		
		<div id="met_vis" class="mar_ext_inf_10">
			<p class="mar_ext_inf_10"><span class="met_sit"><img src="<?=$_CAMINHO; ?>img/menos.png" title="" width="12" height="12" /> <?=A_LANG_OCU_MET; ?></span></p>
			<div class="fun_azul mar_int_10">
				<a href="#" id="des_met"><?=A_LANG_DES_MET; ?></a>
				
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td class="mar_int_dir_20" valign="top">
							<p class="mar_ext_inf_10"><span id="uso_ger" class="categoria"><?=A_LANG_USO_GER; ?></span><br/></p>
							<? $marcado = (in_array('tot_de_vis_por_alu', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="tot_de_vis_por_alu" rel="#tot_de_vis_por_alu" /> <span id="tot_de_vis_por_alu" rel="#uso_ger"><?=A_LANG_TOT_DE_VIS_POR_ALU; ?></span></label><br/>
							<? $marcado = (in_array('tem_med_de_ace_dos_alu', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="tem_med_de_ace_dos_alu" rel="#tem_med_de_ace_dos_alu" /> <span id="tem_med_de_ace_dos_alu" rel="#uso_ger"><?=A_LANG_TEM_MED_DE_ACE_DOS_ALU; ?></span></label><br/>
							<? $marcado = (in_array('fre_de_ace', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="fre_de_ace" rel="#fre_de_ace" /> <span id="fre_de_ace" rel="#uso_ger"><?=A_LANG_FRE_DE_ACE; ?></span></label><br/>
							<? $marcado = (in_array('sis_ope', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<? $marcado = (in_array('tot_de_ace_a_cada_sec', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="tot_de_ace_a_cada_sec" rel="#tot_de_ace_a_cada_sec" /> <span id="tot_de_ace_a_cada_sec" rel="#uso_ger"><?=A_LANG_TOT_DE_ACE_A_CADA_SEC; ?></span></label><br/>
							<? $marcado = (in_array('sis_ope', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="sis_ope" rel="#sis_ope" /> <span id="sis_ope" rel="#uso_ger"><?=A_LANG_SIS_OPE; ?></span></label><br/>
							<? $marcado = (in_array('navegadores', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="navegadores" rel="#navegadores" /> <span id="navegadores" rel="#uso_ger"><?=A_LANG_NAVEGADORES; ?></span></label><br/>
							<? $marcado = (in_array('res_de_tela', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="res_de_tela" rel="#res_de_tela" /> <span id="res_de_tela" rel="#uso_ger"><?=A_LANG_RES_DE_TELA; ?></span></label>				
						</td>
						<td class="mar_int_dir_20" valign="top">
							<p class="mar_ext_inf_10"><span id="amb_de_aula" class="categoria"><?=A_LANG_AMB_DE_AULA; ?></span></p>
							<? $marcado = (in_array('modo_de_nav', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="modo_de_nav" rel="#modo_de_nav" /> <span id="modo_de_nav" rel="#amb_de_aula"><?=A_LANG_MODO_DE_NAV; ?></span></label><br/>
							<? $marcado = (in_array('tot_de_usos_do_sis_de_bus', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="tot_de_usos_do_sis_de_bus" rel="#tot_de_usos_do_sis_de_bus" /> <span id="tot_de_usos_do_sis_de_bus" rel="#amb_de_aula"><?=A_LANG_TOT_DE_USOS_DO_SIS_DE_BUS; ?></span></label><br/>
							<? $marcado = (in_array('pal_pes', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="pal_pes" rel="#pal_pes" /> <span id="pal_pes" rel="#amb_de_aula"><?=A_LANG_PAL_PES; ?></span></label><br/>
							<? $marcado = (in_array('tot_de_ace_aos_con', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="tot_de_ace_aos_con" rel="#tot_de_ace_aos_con" class="gru_1" /> <span id="tot_de_ace_aos_con" rel="#amb_de_aula"><?=A_LANG_TOT_DE_ACE_AOS_CON; ?></span></label><br/>
							<? $marcado = (in_array('tot_de_ace_aos_exer', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="tot_de_ace_aos_exer" rel="#tot_de_ace_aos_exer" class="gru_1" /> <span id="tot_de_ace_aos_exer" rel="#amb_de_aula"><?=A_LANG_TOT_DE_ACE_AOS_EXER; ?></span></label><br/>
							<? $marcado = (in_array('tot_de_ace_aos_exem', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="tot_de_ace_aos_exem" rel="#tot_de_ace_aos_exem" class="gru_1" /> <span id="tot_de_ace_aos_exem" rel="#amb_de_aula"><?=A_LANG_TOT_DE_ACE_AOS_EXEM; ?></span></label><br/>
							<? $marcado = (in_array('tot_de_ace_aos_mat_com', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="tot_de_ace_aos_mat_com" rel="#tot_de_ace_aos_mat_com" class="gru_1" /> <span id="tot_de_ace_aos_mat_com" rel="#amb_de_aula"><?=A_LANG_TOT_DE_ACE_AOS_MAT_COM; ?></span></label>
						</td>
						<td class="mar_int_dir_20" valign="top">
							<p class="mar_ext_inf_10"><span id="for_de_dis" class="mar_ext_inf_10 categoria"><?=A_LANG_FOR_DE_DIS; ?></span></p>
							<? $marcado = (in_array('tot_de_ace_aos_top', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="tot_de_ace_aos_top" rel="#tot_de_ace_aos_top"  class="gru_2" /> <span id="tot_de_ace_aos_top" rel="#for_de_dis"><?=A_LANG_TOT_DE_ACE_AOS_TOP; ?></label><br/>
							<? $marcado = (in_array('tot_de_top_cri', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="tot_de_top_cri" rel="#tot_de_top_cri" /> <span id="tot_de_top_cri" rel="#for_de_dis"><?=A_LANG_TOT_DE_TOP_CRI; ?></label><br/>
							<? $marcado = (in_array('tot_de_res', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="tot_de_res" rel="#tot_de_res"  class="gru_2" /> <span id="tot_de_res" rel="#for_de_dis"><?=A_LANG_TOT_DE_RES; ?></label>
						</td>
						<td valign="top">
							<p class="mar_ext_inf_10"><span id="mur_de_rec" class="categoria"><?=A_LANG_MUR_DE_REC; ?></span></p>
							<? $marcado = (in_array('tot_de_rec_env', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="tot_de_rec_env" rel="#tot_de_rec_env" /> <span id="tot_de_rec_env" rel="#mur_de_rec"><?=A_LANG_TOT_DE_REC_ENV; ?></span></label><br/>
							<? $marcado = (in_array('tip_de_rec_env', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="tip_de_rec_env" rel="#tip_de_rec_env" /> <span id="tip_de_rec_env" rel="#mur_de_rec"><?=A_LANG_TIP_DE_REC_ENV; ?></span></label><br/>
							<? $marcado = (in_array('tot_de_vis', $_METRICAS)) ? 'checked="checked"' : ''; ?>
							<label><input type="checkbox" <?=$marcado; ?> name="metricas[]" value="tot_de_vis" rel="#tot_de_vis" /> <span id="tot_de_vis" rel="#mur_de_rec"><?=A_LANG_TOT_DE_VIS; ?></span></label>
						</td>
					</tr>			
				</table>
			</div>
		</div>
		<!-- / MÉTRICAS PARA SELEÇÃO -->
		<div class="a_dir">
			<input type="submit" name="submit" value="<?=A_LANG_GER_GRA_PARA_ANA; ?>" class="btn" />
		</div>
		<div class="no_ini"></div>
	</div>
</form>