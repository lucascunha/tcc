<?php
	if($titulo) {
		echo '<p class="titulo">', A_LANG_USO_GER, ' - ', A_LANG_NAVEGADORES, '</p>';
	}
?>

<table class="datatable" cellspacing="0" cellpadding="0">
	<thead>
		<th><?=A_LANG_NAVEGADOR; ?></th>
		<th><?=A_LANG_VERSAO; ?></th>
		<th><?=A_LANG_TOT_DE_ACE; ?></th>
	</thead>
	<tbody>
		<? foreach($dados as $linha) { ?>
		<tr>
			<td><?=$linha['config_browser_name']; ?></td>
			<td><?=$linha['config_browser_version']; ?></td>
			<td><?=str_replace('%1%', $linha['total_browser'], A_LANG_ACESSOS); ?></td>
		</tr>
		<? } ?>
	</tbody>
</table>
<div class="no_ini mar_ext_inf_20"></div>