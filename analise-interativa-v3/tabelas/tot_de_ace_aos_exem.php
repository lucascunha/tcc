<?php
	if($titulo) {
		echo '<p class="titulo">', A_LANG_AMB_DE_AULA, ' - ', A_LANG_TOT_DE_ACE_AOS_EXEM, '</p>';
	}
?>

<table class="datatable" cellspacing="0" cellpadding="0">
	<thead>
		<th><?=A_LANG_CURSO; ?></th>
		<th><?=A_LANG_DISCIPLINA; ?></th>
		<th><?=A_LANG_EXEMPLO; ?></th>
		<th><?=A_LANG_TOT_DE_ACE; ?></th>
	</thead>	
	<tbody>
		<? foreach($dados as $linha) { ?>
		<tr>
			<td>
				<?
				if($linha['id_curso'] > 0) {
					echo nom_cur($linha['id_curso']);
				}
				?>
			</td>
			<td>
				<?
				if($linha['id_disc'] > 0) {
					echo nom_dis($linha['id_disc']);
				}
				?>
			</td>
			<td><?=$linha['exemplo']; ?></td>
			<td><?=str_replace('%1%', $linha['TOTAL_VIEWS'], A_LANG_ACESSOS); ?></td>
		</tr>
		<? } ?>
	</tbody>
</table>
<div class="no_ini mar_ext_inf_20"></div>