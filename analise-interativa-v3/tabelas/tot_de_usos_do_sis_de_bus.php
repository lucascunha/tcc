<?php
	if($titulo) {
		echo '<p class="titulo">', A_LANG_AMB_DE_AULA, ' - ', A_LANG_TOT_DE_USOS_DO_SIS_DE_BUS, '</p>';
	}
?>

<table class="datatable" cellspacing="0" cellpadding="0">
	<thead>
		<th><?=A_LANG_ALUNO; ?></th>
		<th><?=A_LANG_CURSO; ?></th>
		<th><?=A_LANG_DISCIPLINA; ?></th>
		<th><?=A_LANG_TOT_DE_BUS; ?></th>
	</thead>
	<tbody>
		<? foreach($dados as $linha) { ?>
		<tr>
			<td><?=$linha['nome_usuario']; ?></td>
			<td><?=$linha['nome_curso']; ?></td>
			<td><?=$linha['nome_disc']; ?></td>
			<td><?=str_replace('%1%', $linha['TOTAL_VIEWS'], A_LANG_BUSCAS); ?></td>
		</tr>
		<? } ?>
	</tbody>
</table>
<div class="no_ini mar_ext_inf_20"></div>