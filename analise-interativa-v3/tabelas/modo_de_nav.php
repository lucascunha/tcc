<?php
	if($titulo) {
		echo '<p class="titulo">', A_LANG_AMB_DE_AULA, ' - ', A_LANG_MODO_DE_NAV, '</p>';
	}
?>

<table class="datatable" cellspacing="0" cellpadding="0">
	<thead>
		<th><?=A_LANG_CURSO; ?></th>
		<th><?=A_LANG_DISCIPLINA; ?></th>
		<th><?=A_LANG_MODO_DE_NAV; ?></th>
		<th><?=A_LANG_TOT_DE_ACE; ?></th>
		<th><?=A_LANG_TEM_TOT_DE_ACE; ?></th>
	</thead>
	<tbody>
		<? 
		foreach($dados as $linha) { 
			$linha['secao'] = str_replace('Ambiente Aula - ', '', $linha['secao']); 
			if($linha['secao'] == 'Modo Livre' || $linha['secao'] == 'Modo Tutorial') {
				$modo = '';
				if($linha['secao'] == 'Modo Livre') $modo = A_LANG_MODO_LIV;
				if($linha['secao'] == 'Modo Tutorial') $modo = A_LANG_MODO_TUT;
		?>
		<tr>
			<td><?=$linha['nome_curso']; ?></td>
			<td><?=$linha['nome_disc']; ?></td>
			<td><?=$modo; ?></td>
			<td><?=str_replace('%1%', $linha['TOTAL_VIEWS'], A_LANG_ACESSOS); ?></td>
			<td><?=str_replace('%1%', $linha['tempo_total'], A_LANG_HORAS); ?></td>
		</tr>
		<? 
			}
		} 
		?>
	</tbody>
</table>
<div class="no_ini mar_ext_inf_20"></div>