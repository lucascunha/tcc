<?php
	if($titulo) {
		echo '<p class="titulo">', A_LANG_MUR_DE_REC, ' - ', A_LANG_TIP_DE_REC_ENV, '</p>';
	}
?>

<table class="datatable" cellspacing="0" cellpadding="0">
	<thead>
		<th><?=A_LANG_CURSO; ?></th>
		<th><?=A_LANG_DISCIPLINA; ?></th>
		<th><?=A_LANG_TOT_DE_ACE; ?></th>
	</thead>				
	<tbody>
		<? foreach($dados as $linha) { ?>
		<tr>
			<td><?=$linha['nome_curso']; ?></td>
			<td><?=$linha['nome_disc']; ?></td>
			<td><?=str_replace('%1%', $linha['TOTAL_VIEWS'], A_LANG_ACESSOS); ?></td>
		</tr>
		<? } ?>
	</tbody>
</table>
<div class="no_ini mar_ext_inf_20"></div>