<?php
	if($titulo) {
		echo '<p class="titulo">', A_LANG_USO_GER, ' - ', A_LANG_TEM_MED_DE_ACE_DOS_ALU, '</p>';
	}
?>

<table class="datatable" cellspacing="0" cellpadding="0">
	<thead>
		<th><?=A_LANG_ALUNO; ?></th>
		<th><?=A_LANG_CURSO; ?></th>
		<th><?=A_LANG_DISCIPLINA; ?></th>
		<th><?=A_LANG_TEM_MED_DE_ACE; ?></th>
	</thead>
	<tbody>
		<? foreach($dados as $linha) { ?>
		<tr>
			<td><?=$linha['nome_usuario']; ?></td>
			<td><?=$linha['nome_curso']; ?></td>
			<td><?=$linha['nome_disc']; ?></td>
			<td><?=str_replace('%1%', $linha['tempo_medio_total'], A_LANG_HORAS); ?></td>
		</tr>
		<? } ?>
	</tbody>
</table>
<div class="no_ini mar_ext_inf_20"></div>