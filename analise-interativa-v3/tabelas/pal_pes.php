<?php
	if($titulo) {
		echo '<p class="titulo">', A_LANG_AMB_DE_AULA, ' - ', A_LANG_PAL_PES, '</p>';
	}
?>

<table class="datatable" cellspacing="0" cellpadding="0">
	<thead>
		<th><?=A_LANG_PAL_PES_SIN; ?></th>
		<th><?=A_LANG_TOT_DE_BUS; ?></th>
	</thead>
	<tbody>
		<? foreach($dados as $linha) { ?>
		<tr>
			<td><?=urldecode($linha['keyword']); ?></td>
			<td><?=str_replace('%1%', $linha['TOTAL_VIEWS'], A_LANG_BUSCAS); ?></td>
		</tr>
		<? } ?>
	</tbody>
</table>
<div class="no_ini mar_ext_inf_20"></div>