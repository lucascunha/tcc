<?php
	if($titulo) {
		echo '<p class="titulo">', A_LANG_USO_GER, ' - ', A_LANG_FRE_DE_ACE, '</p>';
	}
?>

<div id="fre_de_ace_por_dia">
	<table class="datatable" cellspacing="0" cellpadding="0">
		<thead>
			<th><?=A_LANG_DIA; ?></th>
			<th><?=A_LANG_CURSO; ?></th>
			<th><?=A_LANG_DISCIPLINA; ?></th>
			<th><?=A_LANG_TOT_DE_ACE; ?></th>
		</thead>
		<tbody>
			<? foreach($dados['dia'] as $linha) { ?>
			<tr>
				<td><?=$linha['data']; ?></td>
				<td><?=$linha['nome_curso']; ?></td>
				<td><?=$linha['nome_disc']; ?></td>
				<td><?=str_replace('%1%', $linha['total_acessos'], A_LANG_ACESSOS); ?></td>
			</tr>
			<? } ?>
		</tbody>
	</table>
	<div class="no_ini mar_ext_inf_20"></div>
</div>

<div id="fre_de_ace_por_sem">
	<table class="datatable" cellspacing="0" cellpadding="0">
		<thead>
			<th><?=A_LANG_NUM_DA_SEM; ?></th>
			<th><?=A_LANG_SEMANA; ?></th>
			<th><?=A_LANG_CURSO; ?></th>
			<th><?=A_LANG_DISCIPLINA; ?></th>
			<th><?=A_LANG_TOT_DE_ACE; ?></th>
		</thead>
		<tbody>
			<? foreach($dados['semana'] as $linha) { ?>
			<tr>
				<td><?=$linha['num_semana']; ?></td>
				<td><?=$linha['semana']; ?></td>
				<td><?=$linha['nome_curso']; ?></td>
				<td><?=$linha['nome_disc']; ?></td>
				<td><?=str_replace('%1%', $linha['total_acessos'], A_LANG_ACESSOS); ?></td>
			</tr>
			<? } ?>
		</tbody>
	</table>
	<div class="no_ini mar_ext_inf_20"></div>
</div>

<div id="fre_de_ace_por_mes">
	<table class="datatable" cellspacing="0" cellpadding="0">
		<thead>
			<th><?=A_LANG_MES; ?></th>
			<th><?=A_LANG_CURSO; ?></th>
			<th><?=A_LANG_DISCIPLINA; ?></th>
			<th><?=A_LANG_TOT_DE_ACE; ?></th>
		</thead>
		<tbody>
			<? foreach($dados['mes'] as $linha) { ?>
			<tr>
				<td><?=$linha['mes']; ?></td>
				<td><?=$linha['nome_curso']; ?></td>
				<td><?=$linha['nome_disc']; ?></td>
				<td><?=str_replace('%1%', $linha['total_acessos'], A_LANG_ACESSOS); ?></td>
			</tr>
			<? } ?>
		</tbody>
	</table>
	<div class="no_ini mar_ext_inf_20"></div>
</div>