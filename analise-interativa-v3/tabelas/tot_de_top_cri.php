<?php
	if($titulo) {
		echo '<p class="titulo">', A_LANG_FOR_DE_DIS, ' - ', A_LANG_TOT_DE_TOP_CRI, '</p>';
	}
?>

<table class="datatable" cellspacing="0" cellpadding="0">
	<thead>
		<th><?=A_LANG_ALUNO; ?></th>
		<th><?=A_LANG_CURSO; ?></th>
		<th><?=A_LANG_DISCIPLINA; ?></th>
		<th><?=A_LANG_TOPICO; ?></th>
	</thead>	
	<tbody>
		<? foreach($dados as $linha) { ?>
		<tr>
			<td><?=$linha['nome_usuario']; ?></td>
			<td><?=$linha['nome_curso']; ?></td>
			<td><?=$linha['nome_disc']; ?></td>
			<td><?=$linha['titulo']; ?></td>
		</tr>
		<? } ?>
	</tbody>
</table>
<div class="no_ini mar_ext_inf_20"></div>