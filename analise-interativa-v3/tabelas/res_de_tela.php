<?php
	if($titulo) {
		echo '<p class="titulo">', A_LANG_USO_GER, ' - ', A_LANG_RES_DE_TELA, '</p>';
	}
?>

<table class="datatable" cellspacing="0" cellpadding="0">
	<thead>
		<th><?=A_LANG_RES_DE_TELA_SIN; ?></th>
		<th><?=A_LANG_TOT_DE_ACE; ?></th>
	</thead>
	<tbody>
		<? foreach($dados as $linha) { ?>
		<tr>
			<td><?=$linha['config_resolution']; ?></td>
			<td><?=str_replace('%1%', $linha['total_resolution'], A_LANG_ACESSOS); ?></td>
		</tr>
		<? } ?>
	</tbody>
</table>
<div class="no_ini mar_ext_inf_20"></div>