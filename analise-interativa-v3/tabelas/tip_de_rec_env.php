<?php
	if($titulo) {
		echo '<p class="titulo">', A_LANG_MUR_DE_REC, ' - ', A_LANG_TIP_DE_REC_ENV, '</p>';
	}
?>

<table class="datatable" cellspacing="0" cellpadding="0">
	<thead>
		<th><?=A_LANG_TIP_DO_REC; ?></th>
		<th><?=A_LANG_TOT_DE_REC_ENV; ?></th>
	</thead>			
	<tbody>
		<? foreach($dados as $linha) { ?>
		<tr>
			<td><?=$linha['tipo_recado']; ?></td>
			<td><?=str_replace('%1%', $linha['total_recados'], A_LANG_RECADOS); ?></td>
		</tr>
		<? } ?>
	</tbody>
</table>
<div class="no_ini mar_ext_inf_20"></div>