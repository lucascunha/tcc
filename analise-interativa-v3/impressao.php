<!-- CSS -->
<link rel="stylesheet" type="text/css" href="<?=$_CAMINHO; ?>css/analise-interativa.css" />
<link href="<?=$_CAMINHO; ?>js/datatables/media/css/jquery.dataTables.css" type="text/css" rel="stylesheet"/>
<!-- / CSS -->

<!-- JS -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=$_CAMINHO ?>js/masked-input.js"></script>
<script type="text/javascript" src="<?=$_CAMINHO ?>js/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$_CAMINHO ?>js/flot/excanvas.min.js"></script>
<script type="text/javascript" src="<?=$_CAMINHO ?>js/flot/jquery.flot.min.js"></script>
<script type="text/javascript" language="javascript" src="<?=$_CAMINHO ?>js/flot/jquery.flot.axislabels.js"></script>
<script type="text/javascript" language="javascript" src="<?=$_CAMINHO ?>js/flot/jquery.flot.symbol.js"></script>
<script type="text/javascript" language="javascript" src="<?=$_CAMINHO ?>js/flot/jquery.flot.orderBars.js"></script>
<script type="text/javascript" language="javascript" src="<?=$_CAMINHO ?>js/flot/jquery.flot.pie.min.js"></script>

<!-- FERRAMENTA DE ANÁLISE INTERATIVA -->
<!-- DESENVOLVIDO POR BARBARA MOISSA -->
<?php
ob_start();
session_start();

define('WA_PATH', '../');
include "../config/configuracoes.php";

// configurações da ferramenta
$_IMPRESSAO = true;
$_DIRETORIO = '';
$_CAMINHO = $DOCUMENT_SITE. '/analise-interativa-v3/';

// arquivos importantes
include_once $_DIRETORIO. 'funcoes.php';
include_once '../analytics/functions.php';

// define o idioma
if ($_SESSION['A_LANG_IDIOMA_USER'] == '')
	include '../idioma/'. $A_LANG_IDIOMA. '/geral.php';
else
	include '../idioma/'. $_SESSION['A_LANG_IDIOMA_USER'] .'/geral.php'; 

$naoharesultados = false;

$_PROFESSOR = $_SESSION['id_usuario'];
$_CURSO = ($_GET['curso'] > 0) ? $_GET['curso'] : '%';
$_DISCIPLINA = ($_GET['disciplina'] > 0) ? $_GET['disciplina'] : '%';
$_PER_DE = ($_GET['per_de'] > 0) ? $_GET['per_de'] : date(A_LANG_FOR_DA_DATA, strtotime('3 months ago'));
$_PER_ATE = ($_GET['per_ate'] > 0) ? $_GET['per_ate'] : date(A_LANG_FOR_DA_DATA);
$_METRICAS = ($_GET['metricas']) ? $_GET['metricas'] : array('tot_de_vis_por_alu');

$_GRAFICO = ($_GET['grafico'] != '') ? $_GET['grafico'] : 'barras';
$_AGR_POR = ($_GET['agr_por'] != '') ? $_GET['agr_por'] : 'mes';
$_LEGENDA = ($_GET['legenda'] != '') ? $_GET['legenda'] : 'true';

// Busca os resultados para cada uma das métricas
foreach($_METRICAS as $metrica) {
	$per_de = data_para_ban_de_dad($_PER_DE, A_LANG_FOR_DA_DATA);
	$per_ate = data_para_ban_de_dad($_PER_ATE, A_LANG_FOR_DA_DATA);
	$$metrica = dados($metrica, $_CURSO, $_DISCIPLINA, $per_de, $per_ate, $_PROFESSOR);
	
if($metrica == 'fre_de_ace') {
	if(empty($fre_de_ace['dia']) && empty($fre_de_ace['semana']) && empty($fre_de_ace['mes']))
		$naoharesultados = true;
	} else if(empty($$metrica)) {
		$naoharesultados = true;
	}
}

if(!$naoharesultados) {
?>

<div class="bordas mar_int_10 nao_imp mar_ext_inf_20">
	<strong><?=A_LANG_IMPRIMIR; ?>: </strong>
	<label><input type="checkbox" class="opc_de_imp" checked="checked" rel="#gra_com_res" /> <?=A_LANG_GRAFICO; ?></label>
	<label><input type="checkbox" class="opc_de_imp" checked="checked" rel="#tab_com_inf_det" /> <?=A_LANG_TAB_COM_INF_DET; ?></label>
	<input type="button" value="<?=A_LANG_IMPRIMIR; ?>" id="imprimir" class="btn" />
</div>

<div class="mar_ext_inf_20">
	<?
	if($_CURSO > 0) echo '<strong>', A_LANG_CURSO, ': </strong>', nom_cur($_CURSO), '<br />';
	if($_DISCIPLINA > 0) echo '<strong>', A_LANG_DISCIPLINA, ': </strong>', nom_dis($_DISCIPLINA), '<br />';
	echo '<strong>', A_LANG_PERIODO, ': </strong>', A_LANG_PER_DE, ' ', $_PER_DE, ' ', A_LANG_PER_ATE, ' ', $_PER_ATE;
	?>
</div>

<!-- GRÁFICO -->		
<div class="mar_ext_sup_10 mar_ext_inf_20" id="gra_com_res">
	<? include_once $_DIRETORIO. 'grafico.php'; ?>
</div>		
<!-- / GRÁFICO -->	

<!-- TABELA -->		
<div class="mar_ext_sup_10" id="tab_com_inf_det">
	<? include_once $_DIRETORIO. 'tabela.php'; ?>
</div>		
<!-- / TABELA -->

<? } else { ?>
<p id="semresultados"><?=A_LANG_SEM_RES; ?></p>
<? } ?>

<script>
	function mos_met() {
		$("#met_ocu").hide();
		$("#met_vis").show();
	}
	
	function ocu_met() {
		$("#met_vis").hide();
		$("#met_ocu").show();
		
		metricas = [];
		$("input:checkbox[name='metricas[]']:checked").each(function() {
			metrica = $($(this).attr("rel"));
			categoria = $(metrica.attr("rel"));
			metricas.push(categoria.html()+ " - " + metrica.html());
		});
		
		html = "<strong><?=A_LANG_MET_SEL; ?>:</strong> " + metricas.join('; ');
		$("#met_sel").html(html);		
	}

	$(function() {
		$("#imprimir").click(function() {
			window.print();
		});
		
		$(".opc_de_imp").click(function() {
			if($(this).is(":checked")) {
				$($(this).attr("rel")).show();
			} else {
				$($(this).attr("rel")).hide();
			}
		});
		
		// mostra as métricas ao carregar
		mos_met();
		
		// desabilita as métricas ao carregar a página
		$("input:checkbox[name='metricas[]']").attr("disabled", "disabled");
		$("input:checkbox[name='metricas[]']:checked").removeAttr("disabled");
		if($(".gru_1:checked").length > 0) {
			$(".gru_1").removeAttr("disabled");
		}
		if($(".gru_2:checked").length > 0) {
			$(".gru_2").removeAttr("disabled");
		}
		
		// mostra as métricas
		$("#met_ocu .met_sit").click(function() {
			mos_met();
		});
		
		// oculta as métricas
		$("#met_vis .met_sit").click(function() {
			ocu_met();
		});
		
		// máscara no formato __/__/____ para as datas
		$(".data").mask("99/99/9999");
		
		// desabilita todas as métricas
		$("input:checkbox[name='metricas[]']").click(function() {
			if($("input:checkbox[name='metricas[]']:checked").length == 0) {
				$("input:checkbox[name='metricas[]']").removeAttr("disabled");
			} else {
				$("input:checkbox[name='metricas[]']").attr("disabled", "disabled");
				$(this).removeAttr("disabled");
			}
		});
		
		// habilita as metricas que podem ser cruzadas da categoria Ambiente de Aula
		$(".gru_1").click(function() {
			$(".gru_1").removeAttr("disabled");
		});
		
		// habilita as métricas que podem ser cruzadas da categoria Fórum de Discussão
		$(".gru_2").click(function() {
			$(".gru_2").removeAttr("disabled");
		});
		
		// deseleciona métricas
		$("#des_met").click(function(event) {
			event.preventDefault();
				$("input:checkbox[name='metricas[]']").removeAttr("disabled").removeAttr("checked");					
		});
		
		$(".datatable").dataTable({
			"oLanguage": {
				"sLengthMenu": "<?=A_LANG_sLengthMenu; ?>",
				"sZeroRecords": "<?=A_LANG_sZeroRecords; ?>",
				"sInfo": "<?=A_LANG_sInfo; ?>",
				"sInfoEmpty": "",
				"sInfoFiltered": "<?=A_LANG_sInfoFiltered; ?>",
				"sSearch": "<?=A_LANG_sSearch; ?>:"
			},
			"bPaginate": false
		});
	})
</script>
<!-- / JS-->