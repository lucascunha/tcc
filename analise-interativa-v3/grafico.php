<?php
$metrica = '';
$semlegendas = true;

if(count($_METRICAS) == 1) {
	$metrica = $_METRICAS[0];
	switch($metrica) {
		// USO GERAL
		case 'tot_de_vis_por_alu':
			$titulodaanalise = A_LANG_USO_GER. ': ' .A_LANG_TOT_DE_VIS_POR_ALU;
		
			$eixo_x = A_LANG_ALUNO;
			$eixo_y = A_LANG_TOT_DE_ACE;
			
			$dados = array();
			foreach($tot_de_vis_por_alu as $linha) {
				if(!isset($dados[$linha['id_usuario']])) {
					$dados[$linha['id_usuario']] = array(
						'nome_usuario' => $linha['nome_usuario'],
						'total_visitas' => 0
					);
				}
				$dados[$linha['id_usuario']]['total_visitas'] += $linha['total_visitas'];
			}
			
			$linhas = linhas($dados, 'total_visitas', 'nome_usuario');
			$barras = barras($dados, 'total_visitas', 'nome_usuario', A_LANG_TOT_DE_VIS_POR_ALU);
			$setores = setores($dados, 'total_visitas', 'nome_usuario');
		break;
		case 'tem_med_de_ace_dos_alu':
			$titulodaanalise = A_LANG_USO_GER. ': ' .A_LANG_TEM_MED_DE_ACE_DOS_ALU;
			$eixo_x = A_LANG_ALUNO;
			$eixo_y = A_LANG_TEM_MED_DE_ACE;
			
			$length = count($tem_med_de_ace_dos_alu);
			for($ct = 0; $ct < $length; $ct++)
				$tem_med_de_ace_dos_alu[$ct]['tempo_medio'] = numero($tem_med_de_ace_dos_alu[$ct]['tempo_medio']);
			
			$linhas = linhas($tem_med_de_ace_dos_alu, 'tempo_medio', 'nome_usuario');
			$barras = barras($tem_med_de_ace_dos_alu, 'tempo_medio', 'nome_usuario', A_LANG_TEM_MED_DE_ACE_DOS_ALU);
			$setores = setores($tem_med_de_ace_dos_alu, 'tempo_medio', 'nome_usuario');
		break;
		case 'fre_de_ace':
			$titulodaanalise = A_LANG_USO_GER. ': ' .A_LANG_FRE_DE_ACE;
			$eixo_x = A_LANG_PERIODO;
			$eixo_y = A_LANG_TOT_DE_ACE;
			
			$agruparpor = true;
								
			$dadosparamapear = array(
				'dia' => array(),
				'semana' => array(),
				'mes' => array()
			);
			
			$dados = array();
			foreach($fre_de_ace['dia'] as $registro) {
				if(!isset($dados[$registro['data']])) {
					$dados[$registro['data']] = array(
						'data' => $registro['data'],
						'acessos' => 0
					);
				}
				$dados[$registro['data']]['acessos'] += $registro['total_acessos'];
			}	
			$dadosparamapear['dia'] = $dados;
			
			$dados = array();
			foreach($fre_de_ace['semana'] as $registro) {
				if(!isset($dados[$registro['semana']])) {
					$dados[$registro['semana']] = array(
						'semana' => $registro['semana'],
						'acessos' => 0
					);
				}
				$dados[$registro['semana']]['acessos'] += $registro['total_acessos'];
			}	
			$dadosparamapear['semana'] = $dados;
	
			$dados = array();
			foreach($fre_de_ace['mes'] as $registro) {
				if(!isset($dados[$registro['mes']])) {
					$dados[$registro['mes']] = array(
						'mes' => $registro['mes'],
						'acessos' => 0
					);
				}
				$dados[$registro['mes']]['acessos'] += $registro['total_acessos'];
			}	
			$dadosparamapear['mes'] = $dados;
		
			$linhas = array(
				'dia' => (linhas($dadosparamapear['dia'], 'acessos', 'data')),
				'semana' => (linhas($dadosparamapear['semana'], 'acessos', 'semana')),
				'mes' => (linhas($dadosparamapear['mes'], 'acessos', 'mes'))
			);
			$barras = array(
				'dia' => (barras($dadosparamapear['dia'], 'acessos', 'data')),
				'semana' => (barras($dadosparamapear['semana'], 'acessos', 'semana')),
				'mes' => (barras($dadosparamapear['mes'], 'acessos', 'mes'))
			);
			$setores = array(
				'dia' => setores($dadosparamapear['dia'], 'acessos', 'data'),
				'semana' => setores($dadosparamapear['semana'], 'acessos', 'semana'),
				'mes' => setores($dadosparamapear['mes'], 'acessos', 'mes')
			);
	
			$linhas['dia']['valores'] = array($linhas['dia']['valores']);
			$barras['dia']['valores'] = array($barras['dia']['valores']);
		
			$linhas['semana']['valores'] = array($linhas['semana']['valores']);
			$barras['semana']['valores'] = array($barras['semana']['valores']);	
		
			$linhas['mes']['valores'] = array($linhas['mes']['valores']);
			$barras['mes']['valores'] = array($barras['mes']['valores']);			
		break;
		case 'tot_de_ace_a_cada_sec':
			$titulodaanalise = A_LANG_USO_GER. ': ' .A_LANG_TOT_DE_ACE_A_CADA_SEC;
			$eixo_x = A_LANG_SECAO;
			$eixo_y = A_LANG_TOT_DE_ACE;
			
			$dados = array(
				array(
					'secao' => A_LANG_AMB_DE_AULA,
					'total' => 0
				),
				array(
					'secao' => A_LANG_FOR_DE_DIS,
					'total' => 0
				),
				array(
					'secao' => A_LANG_MUR_DE_REC,
					'total' => 0
				)
			);
			
			foreach($tot_de_ace_a_cada_sec as $registro) {
				if($registro['secao'] == 'Ambiente Aula - Modo Livre' || $registro['secao'] == 'Ambiente Aula - Modo Tutorial') {
					$dados[0]['total'] += $registro['TOTAL_VIEWS'];
				} elseif($registro['secao'] == 'Mural de Recados') {
					$dados[1]['total'] += $registro['TOTAL_VIEWS'];
				} elseif($registro['secao'] == 'Fórum de Discussão') {
					$dados[2]['total'] += $registro['TOTAL_VIEWS'];
				}
			}
			
			$linhas = linhas($dados, 'total', 'secao');
			$barras = barras($dados, 'total', 'secao', A_LANG_TOT_DE_ACE_A_CADA_SEC);
			$setores = setores($dados, 'total', 'secao');
		break;
		case 'sis_ope':
			$titulodaanalise = A_LANG_USO_GER. ': ' .A_LANG_SIS_OPE;
			$eixo_x = A_LANG_SIS_OPE_SIN;
			$eixo_y = A_LANG_TOT_DE_ACE;
			
			$linhas = linhas($sis_ope, 'total_os', 'config_os');
			$barras = barras($sis_ope, 'total_os', 'config_os', A_LANG_SIS_OPE);
			$setores = setores($sis_ope, 'total_os', 'config_os');
		break;
		case 'navegadores':
			$titulodaanalise = A_LANG_USO_GER. ': ' .A_LANG_NAVEGADORES;
			$eixo_x = A_LANG_NAVEGADOR;
			$eixo_y = A_LANG_TOT_DE_ACE;
			
			$linhas = linhas($navegadores, 'total_browser', 'config_browser_name');
			$barras = barras($navegadores, 'total_browser', 'config_browser_name', A_LANG_NAVEGADORES);
			$setores = setores($navegadores, 'total_browser', 'config_browser_name');
		break;
		case 'res_de_tela':
			$titulodaanalise = A_LANG_USO_GER. ': ' .A_LANG_RES_DE_TELA;
			$eixo_x = A_LANG_RES_DE_TELA_SIN;
			$eixo_y = A_LANG_TOT_DE_ACE;
			
			$linhas = linhas($res_de_tela, 'total_resolution', 'config_resolution');
			$barras = barras($res_de_tela, 'total_resolution', 'config_resolution', A_LANG_RES_DE_TELA);
			$setores = setores($res_de_tela, 'total_resolution', 'config_resolution');
		break;
		
		// AMBIENTE DE AULA
		case 'modo_de_nav':
			$titulodaanalise = A_LANG_AMB_DE_AULA. ': ' .A_LANG_MODO_DE_NAV;
			$eixo_x = A_LANG_MODO_DE_NAV;
			$eixo_y = A_LANG_TOT_DE_ACE;
		
			$dados = array(
				array(
					'modo' => A_LANG_MODO_LIV,
					'total' => 0
				),
				array(
					'modo' => A_LANG_MODO_TUT,
					'total' => 0
				)
			);
			
			foreach($modo_de_nav as $registro) {
				if($registro['secao'] == 'Ambiente Aula - Modo Livre') {
					$dados[0]['total'] += $registro['TOTAL_VIEWS'];
				} elseif($registro['secao'] == 'Ambiente Aula - Modo Tutorial') {
					$dados[1]['total'] += $registro['TOTAL_VIEWS'];
				}
			}
		
			$linhas = linhas($dados, 'total', 'modo');
			$barras = barras($dados, 'total', 'modo');
			$setores = setores($dados, 'total', 'modo');
		break;
		case 'tot_de_usos_do_sis_de_bus':
			$titulodaanalise = A_LANG_AMB_DE_AULA. ': ' .A_LANG_TOT_DE_USOS_DO_SIS_DE_BUS;
			$eixo_x = A_LANG_ALUNO;
			$eixo_y = A_LANG_TOT_DE_BUS;
			
			$linhas = linhas($tot_de_usos_do_sis_de_bus, 'TOTAL_VIEWS', 'nome_usuario');
			$barras = barras($tot_de_usos_do_sis_de_bus, 'TOTAL_VIEWS', 'nome_usuario', A_LANG_TOT_DE_USOS_DO_SIS_DE_BUS);
			$setores = setores($tot_de_usos_do_sis_de_bus, 'TOTAL_VIEWS', 'nome_usuario');
		break;
		case 'pal_pes':
			$titulodaanalise = A_LANG_AMB_DE_AULA. ': ' .A_LANG_PAL_PES;
			$eixo_x = A_LANG_PAL_PES_SIN;
			$eixo_y = A_LANG_TOT_DE_BUS;
			
			$linhas = linhas($pal_pes, 'TOTAL_VIEWS', 'keyword');
			$barras = barras($pal_pes, 'TOTAL_VIEWS', 'keyword', A_LANG_PAL_PES);
			$setores = setores($pal_pes, 'TOTAL_VIEWS', 'keyword');
		break;
		case 'tot_de_ace_aos_con':
			$titulodaanalise = A_LANG_AMB_DE_AULA. ': ' .A_LANG_TOT_DE_ACE_AOS_CON;
			$eixo_x = A_LANG_CONCEITO;
			$eixo_y = A_LANG_TOT_DE_ACE;
			
			$linhas = linhas($tot_de_ace_aos_con, 'TOTAL_VIEWS', 'conceito');
			$barras = barras($tot_de_ace_aos_con, 'TOTAL_VIEWS', 'conceito', A_LANG_TOT_DE_ACE_AOS_CON);
			$setores = setores($tot_de_ace_aos_con, 'TOTAL_VIEWS', 'conceito');
		break;
		case 'tot_de_ace_aos_exer':
			$titulodaanalise = A_LANG_AMB_DE_AULA. ': ' .A_LANG_TOT_DE_ACE_AOS_EXER;
			$eixo_x = A_LANG_EXERCICIO;
			$eixo_y = A_LANG_TOT_DE_ACE;
			
			$linhas = linhas($tot_de_ace_aos_exer, 'TOTAL_VIEWS', 'exercicio');
			$barras = barras($tot_de_ace_aos_exer, 'TOTAL_VIEWS', 'exercicio', A_LANG_TOT_DE_ACE_AOS_EXER);
			$setores = setores($tot_de_ace_aos_exer, 'TOTAL_VIEWS', 'exercicio');
		break;
		case 'tot_de_ace_aos_exem':
			$titulodaanalise = A_LANG_AMB_DE_AULA. ': ' .A_LANG_TOT_DE_ACE_AOS_EXEM;
			$eixo_x = A_LANG_EXEMPLO;
			$eixo_y = A_LANG_TOT_DE_ACE;
			
			$linhas = linhas($tot_de_ace_aos_exem, 'TOTAL_VIEWS', 'exemplo');
			$barras = barras($tot_de_ace_aos_exem, 'TOTAL_VIEWS', 'exemplo', A_LANG_TOT_DE_ACE_AOS_EXEM);
			$setores = setores($tot_de_ace_aos_exem, 'TOTAL_VIEWS', 'exemplo');
		break;
		case 'tot_de_ace_aos_mat_com':
			$titulodaanalise = A_LANG_AMB_DE_AULA. ': ' .A_LANG_TOT_DE_ACE_AOS_MAT_COM;		
			$eixo_x = A_LANG_MAT_COM;
			$eixo_y = A_LANG_TOT_DE_ACE;
			
			$linhas = linhas($tot_de_ace_aos_mat_com, 'TOTAL_VIEWS', 'material');
			$barras = barras($tot_de_ace_aos_mat_com, 'TOTAL_VIEWS', 'material', A_LANG_TOT_DE_ACE_AOS_MAT_COM);
			$setores = setores($tot_de_ace_aos_mat_com, 'TOTAL_VIEWS', 'material');
		break;
		
		// FÓRUM DE DISCUSSÃO
		case 'tot_de_ace_aos_top':
			$titulodaanalise = A_LANG_FOR_DE_DIS. ': ' .A_LANG_TOT_DE_ACE_AOS_TOP;
			$eixo_x = A_LANG_TOPICO;
			$eixo_y = A_LANG_TOT_DE_ACE;
			
			$linhas = linhas($tot_de_ace_aos_top, 'TOTAL_VIEWS', 'titulo');
			$barras = barras($tot_de_ace_aos_top, 'TOTAL_VIEWS', 'titulo', A_LANG_TOT_DE_ACE_AOS_TOP);
			$setores = setores($tot_de_ace_aos_top, 'TOTAL_VIEWS', 'titulo');
		break;
		case 'tot_de_top_cri':
			$titulodaanalise = A_LANG_FOR_DE_DIS. ': ' .A_LANG_TOT_DE_TOP_CRI;
			$eixo_x = A_LANG_ALUNO;
			$eixo_y = A_LANG_TOT_DE_TOP_CRI;
			
			$dados = array();	
			foreach($tot_de_top_cri as $registro) {
				if(!isset($dados[$registro['id_usuario']])) {
					$dados[$registro['id_usuario']] = array(
						'aluno' => $registro['nome_usuario'],
						'total' => 0
					);
				}
				$dados[$registro['id_usuario']]['total'] += 1;
			}
			
			$linhas = linhas($dados, 'total', 'aluno');
			$barras = barras($dados, 'total', 'aluno', A_LANG_TOT_DE_TOP_CRI);
			$setores = setores($dados, 'total', 'aluno');
		break;
		case 'tot_de_res':
			$titulodaanalise = A_LANG_FOR_DE_DIS. ': ' .A_LANG_TOT_DE_RES;
			$eixo_x = A_LANG_TOPICO;
			$eixo_y = A_LANG_TOT_DE_ACE;
			
			$dados = array();	
			foreach($tot_de_res as $registro) {
				if(!isset($dados[$registro['id_topico']])) {
					$dados[$registro['id_topico']] = array(
						'topico' => $registro['titulo'],
						'total' => 0
					);
				}
				$dados[$registro['id_topico']]['total'] += $registro['TOTAL_PARTICIPACOES'];
			}
			
			$linhas = linhas($dados, 'total', 'topico');
			$barras = barras($dados, 'total', 'topico', A_LANG_TOT_DE_RES);
			$setores = setores($dados, 'total', 'topico');
		break;
		
		// MURAL DE RECADOS
		case 'tot_de_rec_env':
			$titulodaanalise = A_LANG_MUR_DE_REC. ': ' .A_LANG_TOT_DE_REC_ENV;
			$eixo_x = A_LANG_ALUNO;
			$eixo_y = A_LANG_TOT_DE_REC_ENV;
			
			$dados = array();
			foreach($tot_de_rec_env as $registro) {
				if(!isset($dados[$registro['id_usuario']])) {
					$dados[$registro['id_usuario']] = array(
						'nome_usuario' => $registro['nome_usuario'],
						'total_recados' => 0
					);
				}
				$dados[$registro['id_usuario']]['total_recados'] += 1;
			}
			
			$linhas = linhas($dados, 'total_recados', 'nome_usuario');
			$barras = barras($dados, 'total_recados', 'nome_usuario', TOT_DE_REC_ENV);
			$setores = setores($dados, 'total_recados', 'nome_usuario');
		break;
		case 'tip_de_rec_env':
			$titulodaanalise = A_LANG_MUR_DE_REC. ': ' .A_LANG_TIP_DE_REC_ENV;
			$eixo_x = A_LANG_TIP_DO_REC;
			$eixo_y = A_LANG_TOT_DE_REC_ENV;
			
			$linhas = linhas($tip_de_rec_env, 'total_recados', 'tipo_recado');
			$barras = barras($tip_de_rec_env, 'total_recados', 'tipo_recado', A_LANG_TIP_DE_REC_ENV);
			$setores = setores($tip_de_rec_env, 'total_recados', 'tipo_recado');
		break;
		case 'tot_de_vis':
			$titulodaanalise = A_LANG_MUR_DE_REC. ': ' .A_LANG_TOT_DE_VIS;
			$eixo_x = A_LANG_DISCIPLINA;
			$eixo_y = A_LANG_TOT_DE_ACE;
			
			$linhas = linhas($tot_de_vis, 'TOTAL_VIEWS', 'nome_disc');
			$barras = barras($tot_de_vis, 'TOTAL_VIEWS', 'nome_disc', A_LANG_TOT_DE_VIS);
			$setores = setores($tot_de_vis, 'TOTAL_VIEWS', 'nome_disc');
		break;
	}
	
	if($metrica != 'fre_de_ace') {
		$linhas['valores'] = array($linhas['valores']);
		$barras['valores'] = array($barras['valores']);
	}
} else {
	$titulo = true;
	if($_METRICAS == array('tot_de_ace_aos_top', 'tot_de_res')) {
		$titulodaanalise = A_LANG_FOR_DE_DIS. ': ' .A_LANG_TOT_DE_ACE_AOS_TOP. ' x ' .A_LANG_TOT_DE_RES;
		$semlegendas = false;
		$eixo_x = A_LANG_TOPICO;
		$eixo_y = A_LANG_TOT_DE_ACE_RES;
		
		$dadosparamapear = array();
		$numero = 1;
		
		foreach($tot_de_ace_aos_top as $visualizacao) {
			$topico = $visualizacao['id_topico'];
			if(!isset($dadosparamapear[$topico])) {
				$dadosparamapear[$topico] = array(
					'numero' => $numero,
					'topico' => $visualizacao['titulo'],
					'respostas' => 0,
					'visualizacoes' => 0
				);
				$numero++;
			}
			$dadosparamapear[$topico]['visualizacoes'] += $visualizacao['TOTAL_VIEWS'];
		}
		
		foreach($tot_de_res as $resposta) {
			$topico = $resposta['id_topico'];
			if(!isset($dadosparamapear[$topico])) {
				$dadosparamapear[$topico] = array(
					'numero' => $numero,
					'topico' => $resposta['titulo'],
					'respostas' => 0,
					'visualizacoes' => 0
				);
				$numero++;
			}
			$dadosparamapear[$topico]['respostas'] += $resposta['TOTAL_PARTICIPACOES'];
		}
				
		foreach($dadosparamapear as $registro) {
				$totaldeacessos[] = array($registro['numero'], $registro['visualizacoes']);
				$totalderespostas[] = array($registro['numero'], $registro['respostas']);
				$rotulos[] = array($registro['numero'], $registro['topico']);
		}

		$valores = array(
			array(
				'label' => A_LANG_TOT_DE_ACE,
				'data' => $totaldeacessos,
				'bars' => array(
					'show' => true,
					'barWidth' => 0.4,
					'align' => "center",
					'fill' => 0.65,
					'order' => 1
				)
			),
			array(
				'label' => A_LANG_TOT_DE_RES,
				'data' => $totalderespostas,
				'bars' => array(
					'show' => true,
					'barWidth' => 0.4,
					'align' => "center",
					'fill' => 0.65,
					'order' => 2
				)
			)
		);
		
		$barras = array(
			'valores' => $valores,
			'rotulos' => $rotulos
		);
				
		$dadosparamapear = array(
			array(
				'metrica' => A_LANG_TOT_DE_ACE,
				'total' => somar($dadosparamapear, 'visualizacoes')
			),
			array(
				'metrica' => A_LANG_TOT_DE_RES,
				'total' => somar($dadosparamapear, 'respostas')
			)
		);
		$setores = setores($dadosparamapear, 'total', 'metrica');
		
		$valores = array(
			array(
				'label' => A_LANG_TOT_DE_ACE,
				'data' => $totaldeacessos,
				'points' => array(
					'symbol' => 'circle'
				)
			),
			array(
				'label' => A_LANG_TOT_DE_RES,
				'data' => $totalderespostas,
				'points' => array(
					'symbol' => 'triangle'
				)
			)
		);
		$linhas = array(
			'valores' => $valores,
			'rotulos' => $rotulos
		);
	} else {
		$metricasdaanalise = array();
		$eixo_x = A_LANG_CATEGORIA;
		$eixo_y = A_LANG_TOT_DE_ACE;
		
		$dadosparamapear = array();
		if(in_array('tot_de_ace_aos_con', $_METRICAS)) {
			$metricasdaanalise[] = A_LANG_TOT_DE_ACE_AOS_CON;
			$dadosparamapear[] = array(
				'secao' => A_LANG_CONCEITOS,
				'total' => somar($tot_de_ace_aos_con, 'TOTAL_VIEWS')
			);
		}
		if(in_array('tot_de_ace_aos_exer', $_METRICAS)) {
			$metricasdaanalise[] = A_LANG_TOT_DE_ACE_AOS_EXER;
			$dadosparamapear[] = array(
				'secao' => A_LANG_EXERCICIOS,
				'total' => somar($tot_de_ace_aos_exer, 'TOTAL_VIEWS')
			);
		}
		if(in_array('tot_de_ace_aos_exem', $_METRICAS)) {
			$metricasdaanalise[] = A_LANG_TOT_DE_ACE_AOS_EXEM;
			$dadosparamapear[] = array(
				'secao' => A_LANG_EXEMPLOS,
				'total' => somar($tot_de_ace_aos_exem, 'TOTAL_VIEWS')
			);
		}
		if(in_array('tot_de_ace_aos_mat_com', $_METRICAS)) {
			$metricasdaanalise[] = A_LANG_TOT_DE_ACE_AOS_MAT_COM;
			$dadosparamapear[] = array(
				'secao' => A_LANG_MAT_COM_PLU,
				'total' => somar($tot_de_ace_aos_mat_com, 'TOTAL_VIEWS')
			);
		}
		
		$titulodaanalise = A_LANG_AMB_DE_AULA. ': '. implode(' x ', $metricasdaanalise);
		
		$linhas = (linhas($dadosparamapear, 'total', 'secao'));
		$barras = (barras($dadosparamapear, 'total', 'secao', A_LANG_TOT_DE_ACE));
		$setores = setores($dadosparamapear, 'total', 'secao');
	
		$linhas['valores'] = array($linhas['valores']);
		$barras['valores'] = array($barras['valores']);		
	}
}
?>

<form id="opcoesdografico">
	<!-- Opções do gráfico -->
	<div class="a_esq">
		<strong><?=A_LANG_GRAFICO; ?>:</strong>
		<select name="grafico" id="grafico" class="slt">
			<option value="barras" <? if($_GRAFICO == 'barras') echo 'selected="selected"'; ?>><?=A_LANG_BARRAS; ?></option>
			<option value="linhas" <? if($_GRAFICO == 'linhas') echo 'selected="selected"'; ?>><?=A_LANG_LINHAS; ?></option>
			<option value="setores" <? if($_GRAFICO == 'setores') echo 'selected="selected"'; ?>><?=A_LANG_SETORES; ?></option>
		</select>
		<? if($agruparpor) { ?>
		<strong><?=A_LANG_AGR_POR; ?>:</strong>
		<select name="agruparpor" id="agruparpor" class="slt">
			<option value="mes" <? if($_AGR_POR == 'mes') echo 'selected="selected"'; ?>><?=A_LANG_MES; ?></option>
			<option value="semana" <? if($_AGR_POR == 'semana') echo 'selected="selected"'; ?>><?=A_LANG_SEMANA; ?></option>
			<option value="dia" <? if($_AGR_POR == 'dia') echo 'selected="selected"'; ?>><?=A_LANG_DIA; ?></option>
		</select>
		<? } ?>
	</div>
	<!-- / Opções do gráfico -->
	
	<!-- Impressão -->
	<?
	if(!isset($_IMPRESSAO)) 
		include $_DIRETORIO. 'link-impressao.php'; 
	else
		echo '<div class="no_ini"></div>';
	?>
	<!-- / Impressão -->
	
	<!-- Gráfico -->
	<div class="bordas mar_int_20 mar_ext_inf_20">
		<p id="titulodaanalise"><?=$titulodaanalise; ?></p>
		<div id="graficodosresultados"></div>
		
		<!-- Rótulo ou legenda para gráfico de setores -->
		<div class="graficodesetores">
			<strong><?=A_LANG_MOSTRAR; ?>:</strong>
			<label><input type="radio" name="legenda" value="true" <? if($_LEGENDA != 'false') echo 'checked="checked"'; ?> /> <?=A_LANG_LEGENDA; ?></label>
			<label><input type="radio" name="legenda" value="false" <? if($_LEGENDA == 'false') echo 'checked="checked"'; ?> />  <?=A_LANG_ROTULO; ?></label>
		</div>
		<!-- / Rótulo ou legenda para gráfico de setores -->
	
	
		<!-- Tooltip -->
		<div id="tooltip" class="mar_int_10 mar_ext_sup_20"></div>
		<!-- / Tooltip -->
	
	</div>
	<!-- / Gráfico -->
</form>

<script>
	var metrica = "<?=$metrica; ?>";
	var previousPoint = null;

	<? if($metrica == 'fre_de_ace') { ?>
	var vgraficodelinhas = null;
	var vgraficodebarras = null;
	var vgraficodesetores = null;
	var rgraficodelinhas = null;
	var rgraficodebarras = null;
	var rgraficodesetores = null;
	<? } else { ?>
	var vgraficodelinhas = <?=json_encode($linhas['valores']); ?>;
	var vgraficodebarras = <?=json_encode($barras['valores']); ?>;
	var vgraficodesetores = <?=json_encode($setores); ?>;
	var rgraficodelinhas = <?=json_encode($linhas['rotulos']); ?>;
	var rgraficodebarras = <?=json_encode($barras['rotulos']); ?>;
	<? } ?>
	
	function number_format(number, decimals, dec_point, thousands_sep) {
		number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  	var n = !isFinite(+number) ? 0 : +number,
			prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
			sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
			dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
			s = '',
			toFixedFix = function (n, prec) {
				var k = Math.pow(10, prec);
				return '' + Math.round(n * k) / k;
			};
			s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  		if (s[0].length > 3) {
    		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  		}
  		if ((s[1] || '').length < prec) {
   			s[1] = s[1] || '';
    		s[1] += new Array(prec - s[1].length + 1).join('0');
  		}
  	return s.join(dec);
	}

	
	// função que formata a hora
	function formatarHora(val) {
		seconds = val % 60;
		minutes = (val - seconds) / 60;
		minutes = minutes % 60;
		hours = Math.floor(minutes/60);
		if(hours < 10) hours = "0"+hours;
		if(minutes < 10) minutes = "0"+minutes;
		if(seconds < 10) seconds = "0"+seconds;
		return hours+":"+minutes+":"+seconds;
	}
	
	// função que cria dinamicamente o tooltip
	function tooltip(event, pos, item) {
		chart = $("#grafico").val();
		
		if(chart == 'setores') {
			y = item.datapoint[1][0][1];
		} else {
			y = item.datapoint[1];
		}
		
		if(metrica == "tem_med_de_ace_dos_alu") {
			y = formatarHora(y);
		}
		
		if(chart == "setores") {
			x = item.series.label;
		} else if(chart == 'bar') {
			x = rgraficodebarras[item.dataIndex][1];
		} else {
			x = rgraficodelinhas[item.dataIndex][1];
		}
		
		<? if($_METRICAS == array('tot_de_ace_aos_top', 'tot_de_res')) { ?>
		if(item.series.label == "<?=A_LANG_TOT_DE_ACE; ?>") {
			html = "";
		} else {
			html = "";
		}
		<? } else if(count($_METRICAS) > 1) { ?>
			html = "<strong><?=A_LANG_CATEGORIA; ?>:</strong> " +x+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong><?=A_LANG_TOT_DE_ACE; ?></strong>  <?=str_replace('%1%', '" +y+ "', A_LANG_ACESSOS); ?>";
		<? } else { ?>
			html = "<?=tooltip($metrica); ?>";
			
			<? if($metrica == 'fre_de_ace') { ?>
			agruparpor = $("#agruparpor").val();
			if(agruparpor == "dia")	periodo = "<strong><?=A_LANG_DIA ?>:</strong> ";
			if(agruparpor == "semana")	periodo = "<strong><?=A_LANG_SEMANA ?>:</strong> ";
			if(agruparpor == "mes")	periodo = "<strong><?=A_LANG_MES ?>:</strong> ";
			
			
			html = periodo + html;
			<? } ?>
		<? } ?>
		
	 	if(chart == "setores") {
			html += " ("+ number_format(item.series.percent, 2, ',', '.') +"%)";
		}
			
		$("#tooltip")
			.css({ background: item.series.color })
			.html(html)
			.show();
	}

	// função que cria o gráfico conforme as opções
	function criargrafico() {
		grafico = $("#grafico").val();
		$(".graficodesetores").hide();
		
		<? if($metrica == 'fre_de_ace') { ?>	
		agruparpor = $("#agruparpor").val();
		
		if(agruparpor == "dia") {
			vgraficodelinhas = <?=json_encode($linhas['dia']['valores']); ?>;
			vgraficodebarras = <?=json_encode($barras['dia']['valores']); ?>;
			vgraficodesetores = <?=json_encode($setores['dia']); ?>;
			rgraficodelinhas = <?=json_encode($linhas['dia']['rotulos']); ?>;
			rgraficodebarras = <?=json_encode($barras['dia']['rotulos']); ?>;
		} else if(agruparpor == "semana") {
			vgraficodelinhas = <?=json_encode($linhas['semana']['valores']); ?>;
			vgraficodebarras = <?=json_encode($barras['semana']['valores']); ?>;
			vgraficodesetores = <?=json_encode($setores['semana']); ?>;
			rgraficodelinhas = <?=json_encode($linhas['semana']['rotulos']); ?>;
			rgraficodebarras = <?=json_encode($barras['semana']['rotulos']); ?>;
		} else if(agruparpor == "mes") {
			vgraficodelinhas = <?=json_encode($linhas['mes']['valores']); ?>;
			vgraficodebarras = <?=json_encode($barras['mes']['valores']); ?>;
			vgraficodesetores = <?=json_encode($setores['mes']); ?>;
			rgraficodelinhas = <?=json_encode($linhas['mes']['rotulos']); ?>;
			rgraficodebarras = <?=json_encode($barras['mes']['rotulos']); ?>;
		}	
		<? } ?>
		
		switch(grafico) {
			// gráfico de barras
			case "barras":
				$.plot($("#graficodosresultados"), vgraficodebarras, {
					bars: {
						show: true,
						barWidth: 0.5,
						fill: 0.65,
						align: "center"
					},
					xaxis: {
						autoscaleMargin: 0.05,
						ticks: [],
						tickLength: 0,
						axisLabel: "<?=$eixo_x; ?>",
						axisLabelPadding: 10
					},
					yaxis: {
						<? if($metrica == 'tem_med_de_ace_dos_alu') { ?>
						tickFormatter: function(val, axis) {
							return formatarHora(val);
						},
						<? } ?>
						axisLabel: "<?=$eixo_y; ?>",
						axisLabelPadding: 10
					},
					<? if($semlegendas) { ?>
					legend: {
						show: false
					},
					<? } ?>
					grid: {
						borderWidth: 0,
						hoverable: true
					}
				});
				$(".yaxisLabel, .xaxisLabel").css("color", "#000066");
			break;
			
			// gráfico de linhas
			case "linhas":
				$.plot($("#graficodosresultados"), vgraficodelinhas, {
					series: {
						lines: {
							show: true
						},
						points: {
							show: true,
							radius: 3,
							fill: 1
						}
					},
					xaxis: {
						autoscaleMargin: 0.05,
						ticks: [],
						tickLength: 0,
						axisLabel: "<?=$eixo_x; ?>",
						axisLabelPadding: 10
					},
					yaxis: {
						<? if($metrica == "tem_med_de_ace_dos_alu") { ?>
						tickFormatter: function(val, axis) {
							return formatarHora(val);
						},
						<? } ?>
						axisLabel: "<?=$eixo_y; ?>",
						axisLabelPadding: 10
					},
					<? if($semlegendas) { ?>
					legend: {
						show: false
					},
					<? } ?>
					grid: {
						borderWidth: 0,
						hoverable: true
					}
				});
				$(".yaxisLabel, .xaxisLabel").css("color", "#000066");
			break;
			
			// gráfico de setores
			case "setores":
				$(".graficodesetores").show();
				$.plot($("#graficodosresultados"), vgraficodesetores, {
					series: {
						pie: {
							show: true
						}
					},
					grid: {
						show: true,
						axisMargin: 0,
						borderWidth: 1,
						hoverable: true
					},
					legend: {
						show: $("input[name=legenda]:checked").val() == "true"
					}
    		});
			break;
		}
		
		$("#graficodosresultados").bind("plothover", function(event, pos, item) {
			if(item) {
					// previousPoint = item.dataIndex;
					tooltip(event, pos, item);
			} else {
				$("#tooltip").hide();
				// previousPoint = null;
			}
		});		
	}
	
	function resolvertabela(agruparpor) {
		$("#fre_de_ace_por_dia, #fre_de_ace_por_sem, #fre_de_ace_por_mes").hide();
		if(agruparpor == "dia") $("#fre_de_ace_por_dia").show();
		if(agruparpor == "semana") $("#fre_de_ace_por_sem").show();
		if(agruparpor == "mes") $("#fre_de_ace_por_mes").show();
	}

	$(function() {				
		criargrafico();
		
		// altera o grafico
		$("#grafico").change(function() {
			criargrafico();
		});
		
		// mostra rótulos ou legenda no gráfico de setores
		$("input[name=legenda]").click(function() {
			criargrafico();
		});
		
		<? if($metrica == 'fre_de_ace') { ?>
			resolvertabela($("#agruparpor").val());		
		// mostra a métrica Frequencia de acessos de acordo com o período selecionado
		$("#agruparpor").change(function() {
			criargrafico();
			resolvertabela($(this).val());			
		});
		<? } ?>
	})
</script>