<!-- FERRAMENTA DE ANÁLISE INTERATIVA -->
<!-- DESENVOLVIDO POR BARBARA MOISSA -->
<?php

// configurações da ferramenta
$_DIRETORIO = 'analise-interativa-v3/';
$_CAMINHO = $DOCUMENT_SITE. '/' .$_DIRETORIO;

// arquivos importantes
include_once $_DIRETORIO. 'funcoes.php';
include_once 'analytics/functions.php';

// aba da ferramenta, conforme padrão do AdaptWeb
MontaOrelha(array(
	array(
		'LABEL' => A_LANG_ANA_INT,
		'LINK' => 'index.php',
		'ESTADO' => 'ON'
	)
));
?>

<div id="ferramenta">
	<div class="mar_int_20 mar_int_sup_0">
		<!-- AJUDA -->
		<div class="a_dir">
			<a href="<?=$_CAMINHO. A_LANG_LIN_AJU; ?>"><?=A_LANG_AJUDA; ?></a>
		</div>
		<div class="no_ini"></div>
		<!-- / AJUDA -->		
		
		<!-- FILTRO -->
		<div class="mar_ext_sup_10 mar_ext_inf_20">
			<? include_once $_DIRETORIO. 'filtro.php'; ?>
		</div>
		<!-- / FILTRO -->
		
		<?
		$naoharesultados = false;

		// Busca os resultados para cada uma das métricas
		foreach($_METRICAS as $metrica) {
			$per_de = data_para_ban_de_dad($_PER_DE, A_LANG_FOR_DA_DATA);
			$per_ate = data_para_ban_de_dad($_PER_ATE, A_LANG_FOR_DA_DATA);
			$$metrica = dados($metrica, $_CURSO, $_DISCIPLINA, $per_de, $per_ate, $_PROFESSOR);
			
			if($metrica == 'fre_de_ace') {
				if(empty($fre_de_ace['dia']) && empty($fre_de_ace['semana']) && empty($fre_de_ace['mes']))
					$naoharesultados = true;
			} else if(empty($$metrica)) {
				$naoharesultados = true;
			}
		}
		
		if(!$naoharesultados) {
		?>
		
		<!-- GRÁFICO -->		
		<div class="mar_ext_sup_10 mar_ext_inf_20">
			<? include_once $_DIRETORIO. 'grafico.php'; ?>
		</div>		
		<!-- / GRÁFICO -->
		
		<? include $_DIRETORIO. 'link-impressao.php'; ?>		
		
		<!-- TABELA -->		
		<div class="mar_ext_sup_10">
			<? include_once $_DIRETORIO. 'tabela.php'; ?>
		</div>		
		<!-- / TABELA -->
		
		<? include $_DIRETORIO. 'link-impressao.php'; ?>
		<? } else { ?>
		<p id="semresultados"><?=A_LANG_SEM_RES; ?></p>
		<? } ?>
	</div>
</div>

<!-- CSS -->
<link rel="stylesheet" type="text/css" href="<?=$_CAMINHO; ?>css/analise-interativa.css" />
<link href="<?=$_CAMINHO; ?>js/datatables/media/css/jquery.dataTables.css" type="text/css" rel="stylesheet"/>
<!-- / CSS -->

<!-- JS -->
<script type="text/javascript" src="<?=$_CAMINHO ?>js/masked-input.js"></script>
<script type="text/javascript" src="<?=$_CAMINHO ?>js/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=$_CAMINHO ?>js/flot/excanvas.min.js"></script>
<script type="text/javascript" src="<?=$_CAMINHO ?>js/flot/jquery.flot.min.js"></script>
<script type="text/javascript" language="javascript" src="<?=$_CAMINHO ?>js/flot/jquery.flot.axislabels.js"></script>
<script type="text/javascript" language="javascript" src="<?=$_CAMINHO ?>js/flot/jquery.flot.symbol.js"></script>
<script type="text/javascript" language="javascript" src="<?=$_CAMINHO ?>js/flot/jquery.flot.orderBars.js"></script>
<script type="text/javascript" language="javascript" src="<?=$_CAMINHO ?>js/flot/jquery.flot.pie.min.js"></script>

<script>
	function mos_met() {
		$("#met_ocu").hide();
		$("#met_vis").show();
	}
	
	function ocu_met() {
		$("#met_vis").hide();
		$("#met_ocu").show();
		
		metricas = [];
		$("input:checkbox[name='metricas[]']:checked").each(function() {
			metrica = $($(this).attr("rel"));
			categoria = $(metrica.attr("rel"));
			metricas.push(categoria.html()+ " - " + metrica.html());
		});
		
		html = "<strong><?=A_LANG_MET_SEL; ?>:</strong> " + metricas.join('; ');
		$("#met_sel").html(html);		
	}

	$(function() {
		// mostra as métricas ao carregar
		mos_met();
		
		// desabilita as métricas ao carregar a página
		$("input:checkbox[name='metricas[]']").attr("disabled", "disabled");
		$("input:checkbox[name='metricas[]']:checked").removeAttr("disabled");
		if($(".gru_1:checked").length > 0) {
			$(".gru_1").removeAttr("disabled");
		}
		if($(".gru_2:checked").length > 0) {
			$(".gru_2").removeAttr("disabled");
		}
		
		// mostra as métricas
		$("#met_ocu .met_sit").click(function() {
			mos_met();
		});
		
		// oculta as métricas
		$("#met_vis .met_sit").click(function() {
			ocu_met();
		});
		
		// máscara no formato __/__/____ para as datas
		$(".data").mask("99/99/9999");
		
		// desabilita todas as métricas
		$("input:checkbox[name='metricas[]']").click(function() {
			if($("input:checkbox[name='metricas[]']:checked").length == 0) {
				$("input:checkbox[name='metricas[]']").removeAttr("disabled");
			} else {
				$("input:checkbox[name='metricas[]']").attr("disabled", "disabled");
				$(this).removeAttr("disabled");
			}
		});
		
		// habilita as metricas que podem ser cruzadas da categoria Ambiente de Aula
		$(".gru_1").click(function() {
			$(".gru_1").removeAttr("disabled");
		});
		
		// habilita as métricas que podem ser cruzadas da categoria Fórum de Discussão
		$(".gru_2").click(function() {
			$(".gru_2").removeAttr("disabled");
		});
		
		// deseleciona métricas
		$("#des_met").click(function(event) {
			event.preventDefault();
				$("input:checkbox[name='metricas[]']").removeAttr("disabled").removeAttr("checked");					
		});
		
		$(".datatable").dataTable({
			"oLanguage": {
				"sLengthMenu": "<?=A_LANG_sLengthMenu; ?>",
				"sZeroRecords": "<?=A_LANG_sZeroRecords; ?>",
				"sInfo": "<?=A_LANG_sInfo; ?>",
				"sInfoEmpty": "",
				"sInfoFiltered": "<?=A_LANG_sInfoFiltered; ?>",
				"sSearch": "<?=A_LANG_sSearch; ?>:"
			},
			"bPaginate": false
		});
		
		$(".link_imp").click(function(e) {
			href = "<?=$_CAMINHO. 'impressao.php?'. http_build_query($_POST); ?>";
			<? if($metrica == 'fre_de_ace') { ?>
			href += "&agr_por=" + $("#agruparpor").val();
			<? } ?>
			href += "&legenda=" + $("input[name=legenda]:checked").val();
			href += "&grafico=" + $("#grafico").val();
			$(this).attr("href", href);
		});
	})
</script>
<!-- / JS-->

<!-- / FERRAMENTA DE ANÁLISE INTERATIVA -->