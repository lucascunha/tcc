<?php
if(count($_METRICAS) == 1) {
	$titulo = false;
} else {
	$titulo = true;
}

foreach($_METRICAS as $metrica) {
	if(is_file($_DIRETORIO. 'tabelas/'. $metrica . '.php')) {
		$dados = tabela($metrica, $_CURSO, $_DISCIPLINA, $per_de, $per_ate, $_PROFESSOR);
		include $_DIRETORIO. 'tabelas/'. $metrica . '.php';
	}
}