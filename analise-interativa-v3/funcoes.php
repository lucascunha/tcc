<?php

function consulta($sql) {
	return mysql_query($sql);
}

function tot_res($conjunto) {
	return mysql_num_rows($conjunto);
}

function objeto($conjunto) {
	return mysql_fetch_object($conjunto);
}

function registro($sql) {
	$conjunto = consulta($sql);
	if(tot_res($conjunto) > 0) {
		return objeto($conjunto);
	}
	return NULL;
}

function dados($metrica, $curso, $disciplina, $per_de, $per_ate, $professor) {
	
	switch($metrica) {
		case 'tot_de_vis_por_alu': $parametro = 'Tempo_acesso_aluno_geral'; break;
		case 'tem_med_de_ace_dos_alu': $parametro = 'Visitas_por_aluno'; break;
		case 'fre_de_ace': $parametro = 'Frequencia_acessos'; break;
		case 'tot_de_ace_a_cada_sec': $parametro = 'Tempo_secao'; break;
		case 'sis_ope': $parametro = 'Dados_tecnologicos'; break;
		case 'navegadores': $parametro = 'Dados_tecnologicos'; break;
		case 'res_de_tela': $parametro = 'Dados_tecnologicos'; break;
		case 'modo_de_nav': $parametro = 'Tempo_secao'; break;
		case 'tot_de_usos_do_sis_de_bus': $parametro = 'Total_busca'; break;
		case 'pal_pes': $parametro = 'Total_busca_keywords'; break;
		case 'tot_de_ace_aos_con': $parametro = 'Acesso_conceitos'; break;
		case 'tot_de_ace_aos_exer': $parametro = 'Acesso_exercicios'; break;
		case 'tot_de_ace_aos_exem': $parametro = 'Acesso_exemplos'; break;
		case 'tot_de_ace_aos_mat_com': $parametro = 'Acesso_materiais'; break;
		case 'tot_de_ace_aos_top': $parametro = 'Forum_topicos'; break;
		case 'tot_de_top_cri': $parametro = 'Forum_topicos_criacao'; break;
		case 'tot_de_res': $parametro = 'Forum_topicos_resposta'; break;
		case 'tot_de_rec_env': $parametro = 'Recados_envio_aluno'; break;
		case 'tip_de_rec_env': $parametro = 'Recados_envio'; break;
		case 'tot_de_vis': $parametro = 'Recados_acessos'; break;
	}
	
	$dados = wa_query($parametro, $curso, $disciplina, $professor, $per_de, $per_ate);
	
	if($parametro == 'Dados_tecnologicos') {
		switch($metrica) {
			case 'res_de_tela': $dados = $dados['resolution']; break;
			case 'sis_ope': $dados = $dados['os']; break;
			case 'navegadores': $dados = $dados['browser']; break;
		}
	} else if($metrica == 'fre_de_ace') {
		if(A_LANG_FOR_DA_DATA == 'm/d/Y') {
			$dados = traduzirfredeace($dados);
		}
	} else if($metrica == 'tip_de_rec_env') {
		$dados = traduzirtipderecenv($dados);
	}
		
	return $dados;
}

function traduzirtipderecenv($dados) {
	$total = count($dados);
	for($ct = 0; $ct < $total; $ct++) {
		if($dados[$ct]['tipo_recado'] == 'Professor') $dados[$ct]['tipo_recado'] = A_LANG_PROFESSOR;
		if($dados[$ct]['tipo_recado'] == 'Grupo') $dados[$ct]['tipo_recado'] = A_LANG_GRUPO;
	}
	
	return $dados;
}

function data_para_ban_de_dad($data, $for_da_data) {
	if($for_da_data == 'd/m/Y') {
		list($dia, $mes, $ano) = explode('/', $data);
	} else if($for_da_data == 'm/d/Y') {
		list($mes, $dias, $ano) = explode('/', $data);
	}
	
	return date('Y-m-d', mktime(0, 0, 0, $mes, $dia, $ano));
}

function tabela($metrica, $curso, $disciplina, $per_de, $per_ate, $professor) {
	
	switch($metrica) {
		case 'tot_de_vis_por_alu': $parametro = 'Tempo_acesso_aluno_geral'; break;
		case 'tem_med_de_ace_dos_alu': $parametro = 'Tempo_acesso_aluno'; break;
		case 'fre_de_ace': $parametro = 'Frequencia_acessos'; break;
		case 'tot_de_ace_a_cada_sec': $parametro = 'Tempo_secao'; break;
		case 'sis_ope': $parametro = 'Dados_tecnologicos'; break;
		case 'navegadores': $parametro = 'Dados_tecnologicos'; break;
		case 'res_de_tela': $parametro = 'Dados_tecnologicos'; break;
		case 'modo_de_nav': $parametro = 'Tempo_secao'; break;
		case 'tot_de_usos_do_sis_de_bus': $parametro = 'Total_busca'; break;
		case 'pal_pes': $parametro = 'Total_busca_keywords'; break;
		case 'tot_de_ace_aos_con': $parametro = 'Acesso_conceitos'; break;
		case 'tot_de_ace_aos_exer': $parametro = 'Acesso_exercicios'; break;
		case 'tot_de_ace_aos_exem': $parametro = 'Acesso_exemplos'; break;
		case 'tot_de_ace_aos_mat_com': $parametro = 'Acesso_materiais'; break;
		case 'tot_de_ace_aos_top': $parametro = 'Forum_topicos'; break;
		case 'tot_de_top_cri': $parametro = 'Forum_topicos_criacao'; break;
		case 'tot_de_res': $parametro = 'Forum_topicos_resposta'; break;
		case 'tot_de_rec_env': $parametro = 'Recados_envio_aluno'; break;
		case 'tip_de_rec_env': $parametro = 'Recados_envio'; break;
		case 'tot_de_vis': $parametro = 'Recados_acessos'; break;
	}
	
	$dados = wa_query($parametro, $curso, $disciplina, $professor, $per_de, $per_ate);
	
	if($parametro == 'Dados_tecnologicos') {
		switch($metrica) {
			case 'res_de_tela': $dados = $dados['resolution']; break;
			case 'sis_ope': $dados = $dados['os']; break;
			case 'navegadores': $dados = $dados['browser']; break;
		}
	} else if($metrica == 'fre_de_ace') {
		if(A_LANG_FOR_DA_DATA == 'm/d/Y') {
			$dados = traduzirfredeace($dados);
		}
	} else if($metrica == 'tip_de_rec_env') {
		$dados = traduzirtipderecenv($dados);
	}
		
	return $dados;
}

function traduzirfredeace($dados) {
	$total = count($dados['dia']);
	for($ct = 0; $ct < $total; $ct++)
		$dados['dia'][$ct]['data'] = dat_ing($dados['dia'][$ct]['data']);
	
	$total = count($dados['semana']);
	for($ct = 0; $ct < $total; $ct++)
		$dados['semana'][$ct]['semana'] = sem_ing($dados['semana'][$ct]['semana']);
		
	return $dados;
}

function dat_ing($data) {
	list($dia, $mes, $ano) = explode('/', $data);
	return "$mes/$dia/$ano";
}

function sem_ing($semana) {
	list($dia_1, $dia_2) = explode(' -> ', $semana);
	return dat_ing($dia_1). ' -> ' .dat_ing($dia_2);
}

function nom_cur($id) {
	$sql = 'SELECT nome_curso FROM curso WHERE id_curso = '. $id;
	if($curso = registro($sql))
		return $curso->nome_curso;
}

function nom_dis($id) {
	$sql = 'SELECT nome_disc FROM disciplina WHERE id_disc = '. $id;
	if($disciplina = registro($sql))
		return $disciplina->nome_disc;	
}

// mapeia os dados para um grafico de linhas do flot
function linhas($dados, $eixoy, $eixox) {
	$numero = 1;
	$mapeamento = array(
		'valores' => array(),
		'rotulos' => array()
	);
	
	foreach($dados as $dado) {
		$mapeamento['valores'][] = array($numero, floatval($dado[$eixoy]));
		$mapeamento['rotulos'][] = array($numero, htmlentities($dado[$eixox]));
		$numero++;
	}
	
	return $mapeamento;
}

// mapeia os dados para um grafico de barras do flot
function barras($dados, $eixoy, $eixox, $nome) {
	$numero = 1;
	$mapeamento = array(
		'valores' => array(),
		'rotulos' => array()
	);
	
	foreach($dados as $dado) {
		$mapeamento['valores'][] = array($numero, floatval($dado[$eixoy]));
		$mapeamento['rotulos'][] = array($numero, htmlentities($dado[$eixox]));
		$numero++;
	}
	
	$mapeamento['valores'] = array(
		'label' => $nome,
		'data' => $mapeamento['valores']
	);
	
	return $mapeamento;
}

// mapeia os dados para um grafico de setores do flot
function setores($dados, $eixoy, $eixox) {
	$mapeamento = array();
	
	foreach($dados as $dado)
		$mapeamento[] = array(
			'data' => floatval($dado[$eixoy]),
			'label' => htmlentities($dado[$eixox]),
		);
		
	return $mapeamento;
}

function numero($hora) {
	list($horas, $minutos, $segundos) = explode(':', trim($hora));	
	$numero = ($horas * 60 * 60) + ($minutos * 60) + $segundos;
	return $numero;
}

function somar($tabela, $coluna) {
	$total = 0;
	foreach($tabela as $linha)
		$total += $linha[$coluna];
	return $total;
}

function tooltip($metrica) {
	switch($metrica) {
		
		// USO GERAL
		case 'tot_de_vis_por_alu': $tooltip = '<strong>'. A_LANG_ALUNO .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_ACE .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_ACESSOS); break;
		
		case 'tem_med_de_ace_dos_alu': $tooltip = '<strong>'. A_LANG_ALUNO .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TEM_MED_DE_ACE .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_HORAS); break;
		
		case 'fre_de_ace': $tooltip = '" +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_ACE .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_ACESSOS); break;
		
		case 'tot_de_ace_a_cada_sec': $tooltip = '<strong>'. A_LANG_SECAO .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_ACE .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_ACESSOS); break;
		
		case 'sis_ope': $tooltip = '<strong>'. A_LANG_SIS_OPE_SIN .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_ACE .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_ACESSOS); break;
		
		case 'navegadores': $tooltip = '<strong>'. A_LANG_NAVEGADOR .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_ACE .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_ACESSOS); break;
		
		case 'res_de_tela': $tooltip = '<strong>'. A_LANG_RES_DE_TELA_SIN .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_ACE .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_ACESSOS); break;
		
		// AMBIENTE DE AULA
		case 'modo_de_nav': $tooltip = '<strong>'. A_LANG_MODO_DE_NAV .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_ACE .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_ACESSOS); break;
		
		case 'tot_de_usos_do_sis_de_bus': $tooltip = '<strong>'. A_LANG_ALUNO .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_BUS .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_BUSCAS); break;
		
		case 'pal_pes': $tooltip = '<strong>'. A_LANG_PAL_PES_SIN .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_BUS .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_BUSCAS); break;
		
		case 'tot_de_ace_aos_con': $tooltip = '<strong>'. A_LANG_CONCEITO .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_ACE .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_ACESSOS); break;
		
		case 'tot_de_ace_aos_exer': $tooltip = '<strong>'. A_LANG_EXERCICIO .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_ACE .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_ACESSOS); break;
		
		case 'tot_de_ace_aos_exem': $tooltip = '<strong>'. A_LANG_EXEMPLO .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_ACE .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_ACESSOS); break;
		
		case 'tot_de_ace_aos_mat_com': $tooltip = '<strong>'. A_LANG_MAT_COM_SIN .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_ACE .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_ACESSOS); break;
		
		// FÓRUM DE DISCUSSÃO
		case 'tot_de_ace_aos_top': $tooltip = '<strong>'. A_LANG_TOPICO .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_ACE .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_ACESSOS); break;
		
		case 'tot_de_top_cri': $tooltip = '<strong>'. A_LANG_ALUNO .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_TOP_CRI .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_TOPICOS); break;
		
		case 'tot_de_res': $tooltip = '<strong>'. A_LANG_TOPICO .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_RES .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_RESPOSTAS); break;
		
		// MURAL DE RECADOS
		case 'tot_de_rec_env': $tooltip = '<strong>'. A_LANG_ALUNO .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_REC_ENV .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_RECADOS); break;
		
		case 'tip_de_rec_env': $tooltip = '<strong>'. A_LANG_TIP_DO_REC .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_REC_ENV .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_RECADOS); break;
		
		case 'tot_de_vis': $tooltip = '<strong>'. A_LANG_DISCIPLINA .':</strong> " +x+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>'. A_LANG_TOT_DE_ACE .':</strong> '. str_replace('%1%', '" +y+ "', A_LANG_ACESSOS); break;
	}
	
	return $tooltip;
}