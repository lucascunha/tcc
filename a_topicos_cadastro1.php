<?
/* -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Inform�tica  
 *       UEL - Departamento de Computa��o
 * -----------------------------------------------------------------------
 *       @package Estruturador de conte�do   
 *     @subpakage Cadastro de conceito
 *          @file a_topicos_cadastro1.php
 *    @desciption Cadastro de conceitos (sempre que for inserir conceito 
 *                de mesmo n�vel e subconceito) - tamb�m � utilizado 
 *                quando n�o existe conceito cadastrado  
 *         @since 25/06/2003
 *        @author Veronice de Freitas (veronice@jr.eti.br)
 * -----------------------------------------------------------------------         
 */

$Gravar = $_POST['Gravar'];
$cDESCTOP= $_POST['cDESCTOP'];
$cABREVIACAO= $_POST['cABREVIACAO'];
$cPALCHAVE= $_POST['cPALCHAVE'];
$CodigoDisciplina = $_GET['CodigoDisciplina'];
$numtopico = $_GET['numtopico'];

$destino = '';
if (isset($Gravar ))
{
// ===================
// Grava Topicos Inclusao novo
// ===================

 session_start();
 //include "config/configuracoes.php"; 
 //include "include/funcoes.php"; 

 global $conteudo, $id_usuario, $email_usuario, $id_usuario;

 if ($numtopico ==1){$conteudo = array();}
 else 
 {$conteudo = $_SESSION['conteudo']; } 

 // o usuario 2 foi utilizado para demonstra��o da disciplina e n�o pode incluir, excluir, alterar dados  
 if  ($id_usuario <> 2)
 { 

    if ((trim($cDESCTOP) <> "") && (trim($cABREVIACAO) <> "") && (trim($cPALCHAVE) <> "") )  
    { 
        $Linha = array(   
                 "NUMTOP" => $numtopico , 
                   "DESCTOP" => trim($cDESCTOP), 
                   "ABREVIACAO" => trim($cABREVIACAO), 
                   "PALCHAVE" => trim($cPALCHAVE)
            );
        array_push($conteudo,$Linha);
        GravaMatriz($CodigoDisciplina);
        $_SESSION['conteudo'] = $conteudo;
        if ($numtopico <1)
          $destino="Location: a_index.php?opcao=Topicos&StatusGravacao=ok&CodigoDisciplina=".$CodigoDisciplina;
        else
          $destino="Location: a_index.php?opcao=TopicosEstruturarArvore&StatusGravacao=ok&CodigoDisciplina=".$CodigoDisciplina;

    }
    else 
    {
      $logo =  "<img src='imagens/icones/error.png' />";
      ?>
      <script type="text/javascript">
          showNotification({
          message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $logo; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo 'Por favor, preencha todos os campos.'; ?></td></tr></table>",                                
          type: "error",
          autoClose: true,
          duration: 4                                        
         });
      </script> 
      <?php
        //$destino="Location: a_index.php?opcao=TopicosCadastro1&CodigoDisciplina=".$CodigoDisciplina."&numtopico=".$numtopico;
    }
  }
  else
  { 
     echo "<p class=\"texto5\">\n";
     echo A_LANG_DEMO;
     echo "</p>\n";
  }

  if ($destino)
  { 
      header("$destino");
  }
}

 
 // **************************************************************************
 // *  CRIA MATRIZ ORELHA                                                    *
 // **************************************************************************

  $orelha = array();  
  $orelha = array(
    array(   
         "LABEL" => 'Selecionar Disciplina',  
           "LINK" => "a_index.php?opcao=EntradaTopicos",  
           "ESTADO" =>"OFF"
         ),       
      array(   
         "LABEL" => 'Estruturar Disciplina', 
           "LINK" => "", 
           "ESTADO" =>"ON"
         ) ,       
       
         
        ); 

  MontaOrelha($orelha);  

  // echo $CodigoDisciplina;

?>
 
<!-- --------------- cadastro de t�picos ------------------- -->
<FORM action="?opcao=TopicosCadastro1&numtopico=<? echo $numtopico."&CodigoDisciplina=".$CodigoDisciplina; ?>" method="post">

<table CELLSPACING=1 CELLPADDING=1 width="100%"  border =0  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> >
<tr valign="top">
<td >  
  <table CELLSPACING=0 CELLPADDING=0 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; border-left:3px; border-top:3px; border-bottom:3px; border-right:3px; border-style:solid; border-color:#eeeeee;" >  
  <tr>
    <td>
<?

if ($numtopico == 1)
{

  $orelha = array();  
  $orelha = array(

    array(   
         "LABEL" => 'Estruturar Menu',  
           "LINK" => "a_index.php?opcao=TopicosEstruturarArvore&CodigoDisciplina=".$CodigoDisciplina,  
           "ESTADO" =>"ON"
         ), 

      array(   
         "LABEL" => "<font style=\"font-weight:normal;\" color=#999999>".A_LANG_TOPIC."</font>", 
           "LINK" => "" ,
           "ESTADO" =>"OFF"
         ),          
      array(   
         "LABEL" => "<font style=\"font-weight:normal;\" color=#999999>".A_LANG_TOPIC_EXEMPLES."</font>", 
           "LINK" => "", 
           "ESTADO" =>"OFF"
         ), 
      array(   
         "LABEL" => "<font style=\"font-weight:normal;\" color=#999999>".A_LANG_TOPIC_EXERCISES."</font>", 
         "LINK" => "",
           "ESTADO" =>"OFF"
         ),          
      array(   
         "LABEL" => "<font style=\"font-weight:normal;\" color=#999999>Materiais</font>", 
           "LINK" => "",
           "ESTADO" =>"OFF"
         ),
                                       
        ); 

}
else
{
  $orelha = array();  
  $orelha = array(

    array(   
         "LABEL" => 'Estruturar Menu',  
           "LINK" => "a_index.php?opcao=TopicosEstruturarArvore&CodigoDisciplina=".$CodigoDisciplina,  
           "ESTADO" =>"ON"
         ),  
      array(   
         "LABEL" => "<font style=\"font-weight:normal;\" color=#999999>".A_LANG_TOPIC."</font>", 
           "LINK" => "" ,
           "ESTADO" =>"OFF"
         ),          
      array(   
         "LABEL" => "<font style=\"font-weight:normal;\" color=#999999>".A_LANG_TOPIC_EXEMPLES."</font>", 
           "LINK" => "", 
           "ESTADO" =>"OFF"
         ), 
      array(   
         "LABEL" => "<font style=\"font-weight:normal;\" color=#999999>".A_LANG_TOPIC_EXERCISES."</font>", 
         "LINK" => "",
           "ESTADO" =>"OFF"
         ),          
      array(   
         "LABEL" => "<font style=\"font-weight:normal;\" color=#999999>Materiais</font>", 
           "LINK" => "",
           "ESTADO" =>"OFF"
         ),
                                      
        ); 
}

  MontaOrelha($orelha);
  ?>
    </td>
  </tr>

<tr>
  <td> 
    <br>


     
  <table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">    
      <tr>
              
        <td colspan=2>
          <?
          if ($numtopico == 1)
          {?>
            <div class="Mensagem_Amarelo" style='width:97%; margin:0 0 0 10px;'>
                    <?
                    echo "<p class=\"texto1\">\n";     
                    echo "<table cellspacing=\"0\" cellpadding=\"0\">";
                    echo "  <tr>";
                    echo "    <td width=\"50\">";
                    echo "      <img src=\"imagens/icones/warning.png\" alt=\"\" />";
                    echo "    </td>";
                    echo "    <td valign=\"center\">";
                    echo "Cadastre o primeiro Conceito para a disciplina."; 
                    echo "    </td>";
                    echo "  </tr>";
                    echo "</table>";  
                    echo "</p>\n";      
                    ?>    
            </div>  
          <?
          }
          ?>                    
            <div class="bola" style='width:98%; margin:0 0 0 10px;'>
              <h2 style="padding-top:10px; padding-left:20px;">
                <?  //echo "<a href=a_index.php?opcao=Topicos&CodigoDisciplina=$CodigoDisciplina border=0>";
                    echo  strtoupper(" ".ObterNomeDisciplina($CodigoDisciplina));
                    //echo "</a>";
                    echo " &nbsp;&nbsp;&nbsp;&raquo;&nbsp;&nbsp;&nbsp; ";
                ?>
                Conceito N�
                <? 
                    $top=EncontraTopico($numtopico);                  
                          echo $numtopico; 

                 ?>
                </h2>
              </div>
          </td>
            
          </tr>   
          <tr>
              
            <td colspan=2>

            </td>
            
          </tr>     

	  	<tr>
	    	<td  align="right" valign="top" width="170">
	    		<? echo "Nome"; ?>: *
	    	</td>
	    	<td align="left" >
				  <textarea class="button" rows="3" name="cDESCTOP" style="width:580px;" ><?php echo $cDESCTOP;?></textarea>
	    	</td>
	  	</tr>

	  	<tr>
	    	<td  align="right" valign="top">
	    	    <? echo "Descri��o Resumida"; ?>: *
	    	</td>
	    	<td align="left">
				<textarea  class="button"  rows="3" name="cABREVIACAO" style="width:580px;"><?php echo $cABREVIACAO;?></textarea>
	    	</td>
	  	</tr>

	  	<tr>
	    	<td  align="right" valign="top">
	    	    <? echo A_LANG_TOPIC_WORDS_KEY; ?>: *
	    	</td>
	    	<td align="left">
				<textarea  class="button"  rows="3" name="cPALCHAVE" style="width:580px;"><?php echo $cPALCHAVE;?></textarea>
	    	</td>
	  	</tr>

  		<tr>
    		<td  align="right" valign="top">
    		</td>
    		<td>
          <table width='585' CELLSPACING=0 CELLPADDING=0 border="0" > 
            <tr>
              <td align='right'>   
                  <input type="button" class="buttonBig" onClick="history.go(-1)" value="Voltar">
                  &nbsp;&nbsp;       
    			        <INPUT class="buttonBig"  TYPE="reset" value="Limpar Campos" >  
                  &nbsp;&nbsp;  

                  <script type="text/javascript">
                    $(
                      function() 
                      {
                      $("#customDialog").easyconfirm
                      (
                        { locale: 
                          {
                            title: '',
                            text: '<br>Confirma os dados digitados ?<br>',
                            button: ['N�o',' Sim'],
                            closeText: 'Fechar'
                  
                          }, dialog: $("#question")
                        }
                        
                      );
                        $("#customDialog").click(function() 
                        {
                      
                        }
                        );
                  
                      }
                    );
                  </script>
              
                  <input class="buttonBigBig-01" type="submit" value="Cadastrar" name="Gravar" id="customDialog">
                  <div class="dialog" id="question">
                    <table>
                      <tr>
                        <td width="50">
                          <img src="imagens/icones/notice.png" alt="" />
                        </td>
                        <td valign="center">
                          Confirma os dados digitados ?
                        </td>
                      </tr>
                    </table>  
                  </div>
                 <br><br> <br>  <br> 
	    	      </td>
	  	      </tr>
			   </table>	
		    </td>
	     </tr>
    </table>
    <br>
    </td> 
    </tr>
  </table><br>  
  </td> 
    </tr>
  </table> 

 </td>
 </tr>

</TABLE>

  </form>
