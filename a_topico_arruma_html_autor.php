<HTML>
<?php

/* -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Inform�tica  
 *       UEL - Departamento de Computa��o
 * -----------------------------------------------------------------------
 *       @package Navegacao 
 *     @subpakage Matricula
 *          @file n_mat1.php
 *    @desciption Arruma XML Autor
 *         @since 17/08/2003 
 *        @author Marilia Abraao Amaral
 * -----------------------------------------------------------------------         
 * Manutencoes
 *
 *          Data: 17/08/2002
 *   Responsavel: Veronice de Freitas (veronice@jr.eti.br)
 *     Descricao: Adapta��o do programa desenvolvido pela Marilia na 
 *                interface do ambiente de autoria 
 * -----------------------------------------------------------------------         
 */  

# Onde se encontra o termo t�pico substituir por conceito. Tanto em nome de variaveis como
# em coment�rios e mensagens
#arquivo atualizado em 28/11
#***********************************************************************************************
#Inicio da minha funcao de geracao dos xml - entrada e a matriz conteudo da veronice
#***********************************************************************************************
#***********************************************************************************************
#INICIO DA CORRECAO DOS ARQUIVOS HTML DO AUTOR (CAMINHO DAS FIGURAS)
#***********************************************************************************************


#***********************************************************************************************
#Funcao para abertura de arquivo apenas para gravacao neste.
#***********************************************************************************************
function abrearqesc_HTML($arq, &$f)
{
      $f=fopen($arq,"w",1);
}


#***********************************************************************************************
#Funcao para abertura de arquivo. Abre o arquivo para leitura e permite gravacao
#***********************************************************************************************
function abrearqleit($arq, &$f)
{
        $f=fopen($arq,"r+",1);
}
#***********************************************************************************************
#Funcao para arrumar arquivos HTML de conceito
#***********************************************************************************************
function arrumahtmlconceito($conteudo,$CodigoDisciplina,$id_usuario,$DOCUMENT_ROOT)
{
	//Tamanho da matriz
	$tam=sizeof($conteudo);
	for($i=0;$i<$tam;$i++)
	{
		//Tamanho da matriz de arquivos associados para conceito
      	$tamarqassocconceito=sizeof($conteudo[$i]["ARQPRINCIPAL"]["ARQPRINCASSOC"]);

		//Nome do arquivo HMTL que deve ser aberto
		$arq_html=$conteudo[$i]["ARQPRINCIPAL"]["ARQTOP"];

		$caminho=$DOCUMENT_ROOT."/"."disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_html;
	    abrearqleit($caminho,$fp);
		$arquivo_todo=fread($fp,filesize($caminho));
		
		$comentario="<!-- Arquivo com caminho ok -->";

		$achou_comentario=strpos($arquivo_todo,$comentario);

		//fclose($fp);
		
		for ($narq=0;$narq<$tamarqassocconceito;$narq++)
		{

			$arquivo_todo=(string) $arquivo_todo;

	  		$arq_assoc=$conteudo[$i]["ARQPRINCIPAL"]["ARQPRINCASSOC"][$narq]["ARQTOPASSOC"];

			$caminho_arq_assoc="";
			$caminho_arq_assoc="../disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;	
			
			if($achou_comentario === false)
			{	
				$nome = "disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;
				$posicao = strpos($arquivo_todo, $nome);				
				$string_posicao = substr($arquivo_todo, $posicao-3, 3 );
				if ($string_posicao =="../") // / " e =
				{
					$arquivo_todo=$arquivo_todo;	
				}
				else
				{						
					$nome=$arq_assoc;
					$arquivo_todo=str_replace($nome,$caminho_arq_assoc,$arquivo_todo);
				}	
			}
			else
			{   //disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;  original

				$nome = "disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;
				$posicao = strpos($arquivo_todo, $nome);
				$string_posicao = substr($arquivo_todo, $posicao-3, 3 );
				if ($string_posicao =="c=\"") // / " e =
				{
					$nome = "c=\"disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;
					$caminho_arq_assoc="c=\"../disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;	
					$arquivo_todo=str_replace($nome,$caminho_arq_assoc,$arquivo_todo);		
				}
				else
				if ($string_posicao =="c='") // / " e =
				{
					$nome = "c='disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;
					$caminho_arq_assoc="c='../disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;	
					$arquivo_todo=str_replace($nome,$caminho_arq_assoc,$arquivo_todo);		
				}
				else					
				if ($string_posicao =="=./") // / " e =
				{
					$nome = "=./disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;
					$caminho_arq_assoc="=../disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;	
					$arquivo_todo=str_replace($nome,$caminho_arq_assoc,$arquivo_todo);					
				}	
				else
				if ($string_posicao =="rc=") // / " e =
				{
					$nome = "rc=disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;
					$caminho_arq_assoc="rc=../disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;	
					$arquivo_todo=str_replace($nome,$caminho_arq_assoc,$arquivo_todo);		
					//echo 2;	
				}			
				else
				if ($string_posicao =="\"./") // / " e =
				{
					$nome = "\"./disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;
					$caminho_arq_assoc="\"../disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;	
					$arquivo_todo=str_replace($nome,$caminho_arq_assoc,$arquivo_todo);		
					//echo 2;	
				}			
				else
				if ($string_posicao =="'./") // / " e =
				{
					$nome = "'./disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;
					$caminho_arq_assoc="'../disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;	
					$arquivo_todo=str_replace($nome,$caminho_arq_assoc,$arquivo_todo);		
					//echo 2;	
				}										
				else		
				if ($string_posicao =="../") // / " e =
				{
					$arquivo_todo=$arquivo_todo;		
					//echo 2;	

				}	
				
			}

		}//fim do for da matriz com os arquivos associados

		//if($achou_comentario === false)
		{	$data = " <!-- -> ".date("F j, Y, g:i a")." / -->";
			$arquivo_todo = $arquivo_todo.$data;
			abrearqesc_HTML($caminho,$fp);
			fwrite($fp,$arquivo_todo);
			fclose($fp);
	
			//gravar o comentario na primeira linha do arquivo
			abrearqleit($caminho,$fp);
	
			fseek($fp,0,SEEK_END);
	
			fputs($fp,$comentario,32);
			fclose($fp);	
      	}


 	}//fim do for que percorre a matriz de conteudo
}//fim da funcao


#***********************************************************************************************
#Funcao para arrumar arquivos HTML de Exemplo
#***********************************************************************************************

function arrumahtmlexemplo($conteudo,$CodigoDisciplina,$id_usuario,$DOCUMENT_ROOT)
{
	//Tamanho da matriz
	$tam=sizeof($conteudo);
	for($i=0;$i<$tam;$i++)
	{
		//Quantidade de exemplos
		$qtde_exemp=sizeof($conteudo[$i]["EXEMPLO"]);
		for($e=0;$e<$qtde_exemp;$e++)
		{
			//Tamanho da matriz de arquivos associados para exemplo
			$tamarqassocexemplo=sizeof($conteudo[$i]["EXEMPLO"][$e]["EXEMPLOASSOC"]);

			//Nome do arquivo HMTL que deve ser aberto
			$arq_html=$conteudo[$i]["EXEMPLO"][$e]["ARQEXEMP"];
			$caminho=$DOCUMENT_ROOT."/"."disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_html;

			if($tamarqassocexemplo>0)
			{
		      		abrearqleit($caminho,$fp);
				$arquivo_todo=fread($fp,filesize($caminho));

				$comentario="<!-- Arquivo com caminho ok -->";
				$achou_comentario=strpos($arquivo_todo,$comentario);

				fclose($fp);
							
		      		for ($narq=0;$narq<$tamarqassocexemplo;$narq++)
				{
					$arruma_exemp=0;
					$arquivo_todo=(string) $arquivo_todo;
					//trocar aqui pelo caminho do exemplo
		          		$arq_assoc=$conteudo[$i]["EXEMPLO"][$e]["EXEMPLOASSOC"][$narq]["ARQEXEMPASSOC"];
					$nome=$arq_assoc;

					// alterado por Veronice
					if ($achou_comentario === false)
				        {	
					    $nome=$arq_assoc;
					}
					else
				        {	      			
				      	   $nome = "../disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;	
					}// fim do if do comentario
					  
					//armazena o caminho do arquivo exceto o arquivo associado
					// alterado por Veronice				
					$caminho_arq_assoc="";
					$caminho_arq_assoc="../disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;
								
					$arquivo_todo=eregi_replace($nome,$caminho_arq_assoc,$arquivo_todo);
						  		
				}//fim do for da matriz com os arquivos associados

				if ($achou_comentario === false)			
				{	
					abrearqesc_HTML($caminho,$fp);
				      	fwrite($fp,$arquivo_todo);
					fclose($fp);

					//posicionar o ponteiro na primeira linha do arquivo
					//gravar o comentario na primeira linha do arquivo
					abrearqleit($caminho,$fp);

					fseek($fp,0,SEEK_END);

					fputs($fp,$comentario,32);
					fclose($fp);
				} 	

				
			}//fim do if que compara para ver se tem arquivos associados (s� abre se tiver)
		} //fim do for dos exemplos
 	}//fim do for que percorre a matriz de conteudo
}//fim da funcao


#***********************************************************************************************
#Funcao para arrumar arquivos HTML de Exerc�cio
#***********************************************************************************************
function arrumahtmlexercicio($conteudo,$CodigoDisciplina,$id_usuario,$DOCUMENT_ROOT)
{
	//Tamanho da matriz
	$tam=sizeof($conteudo);

	for($i=0;$i<$tam;$i++)
	{
            $qtde_exerc=sizeof($conteudo[$i]["EXERCICIO"]);

	    for($e=0;$e<$qtde_exerc;$e++)
	    {
		//Tamanho da matriz de arquivos associados para exercicio
	      	$tamarqassocexerc=sizeof($conteudo[$i]["EXERCICIO"][$e]["EXERCICIOASSOC"]);

		//Nome do arquivo HMTL que deve ser aberto
		$arq_html=$conteudo[$i]["EXERCICIO"][$e]["ARQEXER"];

		$caminho=$DOCUMENT_ROOT."/"."disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_html;

		if($tamarqassocexerc>0)
		{
		      	abrearqleit($caminho,$fp);
			$arquivo_todo=fread($fp,filesize($caminho));

			$comentario="<!-- Arquivo com caminho ok -->";
			$achou_comentario=strpos($arquivo_todo,$comentario);

			fclose($fp);
					
	      		for ($narq=0;$narq<$tamarqassocexerc;$narq++)
			{
				$arruma_exer=1;
				$arquivo_todo=(string) $arquivo_todo;
				//trocar aqui pelo caminho do exercicio
	          		$arq_assoc=$conteudo[$i]["EXERCICIO"][$e]["EXERCICIOASSOC"][$narq]["ARQEXERASSOC"];
				$nome=$arq_assoc;
		
				// Alterado por Veronice
				if ($achou_comentario === false)
				   {	
					$nome=$arq_assoc;
				   }
				else
			      	   {	      			
			      		$nome = "../disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;	
				   }// fim do if do comentario
				
				 //armazena o caminho do arquivo exceto o arquivo associado
				 // alterado por Veronice				
				$caminho_arq_assoc="";
				$caminho_arq_assoc="../disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;			
							
				$arquivo_todo=eregi_replace($nome,$caminho_arq_assoc,$arquivo_todo);
				
				
			}//fim do for da matriz com os arquivos associados

			// Alterado por Veronice
			if ($achou_comentario === false)
			{	
				abrearqesc_HTML($caminho,$fp);
			      	fwrite($fp,$arquivo_todo);
				fclose($fp);

				//posicionar o ponteiro na primeira linha do arquivo
				//gravar o comentario na primeira linha do arquivo
				abrearqleit($caminho,$fp);
				fseek($fp,0,SEEK_END);
				fputs($fp,$comentario,32);
				fclose($fp);
			}	

		}//fim do if que compara se tem arquivo associado (so faz troca do caminho do html se tiver)
	     }//fim do for com quantidade de exercicios
 	}//fim do for que percorre a matriz de conteudo
}//fim da funcao

#***********************************************************************************************
#Funcao para arrumar arquivos HTML de Material Complementar
#***********************************************************************************************
function arrumahtmlmatcomp($conteudo,$CodigoDisciplina,$id_usuario,$DOCUMENT_ROOT)
{
	//Tamanho da matriz
	$tam=sizeof($conteudo);
	for($i=0;$i<$tam;$i++)
	{

            $qtde_mt=sizeof($conteudo[$i]["MATCOMP"]);
	    for($e=0;$e<$qtde_mt;$e++)
	    {
		//Tamanho da matriz de arquivos associados para material complementar
      		$tamarqassocmatcomp=sizeof($conteudo[$i]["MATCOMP"][$e]["MATCOMPASSOC"]);
		//Nome do arquivo HMTL que deve ser aberto
		$arq_html=$conteudo[$i]["MATCOMP"][$e]["ARQMATCOMP"];
		$caminho=$DOCUMENT_ROOT."/"."disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_html;
		
		if($tamarqassocmatcomp>0)
		{
		        abrearqleit($caminho,$fp);
		        $arquivo_todo=fread($fp,filesize($caminho));

		        $comentario="<!-- Arquivo com caminho ok -->";
		        $achou_comentario=strpos($arquivo_todo,$comentario);

		        fclose($fp);

	      		for ($narq=0;$narq<$tamarqassocmatcomp;$narq++)
			{
				$arruma_mat=0;
				$arquivo_todo=(string) $arquivo_todo;
				//trocar aqui pelo caminho do exercicio
        		        $arq_assoc=$conteudo[$i]["MATCOMP"][$e]["MATCOMPASSOC"][$narq]["ARQASSOCMAT"];
				$nome=$arq_assoc;
				
				// alterado por Veronice
				if ($achou_comentario === false)
				{	
				    $nome=$arq_assoc;
				}
				else
			      	{	      			
			      	    $nome = "../disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;	
				}// fim do if do comentario
	
				//armazena o caminho do arquivo exceto o arquivo associado
				// alterado por Veronice				
				$caminho_arq_assoc="";
				$caminho_arq_assoc="../disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$arq_assoc;			
							
				$arquivo_todo=eregi_replace($nome,$caminho_arq_assoc,$arquivo_todo);
				
			}//fim do for da matriz com os arquivos associados

			// Alterado por Veronice
			if ($achou_comentario === false)
			{	
				abrearqesc_HTML($caminho,$fp);
			        fwrite($fp,$arquivo_todo);
				fclose($fp);

				//posicionar o ponteiro na primeira linha do arquivo
				//gravar o comentario na primeira linha do arquivo
				abrearqleit($caminho,$fp);
				fseek($fp,0,SEEK_END);
				fputs($fp,$comentario,32);
				fclose($fp);
			}	
			
									
		}//fim do if que verifica se tem arquivo associado (so faz se tiver)
	     }//fim do for que conta o numero de materiais complementares
 	}//fim do for que percorre a matriz de conteudo
	return($arruma_mat);
}//fim da funcao


#***********************************************************************************************
#Funcao Geral - chama as demais funcoes.
#***********************************************************************************************
function arrumageral($conteudo,$CodigoDisciplina,$id_usuario,$DOCUMENT_ROOT)
{
        //echo "Arruma arquivos html de conceito da autoria";
        //echo "<BR>\n";
        //echo "................";
        //echo "<BR>\n";
	arrumahtmlconceito($conteudo,$CodigoDisciplina,$id_usuario,$DOCUMENT_ROOT);

	//echo "Acabou de arrumar arquivos html de conceito....";
        //echo "<BR>\n";

        //echo "Arruma arquivos html de Exemplo da autoria";
        //echo "<BR>\n";
        //echo "................";
        //echo "<BR>\n";
	arrumahtmlexemplo($conteudo,$CodigoDisciplina,$id_usuario,$DOCUMENT_ROOT);

        //echo "Acabou de arrumar arquivos html de exemplo....";
        //echo "<BR>\n";


	//echo "Arruma arquivos html de Exercicio da autoria";
        //echo "<BR>\n";
        //echo "................";
        //echo "<BR>\n";
	arrumahtmlexercicio($conteudo,$CodigoDisciplina,$id_usuario,$DOCUMENT_ROOT);

        //echo "Acabou de arrumar arquivos html de exercicio....";
        //echo "<BR>\n";


	//echo "Arruma arquivos html de Material Complementar da autoria";
        //echo "<BR>\n";
        //echo "................";
        //echo "<BR>\n";

	arrumahtmlmatcomp($conteudo,$CodigoDisciplina,$id_usuario,$DOCUMENT_ROOT);
        //echo "Acabou de arrumar arquivos html de material complementar....";
        //echo "<BR>\n";
}
?>
</HTML>