<?
/* -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Inform�tica  
 *       UEL - Departamento de Computa��o
 * -----------------------------------------------------------------------
 *       @package Estruturador de conte�do   
 *     @subpakage Cadastro de conceito
 *          @file a_topicos_verifica.php
 *    @desciption Verificar existencia de topicos
 *         @since 25/06/2003
 *        @author Veronice de Freitas (veronice@jr.eti.br)
 * -----------------------------------------------------------------------         
 */ 
 
 session_start();
 
 // inclui arquivo de configura��es
 include "config/configuracoes.php";
  
 // inclui arquivo de fun��es 
 include "include/funcoes.php"; 

 global $conteudo, $id_usuario, $email_usuario;


 $CodigoDisciplina = $_POST['CodigoDisciplina'];
 

 LeMatriz($CodigoDisciplina);
 

 $conteudo = $_SESSION['conteudo'];

  if ($conteudo[0]['DESCTOP'] <> "") 
  {
       	$destino="Location: a_index.php?opcao=TopicosEstruturarArvore&CodigoDisciplina=".$CodigoDisciplina;
  }
  else 
  {
  	$destino="Location: a_index.php?opcao=TopicosCadastro1&CodigoDisciplina=".$CodigoDisciplina."&numtopico=1";
  }

header($destino);

?>


