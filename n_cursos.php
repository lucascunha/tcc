<?php

global $logado, $id_usuario, $email_usuario, $A_LANG_IDIOMA_USER, $tipo_usuario, $id_aluno, $tipo;
global $num_disc, $num_curso, $id_prof, $caminho, $disciplina;

session_start();
$logado 		=	$_SESSION['logado'];
$id_usuario		=	$_SESSION['id_usuario'];
$email_usuario 	=	$_SESSION['email_usuario'];
$A_LANG_IDIOMA_USER=$_SESSION['A_LANG_IDIOMA_USER'];
$tipo_usuario	=	$_SESSION['tipo_usuario'];
$status_usuario	=	$_SESSION['status_usuario'];
$id_aluno		=	$_SESSION['id_aluno'];
$tipo   		=	$_SESSION['tipo'];

 unset($_SESSION['curso']);
 unset($_SESSION['disciplina']);


include "config/configuracoes.php"; 

$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;
include "include/conecta.php";
  

if (($tipo_usuario == "root") || ($tipo_usuario == "professor"))
{  
  $orelha = array(
  		array(   
   		   "LABEL" => A_LANG_LENVIRONMENT_WATCH_LIBERATED, 
     		   "LINK" => "n_index_navegacao.php?opcao=AssistirDisciplinas",    		                                 
     		   "ESTADO" => "ON"
   		    ), 		   
  		array(   
   		   "LABEL" => A_LANG_LENVIRONMENT_WATCH_MINE,  
     		   "LINK" => "n_index_navegacao.php?opcao=DisciplinasAutor",    		                                 
     		   "ESTADO" => "OFF"
   		   ) 		   		     		       		   		   
   		  ); 
}
else
{
  $orelha = array(
  		array(   
   		   "LABEL" => A_LANG_LENVIRONMENT_WATCH_MY, 
     		   "LINK" => "",    
     		   "ESTADO" => "ON"
   		   )		     		       		   		  
   		  ); 

}
 
MontaOrelha($orelha);  
  
?>



<table CELLSPACING=5 CELLPADDING=5 border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> height="80%" style="height:380px;" >
	<tr valign="top">
		<td>

			<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">
				<tr>
					<td>
						<?php
						
						if ($tipo_usuario=="aluno" )
						   {
							$linhas=0;
							$linhas1=0;
							?>
						
							<p align="right">
						
							<?php

							$sql="Select curso.id_curso, curso.nome_curso, disciplina.id_disc, disciplina.nome_disc, disciplina.status_xml, disciplina.status_disc,disciplina.id_usuario,usuario.nome_usuario from matricula,curso,disciplina,usuario";
							$sql.=" where matricula.id_usuario='$id_aluno' and curso.id_curso=matricula.id_curso and disciplina.id_disc=matricula.id_disc and matricula.status_mat=1 and usuario.id_usuario=disciplina.id_usuario";
							$res=mysql_query($sql,$id);
							$erro=mysql_error();
							echo $erro;
							$linhas=mysql_num_rows($res);
						  	} // if aluno
						
						if (($tipo_usuario=="professor") || ($tipo_usuario=="root"))
						{
							$linhas=0;
							$linhas1=0;
							?>
							<p align="left">
							
							<?php
							$sql1="Select curso.id_curso,curso.nome_curso,disciplina.id_disc,disciplina.nome_disc, disciplina.status_xml from curso_disc,curso,disciplina";
							$sql1.=" where curso.id_usuario='$id_aluno' and disciplina.id_usuario='$id_aluno' and curso.id_curso=curso_disc.id_curso and disciplina.id_disc=curso_disc.id_disc order by disciplina.nome_disc";
							$res1=mysql_query($sql1,$id);
							$linhas1=mysql_num_rows($res1);
							
							$sql="select curso.id_curso,curso.nome_curso, disciplina.id_disc,disciplina.nome_disc, ";
							$sql.="disciplina.status_xml,disciplina.status_disc,disciplina.id_usuario,usuario.nome_usuario ";
							$sql.="from matricula,curso,disciplina,usuario where matricula.id_curso=curso.id_curso and ";
							$sql.="matricula.id_disc=disciplina.id_disc and matricula.id_usuario='$id_aluno' ";
							$sql.="and matricula.status_mat=1 and usuario.id_usuario=disciplina.id_usuario order by disciplina.nome_disc , curso.nome_curso, usuario.nome_usuario ";
						
							$res=mysql_query($sql,$id);
							$linhas=mysql_num_rows($res);
						}
							
						if (($linhas==0 and $tipo_usuario=="aluno")|| ($linhas1==0 and $linhas==0 and $tipo_usuario=="professor"))
						{
						?>
			         	<table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0">
			              <tr>
			                <td>  
			 			   		           <div class="Mensagem_Amarelo">
						   		             <?
						   		             echo "<p class=\"texto1\">\n";     
						   		             echo "<table cellspacing=\"0\" cellpadding=\"0\" width=80%>";
						   		             echo "  <tr>";
						   		             echo "    <td width=\"50\">";
						   		             echo "      <img src=\"imagens/icones/warning.png\" alt=\"\" />";
						   		             echo "    </td>";
						   		             echo "    <td valign=\"center\">";
						   		             
						   		             echo "Nenhuma disciplina liberada. Solicite matr�cula para ter acesso �s disciplinas.";
						   		             echo "    </td>";
						   		             echo "  </tr>";
						   		             echo "</table>";  
						   		             echo "</p>\n";      
						   		             ?>    
						   		           </div>
						   		           <br>
			                  </td>
			                </tr>       
			       		 </table>  
   
						<?php
						}
						else
	    				{
						if ($linhas>0)
						{
						?>	
	  							<div class="Mensagem_Ok">
	   								<?
	  									echo "<p class=\"texto1\">\n";	   
										echo "<table cellspacing=\"0\" cellpadding=\"0\">";
										echo "	<tr>";
										echo "		<td width=\"50\">";
										echo "			<img src=\"imagens/icones/notice.png\" alt=\"\" />";
										echo "		</td>";
										echo "		<td valign=\"center\">";
										echo "Abaixo est�o listadas todas as disciplinas com a matr�cula j� autorizada pelo professor." ;	
										echo "		</td>";
										echo "	</tr>";
										echo "</table>";	
	   									echo "</p>\n";	   	
	    							?>	  
	    						</div><br>
						  <div align="center">
						  	<br>
						  	<table CELLSPACING=0 CELLPADDING=1 BGCOLOR="#ffffff" WIDTH="100%"  border="0" style="padding-left:5px;" >  		  				    
						  		<tr class=Titulo style='height:40px;'>

						  			<td class="redondo_inicio" style="background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; " >
						  			<? echo "Disciplina"; ?>
						  			</td>
						
						  			<td style="background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; ">
						  			<? echo "Curso"; ?>
						  			</td>
						
						  			<td class='redondo_final' style="background: #666699; border-width:0 1px 1px 0; border-color: #666699; border-style : solid; ">
						  			<? echo A_LANG_REQUEST_ACCESS_TYPE_TEACHES; ?>
						  			</td>
						
								</tr>
						
						  			<?php
						   		  
									$nCor = 1;
						
									 
									require_once "avaliacao/funcao/aluno.func.php";
						
						   		   	$conn = &ADONewConnection($A_DB_TYPE);
						   		   	$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
						
						   		   	$aval_liberada = avaliacao_liberada($conn,$id_aluno);
						
									for ($j=0;$j<$linhas;$j++) 
									{				
									    		
										$disciplina=mysql_result($res,$j,3);
										$curso=mysql_result($res,$j,1);
										$num_disc=mysql_result($res,$j,2);
										$num_curso=mysql_result($res,$j,0);
										$st_xml=mysql_result($res,$j,4);
										$st_disc=mysql_result($res,$j,5);
										$id_prof=mysql_result($res,$j,6);
										$nome_prof=mysql_result($res,$j,7);

										if ($nCor == 0) 
										{
										       $nCor = 1;
										       $cStyle = "zebraA";
										}
										else	
										{
											$nCor = 0;
										 	$cStyle = "zebraB";						
										}									

										if ($st_xml==1 and $st_disc==1)
										{
										   	
				   		
											?>
							
											<tr class=<?=$cStyle;?>>
												<td width="45%" class='redondo_inicio'>
													<?php
						    						    if(in_array($num_disc,$aval_liberada)) {
						    						      echo $disciplina." (Conte�do trancado, avalia��o liberada)";
						    						    } else {
						    						      echo "<a  href='n_index_navegacao.php?opcao=Navega&id_prof=$id_prof&num_disc=$num_disc&num_curso=$num_curso&disciplina=$disciplina&curso=$curso' class='tooltip' title='Acessar a Disciplina'>$disciplina</a><sub>&nbsp;</sub></small></font>";
						    						    }
													?>						
												</td>		
												<td width="30%">	
													<?
														echo $curso;
													?>
												</td>
												<td width="25%" class='redondo_final'>	
													<?	
														echo $nome_prof;	
													?>	
												</td>
											</tr>
											<?php
							  			}//fecha o if
							  			else
							  			{
											?>
							
											<tr class=<?=$cStyle;?>>
												<td width="45%" class='redondo_inicio'>
													<?php
													    echo "<tooltip class='tooltip' title='A disciplina ainda n�o foi liberada pelo professor'>$disciplina<sub>&nbsp;</sub></small></font>";
						    						  
													?>						
												</td>		
												<td width="30%">	
													<?
														echo $curso;
													?>
												</td>
												<td width="25%" class='redondo_final'>	
													<?	
														echo $nome_prof;	
													?>	
												</td>
											</tr>
											<?php
	
							  			}
								}//fecha o for
							echo "</table><br></div>";	
						}//fecha o if principal
						else
						{ ?>
						
        				 <table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0">
        				      <tr>
        				        <td>  
 							   		           <div class="Mensagem_Amarelo">
							   		             <?
							   		             echo "<p class=\"texto1\">\n";     
							   		             echo "<table cellspacing=\"0\" cellpadding=\"0\" width=80%>";
							   		             echo "  <tr>";
							   		             echo "    <td width=\"50\">";
							   		             echo "      <img src=\"imagens/icones/warning.png\" alt=\"\" />";
							   		             echo "    </td>";
							   		             echo "    <td valign=\"center\">";
							   		             
							   		             echo "Nenhuma disciplina liberada. Solicite matr�cula para ter acesso �s disciplinas.";
							   		             echo "    </td>";
							   		             echo "  </tr>";
							   		             echo "</table>";  
							   		             echo "</p>\n";      
							   		             ?>    
							   		           </div>
							   		           <br>
        				          </td>
        				        </tr>       
				
        				</table>  
				
						<?php }

						} //linha >0
					?>

</td>
</tr>
</table><br>
</td>
</tr>
</table>


