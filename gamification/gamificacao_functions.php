<?php
// ini_set('display_errors', 1); error_reporting(-1);
include_once(__DIR__."/../analytics/functions.php");
// include_once "../analytics/functions.php";

function atualizaPerfilGam($id_curso, $id_disc, $id_usuario, $acao, $options = []) {
  echo 'atualizando';
  switch($acao) {
    case 'EscreveuRecado':
      escreveuRecado($id_curso, $id_disc, $id_usuario);
      break;
    case 'AcessouConceito':
      acessouConceito($id_curso, $id_disc, $id_usuario, $options['conceito'], $options['total-topicos']);
      break;
    case 'AcessouMaterial':
      acessouMaterial($id_curso, $id_disc, $id_usuario, $options['material']);
      break;
    case 'UtilizouForum':
      utilizouForum($id_curso, $id_disc, $id_usuario);
      break;
    default:
      return('Ação inválida!');
  }
  verificarRanking($id_curso, $id_disc, $id_usuario);
  verificarAcesso($id_curso, $id_disc, $id_usuario);
}

function verificarRanking($id_curso, $id_disc, $id_usuario) {
  echo 'verificouRanking';
  $conn = conectaAdodb();

  $sql = "SELECT usuario.id_usuario, gamificacao_perfil_usuario.pontos
          FROM gamificacao_perfil_usuario
          INNER JOIN usuario ON gamificacao_perfil_usuario.id_usuario = usuario.id_usuario
          WHERE id_curso = '$id_curso'
          AND id_disc = '$id_disc'
          ORDER BY pontos DESC";
  $alunos = $conn->GetAll($sql);
  for($i = 0; $i < 3; $i++) {
    if($alunos[$i]['id_usuario'] == $id_usuario) {
      premiarConquista($id_curso, $id_disc, $id_usuario, $conquista = 'Competidor');
    }
  }
}

function verificarAcesso($id_curso, $id_disc, $id_usuario) {
  echo 'verificouAcesso';
  $info = wa_query('Frequencia_acessos_por_aluno', 390, '378', '', '', '', 1223);
  if(sizeof($info) >= 30) {
    premiarConquista($id_curso, $id_disc, $id_usuario, $conquista = 'Veterano');
  }
}

function escreveuRecado($id_curso, $id_disc, $id_usuario) {
  echo 'escreveu';
  $info = wa_query('Recados_enviados_por_aluno', $id_curso, $id_disc, '', '', '', $id_usuario);
  echo $info[0]['total_recados'];
  if ($info[0]['total_recados'] == 1) {
    incrementarPontos($id_curso, $id_disc, $id_usuario, 50);
    premiarConquista($id_curso, $id_disc, $id_usuario, $conquista = 'Pombo Correio');
  }
}

function acessouConceito($id_curso, $id_disc, $id_usuario, $conceito, $total_conceitos) {
  echo 'acessou';
  if(strpos($conceito, '.xml') === false) return; // Certifica que o conceito está sendo acesso via o link
  $info = wa_query('Acesso_conceitos_por_aluno', $id_curso, $id_disc, '', '', '', $id_usuario, $conceito);
  $conceitos = wa_query('Acesso_todos_conceitos_por_aluno', $id_curso, $id_disc, '', '', '', $id_usuario);
  if(sizeof($conceitos) >= $total_conceitos) {
    premiarConquista($id_curso, $id_disc, $id_usuario, $conquista = 'Estudioso');
  }
  if(sizeof($info) == 0) {
    incrementarPontos($id_curso, $id_disc, $id_usuario, 50);
  }
}

function acessouMaterial($id_curso, $id_disc, $id_usuario, $material) {
  echo 'acessouMaterial';
  $info = wa_query('Acesso_materiais_por_aluno', $id_curso, $id_disc, '', '', '', $id_usuario, $material);
  if(sizeof($info) == 0) {
    incrementarPontos($id_curso, $id_disc, $id_usuario, 25);
  }
}


function utilizouForum($id_curso, $id_disc, $id_usuario) {
  echo 'utilizou';
  incrementarPontos($id_curso, $id_disc, $id_usuario, 100);
  $info = wa_query('Forum_topicos_criacao_por_aluno', $id_curso, $id_disc, '', '', '', $id_usuario, $conceito);
  $total = sizeof($info);
  $info = wa_query('Forum_topicos_resposta_por_aluno', $id_curso, $id_disc, '', '', '', $id_usuario, $conceito);
  for($i = 0; $i < sizeof($info); $i++) {
    $total += $info[$i]['TOTAL_PARTICIPACOES'];
  }
  if($total >= 5) {
    echo 'premiou social 5';
    premiarConquista($id_curso, $id_disc, $id_usuario, $conquista = 'Social');
  }
  if($total >= 10) {
    echo 'premiou social 10';
    premiarConquista($id_curso, $id_disc, $id_usuario, $conquista = 'Social++');
  }
}

function premiarConquista($id_curso, $id_disc, $id_usuario, $conquista) {
  echo 'premiar';
  $conn = conectaAdodb();

  $sql = "SELECT id
          FROM gamificacao_perfil_usuario
          WHERE id_curso = '$id_curso'
          AND id_disc = '$id_disc'
          AND id_usuario = '$id_usuario'";
  $perfil = $conn->GetAll($sql);

  $sql = "SELECT id, pontos
          FROM gamificacao_conquista
          WHERE nome = '$conquista'";
  $conquista = $conn->GetAll($sql);

  $sql = "SELECT id
          FROM gamificacao_conquista_obtida
          WHERE id_perfil_usuario = '".$perfil[0]['id']."'
          AND id_conquista = '".$conquista[0]['id']."'";
  $conquista_obtida = $conn->GetAll($sql);

  if(sizeof($conquista_obtida) != 0) return $conn->Close(); // Retorna se o usuário já possuir a conquista

  $sql = "INSERT INTO gamificacao_conquista_obtida (id_perfil_usuario, id_conquista)
          VALUES ('".$perfil[0]['id']."', '".$conquista[0]['id']."')";
  $rs = $conn->Execute($sql);

  incrementarPontos($id_curso, $id_disc, $id_usuario, $conquista[0]['pontos']);
  $conn->Close();
}

function incrementarPontos($id_curso, $id_disc, $id_usuario, $pontos) {
  echo 'incrementarPontos';
  $conn = conectaAdodb();
  $sql = "UPDATE gamificacao_perfil_usuario
          SET pontos = pontos + $pontos
          WHERE id_disc = '$id_disc'
          AND id_curso = '$id_curso'
          AND id_usuario = '$id_usuario'";
  $rs = $conn->Execute($sql);
  $conn->Close();
}
?>
