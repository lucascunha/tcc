<?php

ini_set('display_errors', 1); error_reporting(-1);

global $aluno, $perfil, $num_curso, $num_disc;

$num_disc   = $_SESSION['num_disc'];
$num_curso  = $_SESSION['num_curso'];
$curso      = $_SESSION['curso'];
$id_prof    = $_SESSION['id_prof'];
$disciplina = $_SESSION['disciplina'];

$conn = &ADONewConnection('mysql');
$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

$sql = "SELECT nome_usuario
        FROM usuario
        WHERE id_usuario = '$id_usuario'";
$aluno = $conn->GetAll($sql);

$sql = "SELECT * FROM gamificacao_conquista";
$conquistas = $conn->GetAll($sql);

$sql = "SELECT *
        FROM gamificacao_perfil_usuario
        WHERE id_curso = '$num_curso'
        AND id_disc = '$num_disc'
        AND id_usuario = '$id_aluno'";
$perfil = $conn->GetAll($sql);
$nivel = (sqrt(625+(100*$perfil[0]['pontos']))-25)/50;

$id_perfil = $perfil[0]['id'];

$sql = "SELECT id_conquista
        FROM gamificacao_conquista_obtida
        WHERE id_perfil_usuario = '$id_perfil'";
$id_conquistas_obtidas = $conn->GetAll($sql);

$sql = "SELECT *
        FROM gamificacao_config
        WHERE id_disc = '$num_disc'";
$config = $conn->GetAll($sql);
$status = $config[0]['status'];
$ranking = $config[0]['ranking'];

$sql = "SELECT usuario.nome_usuario, gamificacao_perfil_usuario.pontos
        FROM gamificacao_perfil_usuario
        INNER JOIN usuario ON gamificacao_perfil_usuario.id_usuario = usuario.id_usuario
        WHERE id_curso = '$num_curso'
        AND id_disc = '$num_disc'
        ORDER BY pontos DESC";
$alunos = $conn->GetAll($sql);

$conquistas_obtidas = array();
for($i = 0; $i < sizeof($id_conquistas_obtidas); $i++){
  $conquistas_obtidas[] = $id_conquistas_obtidas[$i]['id_conquista'];
}
?>

<?
$orelha = array();
$orelha = array(
array(
   "LABEL" => "<a  class=menu href=n_index_navegacao.php?opcao=Navega&id_prof=".$id_prof."&num_disc=".$num_disc."&num_curso=".$num_curso."&disciplina=".$disciplina."&curso=".urlencode($curso).">".A_LANG_LENVIRONMENT_NAVTYPE."</a>",
     "LINK" => "n_index_navegacao.php?opcao=Navega",
     "ESTADO" =>"OFF"
    ),
array(
     "LABEL" => A_LANG_LENVIRONMENT_MURAL,
       "LINK" => "n_index_navegacao.php?opcao=MuralRecados",
       "ESTADO" => "OFF"
     ),
//Alteracao Claudia da Rosa (UDESC) em 13/09/2008 para Fórum de Discussão
 array(
   "LABEL" => "<a  class=menu href=n_index_navegacao.php?opcao=ForumDiscussao&id_prof=".$id_prof."&num_disc=".$num_disc."&num_curso=".$num_curso."&disciplina=".$disciplina."&curso=".urlencode($curso).">".A_LANG_LENVIRONMENT_FORUM."</a>",
     "LINK" => "n_index_navegacao.php?opcao=ForumDiscussao",
     "ESTADO" => "OFF"
   ),
  array(
    'LABEL' => 'Perfil do Aluno',
    'LINK' => 'n_index_navegacao.php?opcao=PerfilAluno',
    'ESTADO' => 'ON'
  )
);
MontaOrelha($orelha);
?>

<?php
  if($status === 'ativada') { ?>
    <div class='main' style='background: white'>
      <div class="topo">
        <div class="perfil">
          <div class="nome"><?php echo $aluno[0]['nome_usuario']; ?></div>
          <div><img src=""></div>
          <div class="pontos">Pontos: <?php echo $perfil[0]['pontos']; ?></div>
          <div class="level">Nivel <?php echo floor($nivel); ?></div>
        </div>
      </div>

      <div class="conquistas clearer">
      <?php for($i = 0; $i < sizeof($conquistas); $i++) { ?>
        <div class="conquista <?php
        if(in_array($conquistas[$i]['id'], $conquistas_obtidas)){
          echo 'obtida';
        } else {
          echo 'nao-obtida';
        }
        ?>">
          <div class="medalha"><img src="<?php echo $conquistas[$i]['caminho_imagem'];?>"></div>
          <div class="descricao">
            <div class="pontos"><?php echo $conquistas[$i]['pontos']; ?></div>
            <div class="nome"><?php echo $conquistas[$i]['nome']; ?></div>
            <div class="descr"><?php echo $conquistas[$i]['descricao']; ?></div>
          </div>
        </div>
      <?php }
      ?>

      </div>

      <?php // echo $aluno[0]['nome_usuario']; ?>

      <div class="ranking">
        <h2> Ranking </h2>
        <?php for($i = 0; $i < min($ranking, sizeof($alunos)); $i++) { ?>
          <div class="item">
            <span class="posicao"></span><?php echo $i+1 ?>&deg; - </span>
            <?php echo $alunos[$i]['nome_usuario']?>
          </div>
        <?php }
        ?>
      </div>
    </div>
  <?php } else { ?>
    <div class='main' style='background: white'>
      <h1>A gamificação está desativada para esta disciplina!</h1>
    </div>
  <?php }
?>



<!-- Inclui os arquivos CSS necessários -->
<link href="<?php echo $DOCUMENT_SITE ?>/gamification/css/gamification.css" type="text/css" rel="stylesheet"/>
