<?
/* -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Inform�tica  
 *       UEL - Departamento de Computa��o
 * -----------------------------------------------------------------------
 *       @package Estruturador de conte�do -    
 *     @subpakage Integra ambiente de autoria - armazenamento em XML  
 *          @file a_topicos_gerar_conteudo_xml.php
 *    @desciption Integra��o do ambiente de autoria com o m�dulo de arma-
 *                zenamento em XML       
 *         @since 25/06/2003
 */ 
?>
<HTML>
<html>
<head>
<LINK REL="StyleSheet" HREF="css/geral.css" TYPE="text/css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/blitzer/jquery-ui.css" type="text/css" />

<script type="text/javascript" src="src/jquery.floatingmessage.js"></script>
<script language="JavaScript" type="text/JavaScript" src="javascript/ajaxlib.js" ></script>
<script type="text/javascript" src="javascript/jquery_notification_v.1.js"></script>
<link href="javascript/jquery_notification.css" type="text/css" rel="stylesheet"/>	

<BODY>
<br><br><br><br><br>
 	<?php

  echo "<div id='loading'> <br><center><img src=\"images/loader.gif\" border=\"0\"><br>Processando...</center> <br><br></div>"; 

  	  $tipo_msg = "success";
      $icone = "<img src='imagens/icones/notice.png' border='0' />";
   		?>
  			<script type="text/javascript">
    		  showNotification({
    		  message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo 'Iniciando Verifica��o...'; ?></td></tr></table>",              
    		  type: "<? echo $tipo_msg; ?>",
    		  autoClose: true,
          duration: 5                                  
    		  });
    		  //error //success //warning
  			</script>  
  			
  
<TABLE WIDTH="100%" HEIGHT="100%" BORDER="0" CELLSPACING="10" CELLPADDING="0" bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?>>
	<tr valign=top>
		<td >
    <?
    $CodigoDisciplina = $_GET['CodigoDisciplina'];
    $id_usuario = $_GET['id_usuario'];
    $verificar = $_GET['verificar'];
    // Arquivo de configura��es
    include "config/configuracoes.php"; 	
          
    // Arquivo de fun��es - Veronice
    include "include/funcoes.php"; 
    
    // arquivos de gera��o de arquivos XML - Marilia
    include "include/a_topico_geracao_xml.php";
        
    // Arquivo para arrumar HTML do Autor - Marilia 
    include "a_topico_arruma_html_autor.php";

    global $A_DB_TYPE, $A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB;
    
    // Troca de idioma
    if ($A_LANG_IDIOMA_USER == "")
      include "idioma/".$A_LANG_IDIOMA."/geral.php";
    else   
      include "idioma/".$A_LANG_IDIOMA_USER."/geral.php";


      $tipo_msg = "success";
      $icone = "<img src='imagens/icones/notice.png' border='0' />";
      ?>
        <script type="text/javascript">
          showNotification({
            message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo 'Gerando Conte�do...'; ?></td></tr></table>",              
            type: "<? echo $tipo_msg; ?>",
            autoClose: true,
                duration: 5                                  
          });
          //error //success //warning
        </script>  
        

    <?php

    $conteudo = GerarConteudo($CodigoDisciplina);               

        $tipo_msg = "success";
        $icone = "<img src='imagens/icones/notice.png' border='0' />";

      ?>
        <script type="text/javascript">
          showNotification({
          message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo 'Verificando Estrutura...'; ?></td></tr></table>",              
          type: "<? echo $tipo_msg; ?>",
          autoClose: true,
          duration: 5                                  
          });
          //error //success //warning
        </script>  
    <?php

	   $controle_erro=verificamatriz($conteudo,$CodigoDisciplina);

     $errogeral=0;
     
     $tipo_msg = "success";
     $icone = "<img src='imagens/icones/notice.png' border='0' />";
   		?>
  			<script type="text/javascript">
    	    showNotification({
    		  message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo 'Verificando Conceito...'; ?></td></tr></table>",              
    		  type: "<? echo $tipo_msg; ?>",
    		  autoClose: true,
          duration: 5                                  
    		  });
    		  //error //success //warning
  			</script>  
 
  	<?php

     $conceito=verificaerroconceito($conteudo);

	    $tipo_msg = "success";
      $icone = "<img src='imagens/icones/notice.png' border='0' />";
   		?>
  			<script type="text/javascript">
     		  showNotification({
   		    message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo 'Verificando Exemplo...'; ?></td></tr></table>",              
   		    type: "<? echo $tipo_msg; ?>",
   		    autoClose: true,
          duration: 5                                  
    		  });
    		  //error //success //warning
  			</script>  
        <br><br>

  	<?php

     $exemplo=verificaerroexemplo($conteudo);

  	    $tipo_msg = "success";
        $icone = "<img src='imagens/icones/notice.png' border='0' />";
   		?>
  			<script type="text/javascript">
    		  showNotification({
   		    message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo 'Verificando Exerc�cio...'; ?></td></tr></table>",              
   		    type: "<? echo $tipo_msg; ?>",
   		    autoClose: true,
          duration: 5                                  
    		  });
    		  //error //success //warning
  			</script>  
        <br><br>

  	<?php

     $exercicio=verificaerroexercicio($conteudo);


  	    $tipo_msg = "success";
        $icone = "<img src='imagens/icones/notice.png' border='0' />";
   		?>
  			<script type="text/javascript">
    		  showNotification({
   		    message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo 'Verificando Material Complementar...'; ?></td></tr></table>",              
   		    type: "<? echo $tipo_msg; ?>",
   		    autoClose: true,
          duration: 5                                  
    		  });
    		  //error //success //warning
  			</script>  
      <br><br>

  	<?php

     $matcomp=verificaerromatcomp($conteudo);
     if (($conceito!=0) || ($exercicio!=0) || ($exemplo!=0) || ($matcomp!=0))
     {
            $errogeral++;
     }
     

  $controle_erro =  $errogeral;

	if ($controle_erro == 0)
	{
	
        	xmlesttop($conteudo,$CodigoDisciplina,$id_usuario,$DOCUMENT_ROOT);

	        geraxml($conteudo,$CodigoDisciplina,$id_usuario,$DOCUMENT_ROOT);        

		if ($controle_html==0)
		{
			// arrumageral($conteudo,$CodigoDisciplina,$id_usuario,$DOCUMENT_ROOT);
		}

		// atualiza '1' em status_xml para indicar que os XML foram gerados 
		$conn = &ADONewConnection($A_DB_TYPE); 
		$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
		$sql="update disciplina set status_xml='1' where id_disc='$CodigoDisciplina';";
		$rs = $conn->Execute($sql);     
		
		if ($rs === false) die(' ');  
		     
		$rs->Close(); 	

          $tipo_msg = "success";
          $icone = "<img src='imagens/icones/success.png' border='0' />";
        ?>
        <script type="text/javascript">
          showNotification({
            message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo A_LANG_GENERATION_SUCCESS; ?></td></tr></table>",              
            type: "<? echo $tipo_msg; ?>",
            autoClose: false
                                             
          });
          //error //success //warning
        </script>  
        <?php


		
	}
	else
	{
        	echo "<BR>\n";
		echo(A_LANG_GENERATION_DATA_LACK);
		
		// atualiza '0' em status_xml para indicar que os XML n�o foram gerados
		$conn = &ADONewConnection($A_DB_TYPE); 
		$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
		$sql="update disciplina set status_xml='0', status_disc='0' where id_disc='$CodigoDisciplina';";
		$rs = $conn->Execute($sql);     
				
		if ($rs === false) die(' ');  
		     
		$rs->Close(); 


      $tipo_msg = "error";
      $icone = "<img src='imagens/icones/error.png' border='0' />";
      ?>
        <script type="text/javascript">
          showNotification({
          message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo A_LANG_GENERATION_DATA_LACK; ?></td></tr></table>",              
          type: "<? echo $tipo_msg; ?>",
          autoClose: false,
          duration: 50                                  
          });
          //error //success //warning
        </script>  
    <?php
	}                                 
    ?>   

		</td>
	</tr>
</table>
    
</BODY>    
</HTML>

<script language="javascript">
var element = document.getElementById("loading");
var element1 = document.getElementById("selection");

        element.style.display = "none";
 //         element.textContent = "fdfdfdf";
        element1.style.display = "block";


</script>

