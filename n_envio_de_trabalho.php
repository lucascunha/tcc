<?
/** -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Informática  
 *       UEL - Departamento de Computação
 * -----------------------------------------------------------------------
  *       @package Navegação
 *     @subpakage EnviarTrabalhos
 *          @file n_envio_de_trabalho.php
 *    @desciption  Envio de trabalhos
 *         @since 17/08/2003
 *        @author Veronice de Freitas (veronice@jr.eti.br) 
 * -----------------------------------------------------------------------
 */ 

if (isset($Enviar))
{ 
	$conn = &ADONewConnection($A_DB_TYPE); 
	$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	//$sql = "INSERT INTO trabalho (id_usuario, id_disc, arquivo, data, id_trabalho_disc) values (".$id_usuario.",".xxIDDISCIPLINAxxxxxx.",".$img.",".time().",".$nID_TRAB_DISC.")";
	// echo $sql;										
	$rs = $conn->Execute($sql);     	
	if ($rs === false) die(A_LANG_DISCIPLINES_MSG1);  				     
	$rs->Close(); 	
}

?>

<? 
  $orelha = array();  
  $orelha = array( 		   
  		array(   
   		   "LABEL" => A_LANG_DISCIPLINES_REGISTER , 
     		   "LINK" => "", 
     		   "ESTADO" =>"ON"
   		   )  		       		       		       		   		   
   		  ); 

  MontaOrelha($orelha);  

?>

<table border="0" bgcolor="#93bee2" width="100%"  height="80%">


<tr valign="top">

<td>   


<br>
<br>
<br>	

	      	  <form name = "Trabalho" action="n_index_navegacao.php?opcao=EnvioTrabalho" method="post">
	      	
	      	
	      			<table border="0">
	      				<tr>
						<td align="right">	
		 				  	<? echo A_LANG_DISCIPLINES_COURSE_DISCIPLINES; ?>:
						</td>		
																								
																								
						<td>
 							<?
		
								  $conn = &ADONewConnection($A_DB_TYPE); 
		  						  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
							  		
		  						  $sql = "SELECT * FROM disciplina where id_usuario = ".$id_usuario." order by nome_disc";
		  						  $nID_DISC = 'disciplina.iddisc';
								  $rs = $conn->Execute($sql);							  									  		  						 		  						 
								  ?>
				
								 						 									 	
								 <br>
								
								 <select class="select" size="1" name="nID_DISC" onchange="EnviarTrabalho()">
		
 	 							  <? 
	    								 
	    								while (!$rs->EOF)
		     							{	
	                							if ($rs->fields[0] == $nID_DISC) 	         							    
	         								    	echo "<option selected value=\"".$rs->fields[0]."\">".$rs->fields[1]."</option>";
	         								else	         						
	         							    		echo "<option value=\"".$rs->fields[0]."\">".$rs->fields[1]."</option>";
	     												 	         								
	                							$rs->MoveNext();
	                							
	                							
	             							}
	             
	             						        $rs->Close(); 
	         						  ?>      	
	         						  </select>								
								 									 									
						  </td>
						  							 	
				      </tr>	
				      
				       <? if (isset($nID_DISC)) 
					    {
				        ?>   	  
				      <tr>
								
						 <td align="right">
						     <? echo A_LANG_WORKS; ?>:
						  </td>
						  	  	
						  </td>	
						  <td> 	
								 <br>
								 	
								 							 
								  <select class="select" size="1" name="nID_TRAB_DISC">
		
 	 							  <? 
	    								//$sql2 = "SELECT * FROM trabalho_disc where id_usuario = ".$id_usuario." and id_disc=".$nID_DISC." order by nome_trab";
	    								$sql2 = "SELECT * FROM trabalho_disc where id_usuario = ".$id_usuario." order by nome_trab"; // falta a identificacao da id_disc
	    								echo $sql2;	    								
								  	$rs2 = $conn->Execute($sql2);
		  		
			  						  if ($rs2 === false) die(A_LANG_NOT_DISC);  
	    							
	    								while (!$rs2->EOF)
		     							{	
	                							if ($rs->fields[0] == $nID_DISC) 	         							    
	         								    	echo "<option selected value=\"".$rs2->fields[0]."\">".$rs2->fields[1]."</option>";
	         								else	         						
	         							    		echo "<option value=\"".$rs2->fields[0]."\">".$rs2->fields[1]."</option>";
	     												 	         								
	                							$rs2->MoveNext();
	                							
	                							
	             							}
	             
	             						        $rs2->Close(); 
	         						  ?>      	
	         						  </select>							
												
						</td>		      					
	      						      						      						      			
	      				</tr>	   
	      				   													
					
			<tr>	   
				  		
	    		 <td width="130" align="right" valign="top">
	    	    		<? echo A_LANG_TOPIC_FILE1; ?>:		    	    		
	    		 </td>
			 <td>	    			    									
				<?			
				while ((strpos($img_name[0], " ")))
					$img_name[0][(strpos($img_name[0], " "))]="_";	
											
				if (!$nfile) 
				   $nfile++;
								   																			    
				    for ($i = sizeof($img)-1;$i >= 0;$i--)
				    {
					if ($img[$i] != '' and $img[$i] != 'none' and $img_size[$i] < 124000)
					   copy($img[$i], "disciplinas/".$id_usuario."/".$CodigoDisciplina."/".$img_name[$i]);
				    }
		
				    while ($nfile--)
				    {
				    ?>
					  <input class="button" type="File" width="90" name="img[]">														
				    <?
				    }
										
				    ?>
							  
		 		   <input class="button" type="submit" name= <? echo A_LANG_TOPIC_SEND ?> value="Enviar">
			   </td>
										
  	  	 </tr>  	
				
				
		<?
		 } // if
		?>			
					
		</table>
				   
 	<br>
 	<br>
 	<br>


	</form>
</td>
</tr>
</table>

<script language="javascript">
/* Funcao script ------------- ListaCurso() ----------  */
function EnviarTrabalho()
{
    document.Trabalho.submit()
}
</script>