<div id="filtro" class="limpar filtro bottom20 padding10">
	<form id="frm_filtro">
		<div class="bottom10 esquerda">
			<!-- Curso -->
			Curso:
			<select name="params[curso]" class="slt">
				<option value="">Selecione um curso...</option>
				<?php
				$select='SELECT id_curso AS id,nome_curso AS nome FROM curso WHERE id_usuario='.$id_usuario.' ORDER BY nome ASC';
				$cursos=mysql_query($select);
				if(mysql_num_rows($cursos)>0){
					while($curso=mysql_fetch_object($cursos))
						echo '<option value="',$curso->id,'">',$curso->nome,'</option>';
				}
				?>
			</select>
			<!-- Disciplina -->
			Disciplina:
			<select name="params[disciplina]" class="slt">
				<option value="">Selecione uma disciplina...</option>
				<?php
				$select='SELECT id_disc AS id,nome_disc AS nome FROM disciplina WHERE id_usuario='.$id_usuario.' ORDER BY nome ASC';
				$disciplinas=mysql_query($select);
				if(mysql_num_rows($disciplinas)>0){
					while($disciplina=mysql_fetch_object($disciplinas))
						echo '<option value="',$disciplina->id,'">',$disciplina->nome,'</option>';
				}
				?>
			</select>
			<!-- Per�odo -->
			Per�odo:
			de <input size="10" class="data txt" name="params[periodo_de]" value="<?=date('d/m/Y',strtotime('90 days ago'))?>"/>
			a <input size="10" class="data txt" name="params[periodo_ate]" value="<?=date('d/m/Y')?>"/>
		</div>
		<div class="direita">
			<input type="submit" value="Gerar gr�ficos" class="btn"/>
		</div>
		<div class="limpar">
			<div id="lbl_metricas">
				<span id="mostrar-metricas"><img src="analise-interativa/img/mostrar.png" title=""/> Mostrar todas as m&eacute;tricas</span>
				<span id="ocultar-metricas"><img src="analise-interativa/img/ocultar.png" title=""/> Ocultar todas as m&eacute;tricas</span>
				<span id="metrica-selecionada">m�trica selecionada</span>
			</div>
			<table id="metricas" class="top10" cellpadding="0" cellspacing="0" width="100%">
				<tr>
				<?php
				$select='SELECT id,nome FROM vi_categoria ORDER BY id ASC';
				$categorias=mysql_query($select);
				if(mysql_num_rows($categorias)>0){
					while($categoria=mysql_fetch_object($categorias)){
						echo '<td valign="top">';
						echo '<p class="bottom10"><strong id="categoria-',$categoria->id,'">',$categoria->nome,'</strong></p>';
						$select='SELECT id,nome FROM vi_metrica WHERE id_categoria='.$categoria->id.' ORDER BY id ASC';
						$metricas=mysql_query($select);
						if(mysql_num_rows($metricas)>0){
							while($metrica=mysql_fetch_object($metricas)){
								echo '<p><label><input type="radio" name="metrica" value="',$metrica->id,'" rel="',$categoria->id,'"',($metrica->id==1)?' checked="checked"':'','/> <span id="metrica-'.$metrica->id.'">',$metrica->nome,'</span></label></p>';
							}
						}
						echo '</td>';
					}
				}
				?>
				</tr>
			</table>
		</div>
	</form>
</div>