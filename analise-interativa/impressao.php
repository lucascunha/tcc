<html>
	<head>
		<meta charset="iso-8859-1" />
		<title>An�lise Interativa - Impress�o</title>
	</head>

	<body>
		<div class="padding-all-20">
			<div class="border-all padding-all-10 not-print margin-bottom-20">
				Imprimir: 
				<label><input type="checkbox" name="print[]" value="hdn_cht" checked="checked" /> Gr�fico</label>
				<label><input type="checkbox" name="print[]" value="hdn_dts" checked="checked" /> Detalhes</label>
				<input type="button" value="Imprimir" id="btn_print">
			</div>

			<?php
			include_once('conexao.php');
			include_once('funcoes.php');
			include_once('../config/configuracoes.php');
			
			if($curso = getCourse($_GET['curso']))
				echo '<strong>Curso: </strong>', $curso->nome, '<br/>';

			if($disciplina = getDiscipline($_GET['disciplina']))
				echo '<strong>Disciplina: </strong>', $disciplina->nome, '<br/>';

			if($_GET['periodo_de'])
				echo '<strong>Per�odo de: </strong>', $_GET['periodo_de'], '&nbsp;&nbsp;&nbsp;&nbsp;';

			if($_GET['periodo_ate'])
				echo '<strong>Per�odo at�: </strong>', $_GET['periodo_ate'], '<br/><br/>';

			//$is_impressao = true;
			//include('resultados.php');
			?>
			<div id="results"></div>
		</div>	

		<!-- Inclui os arquivos CSS necess�rios -->
		<link href="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/datatables/media/css/jquery.dataTables.css" type="text/css" rel="stylesheet"/>
		<link href="<?php echo $DOCUMENT_SITE ?>/analise-interativa/css/interface.css" type="text/css" rel="stylesheet"/>

			<!-- Inclui os arquivos JavaScript necess�rios -->
			<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
			<script type="text/javascript" src="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/flot/excanvas.js"></script>
			<script type="text/javascript" src="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/flot/jquery.flot.min.js"></script>
			<script type="text/javascript" language="javascript" src="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/flot/jquery.flot.pie.min.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/flot/jquery.flot.symbol.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/flot/jquery.flot.orderBars.js"></script>
			<script type="text/javascript" src="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/datatables/media/js/jquery.dataTables.min.js"></script>

		<script>
			$(function() {
				$("input[name='print[]']").click(function() {
					$checked = $("input[name='print[]']");
					length = parseInt($checked.length);
					for (var i=0; i<length; i++) {
						if($checked[i].checked == true){
							$("#" + $checked[i].value).show();
						}else{
							$("#" + $checked[i].value).hide();
						}
					}
				});
				
				$("#btn_print").click(function() {
					window.print();
				});
				
					$("#results").load("<?php echo $DOCUMENT_SITE ?>/analise-interativa/resultados.php", "<?=$_SERVER['QUERY_STRING']; ?>&is_impressao=true", function() {
						$(".print").hide();
					});
			})
		</script>	
	</body>
</html>