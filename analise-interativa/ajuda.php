<html>
	<head>
		<meta charset="utf-8" />
	</head>
	<body>
		<h1 id="inicio">Análise Interativa - Ajuda</h1>
		
		<p><a href="#1">1. Introdução</a></p>
		<p><a href="#2">2. Definição dos Parâmetros da Análise</a></p>
		<p><a href="#3">3. Interação com os Resultados</a></p>
		<p><a href="#4">4. Impressão</a></p>
		
		
		<h2 id="1">1. Introdução</h2>
		<p>O módulo de Análise Interativa permite que você, professor, possa analisar o comportamento de seus alunos frente ao AdaptWeb(R). Desta maneira, você pode ver as ações realizadas por eles e tentar compreendê-los. Este manual tem como objetivo ajudá-lo a entende r o funcionamento deste módulo e as opções disponívels.</p>
		
		<p align="right"><a href="#inicio">Voltar ao início</a></p>
		
		<h2 id="2">2. Definição dos Parâmetros da Análise</h2>
		<p>Os parâmetros que podem ser definidos para analisar os dados coletados são Curso, Disciplina, Período e Métrica. O curso e a Disciplina podem ser selecionados de acordo com os Cursos e Disciplinas de sua autoria. O período é composto por dois campos de entrada que devem ser preenchidos com duas datas (no formato dd/mm/aaaa) delimitadoras.  Um detalhe importante é que, se não for definido um período, a ferramenta entenderá que devem ser pesquisados os últimos 90 dias.</p>
		
		<p>As métricas definem os dados que serão analisados. É possível escolher entre um total de 20 métricas, divididas em quatro categorias:</p>
		
		<ul>
			<li><strong>Uso Geral:</strong>  Total de Visitas por Aluno, Tempo Médio de Acesso dos Alunos, Frequência de Acesso, Total de Acessos a Cada Seção, Sistema Operacional, Navegadores, e Resoluções de Tela;</li>
			<li><strong>Ambiente de Aula:</strong> Modo de Navegação, Total de Usos do Sistema de Busca, Palavras-chave Pesquisadas, Total de Acessos aos Conceitos, Total de Acessos aos Exercícios, Total de Acessos aos Exemplos e Total de Acessos aos Materiais Complementares;</li>
			<li><strong>Fórum de Discussão:</strong> Total de Acessos aos Tópicos, Total de Tópicos Criados e Total de Respostas;</li>
			<li><strong>Mural de Recados:</strong>  Total de Recados Enviados, Tipos de Recados Enviados e Total de Visualizações.</li>
		</ul>
		
		<p>Dentre estas métricas, algumas podem ser representadas no mesmo gráfico. É o caso das métricas Total de Acessos aos Conceitos, Total de Acessos aos Exercícios, Total de Acessos aos Exemplos e Total de Acessos aos Materiais Complementares que podem ser representadas no mesmo mapa, assim como das métricas Total de Acessos aos Tópicos e Total de Respostas</p>
		
		<p align="right"><a href="#inicio">Voltar ao início</a></p>
		
		<h2 id="3">3. Interação com os Resultados</h2>
		
		<p>Após definir os parâmetros da análise e clicar no botão "Gerar Gráficos" a(s) métrica(s) selecionadas serão calculadas e mostradas abaixo do filtro. É possível escolher entre três tipos de gráficos: barras, linhas e setores.  Além disso, alguns gráficos permitem a seleção de legendas ou rótulo ou então de agrupamentos.</p>
		 
		<p>Abaixo do gráfico podem ser vistas tabelas com detalhes dos dados. Esta tabela permite filtrar os registros listados e ordená-los alfabeticamente de acordo com a coluna de sua escolha, para isto, basta clicar no cabeçalho desta.</p>
		
		<p>Além destas opções de configuração dos gráficos e de obtenção dos detalhes, no gráfico, ao descansar o ponteiro do mouse sobre as barras, pontos ou setores, um tooltip aparecerá mostrando o que este elemento representa e seu respectivo valor.</p>
		
		<p align="right"><a href="#inicio">Voltar ao início</a></p>
		
		<h2 id="4">4. Impressão</h2>
		
		<p>Para imprimir os resultados, basta clicar em um dos três links disponíveis. Uma nova página irá se abrir. Nesta página você poderá optar por imprimir o gráfico ou os detalhes.</p>
		
		<p>Infelizmente, a ferramenta não permite que estes resultados sejam exportados em PDF. No entanto, caso esta seja a necessidade é possível salvar os resultados como PDF a partir do navegador Google Chrome ou imprimi-los em PDF a partir de plugins como o CutePDF e PDFCreator.</p>
		
		<p align="right"><a href="#inicio">Voltar ao início</a></p>
	</body>
</html>