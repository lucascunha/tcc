<?
ob_start();
session_start();

include_once('funcoes.php');
include_once('conexao.php');
include('../analytics/functions.php');

$id_usuario = $_SESSION['id_usuario'];
$is_impressao = isset($_GET['is_impressao']);

if(count($_GET['metrica']) == 1) {
	$metric = getMetric($_GET['metrica'][0]);
	$category = getCategory($metric->cid);
	$values = wa_query($metric->param, $_GET['curso'], $_GET['disciplina'], $id_usuario, formatDate($_GET['periodo_de']), formatDate($_GET['periodo_ate']));
	if($metric->array) $values = $values[$metric->array];
	$metric_name = $metric->name;
	$metric_category = $category->name;	
} else {
	
	foreach($_GET['metrica'] as $metrica) {
		$metricas[$metrica] = getMetric($metrica);
		$registros[$metrica] = wa_query($metricas[$metrica]->param, $_GET['curso'], $_GET['disciplina'], $id_usuario, formatDate($_GET['periodo_de']), formatDate($_GET['periodo_ate']));
		if(!$category) $category = getCategory($metricas[$metrica]->cid);
		$metric_name[] = $metricas[$metrica]->name;
	}
	
	$metric_name = implode('; ', $metric_name);
	$metric_category = $category->name;
}

if($metric && $metric->mid == 12) {
	$values = urlDecodeValue($values, $metric->label);
}

if(!$is_impressao) { // se n�o for impress�o
	$metric_name = htmlentities($metric_name);
	$metric_category = htmlentities($metric_category);
}
?>

<? if(empty($values) && empty($registros)) { ?>
<h3>N&atilde;o h&aacute; resultados para: <?=$metric_category; ?> - <?=$metric_name; ?></h3>
<? } else { ?>
<h3 class="margin-bottom-20">Resultados para: <?=$metric_category; ?> - <?=$metric_name; ?></h3>

<!-- Op��es -->
<form id="frm_chart">
	<div class="margin-bottom-10 print">
		<div class="left">
			Gr&aacute;fico:
			<select name="grafico" id="slt_chart">
				<?
				if($metric)
					$charts = array_map('trim', explode(',', $metric->charts));
				else
					$charts = array('line', 'pie', 'bar');
				if(in_array('line', $charts)) echo '<option value="line"', ($_GET['grafico'] == 'line') ? ' selected="selected"' : '','>Linhas</option>';
				if(in_array('pie', $charts)) echo '<option value="pie"', ($_GET['grafico'] == 'pie') ? ' selected="selected"' : '','>Setores</option>';
				if(in_array('bar', $charts)) echo '<option value="bar"', ($_GET['grafico'] == 'bar') ? ' selected="selected"' : '','>Barras</option>';
				?>
			</select>
			<? if($metric->mid == 3) { ?>
			Agrupar por:
			<label><input type="radio" name="groupBy" value="dia" <?=($_GET['groupBy'] == 'dia') ? 'checked="checked"' : ''; ?> /> Dia</label>
			<label><input type="radio" name="groupBy" value="semana" <?=($_GET['groupBy'] == 'semana') ? 'checked="checked"' : ''; ?> /> Semana</label>
			<label><input type="radio" name="groupBy" value="mes" <?=($_GET['groupBy'] == 'mes' || !isset($_GET['groupBy'])) ? 'checked="checked"' : ''; ?> /> M&ecirc;s</label>
			<? } ?>
		</div>
		<div class="right margin-bottom-10 print">
			<a href="<?php echo $DOCUMENT_SITE ?>/analise-interativa/impressao.php?<?=$_SERVER['QUERY_STRING']; ?>" target="_blank" class="lnk_imprimir">Imprimir</a>
		</div>
		<div class="clear"></div>
	</div>
	
	<!-- Gr�fico -->
	<div class="border-all padding-all-20 margin-bottom-20" id="hdn_cht">
			<div id="chart"></div>
			<div class="pie_chart print">
				Mostrar:
	
					<label><input type="radio" name="legend" value="true" <?=($_GET['legend'] == 'true' || !isset($_GET['legend'])) ? 'checked="checked"' : ''; ?>/> Legenda</label>
	
				<label><input type="radio" name="legend" value="false" <?=($_GET['legend'] == 'false') ? 'checked="checked"' : ''; ?>/> R&oacute;tulos</label>			
	
			</div>
			
			<div id="tooltip" class="padding-all-10 margin-top-20"></div>
	
	</div>
</form>


<!-- Detalhes -->


		<div class="right margin-bottom-10 print">
			<a href="<?php echo $DOCUMENT_SITE ?>/analise-interativa/impressao.php?<?=$_SERVER['QUERY_STRING']; ?>" target="_blank" class="lnk_imprimir">Imprimir</a>
		</div>
		<div class="clear"></div>

<div id="hdn_dts">
	<? 
	if($metric) {
		include('tabelas/' . $metric->table_file);
		echo '<div class="clear margin-bottom-20"></div>';
	} else
		foreach($metricas as $metrica) {
			echo '<h3>'.htmlentities($metrica->name).'</h3>';
			$values = $registros[$metrica->mid];
			include('tabelas/' . $metrica->table_file);
			$values = NULL;
			echo '<div class="clear margin-bottom-20"></div>';
		}
	?>
</div>

<div class="clear"></div>

<div class="right print">
	<a href="<?php echo $DOCUMENT_SITE ?>/analise-interativa/impressao.php?<?=$_SERVER['QUERY_STRING']; ?>" target="_blank" class="lnk_imprimir">Imprimir</a>
</div>
<div class="clear"></div>


<?
if($metric) {
	if($metric->mid == 1) {
		$values = mapearTotalVisitasAlunos($values);
	} elseif($metric->mid == 2) {
		$values = wa_query('Visitas_por_aluno', $_GET['curso'], $_GET['disciplina'], $id_usuario, formatDate($_GET['periodo_de']), formatDate($_GET['periodo_ate']));
		$metric->value = 'tempo_medio';
		$values = mapearIndiceHora($values, $metric->value);
		$values = retirarIndicesNulos($values, $metric->value);
	} elseif($metric->mid == 3) {
		$fdias = mapearFrequenciaAcessoDia($values['dia']);
		$fsemanas = mapearFrequenciaAcessoSemana($values['semana']);
		$fmeses = mapearFrequenciaAcessoMes($values['mes']);
	} elseif($metric->mid == 5) {
		$values = mapearTotalAcessosCadaSecao($values);
	} elseif($metric->mid == 10) {
		$values = mapearModoNavegacao($values);
	} elseif($metric->mid == 22) {
		$values = mapearTotalRecadosEnviados($values);
	} elseif($metric->mid == 26) {
		$values = mapearTotalTopicosCriados($values);
	} elseif($metric->mid == 27) {
		$values = mapearTotalRespostas($values);
	}
}
?>
<script>
	var previousPoint = null;
	var tickLine, tickBar, tickPie, pieData;
	
	function number_format(number, decimals, dec_point, thousands_sep) {
		number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  	var n = !isFinite(+number) ? 0 : +number,
			prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
			sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
			dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
			s = '',
			toFixedFix = function (n, prec) {
				var k = Math.pow(10, prec);
				return '' + Math.round(n * k) / k;
			};
			s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  		if (s[0].length > 3) {
    		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  		}
  		if ((s[1] || '').length < prec) {
   			s[1] = s[1] || '';
    		s[1] += new Array(prec - s[1].length + 1).join('0');
  		}
  	return s.join(dec);
	}
	
	function formatarHora(val) {
		seconds = val % 60;
		minutes = (val - seconds) / 60;
		minutes = minutes % 60;
		hours = Math.floor(minutes/60);
		if(hours < 10) hours = "0"+hours;
		if(minutes < 10) minutes = "0"+minutes;
		if(seconds < 10) seconds = "0"+seconds;
		return hours+":"+minutes+":"+seconds;
	}
	
	function tooltip(event, pos, item) {
		chart = $("#slt_chart").val();
		if(chart == 'pie') {
			value = item.datapoint[1][0][1];
		} else
			value = item.datapoint[1];
		<? if($metric && $metric->mid == 2) { ?>
		value = formatarHora(value);
		<? } ?>
		
		prefixo = "";
		<? if(isset($registros['27']) && isset($registros['25'])) { ?>
		if(chart != 'pie')
			prefixo = item.series.label + " - ";
		if(item.series.label == "Total de Acessos") {
			sufixo = " acessos(s)";
		}else{
			sufixo = " resposta(s)";
		}
		
		<? } else { ?>
		sufixo = " <?=getSufixoMetrica($metric->mid); ?>";
		<? } ?>
		
		percent = "";
		if(chart == 'pie') {
			label = item.series.label;
			percent = " ("+ number_format(item.series.percent, 2, ',', '.') +"%)";
		} else if(chart == 'bar')
			label = tickBar[item.dataIndex][1];
		else 
			label = tickLine[item.dataIndex][1];

		$("#tooltip").css({ background: item.series.color }).html("<strong>"+prefixo+label+":</strong> " + value + sufixo + percent).show();
	}
	
	function create_chart(chart){
		$(".pie_chart").hide();
		switch(chart){
			<? 
			if(in_array('line', $charts)) {
				if($metric && $metric->mid != 3) {
					$mapping = mapToLineChart($values, $metric->label, $metric->value);
				} elseif($metric->mid == 3) {
					$dias = mapToLineChart($fdias, 'group', 'acessos');
					$semanas = mapToLineChart($fsemanas, 'group', 'acessos');
					$meses = mapToLineChart($fmeses, 'group', 'acessos');	
				} else {	
					$mapping = mapMultipleToLineChart($registros);
				}
			?>
			case 'line':
				<? if($_GET['metrica'] == array(25, 27)) { ?>
				var data = <?=json_encode($mapping['data']); ?>;
				tickLine = <?=json_encode($mapping['ticks']); ?>;
				<? } elseif($metric->mid==3) { ?>
				groupBy = $("input[name='groupBy']:checked").val();
				if(groupBy == 'dia') {
					var data = [<?=json_encode($dias['data']); ?>];
					tickLine = <?=json_encode($dias['ticks']); ?>;
				} else if(groupBy == 'semana') {
					var data = [<?=json_encode($semanas['data']); ?>];
					tickLine = <?=json_encode($semanas['ticks']); ?>;
				} else {
					var data = [<?=json_encode($meses['data']); ?>];
					tickLine = <?=json_encode($meses['ticks']); ?>;
				}
				<? } else { ?>
				var data = [<?=json_encode($mapping['data']); ?>];
				tickLine = <?=json_encode($mapping['ticks']); ?>;
				<? } ?>
				
				$.plot($("#chart"), data, {
					series: {
						lines: {
							show: true
						},
						points: {
							show: true,
							radius: 3,
							fill: 1
						}
					},
					xaxis: {
						autoscaleMargin: 0.05,
						ticks: tickLine,
						tickLength: 0
					},
					<? if($metric && $metric->mid == 2) { ?>
					yaxis: {
						tickFormatter: function(val, axis) {
							return formatarHora(val);
						}
					},
					<? } ?>
					grid: {
						borderWidth: 0,
						hoverable: true
					}
				});
			break;
		<?
		}
		?>
		
		<? 
			if(in_array('bar', $charts)) {
				if($metric && $metric->mid != 3) {
					$mapping = mapToBarChart($values, $metric->label, $metric->value, '');
				} elseif($metric->mid == 3) {
					$mapping['dia']=mapToBarChart($fdias, 'group', 'acessos', '');
					$mapping['semana']=mapToBarChart($fsemanas, 'group', 'acessos', '');
					$mapping['mes']=mapToBarChart($fmeses, 'group', 'acessos', '');					
				} else {
					$mapping = mapMultipleToBarChart($registros);
				}
			?>
			case 'bar':
				<? if($_GET['metrica'] == array(25, 27)) { ?>
				var data = <?=json_encode($mapping['data']); ?>;
				tickBar = <?=json_encode($mapping['ticks']); ?>;
				<? } elseif($metric->mid==3) { ?>
				
				groupBy = $("input[name='groupBy']:checked").val();
				if(groupBy == 'dia') {
					var data = [<?=json_encode($mapping['dia']['data']); ?>];
					tickBar = <?=json_encode($mapping['dia']['ticks']); ?>;
				} else if(groupBy == 'semana') {
					var data = [<?=json_encode($mapping['semana']['data']); ?>];
					tickBar = <?=json_encode($mapping['semana']['ticks']); ?>;
				} else {
					var data = [<?=json_encode($mapping['mes']['data']); ?>];
					tickBar = <?=json_encode($mapping['mes']['ticks']); ?>;
				}
				
				<? } else { ?>
				var data = [<?=json_encode($mapping['data']); ?>];
				tickBar = <?=json_encode($mapping['ticks']); ?>;
				<? } ?>
				
				$.plot($("#chart"), data, {
					<? if($_GET['metrica'] != array(25, 27)) { ?>
					bars: {
						show: true,
						barWidth: 0.5,
						fill: 0.65,
						align: "center"
					},
					<? } ?>
					xaxis: {
						autoscaleMargin: 0.05,
						ticks: tickBar,
						tickLength: 0
					},
					grid: {
						borderWidth: 0,
						hoverable: true
					}
				});
			break;

			<?
			}
			?>
			
			<?
			if(in_array('pie', $charts)) {
				if($metric && $metric->mid != 3) {
					$mapping = mapToPieChart($values, $metric->label, $metric->value);
				} elseif($metric->mid == 3) {
					$mapping['dia']=mapToPieChart($fdias, 'group', 'acessos');
					$mapping['semana']=mapToPieChart($fsemanas, 'group', 'acessos');
					$mapping['mes']=mapToPieChart($fmeses, 'group', 'acessos');					
				} else {
					$mapping = mapMultipleToPieChart($registros);
				}
			?>
			case 'pie':

			$(".pie_chart").show();
			
			
			<? if($metric->mid==3) { ?>
				groupBy = $("input[name='groupBy']:checked").val();
				if(groupBy == 'dia') {
					pieData = <?=json_encode($mapping['dia']); ?>;
				} else if(groupBy == 'semana') {
					pieData = <?=json_encode($mapping['semana']); ?>;
				} else {
					pieData = <?=json_encode($mapping['mes']); ?>;
				}
			<? } else { ?>
			pieData = <?=json_encode($mapping)?>;
			<? } ?>
			
				

				$.plot($("#chart"), pieData, {
					series: {
						pie: {
							show: true
							<? if(count($mapping) > 10) { ?>,
							combine: {
								threshold: 0.1
							}
							<? } ?>
						}
					},
					grid: {
						show: true,
						axisMargin: 0,
						borderWidth:1,
						hoverable: true
					},
					legend: {
						show: $("input[name=legend]:checked").val() == "true"
					}
    		});
			
			
			break;
			<? 
			}
			?>
		
		}
		
		$("#chart").bind("plothover", function(event, pos, item) {
			if(item) {
					previousPoint = item.dataIndex;
					tooltip(event, pos, item);
			} else {
				$("#tooltip").hide();
				previousPoint = null;
			}
		});
	}
	
	$(function() {
		$(".details").dataTable({
			"oLanguage": {
				"sLengthMenu": "Display _MENU_ records per page",
				"sZeroRecords": "Nenhum registro encontrado",
				"sInfo": "_TOTAL_ registros",
				"sInfoEmpty": "",
				"sInfoFiltered": "(filtrados de um total de _MAX_ registros)",
				"sSearch": "Procurar: "
			},
			"bPaginate": false
		});
		
		create_chart($("#slt_chart").val());
		
		$("#slt_chart").change(function() {
			create_chart($(this).val());
		});
		
		$("input[name=legend]").click(function() {
			create_chart($("#slt_chart").val());
		});
		
		<? if($metric->mid == 3) { ?>
		$("input[name=groupBy]").click(function() {
			create_chart($("#slt_chart").val());
			groupBy = $(this).val();
			$(".tbl_group").hide();
			if(groupBy == 'dia') {
				$("#tbl_dia").show();
			} else if(groupBy == 'semana') {
				$("#tbl_semana").show();
			} else {
				$("#tbl_mes").show();
			}
		});
		<? } ?>
		
		$(".lnk_imprimir").click(function(event) {
			event.preventDefault();
			href = $(this).attr("href");
			data = $("#frm_chart").serialize();
			href = href + '&' + data;
			window.open(href);
		});
		
		$("#tooltip").hide();
	})
</script>
<? } ?>