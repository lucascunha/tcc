<?php
include('analise-interativa/conexao.php');
include('analise-interativa/funcoes.php');

//Contr�i as orelhas (abas) da interface
MontaOrelha(array(
	array(
		'LABEL' => A_LANG_ANALISEINTERATIVA,
		'LINK' => 'index.php',
		'ESTADO' => 'ON'
	)
));
?>

<div id="content">
	<div class="right margin-right-20 margin-bottom-10">
		<a href="<?php echo $DOCUMENT_SITE ?>/analise-interativa/ajuda.php" id="lnk_help" target="_blank">Ajuda</a>
	</div>

	<div class="clear padding-all-20 padding-top-0">
		<form id="frm_metric" action="<?php echo $DOCUMENT_SITE ?>/analise-interativa/resultados.php">
			<!-- Filtro -->
			<div class="border-all margin-bottom-10">
				<div class="padding-all-10">
					<div class="margin-bottom-10">
						<!-- Curso -->
						Curso: <? createSelect('curso', 'Todos os cursos', 'Selecione o(s) curso(s) que voc� deseja analisar.', getCourses($_SESSION['id_usuario']), 'cid', 'name', 0); ?>
						
						<!-- Disciplina -->
						Disciplina: <? createSelect('disciplina', 'Todas as disciplinas', 'Selecione a(s) disciplina(s) que voc� deseja analisar.', getDisciplines($_SESSION['id_usuario']), 'did', 'name', 0); ?>
						
						<!-- Per�odo -->
						Per&iacute;odo: de <input size="10" class="data txt" name="periodo_de" id="periodo_de" value="<?=date('d/m/Y',strtotime('90 days ago'))?>" title="Informe o in�cio do per�odo que voc� deseja analisar."/> a <input size="10" class="data txt" name="periodo_ate" id="periodo_ate" value="<?=date('d/m/Y')?>" title="Informe o fim do per�odo que voc� deseja analisar."/>
					</div>
					
					<!-- M�tricas para sele��o -->
					<div>
						<!-- Cabe�alho -->
						<div id="lbl_metrics">
							<span id="show-metrics"><img src="analise-interativa/img/mostrar.png" title=""/> Mostrar todas as m&eacute;tricas</span>
							<span id="hide-metrics"><img src="analise-interativa/img/ocultar.png" title=""/> Ocultar todas as m&eacute;tricas</span>
							<span id="selected-metric" class="margin-left-20"></span>
						</div>
						<div id="metrics">
							<div class="background padding-all-10 margin-bottom-10 margin-top-10">
								<a href="#" id="lnk_limpar" class="margin-bottom-10">Limpar m�tricas selecionadas</a>
								<!-- Lista de m�tricas por categoria -->
								<table id="tbl_metrics" cellpadding="0" cellspacing="0" width="100%">
									<tr>
									<?
									// Busca as categorias nas quais as m�tricas foram divididas
									$categories = getCategories();
									if(numRows($categories) > 0) {
										while($category = fetchObject($categories)) {
											
											// Busca as m�tricas da categoria "listada"
											$metrics = getMetricsByCategory($category->cid);
											
											// Imprime a coluna da categoria com as m�tricas
											echo '<td valign="top"><p class="margin-bottom-10"><strong id="category-', $category->cid, '">', $category->name, '</strong></p>';
											if(numRows($metrics) > 0) {
												// Imprime as m�tricas
												while($metric = fetchObject($metrics)) {
													echo '<p><label><input type="checkbox" name="metrica[]" value="', $metric->mid, '" rel="', $category->cid, '"', ($metric->mid == 1)? ' checked="checked"' : '', '/> <span id="metric-'.$metric->mid.'">', $metric->name, '</span></label></p>';
												}
											}
											echo '</td>';
										}
									}
									?>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="right margin-bottom-10">
				<input type="submit" value="Gerar gr�ficos para an�lise" class="btn"/>
			</div>
			<div class="clear"></div>
		</form>
			
		<!-- Local onde ser�o carregados o gr�fico e os detalhes -->
		<div id="results"></div>
	</div>
</div>

<!-- Inclui os arquivos CSS necess�rios -->
<link href="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/datatables/media/css/jquery.dataTables.css" type="text/css" rel="stylesheet"/>
<link href="<?php echo $DOCUMENT_SITE ?>/analise-interativa/css/interface.css" type="text/css" rel="stylesheet"/>

<!-- Inclui os arquivos JavaScript necess�rios -->
<script type="text/javascript" src="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/masked-input.js"></script>
<script type="text/javascript" src="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/flot/excanvas.js"></script>
<script type="text/javascript" src="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/flot/jquery.flot.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/flot/jquery.flot.symbol.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/flot/jquery.flot.orderBars.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/flot/jquery.flot.pie.min.js"></script>
<script type="text/javascript" src="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/interface.js"></script>
<script type="text/javascript" src="<?php echo $DOCUMENT_SITE ?>/analise-interativa/js/datatables/media/js/jquery.dataTables.min.js"></script>