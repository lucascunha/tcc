<table class="details margin-bottom-20" cellspacing="0" cellpadding="0">
	<thead>
		<th>Palavra-chave</th>
		<th>Total de Buscas</th>
	</thead>
	<tbody>
		<? foreach($values as $value) { ?>
		<tr>
			<td><?=($value['keyword']); ?></td>
			<td><?=htmlentities($value['TOTAL_VIEWS']); ?> buscas</td>
		</tr>
		<? } ?>
	</tbody>
</table>