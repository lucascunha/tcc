<table class="details margin-bottom-20" cellspacing="0" cellpadding="0">
	<thead>
		<th>Curso</th>
		<th>Disciplina</th>
		<th>Exemplo</th>
		<th>Acessos</th>
	</thead>
	<tbody>
		<? foreach($values as $value) { ?>
		<tr>
			<td>
				<?
				if($value['id_curso'] > 0) {
					$curso = getCourse($value['id_curso']);
					echo htmlentities($curso->name);
				}
				?>
			</td>
			<td>
				<?
				if($value['id_disc'] > 0) {
					$disciplina = getDiscipline($value['id_disc']);
					echo htmlentities($disciplina->name);
				}
				?>
			</td>
			<td><?=htmlentities($value['exemplo']); ?></td>
			<td><?=htmlentities($value['TOTAL_VIEWS']); ?> acessos</td>
		</tr>
		<? } ?>
	</tbody>
</table>