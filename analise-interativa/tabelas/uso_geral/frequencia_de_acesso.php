<div id="tbl_dia" class="tbl_group" style="display: none">
	<table class="details margin-bottom-20" cellspacing="0" cellpadding="0">
		<thead>
			<th>Dia</th>
			<th>Curso</th>
			<th>Disciplina</th>
			<th>Total de Acessos</th>
		</thead>
		<tbody>
			<? foreach($values['dia'] as $value) { ?>
			<tr>
				<td><?=htmlentities($value['data']); ?></td>
				<td><?=htmlentities($value['nome_curso']); ?></td>
				<td><?=htmlentities($value['nome_disc']); ?></td>
				<td><?=htmlentities($value['total_acessos']); ?> acessos</td>
			</tr>
			<? } ?>
		</tbody>
	</table>
</div>

<div id="tbl_semana" class="tbl_group" style="display: none">
	<table class="details margin-bottom-20" cellspacing="0" cellpadding="0">
		<thead>
			<th>N&ordm; Semana</th>
			<th>Semana</th>
			<th>Curso</th>
			<th>Disciplina</th>
			<th>Total de Acessos</th>
		</thead>
		<tbody>
			<? foreach($values['semana'] as $value) { ?>
			<tr>
				<td><?=htmlentities($value['num_semana']); ?></td>
				<td><?=htmlentities($value['semana']); ?></td>
				<td><?=htmlentities($value['nome_curso']); ?></td>
				<td><?=htmlentities($value['nome_disc']); ?></td>
				<td><?=htmlentities($value['total_acessos']); ?> acessos</td>
			</tr>
			<? } ?>
		</tbody>
	</table>
</div>

<div id="tbl_mes" class="tbl_group">
	<table class="details margin-bottom-20" cellspacing="0" cellpadding="0">
		<thead>
			<th>M&ecirc;s</th>
			<th>Curso</th>
			<th>Disciplina</th>
			<th>Total de Acessos</th>
		</thead>
		<tbody>
			<? foreach($values['mes'] as $value) { ?>
			<tr>
				<td><?=htmlentities($value['mes']); ?></td>
				<td><?=htmlentities($value['nome_curso']); ?></td>
				<td><?=htmlentities($value['nome_disc']); ?></td>
				<td><?=htmlentities($value['total_acessos']); ?> acessos</td>
			</tr>
			<? } ?>
		</tbody>
	</table>
</div>