<table class="details margin-bottom-20" cellspacing="0" cellpadding="0">
	<thead>
		<th>Navegador</th>
		<th>Vers&atilde;o</th>
		<th>Total de Acessos</th>
	</thead>
	<tbody>
		<? foreach($values as $value) { ?>
		<tr>
			<td><?=htmlentities($value['config_browser_name']); ?></td>
			<td><?=htmlentities($value['config_browser_version']); ?></td>
			<td><?=htmlentities($value['total_browser']); ?> acessos</td>
		</tr>
		<? } ?>
	</tbody>
</table>