<table class="details margin-bottom-20" cellspacing="0" cellpadding="0">
	<thead>
		<th>Resolu&ccedil;&otilde;es</th>
		<th>Total de Acessos</th>
	</thead>
	<tbody>
		<? foreach($values as $value) { ?>
		<tr>
			<td><?=htmlentities($value['config_resolution']); ?></td>
			<td><?=htmlentities($value['total_resolution']); ?> acessos</td>
		</tr>
		<? } ?>
	</tbody>
</table>