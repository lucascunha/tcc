<table class="details margin-bottom-20" cellspacing="0" cellpadding="0">
	<thead>
		<th>Sistema Operacional</th>
		<th>Total de Acessos</th>
	</thead>
	<tbody>
		<? foreach($values as $value) { ?>
		<tr>
			<td><?=htmlentities($value['config_os']); ?></td>
			<td><?=htmlentities($value['total_os']); ?> acessos</td>
		</tr>
		<? } ?>
	</tbody>
</table>