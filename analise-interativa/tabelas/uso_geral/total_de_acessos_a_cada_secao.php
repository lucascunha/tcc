<table class="details margin-bottom-20" cellspacing="0" cellpadding="0">
	<thead>
		<th>Curso</th>
		<th>Disciplina</th>
		<th>Se&ccedil;&atilde;o</th>
		<th>Acessos</th>
		<th>Tempo total</th>
	</thead>
	<tbody>
		<? foreach($values as $value) { ?>
		<tr>
			<td><?=htmlentities($value['nome_curso']); ?></td>
			<td><?=htmlentities($value['nome_disc']); ?></td>
			<td><?=htmlentities($value['secao']); ?></td>
			<td><?=htmlentities($value['TOTAL_VIEWS']); ?> acessos</td>
			<td><?=htmlentities($value['tempo_total']); ?></td>
		</tr>
		<? } ?>
	</tbody>
</table>