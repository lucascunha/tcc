<table class="details margin-bottom-20" cellspacing="0" cellpadding="0">
	<thead>
		<th>Aluno</th>
		<th>Curso</th>
		<th>Disciplina</th>
		<th>Acessos</th>
		<th>Tempo Total</th>
	</thead>
	<tbody>
		<? foreach($values as $value) { ?>
		<tr>
			<td><?=htmlentities($value['nome_usuario']); ?></td>
			<td><?=htmlentities($value['nome_curso']); ?></td>
			<td><?=htmlentities($value['nome_disc']); ?></td>
			<td><?=htmlentities($value['total_visitas']); ?> acessos</td>
			<td><?=htmlentities($value['tempo_total']); ?> horas</td>
		</tr>
		<? } ?>
	</tbody>
</table>