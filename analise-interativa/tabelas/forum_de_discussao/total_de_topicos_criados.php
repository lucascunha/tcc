<table class="details margin-bottom-20" cellspacing="0" cellpadding="0">
	<thead>
		<th>Aluno</th>
		<th>Curso</th>
		<th>Disciplina</th>
		<th>T&oacute;pico</th>
	</thead>
	<tbody>
		<? foreach($values as $value) { ?>
		<tr>
			<td><?=htmlentities($value['nome_usuario']); ?></td>
			<td><?=htmlentities($value['nome_curso']); ?></td>
			<td><?=htmlentities($value['nome_disc']); ?></td>
			<td><?=htmlentities($value['titulo']); ?></td>
		</tr>
		<? } ?>
	</tbody>
</table>