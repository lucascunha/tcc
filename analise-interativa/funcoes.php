<?php

function mapearTotalVisitasAlunos($registros) {
	$mapeamento = array();
	
	foreach($registros as $registro) {
		if(!isset($mapeamento[$registro['id_usuario']])) {
			$mapeamento[$registro['id_usuario']] = array(
				'nome_usuario' => $registro['nome_usuario'],
				'total_visitas' => 0
			);
		}
		$mapeamento[$registro['id_usuario']]['total_visitas'] += $registro['total_visitas'];
	}
	
	return $mapeamento;
}

function mapearFrequenciaAcessoDia($registros) {
	return mapearFrequenciaAcesso($registros, 'data');
}

function mapearFrequenciaAcessoSemana($registros) {
	return mapearFrequenciaAcesso($registros, 'semana');
}

function mapearFrequenciaAcessoMes($registros) {
	return mapearFrequenciaAcesso($registros, 'mes');
}

function mapearFrequenciaAcesso($registros, $group) {
	$dados = array();
	
	foreach($registros as $registro) {
		if(!isset($dados[$registro[$group]])) {
			$dados[$registro[$group]] = array(
				'group' => $registro[$group],
				'acessos' => 0
			);
		}
		
		$dados[$registro[$group]]['acessos'] += $registro['total_acessos'];
	}		
	
	return $dados;
}

function getTopicName($id) {
	$select = 'SELECT titulo FROM topico_forum WHERE id = '.$id;
	if($row = fetchRow($select))
		return $row->titulo;
	return '';
}

function getSufixoMetrica($id) {
	if($id == 11)
		return ' utiliza&ccedil;&atilde;o(&otilde;es)';
	if($id == 12)
		return ' busca(s)';
	if($id == 22)
		return ' recados(s) enviado(s)';
	if($id == 23)
		return ' recados(s)';
	if($id == 26)
		return ' t&oacute;pico(s) criado(s)';
	if($id == 27)
		return ' resposta(s)';
	return ' acesso(s)';
}

function mapMultipleToPieChart($registros) {
	if(isset($registros['27']) && isset($registros['25'])) {
		$registros = getForumDiscussaoValues($registros);
		$mapping = array(
			array(
				'label' => 'Total de Acessos',
				'data' => 0
			),
			array(
				'label' => 'Total de Respostas',
				'data' => 0
			)
		);
		foreach($registros as $registro) {
			$mapping[0]['data'] += $registro['acessos'];
			$mapping[1]['data'] += $registro['respostas'];
		}
	} else {
		$registros = getAmbienteAulaValues($registros);
		$mapping = mapToPieChart($registros, 'label', 'acessos');
	}
	
	return $mapping;
}

function mapMultipleToBarChart($registros) {
	if(isset($registros['27']) && isset($registros['25'])) {
		$registros = getForumDiscussaoValues($registros);
		foreach($registros as $registro) {
				$serie1[] = array($registro['numero'], $registro['acessos']);
				$serie2[] = array($registro['numero'], $registro['respostas']);
				$ticks[] = array($registro['numero'], $registro['topico']);
		}
		$data = array(
			array(
				'label' => 'Total de Acessos',
				'data' => $serie1,
				'bars' => array(
					'show' => true,
					'barWidth' => 0.4,
					'align' => "center",
					'fill' => 0.65,
					'order' => 1
				)
			),
			array(
				'label' => 'Total de Respostas',
				'data' => $serie2,
				'bars' => array(
					'show' => true,
					'barWidth' => 0.4,
					'align' => "center",
					'fill' => 0.65,
					'order' => 2
				)
			)
		);
		
		$mapping = array(
			'data' => $data,
			'ticks' => $ticks
		);
		
	} else {
		$registros = getAmbienteAulaValues($registros);
		$mapping = mapToBarChart($registros, 'label', 'acessos', '');
	}
	return $mapping;

}

function mapMultipleToLineChart($registros) {
	if(isset($registros['27']) && isset($registros['25'])) {
		$registros = getForumDiscussaoValues($registros);
		foreach($registros as $registro) {
				$serie1[] = array($registro['numero'], $registro['acessos']);
				$serie2[] = array($registro['numero'], $registro['respostas']);
				$ticks[] = array($registro['numero'], $registro['topico']);
		}
		$data = array(
			array(
				'label' => 'Total de Acessos',
				'data' => $serie1,
				'points' => array(
					'symbol' => 'circle'
				)
			),
			array(
				'label' => 'Total de Respostas',
				'data' => $serie2,
				'points' => array(
					'symbol' => 'triangle'
				)
			)
		);
		$mapping = array(
			'data' => $data,
			'ticks' => $ticks
		);
	} else {
		$registros = getAmbienteAulaValues($registros);
		$mapping = mapToLineChart($registros, 'label', 'acessos');
	}
	return $mapping;
}

function getForumDiscussaoValues($registros) {
	$dados = array();
	$numero = 1;
	foreach($registros as $metrica => $values) {
		if($metrica == 27) {
			$newIndice = 'respostas';
			$oldIndice = 'TOTAL_PARTICIPACOES';
		} else {
			$newIndice = 'acessos';
			$oldIndice = 'TOTAL_VIEWS';
		}
		foreach($values as $value) {
			if(!isset($dados[$value['id_topico']])) {
				$dados[$value['id_topico']] = array(
					'numero' => $numero,
					'topico' => ($value['titulo']) ? htmlentities($value['titulo']) : htmlentities(getTopicName($value['id_topico'])),
					'acessos' => 0,
					'respostas' => 0,
				);
				$numero++;
			}
			$dados[$value['id_topico']][$newIndice] += $value[$oldIndice];
		}
	}
	return $dados;
}

function getAmbienteAulaValues($registros) {
	$dados = array();
	
	foreach($registros as $metrica => $registro) {
		if($metrica == 14) $label = 'Conceitos';
		if($metrica == 16) $label = 'Exerc�cios';
		if($metrica == 18) $label = 'Exemplos';
		if($metrica == 20) $label = 'Materiais Complementares';
		
		$acessos = 0;
		foreach($registro as $values)
			$acessos += $values['TOTAL_VIEWS'];
			
		$dados[] = array(
			'label' => $label,
			'acessos' => $acessos
		);
	}
	
	return $dados;
}

function mapearIndiceHora($registros, $indice) {
	$length = count($registros);
	for($i = 0; $i < $length; $i++){
		$registros[$i][$indice] = toInt($registros[$i][$indice]);
	}
	
	return $registros;
}

function retirarIndicesNulos($registros, $indice) {
	$dados = array();
	foreach($registros as $registro)
		if($registro[$indice] > 0)
			$dados[] = $registro;
	return $dados;
}

function mapearTotalAcessosCadaSecao($registros) {
	$mapeamento = array(
		array(
			'secao' => 'Ambiente de Aula',
			'total' => 0
		),
		array(
			'secao' => 'F&oacute;rum de Discuss�o',
			'total' => 0
		),
		array(
			'secao' => 'Mural de Recados',
			'total' => 0
		)
	);
	foreach($registros as $registro) {
		if($registro['secao'] == 'Ambiente Aula - Modo Livre' || $registro['secao'] == 'Ambiente Aula - Modo Tutorial') {
			$mapeamento[0]['total'] += $registro['TOTAL_VIEWS'];
		} elseif($registro['secao'] == 'Mural de Recados') {
			$mapeamento[1]['total'] += $registro['TOTAL_VIEWS'];
		} elseif($registro['secao'] == 'F�rum de Discuss�o') {
			$mapeamento[2]['total'] += $registro['TOTAL_VIEWS'];
		}
	}
	return $mapeamento;
}

function mapearModoNavegacao($registros) {
	$mapeamento = array(
		array(
			'modo' => 'Modo Livre',
			'total' => 0
		),
		array(
			'modo' => 'Modo Tutorial',
			'total' => 0
		)
	);
	foreach($registros as $registro) {
		if($registro['secao'] == 'Ambiente Aula - Modo Livre') {
			$mapeamento[0]['total'] += $registro['TOTAL_VIEWS'];
		} elseif($registro['secao'] == 'Ambiente Aula - Modo Tutorial') {
			$mapeamento[1]['total'] += $registro['TOTAL_VIEWS'];
		}
	}
	return $mapeamento;
}

function mapearTotalRecadosEnviados($registros) {
	$mapeamento = array();
	
	foreach($registros as $registro) {
		if(!isset($mapeamento[$registro['id_usuario']])) {
			$mapeamento[$registro['id_usuario']] = array(
				'nome_usuario' => $registro['nome_usuario'],
				'total_recados' => 0
			);
		}
		$mapeamento[$registro['id_usuario']]['total_recados'] += 1;
	}
	
	return $mapeamento;
}

function mapearTotalTopicosCriados($registros) {
	$mapeamento = array();
	
	foreach($registros as $registro) {
		if(!isset($mapeamento[$registro['id_usuario']])) {
			$mapeamento[$registro['id_usuario']] = array(
				'aluno' => $registro['nome_usuario'],
				'total' => 0
			);
		}
		$mapeamento[$registro['id_usuario']]['total'] += 1;
	}
	
	return $mapeamento;
}

function mapearTotalRespostas($registros) {
	$mapeamento = array();
	
	foreach($registros as $registro) {
		if(!isset($mapeamento[$registro['id_topico']])) {
			$mapeamento[$registro['id_topico']] = array(
				'topico' => $registro['titulo'],
				'total' => 0
			);
		}
		$mapeamento[$registro['id_topico']]['total'] += $registro['TOTAL_PARTICIPACOES'];
	}
	
	return $mapeamento;
}

function urlDecodeValue($registros, $indice) {
	$length = count($registros);
	for($i = 0; $i < $length; $i++) {
		$registros[$i][$indice] = (urldecode($registros[$i][$indice]));
	}
	
	return $registros;
}

function corrigirLabelTempoMedioAcessoAlunos($registros) {
	$length = count($registros);
	for($i = 0; $i < $length; $i++) {
		$registros[$i]['nome_usuario'] = htmlentities($registros[$i]['nome_curso']) . ' -  ' . htmlentities($registros[$i]['nome_disc']) . ' - ' . htmlentities($registros[$i]['nome_usuario']);
	}
	
	return $registros;
};

function query($query) {
	return mysql_query($query);
}

function numRows($rowset) {
	return mysql_num_rows($rowset);
}

function fetchObject($rowset) {
	return mysql_fetch_object($rowset);
}

function fetchRow($select) {
	if($rowset = query($select))
		if(numRows($rowset) > 0)
			return fetchObject($rowset);
	return NULL;
}

function formatDate($date) {
	$date = explode('/', $date);
	$date = array_reverse($date);
	return implode('-', $date);
}

function toInt($hour) {
	list($hours, $minutes, $seconds) = explode(':', trim($hour));	
	$number = ($hours * 60 * 60) + ($minutes * 60) + $seconds;
	return $number;
}

function getCategories(){
	$select = 'SELECT cid, name FROM vi_category ORDER BY cid ASC';
	return query($select);
}

function getCategory($cid) {
	$select = 'SELECT * FROM vi_category WHERE cid = "' . $cid . '"';
	return fetchRow($select);
}

function getMetric($mid) {
	$select = 'SELECT * FROM vi_metric WHERE mid = "' . $mid . '"';
	return fetchRow($select);
}

function getMetricsByCategory($cid) {
	$select = 'SELECT mid, name FROM vi_metric WHERE cid = "' . $cid . '" ORDER BY mid ASC';
	return query($select);
}

function getCourses($pid) {
	$select = 'SELECT id_curso AS cid, nome_curso AS name FROM curso WHERE id_usuario = "' . $pid . '" ORDER BY nome_curso ASC';
	return query($select);
}

function getCourse($id) {
	$select = 'SELECT id_curso AS cid, nome_curso AS name FROM curso WHERE id_curso = "' . $id . '" ORDER BY nome_curso ASC';
	return fetchRow($select);
}

function getDisciplines($pid) {
	$select = 'SELECT id_disc	AS did, nome_disc AS name FROM disciplina WHERE id_usuario = "' . $pid . '" ORDER BY nome_disc ASC';
	return query($select);
}

function getDiscipline($id) {
	$select = 'SELECT id_disc	AS did, nome_disc AS name FROM disciplina WHERE id_disc = "' . $id . '" ORDER BY nome_disc ASC';
	return fetchRow($select);
}

function createSelect($name, $emptyLabel, $title, $rowset, $valueAttr, $labelAttr, $selectedValue = 0){
	echo '<select name="', $name, '" class="slt" title="', $title,'"><option value="">', $emptyLabel, '</option>';
	if(numRows($rowset) > 0)
		while($row = fetchObject($rowset))
			echo '<option value="', $row->$valueAttr, '"', (($row->$valueAttr == $selectedValue) ? ' selected="selected"' : ''), '>', $row->$labelAttr, '</option>';
	echo '</select>';
}

function mapToLineChart($values, $labelAttr, $valueAttr, $function = 'intval') {
	$number = 1;
	$mapping = array(
		'data' => array(),
		'ticks' => array()
	);
	
	foreach($values as $value) {
		$mapping['data'][] = array($number, $function($value[$valueAttr]));
		$mapping['ticks'][] = array($number, htmlentities($value[$labelAttr]));
		$number++;
	}
	
	return $mapping;
}

function mapToBarChart($values, $labelAttr, $valueAttr, $serieName, $function = 'intval') {
	$number = 1;
	$mapping = array(
		'data' => array(),
		'ticks' => array()
	);
	
	foreach($values as $value) {
		$mapping['data'][] = array($number, $function($value[$valueAttr]));
		$mapping['ticks'][] = array($number, htmlentities($value[$labelAttr]));
		$number++;
	}
	$mapping['data'] = array(
		'label' => $serieName,
		'data' => $mapping['data']
	);
	return $mapping;
}

function mapToPieChart($values, $labelAttr, $valueAttr, $function = 'intval') {
	$mapping = array();
	foreach($values as $value)
		$mapping[] = array(
			'label' => htmlentities($value[$labelAttr]),
			'data' => $function($value[$valueAttr]),
		);
	return $mapping;
}