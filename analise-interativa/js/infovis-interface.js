function gerar_graficos_e_detalhes(){
	$("#results").load("analise-interativa/grafico-e-detalhes.php", $("#frm_metrica").serialize());
}
function selecionar_metrica(){
	$checked=$("input[name='metrica']:checked");
	metrica=$("#metrica-"+$checked.val()).html();
	categoria=$("#categoria-"+$checked.attr("rel")).html();
	$("#metrica-selecionada").html("<strong>M&eacute;trica selecionada: </strong>"+categoria+' - '+metrica).show();
}
function mostrar_metricas(){
	$("#ocultar-metricas, #tbl_metricas").show();
	$("#mostrar-metricas, #metrica-selecionada").hide();
}
function ocultar_metricas(){
	$("#ocultar-metricas, #tbl_metricas").hide();
	$("#mostrar-metricas").show();
	selecionar_metrica();
}
$(function(){
	/* Carregar resultados */
	gerar_graficos_e_detalhes();
	$("#frm_filtro").submit(function(event){
		event.preventDefault();
		gerar_graficos_e_detalhes();
	});
	/* Ocultar/mostrar métricas */
	mostrar_metricas();		
	$("#ocultar-metricas").click(function(){
		ocultar_metricas();
	});
	$("#mostrar-metricas").click(function(){
		mostrar_metricas();		
	});
})