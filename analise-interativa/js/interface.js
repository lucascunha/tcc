function desabilitarMetricas() {
	$checked = $("input[name='metrica[]']:checked");
	length = $checked.length;
	if(length > 0) {
		if(length == 1) {
			$("input[name='metrica[]']").attr("disabled", "disabled");
			valor = $checked[0].value;
			if(valor == 25 || valor == 27) {
				$("input[name='metrica[]'][value=25], input[name='metrica[]'][value=27]").removeAttr("disabled");
			} else if(valor == 14 || valor == 16 || valor == 18 || valor == 20) {
				$("input[name='metrica[]'][value=14], input[name='metrica[]'][value=16], input[name='metrica[]'][value=18], input[name='metrica[]'][value=20]").removeAttr("disabled");
			} else {
				$("input[name='metrica[]'][value=" + valor + "]").removeAttr("disabled");
			}
		}
	} else {
		$("input[name='metrica[]']").removeAttr("disabled");
	}
	valor = parseInt(valor);
}

function create_chart_and_details(){
	$("#results").load($("#frm_metric").attr("action"), $("#frm_metric").serialize());
}

function select_metric(){
	$checked = $("input[name='metrica[]']:checked");
	length = $checked.length;
	categoria = $checked[0].getAttribute("rel");
	categoria = $("#category-" + categoria).html();
	if(length == 1) {
		metrica = $checked[0].value;
		metrica = $("#metric-" + metrica).html();
	} else {
		metrica = length + " selecionadas";
	}
	html = "<strong>M�trica selecionada: </strong>" + categoria + ' - ' + metrica;
	$("#selected-metric").html(html).show();
}

function show_metrics(){
	$("#hide-metrics, #metrics").show();
	$("#show-metrics, #selected-metric").hide();
}

function hide_metrics(){
	$("#hide-metrics, #metrics").hide();
	$("#show-metrics").show();
	select_metric();
}

$(function() {
	desabilitarMetricas();
	$(".data").mask("99/99/9999");
	
	/* Carregar resultados */
	create_chart_and_details();
	$("#frm_metric").submit(function(event){
		event.preventDefault();
		minParams = true;
		
		$campo = $("#periodo_de");
		if($campo.val() == "") {
			alert($campo.attr("title"));
			minParams = false;
		}
		
		$campo = $("#periodo_ate");
		if($campo.val() == "") {
			alert($campo.attr("title"));
			minParams = false;
		}
		
		$campo = $("input[name='metrica[]']:checked");
		if($campo.length == 0) {
			alert("Selecione no m�nimo uma m�trica para analisar.");
			minParams = false;
		}
		
		if(minParams)
			create_chart_and_details();
	});
	
	$("input[name='metrica[]']").click(function() {
		desabilitarMetricas();
	});

	/* Ocultar/mostrar m�tricas */
	show_metrics();		
	$("#hide-metrics").click(function(){
		hide_metrics();
	});
	
	$("#show-metrics").click(function(){
		show_metrics();		
	});
	
	$("#lnk_limpar").click(function(event) {
		event.preventDefault();
		$("input[name='metrica[]']").removeAttr("checked").removeAttr("disabled");
	});
	
	$("#lnk_help").click(function(event) {
		event.preventDefault();
		href = $(this).attr("href");
		window.open(href, 'Analise Interativa - Ajuda');
	});
})