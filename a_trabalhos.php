<? 
/* -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Informática  
 *       UEL - Departamento de Computação
 * -----------------------------------------------------------------------
 *       @package Recebimento de trabalho    
 *     @subpakage Recebimento de trabalho 
 *          @file a_trabalhos.php
 *    @desciption  Recebimento de trabalhos
 *         @since 25/06/2003
 *        @author Veronice de Freitas (veronice@jr.eti.br)
 * -----------------------------------------------------------------------         
 */ 

  // Cria a matriz de orelha para apresentação dos trabalhos
  $orelha = array();  
  $orelha = array( 		   
  		array(   
   		   "LABEL" => A_LANG_MNU_WORKS , 
     		   "LINK" => "", 
     		   "ESTADO" =>"ON"
   		   )  		       		       		       		   		   
   		  ); 

  MontaOrelha($orelha);  

?>

<table border="0" bgcolor="#93bee2" width="100%" height="80%">

<tr valign="top" height="30">
	<td>
           <form name = "Trabalhos" action="a_index.php?opcao=Trabalhos" method="post">

		<table border="0"> 
			<tr>
				<td>
		 			<input class="button" type="submit" value="<? echo A_LANG_COURSE_DELETE; ?>" name="Excluir">
				</td>					 				 			
			</tr>
		</table>	

 	</td>
</tr>

<tr  valign="top">
<td>   
		<table border="0">
			<tr>
				<br>
				<td align="right">	
 					<? echo A_LANG_DISCIPLINES_COURSE_DISCIPLINES; ?>: 
				</td>		
																						
																						
				<td>
					<?

						  $conn = &ADONewConnection($A_DB_TYPE); 
  						  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
					  		  						  
  						  $sql = "SELECT * FROM disciplina where id_usuario = ".$id_usuario." order by nome_disc";
						  $rs = $conn->Execute($sql);
  
  						  if ($rs === false) die(A_LANG_NOT_DISC);  

						  if (isset($nID_DISC)) 
						     {								  
						     $sql2 = "SELECT usuario.nome_usuario, usuario.email_usuario, trabalhos.arquivo, trabalhos.descricao, trabalhos.data FROM trabalhos, usuario where usuario.id_usuario = trabalhos.id_usuario and id_disc='".$nID_DISC."'";		  						  		  						  		  						  				 
						     $rs2 = $conn->Execute($sql2);														     
						     }		   		  				
						  ?>
		
						 							 
						  <select class="select" size="1" name="nID_DISC" onchange="AlteraDisciplina()">

						  <? 
							while (!$rs->EOF)
     							{	
        							if ($rs->fields[0] == $nID_DISC) 	         							    
 								    	echo "<option selected value=\"".$rs->fields[0]."\">".$rs->fields[1]."</option>";
 								else	         						
 							    		echo "<option value=\"".$rs->fields[0]."\">".$rs->fields[1]."</option>";
											 	         								
        							$rs->MoveNext();
        							
        							
     							}
     
     						        $rs->Close(); 
 						  ?>      	
 						  </select>							
										
				</td>		      					
					      						      						      			
			</tr>	      			


			<tr>
			       <td colspan=2>
			                <br><br>
			       		<center>	    
					<table CELLSPACING=1 CELLPADDING=1 BORDER=0 BGCOLOR="#009ACD" WIDTH="98%"  border="0" >  		  				    
					<tr BGCOLOR="#009ACD">
			
	 		    			<td align="center" valign="top">
	    		    				<? echo A_LANG_COURSE_DELETE; ?>
		    				</td>

		 		    		<td width="100" align="center" valign="top">
    			    				<? echo A_LANG_REQUEST_ACCESS_TYPE_STUDANT; ?> 
	    					 </td>			    				
	    					<td width="70" align="center" valign="top">
    		    					<? echo A_LANG_REQUEST_ACCESS_EMAIL; ?> 
	    					 </td>	
	 		    			<td  align="center" valign="top">
    		    					<? echo A_LANG_TOPIC_FILE1; ?>
	    					</td>
	 		    			<td  align="center" valign="top">
    		    					<? echo A_LANG_TENVIRONMENT_EXAMPLES_DSC; ?>
	    					</td>			    				 
	 		    			<td width="130" align="center" valign="top">
	    		    				<? echo A_LANG_WORKS_SEND_DATE; ?>
		    				</td>			    				 
					</tr>	
			       
			       
					<?
					if (isset($nID_DISC)) 
					   {	
					       $nCor = 0;								       
					       while (!$rs2->EOF)
     					       {
     					       	  
     					 		if ($nCor == 0) 
				    			{
				       				$nCor = 1;
				       				$cCor = "#CAE1FF";
				    			}
		 		  			else	
		 		    			{
								$nCor = 0;
				 				$cCor = "#BCD2EE";						
				    		 	}	
				    		 	
				    		 	echo "<TR BGCOLOR= $cCor >\n";																	
				    		 	     
				    		 	      

						             echo "<td valign=\"top\" align=\"center\">\n";
  					    		     echo "<input class=\"checkbox\" type=\"checkbox\" name=\"C3\" value=\"ON\">\n";	 
							     echo "</td>\n";

				    		 	      
						             echo "<td valign=\"top\" width=\"100\"  align=\"left\">\n";
  					    		     echo $rs2->fields[0];	 
							     echo "</td>\n";
							     
						             echo "<td valign=\"top\" width=\"100\" align=\"left\">\n";
  					    		     echo $rs2->fields[1];	 
							     echo "</td>\n";
							     
						             echo "<td valign=\"top\" width=\"100\"  align=\"left\">\n";
  					    		     echo $rs2->fields[2];	 
							     echo "</td>\n";
							     
						             echo "<td valign=\"top\" align=\"left\">\n";
  					    		     echo $rs2->fields[3];	 
							     echo "</td>\n";
							     
						             echo "<td valign=\"top\" align=\"left\">\n";
  					    		     echo $rs2->fields[4];	 
							     echo "</td>\n";
							     									     									     
							 echo "</TR>\n";								
     							     					       	  
     					          $rs2->MoveNext();
     					       }	
     					       
        				       $rs2->Close();	
        				       
					   } // if	
					   echo "</table>\n";
					   echo "</center\n>";
					?>   
			       </td>
			</tr>	
		   </table>
				   



	</form>
</td>
</tr>
</table>

<script language="javascript">
/* Funcao script ------------- ListaCurso() ----------  */
function AlteraDisciplina()
{
    document.Trabalhos.submit()
}
</script>