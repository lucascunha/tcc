<?php 
//Dom XML
if (PHP_VERSION>='5')
  require_once('../include/domxml-xml.php');

include "../config/configuracoes.php";

$naveg = "tutorial";
$_SESSION['naveg']  = "tutorial";


global  	$id_aluno,
        	$tipo,
        	$logado,
        	$num_disc,
        	$num_curso,
	        $id_prof,
	        $disciplina,
	        $caminho,
		$naveg,
		$tecno;

session_start();
$logado         = $_SESSION['logado'];
$id_usuario     = $_SESSION['id_usuario'];
$email_usuario  = $_SESSION['email_usuario'];
$A_LANG_IDIOMA_USER=$_SESSION['A_LANG_IDIOMA_USER'];
$tipo_usuario   = $_SESSION['tipo_usuario'];
$status_usuario = $_SESSION['status_usuario'];
$id_aluno       = $_SESSION['id_aluno'];
$tipo           = $_SESSION['tipo'];

$num_disc       = $_SESSION['num_disc'];   
$num_curso      = $_SESSION['num_curso'];  
$curso          = $_SESSION['curso'];      
$id_prof        = $_SESSION['id_prof'];    
$caminho        = $_SESSION['caminho'];    
$disciplina     = $_SESSION['disciplina']; 
$naveg          = $_SESSION['naveg'];    

$cor = $_GET['cor'];
$cor_fundo_DR=$_GET['cor_fundo_DR'];
$acao_DR    = $_GET['acao_DR'];
$categoria  = $_GET['categoria'];
$num_disc   = $_GET['num_disc'];
$num_curso  = $_GET['num_curso'];
$parametro2  = $_GET['parametro2'];
$localizacao  = $_GET['localizacao'];


 if ($A_LANG_IDIOMA_USER == "")
    include "../idioma/".$A_LANG_IDIOMA."/geral.php";
 else   
    include "../idioma/".$A_LANG_IDIOMA_USER."/geral.php";      

/***********************************************************************/
/* AREA DE CONFIGURACOES DE CORES                        	       */
/***********************************************************************/

$cor_padrao  = "$cor"               ;
$cor_fonte   = "#000000"            ;
$titulo_menu = "Conceito"           ;

/***********************************************************************/
/* SETAR CAMINHO ARQUIVO XML                       	    	       */
/***********************************************************************/

        $file =$DOCUMENT_ROOT.$caminho."/estrutura_topico.xml";

/***********************************************************************/
/* SETAR CAMINHO ARQUIVO XML - FIM                 		       */
/***********************************************************************/

/***********************************************************************/
/* CONEXAO COM O BANCO DE DADOS                    		       */
/***********************************************************************/
        
        // Alterado por AUGUSTO SANTOS
        $usuario=$A_DB_USER;
        $senha=$A_DB_PASS;
        $nomebd=$A_DB_DB;
        $array=array();
        include "../include/conecta.php";

/***********************************************************************/
/* FUNCAO UTILIZADO EM TODAS AS FUNCOES DO PROGRAMA                    */
/* ELA CAPTURA OS FILHOS DA ARVORE XML				       */
/***********************************************************************/

function getChildren($node)
{
        $temp = $node->children();
        $collection = array();
        $count=0;
        for ($x=0; $x<sizeof($temp); $x++)
        {
                if ($temp[$x]->type == XML_ELEMENT_NODE)
                        {
                             $collection[$count] = $temp[$x];
                         $count++;
                        }
        }
        return $collection;
}

/***********************************************************************/
/* FUNCAO PARA VERIFICAR SE EXISTE EXEMPLO OU MATCOMPLEMENTAR	       */
/***********************************************************************/

function achar_elemento($pai,$elem,$attri,$valor,$numero_topico, $guia, $guia_campo)
{   global $ac_vt_material;

    $nodes = $pai->children();
    while ($node = array_shift($nodes))
    {
    if ($node->type==XML_ELEMENT_NODE)
    {
      if ($node->tagname=="curso")
      {
       $A=$node->get_attribute($attri);
        if ($A==$valor)
        { //if oo
            $filho_curso = getchildren($node);
            for ($y=0; $y<sizeof($filho_curso); $y++)
            {//for xy
               if ($filho_curso[$y]->type == XML_ELEMENT_NODE)
               {//if xx
                  if (($filho_curso[$y]->tagname)=="elementos")
                  {
                  $filho_elemento = getchildren($filho_curso[$y]);
                  for ($z=0; $z<sizeof($filho_elemento); $z++)
                  {//for xyxx
                     if ($filho_elemento[$z]->type == XML_ELEMENT_NODE)
                     {//if xxxx
                       if (($filho_elemento[$z]->tagname)==$guia)
                       {
                       $A=$filho_elemento[$z]->get_attribute($guia_campo);
                       if ($A=="sim")
                       {
                        return 1;
                       }
                       }
                     } //for xxxx
                  } //if xyxx
                  }
                } //for xx
             } //if xy
        }  //if oo
      }
    }
    }
    return 0;
}

/***********************************************************************/
/* FUNCAO PARA RETIRAR OS TOPICOS QUE NAO SAO DO CURSO		       */
/***********************************************************************/

function achar_curso($pai,$elem,$attri,$valor)
{
    $nodes = $pai->children();
    while ($node = array_shift($nodes))
    {
    if ($node->type==XML_ELEMENT_NODE)
    {
     if ($node->tagname==$elem)
     {
      $A=$node->get_attribute($attri);
      if ($A==$valor)
      {
      return 1;
      }
     }
    }
    }
    return 0;
}

/***********************************************************************/
/* FUNCAO RESPONSAVEL EM HABILITAR OU NAO AS GUIAS SUPERIORES	       */
/* DE ACORDO COM OS PARAMETROS PASSADOS				       */
/***********************************************************************/

function filtro_habilita_guia($pai,$elem,$attri,$valor,$numero_topico,$tag_busca,$tag_busca_atr)
{   global $ac_vt_material;

    $nodes = $pai->children();
    while ($node = array_shift($nodes))
    {
    if ($node->type==XML_ELEMENT_NODE)
    {
      if ($node->tagname=="curso")
      {
       $A=$node->get_attribute($attri);
        if ($A==$valor)
        { //if oo
            $filho_curso = getchildren($node);
            for ($y=0; $y<sizeof($filho_curso); $y++)
            {//for xy
               if ($filho_curso[$y]->type == XML_ELEMENT_NODE)
               {//if xx
                  if (($filho_curso[$y]->tagname)=="elementos")
                  {
                  $filho_elemento = getchildren($filho_curso[$y]);
                  for ($z=0; $z<sizeof($filho_elemento); $z++)
                  {//for xyxx
                     if ($filho_elemento[$z]->type == XML_ELEMENT_NODE)
                     {//if xxxx
                       if (($filho_elemento[$z]->tagname)==$tag_busca)
                       {
                       $A=$filho_elemento[$z]->get_attribute($tag_busca_atr);
                       if ($A=="sim")
                       {
                        return 1;
                       }
                       }
                     } //for xxxx
                  } //if xyxx
                  }
                } //for xx
             } //if xy
        }  //if oo
      }
    }
    }
    return 0;
}

/***********************************************************************/
/* FUNCAO QUE CRIA UM VETOR CONTENDO VALORES DE PREREQUISITOS	       */
/* APENAS ACESSADO NO MODO TUTORIAL				       */
/***********************************************************************/

function filtro_prerequisitos($pai,$numero_topico)
{   global $ac_vt_prereq, $vetor_prereq;

    $nodes = $pai->children();
    while ($node = array_shift($nodes))
    {
    if ($node->type==XML_ELEMENT_NODE)
    {
      if ($node->tagname=="prereq")   //verifica a tag prereq
      {
       	$vetor_prereq[$ac_vt_prereq] = $node -> get_attribute("identprereq");
	//$vetor_prereq = $node -> get_attribute("identprereq");
       	//echo $numero_topico."=".$ac_vt_prereq."===>".$vetor_prereq[$ac_vt_prereq]."<br>";
       	$ac_vt_prereq ++;
      }
    }
    }
    return 0;
}

/***********************************************************************/
/* ESTE FILTRO ACHA TODOS OS TOPICOS DO ARQUIVO XML		       */
/* FUNCAO RECURSIVA PARA GERAR VETORES COMO:			       */
/* CAMINHAMENTO DO SITE, A NAVEGACAO ENTRE IRMAOS E PAIS	       */
/* MENU DE NAVEGACAO, TANTO NO MODO LIVRE E TUTORIAL		       */
/***********************************************************************/

function GeraCaminho($no, $para1, $para2, $quebra)

{       
$categoria = $_GET['categoria'];
 global 	// VARIAVEIS GERAIS
			$id_aluno,
	        	$tipo,
	        	$logado,
	        	$num_disc,
	        	$num_curso,
	        	$id_prof,
	        	$disciplina,
        		$caminho,
	        	$achou_exemplo,
			$naveg, 
			$id,
			$localizacao,

	                // VARIAVEIS PARA GERACAO DO CAMINHAMENTO
	               	$vetor,
        	       	$vetor_caminho,
	               	$ac_vetor_caminho,
	               	$cont,
	               	$topico_num_tratado,
        	       	$topico_descricao,
	               	$para2_tratado,
	               	$topico_num,
	               	$topico_link,
	               	$topico_visitado,
	               	$topico_abreviacao,
	               	$topico_descricao_encodado,
	               	$arquivo_xml,
	               	$guia_exemplo,
	               	$guia_exercicio,
	               	$guia_material,
	               	$parametro3,
	
			// VARIAVEIS PARA PREREQUISITOS
			$ac_vt_prereq,
			$vetor_prereq,
	
	                // VARIAVEIS PARA GERACAO DO MENU
	               	$vetor_topico,
        	       	$ac_vt_topico,
	               	$ct_x,
	               	$identacao,
	               	$flag,
	
	                // VARIAVEIS PARA GERACAO DA NAVEGACAO ENTRE PAIS/FILHOS
	               	$link_pai,
	               	$link_filho,
	               	$link_irmao_ant,
	               	$link_irmao_pos,
	               	$num_curso;

	// LOOP PRINCIPAL DA RECURSAO

        for ($x=0; $x<sizeof($no); $x++)                 //for1
        {
                if ($no[$x]->type == XML_ELEMENT_NODE)   //if1
                {
                if (($no[$x]->tagname)=="topico")        //if2
                {
                        $elemento="curso";
                        $atributo="identcurso";

			//FILTRO POR CURSO
                        $resultado=achar_curso($no[$x],$elemento,$atributo,$num_curso);
                        if ($resultado==1)
                        {
			
			//ARMAZENAMENTO DOS DADOS DO XML EM VARIAVEIS UTILIZADAS NESTA FUNCAO
                        $no[$x]->tagname;
                        $topico_num  = $no[$x]->get_attribute("numtop");
                        $topico_link = $no[$x]->get_attribute("arquivoxml");
                        $topico_link = utf8_decode($topico_link);
                        $pos 	     = strpos($topico_link,".");
                        $ext1 	     = substr($topico_link,$pos+1);
                        $qtde 	     = strlen($topico_link)-(strlen($ext1)+1);
                        $arq_x 	     = substr($topico_link,0,$qtde);
                        $ext2 	     = ".xml";
                        $arquivo_xml = $arq_x.$ext2;
                        $topico_descricao  = $no[$x]->get_attribute("desctop");
                        $topico_descricao  = utf8_decode($topico_descricao);
                        $topico_abreviacao = $no[$x]->get_attribute("abreviacao");
                        $topico_descricao_encodado = urlencode ($topico_descricao);

			// CHAMADAS PARA VERIFICAR QUAIS GUIAS HABILITAR
			if ($parametro3 == $topico_descricao)
                        {
	                        $guia_exemplo  =filtro_habilita_guia($no[$x],"curso","identcurso",$num_curso,$topico_num,"exemplo","possuiexemp");
        	                $guia_exercicio=filtro_habilita_guia($no[$x],"curso","identcurso",$num_curso,$topico_num,"exercicio","possuiexerc");
                	        $guia_material =filtro_habilita_guia($no[$x],"curso","identcurso",$num_curso,$topico_num,"matcomp","possuimatcomp");
                        }

			// INICIO DO BLOCO QUE CRIA VETOR PARA EXIBICAO DO MENU IDENTADO
                        $comprimento = strlen ($topico_num);
                        $identacao = "";

			// <FOR> PARA MONTAR O COMPRIMENTO DA IDENTACAO
                        for ($xx = 0; $xx < $comprimento; $xx ++)
                          {
				if ($xx ==  ($comprimento - 1))
				{
					$identacao = $identacao."<font color=\"gray\"> � </font>";
				}
				else
				{
                           		$identacao = $identacao."<img src = ../imagens/menu/blank.gif>";
				}
                          }

			// ESTE <IF> GERA MENU PARA NAVEGACAO NO MODO TUTORIAL
			if ($naveg == "tutorial") 
			{ 
                        	// CASO O PARAMETRO2 = TOPICO ATUAL, IMPRIME O MENU NA COR VERMELHA
				//if ($para2 == $topico_num)                
				if (($localizacao == "conceito") and ($para2 == $topico_num))               
                        	{
                         		$vetor_topico[$ac_vt_topico] =$identacao."<font color=red>".$topico_descricao."</font>";
		                        $ac_vt_topico ++;
	                        }

				// CASO O PARAMETRO2 <> TOPICO ATUAL, IMPRIME O MENU NAS CORES PRETA, ROXA OU AZUL 
				else
				{
					// CHAMADA PARA MONTAR O VETOR DE PREREQUISITOS
					filtro_prerequisitos($no[$x],$topico_num);

					$result_select = 0;
					$ac_result_select = 0;
		                        for ($xx = 0; $xx < $ac_vt_prereq ; $xx ++)
                			{
       						$select="select id_log from log_usuario where modo_naveg='tutorial' and id_usuario=$id_aluno and id_curso=$num_curso and id_disc=$num_disc and topico='$vetor_prereq[$xx]'";
						mysql_query($select);
						$result_select = mysql_affected_rows();
						if ($result_select > 0)
						{
							$ac_result_select ++;
						}
					//echo "vetor posicao ".$xx."=>".$vetor_prereq[$xx]."<br>";
		                        }
					//echo $topico_descricao."<br>";
					//echo "numero de vetores no XML  = >".$ac_vt_prereq."<br>";
					//echo "numeros de selects no BD  = >".$ac_result_select."<br><br>"; 
					

					
					// CASO NAO ENCONTRAR, O PREREQUISITO NAO EXISTE E O TOPICO ANALISADO DEVE FICAR DESABILITADO NO MENU
					//if ($result_select == 0)
					if (($ac_result_select <> $ac_vt_prereq) and ($topico_num <> 1))
					{
		                         	$vetor_topico[$ac_vt_topico] =$identacao."<font color=black>".$topico_descricao." </font>";
	 		                        $ac_vt_topico ++;
						$ac_vt_prereq = 0;
					}

					// CASO ENCONTRADO DEVE-SE FAZER OUTRO SELECT PARA ANALISAR COR AZUL OU ROXA
					else
					{	
		       				$select="select id_usuario from log_usuario where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disc=$num_disc and topico='$topico_num' and menu='conceito'";
						//$select="select id_usuario from log_usuario_DR where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disciplina=$num_disc and topico='$topico_num' and categoria='Conceito'";
							
						mysql_query($select);
						$result_select=mysql_affected_rows();
						if ($result_select == 0)
						{
				                         $vetor_topico[$ac_vt_topico] =$identacao."<a   class=blue_DR href='javascript:opener.document.location=\"./conceito.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Conceito_pelo_Mapa_em_$categoria\"; self.close();'><font color=blue>".$topico_descricao."</font></a>";
				                         $ac_vt_topico ++;
							 $ac_vt_prereq = 0;
						}
						else
						{
				                         $vetor_topico[$ac_vt_topico] =$identacao."<a class=purple_DR  href='javascript:opener.document.location=\"./conceito.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Conceito_pelo_Mapa_em_$categoria\"; self.close();'><font color=purple>".$topico_descricao."</font></a>";
				                         $ac_vt_topico ++;
							 $ac_vt_prereq = 0;
						}
					}
				}
			}
			else

			// ESTE <IF> GERA MENU PARA NAVEGACAO NO MODO LIVRE
			if ($naveg == "livre") 
			{
                        	// CASO O PARAMETRO2 = TOPICO ATUAL, IMPRIME O MENU NA COR VERMELHA
                        	//if ($para2 == $topico_num)                
				if (($localizacao == "conceito") and ($para2 == $topico_num))               
                        	{
	                         	$vetor_topico[$ac_vt_topico] =$identacao."<font color=red>".$topico_descricao."</font>";
		                        $ac_vt_topico ++;
	                        }
                	        else
        	                {   	
					// SELECT PARA ANALISAR QUAL COR UTILIZAR - AZUL OU ROXA
        		$select="select id_usuario from log_usuario where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disc=$num_disc and topico='$topico_num' and menu='conceito'";
						//$select="select id_usuario from log_usuario_DR where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disciplina=$num_disc and topico='$topico_num' and categoria='Conceito'";
						
					mysql_query($select);
					$result_select=mysql_affected_rows();
					if ($result_select == 0)
					{
		                         	$vetor_topico[$ac_vt_topico] =$identacao."<a   class=blue_DR href='javascript:opener.document.location=\"./conceito.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Conceito_pelo_Mapa_em_$categoria\"; self.close();'><font color=blue>".$topico_descricao."</font></a>";
		                         	$ac_vt_topico ++;
					}
					else
					{
		                         	$vetor_topico[$ac_vt_topico] =$identacao."<a  class=purple_DR  href='javascript:opener.document.location=\"./conceito.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Conceito_pelo_Mapa_em_$categoria\"; self.close();'><font color=purple>".$topico_descricao."</font></a>";
		                         	$ac_vt_topico ++;
					}
        	                }
			}

			// ESTA PARTE SERVIRA PARA GERAR OS TOPICOS QUE SAO OS IRMAOS E PAIS
			// ESTAS 2 VARIAVEIS RECEBEM OS TOPICOS SEM OS PONTOS VINDOS DOS TOPICOS XML, PARA GERAR PROVAVEIS IRMAOS E PAIS
                        $topico_num_tratado = str_replace(".", "", $topico_num );		// Numero atual *********//
                        $para2_tratado      = str_replace(".", "", $para2 );     		// Parametro 2  *********//

			// MONTA O VETOR DE TOPICOS NUMERICOS (AKI SOH PASSA 1 VEZ)
                        if ($quebra == 0)                                        		
                        {
				// PEGA AS SUBSTRINGS POR EX: 2 22 223 2231 etc
                           	for ($cont=1 ; $cont <= strlen($para2_tratado); $cont++)
                        	{
                           		$vetor[$cont] = substr ($para2_tratado, 0, $cont);
                        	}
                           	$quebra = 1;
                              	$cont   = 1;
                        }
			
			// CHAMADAS RECURSIVAS PARA PERCORRER O ARQUIVO XML
                        $dummy = getChildren($no[$x]);
                        GeraCaminho($dummy, $para1, $para2, $quebra);
                        }
                        else
                        {
                         $no[$x]->unlink_node();
                        }

                }  //if2

                }  //if1
            }  		//for1

} 	                //laco pricipal da funcao

   $ac_vt_prereq = 0;
   $ac_vt_topico = 0;
   $ac_vetor_caminho = 0;
   $quebra = 0;
   $dom = xmldocfile($file);
   $root=$dom->root();
   $children = $root->children();
   @GeraCaminho($children, $parametro1, $parametro2, $quebra);

/***********************************************************************/
/* ESTE FILTRO ACHA TODOS OS TOPICOS DO ARQUIVO XML		       */
/* FUNCAO RECURSIVA PARA GERAR VETORES COMO:			       */
/* CAMINHAMENTO DO SITE, A NAVEGACAO ENTRE IRMAOS E PAIS	       */
/* MENU DE NAVEGACAO, TANTO NO MODO LIVRE E TUTORIAL		       */
/***********************************************************************/

function GeraExemplo($no, $para1, $para2, $quebra)

{    
$categoria = $_GET['categoria'];
    global 	// VARIAVEIS GERAIS
			$id_aluno,
	        	$tipo,
	        	$logado,
	        	$num_disc,
	        	$num_curso,
	        	$id_prof,
	        	$disciplina,
        		$caminho,
			$naveg, 
			$id,
			$achou_exemplo,
			$localizacao,

	                // VARIAVEIS PARA GERACAO DO CAMINHAMENTO
	               	$vetor,
        	       	$vetor_caminho,
	               	$ac_vetor_caminho,
	               	$cont,
	               	$topico_num_tratado,
        	       	$topico_descricao,
	               	$para2_tratado,
	               	$topico_num,
	               	$topico_link,
	               	$topico_visitado,
	               	$topico_abreviacao,
	               	$topico_descricao_encodado,
	               	$arquivo_xml,
	               	$guia_exemplo,
	               	$guia_exercicio,
	               	$guia_material,
	               	$parametro3,
	
			// VARIAVEIS PARA PREREQUISITOS
			$ac_vt_prereq,
			$vetor_prereq,
	
	                // VARIAVEIS PARA GERACAO DO MENU
	               	$vetor_topico_exemplo,
        	       	$ac_vt_topico_exemplo,
	               	$ct_x,
	               	$identacao,
	               	$flag,
	
	                // VARIAVEIS PARA GERACAO DA NAVEGACAO ENTRE PAIS/FILHOS
	               	$link_pai,
	               	$link_filho,
	               	$link_irmao_ant,
	               	$link_irmao_pos,
	               	$num_curso;



	// LOOP PRINCIPAL DA RECURSAO

        for ($x=0; $x<sizeof($no); $x++)                 //for1
        {
                if ($no[$x]->type == XML_ELEMENT_NODE)   //if1
                {
                if (($no[$x]->tagname)=="topico")        //if2
                {
                        $elemento="curso";
                        $atributo="identcurso";

			// FILTRO POR CURSO
                        $resultado=achar_curso($no[$x],$elemento,$atributo,$num_curso);
                        if ($resultado==1)
                        {

                        // FILTRO POR EXEMPLO

                        $achou_exemplo=achar_elemento($no[$x],"curso","identcurso",$num_curso,$topico_num,"exemplo", "possuiexemp");

			//ARMAZENAMENTO DOS DADOS DO XML EM VARIAVEIS UTILIZADAS NESTA FUNCAO
                        $no[$x]->tagname;
                        $topico_num  = $no[$x]->get_attribute("numtop");
                        $topico_link = $no[$x]->get_attribute("arquivoxml");
                        $topico_link = utf8_decode($topico_link);
                        $pos 	     = strpos($topico_link,".");
                        $ext1 	     = substr($topico_link,$pos+1);
                        $qtde 	     = strlen($topico_link)-(strlen($ext1)+1);
                        $arq_x 	     = substr($topico_link,0,$qtde);
                        $ext2 	     = ".xml";
                        $arquivo_xml = $arq_x.$ext2;
                        $topico_descricao  = $no[$x]->get_attribute("desctop");
                        $topico_descricao  = utf8_decode($topico_descricao);
                        $topico_abreviacao = $no[$x]->get_attribute("abreviacao");
                        $topico_descricao_encodado = urlencode ($topico_descricao);

			// CHAMADAS PARA VERIFICAR QUAIS GUIAS HABILITAR
                        if ($parametro3 == $topico_descricao)
                        {
                        	$guia_exercicio=filtro_habilita_guia($no[$x],"curso","identcurso",$num_curso,$topico_num,"exercicio","possuiexerc");
                        	$guia_material =filtro_habilita_guia($no[$x],"curso","identcurso",$num_curso,$topico_num,"matcomp","possuimatcomp");
                        }


			// INICIO DO BLOCO QUE CRIA VETOR PARA EXIBICAO DO MENU IDENTADO
                        $comprimento = strlen ($topico_num);
                        $identacao = "";

			// <FOR> PARA MONTAR O COMPRIMENTO DA IDENTACAO
                        for ($xx = 0; $xx < $comprimento; $xx ++)
                          {
				if ($xx ==  ($comprimento - 1))
				{
					$identacao = $identacao."<font color=\"gray\"> � </font>";
				}
				else
				{
                           		$identacao = $identacao."<img src = ../imagens/menu/blank.gif>";
				}
                          }

			if ($naveg == "tutorial") 
			{
                        	// CASO O PARAMETRO2 = TOPICO ATUAL, IMPRIME O MENU NA COR VERMELHA
				if (($localizacao == "exemplo") and ($para2 == $topico_num))              
                        	{
                         		$vetor_topico_exemplo[$ac_vt_topico_exemplo] =$identacao."<font color=red>".$topico_descricao."</font>";
		                        $ac_vt_topico_exemplo ++;
	                        }

				// CASO O PARAMETRO2 <> TOPICO ATUAL, IMPRIME O MENU NAS CORES PRETA, ROXA OU AZUL 
				else
				{
				
					// CHAMADA PARA MONTAR O VETOR DE PREREQUISITOS
					filtro_prerequisitos($no[$x],$topico_num);
	
					$result_select = 0;
					$ac_result_select = 0;
	                	        for ($xx = 0; $xx < $ac_vt_prereq ; $xx ++)
               				{
						$select="select id_log from log_usuario where modo_naveg='tutorial' and id_usuario=$id_aluno and id_curso=$num_curso and id_disc=$num_disc and topico='$vetor_prereq[$xx]' and menu='conceito'";
						mysql_query($select);
						$result_select = mysql_affected_rows();
						if ($result_select > 0)
						{
							$ac_result_select ++;
						}
			                }

					// CASO NAO ENCONTRAR, O PREREQUISITO NAO EXISTE E O TOPICO ANALISADO DEVE FICAR DESABILITADO NO MENU
					if (($ac_result_select <> $ac_vt_prereq) and ($topico_num <> 1) and ($achou_exemplo==1))
					{
	                	         	$vetor_topico_exemplo[$ac_vt_topico_exemplo] =$identacao."<font color=black>".$topico_descricao." </font>";
 		                	        $ac_vt_topico_exemplo ++;
						$ac_vt_prereq = 0;
					}

					// CASO ENCONTRADO DEVE-SE FAZER OUTRO SELECT PARA ANALISAR COR AZUL OU ROXA
					else
                	       		if ($achou_exemplo==1)
                       			{
        					$select="select id_usuario from log_usuario where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disc=$num_disc and topico='$topico_num' and menu='exemplo'";
						//$select="select id_usuario from log_usuario_DR where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disciplina=$num_disc and topico='$topico_num' and categoria='Exemplo'";
							
						mysql_query($select);
						$result_select=mysql_affected_rows();
						if ($result_select == 0)
						{
		                         		$vetor_topico_exemplo[$ac_vt_topico_exemplo] =$identacao."<a   class=blue_DR href='javascript:opener.document.location=\"./exemplo.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Exemplo_pelo_Mapa_em_$categoria\"; self.close();'><font color=blue>".$topico_descricao."</font></a>";
		                         		$ac_vt_topico_exemplo ++;
							$ac_vt_prereq = 0;
						}
						else
						{
		                         		$vetor_topico_exemplo[$ac_vt_topico_exemplo] =$identacao."<a class=purple_DR  href='javascript:opener.document.location=\"./exemplo.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Exemplo_pelo_Mapa_em_$categoria\"; self.close();'><font color=purple>".$topico_descricao."</font></a>";
	        	                 		$ac_vt_topico_exemplo ++;
							$ac_vt_prereq = 0;
						}
		                        }
		                        else
	        	                {
		        	               	$vetor_topico_exemplo[$ac_vt_topico_exemplo] =$identacao."<font color=gray>".$topico_descricao."</font></a>";
		                	       	$ac_vt_topico_exemplo ++;
						$ac_vt_prereq = 0;
		                        }
				}
			}
			else

			// ESTE <IF> GERA MENU PARA NAVEGACAO NO MODO LIVRE
			if ($naveg == "livre") 
			{
                        	// CASO O PARAMETRO2 = TOPICO ATUAL, IMPRIME O MENU NA COR VERMELHA
                        	if (($localizacao == "exemplo") and ($para2 == $topico_num))             
                        	{
	                         	$vetor_topico_exemplo[$ac_vt_topico_exemplo] =$identacao."<font color=red>".$topico_descricao."</font>";
		                        $ac_vt_topico_exemplo ++;
	                        }
                	        else
                        	if ($achou_exemplo==1)
                        	{
        				$select="select id_usuario from log_usuario where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disc=$num_disc and topico='$topico_num' and menu='exemplo'";
						//$select="select id_usuario from log_usuario_DR where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disciplina=$num_disc and topico='$topico_num' and categoria='Exemplo'";
						
					mysql_query($select);
					$result_select=mysql_affected_rows();
					if ($result_select == 0)
					{
	                         		$vetor_topico_exemplo[$ac_vt_topico_exemplo] =$identacao."<a   class=blue_DR href='javascript:opener.document.location=\"./exemplo.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Exemplo_pelo_Mapa_em_$categoria\"; self.close();'><font color=blue>".$topico_descricao."</font></a>";
	                         		$ac_vt_topico_exemplo ++;
					}
					else
					{
	                         		$vetor_topico_exemplo[$ac_vt_topico_exemplo] =$identacao."<a   class=purple_DR href='javascript:opener.document.location=\"./exemplo.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Exemplo_pelo_Mapa_em_$categoria\"; self.close();'><font color=purple>".$topico_descricao."</font></a>";
	                         		$ac_vt_topico_exemplo ++;
					}
                        	}
                        	else
                        	{
                         		$vetor_topico_exemplo[$ac_vt_topico_exemplo] =$identacao."<font color=gray>".$topico_descricao."</font></a>";
                         		$ac_vt_topico_exemplo ++;
                        	}
			}

			// ESTA PARTE SERVIRA PARA GERAR OS TOPICOS QUE SAO OS IRMAOS E PAIS
			// ESTAS 2 VARIAVEIS RECEBEM OS TOPICOS SEM OS PONTOS VINDOS DOS TOPICOS XML, PARA GERAR PROVAVEIS IRMAOS E PAIS
                        $topico_num_tratado = str_replace(".", "", $topico_num );		// Numero atual *********//
                        $para2_tratado      = str_replace(".", "", $para2 );     		// Parametro 2  *********//

			// MONTA O VETOR DE TOPICOS NUMERICOS (AKI SOH PASSA 1 VEZ)
                        if ($quebra == 0)                                        		
                        {
				// PEGA AS SUBSTRINGS POR EX: 2 22 223 2231 etc
                           	for ($cont=1 ; $cont <= strlen($para2_tratado); $cont++)
                        	{
                           		$vetor[$cont] = substr ($para2_tratado, 0, $cont);
                        	}
                           	$quebra = 1;
                              	$cont   = 1;
                        }
			
			// CHAMADAS RECURSIVAS PARA PERCORRER O ARQUIVO XML
                        $dummy = getChildren($no[$x]);
                        GeraExemplo($dummy, $para1, $para2, $quebra);
                        }
                        else
                        {
                         $no[$x]->unlink_node();
                        }

                }  //if2

                }  //if1

            }  		//for1

} 	                //laco pricipal da funcao

   $ac_vt_prereq = 0;
   $ac_vt_topico_exemplo = 0;
   $ac_vetor_caminho = 0;
   $quebra = 0;
   $dom = xmldocfile($file);
   $root=$dom->root();
   $children = $root->children();
   @GeraExemplo($children, $parametro1, $parametro2, $quebra);



/***********************************************************************/
/* ESTE FILTRO ACHA TODOS OS TOPICOS DO ARQUIVO XML		       */
/* FUNCAO RECURSIVA PARA GERAR VETORES COMO:			       */
/* CAMINHAMENTO DO SITE, A NAVEGACAO ENTRE IRMAOS E PAIS	       */
/* MENU DE NAVEGACAO, TANTO NO MODO LIVRE E TUTORIAL		       */
/***********************************************************************/

function GeraExercicio($no, $para1, $para2, $quebra)

{  
$categoria = $_GET['categoria'];
      global 	// VARIAVEIS GERAIS
			$id_aluno,
	        	$tipo,
	        	$logado,
	        	$num_disc,
	        	$num_curso,
	        	$id_prof,
	        	$disciplina,
        		$caminho,
			$naveg, 
			$id,
			$achou_exercicio,
			$localizacao,

	                // VARIAVEIS PARA GERACAO DO CAMINHAMENTO
	               	$vetor,
        	       	$vetor_caminho,
	               	$ac_vetor_caminho,
	               	$cont,
	               	$topico_num_tratado,
        	       	$topico_descricao,
	               	$para2_tratado,
	               	$topico_num,
	               	$topico_link,
	               	$topico_visitado,
	               	$topico_abreviacao,
	               	$topico_descricao_encodado,
	               	$arquivo_xml,
	               	$guia_exemplo,
	               	$guia_exercicio,
	               	$guia_material,
	               	$parametro3,
	
			// VARIAVEIS PARA PREREQUISITOS
			$ac_vt_prereq,
			$vetor_prereq,
	
	                // VARIAVEIS PARA GERACAO DO MENU
	               	$vetor_topico_exercicio,
        	       	$ac_vt_topico_exercicio,
	               	$ct_x,
	               	$identacao,
	               	$flag,
	
	                // VARIAVEIS PARA GERACAO DA NAVEGACAO ENTRE PAIS/FILHOS
	               	$link_pai,
	               	$link_filho,
	               	$link_irmao_ant,
	               	$link_irmao_pos,
	               	$num_curso;


	// LOOP PRINCIPAL DA RECURSAO

        for ($x=0; $x<sizeof($no); $x++)                 //for1
        {
                if ($no[$x]->type == XML_ELEMENT_NODE)   //if1
                {
                if (($no[$x]->tagname)=="topico")        //if2
                {
                        $elemento="curso";
                        $atributo="identcurso";

			// FILTRO POR CURSO
                        $resultado=achar_curso($no[$x],$elemento,$atributo,$num_curso);
                        if ($resultado==1)
                        {

                        // FILTRO POR EXERCICIO

                        $achou_exercicio=achar_elemento($no[$x],"curso","identcurso",$num_curso,$topico_num,"exercicio", "possuiexerc");

			//ARMAZENAMENTO DOS DADOS DO XML EM VARIAVEIS UTILIZADAS NESTA FUNCAO
                        $no[$x]->tagname;
                        $topico_num  = $no[$x]->get_attribute("numtop");
                        $topico_link = $no[$x]->get_attribute("arquivoxml");
                        $topico_link = utf8_decode($topico_link);
                        $pos 	     = strpos($topico_link,".");
                        $ext1 	     = substr($topico_link,$pos+1);
                        $qtde 	     = strlen($topico_link)-(strlen($ext1)+1);
                        $arq_x 	     = substr($topico_link,0,$qtde);
                        $ext2 	     = ".xml";
                        $arquivo_xml = $arq_x.$ext2;
                        $topico_descricao  = $no[$x]->get_attribute("desctop");
                        $topico_descricao  = utf8_decode($topico_descricao);
                        $topico_abreviacao = $no[$x]->get_attribute("abreviacao");
                        $topico_descricao_encodado = urlencode ($topico_descricao);

			// CHAMADAS PARA VERIFICAR QUAIS GUIAS HABILITAR
                        if ($parametro3 == $topico_descricao)
                        {
                        	$guia_exemplo  =filtro_habilita_guia($no[$x],"curso","identcurso",$num_curso,$topico_num,"exemplo","possuiexemp");
                        	$guia_material =filtro_habilita_guia($no[$x],"curso","identcurso",$num_curso,$topico_num,"matcomp","possuimatcomp");
                        }

			// INICIO DO BLOCO QUE CRIA VETOR PARA EXIBICAO DO MENU IDENTADO
                        $comprimento = strlen ($topico_num);
                        $identacao = "";

			// <FOR> PARA MONTAR O COMPRIMENTO DA IDENTACAO
                        for ($xx = 0; $xx < $comprimento; $xx ++)
                          {
				if ($xx ==  ($comprimento - 1))
				{
					$identacao = $identacao."<font color=\"gray\"> � </font>";
				}
				else
				{
                           		$identacao = $identacao."<img src = ../imagens/menu/blank.gif>";
				}
                          }

			if ($naveg == "tutorial") 
			{
                        	// CASO O PARAMETRO2 = TOPICO ATUAL, IMPRIME O MENU NA COR VERMELHA
				if (($localizacao == "exercicio") and ($para2 == $topico_num))              
                        	{
                         		$vetor_topico_exercicio[$ac_vt_topico_exercicio] =$identacao."<font color=red>".$topico_descricao."</font>";
		                        $ac_vt_topico_exercicio ++;
	                        }

				// CASO O PARAMETRO2 <> TOPICO ATUAL, IMPRIME O MENU NAS CORES PRETA, ROXA OU AZUL 
				else
				{
				
					// CHAMADA PARA MONTAR O VETOR DE PREREQUISITOS
					filtro_prerequisitos($no[$x],$topico_num);
	
					$result_select = 0;
					$ac_result_select = 0;
	                	        for ($xx = 0; $xx < $ac_vt_prereq ; $xx ++)
               				{
						$select="select id_log from log_usuario where modo_naveg='tutorial' and id_usuario=$id_aluno and id_curso=$num_curso and id_disc=$num_disc and topico='$vetor_prereq[$xx]' and menu='conceito'";
						mysql_query($select);
						$result_select = mysql_affected_rows();
						if ($result_select > 0)
						{
							$ac_result_select ++;
						}
			                }

					// CASO NAO ENCONTRAR, O PREREQUISITO NAO EXISTE E O TOPICO ANALISADO DEVE FICAR DESABILITADO NO MENU
					if (($ac_result_select <> $ac_vt_prereq) and ($topico_num <> 1) and ($achou_exercicio==1))
					{
	                	         	$vetor_topico_exercicio[$ac_vt_topico_exercicio] =$identacao."<font color=black>".$topico_descricao." </font>";
 		                	        $ac_vt_topico_exercicio ++;
						$ac_vt_prereq = 0;
					}

					// CASO ENCONTRADO DEVE-SE FAZER OUTRO SELECT PARA ANALISAR COR AZUL OU ROXA
					else
                	       		if ($achou_exercicio==1)
                       			{
        					$select="select id_usuario from log_usuario where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disc=$num_disc and topico='$topico_num' and menu='exercicio'";
						//$select="select id_usuario from log_usuario_DR where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disciplina=$num_disc and topico='$topico_num' and categoria='Exercicio'";
							
						mysql_query($select);
						$result_select=mysql_affected_rows();
						if ($result_select == 0)
						{
		                         		$vetor_topico_exercicio[$ac_vt_topico_exercicio] =$identacao."<a   class=blue_DR href='javascript:opener.document.location=\"./exercicio.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Exercicio_pelo_Mapa_em_$categoria\"; self.close();'><font color=blue>".$topico_descricao."</font></a>";
		                         		$ac_vt_topico_exercicio ++;
							$ac_vt_prereq = 0;
						}
						else
						{
		                         		$vetor_topico_exercicio[$ac_vt_topico_exercicio] =$identacao."<a   class=purple_DR href='javascript:opener.document.location=\"./exercicio.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Exercicio_pelo_Mapa_em_$categoria\"; self.close();'><font color=purple>".$topico_descricao."</font></a>";
	        	                 		$ac_vt_topico_exercicio ++;
							$ac_vt_prereq = 0;
						}
		                        }
		                        else
	        	                {
		        	               	$vetor_topico_exercicio[$ac_vt_topico_exercicio] =$identacao."<font color=gray>".$topico_descricao."</font></a>";
		                	       	$ac_vt_topico_exercicio ++;
						$ac_vt_prereq = 0;
		                        }
				}
			}
			else
			// ESTE <IF> GERA MENU PARA NAVEGACAO NO MODO LIVRE
			if ($naveg == "livre") 
			{	
                        	// CASO O PARAMETRO2 = TOPICO ATUAL, IMPRIME O MENU NA COR VERMELHA
	                        if (($localizacao == "exercicio") and ($para2 == $topico_num))  
        		        {       
	                       		$vetor_topico_exercicio[$ac_vt_topico_exercicio] =$identacao."<font color=red>".$topico_descricao."</font>";
			                $ac_vt_topico_exercicio ++;
	                        }
        	                else

                        	if ($achou_exercicio==1)
                        	{
        				$select="select id_usuario from log_usuario where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disc=$num_disc and topico='$topico_num' and menu='exercicio'";
						//$select="select id_usuario from log_usuario_DR where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disciplina=$num_disc and topico='$topico_num' and categoria='Exercicio'";
						
					mysql_query($select);
					$result_select=mysql_affected_rows();
					if ($result_select == 0)
					{
	                         		$vetor_topico_exercicio[$ac_vt_topico_exercicio] =$identacao."<a   class=blue_DR href='javascript:opener.document.location=\"./exercicio.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Exercicio_pelo_Mapa_em_$categoria\"; self.close();'><font color=blue>".$topico_descricao."</font></a>";
	                         		$ac_vt_topico_exercicio ++;
					}
					else
					{
	                         		$vetor_topico_exercicio[$ac_vt_topico_exercicio] =$identacao."<a   class=purple_DR href='javascript:opener.document.location=\"./exercicio.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Exercicio_pelo_Mapa_em_$categoria\"; self.close();'><font color=purple>".$topico_descricao."</font></a>";
	                         		$ac_vt_topico_exercicio ++;

					}
                        	}
                        	else
                        	{
                         		$vetor_topico_exercicio[$ac_vt_topico_exercicio] =$identacao."<font color=gray>".$topico_descricao."</font></a>";
                         		$ac_vt_topico_exercicio ++;
                        	}
			}

			// ESTA PARTE SERVIRA PARA GERAR OS TOPICOS QUE SAO OS IRMAOS E PAIS
			// ESTAS 2 VARIAVEIS RECEBEM OS TOPICOS SEM OS PONTOS VINDOS DOS TOPICOS XML, PARA GERAR PROVAVEIS IRMAOS E PAIS
                        $topico_num_tratado = str_replace(".", "", $topico_num );		// Numero atual *********//
                        $para2_tratado      = str_replace(".", "", $para2 );     		// Parametro 2  *********//

			// MONTA O VETOR DE TOPICOS NUMERICOS (AKI SOH PASSA 1 VEZ)
                        if ($quebra == 0)                                        		
                        {
				// PEGA AS SUBSTRINGS POR EX: 2 22 223 2231 etc
                           	for ($cont=1 ; $cont <= strlen($para2_tratado); $cont++)
                        	{
                           		$vetor[$cont] = substr ($para2_tratado, 0, $cont);
                        	}
                           	$quebra = 1;
                              	$cont   = 1;
                        }
			
			// CHAMADAS RECURSIVAS PARA PERCORRER O ARQUIVO XML
                        $dummy = getChildren($no[$x]);
                        GeraExercicio($dummy, $para1, $para2, $quebra);
                        }
                        else
                        {
                         $no[$x]->unlink_node();
                        }

                }  //if2

                }  //if1

            }  		//for1

} 	                //laco pricipal da funcao


   $ac_vt_topico_exercicio = 0;
   $ac_vetor_caminho = 0;
   $quebra = 0;
   $dom = xmldocfile($file);
   $root=$dom->root();
   $children = $root->children();
   @GeraExercicio($children, $parametro1, $parametro2, $quebra);



/***********************************************************************/
/* ESTE FILTRO ACHA TODOS OS TOPICOS DO ARQUIVO XML		       */
/* FUNCAO RECURSIVA PARA GERAR VETORES COMO:			       */
/* CAMINHAMENTO DO SITE, A NAVEGACAO ENTRE IRMAOS E PAIS	       */
/* MENU DE NAVEGACAO, TANTO NO MODO LIVRE E TUTORIAL		       */
/***********************************************************************/

function GeraMaterial($no, $para1, $para2, $quebra)

{ 
$categoria = $_GET['categoria'];
       global 	// VARIAVEIS GERAIS
			$id_aluno,
	        	$tipo,
	        	$logado,
	        	$num_disc,
	        	$num_curso,
	        	$id_prof,
	        	$disciplina,
        		$caminho,
			$naveg, 
			$id,
			$achou_material,
			$localizacao,

	                // VARIAVEIS PARA GERACAO DO CAMINHAMENTO
	               	$vetor,
        	       	$vetor_caminho,
	               	$ac_vetor_caminho,
	               	$cont,
	               	$topico_num_tratado,
        	       	$topico_descricao,
	               	$para2_tratado,
	               	$topico_num,
	               	$topico_link,
	               	$topico_visitado,
	               	$topico_abreviacao,
	               	$topico_descricao_encodado,
	               	$arquivo_xml,
	               	$guia_exemplo,
	               	$guia_exercicio,
	               	$guia_material,
	               	$parametro3,
	
			// VARIAVEIS PARA PREREQUISITOS
			$ac_vt_prereq,
			$vetor_prereq,
	
	                // VARIAVEIS PARA GERACAO DO MENU
	               	$vetor_topico_material,
        	       	$ac_vt_topico_material,
	               	$ct_x,
	               	$identacao,
	               	$flag,
	
	                // VARIAVEIS PARA GERACAO DA NAVEGACAO ENTRE PAIS/FILHOS
	               	$link_pai,
	               	$link_filho,
	               	$link_irmao_ant,
	               	$link_irmao_pos,
	               	$num_curso;



	// LOOP PRINCIPAL DA RECURSAO

        for ($x=0; $x<sizeof($no); $x++)                 //for1
        {
                if ($no[$x]->type == XML_ELEMENT_NODE)   //if1
                {
                if (($no[$x]->tagname)=="topico")        //if2
                {
                        $elemento="curso";
                        $atributo="identcurso";

			// FILTRO POR CURSO
                        $resultado=achar_curso($no[$x],$elemento,$atributo,$num_curso);
                        if ($resultado==1)
                        {

			// FILTRO POR MATERIAL
	
                        $achou_material=achar_elemento($no[$x],"curso","identcurso",$num_curso,$topico_num, "matcomp", "possuimatcomp");

			//ARMAZENAMENTO DOS DADOS DO XML EM VARIAVEIS UTILIZADAS NESTA FUNCAO
                        $no[$x]->tagname;
                        $topico_num  = $no[$x]->get_attribute("numtop");
                        $topico_link = $no[$x]->get_attribute("arquivoxml");
                        $topico_link = utf8_decode($topico_link);
                        $pos 	     = strpos($topico_link,".");
                        $ext1 	     = substr($topico_link,$pos+1);
                        $qtde 	     = strlen($topico_link)-(strlen($ext1)+1);
                        $arq_x 	     = substr($topico_link,0,$qtde);
                        $ext2 	     = ".xml";
                        $arquivo_xml = $arq_x.$ext2;
                        $topico_descricao  = $no[$x]->get_attribute("desctop");
                        $topico_descricao  = utf8_decode($topico_descricao);
                        $topico_abreviacao = $no[$x]->get_attribute("abreviacao");
                        $topico_descricao_encodado = urlencode ($topico_descricao);

			// CHAMADAS PARA VERIFICAR QUAIS GUIAS HABILITAR

                        if ($parametro3 == $topico_descricao)
                        {
                        	$guia_exemplo  =filtro_habilita_guia($no[$x],"curso","identcurso",$num_curso,$topico_num,"exemplo","possuiexemp");
                        	$guia_exercicio=filtro_habilita_guia($no[$x],"curso","identcurso",$num_curso,$topico_num,"exercicio","possuiexerc");
                        }

			// INICIO DO BLOCO QUE CRIA VETOR PARA EXIBICAO DO MENU IDENTADO
                        $comprimento = strlen ($topico_num);
                        $identacao = "";

			// <FOR> PARA MONTAR O COMPRIMENTO DA IDENTACAO
                        for ($xx = 0; $xx < $comprimento; $xx ++)
                          {
				if ($xx ==  ($comprimento - 1))
				{
					$identacao = $identacao."<font color=\"gray\"> � </font>";
				}
				else
				{
                           		$identacao = $identacao."<img src = ../imagens/menu/blank.gif>";
				}
                          }

			if ($naveg == "tutorial") 
			{
                        	// CASO O PARAMETRO2 = TOPICO ATUAL, IMPRIME O MENU NA COR VERMELHA
				if (($localizacao == "material") and ($para2 == $topico_num))               
                        	{
                         		$vetor_topico_material[$ac_vt_topico_material] =$identacao."<font color=red>".$topico_descricao."</font>";
		                        $ac_vt_topico_material ++;
	                        }

				// CASO O PARAMETRO2 <> TOPICO ATUAL, IMPRIME O MENU NAS CORES PRETA, ROXA OU AZUL 
				else
				{
				
					// CHAMADA PARA MONTAR O VETOR DE PREREQUISITOS
					filtro_prerequisitos($no[$x],$topico_num);
	
					$result_select = 0;
					$ac_result_select = 0;
	                	        for ($xx = 0; $xx < $ac_vt_prereq ; $xx ++)
               				{
						$select="select id_log from log_usuario where modo_naveg='tutorial' and id_usuario=$id_aluno and id_curso=$num_curso and id_disc=$num_disc and topico='$vetor_prereq[$xx]' and menu='conceito'";
						mysql_query($select);
						$result_select = mysql_affected_rows();
						if ($result_select > 0)
						{
							$ac_result_select ++;
						}
			                }

					// CASO NAO ENCONTRAR, O PREREQUISITO NAO EXISTE E O TOPICO ANALISADO DEVE FICAR DESABILITADO NO MENU
					if (($ac_result_select <> $ac_vt_prereq) and ($topico_num <> 1) and ($achou_material==1))
					{
	                	         	$vetor_topico_material[$ac_vt_topico_material] =$identacao."<font color=black>".$topico_descricao." </font>";
 		                	        $ac_vt_topico_material ++;
						$ac_vt_prereq = 0;
					}

					// CASO ENCONTRADO DEVE-SE FAZER OUTRO SELECT PARA ANALISAR COR AZUL OU ROXA
					else
                	       		if ($achou_material==1)
                       			{
        					$select="select id_usuario from log_usuario where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disc=$num_disc and topico='$topico_num' and menu='material'";
						//$select="select id_usuario from log_usuario_DR where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disciplina=$num_disc and topico='$topico_num' and categoria='Material'";
							
						mysql_query($select);
						$result_select=mysql_affected_rows();
						if ($result_select == 0)
						{
		                         		$vetor_topico_material[$ac_vt_topico_material] =$identacao."<a   class=blue_DR href='javascript:opener.document.location=\"./material.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Material_pelo_Mapa_em_$categoria\"; self.close();'><font color=blue>".$topico_descricao."</font></a>";
		                         		$ac_vt_topico_material ++;
							$ac_vt_prereq = 0;
						}
						else
						{
		                         		$vetor_topico_material[$ac_vt_topico_material] =$identacao."<a   class=purple_DR href='javascript:opener.document.location=\"./material.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Material_pelo_Mapa_em_$categoria\"; self.close();'><font color=purple>".$topico_descricao."</font></a>";
	        	                 		$ac_vt_topico_material ++;
							$ac_vt_prereq = 0;
						}
		                        }
		                        else
	        	                {
		        	               	$vetor_topico_material[$ac_vt_topico_material] =$identacao."<font color=gray>".$topico_descricao."</font></a>";
		                	       	$ac_vt_topico_material ++;
						$ac_vt_prereq = 0;
		                        }
				}
			}
			else

			// ESTE <IF> GERA MENU PARA NAVEGACAO NO MODO LIVRE
			if ($naveg == "livre") 
			{  
                        	// CASO O PARAMETRO2 = TOPICO ATUAL, IMPRIME O MENU NA COR VERMELHA
		                if (($localizacao == "material") and ($para2 == $topico_num))                 
                 	        {
                         		$vetor_topico_material[$ac_vt_topico_material] =$identacao."<font color=red>".$topico_descricao."</font>";
		                        $ac_vt_topico_material ++;
	                        }
                        	else
                        	if ($achou_material==1)
                        	{
	        			$select="select id_usuario from log_usuario where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disc=$num_disc and topico='$topico_num' and menu='material'";
						//$select="select id_usuario from log_usuario_DR where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disciplina=$num_disc and topico='$topico_num' and categoria='Material'";
						
					mysql_query($select);
					$result_select=mysql_affected_rows();
					if ($result_select == 0)
					{
	                         		$vetor_topico_material[$ac_vt_topico_material] =$identacao."<a   class=blue_DR href='javascript:opener.document.location=\"./material.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Material_pelo_Mapa_em_$categoria\"; self.close();'><font color=blue>".$topico_descricao."</font></a>";
	                         		$ac_vt_topico_material ++;
					}
					else
					{
	                         		$vetor_topico_material[$ac_vt_topico_material] =$identacao."<a   class=purple_DR href='javascript:opener.document.location=\"./material.livre.php?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Material_pelo_Mapa_em_$categoria\"; self.close();'><font color=purple>".$topico_descricao."</font></a>";
	                         		$ac_vt_topico_material ++;
					}
	                        }
	                        else
	                        {
		                         $vetor_topico_material[$ac_vt_topico_material] =$identacao."<font color=gray>".$topico_descricao."</font></a>";
		                         $ac_vt_topico_material ++;
	                        }
			}

			// ESTA PARTE SERVIRA PARA GERAR OS TOPICOS QUE SAO OS IRMAOS E PAIS
			// ESTAS 2 VARIAVEIS RECEBEM OS TOPICOS SEM OS PONTOS VINDOS DOS TOPICOS XML, PARA GERAR PROVAVEIS IRMAOS E PAIS
                        $topico_num_tratado = str_replace(".", "", $topico_num );		// Numero atual *********//
                        $para2_tratado      = str_replace(".", "", $para2 );     		// Parametro 2  *********//

			// MONTA O VETOR DE TOPICOS NUMERICOS (AKI SOH PASSA 1 VEZ)
                        if ($quebra == 0)                                        		
                        {
				// PEGA AS SUBSTRINGS POR EX: 2 22 223 2231 etc
                           	for ($cont=1 ; $cont <= strlen($para2_tratado); $cont++)
                        	{
                           		$vetor[$cont] = substr ($para2_tratado, 0, $cont);
                        	}
                           	$quebra = 1;
                              	$cont   = 1;
                        }
			
			// CHAMADAS RECURSIVAS PARA PERCORRER O ARQUIVO XML
                        $dummy = getChildren($no[$x]);
                        GeraMaterial($dummy, $para1, $para2, $quebra);
                        }
                        else
                        {
                         $no[$x]->unlink_node();
                        }

                }  //if2

                }  //if1

            }  		//for1

} 	                //laco pricipal da funcao


   $ac_vt_prereq = 0;
   $ac_vt_material = 0;
   $ac_vt_topico_material = 0;
   $ac_vetor_caminho = 0;
   $quebra = 0;
   $dom = xmldocfile($file);
   $root=$dom->root();
   $children = $root->children();
   @GeraMaterial($children, $parametro1, $parametro2, $quebra);


?>


<html>
<head>
<title>	
	<?php  echo A_LANG_SITE_MAP; ?>
</title>


<style type="text/css">
  
	.tabela_td{
	height:-10px;
	}
  .fundo_categoria {
	margin-top:10px;
    margin-bottom:10px;
    padding-left:2px;  
    padding-right:2px;  
    padding-bottom:6px;  
	background-color:#c3c3c3;  
	border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    min-width:300px;
 }
   .fundo_branco {
    margin-bottom:0px;
    padding-left:0px;  
    padding-right:0px;  
    padding-bottom:0px;  
	background-color:#ffffff;  
	/* border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px; */
    min-width:300px;
	font-family:arial,helvetica; 
	font-size:11px;
 }
 
    .fundo_branco_redondo {
    margin-bottom:0px;
	margin-left:5px;
	margin-right:5px;
    padding-left:5px;  
    padding-right:5px;  
    padding-bottom:0px;  
	
	border-radius:3px;-moz-border-radius:3px; -webkit-border-radius:3px;
    height:22px;
	max-width:300px;
 }
 
   .menu_dr {
    margin-bottom:5px;
	padding-top:1px;  
    padding-left:2px;  
    padding-right:2px;  
    padding-bottom:6px;  
	background-color:#7bb0dc;  
	border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    min-width:300px;
 }
 
    .menu_dr_titulo {
    margin-bottom:0px;
    padding-left:0px;  
    padding-right:0px;  
    padding-bottom:0px;  
	background-color:#7bb0dc;  
	border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    width:200px;
 }
 
   .fundo_busca {
    margin-bottom:5px;
    padding-left:2px;  
    padding-right:2px;  
    padding-bottom:6px;  
	background-color:#c3c3c3;  
	border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    min-width:300px;
 }
 
  
  .fundo_categoria_titulo {
  	font-family:arial,helvetica; 
	font-size:12px;
	font-weight: bold;
    padding-top:2px;  
	padding-left:2px;  
    padding-bottom:2px;  
	min-height:25px;
 }
 
    .fundo_tags {
    margin-bottom:5px;
    padding-left:2px;  
    padding-right:2px;  
    padding-bottom:6px;  
	background-color:#c3c3c3;  
	border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    min-width:300px;
 }
 body a:link {text-decoration: none}
body a:hover {text-decoration: underline;}

.purple_DR {
color:purple;
text-decoration: none}

.blue_DR {
color:blue;
text-decoration: none}

 </style>
</head>

<body bgcolor="#<?php echo $cor_fundo_DR;?>" text="#000000" vlink="#0000FF" link="#0000FF" alink="#FF0000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#FFFFFF">
    <td width="84%" >
	&nbsp;
      <b>
       <font face="Arial, Helvetica, sans-serif" size="6">

        <!-- *******************************************************-->
        <!-- ********** Aqui eh impresso o nome da disciplina ******-->
        <!-- *******************************************************-->

	<?php

                echo $disciplina;
	?>
       </font>
      </b>
    </td>
  </tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="100%" height="25" bgcolor="<?php  echo $cor_padrao; ?>" >
    <center>
	<font size="2" face="Arial, Helvetica, sans-serif">
	<b>
	<?php  echo "Mapa de Disciplina"; ?>
	</b></font>
	</center>
    </td>
  </tr>
</table>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	  <tr>
	    <td bgcolor="eeeeee" width="100%" height="25">
	      <div align="left">
		<font color="eeeeee" size="1" face="verdana, Arial, Helvetica, sans-serif">
		<center>
		|
		</center>
		</font>
	      </div>
	    </td>
	  </tr>
	<tr>
	<td background="imagens/pontilhado.gif"  height="2"></td>
  	</tr>
</table>
<br>

<center>
<table width="80%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
<table width="40%" border="0" cellspacing="0" cellpadding="1">
  <tr> 
    <td bgcolor="88bbcc">
	<font size="2" face="Arial, Helvetica, sans-serif">
	<b>&nbsp;&nbsp;<?php  echo A_LANG_TOPIC; ?></b></td>
	</font>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td bgcolor="88bbcc"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td bgcolor="#FFFFFF">
    	  <font style=font-size:11px face="Arial, Helvetica, sans-serif">

<br>
        <?php

		/***********************************************************************/
		/* IMPRIME O CONCEITO			   			       */
		/***********************************************************************/

		for ($ct_x = 0; $ct_x < $ac_vt_topico; $ct_x ++)
		{   
			echo "&nbsp;&nbsp;".$vetor_topico[$ct_x]."<br>";
		}
	?>
<br>

	  </font>
	  </td>
        </tr>
      </table></td>
  </tr>
</table>
</td>
  </tr>
</table>
</center>
<br>

<center>
<table width="80%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
<table width="40%" border="0" cellspacing="0" cellpadding="1">
  <tr> 
    <td bgcolor="aabbaa">
	<font size="2" face="Arial, Helvetica, sans-serif">
	<b>&nbsp;&nbsp;<?php  echo A_LANG_TOPIC_EXEMPLES; ?></b></td>
	</font>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td bgcolor="aabbaa"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td bgcolor="#FFFFFF">
    	  <font style=font-size:11px face="Arial, Helvetica, sans-serif">

<br>
<?php
/***********************************************************************/
/* IMPRIME O EXEMPLO						       */
/***********************************************************************/
        
	for ($ct_x = 0; $ct_x < $ac_vt_topico_exemplo; $ct_x ++)
	{   
		echo "&nbsp;&nbsp;".$vetor_topico_exemplo[$ct_x]."<br>";
	}
?>
<br>

	  </font>
	  </td>
        </tr>
      </table></td>
  </tr>
</table>
</td>
  </tr>
</table>
</center>
<br>


<center>
<table width="80%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
<table width="40%" border="0" cellspacing="0" cellpadding="1">
  <tr> 
    <td bgcolor="aabbcc">
	<font size="2" face="Arial, Helvetica, sans-serif">
	<b>&nbsp;&nbsp;<?php  echo A_LANG_TOPIC_EXERCISES; ?></b></td>
	</font>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td bgcolor="aabbcc"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td bgcolor="#FFFFFF">
    	  <font style=font-size:11px face="Arial, Helvetica, sans-serif">

<br>
<?php
/***********************************************************************/
/* IMPRIME O EXERCICIO						       */
/***********************************************************************/
        
	for ($ct_x = 0; $ct_x < $ac_vt_topico_exercicio; $ct_x ++)
	{   
		echo "&nbsp;&nbsp;".$vetor_topico_exercicio[$ct_x]."<br>";
	}
?>

<br>

	  </font>
	  </td>
        </tr>
      </table></td>
  </tr>
</table>
</td>
  </tr>
</table>
</center>
<br>

<center>
<table width="80%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
<table width="40%" border="0" cellspacing="0" cellpadding="1">
  <tr> 
    <td bgcolor="CBCCA0">
	<font size="2" face="Arial, Helvetica, sans-serif">
	<b>&nbsp;&nbsp;<?php  echo A_LANG_TOPIC_COMPLEMENTARY; ?></b></td>
	</font>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td bgcolor="CBCCA0"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td bgcolor="#FFFFFF">
    	  <font style=font-size:11px face="Arial, Helvetica, sans-serif">

<br>
<?php
/***********************************************************************/
/* IMPRIME O MATERIAL COMPLEMENTAR				       */
/***********************************************************************/
        
	for ($ct_x = 0; $ct_x < $ac_vt_topico_material; $ct_x ++)
	{   
		echo "&nbsp;&nbsp;".$vetor_topico_material[$ct_x]."<br>";
	}
?>


<br>

	  </font>
	  </td>
        </tr>
      </table></td>
  </tr>
</table>
</td>
  </tr>
</table>
</center>
<br>

<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
	<td background="imagens/pontilhado.gif"  height="2"></td>
  	</tr>
	  <tr>
	    <td bgcolor="eeeeee" width="100%">
	      <div align="center">
		<center><font size="1" face="Arial, Helvetica, sans-serif"><a href="javascript:close();"><?php  echo A_LANG_SEARCH_CLOSE; ?></a></font><center>
	      </div>
	    </td>
	  </tr>
</table>
<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
	  <tr>
	    <td bgcolor=<?php  echo $cor_padrao; ?> width="100%">
	      <div align="right">
	        <font size="1" face="Arial, Helvetica, sans-serif"><?php  echo " &nbsp; &nbsp;".A_LANG_SYSTEM_COPYRIGHT."";?> </font>
	      </div>
	    </td>
	  </tr>
</table>
<br>
<br>
</body>
</html>

<?php
			/*****	include('include/adodb/adodb.inc.php'); 

				$conn = &ADONewConnection($A_DB_TYPE); 
				$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
				global $categoria;
				$acao_DR = $_GET['acao_DR'];
				$num_disc = $_GET['num_disc'];
				$num_curso = $_GET['num_curso'];
				$categoria = $_GET['categoria'];
				 
				$insert ="insert into log_usuario_DR (id_usuario, pagina_atual, acao_que_originou, data, hora, ip_real, ip_rede, resolucao_tela, tipo_dispositivo, so, navegador, navegador_versao, velocidade_navegacao, tx_download, duracao_teste, cidade, pais, latitude, longitude, id_disciplina, id_curso, modo_naveg, categoria, topico) values ('$id_aluno', 'mapa_xml.livre.php', '$acao_DR', current_date, current_time,'$ip_real_DR','$ip_rede_DR', '$resolucao_tela_DR', '$tipo_dispositivo_DR', '$so_DR', '$navegador_DR', '$navegador_versao_DR', '$velocidade_navegacao_DR ', '$tx_download_DR', '$duracao_teste_DR', ' $cidade_DR', '$pais_DR' , '$latitude_DR', '$longitude_DR', '$num_disc', '$num_curso', '$naveg', '$categoria', '$parametro2')";

				$rs = $conn->Execute($insert);		
				if ($rs === false) die(A_LANG_REQUEST_ACCESS_MSG3);  
				$rs->Close(); 
*****/

?>