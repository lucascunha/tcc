<?php
global $NomeDisciplina, $conteudo, $tipo_usuario, $id_usuario; 

// **************************************************************************
// *  CRIA MATRIZ ORELHA                                                    *
// **************************************************************************
 
$orelha = array();  
  

  $orelha = array(
  		array(   
   		   "LABEL" => utf8_decode("Repositório"), 
     		   "LINK" => "a_index.php?opcao=Repositorio",    
     		   "ESTADO" => "OFF"
   		   ),
		array(   
   		   "LABEL" => A_LANG_EXPORT, 
     		   "LINK" => "a_index.php?opcao=Export",    
     		   "ESTADO" => "ON"
   		   ),
		array(   
   		   "LABEL" => A_LANG_IMPORT, 
     		   "LINK" => "a_index.php?opcao=Import",    
     		   "ESTADO" => "OFF"
   		   ),
    array(   
         "LABEL" => "Gerenciamento", 
           "LINK" => "a_index.php?opcao=Gerenciar",    
           "ESTADO" => "OFF"
         ),  /*    
		array(   
   		   "LABEL" => A_LANG_OBJECT_SEARCH, 
     		   "LINK" => "a_index.php?opcao=Pesq",    
     		   "ESTADO" => "OFF"
   		   )*/
   		  ); 


MontaOrelha($orelha);  
  
?>

<!-- Início Zamin -->
<?php
include ('repositorio/database.inc.php');
include ('repositorio/config.inc.php');
require_once ('repositorio/exportacao/functions.inc.php');

$idUsuario = $_SESSION['id_usuario'];
$emailUsuario = $_SESSION['email_usuario'];
$nomeUsuario = $_SESSION['nome_usuario'];
?>


<script>
function checaForm()
{

    if (document.frmDisciplina.iddisciplina.selectedIndex < 0)
    {
    	alert ("<? echo A_LANG_REP_SELECT;?>"+"<?=utf8_decode(" para ser disponibilizada no Repositório."); ?>");
        return false;
    }
    if (document.frmDisciplina.descricaodisciplina.value == '')
    {
    	alert ("<?=utf8_decode("Informe uma identificação para a disciplina");?>");
      document.frmDisciplina.descricaodisciplina.focus();
        return false;
    }
    $('#dvLoading').fadeIn(200);
    document.frmDisciplina.submit();

    return true;
}

</script>
<!-- #EndEditable -->

<?
//Se é para exportar
$mensagem = "";
$qt_erro = 0;

if(isset($_POST['iddisciplina']));
{
	$idUsuarioAdaptweb = $_POST['idusuario']; //substituir pelo id do usuario logado no adaptweb
	$idDisciplinaAdaptweb = $_POST['iddisciplina'];
	$descricaoDisciplina = $_POST['descricaodisciplina'];
  $isPrivado = $_POST['exportacaoprivada'];
  if( $isPrivado== null)
  {
    $isPrivado=0;
  } else {
    $isPrivado=1;
  }  
	
  if (is_numeric($idDisciplinaAdaptweb))
	{
		if (exportaDisciplina("$idDisciplinaAdaptweb", "$idUsuarioAdaptweb", $descricaoDisciplina, $nomeUsuario, $emailUsuario, $isPrivado))
    {
		    //echo "alert(\"".A_LANG_REP_EXP_SUCCESS."\");";
        $mensagem = utf8_decode("Exportação da disciplina realizada com sucesso!");
    }
		else
    {  
    	  //die ('<hr>'.A_LANG_REP_EXP_ERROR.'<hr>');
        $mensagem = A_LANG_REP_EXP_ERROR;
        $qt_erro++;
	  }
  }
}

$db = getAdaptwebConn();
?>


<body>
<table CELLSPACING=1 CELLPADDING=1 width="100%"  border =0  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?>  height="80%" >
 <tr valign="top">
    <td>

 <table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">
 <tr>
   <td>
     <table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0">
           <tr>
             <td>  
         
                <?php
                if ($mensagem == '')
                  {
                  echo "<div class=\"Mensagem_Ok\">"; 
                  echo "<p class=\"texto1\">\n";     
                  echo "<table cellspacing=\"0\" cellpadding=\"0\">";
                  echo "  <tr>";
                  echo "    <td width=\"50\">";
                  echo "      <img src=\"imagens/icones/notice.png\" alt=\"\" />";
                  echo "    </td>";
                  echo "    <td valign=\"center\">";
                  echo A_LANG_REP_EXP_TEXT ;  
                  echo "    </td>";
                  echo "  </tr>";
                  echo "</table>";  
                  echo "</p>\n";  
                  echo "</div>";    
                 }
                 ?>    
               </td>
             </tr>       
     </table>  
   </td> 
</tr>


<?php

if  ($id_usuario == 2)
{
      
      $mensagem = A_LANG_DEMO;
      $qt_erro++;
      $Excluir = "demo";
      $status_botao = "disabled";
      
}    

if ($mensagem != '')
{
 
      if (($qt_erro == 0))
      {   
        $tipo_msg = "success";
        $icone = "<img src='imagens/icones/success.png' border='0' />";
      }
      else
      { 
        $tipo_msg = "error";
        $icone = "<img src='imagens/icones/error.png' border='0' />";       
      }
   ?>
  <script type="text/javascript">
      showNotification({
        message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo $mensagem; ?></td></tr></table>",              
        type: "<? echo $tipo_msg; ?>",
        autoClose: true,
        duration: 8                                        
      });
      //error //success //warning
  </script>     

<?php } ?>



  <tr>
    <td>
    	<table border="0" cellspacing="0" cellpadding="0">
    		<tr>
    			<td>
	            	<?
	            	if ($idUsuario > 0)
	            	{
	            	?>
	          	      <table width="100%" border="0" cellspacing="0" cellpadding="5">
        				<tr>
	            			<form method="post" action="a_index.php?opcao=Export" name="frmDisciplina">
	                		<input type="hidden" name="idusuario" value="<?=$idUsuario?>">

	                  			<td width="170" valign="top" align="right">
	                    			<? echo utf8_decode("Disciplinas para Exportação");?>:
	                    		</td>
	                    		<td>	
	                    				<select name="iddisciplina" size="15" maxsize="15" style="width: 526px;">
	                    				<?
	                        			$handle = mysql_query("SELECT id_disc, nome_disc FROM disciplina WHERE Id_usuario = $idUsuario ORDER BY nome_disc", $db);
	                        			if ($handle)
	                            			while ($dados = mysql_fetch_assoc($handle))
	                                			echo '<option value="'.$dados['id_disc'].'">'.$dados['nome_disc'].'</option>';
	                    						?>
	                    				</select>
	                  			</td>
	                  	</tr>
	           
                      <tr>    
                      
                      
                          <td align="right" valign=top>
                            <? echo utf8_decode("Exportação privativa");?>:
                          </td>
                          <td>
                            <input name="exportacaoprivada" type="checkbox" value="1">
                          </td>           
                      
                      </tr>

	                  	<tr>		
	                		
	                		
	                  			<td align="right" valign=top>
	                    			<? echo utf8_decode("Identificador para a disciplina");?>:
	                    		</td>
	                   			<td>
	                   				<input class="text"  name="descricaodisciplina" type="text" value="" maxsize=255 style="width: 520px; ">
	                    		</td>      			
	                		
	                    </tr>
          				<tr>		
	                		<td colspan="2" align="right">
	                   				<!-- <input type="submit" name="Submit" class="buttonBig" value="<? echo A_LANG_REP_EXPORT;?>"> -->
                     <script type="text/javascript">
                      $(
                          
                          function () 
                          {
                          $("#customDialog").easyconfirm
                          (
                            { locale: 
                              {
                                title: '',
                                button: ['<?=utf8_decode("Não");?>',' Sim'],
                                closeText: 'Fechar'
                               }, dialog: $("#question")
                            }
                            
                          );
                          $("#customDialog").click(function() { checaForm(); });
                         
                          }
                        );

                      </script>
                      <br>
                        <input type="button" class="buttonBig" onClick="history.go(-1)" value="Voltar">
                        &nbsp;&nbsp;
                        <input class="buttonBig" type="button" value="<? echo A_LANG_REP_EXPORT;?>" name="Submit" id="customDialog" <? echo $status_botao; ?>>
                        <div class="dialog" id="question">
                          <table>
                            <tr>
                              <td width="50">
                                <img src="imagens/icones/notice.png" alt="" />
                              </td>
                              <td valign="center">
                                <?=utf8_decode("A exportação pode demorar alguns minutos. Continuar ?"); ?>
                              </td>
                            </tr>
                          </table>  
                        </div>



	                  		</td>      			
	                		
	                		</form>
	                    </tr>	                      
      					</table>		

	            	<?
	            	}
	            	?>
	            </td>
	           </tr>
	        </table>   	
      	</td>
   </tr>
 </table>
<br><br>

<!-- Fim Zamin -->
	</td>
   </tr>

</TABLE>

