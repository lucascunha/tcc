<?php
require_once ('repositorio/database.inc.php');

/*
verifica se o arquivo tem a extensao XML
*/
function isXml($strFilename)
{
	if (strtolower(substr($strFilename, strlen($strFilename)-4, 4)) == '.xml')
    	return true;
    else
    	return false;
}


/*
   RESETA os campos referentes a curso, no array passado como parametro
   Essa funcao sempre recebe o array global $conteudo como parametro.
*/
function trataArrayConteudo($arrayConteudo)
{
	//array com informacoes de curso zeradas
    $arrayCurso = array('ID_CURSO' => 0,'NOME_CURSO' =>'', 'STATUSCUR' => 'FALSE');

	for ($i = 0; $i < count($arrayConteudo); $i++)
    	$arrayConteudo[$i]['CURSO'] = $arrayCurso;

    return $arrayConteudo;
}


/*
   Exporta uma disciplina do adaptweb.
   Recebe o id da disciplina NO BANCO DO ADAPTWEB e o id do
   usuario NO BANCO DO ADAPTWEB.
   Os dados s�o armazenados no banco de dados do reposit�rio.
*/
function exportaDisciplina($idDisciplinaAdaptweb, $idUsuarioAdaptweb, $descricao, $nomeUsuario, $emailUsuario, $isPrivado)
{

    include ('repositorio/config.inc.php');

    //conecta com o BD do adaptweb
    $db = getAdaptwebConn();
	//pega os dados do banco AdaptWeb
    $cmdSQL = "SELECT nome_disc, Topicos FROM disciplina WHERE id_disc = 0$idDisciplinaAdaptweb";
    if ($handle = mysql_query($cmdSQL, $db))
    {
		//insere os dados no banco de dados de exporta��o
    	$dados = mysql_fetch_assoc($handle);

		$arrayConteudo = unserialize($dados['Topicos']);
		$arrayConteudo = trataArrayConteudo($arrayConteudo);


        $cmdSQL = "SELECT c.nome_curso FROM disciplina d
                   INNER JOIN curso_disc cd ON (d.id_disc = cd.id_disc)
                   INNER JOIN curso c ON (cd.id_curso = c.id_curso)
                   WHERE d.id_disc = 0$idDisciplinaAdaptweb";
        //pega os cursos originais da disciplina
        $nomeCurso = '';
        if ($handle = mysql_query($cmdSQL, $db))
			while ($cursos = mysql_fetch_array($handle))
            	$nomeCurso = $nomeCurso.$cursos[0].", ";


        //conecta com o BD do repositorio
        $db = getConn();
        $cmdSQL = "INSERT INTO disciplinas (nome, topicos, cursoOriginal, descricao, nomeUsuario, emailUsuario, privado, dataEnvio) VALUES ('".$dados['nome_disc']."', '".serialize($arrayConteudo)."', '$nomeCurso', '$descricao', '$nomeUsuario', '$emailUsuario', '$isPrivado', NOW())";
        if (mysql_query($cmdSQL, $db))
        {

            $DirRandomico = md5(time()).rand(1,100000000);  
            $origemDir = $adaptwebDir.'disciplinas/'.$idUsuarioAdaptweb.'/'.$idDisciplinaAdaptweb;
            $destinoDir = "$adaptwebDir"."repositorio/temp/".$DirRandomico;
            if  (!(is_dir("$adaptwebDir/repositorio/temp/$DirRandomico")))
                  mkdir ("$adaptwebDir/repositorio/temp/$DirRandomico");


            //adiciona os t�picos um a um no reposit�rio
            $idDisciplinaRepositorio = getLastInsertID($db);
            addLearningObject ($idDisciplinaRepositorio, 3);
            addTopicos($idDisciplinaRepositorio, $idDisciplinaAdaptweb, $idUsuarioAdaptweb, $DirRandomico);
            copiarArquivos($origemDir, $destinoDir);

            $Insert = "INSERT INTO repositorioCaminho (idControle, caminhoArquivos)
                       VALUES ($idDisciplinaRepositorio, '$destinoDir' )";
            mysql_query($Insert, $db);

	        return true;
        }
    }
    return false;
}

/*
 Essa fun��o � chamada pela exportaDisciplina.
 Ela pega os topicos da disciplina e os adiciona na tabela TOPICOS do repositorio.
 Depois chama a addFiles que insere os arquivos no banco de dados.
*/

function addTopicos($idDisciplinaRepositorio, $idDisciplinaAdaptweb, $idUsuarioAdaptWeb, $DirRandomico)
{
    include ('repositorio/config.inc.php');
    //Cria um diret�rio de noe randomico
    //$DirRandomico = md5(time()).rand(1,100000000);  
    //conecta com o BD do adaptweb
    $dbAdaptweb = getAdaptwebConn();
    $cmdSQL = "SELECT Topicos FROM disciplina WHERE id_disc = 0$idDisciplinaAdaptweb";

    if ($handle = mysql_query($cmdSQL, $dbAdaptweb))
    	if (mysql_num_rows($handle))
	    {
	        //insere os dados no banco de dados de exporta��o
	        $dados = mysql_fetch_assoc($handle);
	        $topicos = unserialize($dados['Topicos']);
	        $topicos = trataArrayConteudo($topicos);

	        //conecta com o repositorio
	        $dbRepositorio = getConn();

	        for ($i = 0; $i < sizeof($topicos); $i++)
	        {
	            /*
	            ESTRANHO: no array de topicos, a descricao do
	            t�pico est� na variavel $topicos[$i]['abreviacao']...
	            */
	            $cmdSQL = "INSERT INTO topicos (idDisciplina, numero, nome, descricao, palavrasChave) VALUES ($idDisciplinaRepositorio, '".$topicos[$i]['NUMTOP']."', '".$topicos[$i]['DESCTOP']."', '".$topicos[$i]['ABREVIACAO']."', '".$topicos[$i]['PALCHAVE']."')";
                if (mysql_query($cmdSQL, $dbRepositorio))
                {
                    $idTopico = getLastInsertID($dbRepositorio);

			        addLearningObject ($idTopico, 2);

                    //verifica se o topico tem arquivos associados.
                    //se tiver, adiciona eles no repositorio
                    if (isset($topicos[$i]['ARQPRINCIPAL']['ARQTOP']))
                    { 
	                    $arquivoConteudo = $topicos[$i]['ARQPRINCIPAL']['ARQTOP'];
	                    if ($arquivoConteudo != '')
	                    { 
	                        addFile ($arquivoConteudo, $idTopico, $idDisciplinaAdaptweb,$idUsuarioAdaptWeb, $dbRepositorio, $idDisciplinaRepositorio, $DirRandomico);
                            if (isset($topicos[$i]['ARQPRINCIPAL']['ARQPRINCASSOC']))
                            { 
	                            $arquivosAssociados = $topicos[$i]['ARQPRINCIPAL']['ARQPRINCASSOC'];
	                            for ($j = 0; $j < sizeof($arquivosAssociados); $j++)
	                            {
	                                $arquivoAssociado = $arquivosAssociados[$j]['ARQTOPASSOC'];
	                                addFile ($arquivoAssociado, $idTopico, $idDisciplinaAdaptweb,$idUsuarioAdaptWeb, $dbRepositorio, $idDisciplinaRepositorio, $DirRandomico);
	                            }
                            }
	                    }
                    }
                }
                else
                	echo "Erro:<br>$cmdSQL<br>".mysql_error()."<hr>";
//----Samir
// para exemplos
			for($j=0; $j<sizeof($topicos[$i]['EXEMPLO']); $j++)
				{
				  if (isset($topicos[$i]['EXEMPLO'][$j]['ARQEXEMP']))
                    {
	                    $arquivoConteudo = $topicos[$i]['EXEMPLO'][$j]['ARQEXEMP'];
	                    if ($arquivoConteudo != '')
	                    {
	                        addFile ($arquivoConteudo, $idTopico, $idDisciplinaAdaptweb,$idUsuarioAdaptWeb, $dbRepositorio, $idDisciplinaRepositorio, $DirRandomico);
                            if (isset($topicos[$i]['EXEMPLO'][$j]['EXEMPLOASSOC']))
                            {
	                            $arquivosAssociados = $topicos[$i]['EXEMPLO'][$j]['EXEMPLOASSOC'];
	                            for ($z = 0; $z < sizeof($arquivosAssociados); $z++)
	                            {
	                                $arquivoAssociado = $arquivosAssociados[$z]['ARQEXEMPASSOC'];
	                                addFile ($arquivoAssociado, $idTopico, $idDisciplinaAdaptweb,$idUsuarioAdaptWeb, $dbRepositorio, $idDisciplinaRepositorio, $DirRandomico);
	                            }
                            }
	                    }
                    }
				}

// para exerc�cios
			for($j=0; $j<sizeof($topicos[$i]['EXERCICIO']); $j++)
				{
				  if (isset($topicos[$i]['EXERCICIO'][$j]['ARQEXER']))
                    {
	                    $arquivoConteudo = $topicos[$i]['EXERCICIO'][$j]['ARQEXER'];
	                    if ($arquivoConteudo != '')
	                    {
	                        addFile ($arquivoConteudo, $idTopico, $idDisciplinaAdaptweb,$idUsuarioAdaptWeb, $dbRepositorio, $idDisciplinaRepositorio, $DirRandomico);
                            if (isset($topicos[$i]['EXERCICIO'][$j]['EXERCICIOASSOC']))
                            {
	                            $arquivosAssociados = $topicos[$i]['EXERCICIO'][$j]['EXERCICIOASSOC'];
	                            for ($z = 0; $z < sizeof($arquivosAssociados); $z++)
	                            {
	                                $arquivoAssociado = $arquivosAssociados[$z]['ARQEXERASSOC'];
	                                addFile ($arquivoAssociado, $idTopico, $idDisciplinaAdaptweb,$idUsuarioAdaptWeb, $dbRepositorio, $idDisciplinaRepositorio, $DirRandomico);
	                            }
                            }
	                    }
                    }
				}


// para Materiais Complementares
			for($j=0; $j<sizeof($topicos[$i]['MATCOMP']); $j++)
				{
				  if (isset($topicos[$i]['MATCOMP'][$j]['ARQMATCOMP']))
                    {
	                    $arquivoConteudo = $topicos[$i]['MATCOMP'][$j]['ARQMATCOMP'];
	                    if ($arquivoConteudo != '')
	                    {
	                        addFile ($arquivoConteudo, $idTopico, $idDisciplinaAdaptweb,$idUsuarioAdaptWeb, $dbRepositorio, $idDisciplinaRepositorio, $DirRandomico);
                            if (isset($topicos[$i]['MATCOMP'][$j]['MATCOMPASSOC']))
                            {
	                            $arquivosAssociados = $topicos[$i]['MATCOMP'][$j]['MATCOMPASSOC'];
	                            for ($z = 0; $z < sizeof($arquivosAssociados); $z++)
	                            {
	                                $arquivoAssociado = $arquivosAssociados[$z]['ARQASSOCMAT'];
	                                addFile ($arquivoAssociado, $idTopico, $idDisciplinaAdaptweb,$idUsuarioAdaptWeb, $dbRepositorio, $idDisciplinaRepositorio, $DirRandomico);
	                            }
                            }
	                    }
                    }
				}

//----fim-Samir
			}

/*
    $origemDir = $adaptwebDir.'disciplinas/'.$idUsuarioAdaptWeb.'/'.$idDisciplinaAdaptweb;
    $destinoDir = "$adaptwebDir"."repositorio/temp/".$DirRandomico;
    if  (!(is_dir("$adaptwebDir/repositorio/temp/$DirRandomico")))
          mkdir ("$adaptwebDir/repositorio/temp/$DirRandomico", 0777, true);
          
        copiarArquivos($origemDir, $destinoDir);
        
        $cmdSQL = "INSERT INTO repositorioCaminho (idControle, caminhoArquivo)
                   VALUES ($idDisciplinaRepositorio, '$repositorioDir' )";
        mysql_query($cmdSQL, $db);
*/
	    }
        else
        	echo "Disciplina n�o encontrada<br>";
}

function copiarArquivos($origem,$destino)
 {
        clearstatcache();
        $origem = str_replace('//','/',$origem);
        $destino = str_replace('//','/',$destino);
        $arquivos = scandir($origem, 0);
        $total = count($arquivos);

        if(!file_exists($destino)){
            @mkdir($destino, 0777, true);    
        }

        for ($i=0;$i<$total;$i++){
            if (($arquivos[$i] != ".") and ($arquivos[$i] != "..")){
                if(!is_dir($origem. "/"  . $arquivos[$i] . "/")){
                    if(file_exists($destino . "/"  . $arquivos[$i])){
                        @unlink($destino . "/"  . $arquivos[$i]);
                    }
                    if (isXml($arquivos[$i]))
                    {
                        continue; 
                    }else
                    {
                        copy($origem . "/"  . $arquivos[$i],$destino . "/"  . $arquivos[$i]);
                    }
                } else {
                    if (isXml($arquivos[$i]))
                    {
                        continue; 
                    }else
                    {
                        copiarArquivos($origem . "/"  . $arquivos[$i],$destino . "/"  . $arquivos[$i]);
                    }    
                }
            }
        }        
}

if (PHP_VERSION<'5')
{
   function scandir($dir, $sortorder = 0) { 
       if(is_dir($dir))        { 
           $dirlist = opendir($dir); 
           while( ($file = readdir($dirlist)) !== false) { 
               if(!is_dir($file)) { 
                   $files[] = $file; 
               } 
           } 
           ($sortorder == 0) ? asort($files) : rsort($files); 
           return $files; 
       } else { 
       return FALSE; 
       break; 
       } 
   } 
}

/*
 Funcao que adiciona o arquivo $arquivo no banco de dados do repositorio
 Ela recebe o idTopico do t�pico que esse arquivo pertence,$idDisciplinaAdaptweb
 e $idUsuarioAdaptWeb pra achar o caminho do arquivo no sistema de arquivos e
 recebe tambem uma conex�o com o banco de dados , em $db.
*/
function addFile($arquivo, $idTopico, $idDisciplinaAdaptweb, $idUsuarioAdaptWeb, $db, $idDisciplinaRepositorio, $DirRandomico)
{ 
    include ('repositorio/config.inc.php');
    $path = $adaptwebDir.'disciplinas/'.$idUsuarioAdaptWeb.'/'.$idDisciplinaAdaptweb.'/';
    $nome_arquivo = "$path"."$arquivo";

    //$repositorioDir = "$adaptwebDir"."repositorio/temp/".$DirRandomico;
    //if (!(is_dir("$adaptwebDir/repositorio/temp/$DirRandomico")))
    //      mkdir ("$adaptwebDir/repositorio/temp/$DirRandomico");

    if (is_file("$nome_arquivo"))
    {
        if (isXml($nome_arquivo))
            continue;
        else
            $destino =  "$repositorioDir"."$arquivo";   

    //    $data = fread(fopen($nome_arquivo, "r"), filesize($nome_arquivo));
    //    $tamanho = strlen($data);
    //    $data = addslashes($data);
    //    $cmdSQL = "INSERT INTO arquivos (idTopico, nome, tamanho, bytes, idControle, caminhoArquivo)
    //               VALUES (1, '$arquivo', $tamanho, '$data', $idDisciplinaRepositorio, '$repositorioDir' )";
    //    mysql_query($cmdSQL, $db);

        $idArquivoRepositorio = getLastInsertID($db);
        addLearningObject ($idArquivoRepositorio, 1);
		return true;
   }
   return false;
}

/*
Adiciona uma entrada na tabela "LearningObject" do banco do reposit�rio.
Recebe o id do componete instrucional (que � o idArquivo, ou idTopico ou idDisciplina
conforme o valor do metadado aggregationLevel, tamb�m passado como par�metro:
+------------------+---------------------------+
| aggregationLevel | tipo de $idComponente     |
+------------------+---------------------------+
|        1         | raw data (arquivo)        |
+------------------+---------------------------+
|        2         | a lesson (topico)         |
+------------------+---------------------------+
|        3         | a discipline (disciplina) |
+------------------+---------------------------+
|        4         | a course (� se aplica aqui|
+------------------+---------------------------+
*/

function addLearningObject ($idComponente, $aggregationLevel)
{
	switch ($aggregationLevel)
    {
    	case 1: //arquivos
        	$cmdSQL = "SELECT nome, tamanho FROM arquivos WHERE idArquivo = 0$idComponente";
            $dbRepositorio = getConn();
            if ($handle = mysql_query($cmdSQL, $dbRepositorio))
            	if ($dados = mysql_fetch_assoc($handle))
                {
					$cmdSQL = "INSERT INTO learningobjects (title, aggregationLevel, idArquivo) VALUES ('".$dados['nome']."', 1, 0$idComponente)";
                    mysql_query($cmdSQL, $dbRepositorio);

                }
			return true;
            break;

        case 2:  //topicos
        	$cmdSQL = "SELECT nome, descricao, palavrasChave FROM topicos WHERE idTopico = 0$idComponente";
            $dbRepositorio = getConn();
            if ($handle = mysql_query($cmdSQL, $dbRepositorio))
            	if ($dados = mysql_fetch_assoc($handle))
                {
					$cmdSQL = "INSERT INTO learningobjects (title, description, keywords, aggregationLevel, idTopico) VALUES (";
                    $cmdSQL .= "'".$dados['nome']."'".", ";
                    $cmdSQL .= "'".$dados['descricao']."'".", ";
                    $cmdSQL .= "'".$dados['palavrasChave']."'".", ";
                    $cmdSQL .= "2, ";
                    $cmdSQL .= "0$idComponente)";
                    mysql_query($cmdSQL, $dbRepositorio);
                }
			return true;
            break;

        case 3:  //disciplinas
        	$cmdSQL = "SELECT nome, descricao FROM disciplinas WHERE idDisciplina = 0$idComponente";
            $dbRepositorio = getConn();
            if ($handle = mysql_query($cmdSQL, $dbRepositorio))
            	if ($dados = mysql_fetch_assoc($handle))
                {
					$cmdSQL = "INSERT INTO learningobjects (title, description, aggregationLevel, idDisciplina) VALUES (";
                    $cmdSQL .= "'".$dados['nome']."'".", ";
                    $cmdSQL .= "'".$dados['descricao']."'".", ";
                    $cmdSQL .= "3, ";
                    $cmdSQL .= "0$idComponente)";
                    mysql_query($cmdSQL, $dbRepositorio);
                }
			return true;
            break;

        case 4:
			return false;
            break;
    }
	return false;
}