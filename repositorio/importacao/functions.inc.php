<?php
require_once ('repositorio/database.inc.php');


/*
   SETA os campos referentes a curso, no array passado como parametro - o array 'topicos'
   recuperado da disciplina recuperado do banco de dados
   $idCurso eh o id do curso para o qual a disciplina esta sendo importada
*/

function trataArrayConteudo($topicos, $idCurso)
{
	$db = getAdaptwebConn();
    $handle = mysql_query("SELECT nome_curso FROM curso WHERE id_curso = 0$idCurso");
    $dados = mysql_fetch_assoc($handle);

    $arrayConteudo = unserialize($topicos);
    
	$arrayCurso = array('ID_CURSO' => $idCurso,'NOME_CURSO' => $dados['nome_curso'], 'STATUSCUR' => 'TRUE');
	
	$arrayCursoMat = array('ID_CURSOMAT' => $idCurso,'NOME_CURSOMAT' => $dados['nome_curso'], 'STATUSCURMAT' => 'TRUE');
	
	$arrayCursoExemp = array('ID_CURSOEXEMP' => $idCurso,'NOME_CURSOEXEMP' => $dados['nome_curso'], 'STATUSCUREXEMP' => 'TRUE');

	$arrayCursoExer = array('ID_CURSOEXER' => $idCurso,'NOME_CURSOEXER' => $dados['nome_curso'], 'STATUSCUREXER' => 'TRUE');

    for ($i = 0; $i < count($arrayConteudo); $i++)
    {
    	unset($arrayConteudo[$i]['CURSO']);
		$arrayConteudo[$i]['CURSO'][0] = $arrayCurso;
		for ($j = 0; $j < count($arrayConteudo[$i]['MATCOMP']); $j++)
		{
			unset($arrayConteudo[$i]['MATCOMP'][$j]['CURSOMAT']);
			$arrayConteudo[$i]['MATCOMP'][$j]['CURSOMAT'][0] = $arrayCursoMat;
		}
		for ($j = 0; $j < count($arrayConteudo[$i]['EXEMPLO']); $j++)
		{
			unset($arrayConteudo[$i]['EXEMPLO'][$j]['CURSOEXEMPLO']);
			$arrayConteudo[$i]['EXEMPLO'][$j]['CURSOEXEMPLO'][0] = $arrayCursoExemp;
		}
		for ($j = 0; $j < count($arrayConteudo[$i]['EXERCICIO']); $j++)
		{
			unset($arrayConteudo[$i]['EXERCICIO'][$j]['CURSOEXER']);
			$arrayConteudo[$i]['EXERCICIO'][$j]['CURSOEXER'][0]= $arrayCursoExer;
		}
    }
    //mysql_close($db);
    return serialize($arrayConteudo);
}


/* Adiciona uma disciplina na tabela de disciplinas exportadas.
   Recebe o id da disciplina no banco do REPOSITORIO */
function importaDisciplina($idDisciplinaRepositorio, $nomeDisciplina, $idUsuario, $idCurso)
{
    include ('repositorio/config.inc.php');
    //pega os dados do repositorio
    $cmdSQL = "SELECT idDisciplina, nome, topicos FROM disciplinas WHERE idDisciplina = $idDisciplinaRepositorio";
    //conecta com o BD do adaptweb
    $db = getConn();
    if ($handle = mysql_query($cmdSQL, $db))
    {
		//insere os dados no banco de dados de exportação
    	$dados = mysql_fetch_assoc($handle);

		$dbAdaptweb = getAdaptwebConn();

		$arrayConteudo = trataArrayConteudo($dados['topicos'], $idCurso);
		if ($nomeDisciplina == '')
        	//$nomeDisciplina = $dados['nome'];
          return false;

        $cmdSQL = "INSERT INTO disciplina (nome_disc, Topicos, Id_usuario, status_disc, status_xml)
                   VALUES ('".$nomeDisciplina."', '".$arrayConteudo."', $idUsuario, 0, 0)";

        if (mysql_query($cmdSQL, $dbAdaptweb))
        {
	        $idDisciplinaAdaptweb = getLastInsertID($dbAdaptweb);
	        mysql_query("INSERT INTO curso_disc (id_curso, id_disc) VALUES ($idCurso, $idDisciplinaAdaptweb)", $dbAdaptweb);

	        //insere os arquivos que pertencem a essa disciplina
	        $disciplinaDir = "$adaptwebDir"."disciplinas/$idUsuario/$idDisciplinaAdaptweb/";
	        if (!(is_dir("$adaptwebDir/disciplinas/$idUsuario")))
	        	mkdir ("$adaptwebDir/disciplinas/$idUsuario");

            importFiles($idDisciplinaRepositorio, $disciplinaDir);
	        return true;
        }
    	else
    		die('Erro ao executar query. <br>Erro: '.mysql_error()."<br>Comando PHP: mysql_query('$cmdSQL', $dbAdaptweb)");
	}
    return false;
}

function VerificaNome($nomeDisciplina, $idUsuario, $idCurso)
{
    include ('repositorio/config.inc.php');
   // $nomeDisciplina = utf8_decode($nomeDisciplina);
    //pega os dados do repositorio
    $cmdSQL = "SELECT nome_disc FROM disciplina WHERE nome_disc='$nomeDisciplina' and id_usuario=$idUsuario";

    //conecta com o BD do adaptweb
    $db = getConn();
    if ($handle = mysql_query($cmdSQL, $db))
    {
        $qtd = 0;
        $qtd = mysql_num_rows($handle);      

      if ($qtd > 0)
              return false;
      else
              return true;
    }
}


function importFiles($idDisciplina, $path)
{


    include ('repositorio/config.inc.php');
    if (mkdir($path, 0777))
    {
	    // Abre o diretorio e faz a leitura de seu conteudo

        $db = getConn();
    //   $cmdSQL = "SELECT a.nome AS nome, a.bytes AS bytes
    //               FROM arquivos a INNER JOIN topicos t ON (a.idTopico = t.idTopico)
    //               WHERE t.idDisciplina = 0$idDisciplina";
    // $cmdSQL = "SELECT a.nome AS nome, a.bytes AS bytes, a.caminhoArquivo AS origem
    //               FROM arquivos a WHERE a.idControle = $idDisciplina";        
     $cmdSQL = "SELECT a.caminhoArquivos AS origem
                   FROM repositorioCaminho a WHERE a.idControle = $idDisciplina";        

        $handle = mysql_query($cmdSQL, $db);
        $dados = mysql_fetch_assoc($handle);
        $origem = $dados['origem'];

        copiarArquivos($origem, $path);
   } //mkdir
}

function copiarArquivos($origem,$destino)
{
        clearstatcache();
        $origem = str_replace('//','/',$origem);
        $destino = str_replace('//','/',$destino);
        $arquivos = scandir($origem, 0);
        $total = count($arquivos);
        if(!file_exists($destino)){
            @mkdir($destino);    
        }
        for ($i=0;$i<$total;$i++){
            if (($arquivos[$i] != ".") and ($arquivos[$i] != "..")){
                if(!is_dir($origem. "/"  . $arquivos[$i] . "/")){
                    if(file_exists($destino . "/"  . $arquivos[$i])){
                        @unlink($destino . "/"  . $arquivos[$i]);
                    }
                    copy($origem . "/"  . $arquivos[$i],$destino . "/"  . $arquivos[$i]);
                } else {
                    copiarArquivos($origem . "/"  . $arquivos[$i],$destino . "/"  . $arquivos[$i]);
                }
            }
        }        
}

if (PHP_VERSION<'5')
{

   function scandir($dir, $sortorder = 0) { 
       if(is_dir($dir))        { 
           $dirlist = opendir($dir); 
           while( ($file = readdir($dirlist)) !== false) { 
               if(!is_dir($file)) { 
                   $files[] = $file; 
               } 
           } 
           ($sortorder == 0) ? asort($files) : rsort($files); 
           return $files; 
       } else { 
       return FALSE; 
       break; 
       } 
   } 
}


?>