<?php
global $NomeDisciplina, $conteudo, $tipo_usuario, $id_usuario; 

// **************************************************************************
// *  CRIA MATRIZ ORELHA                                                    *
// **************************************************************************
 
$orelha = array();  
  

  $orelha = array(
  		array(   
   		   "LABEL" => "Reposit�rio", 
     		   "LINK" => "a_index.php?opcao=Repositorio",    
     		   "ESTADO" => "OFF"
   		   ),
		array(   
   		   "LABEL" => A_LANG_EXPORT, 
     		   "LINK" => "a_index.php?opcao=Export",    
     		   "ESTADO" => "OFF"
   		   ),
		array(   
   		   "LABEL" => A_LANG_IMPORT, 
     		   "LINK" => "a_index.php?opcao=Import",    
     		   "ESTADO" => "ON"
   		   ),
    array(   
         "LABEL" => "Gerenciamento", 
           "LINK" => "a_index.php?opcao=Gerenciar",    
           "ESTADO" => "OFF"
         )/*,    
		array(   
   		   "LABEL" => A_LANG_OBJECT_SEARCH, 
     		   "LINK" => "a_index.php?opcao=Pesq",    
     		   "ESTADO" => "OFF"
   		   )*/
   		  ); 

MontaOrelha($orelha);  
  
include ('repositorio/config.inc.php');
include ('repositorio/database.inc.php');
require_once ('repositorio/importacao/functions.inc.php');

$idDisciplina = 0;
if (isset($_POST['iddisciplina']))
	$idDisciplina = $_POST['iddisciplina'];

//$nomeDisciplina = '';
if (isset($_POST['nomedisciplina']))
	$nomeDisciplina = $_POST['nomedisciplina'];

$idCurso = 0;
if (isset($_POST['idcurso']))
	$idCurso = $_POST['idcurso'];

$idUsuario = $_SESSION['id_usuario'];
$emailUsuario = $_SESSION['email_usuario'];
$nomeUsuario = $_SESSION['nome_usuario'];
?>

<script type="text/javascript"> 
<!--//--><![CDATA[//><!-- 
function dados_disciplina(valor)
{ 
  var div = document.getElementById("ajax_processador"); 
  var destino = 'repositorio/dadosdisciplina.inc.php?iddisciplina=' + valor;
  div.innerHTML = "<center><img src=imagens/indicator.gif></center>"; 
  var ajax = new Ajax(); 
  ajax.set_receive_handler( function(c) { div.innerHTML = c; } ); 
  //alert (destino);
       ajax.send(destino); 
} 
//--><!]]> 
</script>


<script>
function descricaoDisciplina(idDisciplina)
{
	document.frameDescricao.location.href = 'repositorio/dadosdisciplina.inc.php?iddisciplina=' + idDisciplina;
//    document.frameDescricao.location.reload(true);
}

function enviaForm()
{

    if (document.frmImportacao.iddisciplina.selectedIndex < 0)
    {
    	alert ("Por favor, selecione uma disciplina do Reposit�rio.");
        return false;
    }

    if (document.frmImportacao.idcurso.value < 0)
    {
        alert ("Por favor, associe a disciplina escolhida a um curso.");
        document.frmImportacao.idcurso.focus();
        return false;
    }

    if (document.frmImportacao.nomedisciplina.value =='')
    {
        alert ("Por favor, preencha o campo Nome para a Disciplina.");
        document.frmImportacao.nomedisciplina.focus();
        return false;
    }


    $('#dvLoading').fadeIn(200);
    document.frmImportacao.submit();
    return true;
}

<?
$mensagem = "";
$qt_erro = 0;

if (isset($_POST['iddisciplina']))
{
  if (($idDisciplina > 0) && ($idCurso > 0) && ($idUsuario > 0))

     if (!(VerificaNome($nomeDisciplina, $idUsuario, $idCurso)))
     {
             $mensagem = "Nome de Disciplina j� existe! Escolha outro nome.";
             $qt_erro++;
     }
     else
     { 


	   if (importaDisciplina($idDisciplina, $nomeDisciplina, $idUsuario, $idCurso))
       { 
		          //echo "alert(\"".A_LANG_REP_IMP_SUCCESS."\");";
              $mensagem = "Importa��o da disciplina realizada com sucesso!";

       }  
       else
       {  
    	       $mensagem = A_LANG_REP_IMP_ERROR;
             $qt_erro++;
	     }
      } 
else
{
             $mensagem = A_LANG_REP_INVALID_PAR;
             $qt_erro++;
}  
  
}

if  ($id_usuario == 2)
{
      
      $mensagem = A_LANG_DEMO;
      $qt_erro++;
      $status_botao = "disabled";
      
}    
?>
</script>



<table CELLSPACING=1 CELLPADDING=1 width="100%"  border ="0"  bgcolor="<? echo $A_COR_FUNDO_ORELHA_ON ?>"  >
 <tr valign="top">
    <td>
     
    <form method="post" action="a_index.php?opcao=Import" name="frmImportacao">


      <table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">
          <tr>
           <td>
             <table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0">
                   <tr>
                     <td>  
                        <?php
                        if ($mensagem == '')
                          {
                          echo "<div class=\"Mensagem_Ok\">"; 
                          echo "<p class=\"texto1\">\n";     
                          echo "<table cellspacing=\"0\" cellpadding=\"0\">";
                          echo "  <tr>";
                          echo "    <td width=\"50\">";
                          echo "      <img src=\"imagens/icones/notice.png\" alt=\"\" />";
                          echo "    </td>";
                          echo "    <td valign=\"center\">";
                          echo A_LANG_REP_IMP_TEXT ;  
                          echo "    </td>";
                          echo "  </tr>";
                          echo "</table>";  
                          echo "</p>\n";  
                          echo "</div>";    
                         }
                         ?>    
                      </td>
                  </tr>       
             </table> 
           </td> 
          </tr>

          <?php
          if ($mensagem != '')
          {
                if (($qt_erro == 0))
                {   
                  $tipo_msg = "success";
                  $icone = "<img src='imagens/icones/success.png' border='0' />";
                  $db = getConn();
                  $cmdSQL = "SELECT MAX(id_Disc) as IDzao FROM disciplina WHERE id_usuario = $idUsuario";
                  if ($handle = mysql_query($cmdSQL, $db))
                    if ($dados = mysql_fetch_assoc($handle))
                    {
                        $IDzao =  $dados['IDzao']; 
                    }                        
                }
                else
                { 
                  $tipo_msg = "error";
                  $icone = "<img src='imagens/icones/error.png' border='0' />";       
                }
             ?>
            <script type="text/javascript">
                showNotification({
                  message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo $mensagem; ?></td><?php if ($tipo_msg == 'success') {?><td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class='button' type='button' value='Estruturar Conte�do da Disciplina Importada' onClick=window.parent.location='a_index.php?opcao=TopicosEstruturarArvore&CodigoDisciplina=<?php echo $IDzao; ?>'></td> <?php } ?></tr></table>",              
                  type: "<? echo $tipo_msg; ?>",
                  autoClose: true,
                  duration: 8                                        
                });
                //error //success //warning
            </script>                                                    

          <?php } ?>






          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr> 
                <td valign="top" width = "170" align ="right">
                       <? echo "Disciplinas para Importa��o";/*A_LANG_REP_DISC_IN_REP;*/ ?>:
                </td>
                <td align ="left">
                    <select name="iddisciplina" size="10" maxsize="15" style="width: 526px" onchange="dados_disciplina(this.value);">
                    <?
                      $db = getConn();
                      $handle = mysql_query("SELECT idDisciplina, nome, nomeUsuario FROM disciplinas ORDER BY nome", $db);
                      if ($handle)
                         while ($dados = mysql_fetch_assoc($handle))
                         {
                             if ($idDisciplina == $dados['idDisciplina'])
                                 echo '<option value="'.$dados['idDisciplina'].'">'.$dados['nome'].' - '.$dados['nomeUsuario'].'</option>';
                             else
                                 echo '<option value="'.$dados['idDisciplina'].'">'.$dados['nome'].' - '.$dados['nomeUsuario'].'</option>';
                         }
                    ?>
                    </select>
                    <br><br>
                    <div id="ajax_processador" style="width: 524px; height:120px; border-width:1px; background:#eeeeee; border-color:#000000;  border-style: dotted;" >
                      <table width="100%" border=0 cellspacing=0 cellpadding=2>
                        <tr>
                            <td width="100%">Informa��es sobre a disciplina escolhida.</td>
                        </tr>
                      </table>
                    <div>
               
	              </td>
              </tr>

              <tr>  
                <td valign="top">
                   
                </td>
                <td align ="left">      
                   
                </td>
              </tr>   

              <tr>  
                <td valign="top">
                   
                </td>
                <td align ="left">      
                   
                </td>
              </tr>                 

              <tr>  
                <td valign="top" align="right">
                    <? echo "Associe a disciplina a um curso";?>:
                    
                </td>
                <td align ="left">   
                <div class="styled-select-big">    
                    <select name="idcurso" size="1" maxsize="20" style="width: 548px;" >
                                <option value="-1">Selecione o curso</option>
                      <?
                      $db = getAdaptwebConn();
                      $handle = mysql_query("SELECT id_curso, nome_curso FROM curso WHERE Id_usuario = $idUsuario ORDER BY nome_curso", $db);
                      if ($handle)
                          while ($dados = mysql_fetch_assoc($handle))
                               echo '<option value="'.$dados['id_curso'].'">'.$dados['nome_curso'].'</option>';
                      ?>
                    </select>
                </div>    
                    <br>&nbsp;<a href="a_index.php?opcao=CadastroCurso"><b>Acesse aqui</b></a>, caso o curso desejado ainda n�o esteja criado / listado acima
                </td>
	            </tr>


              <tr>
                 <td valign="middle" align="right">
                 </td>
                 <td valign="middle" align ="left">
                 </td>
              </tr>          

	            <tr>
	               <td valign="top" align="right">
	                   <? echo "Nome para a disciplina";?>:
	               </td>
	               <td valign="middle" align ="left">
	                   <input class="text" name="nomedisciplina" type="text" value="<?=$nomeDisciplina?>" style="width: 520px;" >
	               </td>
              </tr>

              <tr>
                 <td >
                     
                 </td>
                  <td >
                    <table width=525 cellpadding=0 cellspacing=0 border=0>
                     <tr>
                     <td align=right> 
	                    <br><!-- <input type="button" class="buttonBig" name="importar" value="Importar!" onclick="enviaForm()"> -->
                      <script type="text/javascript">
                      $(
                          
                          function () 
                          {
                          $("#customDialog").easyconfirm
                          (
                            { locale: 
                              {
                                title: '',
                                button: ['N�o',' Sim'],
                                closeText: 'Fechar'
                               }, dialog: $("#question")
                            }
                            
                          );
                          $("#customDialog").click(function() { enviaForm() });
                         
                          }
                        );

                      </script>
                        <input type="button" class="buttonBig" onClick="history.go(-1)" value="Voltar">
                        &nbsp;&nbsp;
                        <input class="buttonBig" type="button" value="Importar" name="importar" id="customDialog" <? echo $status_botao; ?>>
                        <div class="dialog" id="question">
                          <table>
                            <tr>
                              <td width="50">
                                <img src="imagens/icones/notice.png" alt="" />
                              </td>
                              <td valign="center">
                                A importa��o pode demorar alguns minutos. Continuar ?
                              </td>
                            </tr>
                          </table>  
                        </div>
                    </td>
                    </tr>
                    </table>    
	               </td>
			        </tr>

 
	        </table>
        </td>
      </tr>
</table>  
    </form>   <br>
	  </td>
  </tr>
</table>
<!--
Fim do Zamin
-->
</td>
</tr>
</table>