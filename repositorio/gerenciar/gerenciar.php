<?
/* -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Inform�tica  
 *       UEL - Departamento de Computa��o
 * -----------------------------------------------------------------------
 *       @package Reposit�rio 
 *     @subpakage Reposit�rio -> Importa��o
 *          @file repositorio.php
 *    @desciption Reposit�rio de Disciplinas
 *         @since 19/06/2004
 *        @author Eduardo Zamin e Samir Merode
 * -----------------------------------------------------------------------         
 */   
?>

<?
global $NomeDisciplina, $conteudo, $tipo_usuario, $id_usuario; 

// **************************************************************************
// *  CRIA MATRIZ ORELHA                                                    *
// **************************************************************************
 
$orelha = array();  
  

  $orelha = array(
  		array(   
   		   "LABEL" => "Reposit�rio", 
     		   "LINK" => "a_index.php?opcao=Repositorio",    
     		   "ESTADO" => "OFF"
   		   ),
		array(   
   		   "LABEL" => A_LANG_EXPORT, 
     		   "LINK" => "a_index.php?opcao=Export",    
     		   "ESTADO" => "OFF"
   		   ),
		array(   
   		   "LABEL" => A_LANG_IMPORT, 
     		   "LINK" => "a_index.php?opcao=Import",    
     		   "ESTADO" => "OFF"
   		   ),
    array(   
         "LABEL" => "Gerenciamento", 
           "LINK" => "a_index.php?opcao=Gerenciar",    
           "ESTADO" => "ON"
         )/*,    
		array(   
   		   "LABEL" => A_LANG_OBJECT_SEARCH, 
     		   "LINK" => "a_index.php?opcao=Pesq",    
     		   "ESTADO" => "OFF"
   		   )*/
   		  ); 

MontaOrelha($orelha);  
  
include ('repositorio/config.inc.php');
include ('repositorio/database.inc.php');
require_once ('repositorio/gerenciar/functions.inc.php');

$idDisciplina = 0;
if (isset($_POST['iddisciplina']))
	$idDisciplina = $_POST['iddisciplina'];

$idUsuario = $_SESSION['id_usuario'];
$emailUsuario = $_SESSION['email_usuario'];
$nomeUsuario = $_SESSION['nome_usuario'];
?>

<script type="text/javascript"> 
<!--//--><![CDATA[//><!-- 
function dados_disciplina(valor)
{ 
  var div = document.getElementById("ajax_processador"); 
  var destino = 'repositorio/dadosdisciplina.inc.php?iddisciplina=' + valor;
  div.innerHTML = "<center><img src=imagens/indicator.gif></center>"; 
  var ajax = new Ajax(); 
  ajax.set_receive_handler( function(c) { div.innerHTML = c; } ); 
  //alert (destino);
       ajax.send(destino); 
} 
//--><!]]> 
</script>


<script>

function enviaForm()
{

    if (document.frmGerencia.iddisciplina.selectedIndex < 0)
    {
    	alert ("Por favor, selecione uma disciplina do Reposit�rio.");
        return false;
    }

    document.frmGerencia.submit();
    return true;
}

<?

$mensagem = "";
$qt_erro = 0;

if (isset($_POST['iddisciplina']))
{
  if (($idDisciplina > 0)  && ($idUsuario > 0))
	   if (apagaDisciplina($idDisciplina, $idUsuario, $emailUsuario))
       { 
		          $mensagem = "Exclus�o da disciplina realizada com sucesso!";
       }  
       else
       {  
    	       $mensagem = A_LANG_REP_IMP_ERROR;
             $qt_erro++;
	     }
  else
  {
             $mensagem = A_LANG_REP_INVALID_PAR;
             $qt_erro++;    
  }  
}

if  ($id_usuario == 2)
{
      
      $mensagem = A_LANG_DEMO;
      $qt_erro++;
      $status_botao = "disabled";
      
}    

?>
</script>



<table CELLSPACING=1 CELLPADDING=1 width="100%"  border ="0"  bgcolor="<? echo $A_COR_FUNDO_ORELHA_ON ?>"  >
 <tr valign="top">
    <td>
     
    <form method="post" action="a_index.php?opcao=Gerenciar" name="frmGerencia">


      <table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">
          <tr>
           <td>
             <table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0">
                   <tr>
                     <td>  
                        <?php
                    if ($mensagem == '')
                    {
                         echo "<div class=\"Mensagem_Ok\">"; 
                         echo "<p class=\"texto1\">\n";     
                         echo "<table cellspacing=\"0\" cellpadding=\"0\">";
                         echo "  <tr>";
                         echo "    <td width=\"50\">";
                         echo "      <img src=\"imagens/icones/notice.png\" alt=\"\" />";
                         echo "    </td>";
                         echo "    <td valign=\"center\">";
                         echo "Atrav�s dessa interface voc� poder� excluir as disciplinas de sua autoria que foram exportadas para o Reposit�rio do AdaptWeb. 
                         Selecione a disciplina na lista abaixo e clique em \"Excluir\". <br> A exclus�o da disciplina do Reposit�rio <b>N�O</b> apaga a disciplina que a originou. ";
                         echo "    </td>";
                         echo "  </tr>";
                         echo "</table>";  
                         echo "</p>\n";   
                         echo "</div>";   
                    }     
                         ?>    
                       
                       </td>
                     </tr>       
             </table>  
           </td> 
          </tr>

<?php
if ($mensagem != '')
{
 
      if (($qt_erro == 0))
      {   
        $tipo_msg = "success";
        $icone = "<img src='imagens/icones/success.png' border='0' />";
      }
      else
      { 
        $tipo_msg = "error";
        $icone = "<img src='imagens/icones/error.png' border='0' />";       
      }
   ?>
  <script type="text/javascript">
      showNotification({
        message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td width=10 >&nbsp;</td><td valign=middle class=mensagem_shadow><?php echo $mensagem; ?></td></tr></table>",              
        type: "<? echo $tipo_msg; ?>",
        autoClose: true,
        duration: 8                                        
      });
      //error //success //warning
  </script>     

<?php } ?>

          <tr>
            <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr> 
                <td valign="top" width = "170" align ="right">
                       <? echo "Minhas Disciplinas no Reposit�rio";/*A_LANG_REP_DISC_IN_REP;*/ ?>:
                </td>
                <td align ="left">
                    <select name="iddisciplina" size="10"  maxsize="15" style="width: 526px" onchange="dados_disciplina(this.value);">
                    <?
                      $db = getConn();
                      $handle = mysql_query("SELECT idDisciplina, nome, nomeUsuario FROM disciplinas where emailUsuario='$emailUsuario' ORDER BY nome", $db);
                      if ($handle)
                         while ($dados = mysql_fetch_assoc($handle))
                         {
                                 echo '<option value="'.$dados['idDisciplina'].'">'.$dados['nome'].' - '.$dados['nomeUsuario'].'</option>';
                         }
                    ?>
                    </select>
                    <br><br>
                    <div id="ajax_processador" style="width: 524px; height:120px; border-width:1px; background:#eeeeee; border-color:#000000;  border-style: dotted;" >
                      <table width="100%" border=0 cellspacing=0 cellpadding=2>
                        <tr>
                            <td width="100%">Informa��es sobre a disciplina escolhida.</td>
                        </tr>
                      </table>
                    <div>
               
	              </td>
              </tr>

              <tr>  
                <td valign="top">
                   
                </td>
                <td align ="left">      
                   
                </td>
              </tr>   

              <tr>
                 <td >
                     
                 </td>
                  <td>
                    <table width=527 cellpadding=0 cellspacing=0 border=0>
                     <tr>
                     <td align=right>   

                      <script type="text/javascript">
                      $(
                          
                          function () 
                          {
                          $("#customDialog").easyconfirm
                          (
                            { locale: 
                              {
                                title: '',
                                button: ['N�o',' Sim'],
                                closeText: 'Fechar'
                               }, dialog: $("#question")
                            }
                            
                          );
                          $("#customDialog").click(function() { enviaForm() });
                         
                          }
                        );

                      </script>
                        <input type="button" class="buttonBig" onClick="history.go(-1)" value="Voltar">
                        &nbsp;&nbsp;
                        <input class="buttonBig" type="button" value="Excluir" name="btnExcluir" id="customDialog" <? echo $status_botao; ?>>
                        <div class="dialog" id="question">
                          <table>
                            <tr>
                              <td width="50">
                                <img src="imagens/icones/notice.png" alt="" />
                              </td>
                              <td valign="center">
                                Confirma a exclus�o da disciplina do Reposit�rio ?
                              </td>
                            </tr>
                          </table>  
                        </div>
                    </td>
                    </tr>
                    </table>                           
	               </td>
			        </tr>

 
	        </table>
        </td>
      </tr>
</table>  
    </form>   <br>
	  </td>
  </tr>
</table>
<!--
Fim do Zamin
-->
</td>
</tr>
</table>