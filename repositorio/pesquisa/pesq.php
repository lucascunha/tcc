<?
/* -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Informática  
 *       UEL - Departamento de Computação
 * -----------------------------------------------------------------------
 *       @package Repositório 
 *     @subpakage Repositório -> Pesquisa
 *          @file repositorio.php
 *    @desciption Repositório de Disciplinas
 *         @since 19/06/2004
 *        @author Eduardo Zamin e Samir Merode
 * -----------------------------------------------------------------------         
 */   
?>

<?
global $NomeDisciplina, $conteudo, $tipo_usuario, $id_usuario; 

include ('repositorio/config.inc.php');
include ('repositorio/database.inc.php');


// **************************************************************************
// *  CRIA MATRIZ ORELHA                                                    *
// **************************************************************************
 
$orelha = array();  
  

  $orelha = array(
  		array(   
   		   "LABEL" => A_LANG_PRESENTATION, 
     		   "LINK" => "a_index.php?opcao=Repositorio",    
     		   "ESTADO" => "OFF"
   		   ),
		array(   
   		   "LABEL" => A_LANG_EXPORT, 
     		   "LINK" => "a_index.php?opcao=Export",    
     		   "ESTADO" => "OFF"
   		   ),
		array(   
   		   "LABEL" => A_LANG_IMPORT, 
     		   "LINK" => "a_index.php?opcao=Import",    
     		   "ESTADO" => "OFF"
   		   ),
    array(   
         "LABEL" => "Gerenciamento de Disciplinas", 
           "LINK" => "a_index.php?opcao=Gerenciar",    
           "ESTADO" => "OFF"
         ),      
		array(   
   		   "LABEL" => A_LANG_OBJECT_SEARCH, 
     		   "LINK" => "a_index.php?opcao=Pesq",    
     		   "ESTADO" => "ON"
   		   )
   		  ); 


MontaOrelha($orelha);  


if  ($id_usuario == 2)
{
      
      $mensagem = A_LANG_DEMO;
      $tipo_msg = "error";
      $icone = "<img src='imagens/icones/error.png' border='0' />";
      ?>
    <script type="text/javascript">
        showNotification({
          message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td valign=middle class=mensagem_shadow>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $mensagem; ?></td></tr></table>",
          type: "<? echo $tipo_msg; ?>",
          autoClose: true,
          duration: 8                                        
        });
    </script>   
    <?      
}
  
?>

<table CELLSPACING=1 CELLPADDING=1 width="100%"  border ="0"  bgcolor="<? echo $A_COR_FUNDO_ORELHA_ON ?>" style="height:480px;" >
 <tr valign="top">
    <td>
     
      <table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px;  " class="tabela_redonda">
          <tr>
           <td colspan=4>
             <table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0">
                   <tr>
                     <td >  
                        <?php
                    if ($mensagem == '')
                    {
                         echo "<div class=\"Mensagem_Ok\">"; 
                         echo "<p class=\"texto1\">\n";     
                         echo "<table cellspacing=\"0\" cellpadding=\"0\">";
                         echo "  <tr>";
                         echo "    <td width=\"50\">";
                         echo "      <img src=\"imagens/icones/notice.png\" alt=\"\" />";
                         echo "    </td>";
                         echo "    <td valign=\"center\">";
                         echo A_LANG_REP_SEARCH;
                         echo "    </td>";
                         echo "  </tr>";
                         echo "</table>";  
                         echo "</p>\n";   
                         echo "</div>";   
                    }     
                         ?>    
                       
                       </td>
                     </tr>       
             </table>  
           </td> 
          </tr>
            <form method="post" action="a_index.php?opcao=Pesq">


      <tr>
      <td>

        <table width=100% border=0 class="tabela_redonda">  
          <tr>
            <td>
              <br><br>
            </td>
          </tr>
          <tr>
           <td>
              <table cellspacing="0" cellpadding="2" align=right >
                <tr>
                  <td width="80">
                    <div align="right">
                        <input type="checkbox" name="chkTitle" value="checkbox" <? if (isset($_POST['chkTitle'])) echo 'checked' ?>>
                    </div>
                  </td>
                  <td width="80">
                        <? echo A_LANG_REP_SCH_TITLE?>
                  </td>
                </tr>  
              </table>
           </td>    
           <td>
              <table cellspacing="0" cellpadding="2" align=center>
                <tr >
                  <td width="80">
                    <div align="right">
                        <input type="checkbox" name="chkDescription" value="checkbox" <? if (isset($_POST['chkDescription'])) echo 'checked' ?>>
                    </div>
                  </td>
                  <td width="80">
                        <? echo A_LANG_TENVIRONMENT_EXAMPLES_DSC;?>
                  </td>
                </tr>  
              </table>
            </td>   
            <td>
              <table cellspacing="0" cellpadding="2" align=left>
                <tr>
                  <td width="100">
                    <div align="right">
                      <input type="checkbox" name="chkKeywords" value="checkbox" <? if (isset($_POST['chkKeywords'])) echo 'checked' ?>>
                    </div>
                  </td>
                  <td width="100">
                      <? echo A_LANG_TOPIC_WORDS_KEY;?>
                  </td>
                </tr>  
              </table>
            </td>   
            <td>
              <table cellspacing="0" cellpadding="2" align=left>
                <tr >
                  <td>
                          <? echo A_LANG_REP_AGGREG;?>&nbsp;&nbsp;
                          <select name="aggregationLevel" style="font-size: 9pt; width:200px;">
                            <option value="0"><? echo A_LANG_REP_SCH_SELECT;?></option>
                            <option value="1" <? if ( (isset($_POST['aggregationLevel'])) && (int) $_POST['aggregationLevel'] == 1) echo 'selected' ?>>1 - <? echo A_LANG_REP_SCH_RAW;?></option>
                            <option value="2" <? if ( (isset($_POST['aggregationLevel'])) && (int) $_POST['aggregationLevel'] == 2) echo 'selected' ?>>2 - <? echo A_LANG_REP_SCH_TOPIC;?></option>
                            <option value="3" <? if ( (isset($_POST['aggregationLevel'])) && (int) $_POST['aggregationLevel'] == 3) echo 'selected' ?>>3 - <? echo A_LANG_REP_SCH_DISC;?></option>
                            <option value="4" <? if ( (isset($_POST['aggregationLevel'])) && (int) $_POST['aggregationLevel'] == 4) echo 'selected' ?>>4 - <? echo A_LANG_REP_SCH_COURSE;?></option>
                          </select>
                  </td>
                </tr>
              </table>
            </td>    
          </tr>
          <tr>
  
                  <td colspan="4">
                    <br><br>
                    <center>
                    <input type="text" name="criterio" style="width:500px;height:25px;" maxlength="64" >&nbsp;&nbsp;&nbsp;
                    <input class=buttonBig type="submit" name="Submit" value="<? echo A_LANG_REP_SCH_SEARCH;?>">
                    </center>
                    <br><br><br>
                  </td>

          </tr>  

        </table>  

<br><br>

</td>
</tr>


          </form>
            <?
			if (isset($_POST['criterio']))
			{
              $primeira_vez=TRUE;

	            $criterio = $_POST['criterio'];

	            $cmdSQL = "SELECT idlearningobject, title, description, keywords, aggregationLevel FROM learningobjects WHERE 1=1 ";

                if (strlen($criterio))
                {
	                if (isset($_POST['chkTitle']))
	                    $cmdSQL .= " AND title LIKE '%$criterio%'";

	                if (isset($_POST['chkDescription']))
	                    $cmdSQL .= " AND description LIKE '%$criterio%'";

	                if (isset($_POST['chkKeywords']))
	                    $cmdSQL .= " AND keywords LIKE '%$criterio%'";
                }

                if ( (int) $_POST['aggregationLevel'] > 0)
                    $cmdSQL .= " AND aggregationLevel = ".$_POST['aggregationLevel'];

              $cmdSQL .= ' ORDER BY aggregationLevel DESC, title ASC';
	            $dbRepositorio = getConn();
                if (!($handle = mysql_query($cmdSQL)))
                	echo A_LANG_REP_SCH_ERROR.": " & mysql_error();
                else
                {
                	if (mysql_num_rows($handle) > 0)

                    {


                      while ($dados = mysql_fetch_assoc($handle))
                      {
                          $idlearningobject = $dados['idlearningobject'];
                          $title = $dados['title'];
                          $description = $dados['description'];
                          $keywords = $dados['keywords'];
                          switch (  (int) $dados['aggregationLevel']  )
                          {
                           		case 1:  $aggregationLevel = '1 - Raw Data'; break;
                                case 2:  $aggregationLevel = '2 - Topic'; break;
                                case 3:  $aggregationLevel = '3 - Discipline'; break;
                                case 4:  $aggregationLevel = '4 - Course'; break;
                                default: $aggregationLevel = '';
                          }
                          ?>
          <tr>
            <td colspan=4>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <?php
                  if ($primeira_vez){
                      $primeira_vez = FALSE;
                      echo "<font size=4>";
                      if (mysql_num_rows($handle) == 1)
                         echo mysql_num_rows($handle).' '.A_LANG_REP_SCH_REG.': <br><br>';
                      else
                         echo mysql_num_rows($handle).' '.A_LANG_REP_SCH_REGS.': <br><br>';
                      }
                      echo "</font>";
                           
                  ?>
                </td>  
              </tr>  
              <tr>
                <td style="border: 1px solid #ccc">
                  <table width="100%" border="0" cellspacing="2" cellpadding="5">
                    <tr> 
                      <td nowrap align="right" bgcolor="#eee"><font size="2"><? echo A_LANG_REP_SCH_TITLE;?>: 
                        </font></td>
                      <td><font size="2"><b> 
                        <?=$title?>
                        </b> </font></td>
                      <td align="right">id: 
                        <?=$idlearningobject?>
                      </td>
                    </tr>
                    <tr> 
                      <td nowrap align="right" bgcolor="#eee"><font size="2"><? echo A_LANG_TENVIRONMENT_EXAMPLES_DSC;?>: 
                        </font></td>
                      <td width="483" colspan="2"> <font size="2"> 
                        <?=$description?>
                        </font></td>
                    </tr>
                    <tr> 
                      <td nowrap align="right" bgcolor="#eee"><font size="2"><? echo A_LANG_TOPIC_WORDS_KEY;?>: 
                        </font></td>
                      <td width="483" colspan="2"> <font size="2"> 
                        <?=$keywords?>
                        </font></td>
                    </tr>
                    <tr> 
                      <td nowrap align="right" bgcolor="#eee"><font size="2"><? echo A_LANG_TOPIC_WORDS_KEY;?>:</font></td>
                      <td width="483" colspan="2"> <font size="2"> 
                        <?=$aggregationLevel?>
                        </font></td>
                    </tr>
                  </table>
	            </td>
	          </tr>
	        </table>
         </td> 
	       </tr>
                        <?
                      }
                	}
                  else
                  {
                    ?>
            <tr>
            <td colspan=4>

              <table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0">
                   <tr>
                     <td >  
                        <?php
                    if ($mensagem == '')
                    {
                         echo "<div class=\"Mensagem_Amarelo\">"; 
                         echo "<p class=\"texto1\">\n";     
                         echo "<table cellspacing=\"0\" cellpadding=\"0\">";
                         echo "  <tr>";
                         echo "    <td width=\"50\">";
                         echo "      <img src=\"imagens/icones/warning.png\" alt=\"\" />";
                         echo "    </td>";
                         echo "    <td valign=\"center\">";
                         echo A_LANG_REP_SCH_NO_REG;
                         echo "    </td>";
                         echo "  </tr>";
                         echo "</table>";  
                         echo "</p>\n";   
                         echo "</div>";   
                    }     
                         ?>    
                       
                       </td>
                     </tr>       
             </table> 
             <br> 
              </td>
              </tr>        
                  <?}



                }
			}
			?>
</table><br>
              </td>
            </tr>
          </table>


