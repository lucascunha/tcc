<?php
// ini_set('display_errors', 1); error_reporting(-1);
ob_start();

$opcao = $_GET['opcao'];
$var = $_GET['var'];

session_start();
$logado 		=	$_SESSION['logado'];
$id_usuario		=	$_SESSION['id_usuario'];
$email_usuario 	=	$_SESSION['email_usuario'];
$A_LANG_IDIOMA_USER=$_SESSION['A_LANG_IDIOMA_USER'];
$tipo_usuario	=	$_SESSION['tipo_usuario'];
$status_usuario	=	$_SESSION['status_usuario'];
$id_aluno		=	$_SESSION['id_aluno'];
$tipo   		=	$_SESSION['tipo'];

// inclui arquivo de configurações
include "config/configuracoes.php";

// inclui arquivo de funções
include "include/funcoes.php";

global $logado, $id_usuario, $email_usuario, $A_LANG_IDIOMA_USER, $tipo_usuario;

// Troca de idioma - idioma de configuração padrão ou idioma selecionado pelo usuário no momento do cadastro
$newlang = $_GET['newlang'];
if ($newlang != '') {
	// $A_LANG_IDIOMA_USER=trocalang($newlang, $id_usuario);
	$_SESSION['A_LANG_IDIOMA_USER'] = $newlang;
	$conn = &ADONewConnection($A_DB_TYPE);
	$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	$sql = 'UPDATE usuario
					SET idioma = "'. $newlang .'"
					WHERE id_usuario='.$id_usuario;
	$conn->Execute($sql);
	include "idioma/".$newlang."/geral.php";
} else {
	if ($A_LANG_IDIOMA_USER == "") {
		include "idioma/".$A_LANG_IDIOMA."/geral.php";
 	}else  {
    		include "idioma/".$A_LANG_IDIOMA_USER."/geral.php";
	}
}



header ("Content-type: text/html; Charset=".A_LANG_CHACTERSET."\"");

 ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<META http-equiv=Content-Type content="text/html; charset=<? echo A_LANG_CHACTERSET; ?>">
<META http-equiv=expires content="0">
<META http-equiv=Content-Language content="<? echo A_LANG_CODE; ?>">
<META name=KEYWORDS content="Adptação, Ensino, Distância">
<META name=DESCRIPTION content="Modulo de Adaptabilidade do Sistema AdaptWeb">
<META name=ROBOTS content="INDEX,FOLLOW">
<META name=resource-type content="document">
<META name=revisit-after content="1 days">
<META name=distribution content="Global">
<META name=GENERATOR content="Windows NotePade">
<META name=rating content="General">
<META name=MS.LOCALE content="<?php echo A_LANG_CODE; ?>">


<?php
 global $opcao, $ctx;
 if (is_null($opcao))
 {
  $opcao="Principal";
 // Contexto(A_LANG_SYSTEM,0);
 }

switch ($opcao)
{
	case "ToDo":
	      // Contexto(A_LANG_MNU_TODO,1);
	       $inc = "a_em_desenvolvimento.php";
	       break;
	case "Autoria":
	      // Contexto(A_LANG_MNU_AUTHORING,1);
	       $inc = "p_acesso_ambiente_autoria.php";
	       break;
	case "Navegacao":
		//Contexto(A_LANG_MNU_NAVIGATION,1);
		$destino="Location:n_index_navegacao.php?opcao=Apresentacao=";
		header($destino);
		break;
	case "Projeto":
		//Contexto(A_LANG_MNU_PROJECT,1);
		$inc = "p_entrada_projeto.php";
		break;
	case "Demo":
		//Contexto(A_LANG_MNU_DEMO,1);
		$inc = "p_entrada_demo.php";
		break;
	case "SolicitaAcesso":
			//Contexto(A_LANG_MNU_NEW_USER,1);
		$inc = "p_solicitacaoacesso.php";
		break;
	case "AutorizarAcessoAutor":
		//Contexto(A_LANG_MNU_RELEASE_AUTHORING,1);
		$inc = "p_autorizar_professor.php";
		break;

	/*QUESTIONÁRIO -19/06/2007*/
	case "QuestionarioResult":
		//Contexto(QS_ROOT,1);
		$inc = "questionario/mostra_resultados.php";
		break;
	case "Login":
		//Contexto(A_LANG_MNU_LOGIN,1);
		$inc = "p_login.php";
		break;

	case "EntradaInstituicoes":
		//Contexto(A_LANG_MNU_ORGANS,1);
		$inc = "p_entrada_Instituicoes.php";
		break;
	case "Apresentacao":
		//Contexto(A_LANG_MNU_ABOUT,1);
		$inc = "p_entrada_apresentacao.php";
		break;
	case "PerguntasFrequentes":
		$inc = "p_perguntas_frequentes.php";
		break;
	case "GraficoAcessoDisciplina":
		$inc = "p_gdLogins.php";
		break;
   	case "AcessoAgenda":
    	//Contexto(A_LANG_MNU_AGENDA,1);
    ?>
		<script type="text/javascript">
		<!--
		window.location = "n_index_agenda.php?opcao=AcessoAgenda"
		//-->
		</script>
    	<?php
		break;
	default:
		//Contexto(A_LANG_MNU_HOME,1);
		$inc = "p_entrada_apresentacao.php";
		break;
} // switch

?>

<title><?php echo $opcao." - AdaptWeb" ?></title>

<LINK REL="StyleSheet" HREF="css/geral.css" TYPE="text/css">
<link href="avaliacao/css/ff.css" rel="stylesheet" type="text/css" media="screen" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.1/themes/blitzer/jquery-ui.css" type="text/css" />
<script type="text/javascript" src="src/jquery.floatingmessage.js"></script>
<script language="JavaScript" type="text/JavaScript" src="javascript/ajaxlib.js" ></script>
<script src="javascript/jquery.easy-confirm-dialog.js"></script>
<script type="text/javascript" src="javascript/jquery_notification_v.1.js"></script>
<link href="javascript/jquery_notification.css" type="text/css" rel="stylesheet"/>
<link rel="stylesheet" href="javascript/colorbox.css" />
<script src="javascript/jquery.colorbox.js"></script>
<link href="javascript/tiptip/tipTip.css" rel="stylesheet">
<script src="javascript/tiptip/jquery.tipTip.js"></script>
<script src="javascript/tiptip/jquery.tipTip.minified.js"></script>



<script>
$(function(){
$(".tooltip").tipTip({maxWidth: "auto", defaultPosition:"right",  edgeOffset: 10, delay: 100});
});
</script>

<style type="text/css">

    .scrollup{
      width:40px;
      height:40px;
      text-indent:-9999px;
      opacity:0.5;
      position:fixed;
      bottom:50px;
      right:50px;
      display:none;
      background: url('imagens/icon_top.png') no-repeat;
    }
</style>


   <script type="text/javascript">
      $(document).ready(function(){

      $(window).scroll(function(){
        if ($(this).scrollTop() > 50) {
          $('.scrollup').fadeIn();
        } else {
          $('.scrollup').fadeOut();
        }
      });

      $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 100);
        return false;
      });


    });
    </script>
</head>

<body>


<?php include 'cabecalho.php'; ?>


<TABLE WIDTH="100%" BORDER="0" CELLSPACING="0" CELLPADDING="0" >

	<TR>
		<!-- opções do menu  -->

		<TD WIDTH="155" HEIGHT="430" valign="top" bgcolor=<? echo $A_COR_FUNDO_MENU ?>>

			<?php menuEntrada($opcao) ?>

		</TD>

		<!-- área de conteúdo -->
		<TD valign="top" >

			<table WIDTH="100%" height="100%" valign="top" CELLPADDING=5 CELLSPACING=5 BORDER=0>
				<tr>
					<td>

						<?php

							include $inc;


					      	?>

					</td>
				</tr>
			</table>

		</td>
	</tr>
</table>



<?php
include 'rodape.php';
?>

</body>

</html>




<a href="#" class="scrollup">Para Cima</a>

<!--
<script type="text/javascript">
    $(document).ready(function() {
            $("body").css("display", "none");
            $("body").fadeIn(200);
    });
</script>
-->
<div id="dvLoading"></div>
<script>
$(window).load(function(){
  $('#dvLoading').fadeOut(200);
});
</script>

