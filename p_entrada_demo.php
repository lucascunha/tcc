<?
/** -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Informática  
 *       UEL - Departamento de Computação
 * -----------------------------------------------------------------------
 *         @name p_entrada_demo.php
 *     @abstract Demonstração do AdaptWeb
 *        @since 25/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *      @package AdaptWeb  
 * -----------------------------------------------------------------------
 */ 
?>

						
<? 
  // cria matriz de orelhas
  $orelha = array(
  		array(   
   		   "LABEL" => A_LANG_SYSTEM , 
     		   "LINK" => "index.php?opcao=Apresentacao", 
     		   "ESTADO" =>"OFF"
   		   ), 		   
  		array(   
   		   "LABEL" => "O Projeto" , 
     		   "LINK" => "index.php?opcao=EntradaInstituicoes", 
     		   "ESTADO" =>"OFF"
   		   ),   		 

  		array(   
   		   "LABEL" => "Demonstração" , 
     		   "LINK" => "index.php?opcao=Demo", 
     		   "ESTADO" =>"ON"
   		   ),   
  	//	array(   
   	//	   "LABEL" => A_LANG_RESEARCHES , 
    // 		   "LINK" => "index.php?opcao=EntradaPesquisadores", 
    // 		   "ESTADO" =>"OFF"
   //		   ), 
  	//	array(   
  	//	   "LABEL" => A_LANG_AUTHORS , 
    //		   "LINK" => "index.php?opcao=EntradaAutores", 
     //		   "ESTADO" =>"OFF"
   	//	   )    		       		   		   
   		  ); 

  MontaOrelha($orelha);  

?>

<table CELLSPACING=5 CELLPADDING=5 border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?>  height="80%" style="height:380px;" >
	<tr valign="top">
		<td>
	<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; height:340px;" class="tabela_redonda" >
	<tr>
		<td valign=top>			
				<div class="bola" style='width:98%; margin:0 0 0 10px;'>
      			<h2 style="padding-top:10px; padding-left:15px;">
			 	   <?
				     echo A_LANG_SYSTEM_NAME_DESC;
				   ?> 	
  	      		</h2>
  	      		</div>
				<br>		
				<table border="0" width= "98%" style='margin:0 0 0 10px;'>


					<tr>
					<td>	
				
					<P align="justify">  
					   							   							     
					 <!--    <? echo A_LANG_INDEX_DEMO_AENVIRONMENT; ?>
					     <br>
					     <a href="#">Autoria (2,2 Mb)</a> - <? echo A_LANG_INDEX_DEMO_FDESC1 ?>
					
					     <br>
					     <br>	
					     <? ECHO A_LANG_INDEX_DEMO_NENVIRNMENT ?>
					     <br>
					     <a href="#">Modo Tutorial (3,1 Mb)</a> - <? echo A_LANG_INDEX_DEMO_FDESC2;?>
					     
					     <br>
					     <br>
					     <br> -->
					     <? //echo A_LANG_INDEX_DEMO_TOLOG; ?> 
					     <? echo "Acesse o Adaptweb e navegue pelo ambiente de demonstração, efetuando login com os dados abaixo: "; ?><br><br>
					     <? //echo A_LANG_INDEX_DEMO_LOGIN; ?>
					     <? echo "<b>E-mail:</b> demo@inf.ufrgs.br"; ?>
					     <br>
					     <? //echo A_LANG_INDEX_DEMO_PASS; ?>
					     <? echo "<b>Senha:</b> demo123"; ?>
						<!--
					     <br> 			
					     <br>
					     					     				     
					     <b><? echo A_LANG_INDEX_DEMO_OBSERVATION; ?>:</b> <? echo A_LANG_INDEX_DEMO_OBSERVATIONTEXT; ?>
					 	-->
					 	<br> 			
					    <br>
					     					     				     
					     <b><? echo A_LANG_INDEX_DEMO_OBSERVATION; ?>:</b> Algumas funcionalidades estarão desabilitadas.
					</p>
					

				</td>
			</tr>
			
			</table>

		</td>
	</tr>
</table>
		</td>
	</tr>
</table>

