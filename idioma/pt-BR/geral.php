<? 
/* -----------------------------------------------------------------------
*  AdaptWeb - Projeto de Pesquisa       
*     UFRGS - Instituto de Inform�tica  
*       UEL - Departamento de Computa��o
* -----------------------------------------------------------------------
*       @package AdaptWeb
*     @subpakage Linguagem
*          @file idioma/pt-BR/geral.php
*    @desciption Arquivo de tradu��o - Portugu�s/Brasil
*         @since 17/08/2003
*        @author Veronice de Freitas (veronice@jr.eti.br)
*   @Translation Veronice de Freitas
* -----------------------------------------------------------------------         
*/  
 
       
  // ************************************************************************
//Dados da interface da ferramenta de autoria
  define("A_LANG_VERSION","Vers�o"); 
  define("A_LANG_SYSTEM","Ambiente AdaptWeb"); 
  define("A_LANG_ATHORING","Ferramenta de Autoria"); 
  define("A_LANG_USER","Usu�rio");  
  define("A_LANG_PASS","Senha");  
  define("A_LANG_NOTAUTH","< n�o autenticado>"); 
  define("A_LANG_HELP","Ajuda"); 
  define("A_LANG_DISCIPLINE","Disciplina");  
define("A_LANG_NAVEGATION","Onde estou: ");   

// Contexto
// observa��o - arquivo p_contexto_navegacao.php
       // ************************************************************************ // * Menu                                                                                 *
  // ************************************************************************
// Home
define("A_LANG_MNU_HOME","In�cio"); 
define("A_LANG_MNU_NAVIGATION","Ambiente Aluno");
define("A_LANG_MNU_AUTHORING","Ambiente Professor"); 
define("A_LANG_MNU_AGENDA","Agenda"); //Altera��o Agenda - Autora: Carina Tissa Aihara
define("A_LANG_MNU_UPDATE_USER","Consultar Cadastro"); //Alterar Cadastro
define("A_LANG_MNU_EXIT","Sair"); 
define("A_LANG_MNU_LOGIN","Login"); 
define("A_LANG_MNU_ABOUT","Sobre");
define("A_LANG_MNU_DEMO","Demonstra��o");
define("A_LANG_MNU_PROJECT","Projeto");
define("A_LANG_MNU_FAQ","Perguntas Freq�entes"); 
define("A_LANG_MNU_RELEASE_AUTHORING","Liberar Autoria"); 
define("A_LANG_MNU_BACKUP","C�pia de Seguran�a"); 
define("A_LANG_MNU_NEW_USER","Novo Usu�rio"); 
define("A_LANG_MNU_FELDER","Question�rio de Felder"); 
define("A_LANG_MNU_APRESENTATION","Apresenta��o");
define("A_LANG_MNU_MY_ACCOUNT","Minha Conta");
define("A_LANG_MNU_MANAGE","Gerenciamento"); 
define("A_LANG_MNU_BAKCUP","C�pia de Seguran�a");
define("A_LANG_MNU_GRAPH","Acesso");
define("A_LANG_MNU_TODO","Em desenvolvimento"); 
// Alterado por Carla-estagio UDESC 2008/02 define("A_LANG_SYSTEM_NAME","AdaptWeb - Ambiente de Ensino-Aprendizagem Adaptativo na Web ");  
define("A_LANG_SYSTEM_NAME","AdaptWeb");

define("A_LANG_SYSTEM_COPYRIGHT","Licensed under the GNU GENERAL PUBLIC LICENSE Version 2"); 
        // Autoria        
        define("A_LANG_MNU_COURSE","Curso");  
        define("A_LANG_MNU_DISCIPLINES","Disciplina");         
        define("A_LANG_MNU_DISCIPLINES_COURSE","Disciplina &harr; Curso");                        
        define("A_LANG_MNU_STRUTURALIZE_TOPICS","Estruturar Conte�do"); 
        define("A_LANG_MNU_ACCESS_LIBERATE","Pedidos de Matr�cula");
        define("A_LANG_MNU_LIBERATE_DISCIPLINES","Liberar Disciplina");
        define("A_LANG_MNU_WORKS","Trabalhos dos alunos"); 
     	 define("A_LANG_MNU_LOG","An�lise de Log");  
	 define("QUESTIONARIO","Question�rio"); 
	 define("QS_ROOT","Resultados QS");   
      

// Estudante
define("A_LANG_MNU_DISCIPLINES_RELEASED","Assistir Disciplinas"); define("A_LANG_MNU_SUBSCRIBE","Solicitar Matr�cula"); define("A_LANG_MNU_WAIT","Aguardando Matr�cula"); 
define("A_LANG_MNU_UPLOAD_FILES","Enviar Trabalhos"); define("A_LANG_MNU_MESSAGE","Quadro de Avisos"); 
// ************************************************************************ // * Formul�rio Principal                                                                          *
  // ************************************************************************ 
  // Apresenta��es
define("A_LANG_ORGANS","Institui��es Participantes");
define("A_LANG_RESEARCHES","Pesquisadores");
define("A_LANG_AUTHORS","Autores"); 

 
  // ************************************************************************ // * Formul�rio - Autoria                                                                     *
  // ************************************************************************
// Apresenta��o
define("A_LANG_MNU_ORGANS",""); 
define("A_LANG_MNU_RESEARCHES",""); 
define("A_LANG_MNU_AUTHORS",""); 

        // Formul�rio de solicita��o de acesso
		
		//2011
		define("A_LANG_REQUEST_ACCESS_SOBRENOME","Sobrenome");
		define("A_LANG_REQUEST_ACCESS_DATA_NASC","Data de Nascimento");
		define("A_LANG_REQUEST_ACCESS_GENERO","Sexo");
		define("A_LANG_REQUEST_ACCESS_FEM","Feminino");
		define("A_LANG_REQUEST_ACCESS_MASC","Masculino");
		define("A_LANG_REQUEST_ACCESS_NACIONALIDADE","Nacionalidade");
		define("A_LANG_REQUEST_ACCESS_LINGUA","L�ngua Materna");
		
		define("A_LANG_REQUEST_ACCESS_LINGUA_ALEMAO","Alem�o");
		define("A_LANG_REQUEST_ACCESS_LINGUA_ARABE","�rabe");
		define("A_LANG_REQUEST_ACCESS_LINGUA_ESPANHOL","Espanhol");
		define("A_LANG_REQUEST_ACCESS_LINGUA_FRANCES","Franc�s");
		define("A_LANG_REQUEST_ACCESS_LINGUA_INGLES","Ingl�s");
		define("A_LANG_REQUEST_ACCESS_LINGUA_ITALIANO","Italiano");
		define("A_LANG_REQUEST_ACCESS_LINGUA_JAPONES","Japon�s");
		define("A_LANG_REQUEST_ACCESS_LINGUA_MANDARIM","Mandarim");
		define("A_LANG_REQUEST_ACCESS_LINGUA_PORTUGUES","Portugu�s");
		
		define("A_LANG_REQUEST_ACCESS_OUTROS_IDIOMAS","Outros Idiomas");
		
		define("A_LANG_REQUEST_ACCESS_OUTRO_PAIS","J� morou em outro pa�s?");
		define("A_LANG_REQUEST_ACCESS_LOCAL_ENSINO_FUNDAMENTAL","Pa�s do ensino Fundamental");
		define("A_LANG_REQUEST_ACCESS_LOCAL_ENSINO_MEDIO","Pa�s do Ensino M�dio");
		define("A_LANG_REQUEST_ACCESS_LOCAL_ENSINO_SUPERIOR","Pa�s do Ensino Superior");
		
		define("A_LANG_REQUEST_ACCESS_NOME_UNIVERSIDADE","Nome da Universidade");
		define("A_LANG_REQUEST_ACCESS_NOME_CURSO","Nome do curso/forma��o");
		define("A_LANG_REQUEST_ACCESS_ANO_UNIVERSIDADE","Ano de ingresso no curso");
		
		
		define("A_LANG_REQUEST_ACCESS_DISPOSITIVOS","Quais dispositivos utilizo");
		define("A_LANG_REQUEST_ACCESS_FERRAMENTAS_COMP","Costumo utilizar ferramentas <br>computacionais para");
		define("A_LANG_REQUEST_ACCESS_UTILIZA_OUTRO_AMBIENTE","Utiliza algum ambiente? <br>(pode selecionar mais de 1)");
		
		
		
		
		
		
		
        define("A_LANG_REQUEST_ACCESS","Solicita��o de Acesso");
        define("A_LANG_REQUEST_ACCESS_USER_TYPE","Tipo de Usu�rio *");
        define("A_LANG_REQUEST_ACCESS_TYPE_STUDANT","Aluno");
        define("A_LANG_REQUEST_ACCESS_TYPE_TEACHES","Professor");                
        define("A_LANG_REQUEST_ACCESS_NAME","Nome Completo *");
        define("A_LANG_REQUEST_ACCESS_EMAIL","E-mail *");
        define("A_LANG_REQUEST_ACCESS_PASS","Senha *");          
        define("A_LANG_REQUEST_ACCESS_PASS2","Confirma��o da Senha *");     
        define("A_LANG_REQUEST_ACCESS_INSTITUTION_EDUCATION","Institui��o de Ensino");     
        define("A_LANG_REQUEST_ACCESS_COMMENT","Observa��o");     
        define("A_LANG_REQUEST_ACCESS_LANGUAGE","Idioma");               
        define("A_LANG_REQUEST_ACCESS_MSG1","N�o foi poss�vel estabelecer conex�o com o banco de dados");     
		define("A_LANG_REQUEST_ACCESS_MSG2","Este e-mail j� est� cadastrado");     
        define("A_LANG_REQUEST_ACCESS_MSG3","N�o foi poss�vel enviar seus dados");     
        define("A_LANG_REQUEST_ACCESS_MSG4","Senha inv�lida (Informe a mesma senha nos campos SENHA e CONFIRMA��O DA SENHA)");     
        define("A_LANG_REQUEST_ACCESS_SAVE","Gravar");  
	// Acrescenta mensagem para este formulario - Carla -est�gio UDESC 2008/02
	define("A_LANG_REQUEST_ACCESS_MSG5","Os campos que possuem * devem ser preenchidos"); 
	define("A_LANG_REQUEST_ACCESS_MSG6","O campo E-mail deve ser Preenchido");
	define("A_LANG_REQUEST_ACCESS_MSG7","O campo Nome deve ser preenchido");
	define("A_LANG_REQUEST_ACCESS_MSG8","Os campo Senha foi preenchido com valor inv�lido");
	define("A_LANG_REQUEST_ACCESS_MSG9","Os campos Senha e Confirma��o de Senha devem ser preenchidos com os mesmos dados");       
	define("A_LANG_REQUEST_ACCESS_MSG10","Seu cadastro foi efetuado com sucesso");
	define("A_LANG_REQUEST_ACCESS_MSG100","Question�rio atualizado com sucesso");
	define("A_LANG_REQUEST_ACESS_OBSERVACAO", "Os campos que possuem * s�o de preenchimento obrigat�rio."); 
	// Fim das alteracoes Carla


        //Formulario de login
        define("A_LANG_LOGIN2","Login");
        define("A_LANG_EMAIL","E-mail");
define("A_LANG_LOGIN_PASS","Senha");      
define("A_LANG_LOGIN_START","Entrar");      
define("A_LANG_LOGIN_MSG1","Senha inv�lida");       
        define("A_LANG_LOGIN_MSG2","Email inv�lido");       
        define("A_LANG_LOGIN_MSG3","Usu�rio n�o autorizado");       
        define("A_LANG_LOGIN_MSG4","Aguarde a libera��o de seu acesso pelo administrador do ambiente AdaptWeb");       
        define("A_LANG_LOGIN_MSG5","N�o foi poss�vel estabelecer conex�o com o banco de dados");       
                

        // Formul�rio de cadastro de Curso
        define("A_LANG_COURSE_REGISTER","Cadastrar Curso");       
        define("A_LANG_COURSE_RESGISTERED","Cursos j� cadastrados");  
        define("A_LANG_COURSE2","Curso para cadastrar");    
        define("A_LANG_COURSE22","Curso para alterar/excluir");                   
        define("A_LANG_COURSE_INSERT","Cadastrar");
        define("A_LANG_COURSE_UPDATE","Alterar");
        define("A_LANG_COURSE_DELETE","Excluir");  
        define("A_LANG_COURSE_MSG1","N�o foi poss�vel estabelecer conex�o com o banco de dados");  
	 define("A_LANG_COURSE_MSG2","N�o foi poss�vel inserir o curso");  
	 define("A_LANG_COURSE_MSG3","N�o foi poss�vel alterar o curso"); 
	 define("A_LANG_COURSE_MSG4","N�o � poss�vel excluir o curso. Para exclu�-lo � necess�rio desvincular com todas as disciplinas."); 
	 define("A_LANG_COURSE_MSG5","N�o foi poss�vel excluir o curso"); 
	 // Incllus�o de mensagens para usabilidade - Carla -estagio UDESC-2008/02
	 define("A_LANG_COURSE_MSG6", "Curso cadastrado com Sucesso");
	 define("A_LANG_COURSE_MSG7","Curso exclu�do com Sucesso");
	// Fim alteracoes Carla
 

        // Cadastro de disciplina
        define("A_LANG_DISCIPLINES_REGISTER","Cadastrar Disciplina");  
        define("A_LANG_DISCIPLINES_REGISTERED","Disciplinas j� cadastradas"); 
        define("A_LANG_DISCIPLINES2","Disciplina para cadastrar");    
        define("A_LANG_DISCIPLINES22","Disciplina para alterar/excluir");    
        define("A_LANG_DISCIPLINES_INSERT","Cadastrar");
        define("A_LANG_DISCIPLINES_UPDATE","Alterar");
        define("A_LANG_DISCIPLINES_DELETE","Excluir");      
        define("A_LANG_DISCIPLINES_MSG1","N�o foi poss�vel inserir a disciplina"); 
        define("A_LANG_DISCIPLINES_MSG2","N�o foi poss�vel alterar a disciplina"); 
        define("A_LANG_DISCIPLINES_MSG3","N�o � poss�vel excluir essa disciplina. Para exclu�-la � necess�rio desvincular com todos os cursos."); 
        define("A_LANG_DISCIPLINES_MSG4","N�o foi poss�vel excluir a disciplina"); 
        // Inclus�o de mensagens para usabilidade - Carla est�gio UDESC 2008/02
	 define("A_LANG_DISCIPLINES_MSG5","J� existe uma disciplina com este nome");
	 define("A_LANG_DISCIPLINES_MSG6","Uma disciplina n�o pode ter o nome em branco");
	 define("A_LANG_DISCIPLINES_MSG7","Para alterar um curso � preciso selecion�-lo e incerir um novo nome");
	 define("A_LANG_DISCIPLINES_MSG8","a disciplina cadastrada com Sucesso");
	 // Final Carla


        // Formul�rio de cadastro de Disciplina / Curso
        define("A_LANG_DISCIPLINES_COURSE1","Disciplina/Curso");
        define("A_LANG_DISCIPLINES_COURSE_DISCIPLINES","Disciplinas cadastradas");   
        define("A_LANG_DISCIPLINES_COURSE2","Vincular disciplina com os cursos");      
        define("A_LANG_DISCIPLINES_COURSE_SAVE","Gravar");
        define("A_LANG_DISCIPLINES_COURSE_MSG1","Cadastre a(s) DISCIPLINA(S) e CURSO(S) para ter acesso a este item.");
        define("A_LANG_DISCIPLINES_COURSE_MSG2","N�o foi poss�vel estabelecer v�nculo da disciplina com os cursos devido a falha de conex�o com o banco de dados.");
        define("A_LANG_DISCIPLINES_COURSE_MSG3","Informe o nome do curso que deseja incluir");
        define("A_LANG_DISCIPLINES_COURSE_MSG4","Curso existente");
        define("A_LANG_DISCIPLINES_COURSE_MSG5","Selecione um curso para altera��o");
        // Inclus�o de mensagens para usabilidade - Carla est�gio UDESC 2008/02
	 define("A_LANG_DISCIPLINES_COURSE_MSG6","O Relacionamento entre Disciplina e Curso foi realizado com Sucesso.");
	 define("A_LANG_DISCIPLINES_COURSE_MSG7","N�o foi selecionado nenhum Curso para esta Disciplina");       
	 define("A_LANG_DISCIPLINES_COURSE_MSG8","Curso alterado com sucesso");
	 define("A_LANG_DISCIPLINES_COURSE_OBS","*Observa��o: Para criar a estrutura do conte�do da disciplina � preciso relacion�-la com pelo menos um curso");
	 //Final Carla

                
	// Formul�rio de autoriza��o de acesso
	 define("A_LANG_LIBERATE_USER1","Matr�cula / Aluno");   // para o professor e administrador
        define("A_LANG_LIBERATE_USER2","Autorizar professor / Autoria"); // para o adiministrador
        define("A_LANG_LIBERATE_MSG1","Em desenvolvimento ... ");
        
        
        // Formul�rio para libera��o da disciplina 
	 define("A_LANG_LIBERATE_DISCIPLINES","Liberar Disciplina");
        define("A_LANG_LIBERATE_DISCIPLINES_MSG1","Em desenvolvimento ... ");
	 // Inclus�o de mensagens para usabilidade - Carla est�gio UDESC 2008/02
	 define("A_LANG_TEXT_LIBERATE","Para liberar a disciplina criada � preciso selecion�-la na lista \"Liberar Disciplina\" e mover para a lista de \"Disciplinas Liberadas\".");
	 define("A_LANG_TEXT_LIBERATE_MAT","Para liberar a matr�cula solicitada pelo aluno � preciso selecion�-la na \"Lista de Alunos\" e mover para a \"Lista de Alunos Matriculados\".");//carla incluiu para liberar matricula de aluno
	 // Final Carla
                                                          
        // ======================= Topicos ==========================
                        
        // Entrada / t�picos
        define("A_LANG_ENTRANCE_TOPICS_DISCIPLINES","Disciplina");
        // **** REVER "Estruturar t�pico"
        define("A_LANG_ENTRANCE_TOPICS_STRUTURALIZE","Estruturar Conte�do da Disciplina");
        define("A_LANG_ENTRANCE_TOPICS_MSG1","N�o foi poss�vel ler as disciplinas");
        define("A_LANG_ENTRANCE_TOPICS_MSG2","Somente estar�o dispon�veis neste formul�rio as disciplinas que estiverem relacionadas com pelo menos um curso.");
        

       // Guia - Lista de t�picos
        define("A_LANG_TOPIC_GENERATE_CONTENT","GERAR CONTE�DO");       
        //orelha
        define("A_LANG_TOPIC_LIST","Conceitos");     
        define("A_LANG_TOPIC_MAINTENANCE","Manuten��o do Conceito");  
        define("A_LANG_TOPIC_EXEMPLES","Exemplos");  
        define("A_LANG_TOPIC_EXERCISES","Exerc�cios");  
        define("A_LANG_TOPIC_COMPLEMENTARY","Material Complementar");  
	  //Inclusao de mensagens de Usabilidade - Carla -estagio UDESC 2008/02
	   define("A_LANG_TOPIC_OBS","<b>*Observa��o:</b> Para <u>Gerar Conte�do</u> na p�gina inicial de conceitos � preciso preencher corretamente todos os campos desta p�gina.<br>
		O arquivo a ser carregado deve ser <b>.html</b>, se houver alguma imagem neste arquivo, tamb�m � preciso fazer o upload da imagem.  &nbsp;O <br>arquivo html pode ser substituido fazendo o upload de outro arquivo .html.  ");
			//Carla inseriu observa��o em pagina q cria topicos
	   define("A_LANG_TOPIC_ARQ_OBS","<b>*Observa��o:</b> Extens�o do arquivo html (ex.:capitulo1.html)");//Carla inseriu observa��o para tipo de arquivo
	   define("A_LANG_TOPIC_GRAVAR_MSG1","Dados gravados com sucesso. Caso seja feita alguma altera��o nesta p�gina � preciso gravar novamente.");
	 // Final Carla


        
        // Guia - manuten��o do t�pico
  	 define("A_LANG_TOPIC_NUMBER","N�mero do Conceito");
	 define("A_LANG_TOPIC_DESCRIPTION","Nome do Conceito");
	 define("A_LANG_TOPIC_SUMARIZED_TOPIC_DESCRIPTION","Descri��o Resumida do Conceito");  
	 define("A_LANG_TOPIC_WORDS_KEY","Palavras-Chave");   
  	 define("A_LANG_TOPIC_PREREQUISIT","Pr�-requisito");
	 define("A_LANG_TOPIC_COURSE","Curso");
	 define("A_LANG_TOPIC_FILE1","Arquivo");
	 define("A_LANG_TOPIC_FILE2","Arquivo(s)");
	 define("A_LANG_TOPIC_MAIN_FILE","Arquivo principal"); 
	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE","Arquivo(s) associado(s)");
	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_STATUS","Situa��o");
	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_UPLOAD",""); 
        define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG1","carregar para o servidor"); 
  	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG2","carregado para o servidor"); 

       // bot�es
        define("A_LANG_TOPIC_SAVE","Gravar");  
        define("A_LANG_TOPIC_SEND","Enviar"); 
        define("A_LANG_TOPIC_SEARCH","Procurar");         
	 define("A_LANG_TOPIC_INSERT_TOPIC_SAME_LEVEL","Inserir conceito no mesmo n�vel");  
        define("A_LANG_TOPIC_INSERT_SUB_TOPIC","Inserir subconceito");  
	 define("A_LANG_TOPIC_DELETE","Excluir");  
	 define("A_LANG_TOPIC_PROMOTE","Promover"); 
	 define("A_LANG_TOPIC_LOWER","Rebaixar"); 
	 // Inclusao de mesanges de Usabilidade - Carla - estagio UDESC 2008/02
	 define("A_LANG_TOPIC_INSERT_MSG1","Preencha todos os campos");//carla inseriu para colocar mensagem para usuario ao criar um topico
	 define("A_LANG_TOPIC_INSERT_MSG2","Preencha o campo Nome");//carla inseriu para colocar mensagem para usuario ao criar um topico
	 define("A_LANG_TOPIC_INSERT_MSG3","Preencha o campo Descri��o");//carla inseriu para colocar mensagem para usuario ao criar um topico
	 define("A_LANG_TOPIC_INSERT_MSG4","Preencha o Campo Palavra-Chave");//carla inseriu para colocar mensagem para usuario ao criar um t�pico
	 define("A_LANG_TOPIC_OBS2","<b>*Observa��o:</b> Todos os campos devem ser preenchidos.");
	// Final Carla


	// Guia - samples
	//define("A_LANG_TOPIC_EXEMPLES_NUMBER_TOPIC","N�mero do T�pico");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Descri��o do exemplo");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","N�vel de Complexidade");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Curso");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Excluir");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Descri��o");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","N�vel");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Curso");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Arquivo");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Arquivo(s) associado(s)");
	// bot�es
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Gravar");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Excluir");


  // Exemplos
  // N�vel de Complexidade
// Descri��o
// N�vel
// Arquivos Associados
        // Descri��o dos Exemplos
  // Exerc�cios
        // Descri��o do exerc�cio
  // Material complementar
        // Menu
// Autorizar Acesso
// Liberar disciplinas
// Novo usu�rio
        // to modify I register in cadastre (alterar cadastro)
        // ************************************************************************ // * Formul�rio - Autoria
  // ************************************************************************ // Disciplinas Liberadas

// Matricula
	// Inclus�o de mesagens de usabilidade (para matricula) Carla - estagio UDESC 2008/02
	   define("A_LANG_LENVIRONMENT_REQUEST_SUCCESS","Solicita��o realizada com sucesso. Aguarde a libera��o da sua matr�cula pelo professor");
	// Final Carla

// Aguardando Matricula
       // Inclus�o de mesagens de usabilidade (para aguardando matricula) Carla - estagio UDESC 2008/02
	   define("A_LANG_NO_DISC_RELEASED","N�o h� disciplinas liberadas.");  
	// Final Carla


        // Autorizar Acesso
        
        // Navega��o
               
        // Tela de Cursos    
        // ************************************************************************ // * Formul�rio - Demo
  // ************************************************************************
  define("A_LANG_DEMO","Modo de demonstra��o do Ambiente AdaptWeb. Algumas funcionalidades est�o desabilitadas.");
//
// Regional Specific Date texts
//
// A little help for date manipulation, from PHP manual on function strftime():
//
// %a - abbreviated weekday name according to the current locale
// %A - full weekday name according to the current locale
// %b - abbreviated month name according to the current locale
// %B - full month name according to the current locale
// %c - preferred date and time representation for the current locale
// %C - century number (the year divided by 100 and truncated to an integer, range 00 to 99)
// %d - day of the month as a decimal number (range 01 to 31)
// %D - same as %m/%d/%y
// %e - day of the month as a decimal number, a single digit is preceded by a space
//      (range ' 1' to '31')
// %h - same as %b
// %H - hour as a decimal number using a 24-hour clock (range 00 to 23)
// %I - hour as a decimal number using a 12-hour clock (range 01 to 12)
// %j - day of the year as a decimal number (range 001 to 366)
// %m - month as a decimal number (range 01 to 12)
// %M - minute as a decimal number
// %n - newline character
// %p - either `am' or `pm' according to the given time value, or the corresponding strings for
//      the current locale
// %r - time in a.m. and p.m. notation
// %R - time in 24 hour notation
// %S - second as a decimal number
// %t - tab character
// %T - current time, equal to %H:%M:%S
// %u - weekday as a decimal number [1,7], with 1 representing Monday
// %U - week number of the current year as a decimal number, starting with the first Sunday as
//      the first day of the first week
// %V - The ISO 8601:1988 week number of the current year as a decimal number, range 01 to 53,
//      where week 1 is the first week that has at least 4 days in the current year, and with
//      Monday as the first day of the week.
// %W - week number of the current year as a decimal number, starting with the first Monday as
//      the first day of the first week
// %w - day of the week as a decimal, Sunday being 0
// %x - preferred date representation for the current locale without the time
// %X - preferred time representation for the current locale without the date
// %y - year as a decimal number without a century (range 00 to 99)
// %Y - year as a decimal number including the century
// %Z - time zone or name or abbreviation
// %% - a literal `%' character
//
// Note: A_LANG_DATESTRING is used for Articles and Comments Date
//       A_LANG_LINKSDATESTRING is used for Web Links creation Date
//       A_LANG_DATESTRING2 is used for Older Articles box on Home
//
  
define("A_LANG_CODE","pt-BR");
define("A_LANG_CHACTERSET","ISO-8859-1");
//define("A_LANG_CHACTERSET","UTF-8");


define("A_LANG_NAME_pt_BR","Portugu�s do Brasil");  
define("A_LANG_NAME_en_US","Ingl�s");  
define("A_LANG_NAME_es_ES","Espanhol");  
define("A_LANG_NAME_fr_FR","Franc�s");  
define("A_LANG_NAME_ja_JP","Japon�s");

define('A_LANG_TIMEZONES','IDLW NT HST YST PST MST CST EST AST GMT-3:30 GMT-3 AT WAT GMT CET EET BT GMT+3:30 GMT+4 GMT+4:30 GMT+5 GMT+5:30 GMT+6 WAST CCT JST ACS GST GMT+11 NZST'); 
define('A_LANG_TZOFFSETS','0 1 2 3 4 5 6 7 8 8.5 9 10 11 12 13 14 15 15.5 16 16.5 17 17.5 18 19 20 21 21.5 22 23 24'); 
define('A_LANG_DATEBRIEF','%b %d, %Y');
define('A_LANG_DATELONG','%A, %B %d, %Y'); 
define('A_LANG_DATESTRING','%A, %B %d @ %H:%M:%S %Z'); 

define('A_LANG_DATESTRING2','%A, %B %d');
define('A_LANG_DATETIMEBRIEF','%b %d, %Y - %I:%M %p');
define('A_LANG_DATETIMELONG','%A, %B %d, %Y - %I:%M %p %Z'); 

define('A_LANG_LINKSDATESTRING','%d-%b-%Y');
define('A_LANG_TIMEBRIEF','%I:%M %p');
define('A_LANG_TIMELONG','%I:%M %p %Z');

define('A_LANG_DAY_OF_WEEK_LONG','Domingo Segunda Ter�a Quarta Quinta Sexta S�bado');
define('A_LANG_DAY_OF_WEEK_SHORT','Dom Seg Ter Qua Qui Sex S�b');
define('A_LANG_MONTH_LONG','Janeiro Fevereiro Mar�o Abril Maio Junho Julho Agosto Setembro Outubro Novembro Dezembro');
define('A_LANG_MONTH_SHORT','Jan Fev Mar Abr Mai Jun Jul Ago Set Out Nov Dez');
  
//************UFRGS******************
	//Index
	  //About
	// alterado por Carladefine('A_LANG_INDEX_ABOUT_INTRO',' O Ambiente AdaptWeb � voltado para  a autoria e apresenta��o adaptativa de disciplinas integrantes de cursos EAD na Web. O objetivo do AdaptWeb � permitir a adequa��o de t�ticas e formas de apresenta��o de conte�dos para alunos de diferentes cursos de gradua��o e com diferentes estilos de aprendizagem, possibilitando diferentes formas de apresenta��o de cada conte�do, de forma adequada a cada curso e �s prefer�ncias individuais dos alunos participantes.<br> O Ambiente AdaptWeb foi inicialmente desenvolvido por pesquisadores da UFRGS e da UEL, atrav�s dos projetos Electra e AdaptWeb, com apoio do CNPq. Visite a p�gina do projeto: <a href=\'http://www.inf.ufrgs.br/adapt/adaptweb\' target=\'_blank\'>http://www.inf.ufrgs.br/adapt/adaptweb</a>');
	// Alteracao mensagem Carla estagio UDESC 2008/02
	define('A_LANG_INDEX_ABOUT_INTRO',' O Ambiente AdaptWeb � voltado para  a autoria e apresenta��o adaptativa de disciplinas integrantes de cursos EAD na Web. O objetivo do AdaptWeb � permitir a adequa��o de t�ticas e formas de apresenta��o de conte�dos para alunos de diferentes cursos de gradua��o e com diferentes estilos de aprendizagem, possibilitando diferentes formas de apresenta��o de cada conte�do, de forma adequada a cada curso e �s prefer�ncias individuais dos alunos participantes.<br>'); 
    define ('A_LANG_PROJETO_INTRO', ' O Ambiente AdaptWeb foi inicialmente desenvolvido por pesquisadores da UFRGS e da UEL, atrav�s dos projetos Electra e AdaptWeb, com apoio do CNPq. A partir de 2006, os pesquisadores da UDESC tamb�m passaram a contribuir com o desenvolvimento e melhorias no AdaptWeb. ');
	define('A_LANG_INDEX_ABOUT_ENVACCESS','Acesso ao ambiente');
	define('A_LANG_INDEX_ABOUT_TINFO','Para disponibilizar o conte�do de suas disciplinas o professor deve efetuar um cadastro de solicita��o de acesso no Ambiente de Autoria e aguardar libera��o do administrador');
	define('A_LANG_INDEX_ABOUT_LINFO','Para ter acesso a(s) disciplina(s) o aluno deve efetuar o cadastro no Ambiente de Navega��o e solicitar matr�cula na(s) disciplina(s) relacionadas ao seu curso');
	  //Demo
	define('A_LANG_INDEX_DEMO_AENVIRONMENT','Ambiente de Autoria');
	define('A_LANG_INDEX_DEMO_FDESC1', 'Criar disciplina(s)');
	define('A_LANG_INDEX_DEMO_NENVIRNMENT','Ambiente de navega��o');
	define('A_LANG_INDEX_DEMO_FDESC2', 'Assistir disciplina(s)');
	define('A_LANG_INDEX_DEMO_TOLOG','Acesse o ambiente e navegue pela disciplina de demonstra��o efetuando login no ambiente com os dados abaixo:');
	define('A_LANG_INDEX_DEMO_LOGIN','login: demo@inf.ufrgs.br');
	define('A_LANG_INDEX_DEMO_PASS', 'senha: 123');
	define('A_LANG_INDEX_DEMO_OBSERVATION','Observa��o');
	define('A_LANG_INDEX_DEMO_OBSERVATIONTEXT', 'ap�s efetuar o login ser� liberado acesso ao ambiente do professor e ao ambiente do aluno pelas op��es "Ambiente do professor" e "Ambiente o aluno" na tela inicial do AdaptWeb.');
	  //Apresenta��o
	define('A_LANG_INDEX_PRESENTATION_INFABOUT', 'Informa��es sobre o projeto:');
	define('A_LANG_DOWNLOAD', 'Download');
	define('A_LANG_PUBLICATIONS', 'Publica��es');
	define('A_LANG_INDEX_PRESENTATION_AVAILABLEAT','Dispon�vel em:');
	

	
	//Ambiente do Aluno - Entrada
	  //Principal
	define('A_LANG_LENVIRONMENT','Ambiente do aluno');
	define('A_LANG_LENVIRONMENT_DESCRIPTION', 'Este ambiente possibilita o aluno ter acesso ao material instrucional de seu(s) professor(es) adaptado ao seu perfil.');	
	define('A_LANG_LENVIRONMENT_WARNING', 'para ter acesso a(s) sua(s) disciplina, o aluno deve inicialmente efetuar seu cadastro para que seja poss�vel logar no ambiente para solicitar matr�cula de sua(s) disciplina(s). Somente ap�s a libera��o de acesso pelo professor respons�vel que o mesmo ter� acesso a disciplina.');
	
	define('A_LANG_LENVIRONMENT_WATCH_LIBERATED', 'Disciplinas liberadas');
	define('A_LANG_LENVIRONMENT_WATCH_MINE', 'Disciplinas de minha autoria (navegar por curso)');
	define('A_LANG_LENVIRONMENT_WATCH_MY', 'Minhas Disciplinas');
	define('A_LANG_LENVIRONMENT_WATCH_NOT', 'N�o h� disciplinas cadastradas');
	define('A_LANG_LENVIRONMENT_WATCH_WARNING', 'Solicite matr�cula para ter acesso �s disciplinas.');
	define('A_LANG_LENVIRONMENT_WATCH_VISUAL', 'Visualizar disciplina para cada curso');
	define('A_LANG_LENVIRONMENT_WATCH_CREATE', 'Crie as disciplinas e gere conte�do (Gerar Conte�do)no Ambiente do Professor (item <b>Estruturar Conte�do => Lista de Conceitos </b>) para ter acesso ao ambiente de navega��o.');
	define('A_LANG_LENVIRONMENT_WATCH_OBS', 'Para visualizar as disciplinas de sua autoria n�o � necess�rio Liberar Disciplina para testar o ambiente de navega��o.');
	define('A_LANG_LENVIRONMENT_REQUEST', 'Solicitar Matr�cula');
	define('A_LANG_LENVIRONMENT_REQUEST_REQ', 'Solicitar');
	define('A_LANG_LENVIRONMENT_REQUEST_NOTC', 'N�o h� curso cadastrado');
	define('A_LANG_LENVIRONMENT_REQUEST_NOTD', 'N�o h� disciplina cadastrada para o curso selecionado');
	define('A_LANG_LENVIRONMENT_REQUEST_SELECT', 'Selecione para solicitar matr�cula:');
	define('A_LANG_LENVIRONMENT_REQUEST_MATR', 'Matriculado');
	define('A_LANG_LENVIRONMENT_WAITING', 'Aguardando libera��o do professor');
	define('A_LANG_LENVIRONMENT_WAITING_NOT', 'N�o h� solicita��es de matr�culas');
	define('A_LANG_LENVIRONMENT_WAITING_WARNING', 'Solicite matr�cula para ter acesso �s disciplinas:');
	define('A_LANG_LENVIRONMENT_NAVTYPE', 'Acessar a Disciplina');
	define('A_LANG_LENVIRONMENT_FORCOURSE', 'para o curso de');
	define('A_LANG_LENVIRONMENT_NETCONECTION', 'Conex�o de Rede');
	define('A_LANG_LENVIRONMENT_SPEED', 'A velocidade �');
	define('A_LANG_LENVIRONMENT_NAVIGATION', 'Navega��o');
	define('A_LANG_LENVIRONMENT_TUTORIAL', 'Tutorial');
	define('A_LANG_LENVIRONMENT_FREE', 'Livre');
	// Inclus�o de mesagens de Usabilidade - Carla estagio UDESC 2008/02
	define('A_LANG_LENVIRONMENT_TUTORIAL_DESC', 'Navega��o auxiliada pelos pr�-requisitos definidos pelo professor.'); // carla
       define('A_LANG_LENVIRONMENT_FREE_DESC', 'Navega��o aberta a todos os conceitos (sem levar em considera��o as sugest�es do professor).');//carla
       define('A_LANG_LENVIRONMENT_PLUS', 'Saiba mais sobre os tipos de navega��o');//CARLA
	define('A_LANG_LENVIRONMENT_NAVIGATION_EXPLAIN', 'Atualmente o Ambiente Adaptweb disponibiliza dois modos de navega��o por uma disciplina (Tutorial e Livre). A diferen�a entre eles � em como navegar pela disciplina. Mas, os mesmos conte�dos s�o dispostos para os dois modos de navega��o.'); //Carla- explicacao no modo de navegacao - a_navega e n_navega
	// Final Carla
	
	//Ambiente do professor
	define('A_LANG_TENVIRONMENT_DESCRIPTION', 'O m�dulo de autoria permite ao autor disponibilizar conte�dos adaptados a diferentes perfis de alunos. Atrav�s do processo de autoria, o autor poder� disponibilizar o conte�do de suas aulas em uma �nica estrutura adaptada para os diferentes cursos. Nesta etapa o autor deve cadastrar a disciplina e para quais cursos deseja disponibiliz�-la. Ap�s estes cadastramentos devem ser inseridos os arquivos de conceito do conte�do, relacionando-os com cada t�pico da disciplina. Al�m disto o autor deve informar a descri��o do t�pico, seu pr�-requisito e para quais cursos deseja disponibilizar o conte�do. O autor poder� inserir exerc�cios, exemplos e materiais adicionais para cada t�pico.');
	define('A_LANG_TENVIRONMENT_AUTHOR', 'Autoria');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR1', 'Ocorreu um erro ao matricular o usu�rio nesta disciplina. Este usu�rio j� deve estar matriculado ou ocorreu um erro no servidor');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR2', 'Ocorreu um erro ao excluir a matr�cula do usu�rio nesta disciplina. Este usu�rio n�o deve estar matriculado ou ocorreu um erro no servidor');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_NOT', 'N�o h� curso cadastrado');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECT', 'Selecione o curso');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_NOTD', 'N�o h� disciplina cadastrada para este curso');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECTD', 'Selecione a disciplina');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_LIST', 'Lista de Alunos:');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_MATR', 'Alunos Matriculados:');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_BACK', 'Voltar para a lista de Conceitos');
	define('A_LANG_TENVIRONMENT_LIBERATE_DESCRIPTION', 'Somente estar�o dispon�veis para serem liberadas as disciplinas que estiverem com o conte�do correto. Para verificar, acesse o item <b>ESTRUTURAR DISCIPLINA</b> e use o bot�o <b>VERIFICAR PEND�NCIAS</b>.');
	define('A_LANG_TENVIRONMENT_LIBERATE_LIB', 'Disciplinas para Liberar:');
	define('A_LANG_TENVIRONMENT_LIBERATE_LIB_DIS', 'Disciplinas Liberadas:');
	define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE', 'Mover Conceito');
	define('A_LANG_TENVIRONMENT_ESTRUCT_EXCLUDE', 'Excluir Conceito');
	define('A_LANG_TENVIRONMENT_ESTRUCT_SELECT', 'Selecione o conceito que deseja <B>MOVER</B> ou <B>EXCLUIR</B>');
	define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE_AFTER', '<B>MOVER</B> conceito selecionado ap�s qual conceito?');
	define('A_LANG_TENVIRONMENT_ESTRUCT_CANCEL', 'a opera��o de MOVER ou EXCLUIR conceito ir� acarretar em cancelamento dos pr�-requisitos');
	define('A_LANG_TENVIRONMENT_ESTRUCT_NOT_POSSIBLE', 'N�o foi poss�vel ler a matriz do Banco de Dados');	
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND', 'Legenda de pr�-requisitos:');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND1', 'Para selecionar v�rios pr�-requisitos use a tecla CTRL');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND2', 'Para retirar o(s) pr�-requisito(s) da sele��o use a tecla CTRL');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND3', 'Os pr�-requisitos em <font class=buttonselecionado>azul escuro</font> est�o selecionados');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND4', 'O fundo <font class=buttonpre>azul</font> indica os pr�-requisitos atuais (caso perder a sele��o)');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND5', 'O fundo <font class=buttonpreautomatico>amarelo</font> indica os pr�-requisitos atuais autom�ticos');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NUMBER', 'N�mero do conceito:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NAME', 'Nome do exemplo:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_DESCRIPTION', 'Descri��o do exemplo:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_KEY', 'Palavras-chave do exemplo:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_LEVEL', 'N�vel de Complexidade:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NOCLASS', 'Sem classifica��o');
	define('A_LANG_TENVIRONMENT_EXAMPLES_EASY', 'F�cil');
	define('A_LANG_TENVIRONMENT_EXAMPLES_MEDIUM', 'M�dio');
	define('A_LANG_TENVIRONMENT_EXAMPLES_COMPLEX', 'Complexo');
	define('A_LANG_TENVIRONMENT_EXAMPLES_DSC', 'Descri��o');
	define('A_LANG_TENVIRONMENT_EXAMPLES_LVL', 'N�vel');
	define('A_LANG_TENVIRONMENT_EXERCISES_NAME', 'Nome do exerc�cio:');
	define('A_LANG_TENVIRONMENT_EXERCISES_DESCRIPTION', 'Descri��o do exerc�cio:');
	define('A_LANG_TENVIRONMENT_EXERCISES_KEY', 'Palavras-chave do exerc�cio:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_NAME', 'Nome do Material <br> Complementar:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_DESCRIPTION', 'Descri��o do Material <br> Complementar:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_KEY', 'Palavras-chave do <br> Material Complementar:');
	define('A_LANG_TENVIRONMENT_ACCESS', 'Acesso ao Ambiente de Autoria');
	define('A_LANG_TENVIRONMENT_WAIT', 'Aguarde o administrador liberar o acesso para o ambiente de autoria.');
	define('A_LANG_TENVIRONMENT_COMMENT', 'O acesso ao ambiente de autoria � gerenciado pelo administrador do AdaptWeb.<br>Ap�s efetuar cadastro no ambiente � necess�rio autoriza��o do administrador para ter acesso ao ambiente de autoria do sistema.');
	define('A_LANG_TENVIRONMENT_DEMO', 'Demonstra��o dos Ambientes');
	define('A_LANG_TENVIRONMENT_ASSOCIATED_FILES','Obs.: Arquivos associados devem ser enviados para o servidor atrav�s da se��o "manuten��o do conceito"');
	// Inclus�o de mensagens usabilidade - Carla estagio UDESC 2008/02
       define("A_LANG_TENVIRONMENT_EXAMPLES_OBS","<b>*Observa��o:</b> Para disponibilizar a disciplina � preciso preencher todos os campos desta p�gina corretamente.");//Carla inseriu para colocar uma observa��o na aba de exemplos.
       define("A_LANG_TENVIRONMENT_EXERC","Para criar um ou mais exerc�cios � preciso preencher todos os campos e fazer o upload dos arquivos em formato <b>.html </b>(ex.: exerc1.html, exerc2.html). Caso este arquivo possua alguma imagem, tamb�m � preciso fazer o upload da mesma.");//Carla inseriu para colocar uma introdu��o na aba de material complementar");//carla
       define("A_LANG_TENVIRONMENT_EXAMPLES","Para criar um ou mais exemplos � preciso preencher todos os campos e fazer o upload dos arquivos em formato <b>.html </b>(ex.:exemplo1.html, exemplo2.html). Caso este arquivo possua alguma imagem, tamb�m � preciso fazer o upload da mesma.");//Carla inseriu para colocar uma introdu��o na aba de exemplos.
       define("A_LANG_TENVIRONMENT_COMPLEMENTARY_DESC","Para criar um ou mais materiais complementares � preciso preencher todos os campos e fazer o upload dos arquivos em formato <b>.html </b>(ex.:mat1.html, mat2.html). Caso este arquivo possua alguma imagem, tamb�m � preciso fazer o upload da mesma.");//Carla inseriu para colocar uma introdu��o na aba de material complementar.
	// Final Carla


	//Op��es de Root
	define('A_LANG_ROOT_NOT_AUTH', 'Professores<br> n�o autorizados:');
	define('A_LANG_ROOT_AUTH', 'Professores<br> autorizados:');
	define('A_LANG_ROOT_DEV', 'Esta op��o encontra-se em fase de desenvolvimento.');

	//Outros
	define('A_LANG_DATA', 'Data');
	define('A_LANG_WARNING', 'Aviso');
	define('A_LANG_COORDINATOR', 'COORDENADOR');
	define('A_LANG_DATABASE_PROBLEM', 'H� problemas com o Banco de Dados');
	define('A_LANG_FAQ', 'Perguntas Freq�entes');
	define('A_LANG_WORKS', 'Trabalhos');
	define('A_LANG_NOT_DISC', 'N�o foi poss�vel apresentar as disciplinas cadastradas');
	define('A_LANG_NO_ROOT', 'N�o foi poss�vel inserir o usu�rio Root');
	define('A_LANG_MIN_VERSION', 'A vers�o m�nima suportada, do banco de dados � [');
	define('A_LANG_INSTALLED_VERSION', '], e a vers�o do instalada � [');
	define('A_LANG_ERROR_BD', 'N�o foi poss�vel gravar a matriz no Banco de Dados');
	define('A_LANG_DISC_NOT_FOUND', 'Nome da Disciplina n�o encontrado ou em branco');
	define('A_LANG_SELECT_COURSE', 'selecione o(s) curso(s) no conceito <b>');
	define('A_LANG_TOPIC_REGISTER', 'Cadastro de Conceitos');
	define('A_LANG_SUMARIZED_DESCRIPTION', 'Descri��o resumida do conceito');
	define('A_LANG_LOADED', 'carregada para o servidor');
	define('A_LANG_FILL_DESCRIPTION', 'Preencha o campo descri��o do material');
	define('A_LANG_WORKS_SEND_DATE', 'Data de envio');
	define('A_LANG_SEARCH_RESULTS', 'Resultados de Busca');
	define('A_LANG_SEARCH_SYSTEM', 'Sistema de Busca');
	define('A_LANG_SEARCH_WORD', 'Palavra buscada');
	define('A_LANG_SEARCH_FOUND', 'Foram encontradas ');
	define('A_LANG_SEARCH_OCCURRANCES', ' ocorr�ncias de ');
	define('A_LANG_SEARCH_IN_TOPIC', ' em CONCEITO');
	define('A_LANG_SEARCH_FOUND1', 'Foi encontrada ');
	define('A_LANG_SEARCH_OCCURRANCE', ' ocorr�ncia de ');
	define('A_LANG_SEARCH_NOT_FOUND', 'Nao foram encontradas ocorr�ncias de ');
	define('A_LANG_SEARCH_CLOSE', 'Fechar');
	define('A_LANG_TOPIC', 'Conceito');
	define('A_LANG_SHOW_MENU', 'Exibir Menu');
	define('A_LANG_CONFIG', 'Configura��es');
	define('A_LANG_MAP', 'Mapa');
	define('A_LANG_PRINT', 'Imprimir esta p�gina?');
	define('A_LANG_CONNECTION', 'N�o foi poss�vel estabelecer uma conex�o com o servidor de banco de dados MySQL. Favor contactar o administrador.');
	define('A_LANG_CONTACT', 'Favor contactar o administrador');
	define('A_LANG_HELP_SYSTEM', 'Sistema de Ajuda');
	define('A_LANG_CONFIG_SYSTEM', 'Configura��es do Sistema');
	define('A_LANG_CONFIG_COLOR', 'Configurar cor de fundo');
	define('A_LANG_NOT_CONNECTED', 'N�o conectou!');
	define('A_LANG_DELETE_CADASTRE', 'Exclus�o de Cadastro de Alunos');
	define('A_LANG_HOME', 'Home');
	define('A_LANG_YOU_HAVE', 'Voc� possui ');
	define('A_LANG_TO_CONFIRM', ' matr�cula(s). Para confirmar esta exclus�o clique em ');
	define('A_LANG_CONFIRM', ' CONFIRMAR');
	define('A_LANG_EXAMPLE_LIST', 'Lista de Exemplos');
	define('A_LANG_COMPLEXITY_LEVEL_EASY', 'N�vel de Complexidade F�cil');
	define('A_LANG_COMPLEXITY_LEVEL_MEDIUM', 'N�vel de Complexidade M�dio');
	define('A_LANG_COMPLEXITY_LEVEL_COMPLEX', 'N�vel de Complexidade Dif�cil');
	define('A_LANG_EXERCISES_LIST', 'Lista de Exerc�cios');
	define('A_LANG_REGISTER_MAINTENENCE', 'Manuten��o de Matr�culas');
	define('A_LANG_REGISTER_RELEASE', 'Libera��o de Matr�culas');
	define('A_LANG_SITE_MAP', 'Mapa do Site');
	define('A_LANG_COMPLEMENTARY_LIST', 'Lista de Material Complementar');
	define('A_LANG_DELETE_TOPIC', 'Exclu�do o conceito ');
	define('A_LANG_AND', ' e ');
	define('A_LANG_SONS', ' subconceito(s).');
	define('A_LANG_CONFIRM_EXCLUSION', 'Voc� tem certeza que deseja excluir a disciplina');
	define('A_LANG_YES', '  Sim  ');
	define('A_LANG_NO', '  N�o  ');
	// Inclusao mensagens usabilidade - Carla estagio UDESC 2008/02
	 define('A_LANG_CONFIRM_EXCLUSION_CURSO', 'Voc� tem certeza que deseja excluir o curso');
	// Final Carla

	
	//Gera��o de conte�do
	define('A_LANG_GENERATION_END', 'Fim da geracao do arquivo xml de elementos do t�pico');
	define('A_LANG_GENERATION_CREATED', 'Foram criados ');
	define('A_LANG_GENERATION_XML', ' arquivos xml.');
	define('A_LANG_GENERATION_END_STRUCT', 'Fim da geracao do XML de Estrutura de Topicos');
/*	define('A_LANG_GENERATION_MISSING_FILE', 'Faltando inserir arquivo de conceito do ');
	define('A_LANG_GENERATION_TH_TOPIC', 'conceito ');
	define('A_LANG_GENERATION_MISSING_COURSE', 'Faltando inserir curso do ');
	define('A_LANG_GENERATION_NO_ASOC', 'Conceito sem curso associado');
	define('A_LANG_GENERATION_MISSING_EXERC_ID', 'Faltando inserir identificador de exercicio do ');
	define('A_LANG_GENERATION_TH_EXERCISE', '� exerc�cio do ');
	define('A_LANG_GENERATION_MISSING_EXERC_DESCRIPTION', 'Faltando inserir descri��o de exercicio do ');
	define('A_LANG_GENERATION_MISSING_EXERC_FILE', 'Faltando inserir arquivo de exercicio do ');
	define('A_LANG_GENERATION_MISSING_EXERC_COMPLEXITY', 'Faltando inserir complexidade de exercicio do ');
	define('A_LANG_GENERATION_TH_EXAMPLE', '� exemplo do ');
	define('A_LANG_GENERATION_MISSING_EXAMP_ID', 'Faltando inserir identificador de exemplo do ');
	define('A_LANG_GENERATION_MISSING_EXAMP_DESCRIPTION', 'Faltando inserir descricao de exemplo do ');
	define('A_LANG_GENERATION_MISSING_EXAMP_FILE', 'Faltando inserir arquivo de exemplo do ');
	define('A_LANG_GENERATION_MISSING_EXAMP_COMPLEXITY' ,'Faltando inserir complexidade de exemplo do ');
	define('A_LANG_GENERATION_TH_MATCOMP', '� material complementar do ');
	define('A_LANG_GENERATION_MISSING_MATCO_ID', 'Faltando inserir identificador de material complementar do ');
	define('A_LANG_GENERATION_MISSING_MATCO_DESCRIPTION', 'Faltando inserir descricao de material complementar do ');
	define('A_LANG_GENERATION_MISSING_MATCO_FILE', 'Faltando inserir arquivo de material complementar do ');
*/	define('A_LANG_GENERATION_VERIFY', 'Verificacao dos dados da autoria');
	define('A_LANG_GENERATION_PROBLEMS', 'Problemas no cadastramento da Disciplina');
/*	define('A_LANG_GENERATION_MISSING_TOPIC_NUMBER', 'Faltando inserir Numero do Topico para o ');
	define('A_LANG_GENERATION_MISSING_TOPIC_DESCRIPTION', 'Faltando inserir Descri��o do Topico para o ');
	define('A_LANG_GENERATION_MISSING_TOPIC_ABREV', 'Faltando inserir Abrevia��o do Topico para o ');
	define('A_LANG_GENERATION_MISSING_TOPIC_KEYWORD', 'Faltando inserir Palavra Chave do Topico para o ');
*/	define('A_LANG_GENERATION_RESULT_XML', 'Resultado da Gera��o do XML');
	define('A_LANG_GENERATION_NO_ERROR', 'NAO HA ERRO');
	define('A_LANG_GENERATION_SUCCESS', 'ARQUIVOS DO CURSO GERADOS COM SUCESSO');
	define('A_LANG_GENERATION_DATA_LACK', 'Arquivos XML nao foram gerados por falta de dados');
	define ('A_LANG_GENERATION_TOPIC','Conceito');	
	define ('A_LANG_GENERATION_EXAMPLE','Exemplo');	
	define ('A_LANG_GENERATION_EXERCISE','Exerc�cio');	
	define ('A_LANG_GENERATION_COMPLEMENTARY','Material Complementar');	
	define ('A_LANG_GENERATION_TOPIC_NUMBER','N�mero T�pico');	
	define ('A_LANG_GENERATION_TOPIC_NAME','Nome T�pico');	
	define ('A_LANG_GENERATION_DESCRIPTION','Descri��o');	
	define ('A_LANG_GENERATION_KEY_WORD','Palavra-chave');	
	define ('A_LANG_GENERATION_COURSE','Curso');	
	define ('A_LANG_GENERATION_PRINC_FILE','Arquivo Principal');	
	define ('A_LANG_GENERATION_ASSOC_FILE','Arquivos Associados');	
	define ('A_LANG_GENERATION_MISSING','Ausente');	
	define ('A_LANG_GENERATION_NO_PROBLEM','Ok');	
	define ('A_LANG_GENERATION_NUMBER','N�mero');	
	define ('A_LANG_GENERATION_COMPLEXITY','N�vel');	
	define ('A_LANG_GENERATION_NOT_EXIST','N�o possui');	

	//Reposit�rio
	define ('A_LANG_MNU_REPOSITORY','Reposit�rio');	
	define ('A_LANG_PRESENTATION','Apresenta��o');
	define ('A_LANG_EXPORT','Exporta��o de Disciplinas');
	define ('A_LANG_IMPORT','Importa��o de Disciplinas');
	define ('A_LANG_OBJECT_SEARCH','Pesquisar Objetos');
	define ('A_LANG_EXP_TEXT', 'Envie suas disciplinas para o Reposit�rio para que outros professores possam utiliz�-la tamb�m.');
	define ('A_LANG_IMP_TEXT', 'Utilize disciplinas dispon�veis no seu ambiente AdaptWeb.');
	define ('A_LANG_SCH_TEXT', 'Os dados das Disciplinas do AdaptWeb tamb�m foram indexados no formato de Objetos de Aprendizagem utilizando metadados do padr�o LOM (Learning Object Metadata). Fa�a buscas por objetos de aprendizagem abrangendo disciplinas, t�picos (conceitos) e arquivos do reposit�rio.');
	define ('A_LANG_REP_TEXT', 'Bem vindo ao Reposit�rio de Disciplinas do AdaptWeb.<br>Atrav�s deste reposit�rio, usu�rios do AdaptWeb de diferentes lugares poder�o compartilhar dados para cria��o de suas disciplinas. Aqui voc� pode fazer importa��o e exporta��o de disciplinas, al�m de buscar objetos de aprendizagem.');
	define ('A_LANG_REP_SELECT', 'Selecione uma disciplina');
	define ('A_LANG_REP_EXP_SUCCESS', 'Exporta��o efetuada com sucesso!');
	define ('A_LANG_REP_EXP_ERROR', 'Erro ao exportar disciplina!');
	define ('A_LANG_REP_EXP_TEXT', 'Atrav�s dessa interface voc� poder� disponibilizar suas disciplinas do AdaptWeb para que outras pessoas possam utiliz�-las.<br>Selecione a disciplina na lista abaixo, especifique uma descri��o para ela e clique em &quot;Exportar&quot;.');
	define ('A_LANG_REP_EXPORT', 'Exportar');
	define ('A_LANG_REP_SELECT_COURSE', 'Selecione um Curso');
	define ('A_LANG_REP_IMP_SUCCESS', 'Importa��o efetuada com sucesso!');
	define ('A_LANG_REP_IMP_ERROR', 'Erro ao importar disciplina!');
	define ('A_LANG_REP_INVALID_PAR', 'Par�metros inv�lidos!');
	define ('A_LANG_REP_IMP_TEXT', 'Atrav�s dessa interface voc� poder� copiar disciplinas dispon�veis no reposit�rio para o seu ambiente de autoria no AdaptWeb!<br>Selecione a disciplina na lista abaixo, especifique o curso que ela ficar� associada, clique em &quot;Importar&quot;.');
	define ('A_LANG_REP_DISC_IN_REP', 'Disciplinas no Reposit�rio');
	define ('A_LANG_REP_MY_COURSES', 'Meus Cursos');
	define ('A_LANG_REP_OTHER_NAME', 'Importar com outro nome');
	define ('A_LANG_REP_IMPORT', 'Importar');
	define ('A_LANG_REP_DISC_DATA', 'Dados da Disciplina');
	define ('A_LANG_REP_SEARCH', 'Digite uma string de busca e escolha os metadados que dever�o ser inclu�dos na pesquisa:');
	define ('A_LANG_REP_SCH_TITLE', 'T�tulo');
	define ('A_LANG_REP_SCH_SELECT', 'Selecione...');
	define ('A_LANG_REP_AGGREG', 'Aggregation Level');
	define ('A_LANG_REP_SCH_RAW', 'Dados (arquivos)');
	define ('A_LANG_REP_SCH_TOPIC', 'Conceito');
	define ('A_LANG_REP_SCH_DISC', 'Disciplina');
	define ('A_LANG_REP_SCH_COURSE', 'Curso');
	define ('A_LANG_REP_SCH_SEARCH', 'Pesquisar');
	define ('A_LANG_REP_SCH_ERROR', 'Erro');
	define ('A_LANG_REP_SCH_NO_REG', 'Nenhum registro encontrado!');
	define ('A_LANG_REP_SCH_REG', 'registro encontrado');
	define ('A_LANG_REP_SCH_REGS', 'registros encontrados');
	define ('A_LANG_REP_DISC_NOT_FOUND', 'Disciplina n�o encontrada');
	define ('A_LANG_COURSES', 'Curso(s)');
	define ('A_LANG_EXP_BY', 'Exportada por');
	define ('A_LANG_ERROR_SELECT_BD', 'Erro ao selecionar a base de dados');

	
/******************************************************************************************** 
 * Avalia��o (Definido por Claudiomar Desanti)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus&atilde;o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 */ 
define("A_LANG_MNU_AVS", "Fun&ccedil;&otilde;es de Avalia&ccedil;&atilde;o"); 
define("A_LANG_MNU_AVS_NAV", "Avalia&ccedil;&atilde;o"); // menu na parte de navega&ccedil;&atilde;o
define("A_LANG_TOPIC_EVALUATION","Avalia&ccedil;&atilde;o"); // orelha de avaliacao

define("A_LANG_AVS_STUDENTS_TOPIC_EVALUATION", "Avalia&ccedil;&otilde;es Liberadas"); // t&oacute;pico da avalia&ccedil;&atilde;o
define("A_LANG_AVS_STUDENTS_TOPIC_RESULTS", "Quadro de Resultados"); // t&oacute;pico de quadro de resultados

define("A_LANG_AVS_FUNCTION_INTRO","Introdu&ccedil;&atilde;o");
define("A_LANG_AVS_FUNCTION_AVERAGE","M&eacute;dia");
define("A_LANG_AVS_FUNCTION_ADJUSTMENT","Ajustar <br /> nota");
define("A_LANG_AVS_FUNCTION_LIBERATE","Liberar <br /> avalia&ccedil;&atilde;o");
define("A_LANG_AVS_FUNCTION_DIVULGE","Divulgar <br /> nota");
define("A_LANG_AVS_FUNCTION_REPORT","Relat&oacute;rio");
define("A_LANG_AVS_FUNCTION_BACKUP","Backup");


define("A_LANG_AVS_TITLE_ADJUSTMENT_AVERAGE","Ajuste M&eacute;dia");
define("A_LANG_AVS_TITLE_ADJUSTMENT_QUESTION_CANCEL","Anular Quest&otilde;es");
define("A_LANG_AVS_TITLE_ADJUSTMENT","Ajuste de Notas");
define("A_LANG_AVS_TITLE_ADJUSTMENT_QUESTION_WEIGHT","Distribuir pesos de avalia&ccedil;&otilde;es realizadas");
define("A_LANG_AVS_TITLE_DIVULGE","Divulga&ccedil;&atilde;o das Notas das Avalia&ccedil;&otilde;es");
define("A_LANG_AVS_TITLE_NEW_AVAL", "Nova Avalia&ccedil;&atilde;o");
define("A_LANG_AVS_TITLE_NEW_AVAL_PRES", "Nova Avalia&ccedil;&atilde;o Presencial");
define("A_LANG_AVS_TITLE_GROUP","Novo Grupo");
define("A_LANG_AVS_TITLE_LIBERATE","Liberar Avalia&ccedil;&atilde;o");
define("A_LANG_AVS_TITLE_AVERAGE","Distribui&ccedil;&atilde;o das m&eacute;dias");
define("A_LANG_AVS_TITLE_REPORT", "Relat&oacute;rio");
define("A_LANG_AVS_TITLE_BACKUP", "Backup");
define("A_LANG_AVS_TITLE_DISSERT","Corre&ccedil;&atilde;o das quest&otilde;es dissertativas");
define("A_LANG_AVS_TITLE_ADJUSTMENT_PRESENTIAL","Ajuste de notas das avalia&ccedil;&otilde;es presenciais");
define("A_LANG_AVS_TITLE_ADD_QUESTION","Adicionar quest&otilde;es existentes");

// -------------- ERROS ------------------------------------------------- //
define("A_LANG_AVS_ERROR_DB_OBJECT","Falha na inicializa&ccedil;&atilde;o, objeto de conex&atilde;o ao banco de dados &eacute; inv&aacute;lido");
define("A_LANG_AVS_ERROR_DB_SAVE","Erro ao salvar as informa&ccedil;&otilde;es");
define("A_LANG_AVS_ERROR_PARAM","<div class='erro'><p>Erro na passagem de par&acirc;metros. Recarregue a p&aacute;gina.</p></div>");

define("A_LANG_AVS_ERROR_VERIFY_AUTHO","<div class='erro'><p>Voc&ecirc; n&atilde;o pode executar esta parte do ambiente.</p></div>");
define("A_LANG_AVS_ERROR_EVALUATION_DELETE","N&atilde;;o foi poss&iacute;vel excluir a quest&atilde;o da avalia&ccedil;&atilde;o");


define("A_LANG_AVS_ERROR_EVALUATION_AVERAGE","Nenhuma avalia&ccedil;&atilde;o foi criada, ou assimilida com algum curso");
define("A_LANG_AVS_ERROR_FUNCTION_EVALUATION_LIBERATE","Nenhuma avalia&ccedil;&atilde;o ainda foi liberada e/ou finalizada");
define("A_LANG_AVS_ERROR_QUESTION_OBJECT","Imposs&iacute;vel carregar a quest&atilde;o");
define("A_LANG_AVS_ERROR_ANSWER_OBJECT","Imposs&iacute;vel carregar a resposta da quest&atilde;o");
define("A_LANG_AVS_ERROR_LOGGED","Voc&ecirc; deve efetuar primeiro o login no sistema para trabalhar com este m&oacute;dulo");
define("A_LANG_AVS_ERROR_STUDENTS_LIBERATE","Erro em carregar a avalia&ccedil;&atilde;o");
define("A_LANG_AVS_ERROR_STUDENTS_VIEW","Est&aacute; avalia&ccedil;&atilde;o n&atilde;o &eacute; sua");
define("A_LANG_AVS_ERROR_EVALUATION_LIBERATE","N&atilde;o foi encontrada a avalia&ccedil;&atilde;o");
define("A_LANG_AVS_ERROR_EVALUATION_LIBERATE_STUDENTS","O aluno n&Atilde;o est&acute; com a avalia&ccedil;&aacute;o liberada");
define("A_LANG_AVS_ERROR_EVALUATION_LOGGED","Voc&ecirc; deve estar logado para acessar a avalia&ccedil;&atilde;o");
define("A_LANG_AVS_ERROR_EVALUATION_INIT","Erro ao tentar inicializar a avalia&ccedil;&atilde;o");
define("A_LANG_AVS_ERROR_EVALUATION_SUBMIT","Avalia&ccedil;&atilde;o j&atilde; foi encerrada pelo professor");
define("A_LANG_AVS_ERROR_AUTHO","Voc&ecirc; n&atilde;o tem autoriza&ccedil;&atilde;o para modificar este m&oacute;dulo");
define("A_LANG_AVS_ERROR_TOPIC_EVALUATION","Nenhum conceito foi encontrado com avalia&ccedil;&otilde;es dispon&iacute;veis. Verifique se as quest&otilde;es das avalia&ccedil;&otilde;es est&atilde;o com os pesos definidos e se o peso da m&eacute;dia j&aacute; est&aacute; definida.");
define("A_LANG_AVS_ERROR_LIBERATE_NOT_STUDENTS", "Nenhum aluno cadastrado na disciplina para realizar as avalia&ccedil;&otilde;es");
define("A_LANG_AVS_ERROR_DIVULGE_VALUE","N&atilde;o foi poss&iacute;vel divulgar as notas.");
define("A_LANG_AVS_ERROR_DIVULGE_NOT_EVALUATION","Nenhuma avalia&ccedil;&atilde;o foi finalizada para que possa ser divulgada.");
define("A_LANG_AVS_ERROR_LIBERATE_EVALUATION","N&atilde;o foi poss&iacute;vel concluir a libera&ccedil;&atilde;o da avalia&ccedil;&atilde;o.");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT","N&atilde;o foi encontrada nenhuma avalia&ccedil;&atilde;o");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_SAVE","Erro em salvar a quest&atilde;o");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_AVERAGE","Erro em calcular a nota");
define("A_LANG_AVS_ERROR_AVERAGE_CALCULE","Erro em calcular a m&eacute;dia");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_STUDENT","Aluno n&atilde;o entregou as respostas desta avalia&ccedil;&atilde;o");
define("A_LANG_AVS_ERROR_ADJUSTMENT_PRESENTIAL","N&atilde;o foram divulgadas as notas, pois alguns alunos ainda n&atilde;o receberam nota");
define("A_LANG_AVS_ERROR_ADD_QUESTION_EXIST","Erro em adicionar as novas quest&otilde;es na avalia&ccedil;&atilde;o");
define("A_LANG_AVS_ERROR_QUESTION_NOT_FOUND","Nenhum resultado foi encontrado");
define("A_LANG_AVS_ERROR_NOT_STUDENTS_LIBERATE","N&atilde;o h&aacute; alunos matriculados nesta disciplina");
define("A_LANG_AVS_ERROR_EVALUATION_FINISH","A avalia&ccedil;&atilde;o j&aacute; foi entregue anteriormente");
define("A_LANG_AVS_ERROR_EVALUATION_FINISH2","A avalia&ccedil;&atilde;o j&aacute; foi encerrada, voc&ecirc; n&atilde;o pode mais entreg&aacute;-la");
define("A_LANG_AVS_ERROR_SESSION_BROWSER","A sess&atilde;o que iniciou a avalia&ccedil;&atilde;o n&atilde;o &eacute; a mesma.<br />Isto pode ter ocorrido pela troca de um computador para outro, ou de navegadores");
define("A_LANG_AVS_ERROR_FUNCTION_CANCEL","N&atilde;o &eacute; cancelar as quest&otilde;es. &Eacute; necess&aacute;rio que haja ao menos uma quest&atilde;o n&atilde;o cancelada");


define("A_LANG_AVS_ERROR_QUESTION_LOGGED","Voc&ecirc; deve efetuar primeiro o login no sistema para trabalhar com este m&otilde;dulo");
define("A_LANG_AVS_ERROR_QUESTION_EVALUATION","A quest&atilde;o n&atilde;o pode ser adicionada a avalia&ccedil;&atilde;o");
define("A_LANG_AVS_ERROR_QUESTION_LACUNA_DELETE","Erro ao excluir a lacuna");
define("A_LANG_AVS_ERROR_QUESTION_ME_DELETE","N&atilde;o foi poss&iacute;vel excluir a alternativa");
// erros utilizados no ajax
define("A_LANG_AVS_ERROR_AJAX_AVERAGE","Ainda n&atilde;o foram definidas avalia&ccedil;&otilde;es para este curso");
define("A_LANG_AVS_ERROR_AJAX_AVERAGE1","Aluno ainda n&atilde;o realizou avalia&ccedil;&otilde;es");
define("A_LANG_AVS_ERROR_AJAX_DIVULGE1","Os alunos assinalados com pend&ecirc;ncia s&atilde;o alunos que ainda tem quest&otilde;es dissertativas a serem corrigidas.");
define("A_LANG_AVS_ERROR_AJAX_DIVULGE2","Nenhum aluno realizou esta avalia&ccedil;&atilde;o. A avalia&ccedil;&atilde;o foi cancelada e os alunos realizaram outra avalia&ccedil;&atilde;o.");
define("A_LANG_AVS_ERROR_AJAX_LIBERATE","Nenhuma avalia&ccedil;&atilde;o est&aacute; liberada para resolu&ccedil;&atilde;o");
define("A_LANG_AVS_ERROR_AJAX_LIBERATE2","Nenhuma avalia&ccedil;&atilde;o foi encerrada");
define("A_LANG_AVS_ERROR_AJAX_AVERAGE3","Esta disciplina n&atilde;o cont&ecirc;m nenhum grupo de valia&ccedil;&atilde;o para que possa ser disponibilizada as m&eacute;dias");
// ------------------- FIM DOS ERROS -----------------------------------------------------//

define("A_LANG_AVS_SUCESS", "Salvo com sucesso");
define("A_LANG_AVS_SUCESS_DIVULGE","As notas da avalia&ccedil;&atilde;o foram liberadas aos alunos.");
define("A_LANG_AVS_SUCESS_LIBERATE","Avalia&ccedil;&atilde;o liberada com sucesso");
define("A_LANG_AVS_SUCESS_EVALUATION","A avalia&ccedil;&atilde;o foi entregue com sucesso");
define("A_LANG_AVS_STUDENTS","Alunos");

// -------------- BOTOES -----------------------------------------------------------------//
define("A_LANG_AVS_BUTTON_SAVE","Salvar");
define("A_LANG_AVS_BUTTON_SAVE_COPY_DEFAULT","Copiar peso default");
define("A_LANG_AVS_BUTTON_SAVE_COPY_MOD","Copiar peso modificado");
define("A_LANG_AVS_BUTTON_SUBMIT","Submeter avalia&ccedil;&atilde;o");
define("A_LANG_AVS_BUTTON_BACK",A_LANG_TENVIRONMENT_AUTHORIZE_BACK);
define("A_LANG_AVS_BUTTON_HELP",A_LANG_HELP);
define("A_LANG_AVS_BUTTON_NEW_GROUP","Novo grupo");
define("A_LANG_AVS_BUTTON_CHANGE_OPTION","Alterar param&ecirc;tro");
define("A_LANG_AVS_BUTTON_NEW_EVALUATION","Nova avalia&ccedil;&atilde;o");
define("A_LANG_AVS_BUTTON_WEIGHT","Dividir pesos igualmente");
define("A_LANG_AVS_BUTTON_NEXT","Avan&ccedil;ar");
define("A_LANG_AVS_BUTTON_LIBERATE_EVALUATION","Liberar nova avalia&ccedil;&atilde;o");
define("A_LANG_AVS_BUTTON_FINISH_EVALUATION","Encerrar avalia&ccedil;&atilde;o");
define("A_LANG_AVS_BUTTON_LIST_STUDENTS","Listar alunos");
define("A_LANG_AVS_BUTTON_CLOSE","Fechar");
define("A_LANG_AVS_BUTTON_SEARCH","Buscar");
define("A_LANG_AVS_BUTTON_MARK","Marcar todos");
define("A_LANG_AVS_BUTTON_UNMARK","Desmarcar todos");

// bot&otilde;es do AVS_TOPICO.PHP
define("A_LANG_AVS_BUTTON_GROUP_NEW","Novo grupo");
define("A_LANG_AVS_BUTTON_GROUP_PARAM","Alterar par&acirc;metros");
define("A_LANG_AVS_BUTTON_GROUP_NEW_EVALUATION","Adicionar avalia&ccedil;&atilde;o");
define("A_LANG_AVS_BUTTON_DUPLICATE_AVAL","Duplicar Avalia&ccedil;&atilde;o");
define("A_LANG_AVS_BUTTON_AVAL_PRESEN","Adicionar avalia&ccedil;&atilde;o presencial");
// bot&otilde;es AVS_NOVA_AVAL.PHP
define("A_LANG_AVS_BUTTON_QUESTION_NEW","Adicionar nova quest&atilde;o");
define("A_LANG_AVS_BUTTON_QUESTION_EXIST","Adicionar quest&atilde;o existente");
// bot&otilde;es utilizados na libera&ccedil;&atilde;o da avalia&ccedil;&atilde;o
define("A_LANG_AVS_BUTTON_EVALUATION_LIBERATE_NEW","Liberar nova avalia&ccedil;&atilde;o");
define("A_LANG_AVS_BUTTON_EVALUATION_LIBERATE_FINISH","Listar avali&ccedil;&otilde;es encerradas");
define("A_LANG_AVS_BUTTON_BACK_LIBERATE","Voltar a p&aacute;gina inicial da Libera&ccedil;&atilde;o");
// bot&otilde;es utilizados nas telas de quest&otilde;es
define("A_LANG_AVS_BUTTON_QUESTION_ADD_LACUNA","Adicionar nova lacuna");
define("A_LANG_AVS_BUTTON_QUESTION_DELETE_LACUNA","Excluir lacuna");
define("A_LANG_AVS_BUTTON_QUESTION_ADD_ME","Adicionar nova alternativa");

// bot&otilde;es utilizados nos ajax
define("A_LANG_AVS_BUTTON_AJAX_AVERAGE_LIST","Listar Notas");
define("A_LANG_AVS_BUTTON_AJAX_DIVULGE","Divulgar");
define("A_LANG_AVS_BUTTON_AJAX_LIBERATE_END","Encerrar Avalia&ccedil;&atilde;o");
define("A_LANG_AVS_BUTTON_AJAX_LIST","Listar Alunos");

// --------------- FIM DOS BOTOES ---------------------------------------------------- //  
// --------------- LABELS - texto ao lado de combobox e spans ------------------------ //
define("A_LANG_AVS_LABEL_COURSE",A_LANG_COURSES);
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_VALUE","Nota");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_EXPLANATION","Explica&ccedil;&atilde;o");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_WEIGHT","Peso");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_CORRECT","Resposta correta");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_OBS_TEACHER","Coment&aacute;rio do professor");
define("A_LANG_AVS_LABEL_EVALUATION_MODEL","Modelos de Avalia&ccedil;&atilde;o");
define("A_LANG_AVS_LABEL_EVALUATION_MODEL_AUTO","Modelos de Avalia&ccedil;&atilde;o Automatizada");
define("A_LANG_AVS_LABEL_EVALUATION_MODEL_PRESENTIAL","Modelos de Avalia&ccedil;&atilde;o Presencial");
define("A_LANG_AVS_LABEL_EVALUATION","Avalia&ccedil;&atilde;o");
define("A_LANG_AVS_LABEL_TOPIC","Conceito");
define("A_LANG_AVS_LABEL_VIEW_EVALUATION","Visualiza&ccedil;&atilde;o da avalia&ccedil;&atilde;o pelo professor");
define("A_LANG_AVS_LABEL_YES","Sim");
define("A_LANG_AVS_LABEL_NOT","N&atilde;o");
define("A_LANG_AVS_LABEL_INFO","Informa&ccedil;&otilde;es");
//

define("A_LANG_AVS_LABEL_QUESTION_DISSERT","Dissertativa");
define("A_LANG_AVS_LABEL_QUESTION_ME","M&uacute;ltipla-Escolha");
define("A_LANG_AVS_LABEL_QUESTION_LACUNAS","Preecher Lacunas");
define("A_LANG_AVS_LABEL_QUESTION_VF","Verdadeiro/Falso");

define("A_LANG_AVS_LABEL_FUNCTION","Fun&ccedil;&otilde;es");
define("A_LANG_AVS_LABEL_QUESTION","Quest&otilde;es");

// labels do AVS_GRUPO.PHP
define("A_LANG_AVS_LABEL_QUESTION_RANDOM","As quest&otilde;es dever&atilde;o ser misturadas?");
define("A_LANG_AVS_LABEL_EVALUATION_PRESENT","Avalia&ccedil;&atilde;o Presencial?");
define("A_LANG_AVS_LABEL_ANOTHER_GROUP","Escolhido em outro grupo");
// labels do AVS_NOVA_AVAL.PHP
define("A_LANG_AVS_LABEL_DESCRIPTION","Descri&ccedil;&atilde;o");
define("A_LANG_AVS_LABEL_DIVULGE_NOTE","Divulgar nota automaticamente?");
define("A_LANG_AVS_LABEL_DISSERT_QUESTION","h&aacute; quest&otilde;es dissertativas na avalia&ccedil;&atilde;o");
// labels do AVS_TOPICO.PHP
define("A_LANG_AVS_LABEL_GROUP_DESCRIPTION","Grupo de avalia&ccedil;&atilde;o para conceito");
define("A_LANG_AVS_LABEL_GROUP_NOT_WEIGHT","Ainda n&atilde;o definido");
define("A_LANG_AVS_LABEL_GROUP_PARAM","Par&acirc;metros:");
define("A_LANG_AVS_LABEL_GROUP_QUESTION_RANDOM","Misturar quest&otilde;es:");
define("A_LANG_AVS_LABEL_GROUP_EVALUATION_PRESENT",A_LANG_AVS_LABEL_EVALUATION_PRESENT);
define("A_LANG_AVS_LABEL_GROUP_COURSE_WEIGHT","Cursos (Peso%):");
define("A_LANG_AVS_LABEL_GROUP_NOT_EVALUATION","N&atilde;o h&aacute; avalia&ccedil;&otilde;es para este grupo");
// labels utilizados no F_AJUSTE.PHP
define("A_LANG_AVS_LABEL_FUNCTION_AVERAGE","Ajustar M&eacute;dia");
define("A_LANG_AVS_LABEL_FUNCTION_QUESTION_DISSERT","Corre&ccedil;&atilde;o de quest&otilde;es dissertativas");
define("A_LANG_AVS_LABEL_FUNCTION_QUESTION_CANCEL","Anular quest&otilde;es da avalia&ccedil;&atilde;o");
// labels utilizados no F_AJUSTE_ANULAR.PHP
define("A_LANG_AVS_LABEL_CHOOSE_EVALUATION","Selecione a avalia&ccedil;&atilde;o");
define("A_LANG_AVS_LABEL_CHOOSE_COURSE","Selecione o curso");
define("A_LANG_AVS_LABEL_CHOOSE_TOPIC","Selecione o conceito");
define("A_LANG_AVS_LABEL_CHOOSE_STUDENTS","Escolha os alunos que ir&atilde;o realizar a avalia&ccedil;&atilde;o");

// labels utilizados no ambiente do aluno
define("A_LANG_AVS_LABEL_USER_NOT_EVALUATION","N&atilde;o h&aacute; avalia&ccedil;&otilde;es liberadas");
define("A_LANG_AVS_LABEL_USER_NOT_VALUE","N&atilde;o h&aacute; notas disponibilizadas");
// labels das telas de quest&otilde;es
define("A_LANG_AVS_LABEL_QUESTION_ENUNCIATE","Enunciado da quest&atilde;o:");
define("A_LANG_AVS_LABEL_QUESTION_EXPLANATION","Explica&ccedil;&atilde;o:");
define("A_LANG_AVS_LABEL_QUESTION_OPTION_CORRECT","Qual op&ccedil;&atilde;o correta?");
define("A_LANG_AVS_LABEL_QUESTION_VF_CORRECT","Verdadeiro");
define("A_LANG_AVS_LABEL_QUESTION_VF_FALSE","Falso");
define("A_LANG_AVS_LABEL_QUESTION_VF_NAME_CORRECT","Nome Verdadeiro");
define("A_LANG_AVS_LABEL_QUESTION_VF_NAME_FALSE","Nome Falso");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_DISSERT","Quest&atilde;o do tipo Dissertativa");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_LACUNA","Quest&atilde;o do tipo Lacunas");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_ME","Quest&atilde;o do tipo M&uacute;ltipla-Escolha");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_VF","Quest&atilde;o do tipo Verdadeiro/Falso");

define("A_LANG_AVS_LABEL_AJAX_DIVULGE1","Ok");
define("A_LANG_AVS_LABEL_AJAX_DIVULGE2","Pendente");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE1","Pendente");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE2","Realizando");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE3","Entregue");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE4","N&atilde;o entregue");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE5","Zerada");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION1","Lista de avalia&ccedil;&otilde;es liberadas");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION2","T&oacute;pico");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION3","Descri&ccedil;&atilde;o");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION4","Lista de avalia&ccedil;&otilde;es encerradas");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION5","Avalia&ccedil;&atilde;o presencial");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION6","Corrigida");
define("A_LANG_AVS_LABEL_AJAX_AVERAGE","Grupo de Avalia&ccedil;&atilde;o para o t&oacute;pico");

define("A_LANG_AVS_LABEL_AJAX_ZERO","A nota cancelada ser&aacute; zerada");

define("A_LANG_AVS_LABEL_SEARCH","Busca por palavra-chave");



// -------------- FIM DOS LABELS ------------------------------------------------------------ //
// -------------- COMBOBOX ----------------------------------------------------------------- //

define("A_LANG_AVS_COMBO_SELECT","Selecione...");
define("A_LANG_AVS_COMBO_DATE","Liberada");

// utilizado no F_AJUSTE_ANULAR.PHP
define("A_LANG_AVS_COMBO_TOPIC","T&oacute;pico:");
define("A_LANG_AVS_COMBO_MARK","Marcar");
define("A_LANG_AVS_COMBO_UNMARK","Desmarcar");


// -------------- TTULOS DE TABELAS -------------------------------------------------------- //
define("A_LANG_AVS_TABLE_MODIFY","Alterar");
define("A_LANG_AVS_TABLE_DELETE","Excluir");
define("A_LANG_AVS_TABLE_DESCRIPTION","Descri&ccedil;&atilde;o");
// t&iacute;tulo do AVS_NOVA_AVAL.PHP
define("A_LANG_AVS_TABLE_TYPE","Tipo");
define("A_LANG_AVS_TABLE_WEIGHT","Peso(%)");
define("A_LANG_AVS_TABLE_WEIGHT_DEFAULT","Peso default(%)");
define("A_LANG_AVS_TABLE_WEIGHT_MOD","Peso modificado(%)");
define("A_LANG_AVS_TABLE_SUM_WEIGHT","Total dos pesos:");
define("A_LANG_AVS_TABLE_REST_WEIGH","Pesos restantes:");
// t&iacute;tulo do AVS_TOPICO.PHP
define("A_LANG_AVS_TABLE_DIVULGE","Divulga&ccedil;&atilde;o");
define("A_LANG_AVS_TABLE_NUMBER_QUESTION","Quantidade de quest&otilde;es");
define("A_LANG_AVS_TABLE_DATE_START","Data libera&ccedil;&atilde;o");
define("A_LANG_AVS_TABLE_DATE_FINISH","Data finaliza&ccedil;&atilde;o");
define("A_LANG_AVS_TABLE_DATE","Data");
define("A_LANG_AVS_TABLE_VALUE_CANCELLED","Notas cancelada");
define("A_LANG_AVS_TABLE_ACTION","A&ccedil;&otilde;es");
define("A_LANG_AVS_TABLE_DATE_CREATE","Cria&ccedil;&atilde;o");
define("A_LANG_AVS_TABLE_DATE_MODIFY","Altera&ccedil;&atilde;o");
define("A_LANG_AVS_TABLE_DIVULGE_AUTO","Autom&aacute;tica");
define("A_LANG_AVS_TABLE_DIVULGE_MANUAL","Manual");
// titulo dos scripts do ambiente do aluno (N_INDEX, N_MURAL)
define("A_LANG_AVS_TABLE_USER_EVALUATION","Avalia&ccedil;&atilde;o");
define("A_LANG_AVS_TABLE_USER_DATE","Data de in&iacute;cio");
define("A_LANG_AVS_TABLE_USER_REALIZE_EVALUATION","Realizar avalia&ccedil;&atilde;o");
define("A_LANG_AVS_TABLE_USER_DESCRIPTION_EVALUATION","Descri&ccedil;&atilde;o");
define("A_LANG_AVS_TABLE_USER_DATE_REALIZE","Data da Realiza&ccedil;&atilde;o");
define("A_LANG_AVS_TABLE_USER_VALUE","Notas");
define("A_LANG_AVS_TABLE_USER_VALUE2","Nota Prova");
define("A_LANG_AVS_TABLE_USER_TOTAL_VALUE","Nota Total");
// titulos de tabelas das telas de quest&otilde;es
define("A_LANG_AVS_TABLE_QUESTION_LACUNA1","Texto inicial");
define("A_LANG_AVS_TABLE_QUESTION_LACUNA2","Resposta da lacuna");
define("A_LANG_AVS_TABLE_QUESTION_LACUNA3","Continua&ccedil;&atilde;o do texto");
define("A_LANG_AVS_TABLE_QUESTION_ME1","Alternativas");
define("A_LANG_AVS_TABLE_QUESTION_ME2","Correta?");
define("A_LANG_AVS_TABLE_QUESTION_ME3","Fixa?");
define("A_LANG_AVS_TABLE_QUESTION_ME4","Descri&ccedil;&atilde;o das Alternativas");
define("A_LANG_AVS_TABLE_QUESTION_ME5","Excluir");
// titulos utilizados nas tabelas do ajuste de notas dissertativa
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT","Aluno");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT1","Quest&otilde;es pendentes");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT2","Aluno n&atilde;o finalizou a avalia&ccedil;&atilde;o");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT3","Corrigir quest&otilde;es");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT4","Alterar corre&ccedil;&otilde;es");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT5","Peso");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT6","Nota");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT7","Explica&ccedil;&atilde;o");

// titulos utilizados nos ajax
define("A_LANG_AVS_TABLE_AJAX_AVERAGE","Aluno");
define("A_LANG_AVS_TABLE_AJAX_AVERAGE1","M&eacute;dia Adquirida");
define("A_LANG_AVS_TABLE_AJAX_AVERAGE2","Nota Participativa");
define("A_LANG_AVS_TABLE_AJAX_AVERAGE3","M&eacute;dia Final");
define("A_LANG_AVS_TABLE_AJAX_QUESTION_CANCEL","Cancelar");
define("A_LANG_AVS_TABLE_AJAX_QUESTION_UNDO","Desfazer");
define("A_LANG_AVS_TABLE_AJAX_QUESTION_CANCEL2","Cancelada");
define("A_LANG_AVS_TABLE_AJAX_DIVULGE1","Aluno");
define("A_LANG_AVS_TABLE_AJAX_DIVULGE2","Pend&ecirc;ncia");

// hints - texto flutuante informativo
define("A_LANG_AVS_HINTS_EVALUATION_EDIT","Editar avalia&ccedil;&atilde;o. Incluir novas quest&otilde;es");
define("A_LANG_AVS_HINTS_EVALUATION_DELETE","Excluir avalia&ccedil;&atilde;o");
define("A_LANG_AVS_HINTS_EVALUATION_VIEW","Visualizar avalia&ccedil;&atilde;o");
define("A_LANG_AVS_HINTS_USER_EVALUATION_VIEW","Esta nota &eacute; referente aos acertos na prova, sem contar as quest&otilde;es anuladas pelo professor");
define("A_LANG_AVS_HINTS_QUESTION_DELETE","Excluir esta alternativa");
define("A_LANG_AVS_HINTS_AJAX_QUESTION_CANCEL","Esta quest&atilde;o j&aacute; foi cancelada anteriormente");
define("A_LANG_AVS_HINTS_AJAX_DIVULGE1","Corrigir as quest&otilde;es dissertativas");
// textos
define("A_LANG_AVS_TEXT_LIBERATE","Este &eacute; o n&uacute;mero da avalia&ccedil;&atilde;o liberada. Guarde-o para consultas futuras.");
define("A_LANG_AVS_TEXT_FIELD_REQUIRE","Os campos marcados com <b>*</b> devem ser preenchidos");
define("A_LANG_AVS_TEXT_ADJUSTMENT","Os campos <b>explica&ccedil;&atilde;o</b> ser&atilde;o apresentados aos alunos como uma explica&ccedil;&atilde;o do erro/acerto. O preenchimento n&atilde;o &eacute; obrigat&oacute;rio.");
define("A_LANG_AVS_TEXT_LIBERATE_STUDENT","Alunos com (*) j&aacute; realizaram esta avalia&ccedil;&atilde;o. Caso eles sejam selecionados novamente, a antiga avalia&ccedil;&atilde;o ser&aacute; exclu&iacute;da.");

// ------------- TEXTOS UTILIZADOS EM JAVASCRIPT ---------------------------------------------//
// OBS: lembre-se em Javascript escreva sem htmlentities ( a = &aacute;)
define("A_LANG_AVS_JS_EVALUATION_CONFIRM","Voc� deseja realmente enviar esta avalia��o?");
define("A_LANG_AVS_JS_DELETE_PRESENT_CONFIRM","As quest�es ser�o retiradas desta avalia��o se ela for presencial");
define("A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT","H� quest�es que n�o tiveram seus pesos definidos");
define("A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT2","A soma do peso das quest�es n�o deve ultrapassar a cem porcento (100%)");
define("A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT3","O peso das quest�es deve somar cem porcento (100%)");
define("A_LANG_AVS_JS_DELETE_QUESTION_CONFIRM","Voc� gostaria de excluir esta quest�o?");
define("A_LANG_AVS_JS_ALERT_QUESTION","Os pesos ainda n�o foram salvos");
define("A_LANG_AVS_JS_ALERT_EVALUATION","Voc� deve escolher uma avalia��o");
define("A_LANG_AVS_JS_ALERT_QUESTION_REQUIRE","O campo ");
define("A_LANG_AVS_JS_ALERT_QUESTION_REQUIRE2"," deve ser preenchido!");
define("A_LANG_AVS_JS_ALERT_QUESTION_LACUNA_DELETE","Voc� gostaria de excluir esta lacuna?");
define("A_LANG_AVS_JS_ALERT_QUESTION_ME_CORRECT","Nenhuma alternativa foi escolhida como correta");
define("A_LANG_AVS_JS_ALERT_QUESTION_ME_REQUIRE","A pergunta n�o foi preenchida");
define("A_LANG_AVS_JS_ALERT_QUESTION_ME_DELETE","Voc� gostaria de excluir esta alternativa?");
define("A_LANG_AVS_JS_ALERT_QUESTION_VF_REQUIRE_CORRECT","Deve ser escolhida uma das op��es para ser a correta");
define("A_LANG_AVS_JS_ALERT_STUDENTS"," necess�rio que seja escolhido os alunos");
define("A_LANG_AVS_JS_ALERT_EVALUATION_DELETE_CONFIRM","Voc� gostaria de excluir esta avali��o?");
// ------------- FIM DO JAVASCRIPT ------------------------------------------------------------//
define("A_LANG_AVS_FORMAT_DATE","%d/%m/%Y"); // string da funcao date para modificar o estilo da data
define("A_LANG_AVS_FORMAT_DATE_TIME","%d/%m/%Y %H:%M:%S"); // string da funcao date para modificar o estilo da data

/**********************************************************************************************/
/*********************************** FIM DA AVALIACAO **************************************/

/******************************************************************************************** 
 * Mural de Recados (Definido por Laisi Corsani)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Estagio Curricular.
 * Orientadora: Isabela Gasparini
 * Supervisora: Avanilde Kemczinski
 */ 
define("A_LANG_MURAL_DISC","Mural de Recados da disciplina "); 
define("A_LANG_MURAL_ENVIARECADO_DISC","Enviar recado na disciplina"); 
define("A_LANG_MURAL_ENVIARECADO","Enviar Recado");
define("A_LANG_MURAL_ESCREVERECADO","Escrever Recado");
define("A_LANG_MURAL_OUTRORECADO","Escrever outro Recado");
define("A_LANG_MURAL_ASS", "ASSUNTO:");
define("A_LANG_MURAL_REM", "REMETENTE:");
define("A_LANG_MURAL_REC","RECADO:");
define("A_LANG_MURAL_DT","DATA:");
define("A_LANG_MURAL_ALUNOS","Alunos:");
define("A_LANG_MURAL_DEST","Destinat�rio:");
define("A_LANG_MURAL_LISTREC","Recados recebidos nos �ltimos sete dias:");
define("A_LANG_MURAL_SENDEMAIL","Enviar e-mail de aviso");
define("A_LANG_MURAL_ASSMIN","Assunto:");
define("A_LANG_MURAL_MSG","Mensagem:");
define("A_LANG_MURAL_PROF","Professor");
define("A_LANG_MURAL_GROUP","Grupo");
define("A_LANG_REC_SUSS","Recado enviado com sucesso!");
define("A_LANG_REC_INSUSS","N�o foi poss�vel enviar o recado!");
define("A_LANG_REC_INSUSS_ALL","N�o foi poss�vel enviar o recado a todos os destinat�rios!");
define("A_LANG_REC_ALERT_DEST","Selecione um destinat�rio!");
define("A_LANG_REC_ALERT_MSG","N�o � poss�vel enviar um recado sem conte�do!");
define("A_LANG_REC_ALERT_ALL","N�o foi poss�vel enviar seu recado! Para enviar um recado voc� precisa selecionar um destinat�rio e escrever uma mensagem!");
define("A_LANG_MURAL_BACK","Voltar ao Mural de Recados");
define('A_LANG_LENVIRONMENT_MURAL', 'Mural de Recados');

/*********************************** FIM MURAL RECADOS **************************************/

/********************************************************************************************
 * Altera��es para Usabilidade (Definido por Carla Cristina Lui Dias)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Estagio Curricular.
 * Orientadora: Isabela Gasparini
 * Supervisora: Avanilde Kemczinski
 */

define("A_LANG_SYSTEM_NAME_DESC","ADAPTWEB - Ambiente de Ensino-Aprendizagem Adaptativo na Web"); 
define("A_LANG_SYSTEM_WELCOME","Seja Bem-Vindo ao");  
define("A_LANG_INTRO_DESC_1","Este ambiente possibilita ao professor disponibilizar materiais para seus alunos.");//calra
define("A_LANG_INTRO_DESC_2","Para navegar pelo ambiente, o professor deve inicialmente efetuar o seu cadastro de usu�rio no item <u>Novo Usu�rio</u> presente no menu de op��es. Ap�s efetuado o cadastro, o professor deve efetuar o seu login no item <u>Login</u> do menu de op��es. Se o professor j� possuir um cadastro, deve apenas efetuar o seu login.");//calra
define("A_LANG_INTRO_DESC_3","Quando o login � realizado, o menu de op��es � modificado e o professor ao clicar no item <u>Ambiente Professor</u> poder� interagir com o ambiente para criar suas disciplinas. Ao clicar neste item, aparecer� um submenu com o t�tulo <b>Ambiente Professor</b> ao final do menu principal, assim o professor poder� solicitar e assistir a(s) disciplina(s) disponibilizada(s) por professores ou por ele mesmo, poder� criar disciplinas, liberar alunos, fazer an�lise de log, aplicar avalia��o, entre outros.");//calra
define("A_LANG_INTRO_DESC_4","Este ambiente possibilita ao aluno ter acesso ao(s) material(is) instrucional(is) divulgado(s) pelo(s) seu (s) professor(es) de acordo com o seu perfil. <p align=justify>Para navegar pelo ambiente, o aluno deve inicialmente efetuar o seu cadastro de usu�rio no item <u>Novo Usu�rio</u> presente no menu de op��es. Ap�s efetuado o cadastro, o aluno deve efetuar o seu login no item <u>Login</u> do menu de op��es. Se o aluno j� possuir um cadastro, deve apenas efetuar o seu login."); //carla
define("A_LANG_INTRO_DESC_5","Quando o login � realizado, o menu de op��es � modificado e o aluno deve clicar no item <u>Ambiente Aluno </u>. Ao clicar neste item, aparecer� um submenu com o t�tulo <b>Ambiente Aluno</b> ao final do menu principal, assim o aluno poder� solicitar e assistir a(s) disciplina(s) disponibilizada(s) pelo(s) seu (s) professor(es).");//Carla
// Itens de Autoria
define("A_LANG_AUTHOR","Ambiente de Autoria");//carla 
define("A_LANG_STUDENT", "Ambiente Aluno"); //carla para n_entrada_navegacao  
define("A_LANG_AUTHOR_INTRO_1","O m�dulo de autoria permite ao autor disponibilizar conte�dos adaptados a diferentes perfis de alunos. Atrav�s do processo de autoria, o autor poder� disponibilizar o conte�do de suas aulas em uma �nica estrutura adaptada para os diferentes cursos. Nesta etapa o autor deve cadastrar a disciplina e para quais cursos deseja disponibiliz�-la.
");//carla           
define("A_LANG_AUTHOR_INTRO_2","Ap�s estes cadastramentos devem ser inseridos os arquivos de conceito do conte�do, relacionando-os com cada t�pico da disciplina. Al�m disto o autor deve informar a descri��o do t�pico, seu pr�-requisito e para quais cursos deseja disponibilizar o conte�do. O autor poder� inserir exerc�cios, exemplos e materiais adicionais para cada t�pico."); //Carla

// Estudantes
define("A_LANG_INTRO_STUD_DESC","Este ambiente possibilita ao aluno ter acesso ao(s) material(is) instrucional(is) divulgado(s) pelo(s) seu(s) professor(es) de acordo com o seu perfil. 
<p align=justify>Para ter acesso a(s) sua(s) disciplina(s), o aluno deve inicialmente clicar no item 
<u>Ambiente Aluno</u> do menu de op��es. Ao clicar neste item aparecer� um
 sub - menu com o t�tulo <b>Ambiente Aluno</b> no final do menu principal, assim o usu�rio deve clicar 
 no item <u>Solicitar Matr�cula</u> no sub - menu para solicitar curso o qual est� relacionado com as 
 disciplinas que ele deseja solicitar.
<p align=justify>Ap�s a solicita��o do curso o usu�rio dever� marcar a disciplina que ele deseja solicitar 
ao professor e clicar no bot�o solicitar. Depois de solicitada a disciplina o usu�rio deve aguardar
a libera��o do professor para que ele possa assistir a disciplina solicitada.");//Carla
define("A_LANG_REQUEST_ACCESS_MSG11","O campo E-mail foi preenchido com valor  inv�lido");//modificado por Carla, inseri * em Mar�o de 2009

/*********************************** FIM USABILIDADE **************************************/

/******************************************************************************************** 
 * F�rum de Discuss�o (Definido por Claudia da Rosa)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como TCC - Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora Avanilde Kemczinski
 */
 
define("A_LANG_TOPICO_INSUSS","N�o foi poss�vel criar o t�pico!"); 
define("A_LANG_RESPOSTA_INSUSS","N�o foi poss�vel responder ao t�pico!"); 
define("A_LANG_TOPICO_SUSS","T�pico criado com sucesso!");
define("A_LANG_RESPOSTA_SUSS","Resposta realizada com sucesso!");
define("A_LANG_TOP_ALERT_TITULO","N�o foi poss�vel criar seu t�pico! Digite um t�tulo!");
define("A_LANG_TOP_ALERT_DESCR","N�o foi poss�vel criar seu t�pico! Digite a sua descri��o!");
define("A_LANG_TOP_ALERT_RESP_DESCR","N�o foi poss�vel responder o t�pico! Digite a sua descri��o!");
define("A_LANG_TOP_ALERT_AVAL","Selecione uma das op��es de avalia��o!");
define("A_LANG_FORUM_OUTRO_TOPICO","Criar outro T�pico");
define("A_LANG_TOP_ALERT_ALL","N�o foi poss�vel criar seu t�pico! Digite o t�tulo e sua descri��o!"); 
define("A_LANG_TOP_ALERT_ALL_PROF","N�o foi poss�vel criar seu t�pico! Digite o t�tulo, sua descri��o e selecione a op��o avalia��o!"); 
define("A_LANG_TOP_ALERT_ALL_AVAL_DESCR","N�o foi poss�vel criar seu t�pico! Digite a sua descri��o e selecione a op��o avalia��o!"); 
define("A_LANG_TOP_ALERT_ALL_AVAL_TITULO","N�o foi poss�vel criar seu t�pico! Digite o t�tulo e selecione a op��o avalia��o!"); 
define("A_LANG_FORUM_DISC","F�rum de Discuss�o da disciplina "); 
define('A_LANG_LENVIRONMENT_FORUM', 'F�rum de Discuss�o');
define("A_LANG_FORUM_BACK","Voltar ao F�rum de Discuss�o");
define("A_LANG_TOPICO_ESCOLHIDO_BACK","Voltar ao T�pico Escolhido");
define("A_LANG_FORUM_LISTTOP","T�picos criados nos �ltimos dias:");
define("A_LANG_FORUM_TITULO","T�tulo: ");
define("A_LANG_FORUM_TITULO_TOP","T�tulo do T�pico: ");
define("A_LANG_FORUM_DESCR","Descri��o:");
define("A_LANG_FORUM_CRIARTOPICODISCUSSAO","Criar T�pico de Discuss�o");
define("A_LANG_FORUM_RESPONDERTOPICO","Responder T�pico");
define("A_LANG_FORUM_AVAL","Avalia��o:");
define("A_LANG_FORUM_SIM","Sim");
define("A_LANG_FORUM_NAO","N�o");
define("A_LANG_FORUM_QUAL_INS","Insuficiente");
define("A_LANG_FORUM_QUAL_REG","Regular");
define("A_LANG_FORUM_QUAL_BOM","Bom");
define("A_LANG_FORUM_QUAL_EXC","Excelente");
define("A_LANG_FORUM_AVAL_SUSS","Avalia��o da(s) participa��o(�es) do aluno realizada com sucesso!");
define("A_LANG_FORUM_AVAL_INSUSS","N�o foi poss�vel fazer a avalia��o das participa��es!");
define("A_LANG_FORUM_LIST_ALUN_MAT","Alunos Matriculados:");
define("A_LANG_FORUM_AVAL_ALUN_BACK","Voltar para Avaliar Alunos");
define("A_LANG_FORUM_ALUNO","Aluno(a)");
define("A_LANG_FORUM_AVAL_PARTIC","N�o h� participa��es de alunos para este t�pico!");
define("A_LANG_FORUM_AVAL_OBS","*Observa��o: Para realizar a avalia��o formativa dos alunos voc� deve primeiro avaliar os t�picos de discuss�o individualmente, atrav�s do link <u> Sim </u> na coluna \"Avalia��o\"");

/*********************************** FIM FORUM DISCUSSAO **************************************/

/******************************************************************************************** 
 * Altera��es para Usabilidade (Definido por Carla Cristina Lui Dias)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Estagio Curricular.
 * Orientadora: Isabela Gasparini
 * Supervisora: Avanilde Kemczinski
 */
define("A_LANG_EMAIL_ERRO","A mensagem de aviso de email n�o foi enviada para todos os destinat�rios!<br><br>Existem emails inv�lidos na lista.");
/*********************************** FIM ERRO EMAIL **************************************/

/*************************************************** IN�CIO AGENDA **********************************************/
 /* Agenda (Autora: Carina Tissa Aihara)
  * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
  * Como Estagio Curricular.
  * Orientadora: Avanilde Kemczinski
  * Supervisora: Isabela Gasparini
  */

define("A_LANG_AGENDA_DEMO","Demonstra��o do Ambiente AdaptWeb - N�o � permitido inser��es, altera��es ou exclus�es!");
define("A_LANG_AGENDA_BUSCA","Busca: ");
define("A_LANG_AGENDA_OK","OK");
define("A_LANG_AGENDA_VISOES_CALENDARIO","Vis�es do Calend�rio: ");
define("A_LANG_AGENDA_DOMINGO","Dom");
define("A_LANG_AGENDA_SEGUNDA","Seg");
define("A_LANG_AGENDA_TERCA","Ter");
define("A_LANG_AGENDA_QUARTA","Qua");
define("A_LANG_AGENDA_QUINTA","Qui");
define("A_LANG_AGENDA_SEXTA","Sex");
define("A_LANG_AGENDA_SABADO","S�b");
define("A_LANG_AGENDA_NOVO_EVENTO","Novo Evento");
define("A_LANG_AGENDA_AUTOR_EVENTO","Autor do Evento");
define("A_LANG_AGENDA_EMAIL","E-mail do Autor");
define("A_LANG_AGENDA_TIPO_EVENTO","Tipo do Evento");
define("A_LANG_AGENDA_SELECIONA_TIPO_EVENTO","Selecione o tipo do evento");
define("A_LANG_AGENDA_TIPO_ASSUNTO","Tipo do Assunto (R�tulo)");
define("A_LANG_AGENDA_SELECIONA_TIPO_ASSUNTO","Selecione o tipo do assunto do evento");
define("A_LANG_AGENDA_PARTICULAR","Particular");
define("A_LANG_AGENDA_DISCIPLINA","Disciplina");
define("A_LANG_AGENDA_CURSO","Curso");
define("A_LANG_AGENDA_DISCIPLINA_CURSO","Disciplina/Curso");
define("A_LANG_AGENDA_AMBIENTE_ADAPTWEB","Ambiente AdaptWeb");
define("A_LANG_AGENDA_DISCIPLINA_E_CURSO_NAO_CADASTRADO","N�o h� nenhuma disciplina e nenhum curso cadastrado!");
define("A_LANG_AGENDA_CURSO_NAO_CADASTRADO","N�o h� nenhum curso cadastrado ou matriculado!");
define("A_LANG_AGENDA_DISCIPLINA_NAO_CADASTRADA","N�o h� nenhuma disciplina cadastrada!");
define("A_LANG_AGENDA_DISCIPLINA_CURSO_NAO_RELACIONADO","N�o h� nenhuma disciplina relacionada a um curso ou n�o h� nenhuma disciplina/curso matriculada!");
define("A_LANG_AGENDA_ASSUNTO","Assunto");
define("A_LANG_AGENDA_SELECIONA_ASSUNTO","Selecione o assunto do evento");
define("A_LANG_AGENDA_NOME_EVENTO","Nome do Evento");
define("A_LANG_AGENDA_DESCRICAO","Descri��o");
define("A_LANG_AGENDA_ARQUIVO","Arquivo");
define("A_LANG_AGENDA_CADASTRAR","Cadastrar");
define("A_LANG_AGENDA_CANCELAR","Cancelar");
define("A_LANG_AGENDA_OBS_CAMPO_INCOMPLETO","Observa��o: Todos os campos com * devem ser preenchidos.");
define("A_LANG_AGENDA_ERRO_ENVIO_DADOS","N�o foi poss�vel enviar os dados!Por favor, tente novamente!");
define("A_LANG_AGENDA_VOLTAR","Voltar");
define("A_LANG_AGENDA_CADASTRO_SUCESSO","O cadastro foi efetuado com sucesso!");
define("A_LANG_AGENDA_AGENDAMENTO","Agendamento");
define("A_LANG_AGENDA_CADASTRAR_NOVO_EVENTO","Cadastrar Novo Evento");
define("A_LANG_AGENDA_VOLTAR_AGENDA","Voltar para agenda");
define("A_LANG_AGENDA_ERRO_UM_CAMPO","O campo ");
define("A_LANG_AGENDA_CADASTRO_ERRO_UM_NOME_AUTOR","nome do autor");
define("A_LANG_AGENDA_CADASTRO_ERRO_UM_TIPO_EVENTO","tipo do evento");
define("A_LANG_AGENDA_CADASTRO_ERRO_UM_TIPO_ASSUNTO","tipo do assunto");
define("A_LANG_AGENDA_CADASTRO_ERRO_UM_ASSUNTO","assunto");
define("A_LANG_AGENDA_CADASTRO_ERRO_UM_NOME_EVENTO","nome do evento");
define("A_LANG_AGENDA_ERRO_UM"," n�o foi preenchido!Por favor, preencha!");
define("A_LANG_AGENDA_ERRO_VARIOS","Os campos a seguir n�o foram preenchidos!Por favor, preencha-os!");
define("A_LANG_AGENDA_CADASTRO_ERRO_VARIOS_NOME_AUTOR","- Nome do autor");
define("A_LANG_AGENDA_CADASTRO_ERRO_VARIOS_TIPO_EVENTO","- Tipo do evento");
define("A_LANG_AGENDA_CADASTRO_ERRO_VARIOS_TIPO_ASSUNTO","- Tipo do assunto");
define("A_LANG_AGENDA_CADASTRO_ERRO_VARIOS_ASSUNTO","- Assunto");
define("A_LANG_AGENDA_CADASTRO_ERRO_VARIOS_NOME_EVENTO","- Nome do evento");
define("A_LANG_AGENDA_SEM_EVENTO","N�o h� nenhum evento cadastrado!Por favor, cadastre um evento!");
define("A_LANG_AGENDA_SELECIONAR_NOME_EVENTO_AGENDAMENTO","Selecione o nome do evento que deseja agendar: ");
define("A_LANG_AGENDA_SELECIONA_NOME_EVENTO","Selecione o nome do evento");
define("A_LANG_AGENDA_EVENTO_SEM_AGENDAMENTO","N�o h� nenhum agendamento para este evento!");
define("A_LANG_AGENDA_NAO","N�o");
define("A_LANG_AGENDA_SIM","Sim");
define("A_LANG_AGENDA_SELECIONAR_AGENDAMENTO","Selecione o agendamento");
define("A_LANG_AGENDA_SELECIONA_AGENDAMENTO","Selecione o agendamento do evento");
define("A_LANG_AGENDA_NOVO_AGENDAMENTO","Novo Agendamento");
define("A_LANG_AGENDA_DESTINATARIO","Destinat�rio");
define("A_LANG_AGENDA_EVENTO","Evento");
define("A_LANG_AGENDA_DESTINO_EVENTO","Destino do Evento");
define("A_LANG_AGENDA_DATA_EVENTO","Data do Evento");
define("A_LANG_AGENDA_HORA_INICIO","Hor�rio de In�cio (hh:mm - 00:00 ~ 23:59)");
define("A_LANG_AGENDA_HORA_FIM","Hor�rio de Fim (hh:mm - 00:00 ~ 23:59)");
define("A_LANG_AGENDA_TIPO_GRUPO_DESTINATARIO","Tipo do Grupo do Destinat�rio");
define("A_LANG_AGENDA_SELECIONA_TIPO_GRUPO_DESTINATARIO","Selecione o tipo do grupo do destinat�rio");
define("A_LANG_AGENDA_GRUPO_DESTINATARIO","Grupo do Destinat�rio");
define("A_LANG_AGENDA_SELECIONA_GRUPO_DESTINATARIO","Selecione o grupo do destinat�rio");
define("A_LANG_AGENDA_ERRO_TIPO_E_GRUPO_DESTINATARIO","Os campos tipo do grupo destinat�rio e grupo do destinat�rio n�o foram preenchido!Por favor, preencha-os!");
define("A_LANG_AGENDA_ERRO_GRUPO_DESTINATARIO","O campo grupo do destinat�rio n�o foi preenchido!Por favor, preencha!");
define("A_LANG_AGENDA_ESCOLHER_DESTINATARIOS","Escolher Destinat�rios");
define("A_LANG_AGENDA_DESTINATARIO_SUCESSO","O evento foi destinado com sucesso!");
define("A_LANG_AGENDA_DESTINAR_OUTRO_TIPO_GRUPO_DESTINATARIO","Deseja destinar esse mesmo evento, agendamento a outro tipo de grupo de usu�rios?");
define("A_LANG_AGENDA_MINHA_AGENDA","Minha Agenda");
define("A_LANG_AGENDA_GRUPO_USUARIOS","Grupo de Usu�rios");
define("A_LANG_AGENDA_SELECIONA_DESTINO_EVENTO","Selecione o destino do evento");
define("A_LANG_AGENDA_EXISTE_AGENDAMENTO_IGUAL","Este agendamento j� foi feito!");
define("A_LANG_AGENDA_AGENDAMENTO_SUCESSO","O agendamento foi efetuado com sucesso!");
define("A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_DESTINO_EVENTO","destino do evento");
define("A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_DATA_EVENTO","data do evento");
define("A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_HORA_INICIO","hor�rio de in�cio");
define("A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_HORA_FIM","hor�rio de fim");
define("A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_DESTINO_EVENTO","- Destino do evento");
define("A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_DATA_EVENTO","- Data do evento");
define("A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_HORA_INICIO","- Hor�rio de in�cio");
define("A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_HORA_FIM","- Hor�rio de fim");
define("A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_TIPO_GRUPO_DESTINATARIO","tipo do grupo do destinat�rio");
define("A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_TIPO_GRUPO_DESTINATARIO","- Tipo do grupo do destinat�rio");
define("A_LANG_AGENDA_AGENDAMENTO_ERRO_UM_GRUPO_DESTINATARIO","grupo do destinat�rio");
define("A_LANG_AGENDA_AGENDAMENTO_ERRO_VARIOS_GRUPO_DESTINATARIO","- Grupo do destinat�rio");
define("A_LANG_AGENDA_AGENDAR","Agendar");
define("A_LANG_AGENDA_AGENDAMENTO_DESTINATARIO_SUCESSO","O agendamento foi efetuado e destinado com sucesso!");
define("A_LANG_AGENDA_USUARIOS_GRUPO","Usu�rios do Grupo");
define("A_LANG_AGENDA_NENHUM_USUARIO_DISCIPLINA","N�o h� nenhum usu�rio matriculado nesta disciplina!");
define("A_LANG_AGENDA_NENHUM_USUARIO_CURSO","N�o h� nenhum usu�rio matriculado neste curso!");
define("A_LANG_AGENDA_NENHUM_USUARIO_DISCIPLINA_CURSO","N�o h� nenhum usu�rio matriculado nesta disciplina/curso!");
define("A_LANG_AGENDA_MUDAR_GRUPO_DESTINATARIO","Mudar o Grupo do Destinat�rio");
define("A_LANG_AGENDA_NENHUM_ALUNO_ADAPTWEB","N�o h� nenhum usu�rio do tipo aluno no AdaptWeb!");
define("A_LANG_AGENDA_NENHUM_PROFESSOR_ADAPTWEB","N�o h� nenhum usu�rio do tipo professor no AdaptWeb!");
define("A_LANG_AGENDA_NENHUM_USUARIO_ADAPTWEB","N�o h� nenhum usu�rio do tipo aluno e professor no AdaptWeb!");
define("A_LANG_AGENDA_SELECIONAR_USUARIO_GRUPO","*Para selecionar mais de um usu�rio ou retirar a sele��o, mantenha a tecla Ctrl pressionada.");
define("A_LANG_AGENDA_SELECIONAR_TODOS_USUARIOS","Selecionar Todos Usu�rios");
define("A_LANG_AGENDA_ENVIAR_DESTINATARIOS","Enviar");
define("A_LANG_AGENDA_DESTINATARIOS","Destinat�rios");
define("A_LANG_AGENDA_ALUNOS_ADAPTWEB","Alunos do AdaptWeb");
define("A_LANG_AGENDA_PROFESSORES_ADAPTWEB","Professores do AdaptWeb");
define("A_LANG_AGENDA_TODOS_USUARIOS_ADAPTWEB","Todo os usu�rios do AdaptWeb");
define("A_LANG_AGENDA_DESTINATARIO_RECEBEU_ESTE_EVENTO","O destinat�rio a seguir j� recebeu este evento!Por favor, desmarque-o!");
define("A_LANG_AGENDA_DESTINATARIOS_RECEBERAM_ESTE_EVENTO","Os destinat�rios a seguir j� receberam este evento!Por favor, desmarque-os!");
define("A_LANG_AGENDA_ROOT_RECEBEU_ESTE_EVENTO","Este evento j� foi enviado ao root!Deseja escolher outro destinat�rio?");
define("A_LANG_AGENDA_NENHUM_EVENTO_DIA","N�o h� nenhum evento neste dia!");
define("A_LANG_AGENDA_ALTERAR_AGENDAMENTO","Alterar Agendamento");
define("A_LANG_AGENDA_EXCLUIR","Excluir");
define("A_LANG_AGENDA_ALTERAR_EVENTO","Alterar Evento");
define("A_LANG_AGENDA_ALTERAR","Alterar");
define("A_LANG_AGENDA_DATA_RECEBIMENTO","Data e hora do Recebimento");
define("A_LANG_AGENDA_DATA_ULTIMA_ATUALIZACAO","Data e hora da �ltima atualiza��o");
define("A_LANG_AGENDA_SELECIONAR_TIPO_VISAO","Selecione o tipo da vis�o");
define("A_LANG_AGENDA_SELECIONAR_VISAO","Selecione a vis�o");
define("A_LANG_AGENDA_TIPO_VISAO","Tipo da vis�o");
define("A_LANG_AGENDA_PESQUISA_PERIODO","Pesquisa por Per�odo");
define("A_LANG_AGENDA_PESQUISAR","Pesquisar");
define("A_LANG_AGENDA_VISAO_DESEJADA","Escolha a vis�o desejada");
define("A_LANG_AGENDA_PERIODO_PESQUISA","Escolha a data inicial e final que deseja pesquisar: ");
define("A_LANG_AGENDA_ATE","at�");
define("A_LANG_AGENDA_AGENDAR_CADASTRO","Agendar Cadastro");
define("A_LANG_AGENDA_TODAS_VISOES","Todas as vis�es");
define("A_LANG_AGENDA_DATA_INVALIDA","Data inv�lida!Por favor, digite outra data!");
define("A_LANG_AGENDA_PER�ODO_INVALIDO","Per�odo inv�lido!Por favor, digite outro per�odo!");
define("A_LANG_AGENDA_NENHUM_EVENTO_PERIODO","N�o h� nenhum evento neste per�odo!");
define("A_LANG_AGENDA_DEST_EXCLUIDO_SUCESSO","Destinat�rio exclu�do com sucesso!");
define("A_LANG_AGENDA_EXCLUIR_EVENTO","Excluir Evento");
define("A_LANG_AGENDA_EXCLUIR","Excluir");
define("A_LANG_AGENDA_EXCLUIR_EVENTO_SUCESSO","O evento foi exclu�do com sucesso!");
define("A_LANG_AGENDA_EXCLUIR_AGENDAMENTO","Excluir Agendamento");
define("A_LANG_AGENDA_ATENCAO_EXCLUIR_AGENDAMENTO","ATEN��O: Ao excluir o agendamento todos destinat�rios deste evento tamb�m ser�o exclu�dos!");
define("A_LANG_AGENDA_ERRO_HORARIO_INICIO","Hor�rio de in�cio inv�lido! Por favor, digite outro hor�rio!");
define("A_LANG_AGENDA_ERRO_HORARIO_FIM","Hor�rio de fim inv�lido! Por favor, digite outro hor�rio!");
define("A_LANG_AGENDA_ERRO_DATA","Data inv�lida!Para fazer agendamentos a data escolhida tem que ser maior ou igual a data de hoje!");
define("A_LANG_AGENDA_ERRO_HORARIO","Hor�rio de evento inv�lido!O hor�rio de in�cio deve ser menor que o hor�rio de fim!");
define("A_LANG_AGENDA_NOME_TIPO_EVENTO_OUTROS","Nome do Tipo do Evento Desejado");
define("A_LANG_AGENDA_LOCAL_EVENTO","Local do Evento");
define("A_LANG_AGENDA_REPETICAO_EVENTO","Repeti��o do Evento");
define("A_LANG_AGENDA_NAO_REPETIR","N�o repetir");
define("A_LANG_AGENDA_DIARIAMENTE","Diariamente");
define("A_LANG_AGENDA_SEMANALMENTE","Semanalmente");
define("A_LANG_AGENDA_MENSALMENTE","Mensalmente");
define("A_LANG_AGENDA_ANUALMENTE","Anualmente");
define("A_LANG_AGENDA_QUANTIDADE_REPETICOES","Quantidade de Repeti��es");
define("A_LANG_AGENDA_DATA_REPETI��O","Data da repeti��o");
define("A_LANG_AGENDA_OU","ou");
define("A_LANG_AGENDA_1X","1 vez");
define("A_LANG_AGENDA_2X","2 vezes");
define("A_LANG_AGENDA_3X","3 vezes");
define("A_LANG_AGENDA_4X","4 vezes");
define("A_LANG_AGENDA_5X","5 vezes");
define("A_LANG_AGENDA_6X","6 vezes");
define("A_LANG_AGENDA_7X","7 vezes");
define("A_LANG_AGENDA_8X","8 vezes");
define("A_LANG_AGENDA_9X","9 vezes");
define("A_LANG_AGENDA_10X","10 vezes");
define("A_LANG_AGENDA_AVISO_EMAIL","Envio de aviso por e-mail");
define("A_LANG_AGENDA_NAO_ENVIAR","N�o enviar");
define("A_LANG_AGENDA_AVISO_UM_DIA","Um dia antes");
define("A_LANG_AGENDA_AVISO_UMA_SEMANA","Uma semana antes");
define("A_LANG_AGENDA_AVISO_UM_MES","Um m�s antes");
define("A_LANG_AGENDA_DATA_ENVIO_AVISO","Data do envio do aviso");
define("A_LANG_AGENDA_TIPO_EVENTO_OUTROS","Tipo do Evento Outros");
define("A_LANG_AGENDA_NAO_SELECIONADO_NENHUM_USUARIO","N�o foi selecionado nenhum usu�rio! Por favor selecione!");
define("A_LANG_AGENDA_EVENTO_DELETADO","O evento foi exclu�do!");
define("A_LANG_AGENDA_ROOT","Root");
define("A_LANG_AGENDA_AGENDAMENTO_MESMO_EVENTO","Agendamento com o mesmo evento");
define("A_LANG_AGENDA_DATA_CRIACAO_AGENDAMENTO","Data da Cria��o do Agendamento");
define("A_LANG_AGENDA_DATA_CRIACAO_AGENDAMENTO","Data da Atualiza��o do Agendamento");
define("A_LANG_AGENDA_DATA_CRIACAO_EVENTO","Data da Cria��o do Evento");
define("A_LANG_AGENDA_HORA_HH_MM","Formato obrigat�rio do hor�rio hh:mm");
define("A_LANG_AGENDA_HORA_INVALIDO","Hor�rio Inv�lido!");
define("A_LANG_AGENDA_HORA_INICIAL","Hor�rio de In�cio");
define("A_LANG_AGENDA_HORA_FINAL","Hor�rio de Fim");
define("A_LANG_AGENDA_GERENCIAMENTO_EVENTO","Eventos");
define("A_LANG_AGENDA_EVENTOS","Eventos");
define("A_LANG_AGENDA_NUMERO","N�");
define("A_LANG_AGENDA_NUMERO_AGENDAMENTOS","N� de Agendamentos");
define("A_LANG_AGENDA_VISUALIZAR","Visualizar");
define("A_LANG_AGENDA_ACOES","A��es");
define("A_LANG_AGENDA_DESEJA_REALMENTE_EXCLUIR","Deseja realmente excluir o(s) destinat�rio?");
define("A_LANG_AGENDA_AGENDAMENTO_ALTERADO","Agendamento Alterado");
define("A_LANG_AGENDA_JA_AGENDADO_PARA_ESTE_DESTINATARIO","Este agendamento j� foi feito para o detinat�rio a seguir! Por favor, volte e exclua este destinat�rio, e depois altere o agendamento caso for preciso!");
define("A_LANG_AGENDA_JA_AGENDADO_PARA_ESTES_DESTINATARIOS","Este agendamento j� foi feito para os detinat�rios a seguir! Por favor, volte e exclua estes destinat�rios, e depois altere o agendamento caso for preciso!");
define("A_LANG_AGENDA_VISUALIZAR_EVENTO","Visualizar Evento");
define("A_LANG_AGENDA_EVENTO_SEM_AGENDAMENTO","Evento sem agendamento!");
define("A_LANG_AGENDA_EVENTO_ALTERADO","Evento Alterado");
define("A_LANG_AGENDA_ATENCAO_EXCLUIR_EVENTO","ATEN��O: Ao excluir o evento todos os agendamentos e destinat�rios deste evento tamb�m ser�o exclu�dos!Deseja realmente excluir este evento?");
define("A_LANG_AGENDA_NENHUM_EVENTO","N�o h� nenhum evento cadastrado!");
define("A_LANG_AGENDA_AJUDA","Ajuda Agenda");
define("A_LANG_AGENDA_O_QUE_E","O que � a agenda?");
define("A_LANG_AGENDA_O_QUE_E_RESP","A agenda � um ferramente onde os usu�rios podem agendar eventos para si pr�prio, ou para outros usu�rios, e estes s�o marcados em um calend�rio, promovendo uma melhor organiza��o, controle e gerenciamento dos eventos.");
define("A_LANG_AGENDA_CALENDARIO","Calend�rio");
define("A_LANG_AGENDA_CALENDARIO_DEF","O calend�rio � o meio onde ser�o apresentados todas as datas que possuem agendamentos, e este ser�o representados pela cor vermelha. O dia corrente ser� representado pela cor azul, e caso esta data tenha algum agendamento ficar� roxo.");
define("A_LANG_AGENDA_VISOES_CALEND","Vis�es do Calend�rio");
define("A_LANG_AGENDA__VISOES_CALEND_DEF","Vis�es do calend�rio serve para filtrar os agendamentos apresentados, para que apare�a somente os agendamentos de uma determinada vis�o escolhida. Primeiro � preciso escolher o tipo da vis�o que pode ser Todas as vis�es, Particular, Disciplina, Curso, Disciplina/Curso e Ambiente AdaptWeb. Ao escolher o tipo da vis�o � preciso escolher a vis�o propriamente dita. S� depois de escolhidos os dois o calend�rio ir� fltrar os eventos, caso contr�rio ele mostrar� todas as vis�es existentes. As vis�es oferecidas s�o definidas atrav�s dos assuntos colocados ao cadastrar o evento.");
define("A_LANG_AGENDA_EVEN_AGEND","Eventos Agendados");
define("A_LANG_AGENDA_EVE_AGEND_DEF","Ao clicar am algum dia do calend�rio abrir� uma p�gina mostrando os eventos agendados para este dia. Caso seja o autor do evento aparecer� fun��es para alterar o agendamento e para excluir os destinat�rios, por�m s� poder� ser feita a altera��o e a exclus�o caso a data do agendamento for igual ou maior que a data corrente.");
define("A_LANG_AGENDA_BUSCA1","Busca");
define("A_LANG_AGENDA_BUSCA_DEF","A busca � uma ferramenta onde pode ser digitado qualquer palavra e caso for encontrado algum evento, agendamento que possua a mesma, ele ser� mostrado.");
define("A_LANG_AGENDA_AGENDAMENTO_DEF","O agendamento � onde � cadastrado o evento e o agendamento, e em seguida � destinado ao(s) destinat�rio(s) escolhido(s).");
define("A_LANG_AGENDA_TIPO_EVENTO_DEF","No cadastro do evento � pedido o Tipo do Evento que pode ser um Aviso para a comunica��o de algo; Avalia��o para agendamento de provas; Trabalho para agendamento de entrega de trabalhos; Atividade para agendamento de atividades, a��es a serem feitas; Acontecimento para divulga��o de eventos, fatos que ocorrer�o; Anota��o para armazenamento de alguma observa��o, coment�rio; Compromisso para agendar obriga��es; Reuni�o para agendar encontros; Outros para o caso do evento n�o se relacionar com nenhuma das alternativas oferecidas.");
define("A_LANG_AGENDA_TIPO_ASSUNTO_DEF","No cadastro do evento � pedido o Tipo do Assunto, podendo ser Particular para eventos referente ao usu�rio; Disciplina referente a uma determinada disciplina; Curso referente a um determinado curso; Disciplina/Curso referente a uma determinada disciplina de um determinado curso; Ambiente AdaptWeb refetene ao mesmo. Para o tipo do usu�rio aluno n�o � apresentado o tipo do assunto Disciplina, pois o aluno s� tem acesso a uma determinada Disciplina de um determinado Curso.");
define("A_LANG_AGENDA_ASSUNTO_DEF","Para os tipos de assunto Particular e Ambiente AdaptWeb, � aberto um novo espa�o para que seja escrito o assunto desejado referente a cada tipo (Ex.: Assunto Particular - Pagamento), j� para os assuntos Disciplina, Curso, e Disciplina/Curso s�o apresentados os nomes das Disciplinas, Cursos, Disciplina/Curso que o usu�rio esteja vinculado, sendo que � preciso escolher uma delas como o assunto");
define("A_LANG_AGENDA_NOME_EVENTO_DEF","O Nome do Evento � um termo mais espec�fico do assunto, mas ele n�o � obrigat�rio, podendo ser colocando s� o assunto, como sendo um nome para o evento. (Ex.: Nome do Evento - Pagamento da Conta de Luz)");
define("A_LANG_AGENDA_DESCR_EVEN","Descri��o do Evento");
define("A_LANG_AGENDA_DESCR_EVEN_DEF","A Descri��o do Evento � onde � especificado, detalhado o evento.");
define("A_LANG_AGENDA_ARQUIVO_DEF","Arquivo � o lugar onde se pode fazer upload de algum arquivo referente ao evento que est� sendo cadastrado.");
define("A_LANG_AGENDA_DESTINO_EVENTO_DEF","No Destino do Evento � poss�vel escolher a Minha Agenda que agenda somente para o usu�rio autor do evento, e o Grupo de Usu�rios que agenda para o usu�rio autor e para determinados destinat�rios escolhidos.");
define("A_LANG_AGENDA_TIPO_GRUPO_DESTINATARIO_DEF","Os Tipos dos Grupos dos Destinat�rios s�o diferentes para cada tipo de usu�rio, aparecendo somente os tipos vinculados ao tipo do usu�rio.");
define("A_LANG_AGENDA_GRUPO_DESTINATARIO_DEF","O Grupo do Destinat�rio � o grupo escolhido para destinar o agendamento.");
define("A_LANG_AGENDA_USUARIOS_GRUPO_DEF","Os Usu�rios do Grupo s�o os destinat�rios que ser�o escolhido para destinar o agendamento.");
define("A_LANG_AGENDA_DATA_EVENTO_DEF","A Data do Evento precisa ser maior ou igual a data corrente");
define("A_LANG_AGENDA_HORA_INI_EVEN","Hora In�cio do Evento");
define("A_LANG_AGENDA_HORA_INI_EVEN_DEF","Hor�rio do In�cio do Evento, podendo ser das 00:00 �s 23:59. Formato obrigat�rio hh:mm");
define("A_LANG_AGENDA_HORA_FIM_EVEN","Hora Fim do Evento");
define("A_LANG_AGENDA_HORA_INI_EVEN_DEF","Hor�rio do Final do Evento, podendo ser das 00:00 �s 23:59, desde que o Hor�rio Final seja maior ou igual ao Hor�rio Inicial. Formato obrigat�rio hh:mm");
define("A_LANG_AGENDA_LOCAL_EVEN_DEF","Local onde acontecer� o evento");
define("A_LANG_AGENDA_REPETICAO_DEF","Repetir o evento diariamente, mensalmente, anualmente, ou em uma data definida.");
define("A_LANG_AGENDA_AVISO_EMAIL_DEF","Enviar aviso por e-mail do evento um dia antes do evento, uma semana antes do evento, um m�s antes do evento ou em uma data definida.");
define("A_LANG_AGENDA_PESQ_PERIODO_DEF","Para realizar a Pesquisa por Per�odo � preciso digitar a data do in�cio e do fim do per�do desejado, e tamb�m � poss�vel escolher a vis�o que queira mostrar.");
define("A_LANG_AGENDA_GERENC_EVEN_DEF","No Gerenciamento dos Eventos � poss�vel visualizar os eventos cadastrados, alterar, excluir e fazer agendamentos para estes eventos. S� poder� ser alterado e exclu�do os eventos com agendamentos iguais ou maiores a data corrente, se houver um agendamento com data menor que a data corrente e precisar alterar o evento, ser� preciso fazer um novo agendamento criando um novo evento. A exclus�o de um evento excluir� junto todos os agendamento feitos para este evento.");

/***************************************************** FIM AGENDA ************************************************/



/*
 * Ferramenta de An�lise Interativa
 * Inclu�do por Barbara Moissa em 2013-11-02
 */
define('A_LANG_ANA_INT', 'An�lise Interativa');
define('A_LANG_LIN_AJU', 'ajuda/pt-br.php');
define('A_LANG_AJUDA', 'Ajuda');
define('A_LANG_IMPRIMIR', 'Imprimir');
define('A_LANG_CURSO', 'Curso');
define('A_LANG_DISCIPLINA', 'Disciplina');
define('A_LANG_PERIODO', 'Per&iacute;odo');
define('A_LANG_PER_DE', 'de');
define('A_LANG_PER_ATE', 'at�');
define('A_LANG_TOD_OS_CUR', 'Todos os cursos');
define('A_LANG_TOD_AS_DIS', 'Todas as disciplinas');
define('A_LANG_MOS_MET', 'Mostrar m�tricas');
define('A_LANG_OCU_MET', 'Ocultar m�tricas');
define('A_LANG_DES_MET', 'Deselecionar m�tricas');
define('A_LANG_USO_GER', 'Uso Geral');
define('A_LANG_AMB_DE_AULA', 'Ambiente de Aula');
define('A_LANG_FOR_DE_DIS', 'F�rum de Discuss�o');
define('A_LANG_MUR_DE_REC', 'Mural de Recados');
define('A_LANG_TOT_DE_VIS_POR_ALU', 'Total de Visitas por Aluno');
define('A_LANG_TEM_MED_DE_ACE_DOS_ALU', 'Tempo M�dio de Acesso dos Alunos');
define('A_LANG_FRE_DE_ACE', 'Frequ�ncia de Acesso');
define('A_LANG_TOT_DE_ACE_A_CADA_SEC', 'Total de Acessos a Cada Se��o');
define('A_LANG_SIS_OPE', 'Sistemas Operacionais');
define('A_LANG_NAVEGADORES', 'Navegadores');
define('A_LANG_RES_DE_TELA', 'Resolu��es de Tela');
define('A_LANG_MODO_DE_NAV', 'Modo de Navega&ccedil;&atilde;o');
define('A_LANG_TOT_DE_USOS_DO_SIS_DE_BUS', 'Total de Usos do Sistema de Busca');
define('A_LANG_PAL_PES', 'Palavras-chave Pesquisadas');
define('A_LANG_TOT_DE_ACE_AOS_CON', 'Total de Acessos aos Conceitos');
define('A_LANG_TOT_DE_ACE_AOS_EXER', 'Total de Acessos aos Exerc�cios');
define('A_LANG_TOT_DE_ACE_AOS_EXEM', 'Total de Acessos aos Exemplos');
define('A_LANG_TOT_DE_ACE_AOS_MAT_COM', 'Total de Acessos aos Materiais Complementares');
define('A_LANG_TOT_DE_ACE_AOS_TOP', 'Total de Acessos aos T�picos');
define('A_LANG_TOT_DE_TOP_CRI', 'Total de T�picos Criados');
define('A_LANG_TOT_DE_RES', 'Total de Respostas');
define('A_LANG_TOT_DE_REC_ENV', 'Total de Recados Enviados');
define('A_LANG_TIP_DE_REC_ENV', 'Tipos de Recados Enviados');
define('A_LANG_TOT_DE_VIS', 'Total de Visualiza��es');
define('A_LANG_GER_GRA_PARA_ANA', 'Gerar gr�fico para an�lise');
define('A_LANG_FOR_DA_DATA', 'd/m/Y');
define('A_LANG_MET_SEL', 'M�tricas selecionadas');
define('A_LANG_ALUNO', 'Aluno');
define('A_LANG_CATEGORIA', 'Categoria');
define('A_LANG_TOT_DE_ACE', 'Total de Acessos');
define('A_LANG_TEM_TOT_DE_ACE', 'Tempo Total de Acesso');
define('A_LANG_ACESSOS', '%1% acesso(s)');
define('A_LANG_HORAS', '%1% hora(s)');
define('A_LANG_TEM_MED_DE_ACE', 'Tempo M�dio de Acesso');
define('A_LANG_DIA', 'Dia');
define('A_LANG_NUM_DA_SEM', 'N�mero da Semana');
define('A_LANG_SEMANA', 'Semana');
define('A_LANG_MES', 'M�s');
define('A_LANG_SECAO', 'Se&ccedil;&atilde;o');
define('A_LANG_SIS_OPE_SIN', 'Sistema Operacional');
define('A_LANG_NAVEGADOR', 'Navegador');
define('A_LANG_VERSAO', 'Vers�o');
define('A_LANG_RES_DE_TELA_SIN', 'Resolu&ccedil;&atilde;o de Tela');
define('A_LANG_TOT_DE_BUS', 'Total de Buscas');
define('A_LANG_BUSCAS', '%1% busca(s)');
define('A_LANG_PAL_PES_SIN', 'Palavra-chave');
define('A_LANG_CONCEITO', 'Conceito');
define('A_LANG_EXEMPLO', 'Exemplo');
define('A_LANG_EXERCICIO', 'Exerc&iacute;cio');
define('A_LANG_MAT_COM', 'Material Complementar');
define('A_LANG_MAT_COM_SIN', 'Material Complementar');
define('A_LANG_TOPICO', 'T&oacute;pico');
define('A_LANG_RESPOSTAS', '%1% resposta(s)');
define('A_LANG_TIP_DO_REC', 'Tipo do Recado');
define('A_LANG_RECADOS', '%1% recados(s)');
define('A_LANG_GRAFICO', 'Gr�fico');
define('A_LANG_BARRAS', 'Barras');
define('A_LANG_LINHAS', 'Linhas');
define('A_LANG_SETORES', 'Setores');
define('A_LANG_AGR_POR', 'Agrupar por');
define('A_LANG_MOSTRAR', 'Mostrar');
define('A_LANG_LEGENDA', 'Legenda');
define('A_LANG_ROTULO', 'R�tulos');
define('A_LANG_sLengthMenu', 'Mostrar _MENU_ registros por p�gina');
define('A_LANG_sZeroRecords', 'Nenhum registro encontrado');
define('A_LANG_sInfo', '_TOTAL_ registros');
define('A_LANG_sInfoFiltered', '(filtrados de um total de _MAX_ registros)');
define('A_LANG_sSearch', 'Procurar');
define('A_LANG_MODO_LIV', 'Modo Livre');
define('A_LANG_MODO_TUT', 'Modo Tutorial');
define('A_LANG_TOT_DE_ACE_RES', 'Total de Acessos/Respostas');
define('A_LANG_CONCEITOS', 'Conceitos');
define('A_LANG_EXEMPLOS', 'Exemplos');
define('A_LANG_EXERCICIOS', 'Exerc�cios');
define('A_LANG_MAT_COM_PLU', 'Materiais Complementares');
define('A_LANG_TOPICOS', '%1% t�pico(s)');
define('A_LANG_SEM_RES', 'N�o h� resultados! Por favor, altere seus filtros e tente novamente.');
define('A_LANG_GRUPO', 'Grupo');
define('A_LANG_PROFESSOR', 'Professor');
define('A_LANG_TAB_COM_INF_DET', 'Tabela com Informa��es Detalhadas');
define('A_LANG_VOL_AO_INI', 'Voltar ao In�cio');
define('A_LANG_VOL_A_FER', 'Voltar a Ferramenta');
