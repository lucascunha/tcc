<?
        /* -----------------------------------------------------------------------
         *  AdaptWeb - Projeto de Pesquisa
         *     UFRGS - Instituto de Inform�tica
         *       UEL - Departamento de Computa��o
         * -----------------------------------------------------------------------
         *       @package AdaptWeb
         *       @subpakage Linguagem
         *       @file idioma/pt-BR/geral.php
         *       @desciption Arquivo de tradu��o - Portugu�s/Brasil
         *       @since 17/08/2003
         *       @author Veronice de Freitas (veronice@jr.eti.br)
         *       @version fran�aise � Josset M�xime (mjosset@inf.ufrgs.br)
         * -----------------------------------------------------------------------
         */

		 // ************************************************************************
		 // * Interface - ????                                                     *
         // ************************************************************************
        //Dados da interface da ferramenta de autoria
          define("A_LANG_VERSION","Version");
          define("A_LANG_SYSTEM","environnement AdaptWeb");
          define("A_LANG_ATHORING","Ferramenta de Autoria");
          define("A_LANG_USER","Utilisateur");
          define("A_LANG_PASS","Mot de passe");
          define("A_LANG_NOTAUTH","< non identifi�>");
          define("A_LANG_HELP","Aide");
          define("A_LANG_DISCIPLINE","Discipline");
        define("A_LANG_NAVEGATION","O� je suis: ");

// Contexte
// observation - archive p_contexto_navegacao.php
// ************************************************************************
// * Menu *
// ************************************************************************
//Home
define("A_LANG_MNU_HOME","D�but");
define("A_LANG_MNU_NAVIGATION","Environnement �l�ve");
define("A_LANG_MNU_AUTHORING"," Environnement Professeur");
define("A_LANG_MNU_UPDATE_USER","Changer le cadastre");
define("A_LANG_MNU_EXIT","Sortir");
define("A_LANG_MNU_LOGIN","Login");
define("A_LANG_MNU_ABOUT","� propos");
define("A_LANG_MNU_DEMO","D�monstration");
define("A_LANG_MNU_PROJECT","Projet");
define("A_LANG_MNU_FAQ","Questions fr�quentes");
define("A_LANG_MNU_RELEASE_AUTHORING","Lib�rer droits d'auteur");
//??
define("A_LANG_MNU_BACKUP","Copie de sauvegarde");
define("A_LANG_MNU_NEW_USER","Nouvel utilisateur");
define("A_LANG_MNU_APRESENTATION","Pr�sentation");
define("A_LANG_MNU_MY_ACCOUNT","Mon compte");
define("A_LANG_MNU_MANAGE","Gestion");
define("A_LANG_MNU_BAKCUP","Copie de sauvegarde");
define("A_LANG_MNU_GRAPH","Acc�s");
define("A_LANG_MNU_TODO","En d�veloppement");
define("A_LANG_SYSTEM_NAME","AdaptWeb - Environnement d'enseignement - Apprentissage adapt� au Web ");
define("A_LANG_SYSTEM_COPYRIGHT","Copyright � 2001-2003 UFRGS/UEL");
// Autoria
define("A_LANG_MNU_COURSE","Cours");
define("A_LANG_MNU_DISCIPLINES","Discipline");
define("A_LANG_MNU_DISCIPLINES_COURSE","Discipline/Cours");
define("A_LANG_MNU_STRUTURALIZE_TOPICS","Structurer le contenu");
define("A_LANG_MNU_ACCESS_LIBERATE","Analyser la demande de matricule");
//??
define("A_LANG_MNU_LIBERATE_DISCIPLINES","Lib�rer la discipline");
define("A_LANG_MNU_WORKS","Travaux des �tudiants");
// Etudiant
define("A_LANG_MNU_DISCIPLINES_RELEASED","Assister aux cours");
define("A_LANG_MNU_SUBSCRIBE","Soliciter un matricule");
define("A_LANG_MNU_WAIT","En attendant un matricule");
define("A_LANG_MNU_UPLOAD_FILES","Envoyer les travaux");
define("A_LANG_MNU_MESSAGE","Avis");
// ************************************************************************
// * Formul�rio Principal *
// ************************************************************************
// Apresenta��o
define("A_LANG_ORGANS","Institutions participantes");
define("A_LANG_RESEARCHES","Chercheurs");
define("A_LANG_AUTHORS","Auteurs");
// Formul�rio de solicita��o de acesso
define("A_LANG_REQUEST_ACCESS","Demande d'acc�s");
define("A_LANG_REQUEST_ACCESS_USER_TYPE","Type d'utilisateur");
define("A_LANG_REQUEST_ACCESS_TYPE_STUDANT","�tudiant");
define("A_LANG_REQUEST_ACCESS_TYPE_TEACHES","Professeur");
define("A_LANG_REQUEST_ACCESS_NAME","Nom");
define("A_LANG_REQUEST_ACCESS_EMAIL","E-mail");
define("A_LANG_REQUEST_ACCESS_PASS","Mot de passe");
define("A_LANG_REQUEST_ACCESS_PASS2","Confirmation du mot de passe");
define("A_LANG_REQUEST_ACCESS_INSTITUTION_EDUCATION","Institution d'enseignement");
define("A_LANG_REQUEST_ACCESS_COMMENT","Observation"); // Verificar a validade desta linha
define("A_LANG_REQUEST_ACCESS_LANGUAGE","Langue");
define("A_LANG_REQUEST_ACCESS_MSG1","La connexion � la base de donn�es n'a pas pu �tre �tablie");
define("A_LANG_REQUEST_ACCESS_MSG2","Les donn�es n'ont pas pu �tre envoy�es (donnez une autre adresse e-mail)");
define("A_LANG_REQUEST_ACCESS_MSG3","Vos donn�es n'ont pas pu �tre envoy�es ");
define("A_LANG_REQUEST_ACCESS_MSG4","Mot de passe invalide (les champs 'MOT DE PASSE' et 'CONFIRMATION DU MOT DE PASSE' doivent correspondre)");
define("A_LANG_REQUEST_ACCESS_SAVE","Sauver");
//Formulario de login
define("A_LANG_LOGIN2","Login");
define("A_LANG_EMAIL","E-mail");
define("A_LANG_LOGIN_PASS","Mot de passe");
define("A_LANG_LOGIN_START","Entrer");
define("A_LANG_LOGIN_MSG1","Mot de passe invalide");
define("A_LANG_LOGIN_MSG2","E-mail invalide");
define("A_LANG_LOGIN_MSG3","Utilisateur non autoris�");
define("A_LANG_LOGIN_MSG4","Attendez l'autorisation d'acc�s de l'administrateur de l'environnement AdaptWeb");
define("A_LANG_LOGIN_MSG5","La connexion � la base de donn�es n'a pas pu �tre �tablie");
// Formul�rio de cadastro de Curso
define("A_LANG_COURSE_REGISTER","Inscription aux cours");
define("A_LANG_COURSE_RESGISTERED","Cours enregistr�s");
define("A_LANG_COURSE2","Cours");
define("A_LANG_COURSE_INSERT","Inclure");
define("A_LANG_COURSE_UPDATE","Mise � jour");
define("A_LANG_COURSE_DELETE","Supprimer");
define("A_LANG_COURSE_MSG1","La connexion � la base de donn�es n'a pas pu �tre �tablie");
define("A_LANG_COURSE_MSG2","Le cours n'a pu �tre ajout�");
define("A_LANG_COURSE_MSG3","Le cours n'a pu �tre modifi�");
define("A_LANG_COURSE_MSG4","Le cours n'a pu �tre supprim� car celui-ci est li� � une autre discipline");
define("A_LANG_COURSE_MSG5","Le cours n'a pu �tre supprim�");
// Cadastro de disciplina
define("A_LANG_DISCIPLINES_REGISTER","Inscription aux disciplines");
define("A_LANG_DISCIPLINES_REGISTERED","Disciplines enregistr�es");
define("A_LANG_DISCIPLINES2","Disciplines");
define("A_LANG_DISCIPLINES_INSERT","Ajouter");
define("A_LANG_DISCIPLINES_UPDATE","Modifier");
define("A_LANG_DISCIPLINES_DELETE","Supprimer");
define("A_LANG_DISCIPLINES_MSG1","La discipline n'a pu �tre ajout�");
define("A_LANG_DISCIPLINES_MSG2","La discipline n'a pu �tre modifi�");
define("A_LANG_DISCIPLINES_MSG3","La discipline n'a pu �tre supprim�e car celle-ci est li�e � un autre cours");
define("A_LANG_DISCIPLINES_MSG4","La discipline n'a pu �tre supprim�e");
// Formul�rio de cadastro de Disciplina / Curso
define("A_LANG_DISCIPLINES_COURSE1","Discipline/Cours");
define("A_LANG_DISCIPLINES_COURSE_DISCIPLINES","Disciplines");
define("A_LANG_DISCIPLINES_COURSE2","Cours");
define("A_LANG_DISCIPLINES_COURSE_SAVE","Sauver");
define("A_LANG_DISCIPLINES_COURSE_MSG1","Enregistrer la (les) discipline(s) et le (les) cours pour avoir acc�s � cet item");
define("A_LANG_DISCIPLINES_COURSE_MSG2","Il n'a pas �t� possible d'�tablir le lien de la discipline vers les cours � cause de l'�chec de connexion avec la base de donn�es");
define("A_LANG_DISCIPLINES_COURSE_MSG3","Informez votre nom");
define("A_LANG_DISCIPLINES_COURSE_MSG4","Cours existant");
define("A_LANG_DISCIPLINES_COURSE_MSG5","S�lectionnez un cours � modifier");
// Formul�rio de autoriza��o de acesso
define("A_LANG_LIBERATE_USER1","Matricule/�tudiant");
// pour le professeur et l'administrateur
define("A_LANG_LIBERATE_USER2","Autoriser Professeur/Auteur");
// para o adiministrador
define("A_LANG_LIBERATE_MSG1","En d�veloppement...");
// Formul�rio para libera��o da disciplina
define("A_LANG_LIBERATE_DISCIPLINES","Autoriser l'acc�s � la discipline");
define("A_LANG_LIBERATE_DISCIPLINES_MSG1","En d�veloppement...");
// ======================= Topicos ==========================
// Entrada / t�picos
define("A_LANG_ENTRANCE_TOPICS_DISCIPLINES","Discipline");
// **** REVER "Estruturar t�pico"
define("A_LANG_ENTRANCE_TOPICS_STRUTURALIZE","Structurer les sujets");
define("A_LANG_ENTRANCE_TOPICS_MSG1","Il n'a pas �t� possible de lire les disciplines");
define("A_LANG_ENTRANCE_TOPICS_MSG2","Dans ce formulaire seront disponibles seulement les disciplines qui sont li�es � au moins un cours.");
// Guia - Lista de t�picos
define("A_LANG_TOPIC_GENERATE_CONTENT","G�n�rer le contenu");
//orelha
define("A_LANG_TOPIC_LIST","Concepts");
define("A_LANG_TOPIC_MAINTENANCE","Maintenance du concept");
define("A_LANG_TOPIC_EXEMPLES","Exemples");
define("A_LANG_TOPIC_EXERCISES","Exercices");
define("A_LANG_TOPIC_COMPLEMENTARY","Support compl�mentaire");
// Guia - manuten��o do t�pico
define("A_LANG_TOPIC_NUMBER","Num�ro du sujet");
define("A_LANG_TOPIC_DESCRIPTION","Nom du sujet");
define("A_LANG_TOPIC_SUMARIZED_TOPIC_DESCRIPTION","Description resum�e du sujet");
define("A_LANG_TOPIC_WORDS_KEY","Mots-cl�s");
define("A_LANG_TOPIC_PREREQUISIT","Pr�-requis");
define("A_LANG_TOPIC_COURSE","Cours");
define("A_LANG_TOPIC_FILE1","Archive");
define("A_LANG_TOPIC_FILE2","Archive(s)");
define("A_LANG_TOPIC_MAIN_FILE","Archive principale");
define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE","Archive(s) associ�e(s)");
define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_STATUS","Situation");
define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_UPLOAD","");
//??
define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG1","Telecharger");
define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG2","Fichier au serveur");
// bot�es
define("A_LANG_TOPIC_SAVE","Sauver");
define("A_LANG_TOPIC_SEND","Envoyer");
define("A_LANG_TOPIC_SEARCH","Chercher");
define("A_LANG_TOPIC_INSERT_TOPIC_SAME_LEVEL","Ajouter un sujet au m�me niveau");
define("A_LANG_TOPIC_INSERT_SUB_TOPIC","Ajouter un sous-sujet");
define("A_LANG_TOPIC_DELETE","Supprimer");
define("A_LANG_TOPIC_PROMOTE","Promouvoir");
define("A_LANG_TOPIC_LOWER","Abaisser");
// Guia - samples
//define("A_LANG_TOPIC_EXEMPLES_NUMBER_TOPIC","Num�ro du sujet");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Description de l'exemple");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Niveau de complexit�");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Cours");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Supprimer");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Description");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Niveau");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Cours");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Archive");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Archive(s) associ�e(s)");
// bot�es
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Sauver");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Supprimer");
// Exemplos
// N�vel de Complexidade
// Descri��o
// N�vel
// Arquivos Associados
// Descri��o dos Exemplos
// Exerc�cios
// Descri��o do exerc�cio
// Material complementar
// Menu
// Autorizar Acesso
// Liberar disciplinas
// Novo usu�rio
// to modify I register in cadastre (alterar cadastro)
// ************************************************************************
// * Formul�rio - Autoria *
// ************************************************************************
// Disciplinas Liberadas
// Matricula
// Aguardando Matricula
// Autorizar Acesso
// Navega��o
// Tela de Cursos
// ************************************************************************
// * Formul�rio - Demo *
// ************************************************************************
define("A_LANG_DEMO","Discipline de d�monstration de l'environement AdaptWeb - tout ajout ou modification est impossible");
//
// Regional Specific Date texts
//
// A little help for date manipulation, from PHP manual on function strftime():
//
// %a - abbreviated weekday name according to the current locale
// %A - full weekday name according to the current locale
// %b - abbreviated month name according to the current locale
// %B - full month name according to the current locale
// %c - preferred date and time representation for the current locale
// %C - century number (the year divided by 100 and truncated to an integer, range 00 to 99)
// %d - day of the month as a decimal number (range 01 to 31)
// %D - same as %m/%d/%y
// %e - day of the month as a decimal number, a single digit is preceded by a space
// (range ' 1' to '31')
// %h - same as %b
// %H - hour as a decimal number using a 24-hour clock (range 00 to 23)
// %I - hour as a decimal number using a 12-hour clock (range 01 to 12)
// %j - day of the year as a decimal number (range 001 to 366)
// %m - month as a decimal number (range 01 to 12)
// %M - minute as a decimal number
// %n - newline character
// %p - either `am' or `pm' according to the given time value, or the corresponding strings for the current locale
// %r - time in a.m. and p.m. notation
// %R - time in 24 hour notation
// %S - second as a decimal number
// %t - tab character
// %T - current time, equal to %H:%M:%S
// %u - weekday as a decimal number [1,7], with 1 representing Monday
// %U - week number of the current year as a decimal number, starting with the first Sunday as
// the first day of the first week
// %V - The ISO 8601:1988 week number of the current year as a decimal number, range 01 to 53,
// where week 1 is the first week that has at least 4 days in the current year, and with
// Monday as the first day of the week.
// %W - week number of the current year as a decimal number, starting with the first Monday as
// the first day of the first week
// %w - day of the week as a decimal, Sunday being 0
// %x - preferred date representation for the current locale without the time
// %X - preferred time representation for the current locale without the date
// %y - year as a decimal number without a century (range 00 to 99)
// %Y - year as a decimal number including the century
// %Z - time zone or name or abbreviation
// %% - a literal `%' character
//
// Note: A_LANG_DATESTRING is used for Articles and Comments Date
// A_LANG_LINKSDATESTRING is used for Web Links creation Date
// A_LANG_DATESTRING2 is used for Older Articles box on Home
//
define("A_LANG_CODE","pt-BR");
define("A_LANG_CHACTERSET","ISO-8859-1");
define("A_LANG_NAME_pt_BR","Portugais du Br�sil");
define("A_LANG_NAME_en_US","Anglais");
define("A_LANG_NAME_es_ES","Espagnol");
define("A_LANG_NAME_fr_FR","Francais");
define("A_LANG_NAME_ja_JP","Japonais");
define('A_LANG_TIMEZONES','IDLW NT HST YST PST MST CST EST AST GMT-3:30 GMT-3 AT WAT GMT CET EET BT GMT+3:30 GMT+4 GMT+4:30 GMT+5 GMT+5:30 GMT+6 WAST CCT JST ACS GST GMT+11 NZST');
define('A_LANG_TZOFFSETS','0 1 2 3 4 5 6 7 8 8.5 9 10 11 12 13 14 15 15.5 16 16.5 17 17.5 18 19 20 21 21.5 22 23 24');
define('A_LANG_DATEBRIEF','%b %d, %Y');
define('A_LANG_DATELONG','%A, %B %d, %Y');
define('A_LANG_DATESTRING','%A, %B %d @ %H:%M:%S %Z');
define('A_LANG_DATESTRING2','%A, %B %d');
define('A_LANG_DATETIMEBRIEF','%b %d, %Y - %I:%M %p');
define('A_LANG_DATETIMELONG','%A, %B %d, %Y - %I:%M %p %Z');
define('A_LANG_LINKSDATESTRING','%d-%b-%Y');
define('A_LANG_TIMEBRIEF','%I:%M %p');
define('A_LANG_TIMELONG','%I:%M %p %Z');
define('A_LANG_DAY_OF_WEEK_LONG','Dimanche Lundi Mardi Mercredi Jeudi Vendredi Samedi');
define('A_LANG_DAY_OF_WEEK_SHORT','Dim Lun Mar Mer Jeu Ven Sam');
define('A_LANG_MONTH_LONG','Janvier F�vrier Mars Avril Mai Juin Juillet Ao�t Septembre Octobre Novembre D�cembre');
define('A_LANG_MONTH_SHORT','Jan Fev Mar Avr Mai Juin Juil Aou Sept Oct Nov Dec');
//************UFRGS******************
//Index
//About
define('A_LANG_INDEX_ABOUT_INTRO',"L'environnement AdaptWeb est r�solument tourn� vers les travaux et pr�sentations adaptatives des disciplines qui composent les cours d'EAD (Enseignement � Distance) sur le web. L'objectif d'AdaptWeb est de permettre l'ad�quation des formes et techniques de pr�sentation du contenu avec les �l�ves de diff�rents cours universitaires et avec diff�rents styles d'apprentissage. Ainsi, pour chaque cours seront disponibles plusieurs formes de pr�sentation du contenu, en fonction de chacun des cours et des pr�f�rences personnelles des �l�ves participants.
L'environnement AdaptWeb a �t� initialement d�velopp� par des chercheurs de l'UFRGS et de l'UEL, � travers des projets Electra et AdaptWeb, et avec le soutien du CNPq. Pour visiter la page du projet: http://www.inf.ufrgs.br/adapt/adaptweb");
define('A_LANG_INDEX_ABOUT_ENVACCESS',"Acc�s � l'environnement");
define('A_LANG_INDEX_ABOUT_TINFO',"Pour rendre disponible le contenu de vos disciplines, le professeur doit effectuer une demande d'acc�s � l'Environnement d'Auteur");
define('A_LANG_INDEX_ABOUT_LINFO',"Pour avoir acc�s � (aux) discipline(s), l'�l�ve doit s'inscrire dans l'Environnement de Navigation et soliciter un matricule dans la (les) discipline(s) en rapport avec son cours");
//Demo
define('A_LANG_INDEX_DEMO_AENVIRONMENT',"Ambiente de Autoria");
define('A_LANG_INDEX_DEMO_FDESC1', 'Cr�er une ou plusieurs discipline(s)');
define('A_LANG_INDEX_DEMO_NENVIRNMENT','Environnement de Navigation');
define('A_LANG_INDEX_DEMO_FDESC2', 'Assister � une ou plusieurs des disciplines');
define('A_LANG_INDEX_DEMO_TOLOG',"Acc�dez � l'environnement et naviguez au sein de la discipline de d�monstration en vous connectant avec les donn�es ci-dessous:");
define('A_LANG_INDEX_DEMO_LOGIN','Login: demo@inf.ufrgs.br');
define('A_LANG_INDEX_DEMO_PASS', 'Mot de passe: 123');
define('A_LANG_INDEX_DEMO_OBSERVATION','Observation');
define('A_LANG_INDEX_DEMO_OBSERVATIONTEXT', "Une fois connect�, vous aurez acc�s � l'environnement professeur et � l'environnement �l�ve gr�ce aux options 'Environnement professeur' et 'Environnement �l�ve' de la page initiale d'AdaptWeb.");
//Apresenta��o
define('A_LANG_INDEX_PRESENTATION_INFABOUT', 'Informations sur le projet:');
define('A_LANG_DOWNLOAD', 'Download');
define('A_LANG_PUBLICATIONS', 'Publications');
define('A_LANG_INDEX_PRESENTATION_AVAILABLEAT','Disponible en:');
//Ambiente do Aluno - Entrada
//Principal
define('A_LANG_LENVIRONMENT_DESCRIPTION', "Cet environnement permet � l'�l�ve d'avoir acc�s aux supports de cours de son (ses) professeur(s) adapt�s � son profil.");
define('A_LANG_LENVIRONMENT_WARNING', "Pour avoir acc�s � sa (ses) discipline(s), l'�l�ve doit initialement s'inscrire pour pouvoir ensuite se connecter � l'environnement et enfin faire une demande de matricule pour sa (ses) disciplines. C'est seulement apr�s que le professeur responsable l'ait autoris� que l'�l�ve peut acc�der � sa (ses) discpline(s).");
define('A_LANG_LENVIRONMENT_WATCH_LIBERATED', 'Disciplines autoris�es');
define('A_LANG_LENVIRONMENT_WATCH_MINE', "Disciplines dont je suis l'auteur (naviguer par cours)");
define('A_LANG_LENVIRONMENT_WATCH_MY', 'Mes disciplines');
define('A_LANG_LENVIRONMENT_WATCH_NOT', "Il n'y a pas de discipline enregistr�e.");
define('A_LANG_LENVIRONMENT_WATCH_WARNING', 'Faites votre demande de matricule pour avoir acc�s aux disciplines.'); define('A_LANG_LENVIRONMENT_WATCH_VISUAL', 'Visualiser les disciplines de chaque cours');
define('A_LANG_LENVIRONMENT_WATCH_CREATE', "Cr�er des disciplines et g�n�rez le contenu dans l'Environnement Professeur (STRUCTURER LE CONTENU => LISTE DES CONCEPTS) pour avoir acc�s � l'Environnement de Navigation.");
define('A_LANG_LENVIRONMENT_WATCH_OBS', "Pour les disciplines dont vous �tes l'auteur, il n'est pas n�cessaire d'obtenir l'acc�s � celles-ci pour tester l'Environnement de Navigation.");
define('A_LANG_LENVIRONMENT_REQUEST', 'Faire une demande de matricule');
define('A_LANG_LENVIRONMENT_REQUEST_REQ', 'Demander');
define('A_LANG_LENVIRONMENT_REQUEST_NOTC', "Il n'y a pas de cours enregistr�");
define('A_LANG_LENVIRONMENT_REQUEST_NOTD', "Il n'y a pas de discipline enregistr�e pour le cours s�lectionn�");
define('A_LANG_LENVIRONMENT_REQUEST_SELECT', 'S�lectionnez pour faire une demande de matricule:');
define('A_LANG_LENVIRONMENT_REQUEST_MATR', 'S�lectionnez pour faire une demande de matricule:');
define('A_LANG_LENVIRONMENT_WAITING', 'Veuillez patienter pendant le traitement de la demande de matricule');
define('A_LANG_LENVIRONMENT_WAITING_NOT', "Il n'y a pas de matricule enregistr�");
define('A_LANG_LENVIRONMENT_WAITING_WARNING', 'Faites une demande de matricule pour avoir acc�s aux disciplines:'); define('A_LANG_LENVIRONMENT_NAVTYPE', 'Type de navigation');
define('A_LANG_LENVIRONMENT_FORCOURSE', 'Pour le cours de');
define('A_LANG_LENVIRONMENT_NETCONECTION', 'Connexion r�seau');
define('A_LANG_LENVIRONMENT_SPEED', 'La vitesse est');
define('A_LANG_LENVIRONMENT_NAVIGATION', 'Navigation');
define('A_LANG_LENVIRONMENT_TUTORIAL', 'Tutorial');
define('A_LANG_LENVIRONMENT_FREE', 'Gratuit');
//Ambiente do professor
define('A_LANG_TENVIRONMENT_DESCRIPTION', "Le module 'Auteur' permet aux auteurs de mettre � disposition des contenus adapt�s � diff�rents profils d'�l�ves. Gr�ce � ce module, l'auteur pourra mettre � disposition le contenu de ses le�ons dans une structure unique adapt�e aux diff�rents cours. L'auteur doit �galement enregistrer sa discipline et indiquer dans quels cours il souhaite la voir appara�tre. Ensuite, doivent �tre ins�r�s les fichiers de concept du contenu en les liant � chacun des sujets de la discipline. En outre, l'auteur doit donner une description du sujet, indiquer les pr�-requis et dans quels cours il souhaite mettre � disposition son contenu. L'auteur pourra ajouter des exercices, des exemples et des supports additionnels pour chaque sujet.");
define('A_LANG_TENVIRONMENT_AUTHOR', "Droit d'auteur");
define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR1', "Une erreur est survenue lors de l'inscription de l'utilisateur dans cette discipline. Cet utilisateur doit d�j� �tre inscrit dans cette discipline ou bien le serveur a renvoy� une erreur.");
define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR2', "Une erreur est survenue lors de la d�sinscription de l'utilisateur dans cette discipline. Cet utilisateur ne doit pas �tre inscrit ou bien le serveur a renvoy� une erreur.");
define('A_LANG_TENVIRONMENT_AUTHORIZE_NOT', "Il n'y a pas de cours enregistr�");
define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECT', 'S�lectionnez le cours');
define('A_LANG_TENVIRONMENT_AUTHORIZE_NOTD', "Il n'y a pas de discipline enregistr�e pour ce cours");
define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECTD', 'S�lectionnez la discipline');
define('A_LANG_TENVIRONMENT_AUTHORIZE_LIST', 'Liste des �l�ves:');
define('A_LANG_TENVIRONMENT_AUTHORIZE_MATR', '�l�ves inscrits:');
define('A_LANG_TENVIRONMENT_AUTHORIZE_BACK', 'Pr�c�dent');
define('A_LANG_TENVIRONMENT_LIBERATE_DESCRIPTION', '* seules les disciplines dont le contenu est correct pourront �tre mise � disposition. Pour v�rifier le contenu des disciplines, acc�dez au guide CONCEPT dans le menu STUCTURER LE CONTENU et utilisez le bouton G�N�RER LE CONTENU.');
define('A_LANG_TENVIRONMENT_LIBERATE_LIB', "Autoriser l'acc�s � la discipline:");
define('A_LANG_TENVIRONMENT_LIBERATE_LIB_DIS', 'Disciplines autoris�es:');
define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE', 'D�placer le concept');
define('A_LANG_TENVIRONMENT_ESTRUCT_EXCLUDE', 'Effacer le concept');
define('A_LANG_TENVIRONMENT_ESTRUCT_SELECT', 'S�lectionnez le concept que vous souhaitez D�PLACER ou EFFACER');
define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE_AFTER', 'D�PLACER le concept s�lectionn� � la suite de quel autre?');
define('A_LANG_TENVIRONMENT_ESTRUCT_CANCEL', "L'op�ration D�PLACER ou EFFACER le concept causera l'annulation des pr�-requis correspondants");
define('A_LANG_TENVIRONMENT_ESTRUCT_NOT_POSSIBLE', "Il n'a pas �t� possible de lire la matrice de la base de donn�es");
define('A_LANG_TENVIRONMENT_TOPICS_LEGEND', "L�gende des pr�-requis:");
define('A_LANG_TENVIRONMENT_TOPICS_LEGEND1', "Pour s�lectionner plusieurs pr�-requis, utilisez la touche CTRL");
define('A_LANG_TENVIRONMENT_TOPICS_LEGEND2', 'Pour supprimer le(s) pr�-requi(s) de la s�lection, utilisez la touche CTRL');
define('A_LANG_TENVIRONMENT_TOPICS_LEGEND3', 'Les pr�-requis en bleu fonc� sont s�lectionn�s');
define('A_LANG_TENVIRONMENT_TOPICS_LEGEND4', 'Le fond bleu indique les pr�-requis actuels (au cas ou la s�lection serait perdue)');
define('A_LANG_TENVIRONMENT_TOPICS_LEGEND5', "Le fond jaune indique les pr�-requis actuels automatiques");
define('A_LANG_TENVIRONMENT_EXAMPLES_NUMBER', 'Num�ro du concept:');
define('A_LANG_TENVIRONMENT_EXAMPLES_NAME', 'Nom du concept:');
define('A_LANG_TENVIRONMENT_EXAMPLES_DESCRIPTION', 'Description du concept:');
define('A_LANG_TENVIRONMENT_EXAMPLES_KEY', 'Mot-Cl� du concept:');
define('A_LANG_TENVIRONMENT_EXAMPLES_LEVEL', 'Niveau de complexit�:');
define('A_LANG_TENVIRONMENT_EXAMPLES_NOCLASS', 'Non classifi�');
define('A_LANG_TENVIRONMENT_EXAMPLES_EASY', 'Facile');
define('A_LANG_TENVIRONMENT_EXAMPLES_MEDIUM', 'Moyen');
define('A_LANG_TENVIRONMENT_EXAMPLES_COMPLEX', 'Compliqu�');
define('A_LANG_TENVIRONMENT_EXAMPLES_DSC', 'Description');
define('A_LANG_TENVIRONMENT_EXAMPLES_LVL', 'Niveau');
define('A_LANG_TENVIRONMENT_EXERCISES_NAME', "Nom de l'exercice:");
define('A_LANG_TENVIRONMENT_EXERCISES_DESCRIPTION', "Description de l'exercice:");
define('A_LANG_TENVIRONMENT_EXERCISES_KEY', "Mot-Cl� de l'exercice:");
define('A_LANG_TENVIRONMENT_COMPLEMENTARY_NAME', 'Nom de <br> compl�ments de cours:');
define('A_LANG_TENVIRONMENT_COMPLEMENTARY_DESCRIPTION', 'Description de <br> compl�ments de cours:');
define('A_LANG_TENVIRONMENT_COMPLEMENTARY_KEY', 'Mot-Cl� de <br> compl�ments de cours:');
define('A_LANG_TENVIRONMENT_ACCESS', "Acc�s � l'Environnement Auteur");
define('A_LANG_TENVIRONMENT_WAIT', "Veuillez patientez pendant que l'administrateur autorise l'acc�s � l'Environnement Auteur");
define('A_LANG_TENVIRONMENT_COMMENT', "L'acc�s � l'Environnement Auteur est g�r� par l'administrateur de l'environnement AdaptWeb. Apr�s s'�tre inscrit dans l'environnement AdaptWeb, il est n�cessaire d'obtenir l'autorisation de l'administrateur pour acc�der � l'Environnement Auteur");
define('A_LANG_TENVIRONMENT_DEMO', 'D�monstration des environnements');
define('A_LANG_TENVIRONMENT_ASSOCIATED_FILES','Obs.: les fichiers associ�s doivent �tre envoy�s au serveur gr�ce � la section "maintenance du concept"');
//Op��es de Root
define('A_LANG_ROOT_NOT_AUTH', 'Professeurs non autoris�s:');
define('A_LANG_ROOT_AUTH', 'Professeurs autoris�s:');
define('A_LANG_ROOT_DEV', 'Cette option est actuellement en d�veloppement.');
//Outros
define('A_LANG_DATA', 'Donn�es');
define('A_LANG_WARNING', 'Avis');
define('A_LANG_COORDINATOR', 'COORDINATEUR');
define('A_LANG_DATABASE_PROBLEM', 'Il y a des probl�mes avec la base de donn�es');
define('A_LANG_FAQ', 'Questions fr�quentes');
define('A_LANG_WORKS', 'Travaux');
define('A_LANG_NOT_DISC', 'Impossible de pr�senter les disciplines enregistr�es');
define('A_LANG_NO_ROOT', "Impossible d'ajouter l'utilisateur Root");
define('A_LANG_MIN_VERSION', 'La version minimum support�e de la base de donn�es est[');
define('A_LANG_INSTALLED_VERSION', '], et la version install�e est [');
define('A_LANG_ERROR_BD', "Impossible d'enregistrer les donn�s dans la base de donn�es");
define('A_LANG_DISC_NOT_FOUND', 'Nom de la discipline non trouv� ou non renseign�');
define('A_LANG_SELECT_COURSE', 'S�lectionnez le(s) cours du concept');
define('A_LANG_TOPIC_REGISTER', 'Enregistrement des concepts');
define('A_LANG_SUMARIZED_DESCRIPTION', 'Description r�sum�e du concept');
define('A_LANG_LOADED', 'Charg�e sur le serveur');
define('A_LANG_FILL_DESCRIPTION', 'Remplissez le champ de description du support de cours');
define('A_LANG_WORKS_SEND_DATE', "Donn�es d'envoi");
define('A_LANG_SEARCH_RESULTS', 'R�sultats de la recherche');
define('A_LANG_SEARCH_SYSTEM', 'Outil de recherche');
define('A_LANG_SEARCH_WORD', 'Mot recherch�');
define('A_LANG_SEARCH_FOUND', "L'outil a trouv� ");
define('A_LANG_SEARCH_OCCURRANCES', ' occurrences de ');
define('A_LANG_SEARCH_IN_TOPIC', ' dans CONCEPT');
define('A_LANG_SEARCH_FOUND1', "L'outil a trouv� ");
define('A_LANG_SEARCH_OCCURRANCE', ' occurrence de ');
define('A_LANG_SEARCH_NOT_FOUND', "La recherche n'a donn� aucun r�sultat de ");
define('A_LANG_SEARCH_CLOSE', 'Fermer');
define('A_LANG_TOPIC', 'Concept');
define('A_LANG_SHOW_MENU', 'Montrer le menu');
define('A_LANG_CONFIG', 'Configurations');
define('A_LANG_MAP', 'Plan');
define('A_LANG_PRINT', 'Imprimer cette page?');
define('A_LANG_CONNECTION', "Impossible d'�tablir une connexion avec le serveur de base de donn�es MySQL. Veuillez contacter l'administrateur.");
define('A_LANG_CONTACT', "Veuillez contacter l'administrateur");
define('A_LANG_HELP_SYSTEM', 'Aide');
define('A_LANG_CONFIG_SYSTEM', 'Configurations du syst�me');
define('A_LANG_CONFIG_COLOR', 'D�finir la couleur de fond');
define('A_LANG_NOT_CONNECTED', 'Non connect�!');
define('A_LANG_DELETE_CADASTRE', "Effacer l'enregistrement des �l�ves");
define('A_LANG_HOME', 'Accueil');
define('A_LANG_YOU_HAVE', 'Vous avez ');
define('A_LANG_TO_CONFIRM', ' matricule(s). Pour confirmer cet effacement cliquer sur ');
define('A_LANG_CONFIRM', ' CONFIRMER');
define('A_LANG_EXAMPLE_LIST', 'Liste des exemples');
define('A_LANG_COMPLEXITY_LEVEL_EASY', 'Niveau de difficult� Facile');
define('A_LANG_COMPLEXITY_LEVEL_MEDIUM', 'Niveau de difficult� Moyen');
define('A_LANG_COMPLEXITY_LEVEL_COMPLEX', 'Niveau de difficult� Difficile');
define('A_LANG_EXERCISES_LIST', 'Liste des exercices');
define('A_LANG_REGISTER_MAINTENENCE', 'Maintenance des matricules');
define('A_LANG_REGISTER_RELEASE', 'Autorisation des matricules');
define('A_LANG_SITE_MAP', 'Plan du site');
define('A_LANG_COMPLEMENTARY_LIST', 'Liste des compl�ments de cours');
define('A_LANG_DELETE_TOPIC', 'Supprim� concept ');
define('A_LANG_AND', ' et ');
define('A_LANG_SONS', ' sous-sujet(s).');
define('A_LANG_CONFIRM_EXCLUSION', '�tes-vous s�r que vous voulez supprimer la discipline');
define('A_LANG_YES', '  Oui  ');
define('A_LANG_NO', '  Non  ');
//Gera��o de conte�do
define('A_LANG_GENERATION_END', 'Fin de la g�n�ration du fichier XML contenant les �l�ments du sujet');
define('A_LANG_GENERATION_CREATED', 'La g�n�ration a cr�� ');
define('A_LANG_GENERATION_XML', ' fichiers XML.');
define('A_LANG_GENERATION_END_STRUCT', 'Fin de la g�n�ration du code XML de Structure des Sujets');
/*
define('A_LANG_GENERATION_MISSING_FILE', 'Il reste � ajouter le fichier de concept de ');
define('A_LANG_GENERATION_TH_TOPIC', '� concept');
define('A_LANG_GENERATION_MISSING_COURSE', 'Il reste � ajouter le cours de ');
define('A_LANG_GENERATION_NO_ASOC', 'Concept sans cours associ�');
define('A_LANG_GENERATION_MISSING_EXERC_ID', "Il reste � ajouter l'identifiant d'exercice de ");
define('A_LANG_GENERATION_TH_EXERCISE', '� exercice et ');
define('A_LANG_GENERATION_MISSING_EXERC_DESCRIPTION', "Il reste � ajouter la description de l'exercice de '); define('A_LANG_GENERATION_MISSING_EXERC_FILE', 'Il reste � ajouter le fichier d'exercice de '); define('A_LANG_GENERATION_MISSING_EXERC_COMPLEXITY', 'Il reste � ajouter le niveau de difficult� de l'exercice de ");
define('A_LANG_GENERATION_TH_EXAMPLE', "� exemple et ");
define('A_LANG_GENERATION_MISSING_EXAMP_ID', "Il reste � ajouter l'identifiant de l'exemple de '); define('A_LANG_GENERATION_MISSING_EXAMP_DESCRIPTION', 'Il reste � ajouter la description de l'exemple de '); define('A_LANG_GENERATION_MISSING_EXAMP_FILE', 'Il reste � ajouter le fichier d'exemple de '); define('A_LANG_GENERATION_MISSING_EXAMP_COMPLEXITY' ,'Il reste � ajouter le niveau de difficult� de l'exemple de ");
define('A_LANG_GENERATION_TH_MATCOMP', '� compl�ment de cours et ');
define('A_LANG_GENERATION_MISSING_MATCO_ID', "Il reste � ajouter l'identifiant du compl�ment de cours de ");
define('A_LANG_GENERATION_MISSING_MATCO_DESCRIPTION', 'Il reste � ajouter la description du compl�ment de cours de ');
define('A_LANG_GENERATION_MISSING_MATCO_FILE', "Il reste � ajouter le fichier de compl�ment de cours de ");
*/
define('A_LANG_GENERATION_VERIFY', "V�rification des donn�es de l'auteur");
define('A_LANG_GENERATION_PROBLEMS', "Probl�mes lors de l'inscription dans la discipline");
/*
define('A_LANG_GENERATION_MISSING_TOPIC_NUMBER', 'Il reste � ajouter le Num�ro du Sujet pour le ');
define('A_LANG_GENERATION_MISSING_TOPIC_DESCRIPTION', 'Il reste � ajouter la Description du Sujet pour le ');
define('A_LANG_GENERATION_MISSING_TOPIC_ABREV', "Il reste � ajouter l'Abr�viation du Sujet pour le ");
define('A_LANG_GENERATION_MISSING_TOPIC_KEYWORD', "Il reste � ajouter le Mot-Cl� du Sujet pour le ");
*/
define('A_LANG_GENERATION_RESULT_XML', "R�sultat de la G�n�ration du code XML");
define('A_LANG_GENERATION_NO_ERROR', 'AUCUNE ERREUR');
define('A_LANG_GENERATION_SUCCESS', 'FICHIERS DU COURS G�N�R�S AVEC SUCC�S');
define('A_LANG_GENERATION_DATA_LACK', "Les fichiers XML n'ont pas pu �tre g�n�r�s par manque de donn�es");
define ('A_LANG_GENERATION_TOPIC','Concept');	
define ('A_LANG_GENERATION_EXAMPLE','Exemple');	
define ('A_LANG_GENERATION_EXERCISE','Exercice');	
define ('A_LANG_GENERATION_COMPLEMENTARY','Support compl�mentaire');	
define ('A_LANG_GENERATION_TOPIC_NUMBER','Num�ro du sujet');	
define ('A_LANG_GENERATION_TOPIC_NAME','Nom du sujet');	
define ('A_LANG_GENERATION_DESCRIPTION','Description');	
define ('A_LANG_GENERATION_KEY_WORD','Mot-Cl�');	
define ('A_LANG_GENERATION_COURSE','Cours');	
define ('A_LANG_GENERATION_PRINC_FILE','Archive principale');	
define ('A_LANG_GENERATION_ASSOC_FILE','Archives Associ�s');	
define ('A_LANG_GENERATION_MISSING','Absent');	
define ('A_LANG_GENERATION_NO_PROBLEM','Ok');	
define ('A_LANG_GENERATION_NUMBER','Num�ro');	
define ('A_LANG_GENERATION_COMPLEXITY','Niveau');	
define ('A_LANG_GENERATION_NOT_EXIST','Non avez');	

	//Reposit�rio
	define ('A_LANG_MNU_REPOSITORY','Repository');	
	define ('A_LANG_PRESENTATION','Presentation');
	define ('A_LANG_EXPORT','Disciplines Exportation');
	define ('A_LANG_IMPORT','Disciplines Importation');
	define ('A_LANG_OBJECT_SEARCH','Search Learning Objects');
	define ('A_LANG_EXP_TEXT', 'Send your disciplines to the Repository, so the other teachers can use it too.');
	define ('A_LANG_IMP_TEXT', 'Use available disciplines in your AdaptWeb environment.');
	define ('A_LANG_SCH_TEXT', 'The AdaptWeb disciplines\' data were also indexed in the Learning Objects format, using metadata of the LOM (Learning Object Metadata) standard. Make searches by learning objects envolving disciplines, topics (concepts) and repository files.');
	define ('A_LANG_REP_TEXT', 'Welcome to the AdaptWeb\'s Disciplines Repository.<br>Through this repository, AdaptWeb users from different locations will be able to share data to create their disciplines. Here you can make the importation and exportation of disciplines, and search for learning objects.');
	define ('A_LANG_REP_SERVER', 'Repository Server');
	define ('A_LANG_REP_ADAPT_SERVER', 'AdaptWeb Server');
	define ('A_LANG_ADAPTWEB', 'AdaptWeb');
	define ('A_LANG_REP_DISC', 'Disciplines Repository');
	define ('A_LANG_REP_JUNE', 'June of 2004');
	define ('A_LANG_REP_SUGESTIONS', 'Doubts, sugestions, claimings');
	define ('A_LANG_REP_SELECT', 'Select a discipline');
	define ('A_LANG_REP_EXP_SUCCESS', 'Exportation succesfully done!');
	define ('A_LANG_REP_EXP_ERROR', 'Error exporting discipline!');
	define ('A_LANG_REP_EXP_TEXT', 'Through this interface you will be able to let you AdaptWeb disciplines available so that other people may use it.<br>Select the discipline in the list above, specify a description to her and click in &quot;Export&quot;.');
	define ('A_LANG_REP_EXPORT', 'Export');
	define ('A_LANG_REP_SELECT_COURSE', 'Select a Course');
	define ('A_LANG_REP_IMP_SUCCESS', 'Importation successfully done!');
	define ('A_LANG_REP_IMP_ERROR', 'Error importing discipline!');
	define ('A_LANG_REP_INVALID_PAR', 'Invalid Parameters!');
	define ('A_LANG_REP_IMP_TEXT', 'Through this interface you will be able to copy available disciplines of the repository to your authoship environment on AdaptWeb!<br>Select the discipline on the list above, specify the course which it will be associated, click in &quot;Import&quot;.');
	define ('A_LANG_REP_DISC_IN_REP', 'Disciplines in Repository');
	define ('A_LANG_REP_MY_COURSES', 'My Courses');
	define ('A_LANG_REP_OTHER_NAME', 'Import with another name');
	define ('A_LANG_REP_IMPORT', 'Import');
	define ('A_LANG_REP_DISC_DATA', 'Discipline Data');
	define ('A_LANG_REP_SEARCH', 'Digit a search string and choose the metadata that must be included in the search:');
	define ('A_LANG_REP_SCH_TITLE', 'Title');
	define ('A_LANG_REP_SCH_SELECT', 'Select...');
	define ('A_LANG_REP_AGGREG', 'Aggregation Level');
	define ('A_LANG_REP_SCH_RAW', 'Raw Data (files)');
	define ('A_LANG_REP_SCH_TOPIC', 'Topic');
	define ('A_LANG_REP_SCH_DISC', 'Discipline');
	define ('A_LANG_REP_SCH_COURSE', 'Course');
	define ('A_LANG_REP_SCH_SEARCH', 'Search');
	define ('A_LANG_REP_SCH_ERROR', 'Error');
	define ('A_LANG_REP_SCH_NO_REG', 'No register found!');
	define ('A_LANG_REP_SCH_REG', 'register found');
	define ('A_LANG_REP_SCH_REGS', 'registers found');
	define ('A_LANG_REP_DISC_NOT_FOUND', 'Discipline not found');
	define ('A_LANG_COURSES', 'Course(s)');
	define ('A_LANG_EXP_BY', 'Exported by');
	define ('A_LANG_ERROR_SELECT_BD', 'Error when selecting the data base');
?>