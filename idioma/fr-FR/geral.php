<? 
/* -----------------------------------------------------------------------
*  AdaptWeb - Projeto de Pesquisa       
*     UFRGS - Instituto de Inform�tica  
*       UEL - Departamento de Computa��o
* -----------------------------------------------------------------------
*       @package AdaptWeb
*     @subpakage Linguagem
*          @file idioma/pt-BR/geral.php
*    @desciption Arquivo de tradu��o - Portugu�s/Brasil
*         @since 17/08/2003
*        @author Veronice de Freitas (veronice@jr.eti.br)
*   @Translation Veronice de Freitas
* -----------------------------------------------------------------------         
*/  
 
       
  // ************************************************************************
//Dados da interface da ferramenta de autoria
  define("A_LANG_VERSION","Version"); 
  define("A_LANG_SYSTEM","Environnement AdaptWeb"); 
  define("A_LANG_ATHORING","Outil de Auteur"); 
  define("A_LANG_USER","Nom d'utilisateur");  
  define("A_LANG_PASS","Mot de passe");  
  define("A_LANG_NOTAUTH","<pas connect�>");
  define("A_LANG_HELP","Aider"); 
  define("A_LANG_DISCIPLINE","Discipline");  
  define("A_LANG_NAVEGATION","L� o� je suis: ");

// Contexto
// observa��o - arquivo p_contexto_navegacao.php
       // ************************************************************************ // * Menu                                                                                 *
  // ************************************************************************
// Home
define("A_LANG_MNU_HOME","accueil"); 
define("A_LANG_MNU_NAVIGATION","Module de l'�tudiant");
define("A_LANG_MNU_AUTHORING","Module de l'enseignant"); 
define("A_LANG_MNU_UPDATE_USER","enregistrer le changement");
define("A_LANG_MNU_EXIT","Exit"); 
define("A_LANG_MNU_LOGIN","Login"); 
define("A_LANG_MNU_ABOUT","� propos de");
define("A_LANG_MNU_DEMO","D�monstration");
define("A_LANG_MNU_PROJECT","Project");
define("A_LANG_MNU_FAQ","FAQ"); 
define("A_LANG_MNU_RELEASE_AUTHORING","Communiqu� Authoring"); 
define("A_LANG_MNU_BACKUP","Backup"); 
define("A_LANG_MNU_NEW_USER","New User"); 
define("A_LANG_MNU_APRESENTATION","Pr�sentation");
define("A_LANG_MNU_MY_ACCOUNT","Mon compte");
define("A_LANG_MNU_MANAGE","Gestion"); 
define("A_LANG_MNU_BAKCUP","Backup");
define("A_LANG_MNU_GRAPH","Acc�s");
define("A_LANG_MNU_TODO","D�veloppement"); 
// Alterado por Carla-estagio UDESC 2008/02 
define("A_LANG_SYSTEM_NAME","AdaptWeb - Enseignement-apprentissage pour Adaptive Web ");  
define("A_LANG_SYSTEM_NAME","AdaptWeb");

define("A_LANG_SYSTEM_COPYRIGHT","Licensed under the GNU GENERAL PUBLIC LICENSE Version 2"); 
        // Autoria        
        define("A_LANG_MNU_COURSE","Cour");  
        define("A_LANG_MNU_DISCIPLINES","Discipline");         
        define("A_LANG_MNU_DISCIPLINES_COURSE","Discipline / Cour");                        
        define("A_LANG_MNU_STRUTURALIZE_TOPICS","Estruturar Conte�do"); 
        define("A_LANG_MNU_ACCESS_LIBERATE","Examen des demandes d'enregistrement");
        define("A_LANG_MNU_LIBERATE_DISCIPLINES","Lib�ration Discipline");
        define("A_LANG_MNU_WORKS","Travaux d'�l�ves"); 
     	 define("A_LANG_MNU_LOG","Log Analysis");  
	 define("QUESTIONARIO","Questionnaire"); 
	 define("QS_ROOT","R�sultats QS");   
      

// Estudante
define("A_LANG_MNU_DISCIPLINES_RELEASED","Regarder Discipline");
define("A_LANG_MNU_SUBSCRIBE","Demande d'inscription");
define("A_LANG_MNU_WAIT","En attente d'enregistrement");
define("A_LANG_MNU_UPLOAD_FILES","D�poser travail");
define("A_LANG_MNU_MESSAGE","Avis");
// ************************************************************************ // * Formul�rio Principal                                                                          *
  // ************************************************************************ 
  // Apresenta��es
define("A_LANG_ORGANS","Partenariat");
define("A_LANG_RESEARCHES","Les chercheurs");
define("A_LANG_AUTHORS","Auteurs"); 

 
  // ************************************************************************ // * Formul�rio - Autoria                                                                     *
  // ************************************************************************
// Apresenta��o
define("A_LANG_MNU_ORGANS",""); 
define("A_LANG_MNU_RESEARCHES",""); 
define("A_LANG_MNU_AUTHORS",""); 

        // Formul�rio de solicita��o de acesso
        define("A_LANG_REQUEST_ACCESS","Demande d'acc�s");
        define("A_LANG_REQUEST_ACCESS_USER_TYPE","Type d'utilisateur *");
        define("A_LANG_REQUEST_ACCESS_TYPE_STUDANT","�tudiant");
        define("A_LANG_REQUEST_ACCESS_TYPE_TEACHES","Professeur");
        define("A_LANG_REQUEST_ACCESS_NAME","Nom *");
        define("A_LANG_REQUEST_ACCESS_EMAIL","E-mail *");
        define("A_LANG_REQUEST_ACCESS_PASS","Mot de Passe *");
        define("A_LANG_REQUEST_ACCESS_PASS2","Confirmation du mot de passe *");
        define("A_LANG_REQUEST_ACCESS_INSTITUTION_EDUCATION","Institution de l'�ducation");
        define("A_LANG_REQUEST_ACCESS_COMMENT","Notification");
        define("A_LANG_REQUEST_ACCESS_LANGUAGE","langage");
        define("A_LANG_REQUEST_ACCESS_MSG1","Impossible de se connecter � la base de donn�es");
	 define("A_LANG_REQUEST_ACCESS_MSG2","arlez d'autres e-mail, il est d�j� enregistr�");
        define("A_LANG_REQUEST_ACCESS_MSG3","Impossible d'envoyer vos coordonn�es");
        define("A_LANG_REQUEST_ACCESS_MSG4","mot de passe non valide");
        define("A_LANG_REQUEST_ACCESS_SAVE","Sauver");
	// Acrescenta mensagem para este formulario - Carla -est�gio UDESC 2008/02
	define("A_LANG_REQUEST_ACCESS_MSG5","Les champs qui ont * doivent �tre remplis");
	define("A_LANG_REQUEST_ACCESS_MSG6","Le champ e-mail  doit �tre rempli");
	define("A_LANG_REQUEST_ACCESS_MSG7","Le champ Nom doit �tre rempli");
	define("A_LANG_REQUEST_ACCESS_MSG8","Le mot de passe domaine est compl�t�e par une valeur non valide");
	define("A_LANG_REQUEST_ACCESS_MSG9","Les champs de mot de passe et Confirmation du mot de passe doit �tre complet avec les m�mes donn�es");
	define("A_LANG_REQUEST_ACCESS_MSG10","Votre enregistrement a �t� effectu� avec succ�s");
	define("A_LANG_REQUEST_ACESS_OBSERVACAO", " Note: Les champs qui ont une * doivent �tre remplis.");
	// Fim das alteracoes Carla


        //Formulario de login
        define("A_LANG_LOGIN2","Login");
        define("A_LANG_EMAIL","E-mail");
        define("A_LANG_LOGIN_PASS","Mot de passe");
        define("A_LANG_LOGIN_START","Entrez");
        define("A_LANG_LOGIN_MSG1","Mot de passe est invalide");
        define("A_LANG_LOGIN_MSG2","E-mail est invalide");
        define("A_LANG_LOGIN_MSG3","Login non autoris�e");
        define("A_LANG_LOGIN_MSG4","Attendre la publication de votre acc�s par l'administrateur du AdaptWeb");
        define("A_LANG_LOGIN_MSG5","Impossible de se connecter � la base de donn�es");
                

        // Formul�rio de cadastro de Curso
        define("A_LANG_COURSE_REGISTER","Inscrivez-vous � des cours");
        define("A_LANG_COURSE_RESGISTERED","Cours enregistr�");
        define("A_LANG_COURSE2","Cour");
        define("A_LANG_COURSE_INSERT","additionner");
        define("A_LANG_COURSE_UPDATE","Changer");
        define("A_LANG_COURSE_DELETE","Supprimer");
        define("A_LANG_COURSE_MSG1","Impossible de se connecter � la base de donn�es");
	 define("A_LANG_COURSE_MSG2","Impossible d'ajouter le cours");
	 define("A_LANG_COURSE_MSG3","Impossible de changer le cours");
	 define("A_LANG_COURSE_MSG4","Ne peuvent �tre enlev�s dans le cours. Pour le supprimer, vous devez entrer la discipline du cours et de supprimer la s�lection de ce cours dans toutes les disciplines.");
	 define("A_LANG_COURSE_MSG5","Impossible de supprimer le cours");
	 // Incllus�o de mensagens para usabilidade - Carla -estagio UDESC-2008/02
	 define("A_LANG_COURSE_MSG6", "Cours enregistr� avec succ�s");
	 define("A_LANG_COURSE_MSG7","Cours supprim� avec succ�s");
	// Fim alteracoes Carla
 

        // Cadastro de disciplina
        define("A_LANG_DISCIPLINES_REGISTER","Registre de Discipline");
        define("A_LANG_DISCIPLINES_REGISTERED","Disciplines enregistr�");
        define("A_LANG_DISCIPLINES2","Discipline");
        define("A_LANG_DISCIPLINES_INSERT","Additionner");
        define("A_LANG_DISCIPLINES_UPDATE","Changer");
        define("A_LANG_DISCIPLINES_DELETE","Supprimer");
        define("A_LANG_DISCIPLINES_MSG1","Impossible d'ajouter de la discipline");
        define("A_LANG_DISCIPLINES_MSG2","Impossible de modifier la discipline");
        define("A_LANG_DISCIPLINES_MSG3","Il ne peut pas supprimer cette discipline. Pour le supprimer, vous devez entrer Discipline / Cours et d�sactiver la s�lection de tous les cours li�s.");
        define("A_LANG_DISCIPLINES_MSG4","Impossible de supprimer la discipline");
        // Inclus�o de mensagens para usabilidade - Carla est�gio UDESC 2008/02
	 define("A_LANG_DISCIPLINES_MSG5","Il ya une discipline de ce nom");
	 define("A_LANG_DISCIPLINES_MSG6","Une discipline ne peut �tre men� � vide");
	 define("A_LANG_DISCIPLINES_MSG7","Pour modifier un cours, vous devez le s�lectionner et entrer un nouveau nom");
	 define("A_LANG_DISCIPLINES_MSG8","la discipline a �t� enregistr� avec succ�s");
	 // Final Carla


        // Formul�rio de cadastro de Disciplina / Curso
        define("A_LANG_DISCIPLINES_COURSE1","Discipline / Cours");
        define("A_LANG_DISCIPLINES_COURSE_DISCIPLINES","Disciplines");
        define("A_LANG_DISCIPLINES_COURSE2","Cours");
        define("A_LANG_DISCIPLINES_COURSE_SAVE","Sauver");
        define("A_LANG_DISCIPLINES_COURSE_MSG1","Ajouter DISCIPLINE COURS et � l'acc�s de ce point.");
        define("A_LANG_DISCIPLINES_COURSE_MSG2","Vous ne pouvez pas le lien avec le cours de discipline � cause d'un manque de connexion � la base de donn�es.");
        define("A_LANG_DISCIPLINES_COURSE_MSG3","Entrez le nom du cours pour y inclure");
        define("A_LANG_DISCIPLINES_COURSE_MSG4","Cours d�j� existant");
        define("A_LANG_DISCIPLINES_COURSE_MSG5","S�lectionnez un cours pour un changement");
        // Inclus�o de mensagens para usabilidade - Carla est�gio UDESC 2008/02
	 define("A_LANG_DISCIPLINES_COURSE_MSG6","Le lien entre des sujets et des cours a �t� un succ�s.");
	 define("A_LANG_DISCIPLINES_COURSE_MSG7","Il n'y a pas de cours choisi pour cette discipline");
	 define("A_LANG_DISCIPLINES_COURSE_MSG8","Cour a �t� chang� avec succ�s");
	 define("A_LANG_DISCIPLINES_COURSE_OBS","Note: Pour cr�er la structure du contenu de la discipline qui sont n�cessaires relaion� avec au moins un cours");
	 //Final Carla

                
	// Formul�rio de autoriza��o de acesso
	 define("A_LANG_LIBERATE_USER1","Inscription / Etudiant");   // para o professor e administrador
        define("A_LANG_LIBERATE_USER2","Permettre aux enseignants / Auteur"); // para o adiministrador
        define("A_LANG_LIBERATE_MSG1","Il est en d�veloppement ...");
        
        
        // Formul�rio para libera��o da disciplina 
	 define("A_LANG_LIBERATE_DISCIPLINES","Lib�ration Discipline");
        define("A_LANG_LIBERATE_DISCIPLINES_MSG1","Il est en d�veloppement ...");
	 // Inclus�o de mensagens para usabilidade - Carla est�gio UDESC 2008/02
	 define("A_LANG_TEXT_LIBERATE","Pour lib�rer de la discipline est �tabli doit s�lectionner dans la liste \"Releasing Discipline \" et passer � la liste des \"Disciplines lib�r� \".");
	 define("A_LANG_TEXT_LIBERATE_MAT","Pour lib�rer l'enregistrement est demand� par l'�tudiant doit choisir dans le r�pertoire \"Liste des �l�ves\" et passer � la \"Liste des �tudiants inscrits\".");//carla incluiu para liberar matricula de aluno
	 // Final Carla
                                                          
        // ======================= Topicos ==========================
                        
        // Entrada / t�picos
        define("A_LANG_ENTRANCE_TOPICS_DISCIPLINES","Discipline");
        // **** REVER "Estruturar t�pico"
        define("A_LANG_ENTRANCE_TOPICS_STRUTURALIZE","Organiser Topique");
        define("A_LANG_ENTRANCE_TOPICS_MSG1","Il ne pouvait pas lire les disciplines");
        define("A_LANG_ENTRANCE_TOPICS_MSG2","ote: Seulement disponible de cette fa�on les sujets sont li�s � au moins un cours.");
        

       // Guia - Lista de t�picos
        define("A_LANG_TOPIC_GENERATE_CONTENT","G�n�rer le contenu");
        //orelha
        define("A_LANG_TOPIC_LIST","Concepts");
        define("A_LANG_TOPIC_MAINTENANCE","Maintenance Concept");
        define("A_LANG_TOPIC_EXEMPLES","Exemples");
        define("A_LANG_TOPIC_EXERCISES","Exercices");
        define("A_LANG_TOPIC_COMPLEMENTARY","Supplementary Material");
	  //Inclusao de mensagens de Usabilidade - Carla -estagio UDESC 2008/02
	   define("A_LANG_TOPIC_OBS","<b> * Note: </b> Pour <u> Cr�ation de contenu </u> sur la page d'accueil de concepts, vous devez remplir correctement tous les champs de cette page. <br> Le fichier est en cours de chargement doit �tre <b>. Html </b>, le cas �ch�ant, l'image dans ce fichier, vous devez t�l�charger l'image. Le fichier html <br> peut �tre remplac� par le t�l�chargement d'un autre fichier. html./");
			//Carla inseriu observa��o em pagina q cria topicos
	   define("A_LANG_TOPIC_ARQ_OBS","<b><b> * Note: </b> Extension du fichier html (par exemple capitulo1.html)");//Carla inseriu observa��o para tipo de arquivo
	   define("A_LANG_TOPIC_GRAVAR_MSG1","Les donn�es enregistr�es. Si vous apportez des modifications � cette page, vous devez �crire � nouveau.");
	 // Final Carla


        
        // Guia - manuten��o do t�pico
  	 define("A_LANG_TOPIC_NUMBER","Nombre de Concept");
	 define("A_LANG_TOPIC_DESCRIPTION","Nom de Concept");
	 define("A_LANG_TOPIC_SUMARIZED_TOPIC_DESCRIPTION","Br�ve description du concept");
	 define("A_LANG_TOPIC_WORDS_KEY","Mots-cl�s");
  	 define("A_LANG_TOPIC_PREREQUISIT","Pr�requis");
	 define("A_LANG_TOPIC_COURSE","Cour");
	 define("A_LANG_TOPIC_FILE1","Fichier");
	 define("A_LANG_TOPIC_FILE2","Fichier(s)");
	 define("A_LANG_TOPIC_MAIN_FILE","Principaux fichier");
	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE","Associated fichier");
	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_STATUS","Situation");
	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_UPLOAD",""); 
        define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG1","t�l�charger vers le serveur");
  	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG2","envoy�s sur le serveur");

       // bot�es
        define("A_LANG_TOPIC_SAVE","Sauver");
        define("A_LANG_TOPIC_SEND","Envoyer");
        define("A_LANG_TOPIC_SEARCH","recherche");
	 define("A_LANG_TOPIC_INSERT_TOPIC_SAME_LEVEL","Ins�rer dans le concept m�me niveau");
        define("A_LANG_TOPIC_INSERT_SUB_TOPIC","Ins�rer des sous-concept");
	 define("A_LANG_TOPIC_DELETE","Supprimer");
	 define("A_LANG_TOPIC_PROMOTE","Promouvoir");
	 define("A_LANG_TOPIC_LOWER","R�trograder");
	 // Inclusao de mesanges de Usabilidade - Carla - estagio UDESC 2008/02
	 define("A_LANG_TOPIC_INSERT_MSG1","Remplissez tous les champs");//carla inseriu para colocar mensagem para usuario ao criar um topico
	 define("A_LANG_TOPIC_INSERT_MSG2","Remplissez le nom de domaine");//carla inseriu para colocar mensagem para usuario ao criar um topico
	 define("A_LANG_TOPIC_INSERT_MSG3","Remplissez le champ de description");//carla inseriu para colocar mensagem para usuario ao criar um topico
	 define("A_LANG_TOPIC_INSERT_MSG4","Remplissez le champ mot-cl�");//carla inseriu para colocar mensagem para usuario ao criar um t�pico
	 define("A_LANG_TOPIC_OBS2","<b> * Note: </b> Tous les champs doivent �tre remplis.");
	// Final Carla


	// Guia - samples
	//define("A_LANG_TOPIC_EXEMPLES_NUMBER_TOPIC","N�mero do T�pico");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Descri��o do exemplo");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","N�vel de Complexidade");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Curso");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Excluir");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Descri��o");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","N�vel");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Curso");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Arquivo");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Arquivo(s) associado(s)");
	// bot�es
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Gravar");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Excluir");


  // Exemplos
  // N�vel de Complexidade
// Descri��o
// N�vel
// Arquivos Associados
        // Descri��o dos Exemplos
  // Exerc�cios
        // Descri��o do exerc�cio
  // Material complementar
        // Menu
// Autorizar Acesso
// Liberar disciplinas
// Novo usu�rio
        // to modify I register in cadastre (alterar cadastro)
        // ************************************************************************ // * Formul�rio - Autoria
  // ************************************************************************ // Disciplinas Liberadas

// Matricula
	// Inclus�o de mesagens de usabilidade (para matricula) Carla - estagio UDESC 2008/02
	   define("A_LANG_LENVIRONMENT_REQUEST_SUCCESS","Demande effectu�e avec succ�s. Attendre la publication de son enregistrement par l'enseignant");
	// Final Carla

// Aguardando Matricula
       // Inclus�o de mesagens de usabilidade (para aguardando matricula) Carla - estagio UDESC 2008/02
	   define("A_LANG_NO_DISC_RELEASED","Il n'y a pas de lib�rer la discipline.");
	// Final Carla


        // Autorizar Acesso
        
        // Navega��o
               
        // Tela de Cursos    
        // ************************************************************************ // * Formul�rio - Demo
  // ************************************************************************
  define("A_LANG_DEMO","La discipline est la d�mo de AdaptWeb - ne permet pas l'insertion et les changements");
//
// Regional Specific Date texts
//
// A little help for date manipulation, from PHP manual on function strftime():
//
// %a - abbreviated weekday name according to the current locale
// %A - full weekday name according to the current locale
// %b - abbreviated month name according to the current locale
// %B - full month name according to the current locale
// %c - preferred date and time representation for the current locale
// %C - century number (the year divided by 100 and truncated to an integer, range 00 to 99)
// %d - day of the month as a decimal number (range 01 to 31)
// %D - same as %m/%d/%y
// %e - day of the month as a decimal number, a single digit is preceded by a space
//      (range ' 1' to '31')
// %h - same as %b
// %H - hour as a decimal number using a 24-hour clock (range 00 to 23)
// %I - hour as a decimal number using a 12-hour clock (range 01 to 12)
// %j - day of the year as a decimal number (range 001 to 366)
// %m - month as a decimal number (range 01 to 12)
// %M - minute as a decimal number
// %n - newline character
// %p - either `am' or `pm' according to the given time value, or the corresponding strings for
//      the current locale
// %r - time in a.m. and p.m. notation
// %R - time in 24 hour notation
// %S - second as a decimal number
// %t - tab character
// %T - current time, equal to %H:%M:%S
// %u - weekday as a decimal number [1,7], with 1 representing Monday
// %U - week number of the current year as a decimal number, starting with the first Sunday as
//      the first day of the first week
// %V - The ISO 8601:1988 week number of the current year as a decimal number, range 01 to 53,
//      where week 1 is the first week that has at least 4 days in the current year, and with
//      Monday as the first day of the week.
// %W - week number of the current year as a decimal number, starting with the first Monday as
//      the first day of the first week
// %w - day of the week as a decimal, Sunday being 0
// %x - preferred date representation for the current locale without the time
// %X - preferred time representation for the current locale without the date
// %y - year as a decimal number without a century (range 00 to 99)
// %Y - year as a decimal number including the century
// %Z - time zone or name or abbreviation
// %% - a literal `%' character
//
// Note: A_LANG_DATESTRING is used for Articles and Comments Date
//       A_LANG_LINKSDATESTRING is used for Web Links creation Date
//       A_LANG_DATESTRING2 is used for Older Articles box on Home
//
  
define("A_LANG_CODE","pt-BR");
define("A_LANG_CHACTERSET","ISO-8859-1");

define("A_LANG_NAME_pt_BR","PPortugais br�silien");
define("A_LANG_NAME_en_US","Anglais");
define("A_LANG_NAME_es_ES","Espagnol");
define("A_LANG_NAME_fr_FR","Fran�ais");
define("A_LANG_NAME_ja_JP","JJaponais");

define('A_LANG_TIMEZONES','IDLW NT HST YST PST MST CST EST AST GMT-3:30 GMT-3 AT WAT GMT CET EET BT GMT+3:30 GMT+4 GMT+4:30 GMT+5 GMT+5:30 GMT+6 WAST CCT JST ACS GST GMT+11 NZST'); 
define('A_LANG_TZOFFSETS','0 1 2 3 4 5 6 7 8 8.5 9 10 11 12 13 14 15 15.5 16 16.5 17 17.5 18 19 20 21 21.5 22 23 24'); 
define('A_LANG_DATEBRIEF','%b %d, %Y');
define('A_LANG_DATELONG','%A, %B %d, %Y'); 
define('A_LANG_DATESTRING','%A, %B %d @ %H:%M:%S %Z'); 

define('A_LANG_DATESTRING2','%A, %B %d');
define('A_LANG_DATETIMEBRIEF','%b %d, %Y - %I:%M %p');
define('A_LANG_DATETIMELONG','%A, %B %d, %Y - %I:%M %p %Z'); 

define('A_LANG_LINKSDATESTRING','%d-%b-%Y');
define('A_LANG_TIMEBRIEF','%I:%M %p');
define('A_LANG_TIMELONG','%I:%M %p %Z');

define('A_LANG_DAY_OF_WEEK_LONG','Lundi Mardi Mercredi Jeudi Vendredi Samedi');
define('A_LANG_DAY_OF_WEEK_SHORT','Lun Mar Mer Jeu Vem Sam');
define('A_LANG_MONTH_LONG','Janvier F�vrier Mars Avril Mai Juin Juillet Ao�t Septembre Octobre Novembre D�cembre');
define('A_LANG_MONTH_SHORT','Jan F�v Mar Avr Mai Jui Jui Aou Sep Oct Nov D�c');
  
//************UFRGS******************
	//Index
	  //About
	// alterado por Carladefine('A_LANG_INDEX_ABOUT_INTRO',' O Ambiente AdaptWeb � voltado para  a autoria e apresenta��o adaptativa de disciplinas integrantes de cursos EAD na Web. O objetivo do AdaptWeb � permitir a adequa��o de t�ticas e formas de apresenta��o de conte�dos para alunos de diferentes cursos de gradua��o e com diferentes estilos de aprendizagem, possibilitando diferentes formas de apresenta��o de cada conte�do, de forma adequada a cada curso e �s prefer�ncias individuais dos alunos participantes.<br> O Ambiente AdaptWeb foi inicialmente desenvolvido por pesquisadores da UFRGS e da UEL, atrav�s dos projetos Electra e AdaptWeb, com apoio do CNPq. Visite a p�gina do projeto: <a href=\'http://www.inf.ufrgs.br/adapt/adaptweb\' target=\'_blank\'>http://www.inf.ufrgs.br/adapt/adaptweb</a>');
	// Alteracao mensagem Carla estagio UDESC 2008/02
	define('A_LANG_INDEX_ABOUT_INTRO'," Environnement AdaptWeb retour pour les auteurs et le d�p�t de l\'ajustement des disciplines des cours EOD sur le web AdaptWeb L\'objectif est de permettre l\'adaptation de la tactique et les moyens de pr�senter le contenu � des �tudiants de diff�rents cours et diff�rents styles d\'apprentissage, qui permet aux diff�rentes formes de pr�sentation de chaque contenu d'une mani�re adapt�e � chaque cours et les pr�f�rences individuelles des �l�ves participants. <br> Environnement AdaptWeb a �t� initialement d�velopp� par les chercheurs de l\'UFRGS et l\'UEL, par le biais de projets et AdaptWeb Electra, avec l\'appui du CNPq. Depuis 2006, les chercheurs ont commenc� � UDESC �galement contribuer au d�veloppement et � l\'am�lioration de AdaptWeb. <br><br> Visitez le  SourceForge AdaptWeb: <a href=\'http://adaptweb.sourceforge.net\' target=\'_blank\'>http://adaptweb.sourceforge.net </a>");
	define('A_LANG_INDEX_ABOUT_ENVACCESS',"L'acc�s � Adaptweb");
	define('A_LANG_INDEX_ABOUT_TINFO',"Pour fournir le contenu de vos disciplines, l'enseignant doit faire une demande d'enregistrement pour l'acc�s � l'environnement et de permettre la lib�ration de l'Administrateur Auteur");
	define('A_LANG_INDEX_ABOUT_LINFO',"Pour acc�der au cours, l'�tudiant doit faire de l'environnement, � la navigation et l'inscription est obligatoire dans la discipline en rapport avec leurs cours");
	  //Demo
	define('A_LANG_INDEX_DEMO_AENVIRONMENT','Auteur Environnement');
	define('A_LANG_INDEX_DEMO_FDESC1', 'Cr�er disciplines');
	define('A_LANG_INDEX_DEMO_NENVIRNMENT','Environnement Navigation');
	define('A_LANG_INDEX_DEMO_FDESC2', 'Regarder la discipline');
	define('A_LANG_INDEX_DEMO_TOLOG',"L'acc�s et de naviguer en faisant preuve de discipline de l'exploitation foresti�re sur l'environnement avec les donn�es ci-dessous:");
	define('A_LANG_INDEX_DEMO_LOGIN','login: demo@inf.ufrgs.br');
	define('A_LANG_INDEX_DEMO_PASS', 'mot de passe: 123');
	define('A_LANG_INDEX_DEMO_OBSERVATION','Notes:');
	define('A_LANG_INDEX_DEMO_OBSERVATIONTEXT', "apr�s avoir effectu� la connexion, l\'acc�s, il sera lib�r� dans l\'environnement de l\'enseignant et les �l�ves par les options de l\'environnement \"Environnement de l\'enseignant et l\'environnement de l\'�tudiant\" sur l\'�cran initial de AdaptWeb.");
	  //Apresenta��o
	define('A_LANG_INDEX_PRESENTATION_INFABOUT', "l\'information sur le projet");
	define('A_LANG_DOWNLOAD', 'T�l�charger');
	define('A_LANG_PUBLICATIONS', 'Publications');
	define('A_LANG_INDEX_PRESENTATION_AVAILABLEAT','Disponible �:');
	

	
	//Ambiente do Aluno - Entrada
	  //Principal
	define('A_LANG_LENVIRONMENT',"El�ve de l\'environnement");
	define('A_LANG_LENVIRONMENT_DESCRIPTION', "Cet environnement permet � l\'�tudiant l\'acc�s au mat�riel p�dagogique pour vous (s) professeur (s) pour l\'adapter � votre profil.");
	define('A_LANG_LENVIRONMENT_WARNING', "Pour avoir acc�s � votre discipline, l\'�tudiant doit d\'abord faire votre inscription afin de vous connecter � l\'environnement afin de demander l\'inscription de votre discipline. C\'est seulement apr�s la lib�ration de l\'acc�s par l\'enseignant responsable qui auront acc�s � la m�me discipline.");
	
	define('A_LANG_LENVIRONMENT_WATCH_LIBERATED', 'Disciplines lib�r�');
	define('A_LANG_LENVIRONMENT_WATCH_MINE', 'Disciplines propres (parcourir par cours)');
	define('A_LANG_LENVIRONMENT_WATCH_MY', 'Mon disciplines');
	define('A_LANG_LENVIRONMENT_WATCH_NOT', "Il n\'ya pas de disciplines enregistr�");
	define('A_LANG_LENVIRONMENT_WATCH_WARNING', "Demande d\'enregistrement pour l\'acc�s aux disciplines.");
	define('A_LANG_LENVIRONMENT_WATCH_VISUAL', 'Voir la discipline pour chaque cours');
	define('A_LANG_LENVIRONMENT_WATCH_CREATE', "Cr�er et g�rer les contenus des disciplines (G�n�rer du contenu), professeur de l\'environnement (point <b> Structuration de contenu => Liste des concepts </b>) pour acc�der � la navigation environnement.");
	define('A_LANG_LENVIRONMENT_WATCH_OBS', "Pour visualiser les disciplines propres n'est pas n�cessaire de tester l\'environnement de sortie de discipline de la navigation.");
	define('A_LANG_LENVIRONMENT_REQUEST', "Demande d\'inscription");
	define('A_LANG_LENVIRONMENT_REQUEST_REQ', 'Demande');
	define('A_LANG_LENVIRONMENT_REQUEST_NOTC', "Il n\'y a pas de cours enregistr�s");
	define('A_LANG_LENVIRONMENT_REQUEST_NOTD', "Il n\'y a pas de discipline inscrite pour le cours choisi");
	define('A_LANG_LENVIRONMENT_REQUEST_SELECT', "�lectionnez � la demande de l\'enregistrement:");
	define('A_LANG_LENVIRONMENT_REQUEST_MATR', 'immatricul�');
	define('A_LANG_LENVIRONMENT_WAITING', "En attente de la lib�ration de l\'enseignant");
	define('A_LANG_LENVIRONMENT_WAITING_NOT', "Il n\'y a pas de demandes d\'enregistrement");
	define('A_LANG_LENVIRONMENT_WAITING_WARNING', "Demande d\'enregistrement pour l\'acc�s des disciplines:");
	define('A_LANG_LENVIRONMENT_NAVTYPE', 'Type de navigationo');
	define('A_LANG_LENVIRONMENT_FORCOURSE', 'pour le cours de');
	define('A_LANG_LENVIRONMENT_NETCONECTION', 'Connexion r�seau');
	define('A_LANG_LENVIRONMENT_SPEED', 'La vitesse est');
	define('A_LANG_LENVIRONMENT_NAVIGATION', 'Navegation');
	define('A_LANG_LENVIRONMENT_TUTORIAL', 'Tutorial');
	define('A_LANG_LENVIRONMENT_FREE', 'libre');
	// Inclus�o de mesagens de Usabilidade - Carla estagio UDESC 2008/02
	define('A_LANG_LENVIRONMENT_TUTORIAL_DESC', "Navigation facilit�e par le pr�-requis �tablis par l\'enseignant."); // carla
       define('A_LANG_LENVIRONMENT_FREE_DESC', "Le navigation est ouverte � tous les concepts (sans prendre en compte les suggestions de l\'enseignant).");//carla
       define('A_LANG_LENVIRONMENT_PLUS', 'En savoir plus sur les types de navigation');//CARLA
	define('A_LANG_LENVIRONMENT_NAVIGATION_EXPLAIN', 'En fait Adaptweb offre deux modes de navigation pour une discipline (Guide de visite guid�e et visite libre). La diff�rence entre eux est comme la navigation par discipline. Mais le m�me contenu sont pr�par�s pour les deux modes de navigation.'); //Carla- explicacao no modo de navegacao - a_navega e n_navega
	// Final Carla
	
	//Ambiente do professor
	define('A_LANG_TENVIRONMENT_DESCRIPTION', "Le module permet � l\'auteur � l\'auteur de contenu sur mesure pour fournir des profils diff�rents des �l�ves. Gr�ce au processus de la paternit�, l\'auteur peut fournir le contenu de leurs cours dans une structure unique, adapt� aux diff�rents cours. � ce stade, l\'auteur doit s\'inscrire � la discipline et des cours qui veulent les rendre disponibles. Apr�s l\'inscription, ins�rer le concept du contenu des dossiers, relatifs � chaque sujet de la discipline. En outre, l\'auteur doit informer la description du sujet, et de ses pr�-requis de cours qui veulent offrir du contenu. L\'auteur peut ins�rer des exercices, des exemples et des documents suppl�mentaires pour chaque sujet.");
	define('A_LANG_TENVIRONMENT_AUTHOR', 'Auteur');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR1', "Il y avait une erreur lorsque vous vous inscrivez dans cette discipline. Cet utilisateur doit �tre inscrit ou d\'une erreur s\'est produite sur le serveur");
	define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR2', "Une erreur est survenue lors de la suppression de l\'utilisateur est l\'enregistrement de ce cours. Cet utilisateur ne devrait pas �tre enregistr�e ou une erreur s\'est produite sur le serveur");
	define('A_LANG_TENVIRONMENT_AUTHORIZE_NOT', "Il n\'y a pas actuellement inscrits ");
	define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECT', 'Choisissez le cours');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_NOTD', "Il n\'y a pas de discipline inscrits � ce cours");
	define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECTD', 'Choisissez une discipline');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_LIST', 'Liste des �tudiants:');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_MATR', 'Les �tudiants inscrits:');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_BACK', 'retour');
	define('A_LANG_TENVIRONMENT_LIBERATE_DESCRIPTION', "<b> Note: </b> Uniquement disponible pour les sujets qui sont disponibles avec un contenu correct. Pour v�rifier le contenu des sujets pour acc�der � la <b> CONCEPT </b> sur le point <b> STRUCTURE DE CONTENU </b> et d\'utiliser les <b> de contenu </b>.");
	define('A_LANG_TENVIRONMENT_LIBERATE_LIB', 'lib�rer <br> discipline:');
	define('A_LANG_TENVIRONMENT_LIBERATE_LIB_DIS', 'Disciplines <br> lib�r�:');
	define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE', 'd�placer concept');
	define('A_LANG_TENVIRONMENT_ESTRUCT_EXCLUDE', 'supprimer concept');
	define('A_LANG_TENVIRONMENT_ESTRUCT_SELECT', 'Choisissez le concept que vous souhaitez <B>BOUGER </ B> ou <B> EFFACER');
	define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE_AFTER', '<b> BOUGER </b> s�lectionnez le concept apr�s concept qui?');
	define('A_LANG_TENVIRONMENT_ESTRUCT_CANCEL', "de d�placer ou de supprimer le concept op�ration se traduira par l\'annulation des conditions pr�alables");
	define('A_LANG_TENVIRONMENT_ESTRUCT_NOT_POSSIBLE', 'incapable de lire la matrice de la base de donn�es');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND', 'Cl� pr�-requis:');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND1', "Pour s�lectionner plusieurs conditions avant d\'utiliser la touche CTRL");
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND2', 'Pour retirer Pr�-requis  de s�lection en utilisant CTRL');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND3', 'Les conditions dans <font class=buttonselecionado> bleu </font> sont s�lectionn�s');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND4', "Le fond <font class=buttonpre> Le fond bleu </font> indique la pr�-requis (s\'il ya perte de la s�lection)");
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND5', 'Le fond <font class=buttonpreautomatico> <font </font> indique les conditions actuelles automatique');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NUMBER', 'Nombre de concept:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NAME', "Nom de l\'exemple");
	define('A_LANG_TENVIRONMENT_EXAMPLES_DESCRIPTION', "Description de l\'exemple:");
	define('A_LANG_TENVIRONMENT_EXAMPLES_KEY', "Mots-cl�s de l\'exemple");
	define('A_LANG_TENVIRONMENT_EXAMPLES_LEVEL', 'Niveau de complexit�:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NOCLASS', 'Sans Classification');
	define('A_LANG_TENVIRONMENT_EXAMPLES_EASY', 'Facile');
	define('A_LANG_TENVIRONMENT_EXAMPLES_MEDIUM', 'Moyen');
	define('A_LANG_TENVIRONMENT_EXAMPLES_COMPLEX', 'Complexe');
	define('A_LANG_TENVIRONMENT_EXAMPLES_DSC', 'Description');
	define('A_LANG_TENVIRONMENT_EXAMPLES_LVL', 'Niveau');
	define('A_LANG_TENVIRONMENT_EXERCISES_NAME', "Nom de l\'exercice::");
	define('A_LANG_TENVIRONMENT_EXERCISES_DESCRIPTION', "Description de l\'exercice:");
	define('A_LANG_TENVIRONMENT_EXERCISES_KEY', "mot-cl� de l\'exercice:");
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_NAME', 'Nom de <br> compl�mentaires mat�riel:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_DESCRIPTION', 'Description du mat�riel <br> Suppl�mentaire:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_KEY', 'Mots-cl�s <br> de mat�riel suppl�mentaire:');
	define('A_LANG_TENVIRONMENT_ACCESS', 'Environnement Acc�s Auteur');
	define('A_LANG_TENVIRONMENT_WAIT', "Attendez que l\'administrateur de l\'environnement, le libre acc�s � l\'environnement de l\'auteur.");
	define('A_LANG_TENVIRONMENT_COMMENT', "Acc�s � l\'environnement de l\'auteur est g�r� par l\'administrateur de l\'environnement AdaptWeb. Une fois que vous vous inscrivez sur l\'environnement est n�cessaire pour permettre � l\'administrateur d'acc�der � l\'environnement de la paternit�.");
	define('A_LANG_TENVIRONMENT_DEMO', 'D�monstration des environnements');
	define('A_LANG_TENVIRONMENT_ASSOCIATED_FILES','Note: les fichiers associ�s doivent �tre envoy�es au serveur par le maintien de');
	// Inclus�o de mensagens usabilidade - Carla estagio UDESC 2008/02
       define("A_LANG_TENVIRONMENT_EXAMPLES_OBS","<b> * Note: </b> Pour assurer la discipline, vous devez remplir tous les champs de cette page correctement.");//Carla inseriu para colocar uma observa��o na aba de exemplos.
       define("A_LANG_TENVIRONMENT_EXERC","Pour en cr�er un ou plusieurs exercices est n�cessaire pour remplir tous les champs et de transf�rer les fichiers de <b>. Html </b> (par exemple exerc1.html, exerc2.html). Si ce fichier est une image, vous devez envoyer le m�me.");//Carla inseriu para colocar uma introdu��o na aba de material complementar");//carla
       define("A_LANG_TENVIRONMENT_EXAMPLES","Pour en cr�er un ou plusieurs exemples, nous devons remplir dans tous les domaines et transf�rer les fichiers de <b>. Html </b> (par exemple exemplo1.html, exemplo2.html). Si ce fichier est une image, vous devez envoyer le m�me.");//Carla inseriu para colocar uma introdu��o na aba de exemplos.
       define("A_LANG_TENVIRONMENT_COMPLEMENTARY_DESC","Pour en cr�er un ou plusieurs autres mati�res est n�cessaire pour remplir tous les champs et de transf�rer les fichiers de <b>. Html </b> (par exemple mat1.html, mat2.html). Si ce fichier est une image, vous devez envoyer le m�me.");//Carla inseriu para colocar uma introdu��o na aba de material complementar.
	// Final Carla


	//Op��es de Root
	define('A_LANG_ROOT_NOT_AUTH', 'Enseignant <br> est pas autoris�e:');
	define('A_LANG_ROOT_AUTH', 'Enseignant <br> est permis:');
	define('A_LANG_ROOT_DEV', 'cette option est en cours de d�veloppement.');

	//Outros
	define('A_LANG_DATA', 'Date');
	define('A_LANG_WARNING', 'Avertissement');
	define('A_LANG_COORDINATOR', 'COORDINATOR');
	define('A_LANG_DATABASE_PROBLEM', 'Il ya quelques probl�mes avec la base de donn�es');
	define('A_LANG_FAQ', 'FAQ');
	define('A_LANG_WORKS', "Offres d'emploi");
	define('A_LANG_NOT_DISC', 'Impossible de montrer les disciplines enregistr�');
	define('A_LANG_NO_ROOT', "Impossible d\'ajouter l\'utilisateur root");
	define('A_LANG_MIN_VERSION', 'Le minimum de la version de base de donn�es [');
	define('A_LANG_INSTALLED_VERSION', '] Et une version est install�e [');
	define('A_LANG_ERROR_BD', "Impossible d'�crire la matrice dans la base de donn�es");
	define('A_LANG_DISC_NOT_FOUND', 'Nom de discipline ne trouve pas ou blanc');
	define('A_LANG_SELECT_COURSE', 's�lectionner le cours sur le concept <b>');
	define('A_LANG_TOPIC_REGISTER', 'Enregistrement des concepts');
	define('A_LANG_SUMARIZED_DESCRIPTION', 'Br�ve description du concept');
	define('A_LANG_LOADED', 'envoy�s sur le serveur');
	define('A_LANG_FILL_DESCRIPTION', 'compl�te dans le champ de description du mat�riel');
	define('A_LANG_WORKS_SEND_DATE', ' Fichier �t� envoy� sur');
	define('A_LANG_SEARCH_RESULTS', 'R�sultats de la recherche');
	define('A_LANG_SEARCH_SYSTEM', 'Syst�me de recherche');
	define('A_LANG_SEARCH_WORD', 'Mots-cl�s de recherche');
	define('A_LANG_SEARCH_FOUND', 'Recherche R�sultats');
	define('A_LANG_SEARCH_OCCURRANCES', ' occurrences de');
	define('A_LANG_SEARCH_IN_TOPIC', 'dans le concept');
	define('A_LANG_SEARCH_FOUND1', 'il a �t� constat�');
	define('A_LANG_SEARCH_OCCURRANCE', 'occurrences de');
	define('A_LANG_SEARCH_NOT_FOUND', "Il n'ya pas eu de cas de");
	define('A_LANG_SEARCH_CLOSE', 'terminer');
	define('A_LANG_TOPIC', 'Concept');
	define('A_LANG_SHOW_MENU', 'Voir Menu');
	define('A_LANG_CONFIG', 'configurations');
	define('A_LANG_MAP', 'Carte');
	define('A_LANG_PRINT', 'Imprimer cette page?');
	define('A_LANG_CONNECTION', "Impossible de se connecter au serveur de base de donn�es MySQL S\'il vous pla�t contacter l\'administrateur.");
	define('A_LANG_CONTACT', "S'il vous pla�t contactez l'administrateur");
	define('A_LANG_HELP_SYSTEM', "Syst�me d'aide");
	define('A_LANG_CONFIG_SYSTEM', 'Configuration du syst�me');
	define('A_LANG_CONFIG_COLOR', 'Configuration de la couleur du fond');
	define('A_LANG_NOT_CONNECTED', "Il n'est pas connect�");
	define('A_LANG_DELETE_CADASTRE', "Supprimer l'inscription d'�tudiants");
	define('A_LANG_HOME', 'Home');
	define('A_LANG_YOU_HAVE', 'vous avez');
	define('A_LANG_TO_CONFIRM', "l'enregistrement. Pour confirmer cette exclusion sur");
	define('A_LANG_CONFIRM', 'Confirmer');
	define('A_LANG_EXAMPLE_LIST', 'Exemple de liste');
	define('A_LANG_COMPLEXITY_LEVEL_EASY', "Niveau de complexit�, c'est facile");
	define('A_LANG_COMPLEXITY_LEVEL_MEDIUM', 'Niveau de complexit� est moyenne');
	define('A_LANG_COMPLEXITY_LEVEL_COMPLEX', 'Niveau de complexit� est difficile');
	define('A_LANG_EXERCISES_LIST', 'Liste des exercices');
	define('A_LANG_REGISTER_MAINTENENCE', 'Entretien des enregistrements');
	define('A_LANG_REGISTER_RELEASE', "Communiqu� d'inscription");
	define('A_LANG_SITE_MAP', 'Plan du site');
	define('A_LANG_COMPLEMENTARY_LIST', 'Liste du mat�riel suppl�mentaire');
	define('A_LANG_DELETE_TOPIC', 'concept a �t� supprim�');
	define('A_LANG_AND', 'et');
	define('A_LANG_SONS', 'subconcepts');
	define('A_LANG_CONFIRM_EXCLUSION', 'Ne vous s�r de vouloir supprimer la discipline');
	define('A_LANG_YES', 'Oui');
	define('A_LANG_NO', 'Non');
	// Inclusao mensagens usabilidade - Carla estagio UDESC 2008/02
	 define('A_LANG_CONFIRM_EXCLUSION_CURSO', 'Ne vous s�r de vouloir supprimer la discipline');
	// Final Carla

	
	//Gera��o de conte�do
	define('A_LANG_GENERATION_END', "Fin de la g�n�ration d\'un fichier xml d\'�l�ments du sujet");
	define('A_LANG_GENERATION_CREATED', 'Il a �t� cr��');
	define('A_LANG_GENERATION_XML', 'xml. fichiers');
	define('A_LANG_GENERATION_END_STRUCT', 'Afin de g�n�rer la structure XML des sujets');
/*	define('A_LANG_GENERATION_MISSING_FILE', 'Faltando inserir arquivo de conceito do ');
	define('A_LANG_GENERATION_TH_TOPIC', 'conceito ');
	define('A_LANG_GENERATION_MISSING_COURSE', 'Faltando inserir curso do ');
	define('A_LANG_GENERATION_NO_ASOC', 'Conceito sem curso associado');
	define('A_LANG_GENERATION_MISSING_EXERC_ID', 'Faltando inserir identificador de exercicio do ');
	define('A_LANG_GENERATION_TH_EXERCISE', '� exerc�cio do ');
	define('A_LANG_GENERATION_MISSING_EXERC_DESCRIPTION', 'Faltando inserir descri��o de exercicio do ');
	define('A_LANG_GENERATION_MISSING_EXERC_FILE', 'Faltando inserir arquivo de exercicio do ');
	define('A_LANG_GENERATION_MISSING_EXERC_COMPLEXITY', 'Faltando inserir complexidade de exercicio do ');
	define('A_LANG_GENERATION_TH_EXAMPLE', '� exemplo do ');
	define('A_LANG_GENERATION_MISSING_EXAMP_ID', 'Faltando inserir identificador de exemplo do ');
	define('A_LANG_GENERATION_MISSING_EXAMP_DESCRIPTION', 'Faltando inserir descricao de exemplo do ');
	define('A_LANG_GENERATION_MISSING_EXAMP_FILE', 'Faltando inserir arquivo de exemplo do ');
	define('A_LANG_GENERATION_MISSING_EXAMP_COMPLEXITY' ,'Faltando inserir complexidade de exemplo do ');
	define('A_LANG_GENERATION_TH_MATCOMP', '� material complementar do ');
	define('A_LANG_GENERATION_MISSING_MATCO_ID', 'Faltando inserir identificador de material complementar do ');
	define('A_LANG_GENERATION_MISSING_MATCO_DESCRIPTION', 'Faltando inserir descricao de material complementar do ');
	define('A_LANG_GENERATION_MISSING_MATCO_FILE', 'Faltando inserir arquivo de material complementar do ');
*/	define('A_LANG_GENERATION_VERIFY', 'V�rifier les donn�es des auteurs');
	define('A_LANG_GENERATION_PROBLEMS', "Probl�mes avec l'enregistrement de la Discipline");
/*	define('A_LANG_GENERATION_MISSING_TOPIC_NUMBER', 'Faltando inserir Numero do Topico para o ');
	define('A_LANG_GENERATION_MISSING_TOPIC_DESCRIPTION', 'Faltando inserir Descri��o do Topico para o ');
	define('A_LANG_GENERATION_MISSING_TOPIC_ABREV', 'Faltando inserir Abrevia��o do Topico para o ');
	define('A_LANG_GENERATION_MISSING_TOPIC_KEYWORD', 'Faltando inserir Palavra Chave do Topico para o ');
*/	define('A_LANG_GENERATION_RESULT_XML', 'R�sultat de la g�n�ration de XML');
	define('A_LANG_GENERATION_NO_ERROR', 'NO ERROR');
	define('A_LANG_GENERATION_SUCCESS', 'LES FICHERS G�N�R�S COURS AVEC SUCCES');
	define('A_LANG_GENERATION_DATA_LACK', "Ne sont pas des fichiers XML g�n�r�s par l'absence de donn�es");
	define ('A_LANG_GENERATION_TOPIC','Concept');
	define ('A_LANG_GENERATION_EXAMPLE','Exemple');
	define ('A_LANG_GENERATION_EXERCISE','Exercice');
	define ('A_LANG_GENERATION_COMPLEMENTARY','upplementary Material');
	define ('A_LANG_GENERATION_TOPIC_NUMBER','certain nombre de th�mes');
	define ('A_LANG_GENERATION_TOPIC_NAME','Nom du th�mes');
	define ('A_LANG_GENERATION_DESCRIPTION','Description');
	define ('A_LANG_GENERATION_KEY_WORD','mot-cl�');
	define ('A_LANG_GENERATION_COURSE','Cour');
	define ('A_LANG_GENERATION_PRINC_FILE','Principal fichier');
	define ('A_LANG_GENERATION_ASSOC_FILE','fichiers associ�s');
	define ('A_LANG_GENERATION_MISSING','Manquant');
	define ('A_LANG_GENERATION_NO_PROBLEM','Ok');	
	define ('A_LANG_GENERATION_NUMBER','Nombre');
	define ('A_LANG_GENERATION_COMPLEXITY','Niveau');
	define ('A_LANG_GENERATION_NOT_EXIST','ne pas exister');

	//Reposit�rio
	define ('A_LANG_MNU_REPOSITORY','Repository');
	define ('A_LANG_PRESENTATION','Pr�sentation');
	define ('A_LANG_EXPORT','Exportation des Disciplines');
	define ('A_LANG_IMPORT','Importation de Disciplines');
	define ('A_LANG_OBJECT_SEARCH',"Recherche d'objets d'apprentissage");
	define ('A_LANG_EXP_TEXT', 'Envie suas disciplinas para o Reposit�rio para que outros professores possam utiliz�-la tamb�m.');
	define ('A_LANG_IMP_TEXT', 'Utilize disciplinas dispon�veis no seu ambiente AdaptWeb.');
	define ('A_LANG_SCH_TEXT', 'Os dados das Disciplinas do AdaptWeb tamb�m foram indexados no formato de Objetos de Aprendizagem utilizando metadados do padr�o LOM (Learning Object Metadata). Fa�a buscas por objetos de aprendizagem abrangendo disciplinas, t�picos (conceitos) e arquivos do reposit�rio.');
	define ('A_LANG_REP_TEXT', 'Bem vindo ao Reposit�rio de Disciplinas do AdaptWeb.<br>Atrav�s deste reposit�rio, usu�rios do AdaptWeb de diferentes lugares poder�o compartilhar dados para cria��o de suas disciplinas. Aqui voc� pode fazer importa��o e exporta��o de disciplinas, al�m de buscar objetos de aprendizagem.');
	define ('A_LANG_REP_SELECT', 'Choisissez une discipline');
	define ('A_LANG_REP_EXP_SUCCESS', "L'exportation a �t� un succ�s!");
	define ('A_LANG_REP_EXP_ERROR', "Erreur d'exportation discipline!");
	define ('A_LANG_REP_EXP_TEXT', 'A trav�s de esta interfaz se puede hacer de sus temas AdaptWeb para que otros puedan utilizarlas. <br> Seleccione la disciplina de la lista a continuaci�n, para especificar una descripci�n y haga clic en "Exportar".');
	define ('A_LANG_REP_EXPORT', 'exporter');
	define ('A_LANG_REP_SELECT_COURSE', 'S�lectionnez un cour');
	define ('A_LANG_REP_IMP_SUCCESS', "L'importation a �t� un succ�s!");
	define ('A_LANG_REP_IMP_ERROR', "Erreur dans l'importation de la discipline!");
	define ('A_LANG_REP_INVALID_PAR', 'Les param�tres sont invalides!');
	define ('A_LANG_REP_IMP_TEXT', 'A trav�s de esta interfaz puede copiar disciplinas disponibles en el repositorio para su entorno de autor�a en AdaptWeb! <br> Seleccione la disciplina de la siguiente lista, seleccione el curso que est� vinculado, haga clic en "Importar".');
	define ('A_LANG_REP_DISC_IN_REP', 'disciplines dans le repository');
	define ('A_LANG_REP_MY_COURSES', 'Mes cours');
	define ('A_LANG_REP_OTHER_NAME', 'Import avec un autre nom');
	define ('A_LANG_REP_IMPORT', 'importer');
	define ('A_LANG_REP_DISC_DATA', 'Donn�es de la Discipline');
	define ('A_LANG_REP_SEARCH', 'Entrez une cha�ne de caract�res � rechercher et � s�lectionner les m�tadonn�es � inclure dans votre recherche:');
	define ('A_LANG_REP_SCH_TITLE', 'Titre');
	define ('A_LANG_REP_SCH_SELECT', 'S�lectionnez');
	define ('A_LANG_REP_AGGREG', "Niveau d'agr�gation");
	define ('A_LANG_REP_SCH_RAW', 'Les donn�es (fichiers)');
	define ('A_LANG_REP_SCH_TOPIC', 'Concept');
	define ('A_LANG_REP_SCH_DISC', 'Discipline');
	define ('A_LANG_REP_SCH_COURSE', 'Cour');
	define ('A_LANG_REP_SCH_SEARCH', 'Recherche');
	define ('A_LANG_REP_SCH_ERROR', 'Erreur');
	define ('A_LANG_REP_SCH_NO_REG', 'Aucun registre a �t� trouv�e!');
	define ('A_LANG_REP_SCH_REG', 'registre trouv�');
	define ('A_LANG_REP_SCH_REGS', 'registres trouv�s');
	define ('A_LANG_REP_DISC_NOT_FOUND', 'discipline introuvable');
	define ('A_LANG_COURSES', 'Cours');
	define ('A_LANG_EXP_BY', "L'exportation par");
	define ('A_LANG_ERROR_SELECT_BD', 'Erreur dans la s�lection de la base de donn�es
');

	
/******************************************************************************************** 
 * Avalia��o (Definido por Claudiomar Desanti)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus&atilde;o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 */ 
define("A_LANG_MNU_AVS", "Fonctions d'�valuation");
define("A_LANG_MNU_AVS_NAV", "�valuation"); // menu na parte de navega&ccedil;&atilde;o
define("A_LANG_TOPIC_EVALUATION","�valuation"); // orelha de avaliacao

define("A_LANG_AVS_STUDENTS_TOPIC_EVALUATION", "Evaluation lib�r�"); // t&oacute;pico da avalia&ccedil;&atilde;o
define("A_LANG_AVS_STUDENTS_TOPIC_RESULTS", "Tableau des r�sultats"); // t&oacute;pico de quadro de resultados

define("A_LANG_AVS_FUNCTION_INTRO","Introduction");
define("A_LANG_AVS_FUNCTION_AVERAGE","Moyenne");
define("A_LANG_AVS_FUNCTION_ADJUSTMENT","R�gler la note");
define("A_LANG_AVS_FUNCTION_LIBERATE","Lib�rer �valuation");
define("A_LANG_AVS_FUNCTION_DIVULGE","Divulgation </br> note");
define("A_LANG_AVS_FUNCTION_REPORT","Rapport");
define("A_LANG_AVS_FUNCTION_BACKUP","Backup");


define("A_LANG_AVS_TITLE_ADJUSTMENT_AVERAGE","Moyenne d'ajustement");
define("A_LANG_AVS_TITLE_ADJUSTMENT_QUESTION_CANCEL","Annuler Questions");
define("A_LANG_AVS_TITLE_ADJUSTMENT","R�gler des notes");
define("A_LANG_AVS_TITLE_ADJUSTMENT_QUESTION_WEIGHT","Distribuer le poids des �valuations");
define("A_LANG_AVS_TITLE_DIVULGE","Disclouse de notes de l'�valuation");
define("A_LANG_AVS_TITLE_NEW_AVAL", "Nouvelle �valuation");
define("A_LANG_AVS_TITLE_NEW_AVAL_PRES", "Nouvelle �valuation pr�sentiel");
define("A_LANG_AVS_TITLE_GROUP","Nouveau groupe");
define("A_LANG_AVS_TITLE_LIBERATE","Lib�rer �valuation");
define("A_LANG_AVS_TITLE_AVERAGE","distribution des moyennes");
define("A_LANG_AVS_TITLE_REPORT", "Rapport");
define("A_LANG_AVS_TITLE_BACKUP", "Backup");
define("A_LANG_AVS_TITLE_DISSERT","correction de s'�tendre questions");
define("A_LANG_AVS_TITLE_ADJUSTMENT_PRESENTIAL","ajustement de la note pr�sentiel questions");
define("A_LANG_AVS_TITLE_ADD_QUESTION","Ajouter existants questions");

// -------------- ERROS ------------------------------------------------- //
define("A_LANG_AVS_ERROR_DB_OBJECT","Boot pas, l'objet de connexion � l'acc�s de base de donn�es est invalide");
define("A_LANG_AVS_ERROR_DB_SAVE","Erreur dans la sauvegarde des informations");
define("A_LANG_AVS_ERROR_PARAM","<div class='erro'> <p> erreur en passant des param�tres, de recharger la page </p> </div>");

define("A_LANG_AVS_ERROR_VERIFY_AUTHO","<div class='erro'> <p> vous ne pouvez pas effectuer cette partie du syst�me = </p> </div>");
define("A_LANG_AVS_ERROR_EVALUATION_DELETE","impossible de supprimer la question de l'�valuation");


define("A_LANG_AVS_ERROR_EVALUATION_AVERAGE","aucune �valuation n'a �t� effectu�e ou lib�r�s");
define("A_LANG_AVS_ERROR_FUNCTION_EVALUATION_LIBERATE","l'incapacit� du chargement de la question");
define("A_LANG_AVS_ERROR_QUESTION_OBJECT","l'incapacit� du chargement de la question");
define("A_LANG_AVS_ERROR_ANSWER_OBJECT","Impossible de charger les r�ponses des questions");
define("A_LANG_AVS_ERROR_LOGGED","VVous devez vous connecter dans le syst�me de premier acc�s � ce module");
define("A_LANG_AVS_ERROR_STUDENTS_LIBERATE","erreur lors du chargement de l'�valuation");
define("A_LANG_AVS_ERROR_STUDENTS_VIEW","Cette �valuation n'est pas la v�tre");
define("A_LANG_AVS_ERROR_EVALUATION_LIBERATE","Il n'a pas �t� trouv� de l'�valuation");
define("A_LANG_AVS_ERROR_EVALUATION_LIBERATE_STUDENTS","�tudiants n'est pas sorti de l'�valuation");
define("A_LANG_AVS_ERROR_EVALUATION_LOGGED","Vous devez �tre connect� pour acc�der � l'�valuation");
define("A_LANG_AVS_ERROR_EVALUATION_INIT","�tudiants n'est pas sorti de l'�valuation");
define("A_LANG_AVS_ERROR_EVALUATION_SUBMIT","L'�valuation a �t� ferm� par l'enseignant");
define("A_LANG_AVS_ERROR_AUTHO","Vous devez �tre connect� pour acc�der � l'�valuation");
define("A_LANG_AVS_ERROR_TOPIC_EVALUATION","");
define("A_LANG_AVS_ERROR_LIBERATE_NOT_STUDENTS", "aucun �tudiant est inscrit dans cette discipline de faire les �valuations");
define("A_LANG_AVS_ERROR_DIVULGE_VALUE","Incapable disclouse les notes");
define("A_LANG_AVS_ERROR_DIVULGE_NOT_EVALUATION","Aucune �valuation a �t� achev� d'�tre divulgu�s");
define("A_LANG_AVS_ERROR_LIBERATE_EVALUATION","l'impossibilit� d'achever la lib�ration de l'�valuation");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT","aucune �valuation n'a �t� trouv�");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_SAVE","erreur dans la salve de la question");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_AVERAGE","erreur dans le calcul de la note");
define("A_LANG_AVS_ERROR_AVERAGE_CALCULE","erreur dans le calcul de la moyenne");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_STUDENT","�tudiants n'ont pas r�pondu � la question de l'�valuation");
define("A_LANG_AVS_ERROR_ADJUSTMENT_PRESENTIAL","la note n'a pas �t� r�v�l�, parce que certains �tudiants n'ont pas de note");
define("A_LANG_AVS_ERROR_ADD_QUESTION_EXIST","Erreur dans l'ajout de nouvelles questions en mati�re d'�valuation");
define("A_LANG_AVS_ERROR_QUESTION_NOT_FOUND","pas de r�sultats a �t� fond�e");
define("A_LANG_AVS_ERROR_NOT_STUDENTS_LIBERATE","Pas les �tudiants inscrits dans cette discipline");
define("A_LANG_AVS_ERROR_EVALUATION_FINISH","l'�valuation a �t� d�j� livr�s.");
define("A_LANG_AVS_ERROR_EVALUATION_FINISH2","l'�valuation a �t� d�j� termin�, vous ne pouvez pas rendre plus");
define("A_LANG_AVS_ERROR_SESSION_BROWSER","la session que vous avez commenc� l'�valuation n'est plus le m�me <br/> mai ont eu lieu par l'�change � un autre ordinateur ou navigateur.");
define("A_LANG_AVS_ERROR_FUNCTION_CANCEL","Vous ne pouvez pas annuler les questions. doit avoir au moins une question qui n'est pas annul�e");


define("A_LANG_AVS_ERROR_QUESTION_LOGGED","vous devez �tre identifi� � l'acc�s de ce module");
define("A_LANG_AVS_ERROR_QUESTION_EVALUATION","la question ne peut pas �tre ajout� � cette �valuation");
define("A_LANG_AVS_ERROR_QUESTION_LACUNA_DELETE","erreur de supprimer le vide");
define("A_LANG_AVS_ERROR_QUESTION_ME_DELETE","incapable de supprimer l'alternative");
// erros utilizados no ajax
define("A_LANG_AVS_ERROR_AJAX_AVERAGE","l'�valuation n'a pas �t� d�fini pour ce cour");
define("A_LANG_AVS_ERROR_AJAX_AVERAGE1","�tudiants ne se rendent pas compte des �valuations");
define("A_LANG_AVS_ERROR_AJAX_DIVULGE1","");
define("A_LANG_AVS_ERROR_AJAX_DIVULGE2","Personne n'a r�alis� cette �valuation, cette �valuation a �t� annul�. L'�tudiant devra faire une autre �valuation");
define("A_LANG_AVS_ERROR_AJAX_LIBERATE","Aucune �valuation est disponible pour r�soudre les");
define("A_LANG_AVS_ERROR_AJAX_LIBERATE2","Pas de discipline a �t� ferm�");
define("A_LANG_AVS_ERROR_AJAX_AVERAGE3","Cette discipline ne sont pas de groupe de avaliation pour les moyennes peuvent �tre disponibles.");
// ------------------- FIM DOS ERROS -----------------------------------------------------//

define("A_LANG_AVS_SUCESS", "Enregistrer avec succ�s!");
define("A_LANG_AVS_SUCESS_DIVULGE","ALes notes d'�valuation a �t� lib�r� pour les �l�ves");
define("A_LANG_AVS_SUCESS_LIBERATE","Evaluation a �t� lib�r� avec succ�s");
define("A_LANG_AVS_SUCESS_EVALUATION","L'�valuation a �t� livr� avec succ�s");
define("A_LANG_AVS_STUDENTS","Etudiants");

// -------------- BOTOES -----------------------------------------------------------------//
define("A_LANG_AVS_BUTTON_SAVE","Sauver");
define("A_LANG_AVS_BUTTON_SAVE_COPY_DEFAULT","Copie par d�faut de poids");
define("A_LANG_AVS_BUTTON_SAVE_COPY_MOD","Copie de modification du poids");
define("A_LANG_AVS_BUTTON_SUBMIT","D�poser �valuation");
define("A_LANG_AVS_BUTTON_BACK",A_LANG_TENVIRONMENT_AUTHORIZE_BACK);
define("A_LANG_AVS_BUTTON_HELP",A_LANG_HELP);
define("A_LANG_AVS_BUTTON_NEW_GROUP","Nouveau groupe");
define("A_LANG_AVS_BUTTON_CHANGE_OPTION","Changement des param�tres");
define("A_LANG_AVS_BUTTON_NEW_EVALUATION","Nouvelle �valuation");
define("A_LANG_AVS_BUTTON_WEIGHT","R�partissez le poids aussi");
define("A_LANG_AVS_BUTTON_NEXT","Suivant");
define("A_LANG_AVS_BUTTON_LIBERATE_EVALUATION","Liberate nouvelle �valuation");
define("A_LANG_AVS_BUTTON_FINISH_EVALUATION","Terminer l'�valuation");
define("A_LANG_AVS_BUTTON_LIST_STUDENTS","Liste des �tudiants");
define("A_LANG_AVS_BUTTON_CLOSE","Fermer");
define("A_LANG_AVS_BUTTON_SEARCH","recherche");
define("A_LANG_AVS_BUTTON_MARK","Marquez tous");
define("A_LANG_AVS_BUTTON_UNMARK","D�sactiv tous");

// bot&otilde;es do AVS_TOPICO.PHP
define("A_LANG_AVS_BUTTON_GROUP_NEW","Nouveau groupe");
define("A_LANG_AVS_BUTTON_GROUP_PARAM","Changement des param�tres");
define("A_LANG_AVS_BUTTON_GROUP_NEW_EVALUATION","Ajouter �valuation");
define("A_LANG_AVS_BUTTON_DUPLICATE_AVAL","d'�valuation en double");
define("A_LANG_AVS_BUTTON_AVAL_PRESEN","jouter pr�sentiel �valuation");
// bot&otilde;es AVS_NOVA_AVAL.PHP
define("A_LANG_AVS_BUTTON_QUESTION_NEW","Ajouter une nouvelle question");
define("A_LANG_AVS_BUTTON_QUESTION_EXIST","Ajouter question existants");
// bot&otilde;es utilizados na libera&ccedil;&atilde;o da avalia&ccedil;&atilde;o
define("A_LANG_AVS_BUTTON_EVALUATION_LIBERATE_NEW","nueba lib�ration d'�valuation");
define("A_LANG_AVS_BUTTON_EVALUATION_LIBERATE_FINISH","Liberate �valuations fini");
define("A_LANG_AVS_BUTTON_BACK_LIBERATE","Retour � la page initiale Liberate");
// bot&otilde;es utilizados nas telas de quest&otilde;es
define("A_LANG_AVS_BUTTON_QUESTION_ADD_LACUNA","Ajouter de nouvelles Lacuna");
define("A_LANG_AVS_BUTTON_QUESTION_DELETE_LACUNA","Supprimer lacune");
define("A_LANG_AVS_BUTTON_QUESTION_ADD_ME","Ajouter de nouvelles alternatives");

// bot&otilde;es utilizados nos ajax
define("A_LANG_AVS_BUTTON_AJAX_AVERAGE_LIST","Liste des notes");
define("A_LANG_AVS_BUTTON_AJAX_DIVULGE","divulguer les notes");
define("A_LANG_AVS_BUTTON_AJAX_LIBERATE_END","fin d'�valuation");
define("A_LANG_AVS_BUTTON_AJAX_LIST","Liste des �tudiants");

// --------------- FIM DOS BOTOES ---------------------------------------------------- //  
// --------------- LABELS - texto ao lado de combobox e spans ------------------------ //
define("A_LANG_AVS_LABEL_COURSE",A_LANG_COURSES);
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_VALUE","Note");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_EXPLANATION","Explication");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_WEIGHT","poids");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_CORRECT","droit de r�ponse");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_OBS_TEACHER","Commentaire de l'enseignant");
define("A_LANG_AVS_LABEL_EVALUATION_MODEL","des mod�les d'�valuation");
define("A_LANG_AVS_LABEL_EVALUATION_MODEL_AUTO","Mod�les pour l'�valuation automatis�e");
define("A_LANG_AVS_LABEL_EVALUATION_MODEL_PRESENTIAL","Mod�les pour l'�valuation pr�sentiel");
define("A_LANG_AVS_LABEL_EVALUATION","�valuation");
define("A_LANG_AVS_LABEL_TOPIC","conception");
define("A_LANG_AVS_LABEL_VIEW_EVALUATION","Vue de l'�valuation par l'enseignant");
define("A_LANG_AVS_LABEL_YES","Oui");
define("A_LANG_AVS_LABEL_NOT","Non");
define("A_LANG_AVS_LABEL_INFO","Informations");
//

define("A_LANG_AVS_LABEL_QUESTION_DISSERT","question s'�tendre");
define("A_LANG_AVS_LABEL_QUESTION_ME","Choix multiple");
define("A_LANG_AVS_LABEL_QUESTION_LACUNAS","Remplissez lacune");
define("A_LANG_AVS_LABEL_QUESTION_VF","Vrai ou faux");

define("A_LANG_AVS_LABEL_FUNCTION","Fonctions");
define("A_LANG_AVS_LABEL_QUESTION","Questions");

// labels do AVS_GRUPO.PHP
define("A_LANG_AVS_LABEL_QUESTION_RANDOM","Les questions doivent �tre mixtes?");
define("A_LANG_AVS_LABEL_EVALUATION_PRESENT","�valuation pr�sentiel?");
define("A_LANG_AVS_LABEL_ANOTHER_GROUP","choisi par un autre groupe");
// labels do AVS_NOVA_AVAL.PHP
define("A_LANG_AVS_LABEL_DESCRIPTION","Description");
define("A_LANG_AVS_LABEL_DIVULGE_NOTE","voulez-vous divulguer le note automatiquement?");
define("A_LANG_AVS_LABEL_DISSERT_QUESTION","Il ya quelques questions � s'�tendre taux");
// labels do AVS_TOPICO.PHP
define("A_LANG_AVS_LABEL_GROUP_DESCRIPTION","Groupe de l'�valuation de concept");
define("A_LANG_AVS_LABEL_GROUP_NOT_WEIGHT","pas encore d�fini");
define("A_LANG_AVS_LABEL_GROUP_PARAM","Param�tres:");
define("A_LANG_AVS_LABEL_GROUP_QUESTION_RANDOM","mix questions:");
define("A_LANG_AVS_LABEL_GROUP_EVALUATION_PRESENT",A_LANG_AVS_LABEL_EVALUATION_PRESENT);
define("A_LANG_AVS_LABEL_GROUP_COURSE_WEIGHT","cours (% poids):");
define("A_LANG_AVS_LABEL_GROUP_NOT_EVALUATION","Il n'y a pas d'�valuation � ce groupe");
// labels utilizados no F_AJUSTE.PHP
define("A_LANG_AVS_LABEL_FUNCTION_AVERAGE","r�gler en moyenne");
define("A_LANG_AVS_LABEL_FUNCTION_QUESTION_DISSERT","fixer s'�tendre questions");
define("A_LANG_AVS_LABEL_FUNCTION_QUESTION_CANCEL","Annuler les questions de l'�valuation");
// labels utilizados no F_AJUSTE_ANULAR.PHP
define("A_LANG_AVS_LABEL_CHOOSE_EVALUATION","s�lectionner l'�valuation");
define("A_LANG_AVS_LABEL_CHOOSE_COURSE","s�lectionner le cours");
define("A_LANG_AVS_LABEL_CHOOSE_TOPIC","s�lectionnez le concept");
define("A_LANG_AVS_LABEL_CHOOSE_STUDENTS","Choisissez les �tudiants qui feront l'�valuation");

// labels utilizados no ambiente do aluno
define("A_LANG_AVS_LABEL_USER_NOT_EVALUATION","Sin evaluaci�n em liberdad");
define("A_LANG_AVS_LABEL_USER_NOT_VALUE","il n'y a pas de notes disponibles");
// labels das telas de quest&otilde;es
define("A_LANG_AVS_LABEL_QUESTION_ENUNCIATE"," travail de la question: ");
define("A_LANG_AVS_LABEL_QUESTION_EXPLANATION","explication");
define("A_LANG_AVS_LABEL_QUESTION_OPTION_CORRECT","dont l'un est l'option correcte?");
define("A_LANG_AVS_LABEL_QUESTION_VF_CORRECT","vrai");
define("A_LANG_AVS_LABEL_QUESTION_VF_FALSE","Faux");
define("A_LANG_AVS_LABEL_QUESTION_VF_NAME_CORRECT","Vrai nom");
define("A_LANG_AVS_LABEL_QUESTION_VF_NAME_FALSE","faux nom");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_DISSERT","Type de question est de s'�tendre");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_LACUNA","Type de question est Lacune");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_ME","type de question est � choix multiple");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_VF","type de questions est vrai ou faux");

define("A_LANG_AVS_LABEL_AJAX_DIVULGE1","Ok");
define("A_LANG_AVS_LABEL_AJAX_DIVULGE2","Pendant");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE1","Pendant");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE2","R�alisation");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE3","rendu");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE4","non rendu");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE5","Avec z�ro");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION1","liste des notes disponibles");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION2","Topique");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION3","Description");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION4","liste des notes qu'il a pris fin");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION5","Evaluation pr�sentiel");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION6","Corrig�");
define("A_LANG_AVS_LABEL_AJAX_AVERAGE","groupe d'�valuation au topique");

define("A_LANG_AVS_LABEL_AJAX_ZERO","La note z�ro sera supprim�");

define("A_LANG_AVS_LABEL_SEARCH","Recherche par mots cl�s");



// -------------- FIM DOS LABELS ------------------------------------------------------------ //
// -------------- COMBOBOX ----------------------------------------------------------------- //

define("A_LANG_AVS_COMBO_SELECT","S�lectionnez ...");
define("A_LANG_AVS_COMBO_DATE","Lib�rer");

// utilizado no F_AJUSTE_ANULAR.PHP
define("A_LANG_AVS_COMBO_TOPIC","Topique");
define("A_LANG_AVS_COMBO_MARK","Marquez");
define("A_LANG_AVS_COMBO_UNMARK","non Marquez");


// -------------- TTULOS DE TABELAS -------------------------------------------------------- //
define("A_LANG_AVS_TABLE_MODIFY","Changer");
define("A_LANG_AVS_TABLE_DELETE","Supprimer");
define("A_LANG_AVS_TABLE_DESCRIPTION","Description");
// t&iacute;tulo do AVS_NOVA_AVAL.PHP
define("A_LANG_AVS_TABLE_TYPE","type");
define("A_LANG_AVS_TABLE_WEIGHT","poid (%)");
define("A_LANG_AVS_TABLE_WEIGHT_DEFAULT","Par d�faut le poid (%)");
define("A_LANG_AVS_TABLE_WEIGHT_MOD","�t� le changement de poids (%)");
define("A_LANG_AVS_TABLE_SUM_WEIGHT","imanche de poids");
define("A_LANG_AVS_TABLE_REST_WEIGH","poids reste");
// t&iacute;tulo do AVS_TOPICO.PHP
define("A_LANG_AVS_TABLE_DIVULGE","Divulgation");
define("A_LANG_AVS_TABLE_NUMBER_QUESTION","nombre de questions");
define("A_LANG_AVS_TABLE_DATE_START","date de lib�ration");
define("A_LANG_AVS_TABLE_DATE_FINISH","date de fin");
define("A_LANG_AVS_TABLE_DATE","Date");
define("A_LANG_AVS_TABLE_VALUE_CANCELLED","annul� note");
define("A_LANG_AVS_TABLE_ACTION","Actions");
define("A_LANG_AVS_TABLE_DATE_CREATE","Cr�ation");
define("A_LANG_AVS_TABLE_DATE_MODIFY","Changer");
define("A_LANG_AVS_TABLE_DIVULGE_AUTO","Automatique");
define("A_LANG_AVS_TABLE_DIVULGE_MANUAL","Manuel");
// titulo dos scripts do ambiente do aluno (N_INDEX, N_MURAL)
define("A_LANG_AVS_TABLE_USER_EVALUATION","�valuation");
define("A_LANG_AVS_TABLE_USER_DATE","Date de d�but");
define("A_LANG_AVS_TABLE_USER_REALIZE_EVALUATION","R�aliser l'�valuation");
define("A_LANG_AVS_TABLE_USER_DESCRIPTION_EVALUATION","Description");
define("A_LANG_AVS_TABLE_USER_DATE_REALIZE","Date de r�aliser");
define("A_LANG_AVS_TABLE_USER_VALUE","Notes");
define("A_LANG_AVS_TABLE_USER_VALUE2","Note du test");
define("A_LANG_AVS_TABLE_USER_TOTAL_VALUE","Total note");
// titulos de tabelas das telas de quest&otilde;es
define("A_LANG_AVS_TABLE_QUESTION_LACUNA1","Texte initial");
define("A_LANG_AVS_TABLE_QUESTION_LACUNA2","R�ponse de Lacuna");
define("A_LANG_AVS_TABLE_QUESTION_LACUNA3","Suite du texte");
define("A_LANG_AVS_TABLE_QUESTION_ME1","Alternatives");
define("A_LANG_AVS_TABLE_QUESTION_ME2","Right?");
define("A_LANG_AVS_TABLE_QUESTION_ME3","Fix�");
define("A_LANG_AVS_TABLE_QUESTION_ME4","description des alternativas");
define("A_LANG_AVS_TABLE_QUESTION_ME5","Supprimer");
// titulos utilizados nas tabelas do ajuste de notas dissertativa
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT","Etudiant");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT1","questions en eendance");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT2","�tudiants ne compl�te pas l'�valuation");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT3","correction de l'�valuation");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT4","Variation corrections");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT5","Poid");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT6","Note");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT7","Explication");

// titulos utilizados nos ajax
define("A_LANG_AVS_TABLE_AJAX_AVERAGE","Etudiant");
define("A_LANG_AVS_TABLE_AJAX_AVERAGE1","Moyenne");
define("A_LANG_AVS_TABLE_AJAX_AVERAGE2","Note de la participation");
define("A_LANG_AVS_TABLE_AJAX_AVERAGE3","moyenne finale");
define("A_LANG_AVS_TABLE_AJAX_QUESTION_CANCEL","Annuler");
define("A_LANG_AVS_TABLE_AJAX_QUESTION_UNDO","d�faire");
define("A_LANG_AVS_TABLE_AJAX_QUESTION_CANCEL2","Annul�");
define("A_LANG_AVS_TABLE_AJAX_DIVULGE1","Etudiant");
define("A_LANG_AVS_TABLE_AJAX_DIVULGE2","Pendance");

// hints - texto flutuante informativo
define("A_LANG_AVS_HINTS_EVALUATION_EDIT","modifier l'�valuation. Ajouter de nouvelles questions");
define("A_LANG_AVS_HINTS_EVALUATION_DELETE","Supprimer l'�valuation");
define("A_LANG_AVS_HINTS_EVALUATION_VIEW","Voir Evaluation");
define("A_LANG_AVS_HINTS_USER_EVALUATION_VIEW","Cette note est correcte lors de l'essai, � l'exclusion des questions annul�e par le professeur");
define("A_LANG_AVS_HINTS_QUESTION_DELETE","Supprimer ce alternative");
define("A_LANG_AVS_HINTS_AJAX_QUESTION_CANCEL","Cette question a d�j� �t� annul�e");
define("A_LANG_AVS_HINTS_AJAX_DIVULGE1","Droit de s'�tendre questions");
// textos
define("A_LANG_AVS_TEXT_LIBERATE","il s'agit du num�ro d'�valuation disponibles. Store � consulter � l'avenir");
define("A_LANG_AVS_TEXT_FIELD_REQUIRE","Les champs avec <b> * </b> sont obligatoires");
define("A_LANG_AVS_TEXT_ADJUSTMENT","Les champs <b> explication </b> sera pr�sent� aux �tudiants comme une explication de l'erreur ou de succ�s. Sa r�alisation n'est pas obligatoire");
define("A_LANG_AVS_TEXT_LIBERATE_STUDENT","");

// ------------- TEXTOS UTILIZADOS EM JAVASCRIPT ---------------------------------------------//
// OBS: lembre-se em Javascript escreva sem htmlentities ( a = &aacute;)
define("A_LANG_AVS_JS_EVALUATION_CONFIRM","Voulez-vous envoyer cette �valuation?");
define("A_LANG_AVS_JS_DELETE_PRESENT_CONFIRM","Toutes les �valuations seront supprim�s si l'option est choisie pr�sentiel");
define("A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT","Il ya certaines questions sans poids d�finis");
define("A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT2","O La somme des poids des questions ne devrait pas d�passer cent pour cent (100%)");
define("A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT3","Le poids des questions qui doivent �tre � cent pour cent");
define("A_LANG_AVS_JS_DELETE_QUESTION_CONFIRM","Voulez-vous supprimer cette question?");
define("A_LANG_AVS_JS_ALERT_QUESTION","le poids n'est pas encore sauver");
define("A_LANG_AVS_JS_ALERT_EVALUATION","vous devez choisir une �valuation");
define("A_LANG_AVS_JS_ALERT_QUESTION_REQUIRE","Le champ");
define("A_LANG_AVS_JS_ALERT_QUESTION_REQUIRE2","il doit �tre rempli!");
define("A_LANG_AVS_JS_ALERT_QUESTION_LACUNA_DELETE","voulez-vous supprimer cette lacune?");
define("A_LANG_AVS_JS_ALERT_QUESTION_ME_CORRECT","aucun des autres a �t� choisi comme droit");
define("A_LANG_AVS_JS_ALERT_QUESTION_ME_REQUIRE","la question n'est pas complet");
define("A_LANG_AVS_JS_ALERT_QUESTION_ME_DELETE","voulez-vous supprimer cette alternative?");
define("A_LANG_AVS_JS_ALERT_QUESTION_VF_REQUIRE_CORRECT","vous devez choisir une des options � la bonne r�ponse");
define("A_LANG_AVS_JS_ALERT_STUDENTS","il est n�cessaire de choisir les �tudiants");
define("A_LANG_AVS_JS_ALERT_EVALUATION_DELETE_CONFIRM","Voulez-vous effacer cette �valuation?");
// ------------- FIM DO JAVASCRIPT ------------------------------------------------------------//
define("A_LANG_AVS_FORMAT_DATE","%d/%m/%Y"); // string da funcao date para modificar o estilo da data
define("A_LANG_AVS_FORMAT_DATE_TIME","%d/%m/%Y %H:%M:%S"); // string da funcao date para modificar o estilo da data

/**********************************************************************************************/
/*********************************** FIM DA AVALIACAO **************************************/

/******************************************************************************************** 
 * Mural de Recados (Definido por Laisi Corsani)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Estagio Curricular.
 * Orientadora: Isabela Gasparini
 * Supervisora: Avanilde Kemczinski
 */ 
define("A_LANG_MURAL_DISC","Mural de message de la discipline ");
define("A_LANG_MURAL_ENVIARECADO_DISC","envoyer un message dans la discipline");
define("A_LANG_MURAL_ENVIARECADO","Envoyer un message");
define("A_LANG_MURAL_ESCREVERECADO","Ecrire message");
define("A_LANG_MURAL_OUTRORECADO","Ecrire autre message");
define("A_LANG_MURAL_ASS", "SUJET:");
define("A_LANG_MURAL_REM", "EXP�DITEUR");
define("A_LANG_MURAL_REC","MESSAGE:");
define("A_LANG_MURAL_DT","Date");
define("A_LANG_MURAL_ALUNOS","Etudiants:");
define("A_LANG_MURAL_DEST","R�cepteur:");
define("A_LANG_MURAL_LISTREC","les messages re�us sur les sept derniers jours:");
define("A_LANG_MURAL_SENDEMAIL","envoyer e-mail d'alerte");
define("A_LANG_MURAL_ASSMIN","Sujet::");
define("A_LANG_MURAL_MSG","message:");
define("A_LANG_MURAL_PROF","Professeur");
define("A_LANG_MURAL_GROUP","Groupe");
define("A_LANG_REC_SUSS","message a �t� envoy� avec succ�s!");
define("A_LANG_REC_INSUSS","permettre d'envoyer le message");
define("A_LANG_REC_INSUSS_ALL","l'impossibilit� d'envoyer le message � tous les r�cepteurs");
define("A_LANG_REC_ALERT_DEST","s�lectionner un r�cepteur!");
define("A_LANG_REC_ALERT_MSG","l'impossibilit� d'envoyer un message sans contenu");
define("A_LANG_REC_ALERT_ALL","Impossible d'envoyer un message! Pour envoyer un message, vous devez choisir une personne et d'�crire un message!");
define("A_LANG_MURAL_BACK","retour murale du message");
define('A_LANG_LENVIRONMENT_MURAL', 'Mural du message');

/*********************************** FIM MURAL RECADOS **************************************/

/********************************************************************************************
 * Altera��es para Usabilidade (Definido por Carla Cristina Lui Dias)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Estagio Curricular.
 * Orientadora: Isabela Gasparini
 * Supervisora: Avanilde Kemczinski
 */

define("A_LANG_SYSTEM_NAME_DESC","Ambiente de Ensino-Aprendizagem Adaptativo na Web (AdaptWeb) "); 
define("A_LANG_SYSTEM_WELCOME","Bienvenue �");
define("A_LANG_INTRO_DESC_1","Cet environnement permet � l'enseignant de lui fournir du mat�riel pour les �tudiants.");//calra
define("A_LANG_INTRO_DESC_2","Pour naviguer dans l'environnement, l'enseignant doit d'abord faire de votre compte d'utilisateur sur le point <u> Nouvel utilisateur </u> dans le menu d'options. Faite apr�s l'inscription, l'enseignant doit signer pour compl�ter votre article <u> Login </u> dans le menu d'options. Si l'enseignant dispose d�j� d'un registre, le seul � remplir votre login.");//calra
define("A_LANG_INTRO_DESC_3","Lorsque la connexion est effectu�e, le menu d'options est chang� et l'enseignant en cliquant sur le point <u> environnement professeur </u> peut interagir avec l'environnement de la cr�er disciplines. En cliquant sur ce point, un sous-menu appara�t avec le titre de Ma�tre <b> Environnement </b> � la fin du menu principal, afin que l'enseignant peut demander et d'assister � l'objet � disposition par les enseignants ou par lui m�me, vous pouvez cr�er des cours , libre pour les �tudiants, pour examiner le journal, l'�valuation s'applique, entre autres.");//calra
define("A_LANG_INTRO_DESC_4","Cet environnement permet aux �tudiants d'avoir acc�s (s) document (s) enseignement (s) indiqu�e (s) de (s) votre (s) professeur (s) en fonction de votre profil. <p align=justify> Pour naviguer dans l'environnement, l'�tudiant doit d'abord remplir votre compte d'utilisateur sur le point <u> Nouvel utilisateur </u> dans le menu d'options. Faite apr�s l'inscription, l'�tudiant doit signer pour compl�ter votre article <u> Login </u> dans le menu d'options. Si l'�tudiant poss�de d�j� un registre, seulement pour compl�ter vos informations de connexion."); //carla
define("A_LANG_INTRO_DESC_5","Lorsque la connexion est effectu�e, le menu d'options est chang� et l'�l�ve doit cliquer sur la question des �tudiants <u> Environnement </u>. En cliquant sur ce point, un sous-menu appara�t avec le titre <b> l'environnement des �tudiants </b> � la fin du menu principal, si l'�tudiant peut demander et d'assister (s) sujet � disposition par ce professeur.");//Carla
// Itens de Autoria
define("A_LANG_AUTHOR","Authoring Environnement");//carla
define("A_LANG_STUDENT", "Environnements �tudiant"); //carla para n_entrada_navegacao
define("A_LANG_AUTHOR_INTRO_1","Le module permet � l'auteur � l'auteur de contenu sur mesure pour fournir des profils diff�rents des �l�ves. Gr�ce au processus de la paternit�, l'auteur peut fournir le contenu de leurs cours dans une structure unique, adapt� aux diff�rents cours. � ce stade, l'auteur doit s'inscrire � la discipline et des cours qui veulent les rendre disponibles.");//carla
define("A_LANG_AUTHOR_INTRO_2","Apr�s l'inscription, ins�rer le concept du contenu des fichiers, relatifs � chaque sujet de la discipline. En outre, l'auteur doit informer la description du topique, et de ses pr�-requis de cours qui veulent offrir du contenu. L'auteur peut ins�rer des exercices, des exemples et des documents suppl�mentaires pour chaque topique."); //Carla

// Estudantes
define("A_LANG_INTRO_STUD_DESC","Cet environnement permet aux �tudiants ont acc�s � du mat�riel p�dagogique divulgu�s par votre professeur en fonction de votre profil.
<p align=justify> Pour acc�der � votre sujet, l'�tudiant doit d'abord cliquer sur la question
Etudiant <u> Environnement </u> dans le menu d'options. En cliquant sur ce point appara�tra une
sous - menu avec le titre <b> Environnement �tudiants </b> � la fin du menu principal, afin que l'utilisateur doit cliquer
Demande d'inscription de la question <u> </u> dans le sous - menu de la demande actuelle qui est li�e � la
Disciplines qui souhaitent lui poser.
<p align=justify> � la demande de ce cours, l'utilisateur doit marquer la discipline qu'ils souhaitent demander que
l'enseignant et de cliquer sur demande. Apr�s la discipline n�cessaire � l'utilisateur doit attendre
la lib�ration de l'enseignant afin qu'il puisse assister � la discipline n�cessaire.
");//Carla
define("A_LANG_REQUEST_ACCESS_MSG11","Le champ E-mail a �t� achev�e avec valeur non valide");//modificado por Carla, inseri * em Mar�o de 2009

/*********************************** FIM USABILIDADE **************************************/

/******************************************************************************************** 
 * F�rum de Discuss�o (Definido por Claudia da Rosa)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como TCC - Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora Avanilde Kemczinski
 */
 
define("A_LANG_TOPICO_INSUSS","Impossible de cr�er le topique!");
define("A_LANG_RESPOSTA_INSUSS","Impossible de r�pondre le topique!");
define("A_LANG_TOPICO_SUSS","Topique cr��e avec succ�s!");
define("A_LANG_RESPOSTA_SUSS","R�ponse effectu� avec succ�s!");
define("A_LANG_TOP_ALERT_TITULO","Impossible de cr�er votre topique! Saisissez un titre!");
define("A_LANG_TOP_ALERT_DESCR","Impossible de cr�er votre topique! Entrez votre description!");
define("A_LANG_TOP_ALERT_RESP_DESCR","Impossible de r�pondre � topique Entrez votre description!");
define("A_LANG_TOP_ALERT_AVAL","S�lectionnez l'une des options pour l'�valuation!");
define("A_LANG_FORUM_OUTRO_TOPICO","Cr�er un autre topique");
define("A_LANG_TOP_ALERT_ALL","Impossible de cr�er votre topique! Entrez un titre et une description!");
define("A_LANG_TOP_ALERT_ALL_PROF","Impossible de cr�er votre topique! Entrez le titre, la description et l'�valuation de s�lectionner!");
define("A_LANG_TOP_ALERT_ALL_AVAL_DESCR","Impossible de cr�er votre topique! Entrez la description et l'�valuation de s�lectionner!");
define("A_LANG_TOP_ALERT_ALL_AVAL_TITULO","Impossible de cr�er votre topique Saisissez un titre et choisir de l'�valuation!");
define("A_LANG_FORUM_DISC","Forum de discussion de le discipline");
define('A_LANG_LENVIRONMENT_FORUM', 'Forum de discussion');
define("A_LANG_FORUM_BACK","Retour au Forum de discussion");
define("A_LANG_TOPICO_ESCOLHIDO_BACK","Retour � le topique choisie");
define("A_LANG_FORUM_LISTTOP","Parmi les sujets abord�s au cours des derniers jours:");
define("A_LANG_FORUM_TITULO","Titre:");
define("A_LANG_FORUM_TITULO_TOP","Titre de topiques");
define("A_LANG_FORUM_DESCR","Description");
define("A_LANG_FORUM_CRIARTOPICODISCUSSAO","Cr�er un topique de discussion");
define("A_LANG_FORUM_RESPONDERTOPICO","R�pondre � la rubrique");
define("A_LANG_FORUM_AVAL","Evaluation");
define("A_LANG_FORUM_SIM","Oui");
define("A_LANG_FORUM_NAO","Non");
define("A_LANG_FORUM_QUAL_INS","insatisfaisant!");
define("A_LANG_FORUM_QUAL_REG","r�gulier");
define("A_LANG_FORUM_QUAL_BOM","Bien");
define("A_LANG_FORUM_QUAL_EXC","Excellent");
define("A_LANG_FORUM_AVAL_SUSS","Evaluation de la part de l'�tudiant avec succ�s!");
define("A_LANG_FORUM_AVAL_INSUSS","Impossible de faire des �valuations de la part!");
define("A_LANG_FORUM_LIST_ALUN_MAT","Les �tudiants inscrits:");
define("A_LANG_FORUM_AVAL_ALUN_BACK","Retour � l'�valuation des �l�ves");
define("A_LANG_FORUM_ALUNO","Etudiant");
define("A_LANG_FORUM_AVAL_PARTIC","Il n'y a pas de participation des �tudiants � ce sujet!");
define("A_LANG_FORUM_AVAL_OBS","Note: Pour r�aliser une �valuation formative des �l�ves, vous devez d\'abord �valuer les sujets de discussion individuellement, via le lien <u> Oui </u> la colonne \"Evaluation \"");

/*********************************** FIM FORUM DISCUSSAO **************************************/

/******************************************************************************************** 
 * Altera��es para Usabilidade (Definido por Carla Cristina Lui Dias)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Estagio Curricular.
 * Orientadora: Isabela Gasparini
 * Supervisora: Avanilde Kemczinski
 */
define("A_LANG_EMAIL_ERRO","El mensaje de ADVERTENCIA n'a pas �t� envoy�e el mensaje de correo electr�nico a todos los destinatarios! n � <br> fils v�lidos en la lista del email.");
/*********************************** FIM ERRO EMAIL **************************************/

/*
 * Ferramenta de An�lise Interativa
 * Inclu�do por Barbara Moissa em 2013-11-02
 */
define('A_LANG_ANALISEINTERATIVA', 'An�lise Interativa');
