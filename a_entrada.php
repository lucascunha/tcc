<?
/* -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Informática  
 *       UEL - Departamento de Computação
 * -----------------------------------------------------------------------
 *       @package Apresentação do Ambiente de Autoria
 *     @subpakage Apresentação do Ambiente de Autoria
 *          @file a_entrada.php
 *    @desciption Apresentação do Ambiente de Autoria
 *         @since 25/06/2003
 * -----------------------------------------------------------------------         
 */  

  $orelha = array();  
  $orelha = array(
  		array(   
   		   "LABEL" => 'Quadro de Avisos' , 
     		   "LINK" => "a_index.php", 
     		   "ESTADO" =>"ON"
   		   )   		       		       		   		   
   		  ); 

  MontaOrelha($orelha);  

?>

<table cellspacing=5 cellpadding=5 border="0" width="100%" bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> style="height:380px;" >
	<tr>
	<td valign=top>


	<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; height:340px;" class="tabela_redonda" >

	<tr>
		<td valign=top>			



<?php

	$usuario=$A_DB_USER;
	$senha=$A_DB_PASS;
	$nomebd=$A_DB_DB;
	include "./include/conecta.php";
?>

					<ul class="ttw-notification-menu">




					    <li id="projects" class="notification-menu-item first-item"><a href="#">Recados Recebidos</a>
					    	<?php
								$data_lim = date('Y-m-d', strtotime("-30 days"));
								$data = date(Y)."-".date(m)."-".date(d);
							 	$sql = "SELECT 	mural.id_recado, destinatario.id_disc, destinatario.id_curso
										FROM 	mural, destinatario 
										WHERE 	mural.st_recado='1'
										AND 	destinatario.id_usuario='$id_usuario'
										and 	mural.id_recado = destinatario.id_recado
										and 	mural.tipo_recado <> '1pessoal'
										and 	dta between '$data_lim' AND '$data' 
										and 	destinatario.id_professor = '$id_usuario'
										order by destinatario.id_curso, destinatario.id_disc";
								$rs = mysql_query($sql,$id);
				 				$n_linhas = mysql_num_rows($rs);

				 				if ($n_linhas == 0)
				 				{	
									echo "<span class=\"notification-bubble\" title=\"Notifications\" style=\"background-color:rgb(0, 180, 90);  display: inline;\">$n_linhas</span>";
								}
								else
								{
									echo "<span class=\"notification-bubble\" title=\"Notifications\" style=\"background-color:rgb(255, 0, 0);  display: inline;\">$n_linhas</span>";
									echo "<ul>";
									for($i=0; $i<mysql_num_rows($rs); $i++)
									{
										$fetch = mysql_fetch_row($rs);	

									  	$sql1 = "SELECT id_curso, id_disc FROM destinatario where id_recado=$fetch[0] ";
										$rs1 = mysql_query($sql1,$id);
										$fetch1 = mysql_fetch_row($rs1);	
										$atual = $fetch1[0].$fetch1[1]."<br>"; 
										$anterior = $id_curso_ant.$id_disc_ant."<br>";

										if ( ($atual <> $anterior))
										{						
											$sql2 = "SELECT nome_curso, nome_disc from curso, disciplina where id_curso = $fetch1[0] and id_disc =  $fetch1[1]  ";
											$rs2 = mysql_query($sql2,$id);
											$fetch2 = mysql_fetch_row($rs2);
											echo "<li><a href=\"n_index_navegacao.php?opcao=MuralRecados&id_prof=$id_usuario&num_disc=$fetch1[1]&num_curso=$fetch1[0]&disciplina=$fetch2[1]&curso=$fetch2[0]&referer=entrada\">$fetch2[1] - $fetch2[0]</a></li>";

										}
											$id_curso_ant = $fetch1[0];
											$id_disc_ant = $fetch1[1];	
									}
									echo "</ul>";	
								}
					    	?>

						</li>
					



					    <li id="tasks" class="notification-menu-item"><a href="#">Recados para Liberar</a>
					    	<?php
					    		$id_curso_ant = '';
								$id_disc_ant = '';

							 	$sql = "SELECT 	mural.id_recado, destinatario.id_disc, destinatario.id_curso
										FROM 	mural, destinatario 
										WHERE 	mural.st_recado='0'
										AND 	destinatario.id_usuario='$id_usuario'
										and 	mural.id_recado = destinatario.id_recado
										and 	mural.tipo_recado = 'geral'
										
										order by destinatario.id_curso, destinatario.id_disc";					    	

								//$sql = "SELECT 	distinct mural.id_recado
								//		FROM 	mural, destinatario 
								//		WHERE 	mural.tipo_recado='geral' 
								//		AND 	mural.st_recado='0'
								//		AND 	destinatario.id_professor='$id_usuario'
								//		and 	mural.id_recado = destinatario.id_recado";
								$rs = mysql_query($sql,$id);
				 				$n_linhas = mysql_num_rows($rs);

				 				if ($n_linhas == 0)
				 				{	
									echo "<span class=\"notification-bubble\" title=\"Notifications\" style=\"background-color:rgb(0, 180, 90);  display: inline;\">$n_linhas</span>";
								}
								else
								{
									echo "<span class=\"notification-bubble\" title=\"Notifications\" style=\"background-color:rgb(255, 0, 0);  display: inline;\">$n_linhas</span>";
									echo "<ul>";
									for($i=0; $i<mysql_num_rows($rs); $i++)
									{
										$fetch = mysql_fetch_row($rs);	

										$sql1 = "SELECT distinct id_curso, id_disc FROM destinatario where id_recado=$fetch[0] ";
										$rs1 = mysql_query($sql1,$id);
										$fetch1 = mysql_fetch_row($rs1);
										$atual = $fetch1[0].$fetch1[1]."<br>"; 
										$anterior = $id_curso_ant.$id_disc_ant."<br>";
										
										if ( ($atual <> $anterior))
										{
											$sql2 = "SELECT nome_curso, nome_disc from curso, disciplina where id_curso = $fetch1[0] and id_disc =  $fetch1[1]  ";
											$rs2 = mysql_query($sql2,$id);
											$fetch2 = mysql_fetch_row($rs2);
											echo "<li><a href=\"n_index_navegacao.php?opcao=MuralRecados&id_prof=$id_usuario&num_disc=$fetch1[1]&num_curso=$fetch1[0]&disciplina=$fetch2[1]&curso=$fetch2[0]&referer=entrada\">$fetch2[1] - $fetch2[0]</a></li>";
										}
										$id_curso_ant = $fetch1[0];
										$id_disc_ant = $fetch1[1];
									}
									echo "</ul>";	
								}
					    	?>
					    </li>




					    <li id="messages" class="notification-menu-item"><a href="#" >Pedidos de Matrícula</a>
						<?php
							$sql = "select 	matricula.id_usuario, matricula.id_curso, matricula.id_disc
										from 	matricula
										where 	matricula.id_professor = $id_usuario
										and 	matricula.status_mat = 0
										order by matricula.id_curso, matricula.id_disc
										"; 

								$rs = mysql_query($sql,$id); 
								$n_linhas_mat=mysql_num_rows($rs);
				 				if ($n_linhas_mat == 0)
				 				{	
									echo "<span class=\"notification-bubble\" title=\"Notifications\" style=\"background-color:rgb(0, 180, 90);  display: inline;\">$n_linhas_mat</span>";
								}
								else
								{
									echo "<span class=\"notification-bubble\" title=\"Notifications\" style=\"background-color:rgb(255, 0, 0);  display: inline;\">$n_linhas_mat</span>";
	
									echo "<ul>";
									for($i=0; $i<mysql_num_rows($rs); $i++)
									{	$fetch = mysql_fetch_row($rs);

										if ( ($fetch[1] != $id_curso_ant) && ($fetch[2] != $id_disc_ant))
										{	
											$sql2 = "SELECT  nome_curso, nome_disc from curso, disciplina where id_curso = $fetch[1] and id_disc =  $fetch[2]  ";
											$rs2 = mysql_query($sql2,$id);
											$fetch2 = mysql_fetch_row($rs2);
											echo "<li><a href=\"a_index.php?opcao=AutorizarAcesso&adi=1&n_disc=$fetch[2]&n_curso=$fetch[1]\">$fetch2[1] - $fetch2[0]</a></li>";
											$id_curso_ant = $fetch[1];
											$id_disc_ant = $fetch[2];
										}	
								
									}
									echo "</ul>";									
								}								

						?>
						
					    </li>


					    <li id="messages" class="notification-menu-item"><a href="a_index.php?opcao=LiberarDisciplina" >Disciplinas para Liberar</a>

						<?php		
					  					$conn = &ADONewConnection($A_DB_TYPE); 
					  					$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
									  	$sql="select id_disc, nome_disc from disciplina where id_usuario=$id_usuario and status_xml=1 and status_disc=0  order by nome_disc";
					  					$rs = mysql_query($sql,$id); 
				 						$n_linhas_disc_liberar = mysql_num_rows($rs);
				 						if ($n_linhas_disc_liberar ==NULL) { $n_linhas_disc_liberar = 0;}

				 						if ($n_linhas_disc_liberar == 0)
				 						{	
											echo "<span class=\"notification-bubble\" title=\"Notifications\" style=\"background-color:rgb(0, 180, 90);  display: inline;\">$n_linhas_disc_liberar</span>";
										}
										else
										{
											echo "<span class=\"notification-bubble\" title=\"Notifications\" style=\"background-color:rgb(255, 0, 0);  display: inline;\">$n_linhas_disc_liberar</span>";
										}
						?>	
					    </li>    


					    <li id="messages" class="notification-menu-item"><a href="n_index_navegacao.php?opcao=AssistirDisciplinas" >ASSISTIR DISCIPLINAS <img border=0 align=top src="imagens/arrow_right.png"></a>

					    </li>   
					  
					
					</ul>

				<div class="bola" style='width:98%; margin:30px 0 0 10px;'>
      			<h2 style="padding-top:10px; padding-left:15px;">
			ADAPTWEB - 			 				         
    		<?
		     echo A_LANG_AUTHOR;
		 	?> 	
  	      		</h2>
  	      		</div>
				<br>
			<table border="0" width= "98%" style='margin:0 0 0 10px;'>
			 <tr>
    		  	 <td>
				  <p align="justify">  <!--  echo A_LANG_TENVIRONMENT_DESCRIPTION;  -->
			    	<? echo "<p align=justify>".A_LANG_AUTHOR_INTRO_1."</p><p align=justify>".A_LANG_AUTHOR_INTRO_2."</p>"; ?> 
			  		</p>
			 	</td>
			 </tr>
			</table>
  		</td>
  	</tr>
 	</table>

  		</td>
  	</tr>
 	</table>


<?php 
$var = $_GET["var"];
if ($var == 1)
	{
	?>
 	<script type="text/javascript">
 	   showNotification({
 	       message: "<table border = 0><tr><td width=50 align=right valign=middle><img src='imagens/icones/success.png' border='0'> </td><td valign=middle class=mensagem_shadow>&nbsp;&nbsp;&nbsp;&nbsp; Login realizado com sucesso !</td></tr></table>",
 	       type: "<? echo $tipo_icone; ?>",
 	       autoClose: true,
 	       duration: 3                                        
 	   });
 	</script>	
    <?php 
	} ?>                			

