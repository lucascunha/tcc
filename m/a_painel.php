<?
include "init.php"; 
?>
<!DOCTYPE html>
<html>
<head>
<? include "include/head.php";?>
</head>
<body>
<!-- home -->
<div data-role="page" id="home" data-dom-cache="true">
    <div data-role="panel" id="controlPanel">
        <ul data-role="listview">
            <li><a href="a_assistir_disciplina.php" rel="external" data-ajax="false">Assistir Disciplina</a></li>
            <li><a href="a_cadastro.php" rel="external" data-ajax="false">Alterar Cadastro</a></li>
        </ul>    
    </div>
  <div data-theme="b" data-role="header">
    <a href="#controlPanel" data-icon="bars" data-theme="b">Menu</a>
    <h3> M-ADAPTWEB </h3>
    <a href="logout.php" data-icon="alert" data-theme="b" data-ajax="false" rel="external">Sair</a>
  </div>
  <div data-role="content" id="top">
    <? include "include/message.php";?>
    <? include "htmls/painel.php";?>
  </div>
  <? include "include/footer.php";?>
</div>
</body>
</html>