<?php
include "init.php"; 


if(isset($_POST["gravar"])){
	
	foreach ($_POST as $campo => $valor) { $$campo = $valor;}

	  $status = 1;
	  $message = "";

/*
		<form id="formCadastro" name="formCadastro" method="post" action="a_cadastro.php">
        	<input type="hidden" name="cTipoUsuario" value="aluno">        
            <p class="field-description">Nome <strong>*</strong></p>
        	<input type="text" name="cNome_Usuario" class="field" value="<?=$cAuxNomeUsuario?>" placeholder="Nome"/>
            <p class="field-description">E-mail</p> 
            <div class="disabled"><?=$cAuxEmail_Usuario?></div>
            <input type="hidden" name="gravar" value="true">                    
            <p class="field-description">Senha</p>
            <input type="password" name="cSenha_Usuario" class="field" value="" placeholder="Senha"/>
			<p class="field-description">Confirmar</p>
            <input type="password" name="cSenha_Usuario2" class="field" value="" placeholder="Repita a senha"/>
			<p class="field-description">Instituição de Ensino</p>
            <input type="text" name="cInstituicao_Ensino" class="field" value="<?=$cAuxInstituicao_Ensino?>" placeholder="Instituição de Ensino"/>
            <span class="buttonBig"><input type="button" id="buttonCadastro" value="Gravar"></span>
            <p class="field-description demo">Observação: Os campos que possuem <strong>*</strong> devem ser obrigatoriamente preenchidos.!</p>
		</form>  
*/

		
		//exit;
	$setSenha = "";	
	if($cSenha_Usuario != ""){
		if($cSenha_Usuario != $cSenha_Usuario2){
			$status = 0;
			$message =  "Senhas não conferem!";		
			
		} elseif((strlen($cSenha_Usuario) < 3)||(strlen( $cSenha_Usuario ) > 15)){
			$status = 0;
			$message =  "Senha com no Mínimo de 3 e Máximo de 15 caracteres !";			
		} 
	} elseif($cNome_Usuario == ""){
			$status = 0;
			$message =  "Nome obrigatório!";			
	} 
	if($status == 1){
	
		  $conn = &ADONewConnection($A_DB_TYPE); 
		  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
		  $sql = "update usuario set nome_usuario = '".$cNome_Usuario."', instituicao_ensino = '".$cInstituicao_Ensino."' ".$setSenha." where id_usuario = '".$id_usuario."'";
		  $rs = $conn->Execute($sql);

		  if($cSenha_Usuario != ""){
			$senha = "update usuario set senha_usuario = '".sha1($cSenha_Usuario)."' where id_usuario = '".$id_usuario."'";
			$rsenha = $conn->Execute($senha);			
		  }
	}
		

	echo json_encode(array("status" => $status, "message" => $message));
	
	exit;


}


$cAuxNomeUsuario = $_POST['cNome_Usuario'];                                
$cAuxSenha_Usuario = $_POST['cSenha_Usuario']; 
$cAuxEmail_Usuario = $_POST['cEmail_Usuario']; 
$cAuxInstituicao_Ensino = $_POST['cInstituicao_Ensino'];
$cAuxObservacao = $_POST['cObservacao'];
$cAuxIdioma = "";         
if ($logado)
{
	  $conn = &ADONewConnection($A_DB_TYPE); 
	  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	  $sql = "SELECT * FROM usuario where id_usuario = '".$id_usuario."'";
	  $rs = $conn->Execute($sql);
	  $cAuxNomeUsuario        = $rs->fields['nome_usuario'];
	  $cAuxSenha_Usuario      = $rs->fields['senha_usuario'];
	  $cAuxEmail_Usuario      = $rs->fields['email_usuario'];
	  $cAuxInstituicao_Ensino = $rs->fields['instituicao_ensino'];
	  $cAuxObservacao         = $rs->fields['observacao'];
	  $cAuxIdioma             = $rs->fields['Idioma'];
	  
	  $rs->Close(); 
}


?>
<!DOCTYPE html>
<html>
<head>
<? include "include/head.php";?>
</head>
<body>
<!-- cadastro -->
<div data-role="page" id="cadastro" data-dom-cache="true">
  <div data-theme="b" data-role="header"> 
  	<a href="#" data-role="button" data-theme="b" data-transition="slide" data-icon="arrow-l" data-rel="back">Voltar</a>
    <h3> M-ADAPTWEB </h3>
  </div>
  <div data-role="content" id="top">
    <? include "forms/formCadastro.php";?>
  </div>
  <? include "include/footer.php";?>
</div>
</body>
</html>