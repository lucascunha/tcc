<?
include "init.php"; 

/* -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Informática  
 *       UEL - Departamento de Computação
 * -----------------------------------------------------------------------
 *       @package Sair  
 *     @subpakage Efetuar logout
 *          @file a_faz_login.php
 *    @desciption Efetua logout 
 *         @since 25/06/2003
 *        @author Veronice de Freitas (veronice@jr.eti.br)
 * -----------------------------------------------------------------------         
 */ 

 session_start();
 session_unset();
 session_destroy();
 global $logado, $id_usuario, $email_usuario;
 $logado = false;
 $id_usuario = "" ;
 $email_usuario = "";
 $destino="Location: index.php";
 header($destino);
?>
<!DOCTYPE html>
<html>
<head>
<? include "include/head.php";?>
<script>
$(document).on('pageinit', function(){
	$.mobile.changePage('index.php', { transition: "slide" });
});
</script>
</head>
<body>
</body>
</html>
