<?php
include "config/configuracoes.php"; 
include "../include/funcoes.php";



if($logado){
	header($destino="Location: a_painel.php");	
	exit;
}

if(isset($_POST['logar'])){ 
	
  $status = 1;
  $message = "";
  if(isset($_POST['email']) && isset($_POST['senha'])){

		$logado = false;
		$email = $_POST['email'];
		$senha = $_POST['senha'];
	
	 global $logado, $id_usuario, $email_usuario, $tipo_usuario, $A_LANG_IDIOMA_USER, $status_usuario;
	
		$conn = &ADONewConnection($A_DB_TYPE); 
		$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
		$sql = "SELECT * FROM usuario where email_usuario ='".$email."';";
		$rs = $conn->Execute($sql);
	  
		if ($rs === false){ 
			$status = 0;
			$message =  A_LANG_LOGIN_MSG5;
		}  
	
		if ($rs->RecordCount() > 0 ) 
		{
			
			$senha = sha1($senha); //Senha Criptografado
			
			if (($rs->fields[2]) == $senha) 
			{
				$_SESSION['logado']=TRUE;
				$_SESSION['id_usuario']=$rs->fields['id_usuario'];
				$_SESSION['email_usuario']=$rs->fields['email_usuario'];
				$_SESSION['A_LANG_IDIOMA_USER']=$rs->fields['idioma'];
				$_SESSION['tipo_usuario']=$rs->fields['tipo_usuario'];
				$_SESSION['status_usuario']=$rs->fields['status_usuario'];		
				$_SESSION['id_aluno']=$rs->fields['id_usuario'];
				$_SESSION['tipo']=$rs->fields['tipo_usuario'];
	
	
				if ($_SESSION['tipo'] =='aluno')
				{
					$status = 1;
					$message =  "";
				}
				else if ($_SESSION['tipo'] =='professor')
				{
					if ($rs->fields['status_usuario'] == "autorizado")
					{
						$status = 0;
						$message =  "Somente Ambiente do Aluno!";
					}	
					else
					{
						$status = 0;
						$message =  "Somente Ambiente do Aluno!";
					}	
				}	
				else if ($_SESSION['tipo']=='root')
				{
					$status = 0;
					$message =  "Somente Ambiente do Aluno!";
				}
				else
				{
					$status = 0;
					$message =  "Somente Ambiente do Aluno!";
				}
			  
				$query = "UPDATE usuario SET sessao_usuario = '".session_id()."', ip_usuario='".$_SERVER['REMOTE_ADDR']."' " ."WHERE id_usuario = '".$_SESSION['id_usuario']."'";
				$conn->Execute($query);
										
				$sql = "SELECT * FROM acesso WHERE id_acesso = 1"; 
				$rs2 = $conn->Execute($sql); 
		
				$record = array(); 
				$record["id_usuario"] = $id_usuario;
				$record["data_hora_inicio"] = time();
				$record["local"] = GetAccessInformation();
				$insertSQL = $conn->GetInsertSQL($rs2, $record);
				$conn->Execute($insertSQL);
				$rs2->Close();
					
			}
			else 
			{
				$status = 0;
				$message =  "Senha inválida";
			}
		}
		else 
		{
			$status = 0;
			$message =  "Email inválido";
		}
		$rs->Close();
	
 }
	
	echo json_encode(array("status" => $status, "message" => $message));
	
	exit;
}
?>
<!DOCTYPE html>
<html>
<head>
<? include "include/head.php";?>
</head>
<body>
<!-- home -->
<div data-role="page" id="home" data-dom-cache="true">
  <div data-theme="b" data-role="header">
    <h3> M-ADAPTWEB </h3>
  </div>
  <div data-role="content" id="top">
    <? include "include/message.php";?>
    <? include "htmls/home.php";?>
  </div>
  <? include "include/footer.php";?>
</div>
<!-- login -->
<div data-role="page" id="login" data-dom-cache="true">
  <div data-theme="b" data-role="header"> <a href="#home" data-role="button" data-theme="b" data-transition="slide" data-icon="arrow-l">Voltar</a>
    <h3> M-ADAPTWEB </h3>
  </div>
  <div data-role="content" id="top">
    <? include "forms/formLogin.php";?>
  </div>
  <? include "include/footer.php";?>
</div>
<!-- cadastro -->
<div data-role="page" id="cadastro" data-dom-cache="true">
  <div data-theme="b" data-role="header"> <a href="#home" data-role="button" data-theme="b" data-transition="slide" data-icon="arrow-l">Voltar</a>
    <h3> M-ADAPTWEB </h3>
  </div>
  <div data-role="content" id="top">
    <? include "forms/formCadastrar.php";?>
  </div>
  <? include "include/footer.php";?>
</div>
</body>
</html>