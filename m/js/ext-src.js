jQuery.fn.reset = function () {
    this.each(function () {
        if ($(this).is('form')) {
            var button = jQuery(jQuery('<input type="reset" />'));
            button.hide();
            $(this).append(button);
            button.click().remove();
        } else if ($(this).parent('form').size()) {
            var button = jQuery(jQuery('<input type="reset" />'));
            button.hide();
            $(this).parent('form').append(button);
            button.click().remove();
        } else if ($(this).find('form').size()) {
            $(this).find('form').each(function () {
                var button = jQuery(jQuery('<input type="reset" />'));
                button.hide();
                $(this).append(button);
                button.click().remove();
            });
        }
    })
    return this;
};
$.extend({
getUrlVars: function(settings) {
var config = {
	'url': window.location.href
};	
if (settings) {
	$.extend(config, settings);
}
var vars = {};
var parts = config.url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) { vars[key] = value; });
return vars;
}
});
(function ($) {
	$.menssagemAlerta = function (settings) {
		var config = {
			'estilo': 'ui-body-a',
			'texto': 'ERRO'
		};
		if (settings) {
			$.extend(config, settings);
		}
		$('<div class="ui-loader ui-corner-all ' + config.estilo + ' ui-loader-verbose ui-loader-textonly"><h1>' + config.texto + '</h1></div>').css({
			"display": "block",
			"opacity": 0.96,
			"top": '50%'
		})
			.appendTo($.mobile.pageContainer)
			.delay(1500)
			.fadeOut(500, function () {
			$(this).remove();
		});
	};
})(jQuery);

$(document).on('pageinit', function(){
	
	 $.mobile.pushStateEnabled = false; 
	
	$('.dismiss').on('click', function(){
		$('.message').hide();		
	});
	

	$('.disabled').on('click', function(){
		$.menssagemAlerta({texto:'Campo desabilitado!'});	
	});
	

	$('.top').on('click', function(){
		$.mobile.silentScroll(0);
		return false;
	});
	
		
	$('#buttonLogin').unbind('click').click( function() {
		$.mobile.showPageLoadingMsg(); 
		 $.ajax({
				  type: "POST",
				  url: "index.php",
				  data: $('#formLogin').serialize(),
				  cache: false,
				  dataType: "json",
				  success: function (json) {
						if(json && json.status == 0) { 
							$.mobile.hidePageLoadingMsg();                              
							$.menssagemAlerta({texto:json.message});
						} else {
							$.mobile.changePage('a_painel.php', { transition: "slide" });
							$.menssagemAlerta({texto:'Login efetuado com sucesso!'});
						}
				  }
				});
	
	return false;
	});	

	$('#buttonCadastro').unbind('click').click( function() {
		$.mobile.showPageLoadingMsg(); 
		 $.ajax({
				  type: "POST",
				  url: "a_cadastro.php",
				  data: $('#formCadastro').serialize(),
				  cache: false,
				  dataType: "json",
				  success: function (json) {
						if(json && json.status == 0) { 
							$.mobile.hidePageLoadingMsg();                              
							$.menssagemAlerta({texto:json.message});
						} else {
							$.menssagemAlerta({texto:'Atualizado com sucesso!'});
							$.mobile.hidePageLoadingMsg();                              
						}
				  }
				});
	
	return false;
	});	

	$('#formLogin').reset();
	$('#formCadastrar').reset();	

});

$(document).delegate('a.top', 'click', function () {
    $('html, body').stop().animate({ scrollTop : 0 }, 500);
    return false;
});
 



