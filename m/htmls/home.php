        <h4>Seja Bem-Vindo ao Ambiente de Ensino-Aprendizagem Adaptativo na Web (AdaptWeb)</h4>
        <p>
            Este ambiente possibilita ao aluno ter acesso ao(s) material(is) instrucional(is) divulgado(s) pelo(s) seu (s) professor(es) de acordo com o seu perfil.
            <br/>Para navegar pelo ambiente, o aluno deve inicialmente efetuar o seu cadastro de usuário no item Novo Usuário presente no menu de opções. Após efetuado o cadastro, o aluno deve efetuar o seu login no item Login do menu de opções. Se o aluno já possuir um cadastro, deve apenas efetuar o seu login.
        </p>
        <a href="#login" data-role="button" data-transition="slide">Login</a>
        <a href="#cadastro" data-role="button" data-transition="slide">Cadastrar Usuário</a>