<?php 
//Dom XML
if (PHP_VERSION>='5')
  require_once('../include/domxml-xml.php');

include "../config/configuracoes.php";
include('../include/adodb/adodb.inc.php'); 	
$naveg = "livre";
session_register("tecno");
session_register("naveg");
$naveg = "livre";
$cor = $_GET['cor'];
$cor_fundo_DR=$_GET['cor_fundo_DR'];

global  	$chave, 
			$ac_achados,
			$id_aluno,
        	$tipo,
        	$logado,
        	$num_disc,
        	$num_curso,
	        $id_prof,
	        $disciplina,
	        $caminho,
			$naveg,
			$tecno;

 if ($A_LANG_IDIOMA_USER == "")
    include "../idioma/".$A_LANG_IDIOMA."/geral.php";
 else   
    include "../idioma/".$A_LANG_IDIOMA_USER."/geral.php";  

/***********************************************************************/
/* AREA DE CONFIGURACOES DE CORES                        	       */
/***********************************************************************/

$cor_padrao  = "$cor"               ;
$cor_fonte   = "#000000"            ;
$titulo_menu = "Conceito"           ;

/***********************************************************************/
/* SETAR CAMINHO ARQUIVO XML                       	    	       */
/***********************************************************************/

        $file =$DOCUMENT_ROOT.$caminho."/estrutura_topico.xml";


/***********************************************************************/
/* SETAR CAMINHO ARQUIVO XML - FIM                 		       */
/***********************************************************************/

/***********************************************************************/
/* CONEXAO COM O BANCO DE DADOS                    		       */
/***********************************************************************/
        
        // Alterado por AUGUSTO SANTOS
        $usuario=$A_DB_USER;
        $senha=$A_DB_PASS;
        $nomebd=$A_DB_DB;
        $array=array();
        include "../include/conecta.php";

/***********************************************************************/
/* FUNCAO UTILIZADO EM TODAS AS FUNCOES DO PROGRAMA                    */
/* ELA CAPTURA OS FILHOS DA ARVORE XML				       */
/***********************************************************************/

function getChildren($node)
{
        $temp = $node->children();
        $collection = array();
        $count=0;
        for ($x=0; $x<sizeof($temp); $x++)
        {
                if ($temp[$x]->type == XML_ELEMENT_NODE)
                        {
                             $collection[$count] = $temp[$x];
                         $count++;
                        }
        }
        return $collection;
}

/***********************************************************************/
/* FUNCAO PARA RETIRAR OS TOPICOS QUE NAO SAO DO CURSO		       */
/***********************************************************************/

function achar_curso($pai,$elem,$attri,$valor)
{
    $nodes = $pai->children();
    while ($node = array_shift($nodes))
    {
    if ($node->type==XML_ELEMENT_NODE)
    {
     if ($node->tagname==$elem)
     {
      $A=$node->get_attribute($attri);
      if ($A==$valor)
      {
      return 1;
      }
     }
    }
    }
    return 0;
}

/***********************************************************************/
/* ESTE FILTRO ACHA TODOS OS TOPICOS DO ARQUIVO XML		       */
/* FUNCAO RECURSIVA PARA GERAR VETORES COMO:			       */
/* CAMINHAMENTO DO SITE, A NAVEGACAO ENTRE IRMAOS E PAIS	       */
/* MENU DE NAVEGACAO, TANTO NO MODO LIVRE E TUTORIAL		       */
/***********************************************************************/

function GeraCaminho($no)

{       global
	
	$chave,
	$num_curso,
	$ac_achados,
	$vetor_achados;

 
	// LOOP PRINCIPAL DA RECURSAO

        for ($x=0; $x<sizeof($no); $x++)                 //for1
        {
                if ($no[$x]->type == XML_ELEMENT_NODE)   //if1
                {
                if (($no[$x]->tagname)=="topico")        //if2
                {
                        $elemento="curso";
                        $atributo="identcurso";

			// FILTRO POR CURSO
                        $resultado=achar_curso($no[$x],$elemento,$atributo,$num_curso);
                        if ($resultado==1)
                        {
				//ARMAZENAMENTO DOS DADOS DO XML EM VARIAVEIS UTILIZADAS NESTA FUNCAO
                	        $no[$x]->tagname;
                        	$topico_num  = $no[$x]->get_attribute("numtop");
	                        $topico_link = $no[$x]->get_attribute("arquivoxml");
	                        $topico_link = utf8_decode($topico_link);
	                        $topico_descricao  = $no[$x]->get_attribute("desctop");
	                        $topico_descricao  = utf8_decode($topico_descricao);
        	                $topico_abreviacao = $no[$x]->get_attribute("abreviacao");
	                        $topico_descricao_encodado = urlencode ($topico_descricao);
				$palavra_chave    =  $no[$x]->get_attribute("palchave");
				$palavra_chave	  =  utf8_decode($palavra_chave);
				$palavra_chave    =  str_replace(",", "", $palavra_chave);   

				if ((strstr(strtolower($palavra_chave),strtolower($chave))) or (strstr(strtolower($topico_descricao),strtolower($chave))))
				{ 	
					$vetor_achados[$ac_achados] =  "<font size=\"2\" face=\"Arial, Helvetica, sans-serif\" color=black>&nbsp;&nbsp;&nbsp;&nbsp; � <a href=\"ad-livre-conceito.php?arq_xml=$topico_link&acao_DR=Clique_Resultado_Busca&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado\" target=\"_top\"><font color=blue>".$topico_descricao."</fonte></a><br>";
										
					$ac_achados++; 
				}
			}			
			// CHAMADAS RECURSIVAS PARA PERCORRER O ARQUIVO XML
	                $dummy = getChildren($no[$x]);
	                GeraCaminho($dummy);

                }  //if2

                }  //if1

            }  	   //for1
		

} 	    //fim laco principal da funcao


   $ac_achados = 0;
   $dom = xmldocfile($file);
   $root=$dom->root();
   $children = $root->children();
   GeraCaminho($children);

?>


<html>
<head>
<title>	
	<? echo A_LANG_SEARCH_RESULTS; ?>
</title>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script> 


 <style type="text/css">

.cell_over  { BACKGROUND-COLOR: #EEEEEE   }
.cell_out   { BACKGROUND-COLOR: #FFFFFF   }
.cell_over1 { BACKGROUND-COLOR: #C9E4FC   }
.cell_out1  { BACKGROUND-COLOR: #FFFFCC   }
body a:link {text-decoration: none}
body a:hover {text-decoration: underline;}
body a:visited {text-decoration: none;}

  
  .tabela_td{
  height:-10px;
  }
   .fundo_categoria {
  margin-top:10px;
    margin-bottom:10px;
    padding-left:2px;  
    padding-right:2px;  
    padding-bottom:6px;  
  background-color:#dddddd;  
  border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    min-width:300px;

 }
 
  .fundo_categoria_titulo {
    font-family:arial,helvetica; 
  font-size:12px;
  font-weight: bold;
    padding-top:2px;  
  padding-left:2px;  
    padding-bottom:2px;  
  min-height:25px;
 }
 
 
   .fundo_branco {
    margin-bottom:0px;
    padding-left:0px;  
    padding-right:0px;  
    padding-bottom:0px;  
  padding-top:3px;  
  background-color:#7bb0dc;  
    min-width:300px;
  font-family:arial,helvetica; 
  font-size:11px;
 }
 
    .fundo_branco_redondo {
    margin-bottom:0px;
    margin-left:5px;
    margin-right:5px;
    padding-left:10px;  
    padding-right:10px;  
    padding-bottom:3px;  
    padding-top:3px;      
    
    color: black;
  
    border-radius:3px;-moz-border-radius:3px; -webkit-border-radius:3px;
    height:22px;
 }
 
   .menu_dr {
    margin-bottom:5px;
    padding-left:2px;  
    padding-right:2px;  
    padding-bottom:6px;  
  background-color:#7bb0dc;  
  border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    
 }
 
   .fundo_busca {
  margin-top:10px;
    margin-bottom:5px;
    padding-left:2px;  
    padding-right:2px;  
    padding-bottom:6px;  
  background-color:#dddddd;  
  border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    
 }
 
    .fundo_tags {
  margin-top:10px;
    margin-bottom:5px;
    padding-left:2px;  
    padding-right:2px;  
    padding-bottom:6px;  
  background-color:#c3c3c3;  
  border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    
 }
 
 
 .busca{
    background-color: #FFF;
    color: #000000;
    font-size: 14px;
    margin-top: 5px;
  margin-bottom: 5px;
  margin-left: 5px;
    width:270px; 
    height:20px;  
  -moz-box-shadow:inset 0 1px 4px #d1d1d1;-webkit-box-shadow:inset 0 1px 4px #d1d1d1;box-shadow:inset 0 1px 4px #d1d1d1;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-transition: all 0.2s ease-in-out;-moz-transition: all 0.2s ease-in-out;-o-transition: all 0.2s ease-in-out;-ms-transition: all 0.2s ease-in-out;transition: all 0.2s ease-in-out;       
}

.purple_DR {
color:purple;
text-decoration: none}

.blue_DR {
color:blue;
text-decoration: none}

    .scrollup{
      width:40px;
      height:40px;      
      text-indent:-9999px;
      opacity:0.5;
      position:fixed;
      bottom:50px;
      right:50px;
      display:none;     
      background: url('../imagens/icon_top.png') no-repeat;
    }
  
    

    </style>
    
    <script type="text/javascript">
      $(document).ready(function(){ 
      
      $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
          $('.scrollup').fadeIn();
        } else {
          $('.scrollup').fadeOut();
        }
      }); 
      
      $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
        return false;
      });
 
    });
    </script>


</head>



<body bgcolor="#<?php echo $cor_fundo_DR;?>" text="#000000" vlink="#0000FF" link="#0000FF" alink="#FF0000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" border="0" cellpadding="5" cellspacing="1">
  <tr >
    <td valign="top" bgcolor=#<? echo $cor_padrao; ?> width="100%" style="border-radius:3px;-moz-border-radius:3px; -webkit-border-radius:3px;"> 
      <div align="center">

        <?php

  {
         echo "<table cellpadding=0 cellspacing=0 border=0>";

         echo "<tr height=30><td><center>$link_pai</center></td><td><td>$link_irmao_ant</td><td>";
         ?>
         <font color=<?php echo $cor_fonte; ?> face="Arial, Helvetica, sans-serif" size="4">
         <div class = "fundo_branco_redondo">
        
         <? echo A_LANG_SEARCH_SYSTEM; ?>
        
         </div>
         </font>
         <?php
         echo "</td><td>$link_irmao_pos</td><td><center>$link_filho</center></td></tr>";
         echo "</table>";
  }
        ?>


      </div>
    </td>
  </tr>
</table>

<table width="100%" border="0" cellpadding="5" cellspacing="1">
  <tr height=26>
    <td height="22" bgcolor="#eeeeee" width="100%" style="border-radius:3px;-moz-border-radius:3px; -webkit-border-radius:3px; padding-left:5px;" valign=middle>
	      <div align="left">
		<font face=arial size=2 color=black>
		<?php
		echo "&nbsp;<b> � </b>".A_LANG_SEARCH_WORD.": <b>".strtoupper($chave)."</b><br>"; 
		?>
		</font>
	      </div>
    </td>
  </tr>

</table>
<br>

<br>
	<?php

	    if ($ac_achados > 1)
	    {
	    	echo "<center><font size=\"2\" face=\"Arial, Helvetica, sans-serif\">".A_LANG_SEARCH_FOUND.$ac_achados.A_LANG_SEARCH_OCCURRANCES."<b>".strtoupper($chave)."</b>".A_LANG_SEARCH_IN_TOPIC."</font></center>";
	    }
	    else	
	    if ($ac_achados == 1)
	    {
	    	echo "<center><font size=\"2\" face=\"Arial, Helvetica, sans-serif\">".A_LANG_SEARCH_FOUND1.$ac_achados.A_LANG_SEARCH_OCCURRANCE."<b>".strtoupper($chave)."</b>".A_LANG_SEARCH_IN_TOPIC."</font></center>";
	    }
	    else
	    {
		echo "<center><font size=\"2\" face=\"Arial, Helvetica, sans-serif\">".A_LANG_SEARCH_NOT_FOUND."<b>".strtoupper($chave)."</b>".A_LANG_SEARCH_IN_TOPIC."</font></center>"; 
	    }
	?>


<br>

<?php
	   for ($ct_x = 0; $ct_x < $ac_achados; $ct_x ++)
	   {
	       echo $vetor_achados[$ct_x];
	   }

?>

<br><br>

<table width="100%" border="0" align="center" cellpadding="10" cellspacing="1">

  <tr>
    <td valign="button" bgcolor=#<? echo $cor_padrao; ?> width="100%" style="border-radius:3px;-moz-border-radius:3px; -webkit-border-radius:3px;">
      <div align="right">
      <font size="1" face="Arial, Helvetica, sans-serif"><? echo " &nbsp; &nbsp;".A_LANG_SYSTEM_COPYRIGHT."";?> </font>
      </div>
    </td>
  </tr>
</table> 





</body>
</html>






