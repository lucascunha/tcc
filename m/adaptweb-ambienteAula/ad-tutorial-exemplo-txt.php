<?php
//Dom XML
if (PHP_VERSION>='5')
  require_once('../include/domxml-xml.php');

$naveg = "tutorial";
$_SESSION['naveg']  = "tutorial";




global  	$id_aluno,
        	$tipo, 
        	$logado,
        	$num_disc,
        	$num_curso,
	        $id_prof,
	        $disciplina,
	        $caminho,
		$naveg,
		$achou_exemplo;

$parametro1 = $_GET['parametro1'];
$parametro2 = $_GET['parametro2'];
$parametro3 = $_GET['parametro3'];
$arq_xml    = $_GET['arq_xml'];
$tecno      = $_GET['tecno'];

if (!($arq_xml))
{
  $parametro1 = $_POST['parametro1'];
  $parametro2 = $_POST['parametro2'];
  $parametro3 = $_POST['parametro3'];
  $arq_xml    = $_POST['arq_xml'];
  $tecno      = $_POST['tecno'];
}

session_start();
$logado         = $_SESSION['logado'];
$id_usuario     = $_SESSION['id_usuario'];
$email_usuario  = $_SESSION['email_usuario'];
$A_LANG_IDIOMA_USER=$_SESSION['A_LANG_IDIOMA_USER'];
$tipo_usuario   = $_SESSION['tipo_usuario'];
$status_usuario = $_SESSION['status_usuario'];
$id_aluno       = $_SESSION['id_aluno'];
$tipo           = $_SESSION['tipo'];

$num_disc       = $_SESSION['num_disc'];   
$num_curso      = $_SESSION['num_curso'];  
$curso          = $_SESSION['curso'];      
$id_prof        = $_SESSION['id_prof'];    
$caminho        = $_SESSION['caminho'];    
$disciplina     = $_SESSION['disciplina']; 
$naveg          = $_SESSION['naveg'];

// Troca de idioma - idioma de configura��o padr�o ou idioma selecionado pelo usu�rio no momento do cadastro
 if ($A_LANG_IDIOMA_USER == "")
    include "../idioma/".$A_LANG_IDIOMA."/geral.php";
 else   
    include "../idioma/".$A_LANG_IDIOMA_USER."/geral.php";  


/***********************************************************************/
/* AREA DE CONFIGURACOES DE CORES                        	       */
/***********************************************************************/

$cor_padrao  = "93aa33"            ;
$cor_fonte   = "000000"            ;
$titulo_menu = "Exemplo Tutorial"  ;



/***********************************************************************/
/* SETAR CAMINHO ARQUIVO XML                                            */
/***********************************************************************/

  $file =$DOCUMENT_ROOT.$caminho."/estrutura_topico.xml";


/***********************************************************************/
/* CONEXAO COM O BANCO DE DADOS                                        */
/***********************************************************************/

  $usuario=$A_DB_USER;
  $senha=$A_DB_PASS;
  $nomebd=$A_DB_DB;
 
  $array=array();
  include "../include/conecta.php";

/***********************************************************************/
/* INSERCAO DA NAVEGACAO NO BANCO DE DADOS         		       */
/***********************************************************************/

	$insert ="insert into log_usuario (id_usuario, id_disc, id_curso, tecnologia, modo_naveg, topico, data_acesso, hora_acesso, menu) ";
	$insert.="values ($id_aluno, $num_disc, $num_curso, ' ', '$naveg', '$parametro2', current_date, current_time, 'exemplo')";

  if (!mysql_query($insert,$id)){
	$erro=mysql_error();
	echo $erro;	
	}


/***********************************************************************/
/* COR FUNDO                                                           */
/***********************************************************************/
		$cor_fundo_DR = "ffffff";
		
/******************** Menu Esquerdo Principal **************************/

$nome_arquivo     = "ad-tutorial-exemplo.php";

$nome_link1       = A_LANG_TOPIC;
$link1            = "./ad-tutorial-conceito.php?arq_xml=$arq_xml&parametro1=$parametro1&parametro2=$parametro2&parametro3=$parametro3&acao_DR=Clique_Menu_Categoria_emExemplo";
$nome_link2       = A_LANG_TOPIC_EXEMPLES;
$link2            = "./ad-tutorial-exemplo.php?arq_xml=$arq_xml&parametro1=$parametro1&parametro2=$parametro2&parametro3=$parametro3&acao_DR=Clique_Menu_Categoria_emExemplo";
$nome_link3       = A_LANG_TOPIC_EXERCISES;
$link3            = "./ad-tutorial-exercicio.php?arq_xml=$arq_xml&parametro1=$parametro1&parametro2=$parametro2&parametro3=$parametro3&acao_DR=Clique_Menu_Categoria_emExemplo";
$nome_link4       = A_LANG_TOPIC_COMPLEMENTARY;
$link4            = "./ad-tutorial-material.php?arq_xml=$arq_xml&parametro1=$parametro1&parametro2=$parametro2&parametro3=$parametro3&acao_DR=Clique_Menu_Categoria_emExemplo";

/*********************** Outros Links (Direito) ************************/

$nome_link5       = A_LANG_SHOW_MENU;
$link5            = "#";
$nome_link6       = A_LANG_CONFIG;
$link6            = "ad-tutorial-configurator.php?cor=$cor_padrao&acao_DR=Clique_Config_emExemplo&num_disc=$num_disc&num_curso=$num_curso&categoria=Exemplo&naveg=$naveg&cor_fundo_DR=$cor_fundo_DR";
$nome_link7       = A_LANG_MAP;
$link7            = "ad-tutorial-mapa_xml.php?cor=$cor_padrao&localizacao=conceito&parametro2=$parametro2&acao_DR=Clique_Mapa_emExemplo&num_disc=$num_disc&num_curso=$num_curso&categoria=Exemplo&cor_fundo_DR=$cor_fundo_DR";
$nome_link8       = A_LANG_MNU_EXIT;
$link8            = "Javascript:window.close();"; //Tinha antes --> "p_faz_sair.php"; Daniel mudou
$nome_link9       = A_LANG_HELP;
$link9            = "ad-tutorial-ajuda.php?cor=$cor_padrao&acao_DR=Clique_Ajuda_emExemplo&num_disc=$num_disc&num_curso=$num_curso&categoria=Exemplo&cor_fundo_DR=$cor_fundo_DR";
$nome_link10 = "";
		
/***********************************************************************/
/* FUNCAO UTILIZADO EM TODAS AS FUNCOES DO PROGRAMA                    */
/* ELA CAPTURA OS FILHOS DA ARVORE XML				       */
/***********************************************************************/

function getChildren($node)
{
        $temp = $node->children();
        $collection = array();
        $count=0;
        for ($x=0; $x<sizeof($temp); $x++)
        {
                if ($temp[$x]->type == XML_ELEMENT_NODE)
                        {
                             $collection[$count] = $temp[$x];
                         $count++;
                        }
        }
        return $collection;
}

/***********************************************************************/
/* FUNCAO PARA RETIRAR OS TOPICOS QUE NAO SAO DO CURSO		       */
/***********************************************************************/

function achar_curso($pai,$elem,$attri,$valor)
{
    $nodes = $pai->children();
    while ($node = array_shift($nodes))
    {
    if ($node->type==XML_ELEMENT_NODE)
    {
     if ($node->tagname==$elem)
     {
      $A=$node->get_attribute($attri);
      if ($A==$valor)
      {
      return 1;
      }
     }
    }
    }
    return 0;
}


/***********************************************************************/
/* FUNCAO PARA VERIFICAR SE EXISTE EXEMPLO OU MATCOMPLEMENTAR	       */
/***********************************************************************/

function achar_elemento($pai,$elem,$attri,$valor,$numero_topico)
{   global $ac_vt_material;

    $nodes = $pai->children();
    while ($node = array_shift($nodes))
    {
    if ($node->type==XML_ELEMENT_NODE)
    {
      if ($node->tagname=="curso")
      {
       $A=$node->get_attribute($attri);
        if ($A==$valor)
        { //if oo
            $filho_curso = getchildren($node);
            for ($y=0; $y<sizeof($filho_curso); $y++)
            {//for xy
               if ($filho_curso[$y]->type == XML_ELEMENT_NODE)
               {//if xx
                  if (($filho_curso[$y]->tagname)=="elementos")
                  {
                  $filho_elemento = getchildren($filho_curso[$y]);
                  for ($z=0; $z<sizeof($filho_elemento); $z++)
                  {//for xyxx
                     if ($filho_elemento[$z]->type == XML_ELEMENT_NODE)
                     {//if xxxx
                       if (($filho_elemento[$z]->tagname)=="exemplo")
                       {
                       $A=$filho_elemento[$z]->get_attribute("possuiexemp");
                       if ($A=="sim")
                       {
                        return 1;
                       }
                       }
                     } //for xxxx
                  } //if xyxx
                  }
                } //for xx
             } //if xy
        }  //if oo
      }
    }
    }
    return 0;
}


/***********************************************************************/
/* FUNCAO RESPONSAVEL EM HABILITAR OU NAO AS GUIAS SUPERIORES	       */
/* DE ACORDO COM OS PARAMETROS PASSADOS				       */
/***********************************************************************/

function filtro_habilita_guia($pai,$elem,$attri,$valor,$numero_topico,$tag_busca,$tag_busca_atr)
{   global $ac_vt_material;

    $nodes = $pai->children();
    while ($node = array_shift($nodes))
    {
    if ($node->type==XML_ELEMENT_NODE)
    {
      if ($node->tagname=="curso")
      {
       $A=$node->get_attribute($attri);
        if ($A==$valor)
        { //if oo
            $filho_curso = getchildren($node);
            for ($y=0; $y<sizeof($filho_curso); $y++)
            {//for xy
               if ($filho_curso[$y]->type == XML_ELEMENT_NODE)
               {//if xx
                  if (($filho_curso[$y]->tagname)=="elementos")
                  {
                  $filho_elemento = getchildren($filho_curso[$y]);
                  for ($z=0; $z<sizeof($filho_elemento); $z++)
                  {//for xyxx
                     if ($filho_elemento[$z]->type == XML_ELEMENT_NODE)
                     {//if xxxx
                       if (($filho_elemento[$z]->tagname)==$tag_busca)
                       {
                       $A=$filho_elemento[$z]->get_attribute($tag_busca_atr);
                       if ($A=="sim")
                       {
                        return 1;
                       }
                       }
                     } //for xxxx
                  } //if xyxx
                  }
                } //for xx
             } //if xy
        }  //if oo
      }
    }
    }
    return 0;
}

/***********************************************************************/
/* FUNCAO QUE CRIA UM VETOR CONTENDO VALORES DE PREREQUISITOS	       */
/* APENAS ACESSADO NO MODO TUTORIAL				       */
/***********************************************************************/

function filtro_prerequisitos($pai,$numero_topico)
{   global $ac_vt_prereq, $vetor_prereq;

    $nodes = $pai->children();
    while ($node = array_shift($nodes))
    {
    if ($node->type==XML_ELEMENT_NODE)
    {
      if ($node->tagname=="prereq")   //verifica a tag prereq
      {
       	$vetor_prereq[$ac_vt_prereq] = $node -> get_attribute("identprereq");
	//$vetor_prereq = $node -> get_attribute("identprereq");
       	//echo $numero_topico."=".$ac_vt_prereq."===>".$vetor_prereq[$ac_vt_prereq]."<br>";
       	$ac_vt_prereq ++;
      }
    }
    }
    return 0;
}


/***********************************************************************/
/* ESTE FILTRO ACHA TODOS OS TOPICOS DO ARQUIVO XML		       */
/* FUNCAO RECURSIVA PARA GERAR VETORES COMO:			       */
/* CAMINHAMENTO DO SITE, A NAVEGACAO ENTRE IRMAOS E PAIS	       */
/* MENU DE NAVEGACAO, TANTO NO MODO LIVRE E TUTORIAL		       */
/***********************************************************************/

function GeraCaminho($no, $para1, $para2, $quebra)

{        global 	// VARIAVEIS GERAIS
			$id_aluno,
	        	$tipo,
	        	$logado,
	        	$num_disc,
	        	$num_curso,
	        	$id_prof,
	        	$disciplina,
        		$caminho,
			$naveg, 
			$id,
			$achou_exemplo,
      $nome_arquivo,     

	                // VARIAVEIS PARA GERACAO DO CAMINHAMENTO
	               	$vetor,
        	       	$vetor_caminho,
	               	$ac_vetor_caminho,
	               	$cont,
	               	$topico_num_tratado,
        	       	$topico_descricao,
	               	$para2_tratado,
	               	$topico_num,
	               	$topico_link,
	               	$topico_visitado,
	               	$topico_abreviacao,
	               	$topico_descricao_encodado,
	               	$arquivo_xml,
	               	$guia_exemplo,
	               	$guia_exercicio,
	               	$guia_material,
	               	$parametro3,
	
			// VARIAVEIS PARA PREREQUISITOS
			$ac_vt_prereq,
			$vetor_prereq,
	
	                // VARIAVEIS PARA GERACAO DO MENU
	               	$vetor_topico,
        	       	$ac_vt_topico,
	               	$ct_x,
	               	$identacao,
	               	$flag,
	
	                // VARIAVEIS PARA GERACAO DA NAVEGACAO ENTRE PAIS/FILHOS
	               	$link_pai,
	               	$link_filho,
	               	$link_irmao_ant,
	               	$link_irmao_pos,
	               	$num_curso;



	// LOOP PRINCIPAL DA RECURSAO

        for ($x=0; $x<sizeof($no); $x++)                 //for1
        {
                if ($no[$x]->type == XML_ELEMENT_NODE)   //if1
                {
                if (($no[$x]->tagname)=="topico")        //if2
                {
                        $elemento="curso";
                        $atributo="identcurso";

			// FILTRO POR CURSO
                        $resultado=achar_curso($no[$x],$elemento,$atributo,$num_curso);
                        if ($resultado==1)
                        {

                        // FILTRO POR EXEMPLO

                        $achou_exemplo=achar_elemento($no[$x],"curso","identcurso",$num_curso,$topico_num);

			//ARMAZENAMENTO DOS DADOS DO XML EM VARIAVEIS UTILIZADAS NESTA FUNCAO
                        $no[$x]->tagname;
                        $topico_num  = $no[$x]->get_attribute("numtop");
                        $topico_link = $no[$x]->get_attribute("arquivoxml");
                        $topico_link = utf8_decode($topico_link);
                        $pos 	     = strpos($topico_link,".");
                        $ext1 	     = substr($topico_link,$pos+1);
                        $qtde 	     = strlen($topico_link)-(strlen($ext1)+1);
                        $arq_x 	     = substr($topico_link,0,$qtde);
                        $ext2 	     = ".xml";
                        $arquivo_xml = $arq_x.$ext2;
                        $topico_descricao  = $no[$x]->get_attribute("desctop");
                        $topico_descricao  = utf8_decode($topico_descricao);
                        $topico_abreviacao = $no[$x]->get_attribute("abreviacao");
                        $topico_descricao_encodado = urlencode ($topico_descricao);

			// CHAMADAS PARA VERIFICAR QUAIS GUIAS HABILITAR
                        if ($parametro3 == $topico_descricao)
                        {
                        	$guia_exercicio=filtro_habilita_guia($no[$x],"curso","identcurso",$num_curso,$topico_num,"exercicio","possuiexerc");
                        	$guia_material =filtro_habilita_guia($no[$x],"curso","identcurso",$num_curso,$topico_num,"matcomp","possuimatcomp");
                        }


                  if ((strlen($topico_descricao)) > 55)
                  {
                      $topico_descricao_aux = substr($topico_descricao, 0, 53). " ...";
                  }  
                  else
                  {
                      $topico_descricao_aux = $topico_descricao; 
                  } 


      // INICIO DO BLOCO QUE CRIA VETOR PARA EXIBICAO DO MENU IDENTADO

                        $comprimento = strlen ($topico_num);
                        if ($comprimento == 2) 
                            $comprimento = 1;
                        $identacao = "";


      // <FOR> PARA MONTAR O COMPRIMENTO DA IDENTACAO
                        for ($xx = 0; $xx < $comprimento; $xx ++)
                          {
                            if ($xx ==  ($comprimento - 1))
                                                        {
                              $identacao = $identacao."<font color=\"gray\"> � </font>";
                            }
                            else
                            {
                              $identacao = $identacao."<img src = ../imagens/menu/blank.gif>";
                            }
                          }

			// ESTE <IF> GERA MENU PARA NAVEGACAO NO MODO LIVRE

			{
                        	// CASO O PARAMETRO2 = TOPICO ATUAL, IMPRIME O MENU NA COR VERMELHA
                        	if ($para2 == $topico_num)                
                        	{
	                         	$vetor_topico[$ac_vt_topico] =$identacao."<font color=red><tooltip class='tooltip' title='$topico_descricao'>".$topico_descricao_aux."</tooltip></font>";
		                        $ac_vt_topico ++;
	                        }
                	        else
                        	if ($achou_exemplo==1)
                        	{
        				$select="select id_usuario from log_usuario where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disc=$num_disc and topico='$topico_num' and menu='exemplo'";
						
					mysql_query($select);
					$result_select=mysql_affected_rows();
          if ($result_select == 0)
          {
                              $vetor_topico[$ac_vt_topico] =$identacao."<a class=blue_DR href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Menu_Lateral_emExemplo><font color=blue><tooltip class='tooltip' title='$topico_descricao'>".$topico_descricao_aux."</tooltip></font></a>";
                              $ac_vt_topico ++;
          }
          else
          {
                              $vetor_topico[$ac_vt_topico] =$identacao."<a class=purple_DR href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Menu_Lateral_emExemplo><font color=purple><tooltip class='tooltip' title='$topico_descricao'>".$topico_descricao_aux."</tooltip></font></a>";
                              $ac_vt_topico ++;
          }
                          }
                          else
                          {
                            $vetor_topico[$ac_vt_topico] =$identacao."<font color=gray><tooltip class='tooltip' title='$topico_descricao'>".$topico_descricao_aux."</tooltip></font></a>";
                            $ac_vt_topico ++;
                          }
      }

			// ESTA PARTE SERVIRA PARA GERAR OS TOPICOS QUE SAO OS IRMAOS E PAIS
			// ESTAS 2 VARIAVEIS RECEBEM OS TOPICOS SEM OS PONTOS VINDOS DOS TOPICOS XML, PARA GERAR PROVAVEIS IRMAOS E PAIS
                        $topico_num_tratado = str_replace(".", "", $topico_num );		// Numero atual *********//
                        $para2_tratado      = str_replace(".", "", $para2 );     		// Parametro 2  *********//

			// MONTA O VETOR DE TOPICOS NUMERICOS (AKI SOH PASSA 1 VEZ)
                        if ($quebra == 0)                                        		
                        {
				// PEGA AS SUBSTRINGS POR EX: 2 22 223 2231 etc
                           	for ($cont=1 ; $cont <= strlen($para2_tratado); $cont++)
                        	{
                           		$vetor[$cont] = substr ($para2_tratado, 0, $cont);
                        	}
                           	$quebra = 1;
                              	$cont   = 1;
                        }
			
			// CHAMADAS RECURSIVAS PARA PERCORRER O ARQUIVO XML
                        $dummy = getChildren($no[$x]);
                        GeraCaminho($dummy, $para1, $para2, $quebra);
                        }
                        else
                        {
                         $no[$x]->unlink_node();
                        }

                }  //if2

                }  //if1

                        // GERA O VETOR DO CAMINHAMENTO (O CAMINHAMENTO EH ARMAZENADO NUM VETOR)

                        if ($cont <= strlen($para2_tratado))
                        {
                            if ($topico_num_tratado == $vetor[$cont])
                            {
                              if ($topico_num_tratado != $para2_tratado)             // Se for diferente imprime azul senao preto
                              {
                                  if ($achou_exemplo==1)
                                  {
                                      $vetor_caminho[$ac_vetor_caminho] = "<font face=arial size=2 color=black><b>"." � "."</b></font>"."<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Caminhamento_emExemplo><font face=arial size=2 color=blue>".$topico_descricao."</font></a>";
                                      $ac_vetor_caminho ++;
                                  }
                                  else
                                  {
                                      $vetor_caminho[$ac_vetor_caminho] = "<font face=arial size=2 color=black><b>"." � "."</b></font>"."<font size=2 face=arial color=gray><!--<b>-->".$topico_descricao."<!--</b>--></font>";
                                      $ac_vetor_caminho ++;
                                  }
                              }
                              else
                              {
                                    $vetor_caminho[$ac_vetor_caminho] = "<font  face=arial size=2 color=black><b>"." � "."</b></font>"."<font face=arial size=2 color=black><b>".$topico_descricao."</b></font>";
                                    $ac_vetor_caminho ++;
                              }
                                  $cont++;
                            }
                         }


			
			// GERA OS LINKS DE NAVEGACAO POR SETAS - ATRIBUI PARA AS VARIAVEIS LINK_PAI, FILHO, IRMAO_ANT E IRMAO_POS O LINK PARA NAVEGAR
                        if ($topico_num_tratado == substr ($para2_tratado, 0, strlen($para2_tratado)-1) && (strlen ($para2_tratado) > 1) && ($achou_exemplo==1))
                        {
                        	$link_pai = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emExemplo><img src=./imagens/seta_c_icon.png  width=20 border=0 class=\"tooltip\" title=\"$topico_descricao\" ></a>
							<div id=\"t1\" style=\"display:none;\">$topico_descricao</div> ";
                        }
                        if (($topico_num_tratado == ($para2_tratado + 1)) && ($achou_exemplo==1))
                        {
                        	$link_irmao_pos = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emExemplo><img src=./imagens/seta_d_icon.png border=0  width=25 class=\"tooltip\" title=\"$topico_descricao\" ></a>
							<div id=\"t2\" style=\"display:none;\">$topico_descricao</div> ";
                        }
                        if (($topico_num_tratado == ($para2_tratado - 1)) && ($achou_exemplo==1))
                        {
                                $link_irmao_ant = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emExemplo><img src=./imagens/seta_e_icon.png border=0  width=25 class=\"tooltip\" title=\"$topico_descricao\" ></a>
							<div id=\"t3\" style=\"display:none;\">$topico_descricao</div> ";
                        }
                        if (($topico_num_tratado == ($para2_tratado."1")) && ($achou_exemplo==1))
                        {
                         	$link_filho = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emExemplo><img src=./imagens/seta_b_icon.png border=0   width=20 class=\"tooltip\" title=\"$topico_descricao\" ></a>
							<div id=\"t4\" style=\"display:none;\">$topico_descricao</div> ";
                               	$flag = 1;
                        }
                        if ($flag <> 1)
                        {
                        	if (($topico_num_tratado == ($para2_tratado."2")) && ($achou_exemplo==1))
                         	{
                               		$link_filho = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emExemplo><img src=./imagens/seta_b_icon.png border=0   width=20 class=\"tooltip\" title=\"$topico_descricao\" ></a>
							<div id=\"t5\" style=\"display:none;\">$topico_descricao</div> ";
                               		$flag = 2;
                         	}
                        }
                        if ($flag > 1)
                        {
                        	if (($topico_num_tratado == ($para2_tratado."3")) && ($achou_exemplo==1))
                         	{
                               		$link_filho = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emExemplo><img src=./imagens/seta_b_icon.png border=0   width=20 class=\"tooltip\" title=\"$topico_descricao\" ></a>
							<div id=\"t6\" style=\"display:none;\">$topico_descricao</div> ";
                               		$flag = 3;
                         	}
                        }
                        if ($flag > 2)
                        {
                        	if (($topico_num_tratado == ($para2_tratado."4")) && ($achou_exemplo==1))
                         	{
                               		$link_filho = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emExemplo><img src=./imagens/seta_b_icon.png border=0   width=20 class=\"tooltip\" title=\"$topico_descricao\" ></a>
							<div id=\"t7\" style=\"display:none;\">$topico_descricao</div> ";
                               		$flag = 4;
                         	}
                        }
                        if ($flag > 3)
                        {
                        	if (($topico_num_tratado == ($para2_tratado."5")) && ($achou_exemplo==1))
                         	{
                               		$link_filho = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emExemplo><img src=./imagens/seta_b_icon.png border=0   width=20 class=\"tooltip\" title=\"$topico_descricao\" ></a>
							<div id=\"t8\" style=\"display:none;\">$topico_descricao</div> ";
                               		$flag = 5;
                         	}
                        }

            }  		//for1
			if ($link_pai == NULL)
			{$link_pai = "<img src=../imagens/seta_c_icon-g.png border=0 width=20>";
			}
			if ($link_filho == NULL)
			{$link_filho = "<img src=../imagens/seta_b_icon-g.png border=0  width=20>";
			}
			if ($link_irmao_ant == NULL)
			{$link_irmao_ant ="<img src=../imagens/seta_e_icon-g.png border=0 width=25>";
			}
			if ($link_irmao_pos == NULL)
			{$link_irmao_pos = "<img src=../imagens/seta_d_icon-g.png border=0 width=25>";
			}
} 	                //laco pricipal da funcao


   $link_pai =       NULL;
   $link_filho =     NULL;
   $link_irmao_ant = NULL;
   $link_irmao_pos = NULL;

   $ac_vt_prereq = 0;
   $ac_vt_topico = 0;
   $ac_vetor_caminho = 0;
   $quebra = 0;
   $dom = xmldocfile($file);
   $root=$dom->root();
   $children = $root->children();
   GeraCaminho($children, $parametro1, $parametro2, $quebra);

?>


  
        <?php
		

           global $tecno;

           include "ad-lib-exemplo.php";
           $file2 =$DOCUMENT_ROOT.$caminho."/".$arq_xml;

           $dom2=xmldocfile($file2);
           $root2=$dom2->root();
           $children2=$root2->children();
           echo    utf8_encode("<h3>".A_LANG_EXAMPLE_LIST." para ".$parametro3."</h3>");
           acha_exemplo($children2,$num_curso,$arq_xml);
          ?>
        
