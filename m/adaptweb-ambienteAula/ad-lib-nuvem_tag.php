<?php 
//Dom XML
if (PHP_VERSION>='5')
	require_once('../include/domxml-xml.php');

include "../config/configuracoes.php";
//session_register("naveg");
$cor_bg=$_GET['cor_bg'];



global  	
$string_achados,$vetor_achados_sem_link	,
			$chave, 
			$ac_achados,
			$id_aluno,
        	$tipo,
        	$logado,
        	$num_disc,
        	$num_curso,
	        $id_prof,
	        $disciplina,
	        $caminho,
			$naveg,
			$tecno;

session_start();
$logado         = $_SESSION['logado'];
$id_usuario     = $_SESSION['id_usuario'];
$email_usuario  = $_SESSION['email_usuario'];
$A_LANG_IDIOMA_USER=$_SESSION['A_LANG_IDIOMA_USER'];
$tipo_usuario   = $_SESSION['tipo_usuario'];
$status_usuario = $_SESSION['status_usuario'];
$id_aluno       = $_SESSION['id_aluno'];
$tipo           = $_SESSION['tipo'];

$num_disc       = $_SESSION['num_disc'];   
$num_curso      = $_SESSION['num_curso'];  
$curso          = $_SESSION['curso'];      
$id_prof        = $_SESSION['id_prof'];    
$caminho        = $_SESSION['caminho'];    
$disciplina     = $_SESSION['disciplina']; 
$naveg          = $_SESSION['naveg'];


$cor 		= $_GET['cor'];
$acao_DR    = $_GET['acao_DR'];
$categoria 	= $_GET['categoria'];
$acao_DR01 	= $_GET['acao_DR01'];
$num_disc 	= $_GET['num_disc'];
$num_curso 	= $_GET['num_curso'];
$topico_DR 	= $_GET['topico_DR'];
$cor_bg 	= $_GET['cor_bg'];


/* CONEXAO COM O BANCO DE DADOS                    		       */
        $usuario=$A_DB_USER;
        $senha=$A_DB_PASS;
        $nomebd=$A_DB_DB;
        $array=array();
        include "../include/conecta.php";


/***********************************************************************/
/* AREA DE CONFIGURACOES DE CORES                        	       */
/***********************************************************************/

$cor_padrao  = $_GET['cor']         ;
$cor_fonte   = "#000000"            ;
$titulo_menu = "Conceito"           ;

/***********************************************************************/
/* SETAR CAMINHO ARQUIVO XML                       	    	       */
/***********************************************************************/

        $file =$DOCUMENT_ROOT.$caminho."/estrutura_topico.xml";


/***********************************************************************/
/* SETAR CAMINHO ARQUIVO XML - FIM                 		       */
/***********************************************************************/


/***********************************************************************/
/* FUNCAO UTILIZADO EM TODAS AS FUNCOES DO PROGRAMA                    */
/* ELA CAPTURA OS FILHOS DA ARVORE XML				       */
/***********************************************************************/

function getChildren($node)
{
        $temp = $node->children();
        $collection = array();
        $count=0;
        for ($x=0; $x<sizeof($temp); $x++)
        {
                if ($temp[$x]->type == XML_ELEMENT_NODE)
                        {
                             $collection[$count] = $temp[$x];
                         $count++;
                        }
        }
        return $collection;
}

/***********************************************************************/
/* FUNCAO PARA RETIRAR OS TOPICOS QUE NAO SAO DO CURSO		       */
/***********************************************************************/

function achar_curso($pai,$elem,$attri,$valor)
{
    $nodes = $pai->children();
    while ($node = array_shift($nodes))
    {
    if ($node->type==XML_ELEMENT_NODE)
    {
     if ($node->tagname==$elem)
     {
      $A=$node->get_attribute($attri);
      if ($A==$valor)
      {
      return 1;
      }
     }
    }
    }
    return 0;
}

/***********************************************************************/
/* ESTE FILTRO ACHA TODOS OS TOPICOS DO ARQUIVO XML		       */
/* FUNCAO RECURSIVA PARA GERAR VETORES COMO:			       */
/* CAMINHAMENTO DO SITE, A NAVEGACAO ENTRE IRMAOS E PAIS	       */
/* MENU DE NAVEGACAO, TANTO NO MODO LIVRE E TUTORIAL		       */
/***********************************************************************/

function GeraCaminho($no)

{       global
	
$string_achados,$vetor_achados_sem_link	,
	$chave,
	$num_curso,
	$ac_achados,
	$vetor_achados;


 
	// LOOP PRINCIPAL DA RECURSAO

        for ($x=0; $x<sizeof($no); $x++)                 //for1
        {
                if ($no[$x]->type == XML_ELEMENT_NODE)   //if1
                {
                if (($no[$x]->tagname)=="topico")        //if2
                {
                        $elemento="curso";
                        $atributo="identcurso";

			// FILTRO POR CURSO
                        $resultado=achar_curso($no[$x],$elemento,$atributo,$num_curso);
                        if ($resultado==1)
                        {
				//ARMAZENAMENTO DOS DADOS DO XML EM VARIAVEIS UTILIZADAS NESTA FUNCAO
                	        $no[$x]->tagname;
                        	$topico_num  = $no[$x]->get_attribute("numtop");
	                        $topico_link = $no[$x]->get_attribute("arquivoxml");
	                        $topico_link = utf8_decode($topico_link);
	                        $topico_descricao  = $no[$x]->get_attribute("desctop");
	                        $topico_descricao  = utf8_decode($topico_descricao);
        	                $topico_abreviacao = $no[$x]->get_attribute("abreviacao");
	                        $topico_descricao_encodado = urlencode ($topico_descricao);
				$palavra_chave    =  $no[$x]->get_attribute("palchave");
				$palavra_chave	  =  utf8_decode($palavra_chave);
				//$palavra_chave    =  str_replace(",", "", $palavra_chave);   


				$palavra_chave_separado = $palavra_chave;
				$palavra_chave_separado = explode(",", $palavra_chave_separado);
				$origem_DR = $_GET['acao_DR'];
				for($i_DR=0; ($i_DR<count($palavra_chave_separado));($i_DR++))
				{
					//echo "chave $i_DR: ".$palavra_chave_separado[$i_DR]."<br />";
					
					$vetor_achados[$ac_achados] =  "<a href=\"./ad-livre-conceito.php?arq_xml=$topico_link&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Tag_Direta_vindo_da_$origem_DR&cor_fundo_DR=$cor_bg\" target=\"_top\"><font color=blue>".$palavra_chave_separado[$i_DR]."</fonte></a>";
										
					$string_achados = $string_achados. $palavra_chave_separado[$i_DR]." ";
					$vetor_achados_sem_link [$ac_achados] =  $palavra_chave_separado[$i_DR];
					
					$ac_achados++; 
				}


				
				{ 	
					//$vetor_achados[$ac_achados] =  "<font size=\"2\" face=\"Arial, Helvetica, sans-serif\" color=black>&nbsp;&nbsp;&nbsp;&nbsp; � <a href=\"./conceito.php?arq_xml=$topico_link&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado\" target=\"_top\"><font color=blue>".$palavra_chave."</fonte></a><br>";
										
					//$ac_achados++; 
				}
			}			
			// CHAMADAS RECURSIVAS PARA PERCORRER O ARQUIVO XML
	                $dummy = getChildren($no[$x]);
	                GeraCaminho($dummy);

                }  //if2

                }  //if1

            }  	   //for1
		

} 	    //fim laco principal da funcao


   $ac_achados = 0;
   $dom = xmldocfile($file);
   $root=$dom->root();
   $children = $root->children();
   GeraCaminho($children);

// Troca de idioma
 if ($A_LANG_IDIOMA_USER == "")
    include "../idioma/".$A_LANG_IDIOMA."/geral.php";
 else   
   include "../idioma/".$A_LANG_IDIOMA_USER."/geral.php";   

?>

<!DOCTYPE html>
<html>
<head>
<title>	
	<? echo A_LANG_SEARCH_RESULTS; ?>
</title>
<style type="text/css">
  
	.tabela_td{
	height:-10px;
	}
	
  .div_tag {
	margin-top:15px;
    margin-bottom:15px;

   	margin-left:10px;
    margin-right:10px;
    

	width:316px;
 }	
	
  .fundo_categoria {
	margin-top:10px;
    margin-bottom:10px;
    padding-left:2px;  
    padding-right:2px;  
    padding-bottom:6px;  
	background-color:#c3c3c3;  
	border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    min-width:300px;
 }
   .fundo_branco {
    margin-bottom:0px;
    padding-left:0px;  
    padding-right:0px;  
    padding-bottom:0px;  
	background-color:#ffffff;  
	/* border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px; */
    min-width:300px;
	font-family:arial,helvetica; 
	font-size:11px;
 }
 
    .fundo_branco_redondo {
    margin-bottom:0px;
	margin-left:5px;
	margin-right:5px;
    padding-left:5px;  
    padding-right:5px;  
    padding-bottom:0px;  
	background-color:#eeeeee;  
	border-radius:3px;-moz-border-radius:3px; -webkit-border-radius:3px;
    height:22px;
	max-width:300px;
 }
 
   .menu_dr {
    margin-bottom:5px;
	padding-top:1px;  
    padding-left:2px;  
    padding-right:2px;  
    padding-bottom:6px;  
	background-color:#7bb0dc;  
	border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    min-width:300px;
 }
 
    .menu_dr_titulo {
    margin-bottom:0px;
    padding-left:0px;  
    padding-right:0px;  
    padding-bottom:0px;  
	background-color:#7bb0dc;  
	border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    width:200px;
 }
 
   .fundo_busca {
    margin-bottom:5px;
    padding-left:2px;  
    padding-right:2px;  
    padding-bottom:6px;  
	background-color:#c3c3c3;  
	border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    min-width:300px;
 }
 
    .fundo_tags {
    margin-bottom:5px;
    padding-left:2px;  
    padding-right:2px;  
    padding-bottom:6px;  
	background-color:#c3c3c3;  
	border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    min-width:300px;
 }

.tabela_redonda {


    -webkit-border-radius: .5em; 
    -moz-border-radius: .5em;
    border-radius: .5em;
    border: solid 1px #bbbbbb;
    
}

 a:link {text-decoration: none}
  a:hover {text-decoration: underline;}
  body a:visited {text-decoration: none;}
 </style>
 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script> 
 <link rel="stylesheet" href="../javascript/colorbox.css" />
<script src="../javascript/jquery.colorbox.js"></script> 


</head>


<body bgcolor="#<?php echo $cor_bg; ?>" text="#000000" vlink="#0000FF" link="#0000FF" alink="#FF0000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">


<br>
<table border=0  align="left" class="tabela_redonda" cellpading=0 cellspacing=0>
<tr>
<td>	
<center>

<div class="div_tag">
<?php 
	   					$acao_DR = $_GET['acao_DR01'];
						$num_disc = $_GET['num_disc'];
						$num_curso = $_GET['num_curso'];
						$categoria = $_GET['categoria'];
						$topico_DR = $_GET['topico_DR'];

	   echo "| ";
		//echo $string_achados;
	   $string_achados_mais1 = "";
	   for ($ct_x = 0; $ct_x < $ac_achados; $ct_x ++)
	   {
		   		$Qt_string = substr_count($string_achados, $vetor_achados_sem_link [$ct_x]); //Qtds de tags
				$tam_font = ($Qt_string)+1;
				if ($tam_font > 4)
				{
					$tam_font = 4;
				}
				
				if ($Qt_string >1)
				{
					$Ja_imprimiu = substr_count($string_achados_mais1, $vetor_achados_sem_link [$ct_x]); 
					if ($Ja_imprimiu == 1)
					{
						//echo $vetor_achados_sem_link [$ct_x];
					}
					else
					{
						//$acao_DR_aux = $acao_DR."_Chave=".$vetor_achados_sem_link[$ct_x];		
						echo "<font face=arial size = $tam_font color=\"red\">";
						echo "&#32;";	
						//echo "<a href=\"ad-livre-busca_xml.php?chave=$vetor_achados_sem_link[$ct_x]&cor=$cor_padrao&acao_DR=$acao_DR&num_disc=$num_disc&num_curso=$num_curso&categoria=$categoria&topico_DR=$topico_DR&cor_fundo_DR=$cor_bg\" class=\"iframe\" >";
						echo "<a href=\"ad-livre-busca_xml.php?chave=$vetor_achados_sem_link[$ct_x]&cor=$cor_padrao&acao_DR=$acao_DR&num_disc=$num_disc&num_curso=$num_curso&categoria=$categoria&topico_DR=$topico_DR&cor_fundo_DR=$cor_bg\"  onclick='parent.$.colorbox({href: this.href, iframe:true, width:\"85%\", height:\"670px\"}); return false;' >";
						
						echo $vetor_achados_sem_link[$ct_x];
						echo "</a>";
						echo "</font>";
						echo "&#32; |";						
						$string_achados_mais1 = $string_achados_mais1.$vetor_achados_sem_link [$ct_x]." ";
					}
				}
				else
				{
					echo "<font face=arial size = $tam_font>";
					echo "&#32;";	
					echo $vetor_achados[$ct_x];
					echo "</font>";
					echo "&#32; |";
				}
				
				//$Ja_imprimiu = substr_count($string_achados_mais1, $vetor_achados_sem_link [$ct_x]); 
				
				
					if ($ct_x == 25)
					{
						$ct_x = $ac_achados - 1;
					}

				// echo $vetor_achados_sem_link [$ct_x];
	   }
	   			//	$string_achados = $palavra_chave_separado[$i_DR]." ";
				//	$vetor_achados_sem_link [$ac_achados] =  $palavra_chave_separado[$i_DR];
?>

</center>
</div>
</td>
</tr>
</table>
</body>
</html>






