<link href="../javascript/tiptip/tipTip.css" rel="stylesheet">
<script src="../javascript/tiptip/jquery.tipTip.js"></script>
<script src="../javascript/tiptip/jquery.tipTip.minified.js"></script>
<script>

</script>
   

    <script type="text/javascript">

      var $j = jQuery.noConflict();   
      $j(document).ready(function(){ 

      var altura_window = $j(window).height();
      var container = document.getElementById ("altura");
      var altura_total = container.offsetHeight;

      //alert (altura_window);
      //alert (altura_total);
    
      $j(window).scroll(function(){
        if ($j(this).scrollTop() > 100) {
          $j('.scrollup').fadeIn();
        } else {
          $j('.scrollup').fadeOut();
        }

        if ($j(this).scrollTop() + altura_window >=  (altura_total - 100) ) 
        {
                    $j('.menu').fadeOut(); // hide
        } else {

                    $j('.menu').fadeIn(); // appear
        }
       
      }); 
      
      $j('.scrollup').click(function(){
        $j("html, body").animate({ scrollTop: 0 }, 500);
        return false;
      });
 
      $j(".iframe").colorbox({iframe:true, width:"85%", height:"670px"});


      $j(".tooltip").tipTip({maxWidth: "auto", defaultPosition:"right",  edgeOffset: 10, delay: 10, fadeIn:10, fadeOut:10});
      $j(".tooltip_bottom").tipTip({maxWidth: "auto", defaultPosition:"bottom",  edgeOffset: 10, delay: 10});

    });
    </script>