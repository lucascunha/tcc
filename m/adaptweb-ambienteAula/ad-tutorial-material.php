<?php
//Dom XML
if (PHP_VERSION>='5')
  require_once('../include/domxml-xml.php');


$naveg = "tutorial";
$_SESSION['naveg']  = "tutorial";

global  	$id_aluno,
        	$tipo,
        	$logado,
        	$num_disc,
        	$num_curso,
	        $id_prof,
	        $disciplina,
	        $caminho,
		$naveg,
		$achou_material;

$parametro1 = $_GET['parametro1'];
$parametro2 = $_GET['parametro2'];
$parametro3 = $_GET['parametro3'];
$arq_xml    = $_GET['arq_xml'];
$tecno      = $_GET['tecno'];

if (!($arq_xml))
{
  $parametro1 = $_POST['parametro1'];
  $parametro2 = $_POST['parametro2'];
  $parametro3 = $_POST['parametro3'];
  $arq_xml    = $_POST['arq_xml'];
  $tecno      = $_POST['tecno'];
}

session_start();
$logado         = $_SESSION['logado'];
$id_usuario     = $_SESSION['id_usuario'];
$email_usuario  = $_SESSION['email_usuario'];
$A_LANG_IDIOMA_USER=$_SESSION['A_LANG_IDIOMA_USER'];
$tipo_usuario   = $_SESSION['tipo_usuario'];
$status_usuario = $_SESSION['status_usuario'];
$id_aluno       = $_SESSION['id_aluno'];
$tipo           = $_SESSION['tipo'];

$num_disc       = $_SESSION['num_disc'];   
$num_curso      = $_SESSION['num_curso'];  
$curso          = $_SESSION['curso'];      
$id_prof        = $_SESSION['id_prof'];    
$caminho        = $_SESSION['caminho'];    
$disciplina     = $_SESSION['disciplina']; 
$naveg          = $_SESSION['naveg'];   

// Troca de idioma - idioma de configura��o padr�o ou idioma selecionado pelo usu�rio no momento do cadastro
 if ($A_LANG_IDIOMA_USER == "")
    include "../idioma/".$A_LANG_IDIOMA."/geral.php";
 else   
    include "../idioma/".$A_LANG_IDIOMA_USER."/geral.php";  

/***********************************************************************/
/* AREA DE CONFIGURACOES DE CORES                                */
/***********************************************************************/

$cor_padrao  = "cdb058"            ;
$cor_fonte   = "000000"            ;
$titulo_menu = A_LANG_TOPIC_COMPLEMENTARY  ;


/***********************************************************************/
/* SETAR CAMINHO ARQUIVO XML                                            */
/***********************************************************************/
  
  require_once ("../config/configuracoes.php"); 
  include('../include/adodb/adodb.inc.php');  
  $file =$DOCUMENT_ROOT.$caminho."/estrutura_topico.xml";


/***********************************************************************/
/* CONEXAO COM O BANCO DE DADOS                                        */
/***********************************************************************/

  $usuario=$A_DB_USER;
  $senha=$A_DB_PASS;
  $nomebd=$A_DB_DB;
 
  $array=array();
  include "../include/conecta.php";


/***********************************************************************/
/* INSERCAO DA NAVEGACAO NO BANCO DE DADOS         		       */
/***********************************************************************/

	$insert ="insert into log_usuario (id_usuario, id_disc, id_curso, tecnologia, modo_naveg, topico, data_acesso, hora_acesso, menu) ";
	$insert.="values ($id_aluno, $num_disc, $num_curso, ' ', '$naveg', '$parametro2', current_date, current_time, 'material')";

        if (!mysql_query($insert,$id)){
	$erro=mysql_error();
	echo $erro;	
	}


/***********************************************************************/
/* COR FUNDO                                                           */
/***********************************************************************/

    $cor_fundo_DR = "ffffff";

/******************** Menu Esquerdo Principal **************************/

$nome_arquivo     = "ad-tutorial-material.php";

$nome_link1       = A_LANG_TOPIC;
$link1            = "./ad-tutorial-conceito.php?arq_xml=$arq_xml&parametro1=$parametro1&parametro2=$parametro2&parametro3=$parametro3&acao_DR=Clique_Menu_Categoria_emMaterial";
$nome_link2       = A_LANG_TOPIC_EXEMPLES;
$link2            = "./ad-tutorial-exemplo.php?arq_xml=$arq_xml&parametro1=$parametro1&parametro2=$parametro2&parametro3=$parametro3&acao_DR=Clique_Menu_Categoria_emMaterial";
$nome_link3       = A_LANG_TOPIC_EXERCISES;
$link3            = "./ad-tutorial-exercicio.php?arq_xml=$arq_xml&parametro1=$parametro1&parametro2=$parametro2&parametro3=$parametro3&acao_DR=Clique_Menu_Categoria_emMaterial";
$nome_link4       = A_LANG_TOPIC_COMPLEMENTARY;
$link4            = "./ad-tutorial-material.php?arq_xml=$arq_xml&parametro1=$parametro1&parametro2=$parametro2&parametro3=$parametro3&acao_DR=Clique_Menu_Categoria_emMaterial";

/*********************** Outros Links (Direito) ************************/

$nome_link5       = A_LANG_SHOW_MENU;
$link5            = "#";
$nome_link6       = A_LANG_CONFIG;
$link6            = "ad-tutorial-configurator.php?cor=$cor_padrao&acao_DR=Clique_Config_emMaterial&num_disc=$num_disc&num_curso=$num_curso&categoria=Material&naveg=$naveg&cor_fundo_DR=$cor_fundo_DR";
$nome_link7       = A_LANG_MAP;
$link7            = "ad-tutorial-mapa_xml.php?cor=$cor_padrao&localizacao=material&parametro2=$parametro2&acao_DR=Clique_Mapa_emMaterial&num_disc=$num_disc&num_curso=$num_curso&categoria=Material&cor_fundo_DR=$cor_fundo_DR";
$nome_link8       = A_LANG_MNU_EXIT;
$link8            = "Javascript:window.close();"; //Tinha antes --> "p_faz_sair.php"; Daniel mudou
$nome_link9       = A_LANG_HELP;
$link9            = "ad-tutorial-ajuda.php?cor=$cor_padrao&acao_DR=Clique_Ajuda_emMaterial&num_disc=$num_disc&num_curso=$num_curso&categoria=Material&cor_fundo_DR=$cor_fundo_DR";


/***********************************************************************/
/* FUNCAO UTILIZADO EM TODAS AS FUNCOES DO PROGRAMA                    */
/* ELA CAPTURA OS FILHOS DA ARVORE XML				       */
/***********************************************************************/

function getChildren($node)
{
        $temp = $node->children();
        $collection = array();
        $count=0;
        for ($x=0; $x<sizeof($temp); $x++)
        {
                if ($temp[$x]->type == XML_ELEMENT_NODE)
                        {
                             $collection[$count] = $temp[$x];
                         $count++;
                        }
        }
        return $collection;
}

/***********************************************************************/
/* FUNCAO PARA RETIRAR OS TOPICOS QUE NAO SAO DO CURSO		       */
/***********************************************************************/

function achar_curso($pai,$elem,$attri,$valor)
{
    $nodes = $pai->children();
    while ($node = array_shift($nodes))
    {
    if ($node->type==XML_ELEMENT_NODE)
    {
     if ($node->tagname==$elem)
     {
      $A=$node->get_attribute($attri);
      if ($A==$valor)
      {
      return 1;
      }
     }
    }
    }
    return 0;
}


/***********************************************************************/
/* FUNCAO PARA VERIFICAR SE EXISTE EXEMPLO OU MATCOMPLEMENTAR	       */
/***********************************************************************/

function achar_elemento($pai,$elem,$attri,$valor,$numero_topico)
{   global $ac_vt_material;

    $nodes = $pai->children();
    while ($node = array_shift($nodes))
    {
    if ($node->type==XML_ELEMENT_NODE)
    {
      if ($node->tagname=="curso")
      {
       $A=$node->get_attribute($attri);
        if ($A==$valor)
        { //if oo
            $filho_curso = getchildren($node);
            for ($y=0; $y<sizeof($filho_curso); $y++)
            {//for xy
               if ($filho_curso[$y]->type == XML_ELEMENT_NODE)
               {//if xx
                  if (($filho_curso[$y]->tagname)=="elementos")
                  {
                  $filho_elemento = getchildren($filho_curso[$y]);
                  for ($z=0; $z<sizeof($filho_elemento); $z++)
                  {//for xyxx
                     if ($filho_elemento[$z]->type == XML_ELEMENT_NODE)
                     {//if xxxx
                       if (($filho_elemento[$z]->tagname)=="matcomp")
                       {
                       $A=$filho_elemento[$z]->get_attribute("possuimatcomp");
                       if ($A=="sim")
                       {
                        return 1;
                       }
                       }
                     } //for xxxx
                  } //if xyxx
                  }
                } //for xx
             } //if xy
        }  //if oo
      }
    }
    }
    return 0;
}

/***********************************************************************/
/* FUNCAO RESPONSAVEL EM HABILITAR OU NAO AS GUIAS SUPERIORES	       */
/* DE ACORDO COM OS PARAMETROS PASSADOS				       */
/***********************************************************************/

function filtro_habilita_guia($pai,$elem,$attri,$valor,$numero_topico,$tag_busca,$tag_busca_atr)
{   global $ac_vt_material;

    $nodes = $pai->children();
    while ($node = array_shift($nodes))
    {
    if ($node->type==XML_ELEMENT_NODE)
    {
      if ($node->tagname=="curso")
      {
       $A=$node->get_attribute($attri);
        if ($A==$valor)
        { //if oo
            $filho_curso = getchildren($node);
            for ($y=0; $y<sizeof($filho_curso); $y++)
            {//for xy
               if ($filho_curso[$y]->type == XML_ELEMENT_NODE)
               {//if xx
                  if (($filho_curso[$y]->tagname)=="elementos")
                  {
                  $filho_elemento = getchildren($filho_curso[$y]);
                  for ($z=0; $z<sizeof($filho_elemento); $z++)
                  {//for xyxx
                     if ($filho_elemento[$z]->type == XML_ELEMENT_NODE)
                     {//if xxxx
                       if (($filho_elemento[$z]->tagname)==$tag_busca)
                       {
                       $A=$filho_elemento[$z]->get_attribute($tag_busca_atr);
                       if ($A=="sim")
                       {
                        return 1;
                       }
                       }
                     } //for xxxx
                  } //if xyxx
                  }
                } //for xx
             } //if xy
        }  //if oo
      }
    }
    }
    return 0;
}

/***********************************************************************/
/* FUNCAO QUE CRIA UM VETOR CONTENDO VALORES DE PREREQUISITOS	       */
/* APENAS ACESSADO NO MODO TUTORIAL				       */
/***********************************************************************/

function filtro_prerequisitos($pai,$numero_topico)
{   global $ac_vt_prereq, $vetor_prereq;

    $nodes = $pai->children();
    while ($node = array_shift($nodes))
    {
    if ($node->type==XML_ELEMENT_NODE)
    {
      if ($node->tagname=="prereq")   //verifica a tag prereq
      {
       	$vetor_prereq[$ac_vt_prereq] = $node -> get_attribute("identprereq");
	//$vetor_prereq = $node -> get_attribute("identprereq");
       	//echo $numero_topico."=".$ac_vt_prereq."===>".$vetor_prereq[$ac_vt_prereq]."<br>";
       	$ac_vt_prereq ++;
      }
    }
    }
    return 0;
}


/***********************************************************************/
/* ESTE FILTRO ACHA TODOS OS TOPICOS DO ARQUIVO XML		       */
/* FUNCAO RECURSIVA PARA GERAR VETORES COMO:			       */
/* CAMINHAMENTO DO SITE, A NAVEGACAO ENTRE IRMAOS E PAIS	       */
/* MENU DE NAVEGACAO, TANTO NO MODO LIVRE E TUTORIAL		       */
/***********************************************************************/

function GeraCaminho($no, $para1, $para2, $quebra)

{        global 	// VARIAVEIS GERAIS
			$id_aluno,
	        	$tipo,
	        	$logado,
	        	$num_disc,
	        	$num_curso,
	        	$id_prof,
	        	$disciplina,
        		$caminho,
			$naveg, 
			$id,
			$achou_material,
            $nome_arquivo, 

	                // VARIAVEIS PARA GERACAO DO CAMINHAMENTO
	               	$vetor,
        	       	$vetor_caminho,
	               	$ac_vetor_caminho,
	               	$cont,
	               	$topico_num_tratado,
        	       	$topico_descricao,
	               	$para2_tratado,
	               	$topico_num,
	               	$topico_link,
	               	$topico_visitado,
	               	$topico_abreviacao,
	               	$topico_descricao_encodado,
	               	$arquivo_xml,
	               	$guia_exemplo,
	               	$guia_exercicio,
	               	$guia_material,
	               	$parametro3,
	
			// VARIAVEIS PARA PREREQUISITOS
			$ac_vt_prereq,
			$vetor_prereq,
	
	                // VARIAVEIS PARA GERACAO DO MENU
	               	$vetor_topico,
        	       	$ac_vt_topico,
	               	$ct_x,
	               	$identacao,
	               	$flag,
	
	                // VARIAVEIS PARA GERACAO DA NAVEGACAO ENTRE PAIS/FILHOS
	               	$link_pai,
	               	$link_filho,
	               	$link_irmao_ant,
	               	$link_irmao_pos,
	               	$num_curso;



	// LOOP PRINCIPAL DA RECURSAO

        for ($x=0; $x<sizeof($no); $x++)                 //for1
        {
                if ($no[$x]->type == XML_ELEMENT_NODE)   //if1
                {
                if (($no[$x]->tagname)=="topico")        //if2
                {
                        $elemento="curso";
                        $atributo="identcurso";

			// FILTRO POR CURSO
                        $resultado=achar_curso($no[$x],$elemento,$atributo,$num_curso);
                        if ($resultado==1)
                        {

			// FILTRO POR MATERIAL
	
                        $achou_material=achar_elemento($no[$x],"curso","identcurso",$num_curso,$topico_num);

			//ARMAZENAMENTO DOS DADOS DO XML EM VARIAVEIS UTILIZADAS NESTA FUNCAO
                        $no[$x]->tagname;
                        $topico_num  = $no[$x]->get_attribute("numtop");
                        $topico_link = $no[$x]->get_attribute("arquivoxml");
                        $topico_link = utf8_decode($topico_link);
                        $pos 	     = strpos($topico_link,".");
                        $ext1 	     = substr($topico_link,$pos+1);
                        $qtde 	     = strlen($topico_link)-(strlen($ext1)+1);
                        $arq_x 	     = substr($topico_link,0,$qtde);
                        $ext2 	     = ".xml";
                        $arquivo_xml = $arq_x.$ext2;
                        $topico_descricao  = $no[$x]->get_attribute("desctop");
                        $topico_descricao  = utf8_decode($topico_descricao);
                        $topico_abreviacao = $no[$x]->get_attribute("abreviacao");
                        $topico_descricao_encodado = urlencode ($topico_descricao);

			// CHAMADAS PARA VERIFICAR QUAIS GUIAS HABILITAR

                        if ($parametro3 == $topico_descricao)
                        {
                        	$guia_exemplo  =filtro_habilita_guia($no[$x],"curso","identcurso",$num_curso,$topico_num,"exemplo","possuiexemp");
                        	$guia_exercicio=filtro_habilita_guia($no[$x],"curso","identcurso",$num_curso,$topico_num,"exercicio","possuiexerc");
                        }

                  if ((strlen($topico_descricao)) > 55)
                  {
                      $topico_descricao_aux = substr($topico_descricao, 0, 53). " ...";
                  }  
                  else
                  {
                      $topico_descricao_aux = $topico_descricao; 
                  } 



      // INICIO DO BLOCO QUE CRIA VETOR PARA EXIBICAO DO MENU IDENTADO

                        $comprimento = strlen ($topico_num);
                        if ($comprimento == 2) 
                            $comprimento = 1;
                        $identacao = "";

			// <FOR> PARA MONTAR O COMPRIMENTO DA IDENTACAO
                        for ($xx = 0; $xx < $comprimento; $xx ++)
                          {
				if ($xx ==  ($comprimento - 1))
				{
					$identacao = $identacao."<font color=\"gray\"> � </font>";
				}
				else
				{
                           		$identacao = $identacao."<img src = ../imagens/menu/blank.gif>";
				}
                          }

			{
                        	// CASO O PARAMETRO2 = TOPICO ATUAL, IMPRIME O MENU NA COR VERMELHA
		                if ($para2 == $topico_num)                   
                 	        {
                         		$vetor_topico[$ac_vt_topico] =$identacao."<font color=red><tooltip class='tooltip' title='$topico_descricao'>".$topico_descricao_aux."</tooltip></font>";
		                        $ac_vt_topico ++;
	                        }
                        	else
                        	if ($achou_material==1)
                        	{
	        			$select="select id_usuario from log_usuario where modo_naveg='$naveg' and id_usuario=$id_aluno and id_curso=$num_curso and id_disc=$num_disc and topico='$topico_num' and menu='material'";
					
          mysql_query($select);
          $result_select=mysql_affected_rows();
          if ($result_select == 0)
          {
                              $vetor_topico[$ac_vt_topico] =$identacao."<a class=blue_DR href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Menu_Lateral_emMaterial><font color=blue><tooltip class='tooltip' title='$topico_descricao'>".$topico_descricao_aux."</tooltip></font></a>";
                              $ac_vt_topico ++;
          }
          else
          {
                              $vetor_topico[$ac_vt_topico] =$identacao."<a class=purple_DR href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Menu_Lateral_emMaterial><font color=purple><tooltip class='tooltip' title='$topico_descricao'>".$topico_descricao_aux."</tooltip></font></a>";
                              $ac_vt_topico ++;
          }
                          }
                          else
                          {
                             $vetor_topico[$ac_vt_topico] =$identacao."<font color=gray><tooltip class='tooltip' title='$topico_descricao'>".$topico_descricao_aux."</tooltip></font></a>";
                             $ac_vt_topico ++;
                          }
      }

			// ESTA PARTE SERVIRA PARA GERAR OS TOPICOS QUE SAO OS IRMAOS E PAIS
			// ESTAS 2 VARIAVEIS RECEBEM OS TOPICOS SEM OS PONTOS VINDOS DOS TOPICOS XML, PARA GERAR PROVAVEIS IRMAOS E PAIS
                        $topico_num_tratado = str_replace(".", "", $topico_num );		// Numero atual *********//
                        $para2_tratado      = str_replace(".", "", $para2 );     		// Parametro 2  *********//

			// MONTA O VETOR DE TOPICOS NUMERICOS (AKI SOH PASSA 1 VEZ)
                        if ($quebra == 0)                                        		
                        {
				// PEGA AS SUBSTRINGS POR EX: 2 22 223 2231 etc
                           	for ($cont=1 ; $cont <= strlen($para2_tratado); $cont++)
                        	{
                           		$vetor[$cont] = substr ($para2_tratado, 0, $cont);
					$vetor_pai_filho[$vetor[$cont]] = 1;
                        	}
                           	$quebra = 1;
                              	$cont   = 1;
                        }
			
			// CHAMADAS RECURSIVAS PARA PERCORRER O ARQUIVO XML
                        $dummy = getChildren($no[$x]);
                        GeraCaminho($dummy, $para1, $para2, $quebra);
                        }
                        else
                        {
                         $no[$x]->unlink_node();
                        }

                }  //if2

                }  //if1

                        // GERA O VETOR DO CAMINHAMENTO (O CAMINHAMENTO EH ARMAZENADO NUM VETOR)

                        if ($cont <= strlen($para2_tratado))
                        {
                            if ($topico_num_tratado == $vetor[$cont])
                            {
                              if ($topico_num_tratado != $para2_tratado)             // Se for diferente imprime azul senao preto
                              {
                                  if ($achou_material==1)
                                  {
                                      $vetor_caminho[$ac_vetor_caminho] = "<font size=2 face=arial  color=black><b>"." � "."</b></font>"."<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Caminhamento_emMaterial><font size=2 face=arial  color=blue>".$topico_descricao."</font></a>";
                                      $ac_vetor_caminho ++;
                                  }
                                  else
                                  {
                                      $vetor_caminho[$ac_vetor_caminho] = "<font size=2 face=arial  color=black><b>"." � "."</b></font>"."<font size=2 face=arial  color=gray>".$topico_descricao."</font></a>";
                                      $ac_vetor_caminho ++;
                                  }
                              }
                              else
                              {
                                    $vetor_caminho[$ac_vetor_caminho] = "<font size=2 face=arial  color=black><b>"." � "."</b></font>"."<font size=2 face=arial  color=black><b>".$topico_descricao."</b></font>";
                                    $ac_vetor_caminho ++;
                              }
                                  $cont++;
                            }
                         }

      // GERA OS LINKS DE NAVEGACAO POR SETAS - ATRIBUI PARA AS VARIAVEIS LINK_PAI, FILHO, IRMAO_ANT E IRMAO_POS O LINK PARA NAVEGAR
                        if ($topico_num_tratado == substr ($para2_tratado, 0, strlen($para2_tratado)-1) && (strlen ($para2_tratado) > 1) && ($achou_material==1))
                        {
                          $link_pai = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emMaterial><img src=../imagens/seta_c_icon.png border=0 width=20 class=\"tooltip\" title=\"$topico_descricao\" ></a><div id=\"t1\" style=\"display:none;\">$topico_descricao</div>";
                        }
                        if (($topico_num_tratado == ($para2_tratado + 1)) && ($achou_material==1))
                        {
                          $link_irmao_pos = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emMaterial><img src=../imagens/seta_d_icon.png border=0 width=25 class=\"tooltip\" title=\"$topico_descricao\" ></a><div id=\"t2\" style=\"display:none;\">$topico_descricao</div>";
                        }
                        if (($topico_num_tratado == ($para2_tratado - 1)) && ($achou_material==1))
                        {
                                $link_irmao_ant = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emMaterial><img src=../imagens/seta_e_icon.png border=0 width=25 class=\"tooltip\" title=\"$topico_descricao\" ></a><div id=\"t3\" style=\"display:none;\">$topico_descricao</div>";
                        }
                        if (($topico_num_tratado == ($para2_tratado."1")) && ($achou_material==1))
                        {
                          $link_filho = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emMaterial><img src=../imagens/seta_b_icon.png border=0 width=20 alt=\"$topico_descricao\" class=\"tooltip\" title=\"$topico_descricao\" ></a><div id=\"t4\" style=\"display:none;\">$topico_descricao</div>";
                                $flag = 1;
                        }
                        if ($flag <> 1)
                        {
                          if (($topico_num_tratado == ($para2_tratado."2")) && ($achou_material==1))
                          {
                                  $link_filho = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emMaterial><img src=../imagens/seta_b_icon.png border=0 width=20 alt=\"$topico_descricao\"  class=\"tooltip\" title=\"$topico_descricao\" ></a><div id=\"t5\" style=\"display:none;\">$topico_descricao</div>";
                                  $flag = 2;
                          }
                        }
                        if ($flag > 1)
                        {
                          if (($topico_num_tratado == ($para2_tratado."3")) && ($achou_material==1))
                          {
                                  $link_filho = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emMaterial><img src=../imagens/seta_b_icon.png border=0 width=20 alt=\"$topico_descricao\"  class=\"tooltip\" title=\"$topico_descricao\" ></a><div id=\"t6\" style=\"display:none;\">$topico_descricao</div>";
                                  $flag = 3;
                          }
                        }
                        if ($flag > 2)
                        {
                          if (($topico_num_tratado == ($para2_tratado."4")) && ($achou_material==1))
                          {
                                  $link_filho = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emMaterial><img src=../imagens/seta_b_icon.png border=0 width=20 alt=\"$topico_descricao\" class=\"tooltip\" title=\"$topico_descricao\" ></a><div id=\"t7\" style=\"display:none;\">$topico_descricao</div>";
                                  $flag = 4;
                          }
                        }
                        if ($flag > 3)
                        {
                          if (($topico_num_tratado == ($para2_tratado."5")) && ($achou_material==1))
                          {
                                  $link_filho = "<a href=./".$nome_arquivo."?arq_xml=$arquivo_xml&parametro1=$topico_link&parametro2=$topico_num&parametro3=$topico_descricao_encodado&acao_DR=Clique_Navegacao_Direta_emMaterial><img src=../imagens/seta_b_icon.png border=0 width=20 alt=\"$topico_descricao\"  class=\"tooltip\" title=\"$topico_descricao\" ></a><div id=\"t8\" style=\"display:none;\">$topico_descricao</div>";
                                  $flag = 5;
                          }
                        }

            }     //for1
      if ($link_pai == NULL)
      {$link_pai = "<img src=../imagens/seta_c_icon-g.png border=0 width=20>";
      }
      if ($link_filho == NULL)
      {$link_filho = "<img src=../imagens/seta_b_icon-g.png border=0  width=20>";
      }
      if ($link_irmao_ant == NULL)
      {$link_irmao_ant ="<img src=../imagens/seta_e_icon-g.png border=0 width=25>";
      }
      if ($link_irmao_pos == NULL)
      {$link_irmao_pos = "<img src=../imagens/seta_d_icon-g.png border=0 width=25>";
      }
}                   //laco pricipal da funcao


   $link_pai =       NULL;
   $link_filho =     NULL;
   $link_irmao_ant = NULL;
   $link_irmao_pos = NULL;

   $ac_vt_prereq = 0;
   $ac_vt_material = 0;
   $ac_vt_topico = 0;
   $ac_vetor_caminho = 0;
   $quebra = 0;
   $dom = xmldocfile($file);
   $root=$dom->root();
   $children = $root->children();
   GeraCaminho($children, $parametro1, $parametro2, $quebra);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<META http-equiv=Content-Type content="text/html; charset=ISO-8859-1">
<title>	
	<?php
		echo strtoupper($disciplina)." - ".($parametro3);
	?>
</title>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script> 
<link rel="stylesheet" href="../javascript/colorbox.css" />
<script src="../javascript/jquery.colorbox.js"></script> 
<?php
include ("../javascript/menu.js");
?>
<link rel="stylesheet" href="ad-inc-css.css" />
<style type="text/css">
   .menu_dr 
   {
    margin-bottom:5px;
    padding-left:2px;  
    padding-right:2px;  
    padding-bottom:6px;  
    background-color:#<?php echo $cor_padrao; ?>;  
    border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;
    }
</style>
    
<?php
include ("funcoes_jquery.php");
?>
</head>

<body bgcolor="#<?php echo $cor_fundo_DR; ?>" text="#000000" vlink="#0000FF" link="#0000FF" alink="#FF0000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" id="altura">
<table width="100%" border="0" cellpadding="1" cellspacing="0">
  <tr >
    <td valign="top" bgcolor="#eeeeee" width="100%"> 
    </td>
  </tr>
</table>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr valign="middle" style="height:90px;">
    <td width="70%" bgcolor="#FFFFFF">
      <table border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            &nbsp;
            <img src="../imagens/loguinho.png" bordder=0>     
          </td>
          <td >
            &nbsp;            
            <b>
              <font face="Arial, Helvetica, sans-serif" size="6">
                <!-- *******************************************************-->
                <!-- ********** Aqui eh impresso o nome da disciplina ******-->
                <!-- *******************************************************-->
                <?php
                  if ((strlen($disciplina)) > 36)
                  {
                      $disciplina_aux = substr($disciplina, 0, 35). " ...";
                  }  
                  else
                  {
                      $disciplina_aux = $disciplina; 
                  }                 
                            echo $disciplina_aux;
                ?>
              </font>
            </b>
          </td>
          <td >
              &nbsp;&nbsp;    
              <img src="../imagens/loguinho-fim.png" border=0>    
          </td>
      </table>          
    </td>   
    
     <td>
    
    <table cellspacing=2 cellpadding=5 bgcolor=#ffffff style="border-radius:5px;-moz-border-radius:5px; -webkit-border-radius:5px;">
    <tr  valign=baseline>
    <td width="143" >  
      <div align="center"><a class='iframe' href="<? echo $link7; ?>" >
      <img src="../imagens/sitemap.png" width="25" border=0></a></div>    
    </td> 
    <td width="143">
      <div align="center"><a class='iframe' href="<? echo $link6; ?>">
      <img src="../imagens/config.png" width="25" border=0></a></div>
    </td>   
    <td width="93">
      <div align="center">      
      <align="right"><a class='iframe' href="<? echo $link9; ?>">
      <img src="../imagens/help.png" width="25" border=0></a></div>
    </td>   
    <td width="93" >
      <div align="center"><a onClick="chama_log('material.tutorial.php', 'Clique_Link_Sair', '<? echo $num_disc;?>', '<? echo $num_curso;?>', '<? echo $naveg;?>', 'Material', '<? echo $parametro2;?>');" href="<? echo $link8; ?>">
      <img src="../imagens/sign-out.png" width="25" border=0></a></div>
    </td>
    </tr>


    <tr  valign=baseline>
    <td width="143"  >
      <div align="center"><a class='iframe' href="<? echo $link7; ?>" ><font face="Arial, Helvetica, sans-serif" size="2"><? echo $nome_link7; ?></font></a></div>      
    </td> 
    <td width="143" >
      <div align="center"><a class='iframe' href="<? echo $link6; ?>"><font face="Arial, Helvetica, sans-serif" size="2"><? echo $nome_link6; ?></font></a></div>
    </td>   
    <td width="93" >
      <div align="center">      
      <align="right"><a class='iframe' href="<? echo $link9; ?>"><font face="Arial, Helvetica, sans-serif" size="2"><? echo $nome_link9; ?></font></a></div>
    </td>   
    <td width="93" >
       <div align="center"><a onClick="chama_log('ad-tutorial-material.php', 'Clique_Link_Sair', '<? echo $num_disc;?>', '<? echo $num_curso;?>', '<? echo $naveg;?>', 'Material', '<? echo $parametro2;?>');" href="<? echo $link8; ?>"><font face="Arial, Helvetica, sans-serif" size="2"><b><? echo $nome_link8; ?></b></font></a></div>
    </td>  
    </tr>
      </table>
      
    </td>
  </tr>   
</table>


<table width="100%" border="0" cellpadding="5" cellspacing="1">
  <tr >
    <td valign="top" bgcolor=#<? echo $cor_padrao; ?> width="100%" style="border-radius:3px;-moz-border-radius:3px; -webkit-border-radius:3px;"> 
      <div align="center">

        <?php

  {
         echo "<table cellpadding=0 cellspacing=0 border=0 >";

         echo "<tr height=30><td><center>$link_pai</center></td><td><td>$link_irmao_ant</td><td>";
         ?>
         <font color=<?php echo $cor_fonte; ?> face="Arial, Helvetica, sans-serif" size="4">
         <div class = "fundo_branco_redondo">
        
          <?php
                echo ($parametro3); ?>
        
         </div>
         </font>
         <?php
         echo "</td><td>$link_irmao_pos</td><td><center>$link_filho</center></td></tr>";
         echo "</table>";
  }
        ?>


      </div>
    </td>
  </tr>
</table>


<table width="100%" border="0" cellpadding="5" cellspacing="1">
  <tr height=26>
    <td height="22" bgcolor="#eeeeee" width="100%" style="border-radius:3px;-moz-border-radius:3px; -webkit-border-radius:3px; padding-left:5px;" valign=middle>
      <div align="left">
      <font face="Arial, Helvetica, sans-serif" size="1" color="#0000FF">

        <?php
        
        /***********************************************************************/
        /********* Aki eh impresso o caminhamento  *****************************/
        /***********************************************************************/
        // Loop para percorrer o vetor e imprimir o caminhamento
        
           for ($ct_x = 1; $ct_x < $ac_vetor_caminho; $ct_x ++)
           {
               echo $vetor_caminho[$ct_x];
           }
        
        ?>

      </font>
      </div>
    </td>
  </tr>

</table>

<table width=100%>
<tr>
  <td width="350" valign="top"> 
  <div class="fundo_categoria">
  <div class="fundo_categoria_titulo">
  <table>
  <tr>
    <td>
    Categorias
    </td>
  </tr>
  </table>  
  </div>
  
  <div class="fundo_branco">
  <table cellpadding=0 cellspacing=1>
  <tr>
  <td >
  <div style="background-color:#ffffff;">
&nbsp;<img src = "../imagens/01-sem-seta.png" width="49">
  </div>
  </td>
  <td>
  <a href="<? echo $link1; ?>"><font size="2" face="Arial, Helvetica, sans-serif" ><? echo $nome_link1; ?></font></a>
  </td>
  </tr>

  <tr>
  <td>
  <div style="background-color:#ffffff;">
&nbsp;<img src = "../imagens/02-sem-seta.png" width="49">
  </div>
  </td>
  <td>
      <?php
        if ($guia_exemplo == 1)
        {
      ?>
      <font size="2" face="Arial, Helvetica, sans-serif"><a href="<? echo $link2; ?>"><? echo $nome_link2; ?></a></font>
      <?php
        }
        else
        {
      ?>
      <font size="2" face="Arial, Helvetica, sans-serif" color="gray"><? echo $nome_link2; ?></font>
      <?php
        }
      ?>
      
  </td>
  </tr>

  <tr>
  <td>
  <div style="background-color:#ffffff;">
&nbsp;<img src = "../imagens/03-sem-seta.png" width="49">
  </div>
  </td>
  <td>
      <?php
        if ($guia_exercicio == 1)
        {
      ?>
      <font size="2" face="Arial, Helvetica, sans-serif"><a href="<? echo $link3; ?>"><? echo $nome_link3; ?></a></font>
      <?php
        }
        else
        {
      ?>
      <font size="2" face="Arial, Helvetica, sans-serif" color="gray"><? echo $nome_link3; ?></font>
      <?php
        }
      ?>
  </td>
  </tr>

  <tr>
  <td>
  <div style="background-color:#ffffff;">
  &nbsp;<img src = "../imagens/04-seta.png" width="49">
  </div>
  </td>
  <td>
      <font size="2" face="Arial, Helvetica, sans-serif" color="red"><? echo $nome_link4; ?></font>
  </td>
  </tr>
  </table>
  </div>

  </div>  
<!-- Conteudo do menu -->

        <!-- *******************************************************-->
        <!-- ********** For para impressao do Menu Identado ********-->
        <!-- *******************************************************-->
<div class = "menu_dr">	
<div class = "fundo_categoria_titulo ">
<table>
<tr>
	<td>
	<img src = "../imagens/material_icone.png" width="20"> 
	</td>
	<td>
    Material Complementar
	</td>
	</tr>
</table>	
  </div>
<div class="fundo_branco"> 
    <table width=100% >
        <?php        
          for ($ct_x = 0; $ct_x < $ac_vt_topico; $ct_x ++)
          {   
            echo "<tr onmouseover=\"this.className='cell_over';\" onmouseout=\"this.className='cell_out';\" bgcolor=\"#ffffff\"><td>";
            echo "&nbsp;&nbsp;".$vetor_topico[$ct_x]."<br>";
            echo "</td></tr>";
          }
        
        ?>
    </table>
    <br>
  </div>
</div>
<!-- fim - Conteudo do menu-->
</td>


<td valign="top">

<script>
  var tamanho_tela = screen.height -250 ;
  document.write('<table width=\"100%\"  border=\"0\" align=\"center\" style=\"margin-top:10px;height:'+tamanho_tela+'px;\" cellpadding=\"25\" cellspacing=\"0\">')

</script>
  <tr>
    <td valign="top">  

         <?php

           global $tecno;

           include "ad-lib-material.php";

           $file2 =$DOCUMENT_ROOT.$caminho."/".$arq_xml;

           $dom2=xmldocfile($file2);
           $root2=$dom2->root();
           $children2=$root2->children();
           echo "<table cellspacing=0 cellpadding=0 border=0 width=\"100%\" style=\"margin-left:20px;\">";
           echo "<tr><td>";
           echo "  <font color=\"#000000\" face=\"Arial, Times New Roman\" size=4><strong>";
           echo    A_LANG_COMPLEMENTARY_LIST." para ".$parametro3;
           echo "  </strong></font>";
           echo "</td></tr>";
           echo "<tr><td><br><br>";
           acha_material($children2,$num_curso,$arq_xml,$caminho);
           echo "</td></tr></table>";
          ?>



   </td>
  </tr>
</table>
        <?php
          $menu_down_material= TRUE;
          include ('ad-inc-links-rodape.php');
        ?>
</td>
</tr>
</table>


<table width="100%" border="0" align="center" cellpadding="10" cellspacing="1">

  <tr>
    <td valign="button" bgcolor=#<? echo $cor_padrao; ?> width="100%" style="border-radius:3px;-moz-border-radius:3px; -webkit-border-radius:3px;">
      <div align="right">
      <font size="1" face="Arial, Helvetica, sans-serif"><? echo " &nbsp; &nbsp;".A_LANG_SYSTEM_COPYRIGHT."";?> </font>
      </div>
    </td>
  </tr>
</table> 


<div align='left'>
<img src="../imagens/rodape_logo.png" border=0>
<br><br>
</div>

<a href="#" class="scrollup">Para Cima</a>

</body>
</html>



<!-- Isabela Gasparini -->
<!-- Parametros passados para a correta execucao (por enqunto)
     ****** Parametro1 = Nome do link
     ****** Parametro2 = Numero do topico. Exemplo 2.2.1, 3.2, etc
     ****** Parametro3 = Descricao da disciplina

Tipos de variaveis:

Variaveis com ct na frente eh um contador. Ex: ct_x
Variaveis com ac na frente eh um acumulador. Ex: ac_vetor_caminho
-->
