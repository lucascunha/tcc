<? 
/* -----------------------------------------------------------------------
*  AdaptWeb - Projeto de Pesquisa       
*     UFRGS - Instituto de Inform�tica  
*       UEL - Departamento de Computa��o
* -----------------------------------------------------------------------
*       @package AdaptWeb
*     @subpakage Linguagem
*          @file idioma/pt-BR/geral.php
*    @desciption Arquivo de tradu��o - Portugu�s/Brasil
*         @since 17/08/2003
*        @author Veronice de Freitas (veronice@jr.eti.br)
*   @Translation Veronice de Freitas
* -----------------------------------------------------------------------         
*/  
 
       // ************************************************************************ // * Interface - ????                                                                     *
  // ************************************************************************
//Dados da interface da ferramenta de autoria
  define("A_LANG_VERSION","Vers�o"); 
  define("A_LANG_SYSTEM","Ambiente AdaptWeb"); 
  define("A_LANG_ATHORING","Ferramenta de Autoria"); 
  define("A_LANG_USER","Usu�rio");  
  define("A_LANG_PASS","Senha");  
  define("A_LANG_NOTAUTH","< n�o autenticado>"); 
  define("A_LANG_HELP","Ajuda"); 
  define("A_LANG_DISCIPLINE","Disciplina");  
define("A_LANG_NAVEGATION","Onde estou: ");   

// Contexto
// observa��o - arquivo p_contexto_navegacao.php
       // ************************************************************************ // * Menu                                                                                 *
  // ************************************************************************
// Home
define("A_LANG_MNU_HOME","In�cio"); 
define("A_LANG_MNU_NAVIGATION","Ambiente Aluno");
define("A_LANG_MNU_AUTHORING","Ambiente Professor"); 
define("A_LANG_MNU_UPDATE_USER","Alterar Cadastro");
define("A_LANG_MNU_EXIT","Sair"); 
define("A_LANG_MNU_LOGIN","Login"); 
define("A_LANG_MNU_ABOUT","Sobre");
define("A_LANG_MNU_DEMO","Demonstra��o");
define("A_LANG_MNU_PROJECT","Projeto");
define("A_LANG_MNU_FAQ","Perguntas Freq�entes"); 
define("A_LANG_MNU_RELEASE_AUTHORING","Liberar Autoria"); 
define("A_LANG_MNU_BACKUP","C�pia de Seguran�a"); 
define("A_LANG_MNU_NEW_USER","Novo Usu�rio"); 
define("A_LANG_MNU_APRESENTATION","Apresenta��o");
define("A_LANG_MNU_MY_ACCOUNT","Minha Conta");
define("A_LANG_MNU_MANAGE","Gerenciamento"); 
define("A_LANG_MNU_BAKCUP","C�pia de Seguran�a");
define("A_LANG_MNU_GRAPH","Acesso");
define("A_LANG_MNU_TODO","Em desenvolvimento"); 
define("A_LANG_SYSTEM_NAME","AdaptWeb - Ambiente de Ensino-Aprendizagem Adaptativo na Web ");  
  define("A_LANG_SYSTEM_COPYRIGHT","Copyright &copy; Licensed under the Academic Free License version 1.2");   

        // Autoria        
        define("A_LANG_MNU_COURSE","Curso");  
        define("A_LANG_MNU_DISCIPLINES","Disciplina");         
        define("A_LANG_MNU_DISCIPLINES_COURSE","Disciplina/Curso");                        
        define("A_LANG_MNU_STRUTURALIZE_TOPICS","Estruturar Conte�do"); 
        define("A_LANG_MNU_ACCESS_LIBERATE","Analisar Pedidos de Matr�cula");
        define("A_LANG_MNU_LIBERATE_DISCIPLINES","Liberar Disciplina");
        define("A_LANG_MNU_WORKS","Trabalhos dos alunos");                 

// Estudante
define("A_LANG_MNU_DISCIPLINES_RELEASED","Assistir Disciplinas"); define("A_LANG_MNU_SUBSCRIBE","Solicitar Matr�cula"); define("A_LANG_MNU_WAIT","Aguardando Matr�cula"); 
define("A_LANG_MNU_UPLOAD_FILES","Enviar Trabalhos"); define("A_LANG_MNU_MESSAGE","Avisos"); 
// ************************************************************************ // * Formul�rio Principal                                                                          *
  // ************************************************************************ 
  // Apresenta��es
define("A_LANG_ORGANS","Institui��es Participantes");
define("A_LANG_RESEARCHES","Pesquisadores");
define("A_LANG_AUTHORS","Autores"); 
  
 
  // ************************************************************************ // * Formul�rio - Autoria                                                                     *
  // ************************************************************************
// Apresenta��o
define("A_LANG_MNU_ORGANS",""); 
define("A_LANG_MNU_RESEARCHES",""); 
define("A_LANG_MNU_AUTHORS",""); 

        // Formul�rio de solicita��o de acesso
        define("A_LANG_REQUEST_ACCESS","Solicita��o de Acesso");
        define("A_LANG_REQUEST_ACCESS_USER_TYPE","Tipo de usu�rio");
        define("A_LANG_REQUEST_ACCESS_TYPE_STUDANT","Aluno");
        define("A_LANG_REQUEST_ACCESS_TYPE_TEACHES","Professor");                
        define("A_LANG_REQUEST_ACCESS_NAME","Nome");
        define("A_LANG_REQUEST_ACCESS_EMAIL","E-mail");
        define("A_LANG_REQUEST_ACCESS_PASS","Senha");          
        define("A_LANG_REQUEST_ACCESS_PASS2","Confirma��o da Senha");     
        define("A_LANG_REQUEST_ACCESS_INSTITUTION_EDUCATION","Institui��o de Ensino");     
        define("A_LANG_REQUEST_ACCESS_COMMENT","Observa��o");     
        define("A_LANG_REQUEST_ACCESS_LANGUAGE","Idioma");               
        define("A_LANG_REQUEST_ACCESS_MSG1","N�o foi poss�vel estabelecer conex�o com o banco de dados");     
define("A_LANG_REQUEST_ACCESS_MSG2","N�o foi poss�vel enviar seus dados. (informe outro e-mail)");     
        define("A_LANG_REQUEST_ACCESS_MSG3","N�o foi poss�vel enviar seus dados");     
        define("A_LANG_REQUEST_ACCESS_MSG4","Senha inv�lida (Informe a mesma senha nos campos SENHA e CONFIRMA��O DA SENHA)");     
        define("A_LANG_REQUEST_ACCESS_SAVE","Gravar");  


        //Formulario de login
        define("A_LANG_LOGIN2","Login");
        define("A_LANG_EMAIL","E-mail");
define("A_LANG_LOGIN_PASS","Senha");      
define("A_LANG_LOGIN_START","Entrar");      
define("A_LANG_LOGIN_MSG1","Senha inv�lida");       
        define("A_LANG_LOGIN_MSG2","Email inv�lido");       
        define("A_LANG_LOGIN_MSG3","Usu�rio n�o autorizado");       
        define("A_LANG_LOGIN_MSG4","Aguarde a libera��o de seu acesso pelo administrador do ambiente AdaptWeb");       
        define("A_LANG_LOGIN_MSG5","N�o foi poss�vel estabelecer conex�o com o banco de dados");       
                

        // Formul�rio de cadastro de Curso
        define("A_LANG_COURSE_REGISTER","Cadastro de Curso");       
        define("A_LANG_COURSE_RESGISTERED","Cursos cadastrados");  
        define("A_LANG_COURSE2","Curso");                      
        define("A_LANG_COURSE_INSERT","Incluir");
        define("A_LANG_COURSE_UPDATE","Alterar");
        define("A_LANG_COURSE_DELETE","Excluir");  
        define("A_LANG_COURSE_MSG1","N�o foi poss�vel estabelecer conex�o com o banco de dados");  
define("A_LANG_COURSE_MSG2","N�o foi poss�vel inserir o curso");  
define("A_LANG_COURSE_MSG3","N�o foi poss�vel alterar o curso"); 
define("A_LANG_COURSE_MSG4","N�o � poss�vel excluir o curso, porque o mesmo esta relacionado com alguma disciplina. Para remover o curso dever� entrar em disciplina curso tirar da sele��o de todas as disciplinas o curso que deseja excluir."); 
define("A_LANG_COURSE_MSG5","N�o foi poss�vel excluir o curso"); 
 

        // Cadastro de disciplina
        define("A_LANG_DISCIPLINES_REGISTER","Cadastro de Disciplina");  
        define("A_LANG_DISCIPLINES_REGISTERED","Disciplinas cadastradas"); 
        define("A_LANG_DISCIPLINES2","Disciplina");    
        define("A_LANG_DISCIPLINES_INSERT","Incluir");
        define("A_LANG_DISCIPLINES_UPDATE","Alterar");
        define("A_LANG_DISCIPLINES_DELETE","Excluir");      
        define("A_LANG_DISCIPLINES_MSG1","N�o foi poss�vel inserir a disciplina"); 
        define("A_LANG_DISCIPLINES_MSG2","N�o foi poss�vel alterar a disciplina"); 
        define("A_LANG_DISCIPLINES_MSG3","N�o � poss�vel excluir a disciplina, porque a mesma esta relacionada com algum curso. Para remover a disciplina dever� entrar em disciplina curso tirar da sele��o todos os cursos que est�o relacionados com a disciplina."); 
        define("A_LANG_DISCIPLINES_MSG4","N�o foi poss�vel excluir a disciplina"); 
        

        // Formul�rio de cadastro de Disciplina / Curso
        define("A_LANG_DISCIPLINES_COURSE1","Disciplina/Curso");
        define("A_LANG_DISCIPLINES_COURSE_DISCIPLINES","Disciplinas");   
        define("A_LANG_DISCIPLINES_COURSE2","Cursos");      
        define("A_LANG_DISCIPLINES_COURSE_SAVE","Gravar");
        define("A_LANG_DISCIPLINES_COURSE_MSG1","Cadastre a(s) DISCIPLINA(S) e CURSO(S) para ter acesso a este item.");
        define("A_LANG_DISCIPLINES_COURSE_MSG2","N�o foi poss�vel estabelecer v�nculo da disciplina com os cursos devido a falha de conex�o com o banco de dados.");
        define("A_LANG_DISCIPLINES_COURSE_MSG3","Informe o nome do curso");
        define("A_LANG_DISCIPLINES_COURSE_MSG4","Curso existente");
        define("A_LANG_DISCIPLINES_COURSE_MSG5","Selecione um curso para altera��o");
                
// Formul�rio de autoriza��o de acesso
define("A_LANG_LIBERATE_USER1","Matr�cula / Aluno");   // para o professor e administrador
        define("A_LANG_LIBERATE_USER2","Autorizar professor / Autoria"); // para o adiministrador
        define("A_LANG_LIBERATE_MSG1","Em desenvolvimento ... ");
        
        
        // Formul�rio para libera��o da disciplina 
define("A_LANG_LIBERATE_DISCIPLINES","Liberar Disciplina");
        define("A_LANG_LIBERATE_DISCIPLINES_MSG1","Em desenvolvimento ... ");
                                                          
        // ======================= Topicos ==========================
                        
        // Entrada / t�picos
        define("A_LANG_ENTRANCE_TOPICS_DISCIPLINES","Disciplina");
        // **** REVER "Estruturar t�pico"
        define("A_LANG_ENTRANCE_TOPICS_STRUTURALIZE","Estruturar T�picos");
        define("A_LANG_ENTRANCE_TOPICS_MSG1","N�o foi poss�vel ler as disciplinas");
        define("A_LANG_ENTRANCE_TOPICS_MSG2","* Somente estar�o dispon�veis neste formul�rio as disciplinas que estiverem relacionadas com pelo menos um curso.");
        

       // Guia - Lista de t�picos
        define("A_LANG_TOPIC_GENERATE_CONTENT","Gerar Conte�do");       
        //orelha
        define("A_LANG_TOPIC_LIST","Conceitos");     
        define("A_LANG_TOPIC_MAINTENANCE","Manuten��o do Conceito");  
        define("A_LANG_TOPIC_EXEMPLES","Exemplos");  
        define("A_LANG_TOPIC_EXERCISES","Exerc�cios");  
        define("A_LANG_TOPIC_COMPLEMENTARY","Material Complementar");          
        
        // Guia - manuten��o do t�pico
  define("A_LANG_TOPIC_NUMBER","N�mero do Conceito");
  define("A_LANG_TOPIC_DESCRIPTION","Nome do Conceito");
  define("A_LANG_TOPIC_SUMARIZED_TOPIC_DESCRIPTION","Descri��o resumida do conceito");  
  define("A_LANG_TOPIC_WORDS_KEY","Palavras-Chave");   
  define("A_LANG_TOPIC_PREREQUISIT","Pr�-requisito");
  define("A_LANG_TOPIC_COURSE","Curso");
define("A_LANG_TOPIC_FILE1","Arquivo");
define("A_LANG_TOPIC_FILE2","Arquivo(s)");
define("A_LANG_TOPIC_MAIN_FILE","Arquivo principal"); 
define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE","Arquivo(s) associado(s)");
define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_STATUS","Situa��o");
define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_UPLOAD",""); 
        define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG1","carregar para o servidor"); 
define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG2","carregado para o servidor"); 
// bot�es
define("A_LANG_TOPIC_SAVE","Gravar");  
        define("A_LANG_TOPIC_SEND","Enviar"); 
        define("A_LANG_TOPIC_SEARCH","Procurar");         
define("A_LANG_TOPIC_INSERT_TOPIC_SAME_LEVEL","Inserir conceito no mesmo n�vel");  
        define("A_LANG_TOPIC_INSERT_SUB_TOPIC","Inserir subconceito");  
define("A_LANG_TOPIC_DELETE","Excluir");  
define("A_LANG_TOPIC_PROMOTE","Promover"); 
define("A_LANG_TOPIC_LOWER","Rebaixar"); 

// Guia - samples
//define("A_LANG_TOPIC_EXEMPLES_NUMBER_TOPIC","N�mero do T�pico");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Descri��o do exemplo");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","N�vel de Complexidade");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Curso");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Excluir");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Descri��o");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","N�vel");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Curso");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Arquivo");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Arquivo(s) associado(s)");
// bot�es
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Gravar");
//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Excluir");


  // Exemplos
  // N�vel de Complexidade
// Descri��o
// N�vel
// Arquivos Associados
        // Descri��o dos Exemplos
  // Exerc�cios
        // Descri��o do exerc�cio
  // Material complementar
        // Menu
// Autorizar Acesso
// Liberar disciplinas
// Novo usu�rio
        // to modify I register in cadastre (alterar cadastro)
        // ************************************************************************ // * Formul�rio - Autoria                                                                     *
  // ************************************************************************ // Disciplinas Liberadas

// Matricula

// Aguardando Matricula
                
        // Autorizar Acesso
        
        // Navega��o
               
        // Tela de Cursos    
        // ************************************************************************ // * Formul�rio - Demo
  // ************************************************************************
  define("A_LANG_DEMO","Disciplina para demonstra��o do Ambiente AdaptWeb - n�o permite inser��es e altera��es");
//
// Regional Specific Date texts
//
// A little help for date manipulation, from PHP manual on function strftime():
//
// %a - abbreviated weekday name according to the current locale
// %A - full weekday name according to the current locale
// %b - abbreviated month name according to the current locale
// %B - full month name according to the current locale
// %c - preferred date and time representation for the current locale
// %C - century number (the year divided by 100 and truncated to an integer, range 00 to 99)
// %d - day of the month as a decimal number (range 01 to 31)
// %D - same as %m/%d/%y
// %e - day of the month as a decimal number, a single digit is preceded by a space
//      (range ' 1' to '31')
// %h - same as %b
// %H - hour as a decimal number using a 24-hour clock (range 00 to 23)
// %I - hour as a decimal number using a 12-hour clock (range 01 to 12)
// %j - day of the year as a decimal number (range 001 to 366)
// %m - month as a decimal number (range 01 to 12)
// %M - minute as a decimal number
// %n - newline character
// %p - either `am' or `pm' according to the given time value, or the corresponding strings for
//      the current locale
// %r - time in a.m. and p.m. notation
// %R - time in 24 hour notation
// %S - second as a decimal number
// %t - tab character
// %T - current time, equal to %H:%M:%S
// %u - weekday as a decimal number [1,7], with 1 representing Monday
// %U - week number of the current year as a decimal number, starting with the first Sunday as
//      the first day of the first week
// %V - The ISO 8601:1988 week number of the current year as a decimal number, range 01 to 53,
//      where week 1 is the first week that has at least 4 days in the current year, and with
//      Monday as the first day of the week.
// %W - week number of the current year as a decimal number, starting with the first Monday as
//      the first day of the first week
// %w - day of the week as a decimal, Sunday being 0
// %x - preferred date representation for the current locale without the time
// %X - preferred time representation for the current locale without the date
// %y - year as a decimal number without a century (range 00 to 99)
// %Y - year as a decimal number including the century
// %Z - time zone or name or abbreviation
// %% - a literal `%' character
//
// Note: A_LANG_DATESTRING is used for Articles and Comments Date
//       A_LANG_LINKSDATESTRING is used for Web Links creation Date
//       A_LANG_DATESTRING2 is used for Older Articles box on Home
//
  
define("A_LANG_CODE","pt-BR");
define("A_LANG_CHACTERSET","ISO-8859-1");

define("A_LANG_NAME_pt_BR","Portugu�s do Brasil");  
define("A_LANG_NAME_en_US","Ingl�s");  
define("A_LANG_NAME_es_ES","Espanhol");  
define("A_LANG_NAME_fr_FR","Franc�s");  
define("A_LANG_NAME_ja_JP","Japon�s");

define('A_LANG_TIMEZONES','IDLW NT HST YST PST MST CST EST AST GMT-3:30 GMT-3 AT WAT GMT CET EET BT GMT+3:30 GMT+4 GMT+4:30 GMT+5 GMT+5:30 GMT+6 WAST CCT JST ACS GST GMT+11 NZST'); 
define('A_LANG_TZOFFSETS','0 1 2 3 4 5 6 7 8 8.5 9 10 11 12 13 14 15 15.5 16 16.5 17 17.5 18 19 20 21 21.5 22 23 24'); 
define('A_LANG_DATEBRIEF','%b %d, %Y');
define('A_LANG_DATELONG','%A, %B %d, %Y'); 
define('A_LANG_DATESTRING','%A, %B %d @ %H:%M:%S %Z'); 

define('A_LANG_DATESTRING2','%A, %B %d');
define('A_LANG_DATETIMEBRIEF','%b %d, %Y - %I:%M %p');
define('A_LANG_DATETIMELONG','%A, %B %d, %Y - %I:%M %p %Z'); 

define('A_LANG_LINKSDATESTRING','%d-%b-%Y');
define('A_LANG_TIMEBRIEF','%I:%M %p');
define('A_LANG_TIMELONG','%I:%M %p %Z');

define('A_LANG_DAY_OF_WEEK_LONG','Domingo Segunda Ter�a Quarta Quinta Sexta S�bado');
define('A_LANG_DAY_OF_WEEK_SHORT','Dom Seg Ter Qua Qui Sex S�b');
define('A_LANG_MONTH_LONG','Janeiro Fevereiro Mar�o Abril Maio Junho Julho Agosto Setembro Outubro Novembro Dezembro');
define('A_LANG_MONTH_SHORT','Jan Fev Mar Abr Mai Jun Jul Ago Set Out Nov Dez');
  
//************UFRGS******************
	//Index
	  //About
	define('A_LANG_INDEX_ABOUT_INTRO',' O Ambiente AdaptWeb � voltado para  a autoria e apresenta��o adaptativa de disciplinas integrantes de cursos EAD na Web. O objetivo do AdaptWeb � permitir a adequa��o de t�ticas e formas de apresenta��o de conte�dos para alunos de diferentes cursos de gradua��o e com diferentes estilos de aprendizagem, possibilitando diferentes formas de apresenta��o de cada conte�do, de forma adequada a cada curso e �s prefer�ncias individuais dos alunos participantes.<br> O Ambiente AdaptWeb foi inicialmente desenvolvido por pesquisadores da UFRGS e da UEL, atrav�s dos projetos Electra e AdaptWeb, com apoio do CNPq. Visite a p�gina do projeto: <a href=\'http://www.inf.ufrgs.br/adapt/adaptweb\' target=\'_blank\'>http://www.inf.ufrgs.br/adapt/adaptweb</a>');
	define('A_LANG_INDEX_ABOUT_ENVACCESS','Acesso ao ambiente');
	define('A_LANG_INDEX_ABOUT_TINFO','para disponibilizar o conte�do de suas disciplinas o professor deve efetuar um cadastro de solicita��o de acesso no Ambiente de Autoria');
	define('A_LANG_INDEX_ABOUT_LINFO','para ter acesso a(s) disciplina(s) o aluno deve efetuar o cadastro no Ambiente de Navega��o e solicitar matricula na(s) disciplina(s) relacionadas ao seu curso');
	  //Demo
	define('A_LANG_INDEX_DEMO_AENVIRONMENT','Ambiente de Autoria');
	define('A_LANG_INDEX_DEMO_FDESC1', 'Criar disciplina(s)');
	define('A_LANG_INDEX_DEMO_NENVIRNMENT','Ambiente de navega��o');
	define('A_LANG_INDEX_DEMO_FDESC2', 'Assistir disciplina(s)');
	define('A_LANG_INDEX_DEMO_TOLOG','Acesse o ambiente e navegue pela disciplina de demonstra��o efetuando login no ambiente com os dados abaixo:');
	define('A_LANG_INDEX_DEMO_LOGIN','login: demo@inf.ufrgs.br');
	define('A_LANG_INDEX_DEMO_PASS', 'senha: 123');
	define('A_LANG_INDEX_DEMO_OBSERVATION','Observa��o');
	define('A_LANG_INDEX_DEMO_OBSERVATIONTEXT', 'ap�s efetuar o login ser� liberado acesso ao ambiente do professor e ao ambiente do aluno pelas op��es "Ambiente do professor" e "Ambiente o aluno" na tela inicial do AdaptWeb.');
	  //Apresenta��o
	define('A_LANG_INDEX_PRESENTATION_INFABOUT', 'Informa��es sobre o projeto:');
	define('A_LANG_DOWNLOAD', 'Download');
	define('A_LANG_PUBLICATIONS', 'Publica��es');
	define('A_LANG_INDEX_PRESENTATION_AVAILABLEAT','Dispon�vel em:');
	

	
	//Ambiente do Aluno - Entrada
	  //Principal
	define('A_LANG_LENVIRONMENT','Ambiente do aluno');
	define('A_LANG_LENVIRONMENT_DESCRIPTION', 'Este ambiente possibilite o aluno ter acesso ao material instrucional de seu(s) professor(es) adaptado ao seu perfil.');	
	define('A_LANG_LENVIRONMENT_WARNING', 'para ter acesso a(s) sua(s) disciplina, o aluno deve inicialmente eferuar seu cadastro para que seja poss�vel logar no ambiente para solicitar matricula de sua(s) disciplina(s). Somente ap�s a libera��o de acesso pelo professor respons�vel que o mesmo ter� acesso a disciplina.');
	
	define('A_LANG_LENVIRONMENT_WATCH_LIBERATED', 'Disciplinas liberadas');
	define('A_LANG_LENVIRONMENT_WATCH_MINE', 'Disciplinas de minha autoria (navegar por curso)');
	define('A_LANG_LENVIRONMENT_WATCH_MY', 'Minhas Disciplinas');
	define('A_LANG_LENVIRONMENT_WATCH_NOT', 'N�o h� disciplinas cadastradas');
	define('A_LANG_LENVIRONMENT_WATCH_WARNING', 'Solicite matr�culas para ter acesso �s disciplinas.');
	define('A_LANG_LENVIRONMENT_WATCH_VISUAL', 'Visualizar disciplina para cada curso');
	define('A_LANG_LENVIRONMENT_WATCH_CREATE', 'Crie as disciplinas e gere conte�do (Gerar Conte�do)no Ambiente do Professor (item <b>Estruturar Conte�do => Lista de Conceitos </b>) para ter acesso ao ambiente de navega��o.');
	define('A_LANG_LENVIRONMENT_WATCH_OBS', 'para visualizar as disciplinas de sua autoria n�o � necess�rio Liberar Disciplina para testar o ambiente de navega��o.');
	define('A_LANG_LENVIRONMENT_REQUEST', 'Solicitar Matr�cula');
	define('A_LANG_LENVIRONMENT_REQUEST_REQ', 'Solicitar');
	define('A_LANG_LENVIRONMENT_REQUEST_NOTC', 'N�o h� curso cadastrado');
	define('A_LANG_LENVIRONMENT_REQUEST_NOTD', 'N�o h� disciplina cadastrada para o curso selecionado');
	define('A_LANG_LENVIRONMENT_REQUEST_SELECT', 'Selecione para solicitar matr�cula:');
	define('A_LANG_LENVIRONMENT_REQUEST_MATR', 'Selecione para solicitar matr�cula:');
	define('A_LANG_LENVIRONMENT_WAITING', 'Aguardando Matr�cula');
	define('A_LANG_LENVIRONMENT_WAITING_NOT', 'N�o h� matr�culas cadastradas');
	define('A_LANG_LENVIRONMENT_WAITING_WARNING', 'Solicite matr�culas para ter acesso �s disciplinas:');
	define('A_LANG_LENVIRONMENT_NAVTYPE', 'Tipo de Navega��o');
	define('A_LANG_LENVIRONMENT_FORCOURSE', 'para o curso de');
	define('A_LANG_LENVIRONMENT_NETCONECTION', 'Conex�o de Rede');
	define('A_LANG_LENVIRONMENT_SPEED', 'A velocidade �');
	define('A_LANG_LENVIRONMENT_NAVIGATION', 'Navega��o');
	define('A_LANG_LENVIRONMENT_TUTORIAL', 'Tutorial');
	define('A_LANG_LENVIRONMENT_FREE', 'Livre');
	
	//Ambiente do professor
	define('A_LANG_TENVIRONMENT_DESCRIPTION', 'O m�dulo de autoria permite ao autor disponibilizar conte�dos adaptados a diferentes perfis de alunos. Atrav�s do processo de autoria, o autor poder� disponibilizar o conte�do de suas aulas em uma �nica estrutura adaptada para os diferentes cursos. Nesta etapa o autor deve cadastrar a disciplina e para quais cursos deseja disponibiliz�-la. Ap�s estes cadastramentos devem ser inseridos os arquivos de conceito do conte�do, relacionando-os com cada t�pico da disciplina. Al�m disto o autor deve informar a descri��o do t�pico, seu pr�-requisito e para quais cursos deseja disponibilizar o conte�do. O autor poder� inserir exerc�cios, exemplos e materiais adicionais para cada t�pico.');
	define('A_LANG_TENVIRONMENT_AUTHOR', 'Autoria');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR1', 'Ocorreu um erro ao matricular o usu�rio nesta disciplina. Este usu�rio j� deve estar matriculado ou ocorreu um erro no servidor');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR2', 'Ocorreu um erro ao excluir a matr�cula do usu�rio nesta disciplina. Este usu�rio n�o deve estar matriculado ou ocorreu um erro no servidor');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_NOT', 'N�o h� curso cadastrado');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECT', 'Selecione o curso');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_NOTD', 'N�o h� disciplina cadastrada para este curso');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECTD', 'Selecione a disciplina');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_LIST', 'Lista de Alunos:');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_MATR', 'Alunos Matriculados:');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_BACK', 'Voltar');
	define('A_LANG_TENVIRONMENT_LIBERATE_DESCRIPTION', '* somente estar�o discponiveis para serem liberadas as disciplinas que estiverem com conte�do correto. Para verificar o conte�do das disciplinas acesse a guia <b>CONCEITO</b> no item <b>ESTRUTURAR CONTE�DO</b> e use o bot�o <b>GERAR CONTEUDO</b>.');
	define('A_LANG_TENVIRONMENT_LIBERATE_LIB', 'Liberar<br> disciplina:');
	define('A_LANG_TENVIRONMENT_LIBERATE_LIB_DIS', 'Disciplinas<br> liberadas:');
	define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE', 'Mover Conceito');
	define('A_LANG_TENVIRONMENT_ESTRUCT_EXCLUDE', 'Excluir Conceito');
	define('A_LANG_TENVIRONMENT_ESTRUCT_SELECT', 'Selecione o conceito que deseja <B>MOVER</B> ou <B>EXCLUIR</B>');
	define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE_AFTER', '<B>MOVER</B> conceito selecionado ap�s qual conceito?');
	define('A_LANG_TENVIRONMENT_ESTRUCT_CANCEL', 'a opera��o de MOVER ou EXCLUIR conceito ir� acarretar em cancelamento dos pr�-requisitos');
	define('A_LANG_TENVIRONMENT_ESTRUCT_NOT_POSSIBLE', 'N�o foi poss�vel ler a matriz do Banco de Dados');	
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND', 'Legenda de pr�-requisitos:');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND1', 'Para selecionar v�rios pr�-requisitos use a tecla CTRL');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND2', 'Para retirar o(s) pr�-requisito(s) da sele��o use a tecla CTRL');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND3', 'Os pr�-requisitos em <font class=buttonselecionado>azul escuro</font> est�o selecionados');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND4', 'O fundo <font class=buttonpre>azul</font> indica os pr�-requisitos atuais (caso perder a sele��o)');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND5', 'O fundo <font class=buttonpreautomatico>amarelo</font> indica os pr�-requisitos atuais autom�ticos');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NUMBER', 'N�mero do conceito:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_DESCRIPTION', 'Nome do exemplo:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_LEVEL', 'N�vel de Complexidade:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NOCLASS', 'Sem classifica��o');
	define('A_LANG_TENVIRONMENT_EXAMPLES_EASY', 'F�cil');
	define('A_LANG_TENVIRONMENT_EXAMPLES_MEDIUM', 'M�dio');
	define('A_LANG_TENVIRONMENT_EXAMPLES_COMPLEX', 'Complexo');
	define('A_LANG_TENVIRONMENT_EXAMPLES_DSC', 'Descri��o');
	define('A_LANG_TENVIRONMENT_EXAMPLES_LVL', 'N�vel');
	define('A_LANG_TENVIRONMENT_EXERCISES_DESCRIPTION', 'Nome do exerc�cio:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_DESCRIPTION', 'Descri��o do Material <br> Complementar:');
	define('A_LANG_TENVIRONMENT_ACCESS', 'Acesso ao Ambiente de Autoria');
	define('A_LANG_TENVIRONMENT_WAIT', 'Aguarde o administrador do ambiente liberar acesso para o ambiente de autoria.');
	define('A_LANG_TENVIRONMENT_COMMENT', 'o acesso ao ambiente de autoria � gerenciado pelo administrador do ambiente AdaptWeb. Ap�s efetuar cadastro no ambiente <br> � necess�rio autoriza��o do administrador para acesso ao ambiente de autoria.');
	define('A_LANG_TENVIRONMENT_DEMO', 'Demonstra��o dos Ambientes');
	define('A_LANG_TENVIRONMENT_ASSOCIATED_FILES','Obs.: Arquivos associados devem ser enviados para o servidor atrav�s da se��o "manuten��o do conceito"');
	
	//Op��es de Root
	define('A_LANG_ROOT_NOT_AUTH', 'Professores<br> n�o autorizados:');
	define('A_LANG_ROOT_AUTH', 'Professores<br> autorizados:');
	define('A_LANG_ROOT_DEV', 'Esta op��o encontra-se em fase de desenvolvimento.');

	//Outros
	define('A_LANG_DATA', 'Data');
	define('A_LANG_WARNING', 'Aviso');
	define('A_LANG_COORDINATOR', 'COORDENADOR');
	define('A_LANG_DATABASE_PROBLEM', 'H� problemas com o Banco de Dados');
	define('A_LANG_FAQ', 'Perguntas Freq�entes');
	define('A_LANG_WORKS', 'Trabalhos');
	define('A_LANG_NOT_DISC', 'N�o foi poss�vel apresentar as disciplinas cadastradas');
	define('A_LANG_NO_ROOT', 'N�o foi poss�vel inserir o usu�rio Root');
	define('A_LANG_MIN_VERSION', 'A vers�o m�nima suportada, do banco de dados � [');
	define('A_LANG_INSTALLED_VERSION', '], e a vers�o do instalada � [');
	define('A_LANG_ERROR_BD', 'N�o foi poss�vel gravar a matriz no Banco de Dados');
	define('A_LANG_DISC_NOT_FOUND', 'Nome da Disciplina n�o encontrado ou em branco');
	define('A_LANG_SELECT_COURSE', 'selecione o(s) curso(s) no conceito <b>');
	define('A_LANG_TOPIC_REGISTER', 'Cadastro de Conceitos');
	define('A_LANG_SUMARIZED_DESCRIPTION', 'Descri��o resumida<br> do conceito');
	define('A_LANG_LOADED', 'carregada para o servidor');
	define('A_LANG_FILL_DESCRIPTION', 'Preencha o campo descri��o do material');
	define('A_LANG_WORKS_SEND_DATE', 'Data de envio');
	define('A_LANG_SEARCH_RESULTS', 'Resultados de Busca');
	define('A_LANG_SEARCH_SYSTEM', 'Sistema de Busca');
	define('A_LANG_SEARCH_WORD', 'Palavra buscada');
	define('A_LANG_SEARCH_FOUND', 'Foram encontradas ');
	define('A_LANG_SEARCH_OCCURRANCES', ' ocorr�ncias de ');
	define('A_LANG_SEARCH_IN_TOPIC', ' em CONCEITO');
	define('A_LANG_SEARCH_FOUND1', 'Foi encontrada ');
	define('A_LANG_SEARCH_OCCURRANCE', ' ocorr�ncia de ');
	define('A_LANG_SEARCH_NOT_FOUND', 'Nao foram encontradas ocorr�ncias de ');
	define('A_LANG_SEARCH_CLOSE', 'Fechar');
	define('A_LANG_TOPIC', 'Conceito');
	define('A_LANG_SHOW_MENU', 'Exibir Menu');
	define('A_LANG_CONFIG', 'Configura��es');
	define('A_LANG_MAP', 'Mapa');
	define('A_LANG_PRINT', 'Imprimir esta p�gina?');
	define('A_LANG_CONNECTION', 'N�o foi poss�vel estabelecer uma conex�o com o servidor de banco de dados MySQL. Favor contactar o administrador.');
	define('A_LANG_CONTACT', 'Favor contactar o administrador');
	define('A_LANG_HELP_SYSTEM', 'Sistema de Ajuda');
	define('A_LANG_CONFIG_SYSTEM', 'Configura��es do Sistema');
	define('A_LANG_CONFIG_COLOR', 'Configurar cor de fundo');
	define('A_LANG_NOT_CONNECTED', 'N�o conectou!');
	define('A_LANG_DELETE_CADASTRE', 'Exclus�o de Cadastro de Alunos');
	define('A_LANG_HOME', 'Home');
	define('A_LANG_YOU_HAVE', 'Voc� possui ');
	define('A_LANG_TO_CONFIRM', ' matr�cula(s). Para confirmar esta exclus�o clique em ');
	define('A_LANG_CONFIRM', ' CONFIRMAR');
	define('A_LANG_EXAMPLE_LIST', 'Lista de Exemplos');
	define('A_LANG_COMPLEXITY_LEVEL_EASY', 'N�vel de Complexidade F�cil');
	define('A_LANG_COMPLEXITY_LEVEL_MEDIUM', 'N�vel de Complexidade M�dio');
	define('A_LANG_COMPLEXITY_LEVEL_COMPLEX', 'N�vel de Complexidade Dif�cil');
	define('A_LANG_EXERCISES_LIST', 'Lista de Exerc�cios');
	define('A_LANG_REGISTER_MAINTENENCE', 'Manuten��o de Matr�culas');
	define('A_LANG_REGISTER_RELEASE', 'Libera��o de Matr�culas');
	define('A_LANG_SITE_MAP', 'Mapa do Site');
	define('A_LANG_COMPLEMENTARY_LIST', 'Lista de Material Complementar');
	define('A_LANG_DELETE_TOPIC', 'Exclu�do o conceito ');
	define('A_LANG_AND', ' e ');
	define('A_LANG_SONS', ' subconceito(s).');
		
	//Gera��o de conte�do
	//Alterado por Douglas
	define('A_LANG_GENERATION_END', 'Fim da geracao do arquivo xml de elementos do t�pico');
	define('A_LANG_GENERATION_CREATED', 'Foram criados ');
	define('A_LANG_GENERATION_XML', ' arquivos xml.');
	define('A_LANG_GENERATION_END_STRUCT', 'Fim da geracao do XML de Estrutura de Topicos');
/*	define('A_LANG_GENERATION_MISSING_FILE', 'Faltando inserir arquivo de conceito do ');
	define('A_LANG_GENERATION_TH_TOPIC', 'conceito ');
	define('A_LANG_GENERATION_MISSING_COURSE', 'Faltando inserir curso do ');
	define('A_LANG_GENERATION_NO_ASOC', 'Conceito sem curso associado');
	define('A_LANG_GENERATION_MISSING_EXERC_ID', 'Faltando inserir identificador de exercicio do ');
	define('A_LANG_GENERATION_TH_EXERCISE', '� exerc�cio do ');
	define('A_LANG_GENERATION_MISSING_EXERC_DESCRIPTION', 'Faltando inserir descri��o de exercicio do ');
	define('A_LANG_GENERATION_MISSING_EXERC_FILE', 'Faltando inserir arquivo de exercicio do ');
	define('A_LANG_GENERATION_MISSING_EXERC_COMPLEXITY', 'Faltando inserir complexidade de exercicio do ');
	define('A_LANG_GENERATION_TH_EXAMPLE', '� exemplo do ');
	define('A_LANG_GENERATION_MISSING_EXAMP_ID', 'Faltando inserir identificador de exemplo do ');
	define('A_LANG_GENERATION_MISSING_EXAMP_DESCRIPTION', 'Faltando inserir descricao de exemplo do ');
	define('A_LANG_GENERATION_MISSING_EXAMP_FILE', 'Faltando inserir arquivo de exemplo do ');
	define('A_LANG_GENERATION_MISSING_EXAMP_COMPLEXITY' ,'Faltando inserir complexidade de exemplo do ');
	define('A_LANG_GENERATION_TH_MATCOMP', '� material complementar do ');
	define('A_LANG_GENERATION_MISSING_MATCO_ID', 'Faltando inserir identificador de material complementar do ');
	define('A_LANG_GENERATION_MISSING_MATCO_DESCRIPTION', 'Faltando inserir descricao de material complementar do ');
	define('A_LANG_GENERATION_MISSING_MATCO_FILE', 'Faltando inserir arquivo de material complementar do ');
*/
	define('A_LANG_GENERATION_VERIFY', 'Verificacao dos dados da autoria');
	define('A_LANG_GENERATION_PROBLEMS', 'Problemas no cadastramento da Disciplina');
/*	define('A_LANG_GENERATION_MISSING_TOPIC_NUMBER', 'Faltando inserir Numero do Topico para o ');
	define('A_LANG_GENERATION_MISSING_TOPIC_DESCRIPTION', 'Faltando inserir Descri��o do Topico para o ');
	define('A_LANG_GENERATION_MISSING_TOPIC_ABREV', 'Faltando inserir Abrevia��o do Topico para o ');
	define('A_LANG_GENERATION_MISSING_TOPIC_KEYWORD', 'Faltando inserir Palavra Chave do Topico para o ');
*/
	define('A_LANG_GENERATION_RESULT_XML', 'Resultado da Gera��o do XML');
	define('A_LANG_GENERATION_NO_ERROR', 'NAO HA ERRO');
	define('A_LANG_GENERATION_SUCCESS', 'ARQUIVOS DO CURSO GERADOS COM SUCESSO');
	define('A_LANG_GENERATION_DATA_LACK', 'Arquivos XML nao foram gerados por falta de dados');
	define ('A_LANG_GENERATION_TOPIC','Conceito');	
	define ('A_LANG_GENERATION_EXAMPLE','Exemplo');	
	define ('A_LANG_GENERATION_EXERCISE','Exerc�cio');	
	define ('A_LANG_GENERATION_COMPLEMENTARY','Material Complementar');	
	define ('A_LANG_GENERATION_TOPIC_NUMBER','N�mero T�pico');	
	define ('A_LANG_GENERATION_TOPIC_NAME','Nome T�pico');	
	define ('A_LANG_GENERATION_DESCRIPTION','Descri��o');	
	define ('A_LANG_GENERATION_KEY_WORD','Palavra-chave');	
	define ('A_LANG_GENERATION_COURSE','Curso');	
	define ('A_LANG_GENERATION_PRINC_FILE','Arquivo Principal');	
	define ('A_LANG_GENERATION_ASSOC_FILE','Arquivos Associados');	
	define ('A_LANG_GENERATION_MISSING','Ausente');	
	define ('A_LANG_GENERATION_NO_PROBLEM','Ok');	
	define ('A_LANG_GENERATION_NUMBER','N�mero');	
	define ('A_LANG_GENERATION_DESCRIPTION','Descri��o');	
	define ('A_LANG_GENERATION_COMPLEXITY','N�vel');	
	define ('A_LANG_GENERATION_NOT_EXIST','N�o possui');	

?>