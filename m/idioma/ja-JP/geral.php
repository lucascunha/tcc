<? 
	/* -----------------------------------------------------------------------
	 *  AdaptWeb - Projeto de Pesquisa       
	 *     UFRGS - Instituto de Inform�tica  
	 *       UEL - Departamento de Computa��o
	 * -----------------------------------------------------------------------
	 *       @package AdaptWeb
	 *     @subpakage Linguagem
	 *          @file idioma/pt-BR/geral.php
	 *    @desciption Arquivo de tradu��o - Portugu�s/Brasil
	 *         @since 17/08/2003
	 *        @author Veronice de Freitas (veronice@jr.eti.br)
	 *   @Translation Veronice de Freitas
	 * -----------------------------------------------------------------------         
	 */  
 

       	// ****************************************************************************************
 	// * Interface - ????                                                                     *
 	// ****************************************************************************************

	//Dados da interface da ferramenta de autoria
  	define("A_LANG_VERSION","�_�C�A���O�i�r");	
  	define("A_LANG_SYSTEM",""); 	
  	define("A_LANG_ATHORING","Ferramenta de Autoria"); 	
  	define("A_LANG_USER","�z�[��");  	
  	define("A_LANG_PASS","Senha");  
  	define("A_LANG_NOTAUTH","< n�o autenticado >"); 
  	define("A_LANG_HELP","�O�i�"); 
  	define("A_LANG_DISCIPLINE","Disciplina");  
	define("A_LANG_NAVEGATION","Onde estou: ");	  	

	// context 	
	// observa��o - arquivo p_contexto_navegacao.php
	

       	// ****************************************************************************************
 	// * Menu                                                                                 *
 	// ****************************************************************************************	
 	
	// Home    
	define("A_LANG_MNU_HOME","�v���t�@�C�"); 
	define("A_LANG_MNU_NAVIGATION","�}�C�N���\�t�g");
	define("A_LANG_MNU_AUTHORING","�}�C�N���\�t�g");	
	define("A_LANG_MNU_UPDATE_USER","���x�Ȍ���");
	define("A_LANG_MNU_EXIT","�J�");	
	define("A_LANG_MNU_LOGIN","Login");		
	define("A_LANG_MNU_ABOUT","�@�l��");
	define("A_LANG_MNU_DEMO","�@�l�̂��q�l");
	define("A_LANG_MNU_PROJECT","�J����");
	define("A_LANG_MNU_FAQ","�Z�L�����e�B���");		
	define("A_LANG_MNU_RELEASE_AUTHORING","�T�|�[�g�I�����C��");	
	define("A_LANG_MNU_BACKUP","���i�J�^���O");	
	define("A_LANG_MNU_NEW_USER","Novo Usu�rio"); 	
	define("A_LANG_MNU_APRESENTATION","���[�U�[�ʃT�C�g");
	define("A_LANG_MNU_MY_ACCOUNT","�T�C�g�}�b�v");
	define("A_LANG_MNU_MANAGE","���i�J�^���O");	
	define("A_LANG_MNU_TODO","Em desenvolvimento");	
	define("A_LANG_SYSTEM_NAME","AdaptWeb - �q�����������w�Z�ŉ���׋����Ă��邩�����m�ł���");  
  	define("A_LANG_SYSTEM_COPYRIGHT","Copyright &copy; 2001-2003 UFRGS/UEL");   		
	
        // Authoring        
        define("A_LANG_MNU_COURSE","Curso");  
        define("A_LANG_MNU_DISCIPLINES","Disciplina");         
        define("A_LANG_MNU_DISCIPLINES_COURSE","Disciplina/Curso");                        
        define("A_LANG_MNU_STRUTURALIZE_TOPICS","Estruturar Conte�do"); 
        define("A_LANG_MNU_ACCESS_LIBERATE","Autorizar Acesso");
        define("A_LANG_MNU_LIBERATE_DISCIPLINES","Liberar Disciplina");
        define("A_LANG_MNU_WORKS","Trabalhos dos alunos");                 
			
	// Student
	define("A_LANG_MNU_DISCIPLINES_RELEASED","Assistir Disciplinas");					
	define("A_LANG_MNU_SUBSCRIBE","Solicitar Matr�cula");					
	define("A_LANG_MNU_WAIT","Aguardando Matr�cula");	
	define("A_LANG_MNU_UPLOAD_FILES","Enviar Trabalhos");							
	
 
 	// ****************************************************************************************
 	// * Form - Authoring                                                                     *
 	// ****************************************************************************************

	// Apresentacao
	define("A_LANG_MNU_ORGANS","");	
	define("A_LANG_MNU_RESEARCHES","");	
	define("A_LANG_MNU_AUTHORS","");				

        // Formul�rio de solicita��o de acesso
        define("A_LANG_REQUEST_ACCESS","Solicita��o de Acesso");
        define("A_LANG_REQUEST_ACCESS_USER_TYPE","T�po de usu�rio");
        define("A_LANG_REQUEST_ACCESS_TYPE_STUDANT","Aluno");
        define("A_LANG_REQUEST_ACCESS_TYPE_TEACHES","Professor");                
        define("A_LANG_REQUEST_ACCESS_NAME","Nome");
        define("A_LANG_REQUEST_ACCESS_EMAIL","E-mail");
        define("A_LANG_REQUEST_ACCESS_PASS","Senha");          
        define("A_LANG_REQUEST_ACCESS_PASS2","Confirma��o da Senha");     
        define("A_LANG_REQUEST_ACCESS_INSTITUTION_EDUCATION","Institui��o de Ensino");     
        define("A_LANG_REQUEST_ACCESS_COMMENT","Observa��o");     
        define("A_LANG_REQUEST_ACCESS_LANGUAGE","Idioma"); 
        define("A_LANG_REQUEST_ACCESS_LANGUAGE1","Portugu�s do Brasil"); 
        define("A_LANG_REQUEST_ACCESS_LANGUAGE2","Espanhol"); 
        define("A_LANG_REQUEST_ACCESS_LANGUAGE3","Franc�s"); 
        define("A_LANG_REQUEST_ACCESS_LANGUAGE4","Ingl�s");         
        define("A_LANG_REQUEST_ACCESS_MSG1","N�o foi poss�vel estabelecer conex�o com o banco de dados");     
	define("A_LANG_REQUEST_ACCESS_MSG2","N�o foi poss�vel enviar seus dados. (informe outro email)");     
        define("A_LANG_REQUEST_ACCESS_MSG3","N�o foi poss�vel enviar seus dados");     
        define("A_LANG_REQUEST_ACCESS_MSG4","Senha inv�lida (Informe a mesma senha nos campos SENHA e CONFIRMA��O DA SENHA)");     
        define("A_LANG_REQUEST_ACCESS_SAVE","Gravar");  


        //Formulario de login
        define("A_LANG_LOGIN2","Login");
        define("A_LANG_EMAIL","E-mail");
	define("A_LANG_LOGIN_PASS","Senha");      
	define("A_LANG_LOGIN_START","Entrar");      
	define("A_LANG_LOGIN_MSG1","Senha inv�lida");       
        define("A_LANG_LOGIN_MSG2","Email inv�lido");       
        define("A_LANG_LOGIN_MSG3","Usu�rio n�o autorizado");       
        define("A_LANG_LOGIN_MSG4","Aguarde a libera��o de seu acesso pelo administrador do ambiente AdaptWeb");       
        define("A_LANG_LOGIN_MSG5","N�o foi possibel estabelecer conex�o com o banco de dados");       
                

        // Formulario de cadastro de Curso
        define("A_LANG_COURSE_REGISTER","Cadastro de Curso");       // (cadastro de curso)I register in cadastre of course
        define("A_LANG_COURSE_RESGISTERED","Cursos cadastrados");   // Registered in cadastre courses
        define("A_LANG_COURSE2","Curso");                      
        define("A_LANG_COURSE_INSERT","Incluir");
        define("A_LANG_COURSE_UPDATE","Alterar");
        define("A_LANG_COURSE_DELETE","Excluir");  
        define("A_LANG_COURSE_MSG1","N�o foi possivel estabelecer conex�o com o banco de dados");  
	define("A_LANG_COURSE_MSG2","N�o foi poss�vel inserir o curso");  
	define("A_LANG_COURSE_MSG3","N�o foi poss�vel alterar o curso"); 
	define("A_LANG_COURSE_MSG4","N�o � poss�vel excluir o curso, porque o mesmo esta relacionado com alguma disciplina. Para remover o curso dever� entrar em disciplina curso tirar da sele��o de todas as disciplinas o curso que deseja excluir."); 
	define("A_LANG_COURSE_MSG5","N�o foi poss�vel excluir o curso"); 
		 

        // Cadastro de disciplina
        define("A_LANG_DISCIPLINES_REGISTER","Cadastro de Disciplina");  // I register in cadastre of disciplines
        define("A_LANG_DISCIPLINES_REGISTERED","Disciplinas cadastradas"); // You discipline registered in cadastre
        define("A_LANG_DISCIPLINES2","Disciplina");    
        define("A_LANG_DISCIPLINES_INSERT","Incluir");
        define("A_LANG_DISCIPLINES_UPDATE","Alterar");
        define("A_LANG_DISCIPLINES_DELETE","Excluir");      
        define("A_LANG_DISCIPLINES_MSG1","N�o foi poss�vel inserir a disciplina"); 
        define("A_LANG_DISCIPLINES_MSG2","N�o foi poss�vel alterar a disciplina"); 
        define("A_LANG_DISCIPLINES_MSG3","N�o � poss�vel excluir a disciplina, porque a mesma esta relacionada com algum curso. Para remover a disciplina dever� entrar em disciplina curso tirar da sele��o todos os cursos que est�o relacionados com a disciplina."); 
        define("A_LANG_DISCIPLINES_MSG4","N�o foi poss�vel excluir a disciplina"); 
        

        // Formul�rio de cadastro de Disciplina / Curso
        define("A_LANG_DISCIPLINES_COURSE1","Disciplina / Curso");
        define("A_LANG_DISCIPLINES_COURSE_DISCIPLINES","Disciplinas");   
        define("A_LANG_DISCIPLINES_COURSE2","Cursos");      
        define("A_LANG_DISCIPLINES_COURSE_SAVE","Gravar");
        define("A_LANG_DISCIPLINES_COURSE_MSG1","Cadastre a(s) DISCIPLINA(S) e CURSO(S) para ter acesso a este item.");
        define("A_LANG_DISCIPLINES_COURSE_MSG2","N�o foi possibel estabecer vinculo da disciplina com os cursos, devido a falha de conexao com banco de dados.");
        

	// Formulario de autoriza��o de acesso
	define("A_LANG_LIBERATE_USER1","Matricula / Aluno");   // para o professor e administrador
        define("A_LANG_LIBERATE_USER2","Autoria / Professor"); // para o adiministrador
        define("A_LANG_LIBERATE_MSG1","Em desenvolvimento ... ");
        
        
        // Formulario para libera��o da disciplina 
	define("A_LANG_LIBERATE_DISCIPLINES","Liberar Disciplina");
        define("A_LANG_LIBERATE_DISCIPLINES_MSG1","Em desenvolvimento ... ");
                                                          
        // ======================= Topicos ==========================
                        
        // Entrada / topicos
        define("A_LANG_ENTRANCE_TOPICS_DISCIPLINES","Disciplina");
        // **** REVER "Estuturar topico"
        define("A_LANG_ENTRANCE_TOPICS_STRUTURALIZE","Estruturar T�picos");
        define("A_LANG_ENTRANCE_TOPICS_MSG1","N�o foi poss�vel ler as disciplinas");
        define("A_LANG_ENTRANCE_TOPICS_MSG2","* Somente estar�o dispon�veis neste formul�rio as disciplinas que estiverem relacionadas com pelo menos um curso.");
        

       // Guia - Lista de t�picos
        define("A_LANG_TOPIC_GENERATE_CONTENT","Gerar Conte�do");       
        //orelha
        define("A_LANG_TOPIC_LIST","Conceitos");     	
        define("A_LANG_TOPIC_MAINTENANCE","Manuten��o do Conceito");  
        define("A_LANG_TOPIC_EXEMPLES","Exemplos");  
        define("A_LANG_TOPIC_EXERCISES","Exerc�cios");  
        define("A_LANG_TOPIC_COMPLEMENTARY","Material Complementar");          
        
        // Guia - manuten��o do t�pico
  	define("A_LANG_TOPIC_NUMBER","N�mero do Conceito");
  	define("A_LANG_TOPIC_DESCRIPTION","Descri��o do Conceito");
  	define("A_LANG_TOPIC_SUMARIZED_TOPIC_DESCRIPTION","Descri��o resumida do conceito");  	
  	define("A_LANG_TOPIC_WORDS_KEY","Palavras-Chave");   
  	define("A_LANG_TOPIC_PREREQUISIT","Pr�-requisito");
  	define("A_LANG_TOPIC_COURSE","Curso");
	define("A_LANG_TOPIC_FILE1","Arquivo");
	define("A_LANG_TOPIC_FILE2","Arquivo(s)");
	define("A_LANG_TOPIC_MAIN_FILE","Arquivo principal");	
	define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE","Arquivo(s) associado(s)");
	define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_STATUS","Situa��o");
	define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_UPLOAD","");		
        define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG1","carregar para o servidor"); 
	define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG2","carregado para o servidor"); 
	// botoes
	define("A_LANG_TOPIC_SAVE","Gravar");  
        define("A_LANG_TOPIC_SEND","Enviar"); 
        define("A_LANG_TOPIC_SEARCH","Procurar");         
	define("A_LANG_TOPIC_INSERT_TOPIC_SAME_LEVEL","Inserir conceito no mesmo n�vel");  
        define("A_LANG_TOPIC_INSERT_SUB_TOPIC","Inserir subconceito");  
	define("A_LANG_TOPIC_DELETE","Excluir");  
	define("A_LANG_TOPIC_PROMOTE","Promover"); 
	define("A_LANG_TOPIC_LOWER","Rebaixar"); 
		
	// Guia - samples
	//define("A_LANG_TOPIC_EXEMPLES_NUMBER_TOPIC","N�mero do T�pico");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Descri��o do exemplo");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","N�vel de Complexidade");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Curso");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Excluir");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Descri��o");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","N�vel");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Curso");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Arquivo");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Arquivo(s) associado(s)");
	// bot�es
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Gravar");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Excluir");
	 
	
  	// Exemplos
 	// Level of Complexity 	 	
	// Description
	// Level
	// Archives associates
        // Description of the example
 
  	// Exercises
        // Description of exercise
  	

  	// Material complementar




        // menu
	// authorize access
	// liberate disciplines
	// new user
        // to modify I register in cadastre (alterar cadastro)
        
        
       	// ****************************************************************************************
 	// * Form - Authoring                                                                     *
 	// ****************************************************************************************

	// Disciplinas Liberadas
	
	
	// Matricula
		
	
	// Aguardando Matricula        
        
        
        // Autorizar Acesso
        
        
        // Navega��o
        
        
        // Tela de Cursos    
        
        
        
        
					
	//
	// Regional Specific Date texts
	//
	// A little help for date manipulation, from PHP manual on function strftime():
	//
	// %a - abbreviated weekday name according to the current locale
	// %A - full weekday name according to the current locale
	// %b - abbreviated month name according to the current locale
	// %B - full month name according to the current locale
	// %c - preferred date and time representation for the current locale
	// %C - century number (the year divided by 100 and truncated to an integer, range 00 to 99)
	// %d - day of the month as a decimal number (range 01 to 31)
	// %D - same as %m/%d/%y
	// %e - day of the month as a decimal number, a single digit is preceded by a space
	//      (range ' 1' to '31')
	// %h - same as %b
	// %H - hour as a decimal number using a 24-hour clock (range 00 to 23)
	// %I - hour as a decimal number using a 12-hour clock (range 01 to 12)
	// %j - day of the year as a decimal number (range 001 to 366)
	// %m - month as a decimal number (range 01 to 12)
	// %M - minute as a decimal number
	// %n - newline character
	// %p - either `am' or `pm' according to the given time value, or the corresponding strings for
	//      the current locale
	// %r - time in a.m. and p.m. notation
	// %R - time in 24 hour notation
	// %S - second as a decimal number
	// %t - tab character
	// %T - current time, equal to %H:%M:%S
	// %u - weekday as a decimal number [1,7], with 1 representing Monday
	// %U - week number of the current year as a decimal number, starting with the first Sunday as
	//      the first day of the first week
	// %V - The ISO 8601:1988 week number of the current year as a decimal number, range 01 to 53,
	//      where week 1 is the first week that has at least 4 days in the current year, and with
	//      Monday as the first day of the week.
	// %W - week number of the current year as a decimal number, starting with the first Monday as
	//      the first day of the first week
	// %w - day of the week as a decimal, Sunday being 0
	// %x - preferred date representation for the current locale without the time
	// %X - preferred time representation for the current locale without the date
	// %y - year as a decimal number without a century (range 00 to 99)
	// %Y - year as a decimal number including the century
	// %Z - time zone or name or abbreviation
	// %% - a literal `%' character
	//
	// Note: A_LANG_DATESTRING is used for Articles and Comments Date
	//       A_LANG_LINKSDATESTRING is used for Web Links creation Date
	//       A_LANG_DATESTRING2 is used for Older Articles box on Home
	//
  	
	define("A_LANG_CODE","ja-JP");
	define("A_LANG_CHACTERSET","Shift_JIS");

	define("A_LANG_NAME_pt-BR","Portugu�s do Brasil");  	
	define("A_LANG_NAME_en-US","Ingl�s");  	
	define("A_LANG_NAME_es-ES","Espanhol");  	
	define("A_LANG_NAME_fr-FR","Frac�s");  	

	define('A_LANG_TIMEZONES','IDLW NT HST YST PST MST CST EST AST GMT-3:30 GMT-3 AT WAT GMT CET EET BT GMT+3:30 GMT+4 GMT+4:30 GMT+5 GMT+5:30 GMT+6 WAST CCT JST ACS GST GMT+11 NZST'); 
	define('A_LANG_TZOFFSETS','0 1 2 3 4 5 6 7 8 8.5 9 10 11 12 13 14 15 15.5 16 16.5 17 17.5 18 19 20 21 21.5 22 23 24'); 
	define('A_LANG_DATEBRIEF','%b %d, %Y');
	define('A_LANG_DATELONG','%A, %B %d, %Y'); 
	define('A_LANG_DATESTRING','%A, %B %d @ %H:%M:%S %Z'); 

	define('A_LANG_DATESTRING2','%A, %B %d');
	define('A_LANG_DATETIMEBRIEF','%b %d, %Y - %I:%M %p');
	define('A_LANG_DATETIMELONG','%A, %B %d, %Y - %I:%M %p %Z'); 

	define('A_LANG_LINKSDATESTRING','%d-%b-%Y');
	define('A_LANG_TIMEBRIEF','%I:%M %p');
	define('A_LANG_TIMELONG','%I:%M %p %Z');

	define('A_LANG_DAY_OF_WEEK_LONG','Sunday Monday Tuesday Wednesday Thursday Friday Saturday');
	define('A_LANG_DAY_OF_WEEK_SHORT','Sun Mon Tue Wed Thu Fri Sat');
	define('A_LANG_MONTH_LONG','January February March April May June July August September October November December');
	define('A_LANG_MONTH_SHORT','Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec');

  	
  		
?>
