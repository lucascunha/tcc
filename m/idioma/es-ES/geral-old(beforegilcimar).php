<?
	/* -----------------------------------------------------------------------
	 *  AdaptWeb - Proyecto de Investigaci�n
	 *     UFRGS - Instituto de Inform�tica
	 *       UEL - Departamento de Computaci�n
	 * -----------------------------------------------------------------------
	 *       @package AdaptWeb
	 *     @subpakage Linguagem
	 *          @file idioma/pt-BR/geral.php
	 *    @desciption Arquivo de tradu��o - Espanhol/Uruguai
	 *         @since 17/08/2003
	 *        @author Veronice de Freitas (veronice@jr.eti.br)
	 *   @Translation Lydia Silva (lydiasil@adinet.com.uy)
	 * -----------------------------------------------------------------------
	 */

       	// ************************************************************************ 	// * Interface - ????                                                                     *
 	// ************************************************************************
	//Datos de la interface de la herramienta de autor�a
  	define("A_LANG_VERSION","Versi�n");
  	define("A_LANG_SYSTEM","Ambiente AdaptWeb");
  	define("A_LANG_ATHORING","Herramienta de Autor�a");
  	define("A_LANG_USER","Usuario");
  	define("A_LANG_PASS","Contrase�a");
  	define("A_LANG_NOTAUTH","< no autenticado>");
  	define("A_LANG_HELP","Ayuda");
  	define("A_LANG_DISCIPLINE","Disciplina");
	define("A_LANG_NAVEGATION","Donde estoy: ");

	// Contexto
	// observaci�n - archivo p_contexto_navegacao.php
       	// ************************************************************************ 	// * Men�                                                                                 *
 	// ************************************************************************
	// Home
	define("A_LANG_MNU_HOME","Inicio");
	define("A_LANG_MNU_NAVIGATION","Ambiente Alumno");
	define("A_LANG_MNU_AUTHORING","Ambiente Profesor");
	define("A_LANG_MNU_UPDATE_USER","Alterar Registro");
	define("A_LANG_MNU_EXIT","Salir");
	define("A_LANG_MNU_LOGIN","Login");
	define("A_LANG_MNU_ABOUT","Sobre");
	define("A_LANG_MNU_DEMO","Demostraci�n");
	define("A_LANG_MNU_PROJECT","Proyecto");
	define("A_LANG_MNU_FAQ","Perguntas Frecuentes");
	define("A_LANG_MNU_RELEASE_AUTHORING","Liberar Autor�a");
	define("A_LANG_MNU_BACKUP","Copia de Seguridad");
	define("A_LANG_MNU_NEW_USER","Nuevo Usuario");
	define("A_LANG_MNU_APRESENTATION","Presentaci�n");
	define("A_LANG_MNU_MY_ACCOUNT","Mi Cuenta");
	define("A_LANG_MNU_MANAGE","Gerenciamento");
	define("A_LANG_MNU_BAKCUP","Copia de Seguridad");
	define("A_LANG_MNU_GRAPH","Acceso");
	define("A_LANG_MNU_TODO","En desarrollo");
	define("A_LANG_SYSTEM_NAME","AdaptWeb - Ambiente de Ense�anza-Aprendizage Adaptativo en la Web ");
	define("A_LANG_SYSTEM_COPYRIGHT","Licensed under the GNU GENERAL PUBLIC LICENSE Version 2"); 
// Autor�a
        define("A_LANG_MNU_COURSE","Curso");
        define("A_LANG_MNU_DISCIPLINES","Disciplina");
        define("A_LANG_MNU_DISCIPLINES_COURSE","Disciplina/Curso");
        define("A_LANG_MNU_STRUTURALIZE_TOPICS","Estructurar Contenido");
        define("A_LANG_MNU_ACCESS_LIBERATE","Analizar Peticiones de Matricula");
        define("A_LANG_MNU_LIBERATE_DISCIPLINES","Liberar Disciplina");
        define("A_LANG_MNU_WORKS","Trabajos de los alumnos");

	// Estudiante
	define("A_LANG_MNU_DISCIPLINES_RELEASED","Asistir Disciplinas");		define("A_LANG_MNU_SUBSCRIBE","Solicitar Matr�cula");				define("A_LANG_MNU_WAIT","Aguardando Matr�cula");
	define("A_LANG_MNU_UPLOAD_FILES","Enviar Trabajos");			define("A_LANG_MNU_MESSAGE","Avisos");
	// ************************************************************************ 	// * Formulario Principal                                                                          *
 	// ************************************************************************
 	// Presentaciones
	define("A_LANG_ORGANS","Instituciones Participantes");
	define("A_LANG_RESEARCHES","Investigadores");
	define("A_LANG_AUTHORS","Autores");


 	// ************************************************************************ 	// * Formulario - Autor�a                                                                     *
 	// ************************************************************************
	// Presentaci�n
	define("A_LANG_MNU_ORGANS","");
	define("A_LANG_MNU_RESEARCHES","");
	define("A_LANG_MNU_AUTHORS","");

        // Formulario de solicitud de acceso
        define("A_LANG_REQUEST_ACCESS","Solicitud de Acceso");
        define("A_LANG_REQUEST_ACCESS_USER_TYPE","Tipo de usuario");
        define("A_LANG_REQUEST_ACCESS_TYPE_STUDANT","Alumno");
        define("A_LANG_REQUEST_ACCESS_TYPE_TEACHES","Profesor");
        define("A_LANG_REQUEST_ACCESS_NAME","Nombre");
        define("A_LANG_REQUEST_ACCESS_EMAIL","E-mail");
        define("A_LANG_REQUEST_ACCESS_PASS","Contrase�a");
        define("A_LANG_REQUEST_ACCESS_PASS2","Confirmaci�n de la Contrase�a ");
        define("A_LANG_REQUEST_ACCESS_INSTITUTION_EDUCATION","Instituto de Ense�anza");
        define("A_LANG_REQUEST_ACCESS_COMMENT","Observaci�n");
        define("A_LANG_REQUEST_ACCESS_LANGUAGE","Idioma");
        define("A_LANG_REQUEST_ACCESS_MSG1","No fue posible establecer conexi�n con la base de datos ");
	define("A_LANG_REQUEST_ACCESS_MSG2","No fue posible enviar sus datos. (informe otro e-mail)");
        define("A_LANG_REQUEST_ACCESS_MSG3"," No fue posible enviar sus datos");
        define("A_LANG_REQUEST_ACCESS_MSG4","Contrase�a inv�lida (Informe la misma contrase�a nos campos CONTRASE�A y CONFIRMACION DE LA CONTRASE�A)");
        define("A_LANG_REQUEST_ACCESS_SAVE","Grabar");


        //Formulario de login
        define("A_LANG_LOGIN2","Login");
        define("A_LANG_EMAIL","E-mail");
	define("A_LANG_LOGIN_PASS","Contrase�a");
	define("A_LANG_LOGIN_START","Entrar");
	define("A_LANG_LOGIN_MSG1","Contrase�a inv�lida");
        define("A_LANG_LOGIN_MSG2","Email inv�lido");
        define("A_LANG_LOGIN_MSG3","Usuario no autorizado");
        define("A_LANG_LOGIN_MSG4","Aguarde la liberaci�n de su acceso por el administrador del ambiente AdaptWeb");
        define("A_LANG_LOGIN_MSG5","No fue posible establecer conexi�n con la base de datos ");


        // Formulario de registro de Curso
        define("A_LANG_COURSE_REGISTER","Registro de Curso");
        define("A_LANG_COURSE_RESGISTERED","Cursos registrados");
        define("A_LANG_COURSE2","Curso");
        define("A_LANG_COURSE_INSERT","Incluir");
        define("A_LANG_COURSE_UPDATE","Alterar");
        define("A_LANG_COURSE_DELETE","Excluir");
        define("A_LANG_COURSE_MSG1"," No fue posible establecer conexi�n con la base de datos ");
	define("A_LANG_COURSE_MSG2"," No fue posible inserir el curso");
	define("A_LANG_COURSE_MSG3"," No fue posible alterar el curso");
	define("A_LANG_COURSE_MSG4"," No es posible excluir el curso, porque el mismo esta relacionado con alguna disciplina. Para remover el curso deber� entrar en disciplina curso sacar de la selecci�n de todas las disciplinas el curso que desea excluir.");
	define("A_LANG_COURSE_MSG5"," No fue posible excluir el curso");


        // Registro de disciplina
        define("A_LANG_DISCIPLINES_REGISTER","Registro de Disciplina");
        define("A_LANG_DISCIPLINES_REGISTERED","Disciplinas registradas");
        define("A_LANG_DISCIPLINES2","Disciplina");
        define("A_LANG_DISCIPLINES_INSERT","Incluir");
        define("A_LANG_DISCIPLINES_UPDATE","Alterar");
        define("A_LANG_DISCIPLINES_DELETE","Excluir");
        define("A_LANG_DISCIPLINES_MSG1"," No fue posible insertar la disciplina");
        define("A_LANG_DISCIPLINES_MSG2"," No fue posible alterar la disciplina");
        define("A_LANG_DISCIPLINES_MSG3"," No es posible excluir la disciplina, porque la misma esta relacionada con alg�n curso. Para remover la disciplina deber� entrar en disciplina curso tirar de la selecci�n todos los cursos que est�n relacionados con la disciplina.");
        define("A_LANG_DISCIPLINES_MSG4"," No fue posible excluir la disciplina");


        // Formulario de registro de Disciplina / Curso
        define("A_LANG_DISCIPLINES_COURSE1","Disciplina/Curso");
        define("A_LANG_DISCIPLINES_COURSE_DISCIPLINES","Disciplinas");
        define("A_LANG_DISCIPLINES_COURSE2","Cursos");
        define("A_LANG_DISCIPLINES_COURSE_SAVE","Grabar");
        define("A_LANG_DISCIPLINES_COURSE_MSG1","Registre la(s) DISCIPLINA(S) y CURSO(S) para tener acceso a este �tem.");
        define("A_LANG_DISCIPLINES_COURSE_MSG2"," No fue posible establecer v�nculo de la disciplina con los cursos debido a una falla de conexi�n con la base de datos.");
        define("A_LANG_DISCIPLINES_COURSE_MSG3","Informe el nombre del curso");
        define("A_LANG_DISCIPLINES_COURSE_MSG4","Curso existente");
        define("A_LANG_DISCIPLINES_COURSE_MSG5","Seleccione un curso para alteraci�n");

	// Formulario de autorizaci�n de acceso
	define("A_LANG_LIBERATE_USER1","Matr�cula / Alumno");   // para el profesor y administrador
        define("A_LANG_LIBERATE_USER2","Autorizar profesor / Autor�a"); // para o adiministrador
        define("A_LANG_LIBERATE_MSG1","Em desarrollo ... ");


        // Formulario para liberaci�n de la disciplina
	define("A_LANG_LIBERATE_DISCIPLINES","Liberar Disciplina");
        define("A_LANG_LIBERATE_DISCIPLINES_MSG1","En desarrollo ... ");

        // ======================= T�picos ==========================

        // Entrada / t�picos
        define("A_LANG_ENTRANCE_TOPICS_DISCIPLINES","Disciplina");
        // **** REVER "Estructurar t�pico"
        define("A_LANG_ENTRANCE_TOPICS_STRUTURALIZE","Estructurar T�picos");
        define("A_LANG_ENTRANCE_TOPICS_MSG1"," No fue posible leer las disciplinas");
        define("A_LANG_ENTRANCE_TOPICS_MSG2","* Solamente estar�n disponibles en este formulario las disciplinas relacionadas con por lo menos un curso.");


       // Guia - Lista de t�picos
        define("A_LANG_TOPIC_GENERATE_CONTENT","Generar Contenido");
        //orelha
        define("A_LANG_TOPIC_LIST","Conceptos");
        define("A_LANG_TOPIC_MAINTENANCE","Mantenimiento de Concepto");
        define("A_LANG_TOPIC_EXEMPLES","Ejemplos");
        define("A_LANG_TOPIC_EXERCISES","Ejercicios");
        define("A_LANG_TOPIC_COMPLEMENTARY","Material Complementario");

        // Guia - mantenimiento de t�pico
  	define("A_LANG_TOPIC_NUMBER","N�mero de Concepto");
  	define("A_LANG_TOPIC_DESCRIPTION","Nombre de Concepto");
  	define("A_LANG_TOPIC_SUMARIZED_TOPIC_DESCRIPTION","Descripci�n resumida de concepto");
  	define("A_LANG_TOPIC_WORDS_KEY","Palabras-Clave");
  	define("A_LANG_TOPIC_PREREQUISIT","Pre-requisito");
  	define("A_LANG_TOPIC_COURSE","Curso");
	define("A_LANG_TOPIC_FILE1","Archivo");
	define("A_LANG_TOPIC_FILE2","Archivo(s)");
	define("A_LANG_TOPIC_MAIN_FILE","Archivo principal");
	define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE","Archivo(s) asociado(s)");
	define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_STATUS","Situaci�n");
	define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_UPLOAD","");
        define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG1","cargar para el servidor");
	define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG2","cargado para el servidor");
	// botones
	define("A_LANG_TOPIC_SAVE","Grabar");
        define("A_LANG_TOPIC_SEND","Enviar");
        define("A_LANG_TOPIC_SEARCH","Procurar");
	define("A_LANG_TOPIC_INSERT_TOPIC_SAME_LEVEL","Insertar concepto en el mismo nivel ");
        define("A_LANG_TOPIC_INSERT_SUB_TOPIC","Insertar subconcepto");
	define("A_LANG_TOPIC_DELETE","Excluir");
	define("A_LANG_TOPIC_PROMOTE","Promover");
	define("A_LANG_TOPIC_LOWER","Rebajar");

	// Guia - samples
	//define("A_LANG_TOPIC_EXEMPLES_NUMBER_TOPIC","N�mero de T�pico");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Descripci�n del ejemplo");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Nivel de Complejidad");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Curso");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Excluir");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Descripci�n");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Nivel");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Curso");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Archivo");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Archivo(s) asociado(s)");
	// botones
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Grabar");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Excluir");


  	// Ejemplos
 	// Nivel de Complejidad
	// Descripci�n
	// Nivel
	// Archivos Asociados
        // Descripci�n de los Ejemplos
  	// Ejercicios
        // Descripci�n del ejercicio
  	// Material complementario
        // Menu
	// Autorizar Acceso
	// Liberar disciplinas
	// Nuevo usuario
        // to modify I register in cadastre (alterar registro)
        // ************************************************************************ 	// * Formulario - Autoria                                                                     *
 	// ************************************************************************	// Disciplinas Liberadas

	// Matricula

	// Aguardando Matricula

        // Autorizar Acceso

        // Navegaci�n

        // Pantalla de Cursos
        // ************************************************************************ 	// * Formulario - Demo
 	// ************************************************************************
     define("A_LANG_DEMO","Disciplina para demostraci�n del Ambiente AdaptWeb - no permite inserciones y alteraciones ");
	//
	// Regional Specific Date texts
	//
	// A little help for date manipulation, from PHP manual on function strftime():
	//
	// %a - abbreviated weekday name according to the current locale
	// %A - full weekday name according to the current locale
	// %b - abbreviated month name according to the current locale
	// %B - full month name according to the current locale
	// %c - preferred date and time representation for the current locale
	// %C - century number (the year divided by 100 and truncated to an integer, range 00 to 99)
	// %d - day of the month as a decimal number (range 01 to 31)
	// %D - same as %m/%d/%y
	// %e - day of the month as a decimal number, a single digit is preceded by a space
	//      (range ' 1' to '31')
	// %h - same as %b
	// %H - hour as a decimal number using a 24-hour clock (range 00 to 23)
	// %I - hour as a decimal number using a 12-hour clock (range 01 to 12)
	// %j - day of the year as a decimal number (range 001 to 366)
	// %m - month as a decimal number (range 01 to 12)
	// %M - minute as a decimal number
	// %n - newline character
	// %p - either `am' or `pm' according to the given time value, or the corresponding strings for
	//      the current locale
	// %r - time in a.m. and p.m. notation
	// %R - time in 24 hour notation
	// %S - second as a decimal number
	// %t - tab character
	// %T - current time, equal to %H:%M:%S
	// %u - weekday as a decimal number [1,7], with 1 representing Monday
	// %U - week number of the current year as a decimal number, starting with the first Sunday as
	//      the first day of the first week
	// %V - The ISO 8601:1988 week number of the current year as a decimal number, range 01 to 53,
	//      where week 1 is the first week that has at least 4 days in the current year, and with
	//      Monday as the first day of the week.
	// %W - week number of the current year as a decimal number, starting with the first Monday as
	//      the first day of the first week
	// %w - day of the week as a decimal, Sunday being 0
	// %x - preferred date representation for the current locale without the time
	// %X - preferred time representation for the current locale without the date
	// %y - year as a decimal number without a century (range 00 to 99)
	// %Y - year as a decimal number including the century
	// %Z - time zone or name or abbreviation
	// %% - a literal `%' character
	//
	// Note: A_LANG_DATESTRING is used for Articles and Comments Date
	//       A_LANG_LINKSDATESTRING is used for Web Links creation Date
	//       A_LANG_DATESTRING2 is used for Older Articles box on Home
	//

	define("A_LANG_CODE","pt-BR");
	define("A_LANG_CHACTERSET","ISO-8859-1");

	define("A_LANG_NAME_pt_BR","Portugu�s de Brasil");
	define("A_LANG_NAME_en_US","Ingl�s");
	define("A_LANG_NAME_es_ES","Espa�ol");
	define("A_LANG_NAME_fr_FR","Franc�s");
	define("A_LANG_NAME_ja_JP","Japon�s");

	define('A_LANG_TIMEZONES','IDLW NT HST YST PST MST CST EST AST GMT-3:30 GMT-3 AT WAT GMT CET EET BT GMT+3:30 GMT+4 GMT+4:30 GMT+5 GMT+5:30 GMT+6 WAST CCT JST ACS GST GMT+11 NZST');
	define('A_LANG_TZOFFSETS','0 1 2 3 4 5 6 7 8 8.5 9 10 11 12 13 14 15 15.5 16 16.5 17 17.5 18 19 20 21 21.5 22 23 24');
	define('A_LANG_DATEBRIEF','%b %d, %Y');
	define('A_LANG_DATELONG','%A, %B %d, %Y');
	define('A_LANG_DATESTRING','%A, %B %d @ %H:%M:%S %Z');

	define('A_LANG_DATESTRING2','%A, %B %d');
	define('A_LANG_DATETIMEBRIEF','%b %d, %Y - %I:%M %p');
	define('A_LANG_DATETIMELONG','%A, %B %d, %Y - %I:%M %p %Z');

	define('A_LANG_LINKSDATESTRING','%d-%b-%Y');
	define('A_LANG_TIMEBRIEF','%I:%M %p');
	define('A_LANG_TIMELONG','%I:%M %p %Z');

	define('A_LANG_DAY_OF_WEEK_LONG','Domingo Lunes Martes Mi�rcoles Jueves Viernes S�bado');
	define('A_LANG_DAY_OF_WEEK_SHORT','Dom Lun Mar Mie Jue Vie S�b');
	define('A_LANG_MONTH_LONG','Enero Febrero Marzo Abril Mayo Junio Julio Agosto Setiembre Octubre Noviembre Diciembre');
	define('A_LANG_MONTH_SHORT','Ene Feb Mar Abr May Jun Jul Ago Set Oct Nov Dic');
  
//************UFRGS******************
	//Index
	  //About
	define('A_LANG_INDEX_ABOUT_INTRO','El ambiente AdaptWeb se ocupa de la construcci�n y presentaci�n adaptativa de disciplinas integrantes de cursos EAD en la Web. El objetivo del ambiente AdaptWeb es adecuar las t�cticas y estilos de presentaci�n de material did�ctico para alumnos de diferentes cursos de graduaci�n y con diferentes estilos de aprendizaje, presentando el material de la manera mas adecuada para cada curso y respetando las preferencias individuales de los alumnos participantes.<br> El Ambiente AdaptWeb fu� inicialmente desarrollado por investigadores de la UFRGS y de la UEL, a trav�s de los proyectos Electra y AdaptWeb, con apoyo del CNPq. Visita el sitio del proyecto: <a href=\'http://www.inf.ufrgs.br/adapt/adaptweb\' target=\'_blank\'>http://www.inf.ufrgs.br/adapt/adaptweb</a>');
	define('A_LANG_INDEX_ABOUT_ENVACCESS','Acceso al ambiente');
	define('A_LANG_INDEX_ABOUT_TINFO','para disponibilizar el contenido de sus disciplinas el profesor debe efectuar un registro de solicitud de acceso en el Ambiente de Autor�a ');
	define('A_LANG_INDEX_ABOUT_LINFO','para tener acceso a la(s) disciplina(s) el alumno debe efectuar el registro en el Ambiente de Navegaci�n y solicitar matricula en la(s) disciplina(s) relacionadas a su curso');
	  //Demo
	define('A_LANG_INDEX_DEMO_AENVIRONMENT','Ambiente de Autoria');
	define('A_LANG_INDEX_DEMO_FDESC1', 'Crear disciplina(s)');
	define('A_LANG_INDEX_DEMO_NENVIRNMENT','Ambiente de navegaci�n');
	define('A_LANG_INDEX_DEMO_FDESC2', 'Cursar disciplina(s)');
	define('A_LANG_INDEX_DEMO_TOLOG','Entre al ambiente y navegue por la disciplina de ejemplo efectuando el loguin en el ambiente con los datos abajo:');
	define('A_LANG_INDEX_DEMO_LOGIN','login: demo@inf.ufrgs.br');
	define('A_LANG_INDEX_DEMO_PASS', 'senha: 123');
	define('A_LANG_INDEX_DEMO_OBSERVATION','Observaci�n');
	define('A_LANG_INDEX_DEMO_OBSERVATIONTEXT', 'despu�s de efectuar el login ser� habilitado el acceso al ambiente del profesor y al ambiente del alumno por las opciones "Ambiente del profesor" y "Ambiente del alumno" en la pantalla inicial del AdaptWeb.');
	  //Apresenta��o
	define('A_LANG_INDEX_PRESENTATION_INFABOUT', 'Informaci�n sobre el proyecto:');
	define('A_LANG_DOWNLOAD', 'Download');
	define('A_LANG_PUBLICATIONS', 'Publicaciones');
	define('A_LANG_INDEX_PRESENTATION_AVAILABLEAT','Disponible en:');
	

	
	//Ambiente do Aluno - Entrada
	  //Principal
	define('A_LANG_LENVIRONMENT','Ambiente del alumno');
	define('A_LANG_LENVIRONMENT_DESCRIPTION', 'Este ambiente posibilita al alumno a tener acceso al material did�ctico de su(s) profesor(es) adaptado a su perfil');	
	define('A_LANG_LENVIRONMENT_WARNING', 'para tener acceso a su(s) disciplinas, el alumno debe inicialmente efectuar su registro en el ambiente y solicitar matr�cula en dicha(s) disciplina(s). Solamente despu�s de la habilitaci�n de acceso por el profesor responsable ser� posible acceder a una disciplina.');
	
	define('A_LANG_LENVIRONMENT_WATCH_LIBERATED', 'Disciplinas habilitadas');
	define('A_LANG_LENVIRONMENT_WATCH_MINE', 'Disciplinas de mi autoria (navegar por curso)');
	define('A_LANG_LENVIRONMENT_WATCH_MY', 'Mis Disciplinas');
	define('A_LANG_LENVIRONMENT_WATCH_NOT', 'No hay disciplinas registradas');
	define('A_LANG_LENVIRONMENT_WATCH_WARNING', 'Solicite matr�culas para tener acceso a las disciplinas.');
	define('A_LANG_LENVIRONMENT_WATCH_VISUAL', 'Visualizar disciplina para cada curso');
	define('A_LANG_LENVIRONMENT_WATCH_CREATE', 'Cree las disciplinas y genere material (Generar Material) en el Ambiente del Profesor (item <b>Estructurar Contenido => Lista de Conceptos </b>) para tener acceso al ambiente de navegaci�n.');
	define('A_LANG_LENVIRONMENT_WATCH_OBS', 'para visualizar las disciplinas de su autoria no es necesario Habilitar Disciplina para testear el ambiente de navegaci�n.');
	define('A_LANG_LENVIRONMENT_REQUEST', 'Solicitar Matr�cula');
	define('A_LANG_LENVIRONMENT_REQUEST_REQ', 'Solicitar');
	define('A_LANG_LENVIRONMENT_REQUEST_NOTC', 'No hay curso registrado');
	define('A_LANG_LENVIRONMENT_REQUEST_NOTD', 'No hay disciplina registrada para el curso seleccionado');
	define('A_LANG_LENVIRONMENT_REQUEST_SELECT', 'Seleccione para solicitar matr�cula:');
	define('A_LANG_LENVIRONMENT_REQUEST_MATR', 'Seleccione para solicitar matr�cula:');
	define('A_LANG_LENVIRONMENT_WAITING', 'Aguardando Matr�cula');
	define('A_LANG_LENVIRONMENT_WAITING_NOT', 'No hay matriculas registradas');
	define('A_LANG_LENVIRONMENT_WAITING_WARNING', 'Solicite matr�cula para tener acceso a las disciplinas:');
	define('A_LANG_LENVIRONMENT_NAVTYPE', 'Tipo de Navegaci�n');
	define('A_LANG_LENVIRONMENT_FORCOURSE', 'para el curso de');
	define('A_LANG_LENVIRONMENT_NETCONECTION', 'Conexi�n de Red');
	define('A_LANG_LENVIRONMENT_SPEED',  'La velocidad es');
	define('A_LANG_LENVIRONMENT_NAVIGATION', 'Navegaci�n');
	define('A_LANG_LENVIRONMENT_TUTORIAL', 'Tutorial');
	define('A_LANG_LENVIRONMENT_FREE', 'Libre');

	
	//Ambiente do professor
	define('A_LANG_TENVIRONMENT_DESCRIPTION', ' El modulo de autor�a permite al autor disponibilizar material adaptado a diferentes perfiles de alumnos. A trav�s del proceso de autor�a el autor puede disponibilizar el material de sus aulas en una �nica estructura adaptada para para diferentes cursos. En esta etapa el autor debe registrar la disciplina e indicar para cuales cursos desea hacerla disponible. Despu�s del registro deben ser ingresados los archivos de los conceptos que incluye el material, relacion�ndolos con cada t�pico de la disciplina. Adem�s el autor debe proporcionar una descripci�n del t�pico, sus prerrequisitos y dar la indicaci�n de para que cursos desea disponibilizar el contenido. El  autor podr� agregar ejercicios, ejemplos y material complementario para cada t�pico');
	define('A_LANG_TENVIRONMENT_AUTHOR', 'Autoria');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR1', 'Ocurri� un error al intentar matricular al usuario en esta disciplina. Ocurri� un error en el servidor, probablemente el usuario ya est� matriculado');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR2', 'Ocurri� un error al intentar anular la matr�cula del usuario en esta disciplina. Ocurri� un error en el servidor, probablemente el usuario no est� matriculado');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_NOT', 'No hay curso registrado');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECT', 'Seleccione el curso');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_NOTD', 'No hay disciplina registrada para este curso');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECTD', 'Seleccione la disciplina');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_LIST', 'Lista de Alumnos:');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_MATR', 'Alumnos Matriculados:');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_BACK', 'Volver');
	define('A_LANG_TENVIRONMENT_LIBERATE_DESCRIPTION', '* solamente estar�n disponibles para ser liberadas las disciplinas que su contenido correctamente estructurado. Para verificar el contenido de las disciplinas entre en la guia <b>CONCEITO</b> en el �tem <b>ESTRUCTURAR CONTENIDO</b> y use el bot�n <b>GENERAR CONTENIDO</b>.');
	define('A_LANG_TENVIRONMENT_LIBERATE_LIB', 'Habilitar<br> disciplina:');
	define('A_LANG_TENVIRONMENT_LIBERATE_LIB_DIS', 'Disciplinas<br> habilitadas:');
	define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE', 'Mover Concepto');
	define('A_LANG_TENVIRONMENT_ESTRUCT_EXCLUDE', 'Excluir Concepto');
	define('A_LANG_TENVIRONMENT_ESTRUCT_SELECT', 'Seleccione el concepto que desea <B>MOVER</B> ou <B>EXCLUIR</B>');
	define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE_AFTER', '<B>MOVER</B> concepto seleccionado despu�s de cual concepto?');
	define('A_LANG_TENVIRONMENT_ESTRUCT_CANCEL', 'la operaci�n de MOVER o EXCLUIR concepto lleva consigo el cancelamiento de los prerequisitos');
	define('A_LANG_TENVIRONMENT_ESTRUCT_NOT_POSSIBLE', 'No fue posible leer la matriz de la base de datos');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND', 'R�tulo de prerequisitos:');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND1', 'Para seleccionar varios prerequisitos use la tecla CTRL');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND2', 'Para retirar lo(s) prerequisito(s) de la selecci�n use la tecla CTRL');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND3', 'Los prerequisitos en <font class=buttonselecionado>azul oscuro</font> est�n seleccionados');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND4', 'El fondo <font class=buttonpre>azul</font> indica los prerequisitos actuales (en caso de perder la selecci�n)');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND5', 'El fondo <font class=buttonpreautomatico>amarillo</font> indica los prerequisitos actuales autom�ticos');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NUMBER', 'N�mero de concepto:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NAME', 'Nombre del concepto:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_DESCRIPTION', 'Descripci�n del concepto:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_KEY', 'Palabras clave del concepto:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_LEVEL', 'Nivel de Complejidad:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NOCLASS', 'Sin clasificaci�n');
	define('A_LANG_TENVIRONMENT_EXAMPLES_EASY', 'F�cil');
	define('A_LANG_TENVIRONMENT_EXAMPLES_MEDIUM', 'Medio');
	define('A_LANG_TENVIRONMENT_EXAMPLES_COMPLEX', 'Complejo');
	define('A_LANG_TENVIRONMENT_EXAMPLES_DSC', 'Descripci�n');
	define('A_LANG_TENVIRONMENT_EXAMPLES_LVL', 'Nivel');
	define('A_LANG_TENVIRONMENT_EXERCISES_NAME', 'Nombre del ejercicio:');
	define('A_LANG_TENVIRONMENT_EXERCISES_DESCRIPTION', 'Descripci�n del ejercicio:');
	define('A_LANG_TENVIRONMENT_EXERCISES_KEY', 'Palabras clave del ejercicio:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_NAME', 'Nombre del Material <br> Complementario:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_DESCRIPTION', 'Descripci�n del Material <br> Complementario:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_KEY', 'Palabras clave del <br> Material Complementario:');
	define('A_LANG_TENVIRONMENT_ACCESS', 'Acceso al Ambiente de Autoria');
	define('A_LANG_TENVIRONMENT_WAIT', 'Aguarde a que el administrador del ambiente libere el  acceso para el ambiente de autoria.');
	define('A_LANG_TENVIRONMENT_COMMENT', 'el acceso al ambiente de autoria es gerenciado por el administrador del ambiente AdaptWeb. Despu�s de efectuar el registro en el ambiente <br> es necesaria la  autorizaci�n del administrador para tener acceso al ambiente de autoria.');
	define('A_LANG_TENVIRONMENT_DEMO', 'Demostraci�n de los Ambientes');
	define('A_LANG_TENVIRONMENT_ASSOCIATED_FILES','Obs.: Archivos asociados debem ser enviados para el servidor atrav�s de la secci�n "manutenci�n del concepto"');

	
	//Op��es de Root
	define('A_LANG_ROOT_NOT_AUTH', 'Profesores<br> no autorizados:');
	define('A_LANG_ROOT_AUTH', 'Profesores<br> autorizados:');
	define('A_LANG_ROOT_DEV', 'Esta opci�n se encuentra en fase de desarrollo.');

	//Outros
	define('A_LANG_DATA', 'Fecha');
	define('A_LANG_WARNING', 'Aviso');
	define('A_LANG_COORDINATOR', 'COORDENADOR');
	define('A_LANG_DATABASE_PROBLEM', 'Hay problemas con la Base de datos');
	define('A_LANG_FAQ', 'Preguntas Frecuentes');
	define('A_LANG_WORKS', 'Trabajos');
	define('A_LANG_NOT_DISC', 'No fue posible presentar las disciplinas registradas');
	define('A_LANG_NO_ROOT', 'No fue posible agregar el usuario Root');
	define('A_LANG_MIN_VERSION',  'La versi�n m�nima soportada, de la base de datos es [');
	define('A_LANG_INSTALLED_VERSION', '], y la versi�n  instalada es [');
	define('A_LANG_ERROR_BD', 'No fue posible grabar la matriz en la Base de datos');
	define('A_LANG_DISC_NOT_FOUND', 'Nombre de la Disciplina no encontrado o en blanco');
	define('A_LANG_SELECT_COURSE', 'seleccione el(los) curso(s) en el Concepto <b>');
	define('A_LANG_TOPIC_REGISTER', 'Registro de Conceptos');
	define('A_LANG_SUMARIZED_DESCRIPTION', 'Descripci�n resumida<br> del Concepto');
	define('A_LANG_LOADED', 'cargada para el servidor');
	define('A_LANG_FILL_DESCRIPTION', 'Complete el campo descripci�n del material');
	define('A_LANG_WORKS_SEND_DATE', 'Fecha de envio');
	define('A_LANG_SEARCH_RESULTS', 'Resultados de B�squeda');
	define('A_LANG_SEARCH_SYSTEM', 'Sistema de B�squeda');
	define('A_LANG_SEARCH_WORD', 'Palabra B�scada');
	define('A_LANG_SEARCH_FOUND', 'Fueron encontradas ');
	define('A_LANG_SEARCH_OCCURRANCES', ' ocurrencias de ');
	define('A_LANG_SEARCH_IN_TOPIC', ' en CONCEPTO');
	define('A_LANG_SEARCH_FOUND1', 'Fue encontrada ');
	define('A_LANG_SEARCH_OCCURRANCE', ' ocurrencia de ');
	define('A_LANG_SEARCH_NOT_FOUND', 'No fueron encontradas ocurrencias de ');
	define('A_LANG_SEARCH_CLOSE', 'Cerrar');
	define('A_LANG_TOPIC', 'Concepto');
	define('A_LANG_SHOW_MENU', 'Ver Menu');
	define('A_LANG_CONFIG', 'Configuraciones');
	define('A_LANG_MAP', 'Mapa');
	define('A_LANG_PRINT', 'Imprimir esta p�gina?');
	define('A_LANG_CONNECTION', 'No fue posible establecer una  conexi�n con el servidor de base de datos MySQL. Favor entrar en contacto con el administrador.');
	define('A_LANG_CONTACT', 'Favor entrar en contacto con el administrador');
	define('A_LANG_HELP_SYSTEM', 'Sistema de Ayuda');
	define('A_LANG_CONFIG_SYSTEM', 'Configuraciones del Sistema');
	define('A_LANG_CONFIG_COLOR', 'Configurar color de fondo');
	define('A_LANG_NOT_CONNECTED', 'No conect�!');
	define('A_LANG_DELETE_CADASTRE', 'Exclusi�n  de Registro de Alumnos');
	define('A_LANG_HOME', 'Home');
	define('A_LANG_YOU_HAVE', 'Usted tiene');
	define('A_LANG_TO_CONFIRM', ' matr�cula(s). Para confirmar esta exclusi�n cliquee en ');
	define('A_LANG_CONFIRM', ' CONFIRMAR');
	define('A_LANG_EXAMPLE_LIST', 'Lista de Ejemplos');
	define('A_LANG_COMPLEXITY_LEVEL_EASY', 'Nivel de Complejidad Facil');
	define('A_LANG_COMPLEXITY_LEVEL_MEDIUM', 'Nivel de Complejidad Medio');
	define('A_LANG_COMPLEXITY_LEVEL_COMPLEX', 'Nivel de Complejidad Dif�cil');
	define('A_LANG_EXERCISES_LIST', 'Lista de Ejercicios');
	define('A_LANG_REGISTER_MAINTENENCE', 'Mantenimiento de Matr�culas');
	define('A_LANG_REGISTER_RELEASE', 'Liberaci�n de Matr�culas');
	define('A_LANG_SITE_MAP', 'Mapa del Sitio');
	define('A_LANG_COMPLEMENTARIY_LIST', 'Lista de Material Complementario');
	define('A_LANG_DELETE_TOPIC', 'Excluir el t�pico ');
	define('A_LANG_AND', ' y ');
	define('A_LANG_SONS', ' hijos');
	define('A_LANG_CONFIRM_EXCLUSION', 'Est�s cierto de que quieres excluir la disciplina');
	define('A_LANG_YES', '  S�  ');
	define('A_LANG_NO', '  No  ');

		
	//Gera��o de conte�do
	define('A_LANG_GENERATION_END', 'Fin de la generaci�n del archivo xml de elementos del t�pico');
	define('A_LANG_GENERATION_CREATED', 'Fueron creados ');
	define('A_LANG_GENERATION_XML', ' archivos xml.');
	define('A_LANG_GENERATION_END_STRUCT', 'Fin de la generaci�n del XML de Estructura de T�picos');
	define('A_LANG_GENERATION_MISSING_FILE', 'Faltando agregar archivo de Concepto del ');
/*	define('A_LANG_GENERATION_TH_TOPIC', '� Concepto');
	define('A_LANG_GENERATION_MISSING_COURSE', 'Faltando agregar curso del ');
	define('A_LANG_GENERATION_NO_ASOC', 'Concepto sin curso asociado');
	define('A_LANG_GENERATION_MISSING_EXERC_ID', 'Faltando agregar identificador de ejercicio del ');
	define('A_LANG_GENERATION_TH_EXERCISE', '� ejercicio y ');
	define('A_LANG_GENERATION_MISSING_EXERC_DESCRIPTION', 'Faltando agregar descripci�n de ejercicio del ');
	define('A_LANG_GENERATION_MISSING_EXERC_FILE', 'Faltando agregar archivo de ejercicio del ');
	define('A_LANG_GENERATION_MISSING_EXERC_COMPLEXITY', 'Faltando agregar complejidad del ejercicio del ');
	define('A_LANG_GENERATION_TH_EXAMPLE', '� ejemplo y ');
	define('A_LANG_GENERATION_MISSING_EXAMP_ID', 'Faltando agregar identificador de ejemplo del ');
	define('A_LANG_GENERATION_MISSING_EXAMP_DESCRIPTION', 'Faltando agregar descripci�n de ejemplo del ');
	define('A_LANG_GENERATION_MISSING_EXAMP_FILE', 'Faltando agregar archivo de ejemplo del ');
	define('A_LANG_GENERATION_MISSING_EXAMP_COMPLEXITY' ,'Faltando agregar complejidad de ejemplo del ');
	define('A_LANG_GENERATION_TH_MATCOMP', '� material complementario y ');
	define('A_LANG_GENERATION_MISSING_MATCO_ID', 'Faltando agregar identificador de material complementario del ');
	define('A_LANG_GENERATION_MISSING_MATCO_DESCRIPTION', 'Faltando agregar descripci�n de material complementario del ');
	define('A_LANG_GENERATION_MISSING_MATCO_FILE', 'Faltando agregar archivo de material complementario del ');
*/	define('A_LANG_GENERATION_VERIFY', 'Verificaci�n de los datos de la autoria');
	define('A_LANG_GENERATION_PROBLEMS', 'Problemas en el registro de la Disciplina');
/*	define('A_LANG_GENERATION_MISSING_TOPIC_NUMBER', 'Faltando agregar Numero del T�pico para el ');
	define('A_LANG_GENERATION_MISSING_TOPIC_DESCRIPTION', 'Faltando agregar Descripci�n del T�pico para el ');
	define('A_LANG_GENERATION_MISSING_TOPIC_ABREV', 'Faltando agregar Abreviaci�n del T�pico para el ');
	define('A_LANG_GENERATION_MISSING_TOPIC_KEYWORD', 'Faltando agregar Palabra Clave del T�pico para el ');
*/	define('A_LANG_GENERATION_RESULT_XML', 'Resultado de la Generaci�n del XML');
	define('A_LANG_GENERATION_NO_ERROR', 'NO HAY ERROR');
	define('A_LANG_GENERATION_SUCCESS', 'ARCHIVOS DEL CURSO GENERADOS CON �XITO');
	define('A_LANG_GENERATION_DATA_LACK', 'Archivos XML no fueron generados por falta de dados');
	define ('A_LANG_GENERATION_TOPIC','Concepto');	
	define ('A_LANG_GENERATION_EXAMPLE','Ejemplo');	
	define ('A_LANG_GENERATION_EXERCISE','Ejercicio');	
	define ('A_LANG_GENERATION_COMPLEMENTARY','Material Complementario');	
	define ('A_LANG_GENERATION_TOPIC_NUMBER','N�mero del T�pico');	
	define ('A_LANG_GENERATION_TOPIC_NAME','Nombre del T�pico');	
	define ('A_LANG_GENERATION_DESCRIPTION','Descripci�n');	
	define ('A_LANG_GENERATION_KEY_WORD','Palabra Clave');	
	define ('A_LANG_GENERATION_COURSE','Curso');	
	define ('A_LANG_GENERATION_PRINC_FILE','Archivo Principal');	
	define ('A_LANG_GENERATION_ASSOC_FILE','Archivos Asociados');	
	define ('A_LANG_GENERATION_MISSING','Ausente');	
	define ('A_LANG_GENERATION_NO_PROBLEM','Ok');	
	define ('A_LANG_GENERATION_NUMBER','N�mero');	
	define ('A_LANG_GENERATION_COMPLEXITY','Complejidad');	
	define ('A_LANG_GENERATION_NOT_EXIST','No tiene');	
	
	//Reposit�rio
	define ('A_LANG_MNU_REPOSITORY','Repository');	
	define ('A_LANG_PRESENTATION','Presentation');
	define ('A_LANG_EXPORT','Disciplines Exportation');
	define ('A_LANG_IMPORT','Disciplines Importation');
	define ('A_LANG_OBJECT_SEARCH','Search Learning Objects');
	define ('A_LANG_EXP_TEXT', 'Send your disciplines to the Repository, so the other teachers can use it too.');
	define ('A_LANG_IMP_TEXT', 'Use available disciplines in your AdaptWeb environment.');
	define ('A_LANG_SCH_TEXT', 'The AdaptWeb disciplines\' data were also indexed in the Learning Objects format, using metadata of the LOM (Learning Object Metadata) standard. Make searches by learning objects envolving disciplines, topics (concepts) and repository files.');
	define ('A_LANG_REP_TEXT', 'Welcome to the AdaptWeb\'s Disciplines Repository.<br>Through this repository, AdaptWeb users from different locations will be able to share data to create their disciplines. Here you can make the importation and exportation of disciplines, and search for learning objects.');
	define ('A_LANG_REP_SERVER', 'Repository Server');
	define ('A_LANG_REP_ADAPT_SERVER', 'AdaptWeb Server');
	define ('A_LANG_ADAPTWEB', 'AdaptWeb');
	define ('A_LANG_REP_DISC', 'Disciplines Repository');
	define ('A_LANG_REP_JUNE', 'June of 2004');
	define ('A_LANG_REP_SUGESTIONS', 'Doubts, sugestions, claimings');
	define ('A_LANG_REP_SELECT', 'Select a discipline');
	define ('A_LANG_REP_EXP_SUCCESS', 'Exportation succesfully done!');
	define ('A_LANG_REP_EXP_ERROR', 'Error exporting discipline!');
	define ('A_LANG_REP_EXP_TEXT', 'Through this interface you will be able to let you AdaptWeb disciplines available so that other people may use it.<br>Select the discipline in the list above, specify a description to her and click in &quot;Export&quot;.');
	define ('A_LANG_REP_EXPORT', 'Export');
	define ('A_LANG_REP_SELECT_COURSE', 'Select a Course');
	define ('A_LANG_REP_IMP_SUCCESS', 'Importation successfully done!');
	define ('A_LANG_REP_IMP_ERROR', 'Error importing discipline!');
	define ('A_LANG_REP_INVALID_PAR', 'Invalid Parameters!');
	define ('A_LANG_REP_IMP_TEXT', 'Through this interface you will be able to copy available disciplines of the repository to your authoship environment on AdaptWeb!<br>Select the discipline on the list above, specify the course which it will be associated, click in &quot;Import&quot;.');
	define ('A_LANG_REP_DISC_IN_REP', 'Disciplines in Repository');
	define ('A_LANG_REP_MY_COURSES', 'My Courses');
	define ('A_LANG_REP_OTHER_NAME', 'Import with another name');
	define ('A_LANG_REP_IMPORT', 'Import');
	define ('A_LANG_REP_DISC_DATA', 'Discipline Data');
	define ('A_LANG_REP_SEARCH', 'Digit a search string and choose the metadata that must be included in the search:');
	define ('A_LANG_REP_SCH_TITLE', 'Title');
	define ('A_LANG_REP_SCH_SELECT', 'Select...');
	define ('A_LANG_REP_AGGREG', 'Aggregation Level');
	define ('A_LANG_REP_SCH_RAW', 'Raw Data (files)');
	define ('A_LANG_REP_SCH_TOPIC', 'Topic');
	define ('A_LANG_REP_SCH_DISC', 'Discipline');
	define ('A_LANG_REP_SCH_COURSE', 'Course');
	define ('A_LANG_REP_SCH_SEARCH', 'Search');
	define ('A_LANG_REP_SCH_ERROR', 'Error');
	define ('A_LANG_REP_SCH_NO_REG', 'No register found!');
	define ('A_LANG_REP_SCH_REG', 'register found');
	define ('A_LANG_REP_SCH_REGS', 'registers found');
	define ('A_LANG_REP_DISC_NOT_FOUND', 'Discipline not found');
	define ('A_LANG_COURSES', 'Course(s)');
	define ('A_LANG_EXP_BY', 'Exported by');
	define ('A_LANG_ERROR_SELECT_BD', 'Error when selecting the data base');
?>