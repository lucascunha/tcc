<? 
/* -----------------------------------------------------------------------
*  AdaptWeb - Projeto de Pesquisa       
*     UFRGS - Instituto de Inform�tica  
*       UEL - Departamento de Computa��o
* -----------------------------------------------------------------------
*       @package AdaptWeb
*     @subpakage Linguagem
*          @file idioma/pt-BR/geral.php
*    @desciption Arquivo de tradu��o - Portugu�s/Brasil
*         @since 17/08/2003
*        @author Veronice de Freitas (veronice@jr.eti.br)
*   @Translation Veronice de Freitas
* -----------------------------------------------------------------------         
*/  
 
       
  // ************************************************************************
//Dados da interface da ferramenta de autoria
  define("A_LANG_VERSION","Versi�n"); 
  define("A_LANG_SYSTEM","Ambiente AdaptWeb"); 
  define("A_LANG_ATHORING","Herramienta de Autor"); 
  define("A_LANG_USER","Usuario");  
  define("A_LANG_PASS","Contrase�a");  
  define("A_LANG_NOTAUTH","< no autenticado>"); 
  define("A_LANG_HELP","Ayuda"); 
  define("A_LANG_DISCIPLINE","Disciplina");  
  define("A_LANG_NAVEGATION","D�nde estoy: ");

// Contexto
// observa��o - arquivo p_contexto_navegacao.php
       // ************************************************************************ // * Menu                                                                                 *
  // ************************************************************************
// Home
define("A_LANG_MNU_HOME","Inicio"); 
define("A_LANG_MNU_NAVIGATION","Ambiente Estudiante");
define("A_LANG_MNU_AUTHORING","Ambiente Profesor"); 
define("A_LANG_MNU_UPDATE_USER","Cambio de registro");
define("A_LANG_MNU_EXIT","Salir"); 
define("A_LANG_MNU_LOGIN","Login"); 
define("A_LANG_MNU_ABOUT","Sobre");
define("A_LANG_MNU_DEMO","Demostraci�n");
define("A_LANG_MNU_PROJECT","Projet");
define("A_LANG_MNU_FAQ","Preguntas m�s frecuentes"); 
define("A_LANG_MNU_RELEASE_AUTHORING","Autor liberaci�n"); 
define("A_LANG_MNU_BACKUP","Copia de seguridad"); 
define("A_LANG_MNU_NEW_USER","Nuevo Usuario"); 
define("A_LANG_MNU_APRESENTATION","Presentaci�n");
define("A_LANG_MNU_MY_ACCOUNT","Mi Cuenta");
define("A_LANG_MNU_MANAGE","Gesti�n"); 
define("A_LANG_MNU_BAKCUP","Copia de seguridad");
define("A_LANG_MNU_GRAPH","Acceso");
define("A_LANG_MNU_TODO","En desarrollo"); 
// Alterado por Carla-estagio UDESC 2008/02 define("A_LANG_SYSTEM_NAME","AdaptWeb - Ambiente de Ensino-Aprendizagem Adaptativo na Web ");  
define("A_LANG_SYSTEM_NAME","AdaptWeb");

define("A_LANG_SYSTEM_COPYRIGHT","Licensed under the GNU GENERAL PUBLIC LICENSE Version 2"); 
        // Autoria        
        define("A_LANG_MNU_COURSE","Curso");  
        define("A_LANG_MNU_DISCIPLINES","Disciplina");         
        define("A_LANG_MNU_DISCIPLINES_COURSE","Disciplina/Curso");                        
        define("A_LANG_MNU_STRUTURALIZE_TOPICS","Estructuraci�n de contenido"); 
        define("A_LANG_MNU_ACCESS_LIBERATE","Examinar las solicitudes de registro");
        define("A_LANG_MNU_LIBERATE_DISCIPLINES","Liberaci�n de Disciplina");
        define("A_LANG_MNU_WORKS","Trabajo de los estudiantes"); 
     	 define("A_LANG_MNU_LOG","An�lisis de log");  
	 define("QUESTIONARIO","Cuestionario"); 
	 define("QS_ROOT","Resultados QS");   
      

// Estudante
define("A_LANG_MNU_DISCIPLINES_RELEASED","Ver disciplinas");
define("A_LANG_MNU_SUBSCRIBE","Solicitud de Inscripci�n");
define("A_LANG_MNU_WAIT","En espera de Inscripci�n");
define("A_LANG_MNU_UPLOAD_FILES","Enviar Trabajos");
define("A_LANG_MNU_MESSAGE","Avisos");
// ************************************************************************ // * Formul�rio Principal                                                                          *
  // ************************************************************************ 
  // Apresenta��es
define("A_LANG_ORGANS","Instituciones participantes");
define("A_LANG_RESEARCHES","Investigadores");
define("A_LANG_AUTHORS","Autores"); 

 
  // ************************************************************************ // * Formul�rio - Autoria                                                                     *
  // ************************************************************************
// Apresenta��o
define("A_LANG_MNU_ORGANS",""); 
define("A_LANG_MNU_RESEARCHES",""); 
define("A_LANG_MNU_AUTHORS",""); 

        // Formul�rio de solicita��o de acesso
        define("A_LANG_REQUEST_ACCESS","Solicitud de Acceso");
        define("A_LANG_REQUEST_ACCESS_USER_TYPE","Tipo de usuario *");
        define("A_LANG_REQUEST_ACCESS_TYPE_STUDANT","Estudiante");
        define("A_LANG_REQUEST_ACCESS_TYPE_TEACHES","Profesor");                
        define("A_LANG_REQUEST_ACCESS_NAME","Nombre *");
        define("A_LANG_REQUEST_ACCESS_EMAIL","Correo Electr�nico: *");
        define("A_LANG_REQUEST_ACCESS_PASS","Contrase�a *");          
        define("A_LANG_REQUEST_ACCESS_PASS2","confirmaci�n de la contrase�a *");     
        define("A_LANG_REQUEST_ACCESS_INSTITUTION_EDUCATION","Instituci�n de Educaci�n");     
        define("A_LANG_REQUEST_ACCESS_COMMENT","Observaci�n");     
        define("A_LANG_REQUEST_ACCESS_LANGUAGE","Idioma");               
        define("A_LANG_REQUEST_ACCESS_MSG1","No se puede conectar a la base de datos");     
	    define("A_LANG_REQUEST_ACCESS_MSG2","Decirle a otro e-mail, que este ya est� registrado");
        define("A_LANG_REQUEST_ACCESS_MSG3","No se puede enviar sus datos");     
        define("A_LANG_REQUEST_ACCESS_MSG4","Contrase�a no v�lida (Introduzca la misma contrase�a en el campo Contrase�a y confirmar contrase�a)");     
        define("A_LANG_REQUEST_ACCESS_SAVE","Salvar");  
	// Acrescenta mensagem para este formulario - Carla -est�gio UDESC 2008/02
	define("A_LANG_REQUEST_ACCESS_MSG5","Los campos que tienen * son obligatorios"); 
	define("A_LANG_REQUEST_ACCESS_MSG6","El campo de correo electr�nico deben ser llenadas");
	define("A_LANG_REQUEST_ACCESS_MSG7","El campo de Nombre deben ser llenadas");
	define("A_LANG_REQUEST_ACCESS_MSG8","El campo de contrase�a debe ser llenado con um valor v�lido");
	define("A_LANG_REQUEST_ACCESS_MSG9","Los campos Contrase�a y Confirmaci�n de contrase�a debe ser llenado con los mismos datos");       
	define("A_LANG_REQUEST_ACCESS_MSG10","Tu inscripci�n se realiz� con �xito");
	define("A_LANG_REQUEST_ACESS_OBSERVACAO", " Nota: Los campos que tienen * son obligatorios."); 
	// Fim das alteracoes Carla


        //Formulario de login
        define("A_LANG_LOGIN2","Login");
        define("A_LANG_EMAIL","correa eletr�nico");
define("A_LANG_LOGIN_PASS","Contrase�a");      
define("A_LANG_LOGIN_START","Entrar");      
define("A_LANG_LOGIN_MSG1","Contrase�a no v�lida");       
        define("A_LANG_LOGIN_MSG2","Correo electr�nico no v�lido");       
        define("A_LANG_LOGIN_MSG3","Usuario no autorizado");       
        define("A_LANG_LOGIN_MSG4","Espere a que la liberaci�n de su acceso por el administrador del ambiente AdaptWeb");       
        define("A_LANG_LOGIN_MSG5","No se puede conectar a la base de datos");       
                

        // Formul�rio de cadastro de Curso
        define("A_LANG_COURSE_REGISTER","Registro de Curso");       
        define("A_LANG_COURSE_RESGISTERED","Cursos registrados");  
        define("A_LANG_COURSE2","Curso");                      
        define("A_LANG_COURSE_INSERT","Agregar");
        define("A_LANG_COURSE_UPDATE","Cambiar");
        define("A_LANG_COURSE_DELETE","Eliminar");  
        define("A_LANG_COURSE_MSG1","No se puede conectar a la base de datos");  
	 define("A_LANG_COURSE_MSG2","No se puede agregar en el curso");  
	 define("A_LANG_COURSE_MSG3","No se puede cambiar en el curso"); 
	 define("A_LANG_COURSE_MSG4","No se puede eliminar en el curso. Para quitarlo, debe introducir la disciplina / Curso y quitar la selecci�n de este curso en todas las materias."); 
	 define("A_LANG_COURSE_MSG5","No es posible eliminar el curso"); 
	 // Incllus�o de mensagens para usabilidade - Carla -estagio UDESC-2008/02
	 define("A_LANG_COURSE_MSG6", "Curso registrado con �xito");
	 define("A_LANG_COURSE_MSG7","Curso eliminado con �xito");
	// Fim alteracoes Carla
 

        // Cadastro de disciplina
        define("A_LANG_DISCIPLINES_REGISTER","Registro de Disciplina");  
        define("A_LANG_DISCIPLINES_REGISTERED","Disciplinas registradas"); 
        define("A_LANG_DISCIPLINES2","Disciplina");    
        define("A_LANG_DISCIPLINES_INSERT","Agregar");
        define("A_LANG_DISCIPLINES_UPDATE","Cambiar");
        define("A_LANG_DISCIPLINES_DELETE","Eliminar");      
        define("A_LANG_DISCIPLINES_MSG1","No se puede Agregar en la disciplina"); 
        define("A_LANG_DISCIPLINES_MSG2","No se puede cambiar la disciplina"); 
        define("A_LANG_DISCIPLINES_MSG3","No se puede eliminar esta disciplina. Para quitarlo, debe introducir la disciplina / Curso y la selecci�n de todos los cursos mencionados."); 
        define("A_LANG_DISCIPLINES_MSG4","No es posible eliminar la disciplina"); 
        // Inclus�o de mensagens para usabilidade - Carla est�gio UDESC 2008/02
	 define("A_LANG_DISCIPLINES_MSG5","Ya hay una disciplina con este nombre");
	 define("A_LANG_DISCIPLINES_MSG6","Una disciplina no puede ser nombrado en blanco");
	 define("A_LANG_DISCIPLINES_MSG7","Para cambiar de un curso que debe seleccionar la incertidumbre y un nuevo nombre");
	 define("A_LANG_DISCIPLINES_MSG8","La disciplina se ha registrado con �xito");
	 // Final Carla


        // Formul�rio de cadastro de Disciplina / Curso
        define("A_LANG_DISCIPLINES_COURSE1","Disciplina/Curso");
        define("A_LANG_DISCIPLINES_COURSE_DISCIPLINES","Disciplinas");   
        define("A_LANG_DISCIPLINES_COURSE2","Cursos");      
        define("A_LANG_DISCIPLINES_COURSE_SAVE","Salvar");
        define("A_LANG_DISCIPLINES_COURSE_MSG1","Registre la (s) disciplina (S) y CURSO (S) para acceder a este tema.");
        define("A_LANG_DISCIPLINES_COURSE_MSG2","No se puede vincular la disciplina con los cursos debido a la falta de conexi�n a la base de datos.");
        define("A_LANG_DISCIPLINES_COURSE_MSG3","Introduzca el nombre del curso para incluir");
        define("A_LANG_DISCIPLINES_COURSE_MSG4","Curso existente");
        define("A_LANG_DISCIPLINES_COURSE_MSG5","Seleccione un curso para el cambio");
        // Inclus�o de mensagens para usabilidade - Carla est�gio UDESC 2008/02
	 define("A_LANG_DISCIPLINES_COURSE_MSG6","La relaci�n entre las Disciplinas y los cursos ha sido un �xito.");
	 define("A_LANG_DISCIPLINES_COURSE_MSG7","No hab�a seleccionado ninguno curso para esta  Disciplina");       
	 define("A_LANG_DISCIPLINES_COURSE_MSG8","Curso cambiado con �xito");
	 define("A_LANG_DISCIPLINES_COURSE_OBS","* Nota: Para crear la estructura de los contenidos de la disciplina que se necesita relaion� con por lo menos un curso");
	 //Final Carla

                
	// Formul�rio de autoriza��o de acesso
	 define("A_LANG_LIBERATE_USER1","Inscripci�n / Estudiante");   // para o professor e administrador
        define("A_LANG_LIBERATE_USER2","Permiten a los profesores / Autor"); // para o adiministrador
        define("A_LANG_LIBERATE_MSG1","	En desarrollo ... ");
        
        
        // Formul�rio para libera��o da disciplina 
	 define("A_LANG_LIBERATE_DISCIPLINES","Liberaci�n de Disciplina");
        define("A_LANG_LIBERATE_DISCIPLINES_MSG1","En desarrollo ... ");
	 // Inclus�o de mensagens para usabilidade - Carla est�gio UDESC 2008/02
	 define("A_LANG_TEXT_LIBERATE","A la libertad de la disciplina se establece que debe seleccionar en la lista \"La liberaci�n de Disciplina\" y pasar a la lista de \"Disciplinas Libertad\".");
	 define("A_LANG_TEXT_LIBERATE_MAT","Para realizar la inscripci�n sea solicitada por el estudiante debe seleccionar en la \"Lista de los Estudiantes\" y pasar a la \"Lista de estudiantes inscritos\"."); //carla incluiu para liberar matricula de aluno
	 // Final Carla
                                                          
        // ======================= Topicos ==========================
                        
        // Entrada / t�picos
        define("A_LANG_ENTRANCE_TOPICS_DISCIPLINES","Disciplina");
        // **** REVER "Organizador Tema"
        define("A_LANG_ENTRANCE_TOPICS_STRUTURALIZE","Organizar T�pico");
        define("A_LANG_ENTRANCE_TOPICS_MSG1","No se pudo leer las disciplinas");
        define("A_LANG_ENTRANCE_TOPICS_MSG2","Nota: S�lo disponible en esta forma los temas que est�n relacionados con por lo menos un curso.");
        

       // Guia - Lista de t�picos
        define("A_LANG_TOPIC_GENERATE_CONTENT","Generar contenido");       
        //orelha
        define("A_LANG_TOPIC_LIST","Conceptos");     
        define("A_LANG_TOPIC_MAINTENANCE","Mantenimiento de Concepto");  
        define("A_LANG_TOPIC_EXEMPLES","Ejemplos");  
        define("A_LANG_TOPIC_EXERCISES","Ejercicios");  
        define("A_LANG_TOPIC_COMPLEMENTARY","Material complementario");  
	  //Inclusao de mensagens de Usabilidade - Carla -estagio UDESC 2008/02
	   define("A_LANG_TOPIC_OBS","<b>* Nota: </b> Para <u> Creaci�n de Contenido </u> en la p�gina de inicio de los conceptos que usted debe llenar correctamente todos los campos de esta p�gina. <br>
El archivo se carga debe ser <b>. Html </b>, en caso de cualquier imagen de este archivo, tiene que cargar la imagen. El archivo html <br> puede ser sustituido por subir otro archivo. Html.  ");
			//Carla inseriu observa��o em pagina q cria topicos
	   define("A_LANG_TOPIC_ARQ_OBS","* Nota: </b> Extensi�n del archivo html (por ejemplo, capitulo1.html)");//Carla inseriu observa��o para tipo de arquivo
	   define("A_LANG_TOPIC_GRAVAR_MSG1","Datos guardados con �xito. Si se realiza alg�n cambio en esta p�gina usted debe escribir de nuevo.");
	 // Final Carla


        
        // Guia - manuten��o do t�pico
  	 define("A_LANG_TOPIC_NUMBER","N�mero del Concepto");
	 define("A_LANG_TOPIC_DESCRIPTION","Nombre del Concepto");
	 define("A_LANG_TOPIC_SUMARIZED_TOPIC_DESCRIPTION","Breve descripci�n del concepto");  
	 define("A_LANG_TOPIC_WORDS_KEY","Palabras clave");   
  	 define("A_LANG_TOPIC_PREREQUISIT","Pre-requisito");
	 define("A_LANG_TOPIC_COURSE","Curso");
	 define("A_LANG_TOPIC_FILE1","Archivo");
	 define("A_LANG_TOPIC_FILE2","Archivo(s)");
	 define("A_LANG_TOPIC_MAIN_FILE","Archivo principal"); 
	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE","Archivo asociado");
	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_STATUS","Ubicaci�n");
	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_UPLOAD",""); 
        define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG1","cargando para el servidor"); 
  	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG2","cargando para el servidor"); 

       // bot�es
        define("A_LANG_TOPIC_SAVE","Salvar");  
        define("A_LANG_TOPIC_SEND","Enviar"); 
        define("A_LANG_TOPIC_SEARCH","B�squeda");         
	 define("A_LANG_TOPIC_INSERT_TOPIC_SAME_LEVEL","Insertar el mismo concepto a nivel");  
        define("A_LANG_TOPIC_INSERT_SUB_TOPIC","Insertar sub-concepto");  
	 define("A_LANG_TOPIC_DELETE","Eliminar");  
	 define("A_LANG_TOPIC_PROMOTE","Promover"); 
	 define("A_LANG_TOPIC_LOWER","Degradar"); 
	 // Inclusao de mesanges de Usabilidade - Carla - estagio UDESC 2008/02
	 define("A_LANG_TOPIC_INSERT_MSG1","Rellene todos los campos");//carla inseriu para colocar mensagem para usuario ao criar um topico
	 define("A_LANG_TOPIC_INSERT_MSG2","Rellene el campo Nombre");//carla inseriu para colocar mensagem para usuario ao criar um topico
	 define("A_LANG_TOPIC_INSERT_MSG3","Rellene el campo Descripci�n");//carla inseriu para colocar mensagem para usuario ao criar um topico
	 define("A_LANG_TOPIC_INSERT_MSG4","Rellene el campo palabras clave");//carla inseriu para colocar mensagem para usuario ao criar um t�pico
	 define("A_LANG_TOPIC_OBS2","<b> * Nota: </b> Todos los campos son obligatorios.");
	// Final Carla


	// Guia - samples
	//define("A_LANG_TOPIC_EXEMPLES_NUMBER_TOPIC","N�mero del Tema");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Descripci�n del ejemplo");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Nivel de Complejidad");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Curso");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Eliminar");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Descripci�n");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Nivel");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Curso");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Archivo");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","archivo asociado");
	// bot�es
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Salvar");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Eliminar");


  // Exemplos
  // N�vel de Complexidade
// Descri��o
// N�vel
// Arquivos Associados
        // Descri��o dos Exemplos
  // Exerc�cios
        // Descri��o do exerc�cio
  // Material complementar
        // Menu
// Autorizar Acesso
// Liberar disciplinas
// Novo usu�rio
        // to modify I register in cadastre (alterar cadastro)
        // ************************************************************************ // * Formul�rio - Autoria
  // ************************************************************************ // Disciplinas Liberadas

// Matricula
	// Inclus�o de mesagens de usabilidade (para matricula) Carla - estagio UDESC 2008/02
	   define("A_LANG_LENVIRONMENT_REQUEST_SUCCESS","Solicitud de �xito. Espere a que la publicaci�n de su registro por el profesor");
	// Final Carla

// Aguardando Matricula
       // Inclus�o de mesagens de usabilidade (para aguardando matricula) Carla - estagio UDESC 2008/02
	   define("A_LANG_NO_DISC_RELEASED","No hay temas em liberdad");  
	// Final Carla


        // Autorizar Acesso
        
        // Navega��o
               
        // Tela de Cursos    
        // ************************************************************************ // * Formul�rio - Demo
  // ************************************************************************
  define("A_LANG_DEMO","Disciplina para demonstra��o do Ambiente AdaptWeb - n�o permite inser��es e altera��es");
//
// Regional Specific Date texts
//
// A little help for date manipulation, from PHP manual on function strftime():
//
// %a - abbreviated weekday name according to the current locale
// %A - full weekday name according to the current locale
// %b - abbreviated month name according to the current locale
// %B - full month name according to the current locale
// %c - preferred date and time representation for the current locale
// %C - century number (the year divided by 100 and truncated to an integer, range 00 to 99)
// %d - day of the month as a decimal number (range 01 to 31)
// %D - same as %m/%d/%y
// %e - day of the month as a decimal number, a single digit is preceded by a space
//      (range ' 1' to '31')
// %h - same as %b
// %H - hour as a decimal number using a 24-hour clock (range 00 to 23)
// %I - hour as a decimal number using a 12-hour clock (range 01 to 12)
// %j - day of the year as a decimal number (range 001 to 366)
// %m - month as a decimal number (range 01 to 12)
// %M - minute as a decimal number
// %n - newline character
// %p - either `am' or `pm' according to the given time value, or the corresponding strings for
//      the current locale
// %r - time in a.m. and p.m. notation
// %R - time in 24 hour notation
// %S - second as a decimal number
// %t - tab character
// %T - current time, equal to %H:%M:%S
// %u - weekday as a decimal number [1,7], with 1 representing Monday
// %U - week number of the current year as a decimal number, starting with the first Sunday as
//      the first day of the first week
// %V - The ISO 8601:1988 week number of the current year as a decimal number, range 01 to 53,
//      where week 1 is the first week that has at least 4 days in the current year, and with
//      Monday as the first day of the week.
// %W - week number of the current year as a decimal number, starting with the first Monday as
//      the first day of the first week
// %w - day of the week as a decimal, Sunday being 0
// %x - preferred date representation for the current locale without the time
// %X - preferred time representation for the current locale without the date
// %y - year as a decimal number without a century (range 00 to 99)
// %Y - year as a decimal number including the century
// %Z - time zone or name or abbreviation
// %% - a literal `%' character
//
// Note: A_LANG_DATESTRING is used for Articles and Comments Date
//       A_LANG_LINKSDATESTRING is used for Web Links creation Date
//       A_LANG_DATESTRING2 is used for Older Articles box on Home
//
  
define("A_LANG_CODE","pt-BR");
define("A_LANG_CHACTERSET","ISO-8859-1");

define("A_LANG_NAME_pt_BR","Portugu�s del Brasil");  
define("A_LANG_NAME_en_US","Ingl�s");  
define("A_LANG_NAME_es_ES","Espa�ol");  
define("A_LANG_NAME_fr_FR","Franc�s");  
define("A_LANG_NAME_ja_JP","Japon�s");

define('A_LANG_TIMEZONES','IDLW NT HST YST PST MST CST EST AST GMT-3:30 GMT-3 AT WAT GMT CET EET BT GMT+3:30 GMT+4 GMT+4:30 GMT+5 GMT+5:30 GMT+6 WAST CCT JST ACS GST GMT+11 NZST'); 
define('A_LANG_TZOFFSETS','0 1 2 3 4 5 6 7 8 8.5 9 10 11 12 13 14 15 15.5 16 16.5 17 17.5 18 19 20 21 21.5 22 23 24'); 
define('A_LANG_DATEBRIEF','%b %d, %Y');
define('A_LANG_DATELONG','%A, %B %d, %Y'); 
define('A_LANG_DATESTRING','%A, %B %d @ %H:%M:%S %Z'); 

define('A_LANG_DATESTRING2','%A, %B %d');
define('A_LANG_DATETIMEBRIEF','%b %d, %Y - %I:%M %p');
define('A_LANG_DATETIMELONG','%A, %B %d, %Y - %I:%M %p %Z'); 

define('A_LANG_LINKSDATESTRING','%d-%b-%Y');
define('A_LANG_TIMEBRIEF','%I:%M %p');
define('A_LANG_TIMELONG','%I:%M %p %Z');

define('A_LANG_DAY_OF_WEEK_LONG','Domingo Lunes Martes Mi�rcoles Jueves Viernes S�bado');
define('A_LANG_DAY_OF_WEEK_SHORT','Dom Lun Mar Mie Jue Vie S�b');
define('A_LANG_MONTH_LONG','Enero Febrero Marzo Abril Mayo Junio Julio Agosto Septiembre Octubre Noviembre Diciembre');
define('A_LANG_MONTH_SHORT','Ene Feb Mar Abr May Jun Jul Ago Sep Oct Nov Dec');
  
//************UFRGS******************
	//Index
	  //About
	// alterado por Carladefine('A_LANG_INDEX_ABOUT_INTRO','Medio Ambiente AdaptWeb es devuelto a los autores y la presentaci�n de adaptaci�n de las disciplinas de los cursos de educaci�n abierta ya distancia en la web AdaptWeb El objetivo es permitir la adecuaci�n de las t�cticas y formas de presentar los contenidos a los estudiantes en diversos cursos y con diferentes estilos de aprendizaje, proporcionar diferentes formas de presentaci�n de cada contenido en una manera apropiada para cada curso y las preferencias individuales de los estudiantes participantes. <br> Medio Ambiente AdaptWeb fue inicialmente desarrollado por investigadores de la UFRGS y la UEL, a trav�s de proyectos y AdaptWeb Electra, con el apoyo de CNPq. Visita la p�gina del proyecto: <a href=\'http://www.inf.ufrgs.br/adapt/adaptweb\' target=\'_blank\'> http://www.inf.ufrgs.br/adapt / adaptweb </a>');
	// Alteracao mensagem Carla estagio UDESC 2008/02
	define('A_LANG_INDEX_ABOUT_INTRO','Ambiente AdaptWeb es devuelto a los autores y la presentaci�n de adaptaci�n de las disciplinas de los cursos de educaci�n abierta ya distancia en la web AdaptWeb El objetivo es permitir la adecuaci�n de las t�cticas y formas de presentar los contenidos a los estudiantes en diversos cursos y con diferentes estilos de aprendizaje, que permite diferentes formas de presentaci�n de cada contenido en una manera apropiada para cada curso y las preferencias individuales de los estudiantes participantes. <br> Ambiente AdaptWeb fue inicialmente desarrollado por investigadores de la UFRGS y la UEL, a trav�s de proyectos y AdaptWeb Electra, con el apoyo del CNPq. A partir de 2006, los investigadores comenzaron a UDESC tambi�n contribuir al desarrollo y mejoras en AdaptWeb. <br> Visite la p�gina de la SourceForge AdaptWeb: <a href=\'http://adaptweb.sourceforge.net\' target=\'_blank\'> http://adaptweb.sourceforge.net </a>');
	define('A_LANG_INDEX_ABOUT_ENVACCESS','Acceso al ambiente');
	define('A_LANG_INDEX_ABOUT_TINFO','Para proporcionar el contenido de sus temas el profesor debe hacer una solicitud de registro de acceso para el medio ambiente y permitir la liberaci�n de Autor Administrador');
	define('A_LANG_INDEX_ABOUT_LINFO','Para acceder a la (s) tema (s) el estudiante debe hacer el medio ambiente en la navegaci�n y la solicitud de registro (s) tema (s) relacionado con su curso');
	  //Demo
	define('A_LANG_INDEX_DEMO_AENVIRONMENT','Ambiente de Autor');
	define('A_LANG_INDEX_DEMO_FDESC1', 'Crear disciplina');
	define('A_LANG_INDEX_DEMO_NENVIRNMENT','Ambiente de Navegaci�n');
	define('A_LANG_INDEX_DEMO_FDESC2', 'Ver la disciplina');
	define('A_LANG_INDEX_DEMO_TOLOG','Acceso y navegar por el ambiente mediante la demostraci�n de la disciplina de la tala sobre el ambiente con los datos a continuaci�n:');
	define('A_LANG_INDEX_DEMO_LOGIN','login: demo@inf.ufrgs.br');
	define('A_LANG_INDEX_DEMO_PASS', 'contrase�a: 123');
	define('A_LANG_INDEX_DEMO_OBSERVATION','Nota');
	define('A_LANG_INDEX_DEMO_OBSERVATIONTEXT', 'despu�s de la tala en el acceso se dar� a conocer en el ambiente del profesor y el estudiante de ambiente por las opciones "Ambiente de la maestra y el estudiante el ambiente" en la pantalla inicial AdaptWeb.');
	  //Apresenta��o
	define('A_LANG_INDEX_PRESENTATION_INFABOUT', 'Informaci�n sobre el proyecto:');
	define('A_LANG_DOWNLOAD', 'Download');
	define('A_LANG_PUBLICATIONS', 'Publicaciones');
	define('A_LANG_INDEX_PRESENTATION_AVAILABLEAT','Disponible en:');
	

	
	//Ambiente do Aluno - Entrada
	  //Principal
	define('A_LANG_LENVIRONMENT','Ambiente del estudiante');
	define('A_LANG_LENVIRONMENT_DESCRIPTION', 'Este Ambiente permite al estudiante el acceso a material did�ctico para que usted (s) maestro (s) de adaptarse a su perfil.');
	define('A_LANG_LENVIRONMENT_WARNING', 'de acceso (s) de su (s) de la disciplina, el estudiante debe hacer su inscripci�n con el fin de ingresar en el ambiente a la solicitud de registro de su (s) tema (s). S�lo despu�s de la liberaci�n del acceso de los responsables docentes que tendr�n acceso a la misma disciplina.');
	
	define('A_LANG_LENVIRONMENT_WATCH_LIBERATED', 'Cursos liberados');
	define('A_LANG_LENVIRONMENT_WATCH_MINE', 'Disciplinas de mi propia autoria (navegar por supuesto)');
	define('A_LANG_LENVIRONMENT_WATCH_MY', 'Mis Disciplinas');
	define('A_LANG_LENVIRONMENT_WATCH_NOT', 'No hay temas inscritos');
	define('A_LANG_LENVIRONMENT_WATCH_WARNING', 'Solicitud de registro para el acceso a las disciplinas.');
	define('A_LANG_LENVIRONMENT_WATCH_VISUAL', 'Mostrar la disciplina de cada curso');
	define('A_LANG_LENVIRONMENT_WATCH_CREATE', 'Crear y gestionar contenido de las disciplinas (Generar Contenido) Profesor Ambiente (tema <b> Estructuraci�n Contenido => Lista de Conceptos </b>) para acceder a la navegaci�n del ambiente.');
	define('A_LANG_LENVIRONMENT_WATCH_OBS', 'para ver las disciplinas de su propia autor no es necesario poner a prueba la disciplina de lanzamiento de la navegaci�n en el ambiente.');
	define('A_LANG_LENVIRONMENT_REQUEST', 'Solicitud de Inscripci�n');
	define('A_LANG_LENVIRONMENT_REQUEST_REQ', 'Solicitar');
	define('A_LANG_LENVIRONMENT_REQUEST_NOTC', 'No hay cursos registrados');
	define('A_LANG_LENVIRONMENT_REQUEST_NOTD', 'no hay ninguna disciplina cadastrada para el curso selecionado');
	define('A_LANG_LENVIRONMENT_REQUEST_SELECT', 'Seleccione para solicitar la inscripci�n:');
	define('A_LANG_LENVIRONMENT_REQUEST_MATR', 'Registrado');
	define('A_LANG_LENVIRONMENT_WAITING', 'esperando la liberaci�n del profesor');
	define('A_LANG_LENVIRONMENT_WAITING_NOT', 'No hay ninguna solicitudes de registro');
	define('A_LANG_LENVIRONMENT_WAITING_WARNING', 'Solicitud de registro para el acceso a las disciplinas');
	define('A_LANG_LENVIRONMENT_NAVTYPE', 'Tipo de navegaci�n');
	define('A_LANG_LENVIRONMENT_FORCOURSE', 'para el curso de');
	define('A_LANG_LENVIRONMENT_NETCONECTION', 'Conexi�n de red');
	define('A_LANG_LENVIRONMENT_SPEED', 'La velocidad es');
	define('A_LANG_LENVIRONMENT_NAVIGATION', 'Navegaci�n');
	define('A_LANG_LENVIRONMENT_TUTORIAL', 'Tutorial');
	define('A_LANG_LENVIRONMENT_FREE', 'Libre');
	// Inclus�o de mesagens de Usabilidade - Carla estagio UDESC 2008/02
	define('A_LANG_LENVIRONMENT_TUTORIAL_DESC', 'Con ayuda de la navegaci�n por los requisitos establecidos por el profesor.'); // carla
       define('A_LANG_LENVIRONMENT_FREE_DESC', 'Navegaci�n est� abierta a todos los conceptos (sin tener en cuenta las sugerencias del profesor).');//carla
       define('A_LANG_LENVIRONMENT_PLUS', 'Obtenga m�s informaci�n acerca de los tipos de navegaci�n');//CARLA
	define('A_LANG_LENVIRONMENT_NAVIGATION_EXPLAIN', 'En la actualidad, el  Ambiente Adaptweb ofrece dos modos de navegaci�n para una disciplina (y Tutoriales Gratis). La diferencia entre ellos es como la navegaci�n por la disciplina. Pero el mismo contenido se preparan para los dos modos de navegaci�n.'); //Carla- explicacao no modo de navegacao - a_navega e n_navega
	// Final Carla
	
	//Ambiente do professor
	define('A_LANG_TENVIRONMENT_DESCRIPTION', 'El m�dulo permite a la autora al autor ofrecer contenido adaptado a diferentes perfiles de los estudiantes. A trav�s del proceso de autor�a, el autor puede proporcionar el contenido de sus clases en una sola estructura adaptada a los diferentes cursos. En esta fase el autor debe inscribirse en la disciplina y los cursos que desea que est� disponible. Tras el registro insertar el concepto de los contenidos de archivos, relativos a cada tema de la disciplina. Adem�s, el autor deber� informar a la descripci�n del tema, y sus cursos de pre-requisito que quiere dar contenido. El autor puede insertar ejercicios, ejemplos y materiales adicionales para cada tema.');
	define('A_LANG_TENVIRONMENT_AUTHOR', 'Autor');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR1', 'Se ha producido un error cuando se inscriba en esta disciplina. Este usuario debe estar ya registrado o se ha producido un error en el servidor');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR2', 'Se produjo un error al eliminar el registro del usuario en este curso. Este usuario debe estar registrado o no se ha producido un error en el servidor');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_NOT', 'No hay ninguno curso registrado');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECT', 'Seleccione el curso');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_NOTD', 'no hay disciplina registrado en este curso');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECTD', 'Seleccione la disciplina');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_LIST', 'Lista de los Estudiantes:');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_MATR', 'Estudiantes registrados:');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_BACK', 'Volver');
	define('A_LANG_TENVIRONMENT_LIBERATE_DESCRIPTION', '<b>Observa��o: </b> Somente estar�o dispon�veis para serem liberadas as disciplinas que estiverem com conte�do correto. Para verificar o conte�do das disciplinas acesse a guia <b>CONCEITO</b> no item <b>ESTRUTURAR CONTE�DO</b> e use o bot�o <b>GERAR CONTE�DO</b>.');
	define('A_LANG_TENVIRONMENT_LIBERATE_LIB', 'Liberar<br> disciplina:');
	define('A_LANG_TENVIRONMENT_LIBERATE_LIB_DIS', 'Disciplinas <br> liberadas:');
	define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE', 'Mover Concepto');
	define('A_LANG_TENVIRONMENT_ESTRUCT_EXCLUDE', 'Eliminar Concepto');
	define('A_LANG_TENVIRONMENT_ESTRUCT_SELECT', 'Seleccione el concepto que desea <b> Mover </b> o <b> Eliminar </b>');
	define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE_AFTER', '<b> Movimiento </b> concepto seleccionado despu�s de que concepto?');
	define('A_LANG_TENVIRONMENT_ESTRUCT_CANCEL', 'para mover o eliminar concepto operaci�n dar� lugar a la cancelaci�n de los requisitos previos');
	define('A_LANG_TENVIRONMENT_ESTRUCT_NOT_POSSIBLE', 'No es posible leer la matriz de la base de datos');	
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND', 'Clave pre-requisitos:');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND1', 'Para seleccionar varios requisitos antes de utilizar la tecla CTRL');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND2', 'Para retirar (s) de pre-requisito (s) de selecci�n utilice CTRL');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND3', 'Los requisitos en <font class=buttonselecionado>azul oscuro</font> est�o selecionados ');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND4', 'El fondo  class=buttonpre> <font> azul </font> indica el pre-requisitos (en caso de perder la selecci�n)');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND5', 'El fondo <font class=buttonpreautomatico>amarillo</font> indica los prerequisitos actuales automaticos');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NUMBER', 'N�mero del concepto:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NAME', 'Nombre de ejemplo:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_DESCRIPTION', 'Descripci�n del ejemplo:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_KEY', 'Palabras clave en el ejemplo:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_LEVEL', 'Nivel de Complejidad:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NOCLASS', 'Sin Classificaci�n');
	define('A_LANG_TENVIRONMENT_EXAMPLES_EASY', 'F�cil');
	define('A_LANG_TENVIRONMENT_EXAMPLES_MEDIUM', 'Medio');
	define('A_LANG_TENVIRONMENT_EXAMPLES_COMPLEX', 'Complejo');
	define('A_LANG_TENVIRONMENT_EXAMPLES_DSC', 'Descrici�n');
	define('A_LANG_TENVIRONMENT_EXAMPLES_LVL', 'N�vel');
	define('A_LANG_TENVIRONMENT_EXERCISES_NAME', 'Nombre del ejercicio:');
	define('A_LANG_TENVIRONMENT_EXERCISES_DESCRIPTION', 'Descripci�n de la Oficina:');
	define('A_LANG_TENVIRONMENT_EXERCISES_KEY', 'Palabras clave del ejercicio:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_NAME', 'Palabras clave suplementario:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_DESCRIPTION', 'Descripci�n de <br> material suplementario:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_KEY', 'Palabras clave <br> de material: suplementario');
	define('A_LANG_TENVIRONMENT_ACCESS', 'Acceso al Ambiente de Autor');
	define('A_LANG_TENVIRONMENT_WAIT', 'Espere a que el administrador del ambiente libre acceso para el ambiente de autor�a.');
	define('A_LANG_TENVIRONMENT_COMMENT', 'Acceso al ambiente de autor�a est� gestionada por el administrador del ambiente AdaptWeb. Una vez que usted se registre en el ambiente es necesaria para permitir al administrador para acceder al entorno de autor�a.');
	define('A_LANG_TENVIRONMENT_DEMO', 'Demostraci�n de Ambientes');
	define('A_LANG_TENVIRONMENT_ASSOCIATED_FILES','Nota: Los archivos asociados deben ser enviados al servidor a trav�s del "mantenimiento del concepto."');
	// Inclus�o de mensagens usabilidade - Carla estagio UDESC 2008/02
       define("A_LANG_TENVIRONMENT_EXAMPLES_OBS","<b> * Nota: </b> Proporcionar a la disciplina es necesario rellenar todos los campos de esta p�gina correctamente.");//Carla inseriu para colocar uma observa��o na aba de exemplos.
       define("A_LANG_TENVIRONMENT_EXERC","Para crear uno o m�s ejercicios que se necesita para llenar todos los campos y subir los archivos de <b>. Html </b> (por ejemplo, exerc1.html, exerc2.html). Si este archivo tiene la imagen, es necesario cargar el mismo.");//Carla inseriu para colocar uma introdu��o na aba de material complementar");//carla
       define("A_LANG_TENVIRONMENT_EXAMPLES","Para crear uno o m�s ejemplos que deben rellenar todos los campos y subir los archivos de <b>. Html </b> (por ejemplo, exemplo1.html, exemplo2.html). Si este archivo tiene la imagen, es necesario cargar el mismo.");//Carla inseriu para colocar uma introdu��o na aba de exemplos.
       define("A_LANG_TENVIRONMENT_COMPLEMENTARY_DESC","Para crear uno o m�s de material adicional que se necesita para llenar todos los campos y subir los archivos de <b>. Html </b> (por ejemplo, mat1.html, mat2.html). Si este archivo tiene la imagen, es necesario cargar el mismo.");//Carla inseriu para colocar uma introdu��o na aba de material complementar.
	// Final Carla


	//Op��es de Root
	define('A_LANG_ROOT_NOT_AUTH', 'Los profesores no <br> autorizados');
	define('A_LANG_ROOT_AUTH', 'Profesores<br> autorizados:');
	define('A_LANG_ROOT_DEV', 'Esta opci�n se encuentra en fase de desarrollo.');

	//Outros
	define('A_LANG_DATA', 'Fecha');
	define('A_LANG_WARNING', 'Advertencia');
	define('A_LANG_COORDINATOR', 'COORDINADOR');
	define('A_LANG_DATABASE_PROBLEM', 'Hay problemas con la base de datos');
	define('A_LANG_FAQ', 'Preguntas m�s frecuentes');
	define('A_LANG_WORKS', 'trabajos');
	define('A_LANG_NOT_DISC', 'No se puede ver las Disciplinas inscritas');
	define('A_LANG_NO_ROOT', 'No se puede insertar el usuario root');
	define('A_LANG_MIN_VERSION', 'La versi�n m�nima de la base de datos [');
	define('A_LANG_INSTALLED_VERSION', '] Y la versi�n instalada es [');
	define('A_LANG_ERROR_BD', 'No se puede escribir la matriz en la base de datos');
	define('A_LANG_DISC_NOT_FOUND', 'Nombre del Curso no se encuentra o est�s en blanco');
	define('A_LANG_SELECT_COURSE', 'seleccionar el curso en el concepto <b>');
	define('A_LANG_TOPIC_REGISTER', 'Registro de Concepts');
	define('A_LANG_SUMARIZED_DESCRIPTION', 'Breve descripci�n <br> del concepto ');
	define('A_LANG_LOADED', 'cargado para el servidor');
	define('A_LANG_FILL_DESCRIPTION', 'Introducir en el campo de descripci�n de los materiales');
	define('A_LANG_WORKS_SEND_DATE', 'Fecha de transmisi�n');
	define('A_LANG_SEARCH_RESULTS', 'Resultados de la b�squeda');
	define('A_LANG_SEARCH_SYSTEM','Sistema de B�squeda');
	define('A_LANG_SEARCH_WORD', 'Palabra de la b�squeda');
	define('A_LANG_SEARCH_FOUND', 'Se encontr� ');
	define('A_LANG_SEARCH_OCCURRANCES', 'casos de ');
	define('A_LANG_SEARCH_IN_TOPIC', ' en concepto');
	define('A_LANG_SEARCH_FOUND1', 'Se encontr�');
	define('A_LANG_SEARCH_OCCURRANCE', ' casos de ');
	define('A_LANG_SEARCH_NOT_FOUND', 'No hubo casos de ');
	define('A_LANG_SEARCH_CLOSE', 'Cerrar');
	define('A_LANG_TOPIC', 'Concepto');
	define('A_LANG_SHOW_MENU', 'Ver Men�');
	define('A_LANG_CONFIG', 'Configuraci�n');
	define('A_LANG_MAP', 'Mapa');
	define('A_LANG_PRINT', 'Imprimir esta p�gina?');
	define('A_LANG_CONNECTION', 'No es posible conectar con el servidor de base de datos MySQL por favor p�ngase en contacto con el administrador.');
	define('A_LANG_CONTACT', 'Por favor, p�ngase en contacto con el administrador');
	define('A_LANG_HELP_SYSTEM', 'Sistema de ayuda');
	define('A_LANG_CONFIG_SYSTEM', 'Configuraci�n del sistema');
	define('A_LANG_CONFIG_COLOR', 'Establecer color de fondo');
	define('A_LANG_NOT_CONNECTED', 'No conectado!');
	define('A_LANG_DELETE_CADASTRE', 'Exclusi�n de Registro de estudiantes');
	define('A_LANG_HOME', 'Home');
	define('A_LANG_YOU_HAVE', 'Usted tiene');
	define('A_LANG_TO_CONFIRM', 'registro. Para confirmar esta exclusi�n haga clic en ');
	define('A_LANG_CONFIRM', ' CONFIRMAR');
	define('A_LANG_EXAMPLE_LIST', 'Lista de ejemplos');
	define('A_LANG_COMPLEXITY_LEVEL_EASY', 'Complejidad de N�vel F�cil');
	define('A_LANG_COMPLEXITY_LEVEL_MEDIUM', 'Complejidad de Nivel Medio');
	define('A_LANG_COMPLEXITY_LEVEL_COMPLEX', 'Nivel de Complejidad Dif�cil');
	define('A_LANG_EXERCISES_LIST', 'Lista de Ejercicios');
	define('A_LANG_REGISTER_MAINTENENCE', 'Mantenimiento de Registros');
	define('A_LANG_REGISTER_RELEASE', 'Liberaci�n de Inscripci�n');
	define('A_LANG_SITE_MAP', 'Mapa del sitio');
	define('A_LANG_COMPLEMENTARY_LIST', 'Lista de material');
	define('A_LANG_DELETE_TOPIC', 'No el concepto');
	define('A_LANG_AND', ' Y ');
	define('A_LANG_SONS', ' sub-concepto');
	define('A_LANG_CONFIRM_EXCLUSION', '�Est�s seguro de que desea eliminar la disciplina?');
	define('A_LANG_YES', 'S�  ');
	define('A_LANG_NO', '  No   ');
	// Inclusao mensagens usabilidade - Carla estagio UDESC 2008/02
	 define('A_LANG_CONFIRM_EXCLUSION_CURSO', '�Est�s seguro de que desea eliminar el curso?');
	// Final Carla

	
	//Gera��o de conte�do
	define('A_LANG_GENERATION_END', 'Fin de la generaci�n de archivos XML de los elementos del Tema');
	define('A_LANG_GENERATION_CREATED', 'Se crearon ');
	define('A_LANG_GENERATION_XML', ' arqchivos xml.');
	define('A_LANG_GENERATION_END_STRUCT', 'Fin de la generaci�n de la estructura XML de Temas');
/*	define('A_LANG_GENERATION_MISSING_FILE', 'Deves Incluir archivo de concepto do ');
	define('A_LANG_GENERATION_TH_TOPIC', 'concepto ');
	define('A_LANG_GENERATION_MISSING_COURSE', 'Inserte el curso que falta ');
	define('A_LANG_GENERATION_NO_ASOC', 'Concepto sin curso associado');
	define('A_LANG_GENERATION_MISSING_EXERC_ID', 'Usted deves incluir identificador del ejerc�cio ');
	define('A_LANG_GENERATION_TH_EXERCISE', '� ejerc�cio del ');
	define('A_LANG_GENERATION_MISSING_EXERC_DESCRIPTION', 'Usted deves incluir descrici�n de ejercicio del ');
	define('A_LANG_GENERATION_MISSING_EXERC_FILE', 'Usted deves incluir archivo de ejercicio del ');
	define('A_LANG_GENERATION_MISSING_EXERC_COMPLEXITY', 'Usted deves incluir la complejidad del ejercicio del ');
	define('A_LANG_GENERATION_TH_EXAMPLE', '�ejemplo de ');
	define('A_LANG_GENERATION_MISSING_EXAMP_ID', 'Usted deves incluir el identificador de ejemplo del ');
	define('A_LANG_GENERATION_MISSING_EXAMP_DESCRIPTION', 'Usted deves incluir la descrici�n de ejemplo del ');
	define('A_LANG_GENERATION_MISSING_EXAMP_FILE', 'Usted deves incluir el archivo de ejemplo del ');
	define('A_LANG_GENERATION_MISSING_EXAMP_COMPLEXITY' ,'Usted deves incluir la complejidad de ejemplo del ');
	define('A_LANG_GENERATION_TH_MATCOMP', '� Material complementario del ');
	define('A_LANG_GENERATION_MISSING_MATCO_ID', 'Usted deves incluir el identificador de material complementario del ');
	define('A_LANG_GENERATION_MISSING_MATCO_DESCRIPTION', 'Usted deves incluir descrici�n del material complementario del ');
	define('A_LANG_GENERATION_MISSING_MATCO_FILE', 'Usted deves incluir un archivo de material complementario del ');
*/	define('A_LANG_GENERATION_VERIFY', 'Comprobar los datos de los autores');
	define('A_LANG_GENERATION_PROBLEMS', 'Problemas con el registro de la disciplina');
/*	define('A_LANG_GENERATION_MISSING_TOPIC_NUMBER', 'Falta el n�mero de temas para el ');
	define('A_LANG_GENERATION_MISSING_TOPIC_DESCRIPTION', 'Usted deves incluir la Descrici�n del Tema para el ');
	define('A_LANG_GENERATION_MISSING_TOPIC_ABREV', 'Usted deves incluir la Abreviatura del tema para el ');
	define('A_LANG_GENERATION_MISSING_TOPIC_KEYWORD', 'Usted deves incluir Palabras clave para el tema ');
*/	define('A_LANG_GENERATION_RESULT_XML', 'Resultado de la generaci�n de XML');
	define('A_LANG_GENERATION_NO_ERROR', 'NO HAY ERROR');
	define('A_LANG_GENERATION_SUCCESS', 'ARCHVOS DEL CURSO GENERADOS CON �XITO');
	define('A_LANG_GENERATION_DATA_LACK', 'Archivos XML no se genera por la falta de datos');
	define ('A_LANG_GENERATION_TOPIC','Concepto');	
	define ('A_LANG_GENERATION_EXAMPLE','Ejemplo');	
	define ('A_LANG_GENERATION_EXERCISE','Ejerc�cio');	
	define ('A_LANG_GENERATION_COMPLEMENTARY','Material Complementario');	
	define ('A_LANG_GENERATION_TOPIC_NUMBER','N�mero de Tema');	
	define ('A_LANG_GENERATION_TOPIC_NAME','Nombre del Tema');	
	define ('A_LANG_GENERATION_DESCRIPTION','Descrici�n');	
	define ('A_LANG_GENERATION_KEY_WORD','Palavra clave');	
	define ('A_LANG_GENERATION_COURSE','Curso');	
	define ('A_LANG_GENERATION_PRINC_FILE','Archivo Principal');	
	define ('A_LANG_GENERATION_ASSOC_FILE','Archivos Associados');	
	define ('A_LANG_GENERATION_MISSING','Ausente');	
	define ('A_LANG_GENERATION_NO_PROBLEM','Ok');	
	define ('A_LANG_GENERATION_NUMBER','N�mero');	
	define ('A_LANG_GENERATION_COMPLEXITY','N�vel');	
	define ('A_LANG_GENERATION_NOT_EXIST','No se ha');	

	//Reposit�rio
	define ('A_LANG_MNU_REPOSITORY','Repositorio');	
	define ('A_LANG_PRESENTATION','Presentaci�n');
	define ('A_LANG_EXPORT','Exportaci�n de Disciplinas');
	define ('A_LANG_IMPORT','Importaciones de Disciplinas');
	define ('A_LANG_OBJECT_SEARCH','B�squeda de Objetos de Aprendizaje');
	define ('A_LANG_EXP_TEXT', 'Env�e sus disciplinas en el repositorio para que otros profesores puedan utilizarlo tambi�n.');
	define ('A_LANG_IMP_TEXT', 'Utilice las disciplinas disponibles en ambiente AdaptWeb.');
	define ('A_LANG_SCH_TEXT', 'Los datos de las disciplinas del AdaptWeb tambi�n se indexan en el formato de objetos de aprendizaje utilizando el est�ndar de metadatos LOM (Metadatos de objetos de aprendizaje). B�squeda de objetos de aprendizaje que abarcan temas, los temas (conceptos) y el repositorio de archivos.');
	define ('A_LANG_REP_TEXT', 'Bienvenido al repositorio de las Disciplinas del AdaptWeb. <br> A trav�s de este repositorio, los usuarios de diferentes lugares pueden compartir AdaptWeb datos para crear sus disciplinas. Aqu� usted puede importar y exportar las disciplinas, y buscar objetos de aprendizaje.');
	define ('A_LANG_REP_SELECT', 'Seleccione una disciplina');
	define ('A_LANG_REP_EXP_SUCCESS', 'La exportaci�n tuvo �xito!');
	define ('A_LANG_REP_EXP_ERROR', 'Error al exportar la disciplina!');
	define ('A_LANG_REP_EXP_TEXT', 'A trav�s de esta interfaz se puede hacer de sus temas AdaptWeb para que otros puedan utilizarlas. <br> Seleccione la disciplina de la lista a continuaci�n, para especificar una descripci�n y haga clic en "Exportar".');
	define ('A_LANG_REP_EXPORT', 'Exportar');
	define ('A_LANG_REP_SELECT_COURSE', 'Seleccione un Curso');
	define ('A_LANG_REP_IMP_SUCCESS', 'Laimportaci�n se fue un �xito!');
	define ('A_LANG_REP_IMP_ERROR', 'Error al importar la disciplina!');
	define ('A_LANG_REP_INVALID_PAR', 'Par�metros no v�lidos!');
	define ('A_LANG_REP_IMP_TEXT', 'A trav�s de esta interfaz puede copiar disciplinas disponibles en el repositorio para su entorno de autor�a en AdaptWeb! <br> Seleccione la disciplina de la siguiente lista, seleccione el curso que est� vinculado, haga clic en "Importar".');
	define ('A_LANG_REP_DISC_IN_REP', 'Disciplinas en el repositorio');
	define ('A_LANG_REP_MY_COURSES', 'Mis Cursos');
	define ('A_LANG_REP_OTHER_NAME', 'Importar con otro nombre');
	define ('A_LANG_REP_IMPORT', 'Importar');
	define ('A_LANG_REP_DISC_DATA', 'Datos de la disciplina');
	define ('A_LANG_REP_SEARCH', 'Introduzca una String para buscar y seleccionar los metadatos que se incluir�n en su b�squeda:');
	define ('A_LANG_REP_SCH_TITLE', 'T�tulo');
	define ('A_LANG_REP_SCH_SELECT', 'Seleccione ...');
	define ('A_LANG_REP_AGGREG', 'Nivel de Agregaci�n');
	define ('A_LANG_REP_SCH_RAW', 'Datos (archivos)');
	define ('A_LANG_REP_SCH_TOPIC', 'Concepto');
	define ('A_LANG_REP_SCH_DISC', 'Disciplina');
	define ('A_LANG_REP_SCH_COURSE', 'Curso');
	define ('A_LANG_REP_SCH_SEARCH', 'Buscar');
	define ('A_LANG_REP_SCH_ERROR', 'Error');
	define ('A_LANG_REP_SCH_NO_REG', 'Ninguno registro Localizado!');
	define ('A_LANG_REP_SCH_REG', 'registro localizado');
	define ('A_LANG_REP_SCH_REGS', 'registros localizados');
	define ('A_LANG_REP_DISC_NOT_FOUND', 'La disciplina no se ha encontrado');
	define ('A_LANG_COURSES', 'Curso(s)');
	define ('A_LANG_EXP_BY', 'Exportado por');
	define ('A_LANG_ERROR_SELECT_BD', 'Error seleccionando la base de datos');

	
/******************************************************************************************** 
 * Avalia��o (Definido por Claudiomar Desanti)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus&atilde;o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 */ 
define("A_LANG_MNU_AVS", "Funciones de evaluaci�n"); 
define("A_LANG_MNU_AVS_NAV", "Evaluaci�n"); // menu na parte de navega&ccedil;&atilde;o
define("A_LANG_TOPIC_EVALUATION","Evaluaci�n"); // orelha de avaliacao

define("A_LANG_AVS_STUDENTS_TOPIC_EVALUATION", "evaluaci�nes en libertad"); // t&oacute;pico da avalia&ccedil;&atilde;o
define("A_LANG_AVS_STUDENTS_TOPIC_RESULTS", "Tabla de resultados"); // t&oacute;pico de quadro de resultados

define("A_LANG_AVS_FUNCTION_INTRO","introducci�n");
define("A_LANG_AVS_FUNCTION_AVERAGE","Media");
define("A_LANG_AVS_FUNCTION_ADJUSTMENT","Ajustar <br/> nota");
define("A_LANG_AVS_FUNCTION_LIBERATE","Liberar <br/> evaluaci�n ");
define("A_LANG_AVS_FUNCTION_DIVULGE","Revelar <br/> nota");
define("A_LANG_AVS_FUNCTION_REPORT","informe");
define("A_LANG_AVS_FUNCTION_BACKUP","copia de seguridad");


define("A_LANG_AVS_TITLE_ADJUSTMENT_AVERAGE","Ajuste de la media");
define("A_LANG_AVS_TITLE_ADJUSTMENT_QUESTION_CANCEL","cancelar preguntas");
define("A_LANG_AVS_TITLE_ADJUSTMENT","Ajuste de la notas");
define("A_LANG_AVS_TITLE_ADJUSTMENT_QUESTION_WEIGHT","Distribuir el peso de las evaluaciones");
define("A_LANG_AVS_TITLE_DIVULGE","Divulgaci�n de las notas de las evaluaciones");
define("A_LANG_AVS_TITLE_NEW_AVAL", "Nueva evaluaci�n");
define("A_LANG_AVS_TITLE_NEW_AVAL_PRES", "Nueva Evaluaci�n presencial");
define("A_LANG_AVS_TITLE_GROUP","Nuevo");
define("A_LANG_AVS_TITLE_LIBERATE","liberar evaluaci�n");
define("A_LANG_AVS_TITLE_AVERAGE","Distribuci�n de las media");
define("A_LANG_AVS_TITLE_REPORT", "Informe");
define("A_LANG_AVS_TITLE_BACKUP", "Copia de Seguridad");
define("A_LANG_AVS_TITLE_DISSERT","Discurso de la correcci�n de las cuestiones");
define("A_LANG_AVS_TITLE_ADJUSTMENT_PRESENTIAL","Ajuste de las notas de las evaluaciones presenciales");
define("A_LANG_AVS_TITLE_ADD_QUESTION","Agregar questiones que ya existe");

// -------------- ERROS ------------------------------------------------- //
define("A_LANG_AVS_ERROR_DB_OBJECT","Error en la Inicializaci�n , el objeto de conexi�n a la base de datos no es v�lido");
define("A_LANG_AVS_ERROR_DB_SAVE","Error al guardar la informaci�n");
define("A_LANG_AVS_ERROR_PARAM","<div class='erro'><p>Error de paso de par�metros. Recargar la p�gina</p></div>");

define("A_LANG_AVS_ERROR_VERIFY_AUTHO","<div class='erro'><p>Usted no se puede ejecutar esta parte del  ambiente.</p></div>");
define("A_LANG_AVS_ERROR_EVALUATION_DELETE","No es posible eliminar la cuesti�n de la evaluaci�n");


define("A_LANG_AVS_ERROR_EVALUATION_AVERAGE","Evaluaci�n no se ha creado o integrado con un curso");
define("A_LANG_AVS_ERROR_FUNCTION_EVALUATION_LIBERATE","Evaluaci�n no ha concluido o puesto en libertad");
define("A_LANG_AVS_ERROR_QUESTION_OBJECT","No es posible cargar la pregunta");
define("A_LANG_AVS_ERROR_ANSWER_OBJECT","No es posible cargar la respuesta de la cuesti�n");
define("A_LANG_AVS_ERROR_LOGGED","primero usted debe iniciar sesi�n en el sistema para acceder a este m�dulo");
define("A_LANG_AVS_ERROR_STUDENTS_LIBERATE","Error al cargar la evaluaci�n");
define("A_LANG_AVS_ERROR_STUDENTS_VIEW","Esta evaluaci�n no es tuya");
define("A_LANG_AVS_ERROR_EVALUATION_LIBERATE","No hubo ninguna evaluaci�n");
define("A_LANG_AVS_ERROR_EVALUATION_LIBERATE_STUDENTS","El estudiante no est� com la evaluaci�n liberada");
define("A_LANG_AVS_ERROR_EVALUATION_LOGGED","Usted tiene que estar registrado para acceder a la evaluaci�n");
define("A_LANG_AVS_ERROR_EVALUATION_INIT","Error al intentar acceder a la evaluaci�n");
define("A_LANG_AVS_ERROR_EVALUATION_SUBMIT","La evaluaci�n ha sido cerrado por el profesor");
define("A_LANG_AVS_ERROR_AUTHO","Usted no tiene permiso para acceder a esta modulo");
define("A_LANG_AVS_ERROR_TOPIC_EVALUATION","Concepto no se encontr� con evaluaciones disponibles. Compruebe que las preguntas de la evaluaci�n se definen con el peso y el peso medio y se define.");
define("A_LANG_AVS_ERROR_LIBERATE_NOT_STUDENTS", "Ning�n estudiante est� matriculado en la disciplina para hacer las evaluaciones");
define("A_LANG_AVS_ERROR_DIVULGE_VALUE","No fue posible divulgar las notas");
define("A_LANG_AVS_ERROR_DIVULGE_NOT_EVALUATION","No se complet� la evaluaci�n que debe revelarse.");
define("A_LANG_AVS_ERROR_LIBERATE_EVALUATION","No fue posible completar la liberaci�n de la evaluaci�n");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT","No hubo ninguna evaluaci�n");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_SAVE","Error al guardar la cuesti�n");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_AVERAGE","Error en calcular  la nota");
define("A_LANG_AVS_ERROR_AVERAGE_CALCULE","error em calcular la media");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_STUDENT","Estudiante no las respuestas a esta evaluaci�n");
define("A_LANG_AVS_ERROR_ADJUSTMENT_PRESENTIAL","No se revelaron las notas, porque algunos estudiantes no recibieron nota");
define("A_LANG_AVS_ERROR_ADD_QUESTION_EXIST","Error en la adici�n de nuevas preguntas em la evaluaci�n");
define("A_LANG_AVS_ERROR_QUESTION_NOT_FOUND","No se han encontrado resultados");
define("A_LANG_AVS_ERROR_NOT_STUDENTS_LIBERATE","No hay estudiantes matriculados em este curso");
define("A_LANG_AVS_ERROR_EVALUATION_FINISH","La evaluaci�n ya se entreg�");
define("A_LANG_AVS_ERROR_EVALUATION_FINISH2","La evaluaci�n ya se ha cerrado, ya no se puede entregarla");
define("A_LANG_AVS_ERROR_SESSION_BROWSER"," La sesi�n que se centrar� la evaluaci�n no es el mismo, esto puede haber ocurrido por el cambio a otro computador o navegador.");
define("A_LANG_AVS_ERROR_FUNCTION_CANCEL","No se puede cancelar las preguntas. debe haber al menos una pregunta que no se cancela");


define("A_LANG_AVS_ERROR_QUESTION_LOGGED","Usted debe iniciar sesi�n en el sistema para trabajar con este m�dulo ");
define("A_LANG_AVS_ERROR_QUESTION_EVALUATION","La cuesti�n no se puede agregar a la evaluaci�n");
define("A_LANG_AVS_ERROR_QUESTION_LACUNA_DELETE","Error al eliminar la lacuna");
define("A_LANG_AVS_ERROR_QUESTION_ME_DELETE","No fue posible borrar la alternativa");
// erros utilizados no ajax
define("A_LANG_AVS_ERROR_AJAX_AVERAGE","Las evaluaciones todav�a no se han definido para este curso");
define("A_LANG_AVS_ERROR_AJAX_AVERAGE1","Ninguno estudiante no lo hayan hecho evaluaci�n");
define("A_LANG_AVS_ERROR_AJAX_DIVULGE1","los alunos marcados con contienda son los estudiantes son estudiantes que todav�a tienen cuestiones que deben abordarse discurso");
define("A_LANG_AVS_ERROR_AJAX_DIVULGE2","Ning�n estudiante h� celebrado esta evaluaci�n. La evaluaci�n fue cancelado y otra evaluaci�n se aplicar� a los estudiantes");
define("A_LANG_AVS_ERROR_AJAX_LIBERATE","Evaluaci�n no se libera a resolver");
define("A_LANG_AVS_ERROR_AJAX_LIBERATE2","Ninguna evaluaci�n se ha cerrado");
define("A_LANG_AVS_ERROR_AJAX_AVERAGE3","Este curso contiene ning�n grupo de evaluaci�n para ser puesto las medias a disposici�n ");
// ------------------- FIM DOS ERROS -----------------------------------------------------//

define("A_LANG_AVS_SUCESS", "Salvo com sucesso");
define("A_LANG_AVS_SUCESS_DIVULGE","Las notas de la evaluaci�n fueron puestos en libertad a los estudiantes.");
define("A_LANG_AVS_SUCESS_LIBERATE","Evaluaci�n publicado con �xito");
define("A_LANG_AVS_SUCESS_EVALUATION","La evaluaci�n se ha entregado con �xito");
define("A_LANG_AVS_STUDENTS","Estudiantes");

// -------------- BOTOES -----------------------------------------------------------------//
define("A_LANG_AVS_BUTTON_SAVE","Guardar");
define("A_LANG_AVS_BUTTON_SAVE_COPY_DEFAULT","Copiar peso default");
define("A_LANG_AVS_BUTTON_SAVE_COPY_MOD","Copiar peso modificado");
define("A_LANG_AVS_BUTTON_SUBMIT","Presentar la evaluaci�n");
define("A_LANG_AVS_BUTTON_BACK",A_LANG_TENVIRONMENT_AUTHORIZE_BACK);
define("A_LANG_AVS_BUTTON_HELP",A_LANG_HELP);
define("A_LANG_AVS_BUTTON_NEW_GROUP","Nuevo grupo");
define("A_LANG_AVS_BUTTON_CHANGE_OPTION","trocar par�metro");
define("A_LANG_AVS_BUTTON_NEW_EVALUATION","nueva evaluaci�n");
define("A_LANG_AVS_BUTTON_WEIGHT","Dividir el peso por igual");
define("A_LANG_AVS_BUTTON_NEXT","Siguiente");
define("A_LANG_AVS_BUTTON_LIBERATE_EVALUATION","Liberar nueva evaluaci�n");
define("A_LANG_AVS_BUTTON_FINISH_EVALUATION","Cerrar evaluaci�n");
define("A_LANG_AVS_BUTTON_LIST_STUDENTS","Listar estudiantes");
define("A_LANG_AVS_BUTTON_CLOSE","Cerrar");
define("A_LANG_AVS_BUTTON_SEARCH","Buscar");
define("A_LANG_AVS_BUTTON_MARK","Marcar todos");
define("A_LANG_AVS_BUTTON_UNMARK","Desmarcar todos");

// bot&otilde;es do AVS_TOPICO.PHP
define("A_LANG_AVS_BUTTON_GROUP_NEW","Nuevo grupo");
define("A_LANG_AVS_BUTTON_GROUP_PARAM","trocar par�metros");
define("A_LANG_AVS_BUTTON_GROUP_NEW_EVALUATION","Agregar Evaluaci�n");
define("A_LANG_AVS_BUTTON_DUPLICATE_AVAL","Duplicar Evaluaci�n");
define("A_LANG_AVS_BUTTON_AVAL_PRESEN","Agregar evaluaci�n de presencia");
// bot&otilde;es AVS_NOVA_AVAL.PHP
define("A_LANG_AVS_BUTTON_QUESTION_NEW","Agregar nueva questi�n");
define("A_LANG_AVS_BUTTON_QUESTION_EXIST","Agregar questi�n ya existente");
// bot&otilde;es utilizados na libera&ccedil;&atilde;o da avalia&ccedil;&atilde;o
define("A_LANG_AVS_BUTTON_EVALUATION_LIBERATE_NEW","liberar nueba evaluaci�n");
define("A_LANG_AVS_BUTTON_EVALUATION_LIBERATE_FINISH","Lista de Evaluaciones cerradas");
define("A_LANG_AVS_BUTTON_BACK_LIBERATE","volver a folio de liberaci�n");
// bot&otilde;es utilizados nas telas de quest&otilde;es
define("A_LANG_AVS_BUTTON_QUESTION_ADD_LACUNA","agregar nueba lacuna");
define("A_LANG_AVS_BUTTON_QUESTION_DELETE_LACUNA","Eliminar Lacuna");
define("A_LANG_AVS_BUTTON_QUESTION_ADD_ME","agregar nueva opci�n");

// bot&otilde;es utilizados nos ajax
define("A_LANG_AVS_BUTTON_AJAX_AVERAGE_LIST","Listar notas");
define("A_LANG_AVS_BUTTON_AJAX_DIVULGE","Revelar notas");
define("A_LANG_AVS_BUTTON_AJAX_LIBERATE_END","Cerrar evaluaci�n");
define("A_LANG_AVS_BUTTON_AJAX_LIST","listar estudiantes");

// --------------- FIM DOS BOTOES ---------------------------------------------------- //  
// --------------- LABELS - texto ao lado de combobox e spans ------------------------ //
define("A_LANG_AVS_LABEL_COURSE",A_LANG_COURSES);
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_VALUE","Nota");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_EXPLANATION","Explicaci�n");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_WEIGHT","Peso");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_CORRECT","Respuesta correcta");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_OBS_TEACHER","Comentario del profesor");
define("A_LANG_AVS_LABEL_EVALUATION_MODEL","Modelos de Evaluaci�n");
define("A_LANG_AVS_LABEL_EVALUATION_MODEL_AUTO","Modelos de evaluaci�n automatizada");
define("A_LANG_AVS_LABEL_EVALUATION_MODEL_PRESENTIAL","Modelos de evaluaci�n de Presencia");
define("A_LANG_AVS_LABEL_EVALUATION","Evaluaci�n");
define("A_LANG_AVS_LABEL_TOPIC","Concepto");
define("A_LANG_AVS_LABEL_VIEW_EVALUATION","Visualizaci�n de la evaluaci�n por el profesor");
define("A_LANG_AVS_LABEL_YES","S�");
define("A_LANG_AVS_LABEL_NOT","No");
define("A_LANG_AVS_LABEL_INFO","informaciones");
//

define("A_LANG_AVS_LABEL_QUESTION_DISSERT","Dissertativa");
define("A_LANG_AVS_LABEL_QUESTION_ME","De opci�n m�ltiple");
define("A_LANG_AVS_LABEL_QUESTION_LACUNAS","Colmar las lacunas");
define("A_LANG_AVS_LABEL_QUESTION_VF","Verdad o falso");

define("A_LANG_AVS_LABEL_FUNCTION","Funciones");
define("A_LANG_AVS_LABEL_QUESTION","Cuestiones");

// labels do AVS_GRUPO.PHP
define("A_LANG_AVS_LABEL_QUESTION_RANDOM","Las preguntas que deben mezclarse?");
define("A_LANG_AVS_LABEL_EVALUATION_PRESENT","Evaluaci�n de presencia");
define("A_LANG_AVS_LABEL_ANOTHER_GROUP","Elegido em otro grupo");
// labels do AVS_NOVA_AVAL.PHP
define("A_LANG_AVS_LABEL_DESCRIPTION","Descripci�n");
define("A_LANG_AVS_LABEL_DIVULGE_NOTE","Divulgar las notas autom�ticamente?");
define("A_LANG_AVS_LABEL_DISSERT_QUESTION","Hay preguntas dissertativas a evaluaci�n");
// labels do AVS_TOPICO.PHP
define("A_LANG_AVS_LABEL_GROUP_DESCRIPTION","Grupo de evaluaci�n por concepto ");
define("A_LANG_AVS_LABEL_GROUP_NOT_WEIGHT","A�n no definido");
define("A_LANG_AVS_LABEL_GROUP_PARAM","Par�metros:");
define("A_LANG_AVS_LABEL_GROUP_QUESTION_RANDOM","Mezclar preguntas:");
define("A_LANG_AVS_LABEL_GROUP_EVALUATION_PRESENT",A_LANG_AVS_LABEL_EVALUATION_PRESENT);
define("A_LANG_AVS_LABEL_GROUP_COURSE_WEIGHT","Cursos (Peso%):");
define("A_LANG_AVS_LABEL_GROUP_NOT_EVALUATION","No hay ninguna evaluaci�n para este grupo");
// labels utilizados no F_AJUSTE.PHP
define("A_LANG_AVS_LABEL_FUNCTION_AVERAGE","Ajustar media");
define("A_LANG_AVS_LABEL_FUNCTION_QUESTION_DISSERT","Fija preguntas dissertativas");
define("A_LANG_AVS_LABEL_FUNCTION_QUESTION_CANCEL","Calcelar cuestiones de evaluaci�n");
// labels utilizados no F_AJUSTE_ANULAR.PHP
define("A_LANG_AVS_LABEL_CHOOSE_EVALUATION","Seleccione la evaluaci�n");
define("A_LANG_AVS_LABEL_CHOOSE_COURSE","Seleccione el curso");
define("A_LANG_AVS_LABEL_CHOOSE_TOPIC","Seleccione el concepto");
define("A_LANG_AVS_LABEL_CHOOSE_STUDENTS","Elegir los estudiantes que realizan la evaluaci�n");

// labels utilizados no ambiente do aluno
define("A_LANG_AVS_LABEL_USER_NOT_EVALUATION","Sin evaluaci�n em liberdad");
define("A_LANG_AVS_LABEL_USER_NOT_VALUE","sin notas disponibles");
// labels das telas de quest&otilde;es
define("A_LANG_AVS_LABEL_QUESTION_ENUNCIATE","Redacci�n de la cuesti�n:");
define("A_LANG_AVS_LABEL_QUESTION_EXPLANATION","Explicaci�n:");
define("A_LANG_AVS_LABEL_QUESTION_OPTION_CORRECT","�Cual es la opci�n correcta?");
define("A_LANG_AVS_LABEL_QUESTION_VF_CORRECT","Verdadero");
define("A_LANG_AVS_LABEL_QUESTION_VF_FALSE","Falso");
define("A_LANG_AVS_LABEL_QUESTION_VF_NAME_CORRECT","Nombre Real");
define("A_LANG_AVS_LABEL_QUESTION_VF_NAME_FALSE","Nome Falso");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_DISSERT","Cuesti�n del tipo dissertativa");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_LACUNA","Cuesti�n del tipo lacuna");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_ME","Cuesti�n  de opci�n m�ltiple");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_VF","Custion del tipo verdadero o falso");

define("A_LANG_AVS_LABEL_AJAX_DIVULGE1","Ok");
define("A_LANG_AVS_LABEL_AJAX_DIVULGE2","Pendiente");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE1","Pendiente");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE2","haciendo");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE3","Entegado");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE4","No entergado");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE5","Cero");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION1","Lista de evaluaci�n em liberdad");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION2","Tema");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION3","Descripci�n");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION4","Lista de evaluaciones cerradas");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION5","Evaluaci�n de presencia");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION6","Fijo");
define("A_LANG_AVS_LABEL_AJAX_AVERAGE","Grupo de Evaluaci�n para el tema");

define("A_LANG_AVS_LABEL_AJAX_ZERO","La nota se cancelar� cero");

define("A_LANG_AVS_LABEL_SEARCH","Buscar por palabra clave");



// -------------- FIM DOS LABELS ------------------------------------------------------------ //
// -------------- COMBOBOX ----------------------------------------------------------------- //

define("A_LANG_AVS_COMBO_SELECT","Seleccione ...");
define("A_LANG_AVS_COMBO_DATE","Em liberdad");

// utilizado no F_AJUSTE_ANULAR.PHP
define("A_LANG_AVS_COMBO_TOPIC","Tema:");
define("A_LANG_AVS_COMBO_MARK","se�alar");
define("A_LANG_AVS_COMBO_UNMARK","Anul");


// -------------- TTULOS DE TABELAS -------------------------------------------------------- //
define("A_LANG_AVS_TABLE_MODIFY","Cambiar");
define("A_LANG_AVS_TABLE_DELETE","Eliminar");
define("A_LANG_AVS_TABLE_DESCRIPTION","Descripci�n");
// t&iacute;tulo do AVS_NOVA_AVAL.PHP
define("A_LANG_AVS_TABLE_TYPE","Tipo");
define("A_LANG_AVS_TABLE_WEIGHT","Peso(%)");
define("A_LANG_AVS_TABLE_WEIGHT_DEFAULT","Peso default(%)");
define("A_LANG_AVS_TABLE_WEIGHT_MOD","Peso modificado(%)");
define("A_LANG_AVS_TABLE_SUM_WEIGHT","Total de los pesos:");
define("A_LANG_AVS_TABLE_REST_WEIGH","Pesos sobrante:");
// t&iacute;tulo do AVS_TOPICO.PHP
define("A_LANG_AVS_TABLE_DIVULGE","Divulgaci�n");
define("A_LANG_AVS_TABLE_NUMBER_QUESTION","Cantidad de preguntas");
define("A_LANG_AVS_TABLE_DATE_START","Fecha de  liberaci�n");
define("A_LANG_AVS_TABLE_DATE_FINISH","Fecha de Finalizaci�n");
define("A_LANG_AVS_TABLE_DATE","Fecha");
define("A_LANG_AVS_TABLE_VALUE_CANCELLED","Notas canceladas");
define("A_LANG_AVS_TABLE_ACTION","Evaluaciones");
define("A_LANG_AVS_TABLE_DATE_CREATE","Creaci�n");
define("A_LANG_AVS_TABLE_DATE_MODIFY","Enmienda");
define("A_LANG_AVS_TABLE_DIVULGE_AUTO","Automatico");
define("A_LANG_AVS_TABLE_DIVULGE_MANUAL","Manual");
// titulo dos scripts do ambiente do aluno (N_INDEX, N_MURAL)
define("A_LANG_AVS_TABLE_USER_EVALUATION","Evaluaci�n");
define("A_LANG_AVS_TABLE_USER_DATE","Fecha de inicio");
define("A_LANG_AVS_TABLE_USER_REALIZE_EVALUATION","hacer la evaluaci�n");
define("A_LANG_AVS_TABLE_USER_DESCRIPTION_EVALUATION","descripci�n");
define("A_LANG_AVS_TABLE_USER_DATE_REALIZE","Fecha de realizaci�n");
define("A_LANG_AVS_TABLE_USER_VALUE","Notas");
define("A_LANG_AVS_TABLE_USER_VALUE2","nota de la prueba");
define("A_LANG_AVS_TABLE_USER_TOTAL_VALUE","Nota Total");
// titulos de tabelas das telas de quest&otilde;es
define("A_LANG_AVS_TABLE_QUESTION_LACUNA1","Texto de inicio");
define("A_LANG_AVS_TABLE_QUESTION_LACUNA2","La respuesta de la lacuna");
define("A_LANG_AVS_TABLE_QUESTION_LACUNA3","Continuaci�n del texto");
define("A_LANG_AVS_TABLE_QUESTION_ME1","Alternativas");
define("A_LANG_AVS_TABLE_QUESTION_ME2","Correcta?");
define("A_LANG_AVS_TABLE_QUESTION_ME3","Fijo?");
define("A_LANG_AVS_TABLE_QUESTION_ME4","Descripci�n de las alternativas");
define("A_LANG_AVS_TABLE_QUESTION_ME5","Eliminar");
// titulos utilizados nas tabelas do ajuste de notas dissertativa
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT","Estudiante");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT1","Cuestiones pendientes");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT2","	Estudiante no se complet� la evaluaci�n");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT3","Corregir cuestiones");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT4","Cambiar las correcciones");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT5","Peso");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT6","Nota");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT7","Explicaci�n");

// titulos utilizados nos ajax
define("A_LANG_AVS_TABLE_AJAX_AVERAGE","Estudiante");
define("A_LANG_AVS_TABLE_AJAX_AVERAGE1","Media adquirida");
define("A_LANG_AVS_TABLE_AJAX_AVERAGE2","Nota de la participaci�n");
define("A_LANG_AVS_TABLE_AJAX_AVERAGE3","Media final");
define("A_LANG_AVS_TABLE_AJAX_QUESTION_CANCEL","Cancelar");
define("A_LANG_AVS_TABLE_AJAX_QUESTION_UNDO","Deshacer");
define("A_LANG_AVS_TABLE_AJAX_QUESTION_CANCEL2","Cancelada");
define("A_LANG_AVS_TABLE_AJAX_DIVULGE1","Estudiante");
define("A_LANG_AVS_TABLE_AJAX_DIVULGE2","Pendencia");

// hints - texto flutuante informativo
define("A_LANG_AVS_HINTS_EVALUATION_EDIT","Edici�n de evaluaci�n. Incluir nuevas custiones");
define("A_LANG_AVS_HINTS_EVALUATION_DELETE","Deletar Evaluaciones");
define("A_LANG_AVS_HINTS_EVALUATION_VIEW","Exhibir la evaluaci�n");
define("A_LANG_AVS_HINTS_USER_EVALUATION_VIEW","Esta nota es correcto al referirse a las pruebas, no a las cuestiones, por el profesor");
define("A_LANG_AVS_HINTS_QUESTION_DELETE","Eliminar esta alternativa");
define("A_LANG_AVS_HINTS_AJAX_QUESTION_CANCEL","Esta cuesti�n ha sido previamente cancelado");
define("A_LANG_AVS_HINTS_AJAX_DIVULGE1","Abordar cuestiones dissertativas");
// textos
define("A_LANG_AVS_TEXT_LIBERATE","Este es el n�mero de la evaluaci�n en libertad. Gu�rdelo para futuras referencias.");
define("A_LANG_AVS_TEXT_FIELD_REQUIRE","<b>Los campos marcados con *  </b> deben ser rellenados");
define("A_LANG_AVS_TEXT_ADJUSTMENT","<b>Los campos explicaci�n  </b> se presentar� a los estudiantes como una explicaci�n del error / �xito. El relleno no es necesario.");
define("A_LANG_AVS_TEXT_LIBERATE_STUDENT","Estudiantes con (*) ya han realizado esta evaluaci�n. Si son seleccionados una vez m�s, el antiguo se borrar�.");

// ------------- TEXTOS UTILIZADOS EM JAVASCRIPT ---------------------------------------------//
// OBS: lembre-se em Javascript escreva sem htmlentities ( a = &aacute;)
define("A_LANG_AVS_JS_EVALUATION_CONFIRM","Est� seguro de que desea enviar esta evaluaci�n?");
define("A_LANG_AVS_JS_DELETE_PRESENT_CONFIRM","	Las cuestones que esta evaluaci�n se retirar� si es presencial");
define("A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT","Hay cuestiones que no han tenido sus ponderaciones se definen");
define("A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT2","Al todo el peso de las preguntas no deber�n exceder de un cien por ciento (100%)");
define("A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT3","El peso de las cuestiones que debe a�adir un cien por ciento (100%)");
define("A_LANG_AVS_JS_DELETE_QUESTION_CONFIRM","�Desea eliminarlo?");
define("A_LANG_AVS_JS_ALERT_QUESTION","Los pesos ya no se  han guardado");
define("A_LANG_AVS_JS_ALERT_EVALUATION","Usted debe elegir una evaluaci�n");
define("A_LANG_AVS_JS_ALERT_QUESTION_REQUIRE","el campo ");
define("A_LANG_AVS_JS_ALERT_QUESTION_REQUIRE2"," Debe ser completado!");
define("A_LANG_AVS_JS_ALERT_QUESTION_LACUNA_DELETE","�Desea eliminar esta lacuna?");
define("A_LANG_AVS_JS_ALERT_QUESTION_ME_CORRECT","No fue elegido ninguna alternativa correcta");
define("A_LANG_AVS_JS_ALERT_QUESTION_ME_REQUIRE","la pergunta no fue llenada");
define("A_LANG_AVS_JS_ALERT_QUESTION_ME_DELETE","�Desea eliminar esta alternativa?");
define("A_LANG_AVS_JS_ALERT_QUESTION_VF_REQUIRE_CORRECT","Debe elegirse entre las opciones a ser la correcta");
define("A_LANG_AVS_JS_ALERT_STUDENTS"," es necesario seleccionar los estudiantes");
define("A_LANG_AVS_JS_ALERT_EVALUATION_DELETE_CONFIRM","�Desea eliminar esta evaluaci�n?");
// ------------- FIM DO JAVASCRIPT ------------------------------------------------------------//
define("A_LANG_AVS_FORMAT_DATE","%d/%m/%Y"); // string da funcao date para modificar o estilo da data
define("A_LANG_AVS_FORMAT_DATE_TIME","%d/%m/%Y %H:%M:%S"); // string da funcao date para modificar o estilo da data

/**********************************************************************************************/
/*********************************** FIM DA AVALIACAO **************************************/

/******************************************************************************************** 
 * Mural de Recados (Definido por Laisi Corsani)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Estagio Curricular.
 * Orientadora: Isabela Gasparini
 * Supervisora: Avanilde Kemczinski
 */ 
define("A_LANG_MURAL_DISC","Mural de recados de la disciplina "); 
define("A_LANG_MURAL_ENVIARECADO_DISC","Enviar recado en la disciplina"); 
define("A_LANG_MURAL_ENVIARECADO","Enviar Recado");
define("A_LANG_MURAL_ESCREVERECADO","Escribir recado");
define("A_LANG_MURAL_OUTRORECADO","Escribir otro recado");
define("A_LANG_MURAL_ASS", "ASUNTO:");
define("A_LANG_MURAL_REM", "REMITENTE:");
define("A_LANG_MURAL_REC","RECADO");
define("A_LANG_MURAL_DT","FECHA:");
define("A_LANG_MURAL_ALUNOS","Estudiantes:");
define("A_LANG_MURAL_DEST","Destinatario");
define("A_LANG_MURAL_LISTREC","Los recados recibidos en los �ltimos siete d�as:");
define("A_LANG_MURAL_SENDEMAIL","Enviar e-mail de aviso");
define("A_LANG_MURAL_ASSMIN","Asunto:");
define("A_LANG_MURAL_MSG","mensaje");
define("A_LANG_MURAL_PROF","Profesor");
define("A_LANG_MURAL_GROUP","Grupo");
define("A_LANG_REC_SUSS","Mensaje enviado con �xito!");
define("A_LANG_REC_INSUSS","No se puede enviar el mensaje!");
define("A_LANG_REC_INSUSS_ALL","No se puede enviar el mensaje a todos los destinatarios!");
define("A_LANG_REC_ALERT_DEST","Seleccione un destino!");
define("A_LANG_REC_ALERT_MSG","No se puede enviar un recado sin contenido!");
define("A_LANG_REC_ALERT_ALL","No se puede enviar un recado! Para enviar un recado que debe seleccionar una persona y escribir un mensaje!");
define("A_LANG_MURAL_BACK","Volver a mural de recados!");
define('A_LANG_LENVIRONMENT_MURAL', 'mural de recados');

/*********************************** FIM MURAL RECADOS **************************************/

/********************************************************************************************
 * Altera��es para Usabilidade (Definido por Carla Cristina Lui Dias)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Estagio Curricular.
 * Orientadora: Isabela Gasparini
 * Supervisora: Avanilde Kemczinski
 */

define("A_LANG_SYSTEM_NAME_DESC","Ambiente de Ense�anza-aprendizaje Adaptativo na Web (AdaptWeb) ");
define("A_LANG_SYSTEM_WELCOME","Bienvenido a");  
define("A_LANG_INTRO_DESC_1","Este entorno permite que el profesor para proporcionar materiales para sus estudiantes.");//calra
define("A_LANG_INTRO_DESC_2","Para navegar por el ambiente, el profesor debe hacer inicialmente su cuenta de usuario sobre el tema <u> Nuevo usuario </u> en el men� de opciones. Despu�s de la registraci�n, el profesor debe acceder a su punto <u> inicio de sesi�n </u> en el men� de opciones. Si el profesor ya tiene un registro, s�lo para completar su informaci�n de acceso.");//calra
define("A_LANG_INTRO_DESC_3","Cuando el acceso se realiza, las opciones del men� se cambia y el profesor haciendo clic en el tema del <u>ambiente Profesor</u> pueden interactuar con el ambiente para crear sus disciplinas. Al hacer clic sobre este tema, aparece un submen� con el t�tulo de <b>Profesor  Ambiente </b> al final del men� principal, por lo que el profesor puede solicitar y asistir (s) tema (s) disponible (s) por parte de los maestros o por �l mismo, puede crear cursos, los alumnos libres, a fin de examinar el registro, la evaluaci�n se aplican, entre otros.");//calra
define("A_LANG_INTRO_DESC_4","Este ambiente permite a los estudiantes tener acceso a la (s) de material (s) de instrucci�n (s) de divulgar (s) de la (s) de su (s) professor (s) de acuerdo a su perfil. <p align=justify> Para navegar por el ambiente, el estudiante debe completar su cuenta de usuario en el tema <u> Nuevo usuario </u> en el men� de opciones. Despu�s de la matriculaci�n, el estudiante debe acceder a su punto <u>inicio de sesi�n</u> en el men� de opciones. Si el estudiante ya tiene un registro, s�lo para completar su informaci�n de acceso."); //carla
define("A_LANG_INTRO_DESC_5","Cuando el acceso se realiza, las opciones del men� se cambia y el estudiante debe hacer clic sobre el tema <u>Estudiantil Medio Ambiente</u>. Al hacer clic sobre este tema, aparece un submen� con el t�tulo de Estudiantes <b> Medio Ambiente </b> al final del men� principal, por lo que el estudiante puede solicitar y asistir (s) tema (s) disponible (s) de la (s) de su (s) maestro (s).");//Carla
// Itens de Autoria
define("A_LANG_AUTHOR","Ambiente de Autor");//carla 
define("A_LANG_STUDENT", "Ambiente del Estudiante"); //carla para n_entrada_navegacao  
define("A_LANG_AUTHOR_INTRO_1","El m�dulo permite a la autora al autor ofrecer contenido adaptado a diferentes perfiles de los estudiantes. A trav�s del proceso de autor�a, el autor puede proporcionar el contenido de sus clases en una sola estructura adaptada a los diferentes cursos. En esta fase el autor debe inscribirse en la disciplina y los cursos que desea que est� disponible.
");//carla           
define("A_LANG_AUTHOR_INTRO_2","Tras el registro insertar el concepto de los contenidos de archivos, relativos a cada tema de la disciplina. Adem�s, el autor deber� informar a la descripci�n del tema, y sus cursos de pre-requisito que quiere dar contenido. El autor puede insertar ejercicios, ejemplos y materiales adicionales para cada tema."); //Carla

// Estudantes
define("A_LANG_INTRO_STUD_DESC","Este ambiente permite a los estudiantes tener acceso a la (s) de material (s) de instrucci�n (s) de divulgar (s) de la (s) de su (s) profesor(s) de acuerdo a su perfil.
Para acceder a <p align=justify> (s) de su (s) tema (s), el estudiante debe hacer clic en el tema
<u>Estudiante  Ambiente</u> en el men� de opciones. Al hacer clic sobre este tema se publicar� un
  sub - men� con el t�tulo de  <b>Ambiente  Estudiantes </b> al final del men� principal, de modo que el usuario debe hacer clic en
  Solicitud de Registro <u>el tema</u> en el sub - men� de la petici�n actual, que est� relacionada con la
  disciplinas que quieran preguntarle.
<p align=justify> A solicitud del curso, el usuario debe marcar la disciplina que deseen solicitar que
el profesor y haga clic en solicitud. Despu�s de la disciplina requiere que el usuario debe esperar
la liberaci�n de la maestra para que pueda asistir a la disciplina necesaria.");//Carla
define("A_LANG_REQUEST_ACCESS_MSG11","E-mail El campo se llen� de valor no v�lido");//modificado por Carla, inseri * em Mar�o de 2009

/*********************************** FIM USABILIDADE **************************************/

/******************************************************************************************** 
 * F�rum de Discuss�o (Definido por Claudia da Rosa)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como TCC - Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora Avanilde Kemczinski
 */
 
define("A_LANG_TOPICO_INSUSS","	No se puede crear el tema!"); 
define("A_LANG_RESPOSTA_INSUSS","No se puede responder con el tema!"); 
define("A_LANG_TOPICO_SUSS","El tema se ha creado correctamente!");
define("A_LANG_RESPOSTA_SUSS","Respuesta de hecho con �xito!");
define("A_LANG_TOP_ALERT_TITULO","No se puede crear tu tema! Introduzca un t�tulo!");
define("A_LANG_TOP_ALERT_DESCR","No se puede crear tu tema! Introduzca su descripci�n!");
define("A_LANG_TOP_ALERT_RESP_DESCR","No se puede responder el tema! Introduzca su descripci�n!");
define("A_LANG_TOP_ALERT_AVAL","Seleccione una de las opciones para la evaluaci�n!");
define("A_LANG_FORUM_OUTRO_TOPICO","Crear otro tema");
define("A_LANG_TOP_ALERT_ALL","No se puede crear tu tema! Introduzca un t�tulo y una descripci�n!"); 
define("A_LANG_TOP_ALERT_ALL_PROF","No se puede crear tu tema! Introduzca el t�tulo, la descripci�n y seleccione la evaluaci�n!"); 
define("A_LANG_TOP_ALERT_ALL_AVAL_DESCR","No se puede crear tu tema! Escriba la descripci�n y evaluaci�n seleccione!");
define("A_LANG_TOP_ALERT_ALL_AVAL_TITULO","No se puede crear tu tema! Escriba un t�tulo y elegir la evaluaci�n!"); 
define("A_LANG_FORUM_DISC","Foro de Discusi�n de la disciplina "); 
define('A_LANG_LENVIRONMENT_FORUM', 'Foro de Discusi�n');
define("A_LANG_FORUM_BACK","Volver al Foro de Discusi�n");
define("A_LANG_TOPICO_ESCOLHIDO_BACK","Volver ao tema elegido");
define("A_LANG_FORUM_LISTTOP","Los temas creados en los �ltimos d�as:");
define("A_LANG_FORUM_TITULO","T�tulo: ");
define("A_LANG_FORUM_TITULO_TOP","T�tulo del Tema: ");
define("A_LANG_FORUM_DESCR","Descripci�n:");
define("A_LANG_FORUM_CRIARTOPICODISCUSSAO","Crear Tema de Discusi�n");
define("A_LANG_FORUM_RESPONDERTOPICO","Responder al Tema");
define("A_LANG_FORUM_AVAL","Evaluaci�n");
define("A_LANG_FORUM_SIM","S�");
define("A_LANG_FORUM_NAO","No");
define("A_LANG_FORUM_QUAL_INS","no suficiente");
define("A_LANG_FORUM_QUAL_REG","Regular");
define("A_LANG_FORUM_QUAL_BOM","Bueno");
define("A_LANG_FORUM_QUAL_EXC","Excelente");
define("A_LANG_FORUM_AVAL_SUSS","Evaluaci�n de la participaci�n del estudiante �xito!");
define("A_LANG_FORUM_AVAL_INSUSS","No se puede realizar la evaluaci�n de las parcicipaciones!");
define("A_LANG_FORUM_LIST_ALUN_MAT","Alunos Registrados:");
define("A_LANG_FORUM_AVAL_ALUN_BACK","Volver para evaluar a los estudiantes");
define("A_LANG_FORUM_ALUNO","Estudiante");
define("A_LANG_FORUM_AVAL_PARTIC","No Tiene participaciones de los estudiantes en los temas!");
define("A_LANG_FORUM_AVAL_OBS","*Nota: Para realizar una evaluaci�n formativa de los estudiantes primero debe evaluar los temas de discusi�n por separado, a trav�s del enlace <u> S� </u> la columna \"Evaluaci�n \"");

/*********************************** FIM FORUM DISCUSSAO **************************************/

/******************************************************************************************** 
 * Altera��es para Usabilidade (Definido por Carla Cristina Lui Dias)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Estagio Curricular.
 * Orientadora: Isabela Gasparini
 * Supervisora: Avanilde Kemczinski
 */
define("A_LANG_EMAIL_ERRO","El mensaje de advertencia no se ha enviado el mensaje de correo electr�nico a todos los destinatarios! <br> No son v�lidos en la lista del email.");
/*********************************** FIM ERRO EMAIL **************************************/

?>
