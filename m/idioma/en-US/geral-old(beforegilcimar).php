<?
	/* -----------------------------------------------------------------------
	 *  AdaptWeb - Research Project
	 *     UFRGS - Informatics Institute
	 *       UEL - Computing Department
	 * -----------------------------------------------------------------------
	 *       @package AdaptWeb
	 *     @subpakage Language
	 *          @file idioma/pt-BR/geral.php
	 *    @description Traduction File - English/USA
	 *         @since 17/08/2003
	 *        @author Veronice de Freitas (veronice@jr.eti.br)
	 *   @Translation Cassio Rugeri Cons
	 * -----------------------------------------------------------------------
	 */

       	// ************************************************************************ 	// * Interface - ????                                                                     *
 	// ************************************************************************
	//Interface data of the authorship tool
	define("A_LANG_VERSION","Version");
  	define("A_LANG_SYSTEM","AdaptWeb Environment");
  	define("A_LANG_ATHORING","Authorship Tool ");
  	define("A_LANG_USER","User");
  	define("A_LANG_PASS","Password");
  	define("A_LANG_NOTAUTH","< not authenticated>");
  	define("A_LANG_HELP","Help");
  	define("A_LANG_DISCIPLINE","Discipline");
	define("A_LANG_NAVEGATION","Where am I: ");

	// Context
	// comment - file p_contexto_navegacao.php
       	// ************************************************************************ 	// * Menu                                                                                 *
 	// ************************************************************************
	// Home
	define("A_LANG_MNU_HOME","Home");
	define("A_LANG_MNU_NAVIGATION","Learner Environment");
	define("A_LANG_MNU_AUTHORING","Teacher Environment");
	define("A_LANG_MNU_UPDATE_USER","Update User");
	define("A_LANG_MNU_EXIT","Exit");
	define("A_LANG_MNU_LOGIN","Login");
	define("A_LANG_MNU_ABOUT","About");
	define("A_LANG_MNU_DEMO","Demonstration");
	define("A_LANG_MNU_PROJECT","Project");
	define("A_LANG_MNU_FAQ","Frequently Asked Questions");
	define("A_LANG_MNU_RELEASE_AUTHORING","Release Authoring");
	define("A_LANG_MNU_BACKUP","Backup");
	define("A_LANG_MNU_NEW_USER","New User");
	define("A_LANG_MNU_APRESENTATION","Presentation");
	define("A_LANG_MNU_MY_ACCOUNT","My Account");
	define("A_LANG_MNU_MANAGE","Manage");
	define("A_LANG_MNU_BAKCUP","Backup");
	define("A_LANG_MNU_GRAPH","Access");
	define("A_LANG_MNU_TODO","Under Development");
	define("A_LANG_SYSTEM_NAME","AdaptWeb - Teaching-Learning Adaptative Environment at Web");
  	define("A_LANG_SYSTEM_NAME_DESC","Teaching-Learning Adaptative Environment at Web (AdaptWeb)"); //Inserimos para teste em outros idiomas-verificar se � compativel mesma variavel em BR-Carla est�gio

	define("A_LANG_SYSTEM_COPYRIGHT","Licensed under the GNU GENERAL PUBLIC LICENSE Version 2"); 

        // Autorship
        define("A_LANG_MNU_COURSE","Course");
        define("A_LANG_MNU_DISCIPLINES","Discipline");
        define("A_LANG_MNU_DISCIPLINES_COURSE","Discipline/Course");
        define("A_LANG_MNU_STRUTURALIZE_TOPICS","Structuralize Content");
        define("A_LANG_MNU_ACCESS_LIBERATE","Analyze Registration Requests");
        define("A_LANG_MNU_LIBERATE_DISCIPLINES","Liberate Discipline");
        define("A_LANG_MNU_WORKS","Learner's Task");

	// Studant
	define("A_LANG_MNU_DISCIPLINES_RELEASED","Watch Disciplines");		define("A_LANG_MNU_SUBSCRIBE","Request Registration");				define("A_LANG_MNU_WAIT","Waiting Registration");
	define("A_LANG_MNU_UPLOAD_FILES","Upload Task");			define("A_LANG_MNU_MESSAGE","Warnings");
	// ************************************************************************ 	// * Main Form                                                                                               *
 	// ************************************************************************
 	// Presentations
	define("A_LANG_ORGANS","Participant Instituitions");
	define("A_LANG_RESEARCHES","Researchers");
	define("A_LANG_AUTHORS","Authors");


 	// ************************************************************************ 	// * Form - Authorship                                                                     *
 	// ************************************************************************
	// Presentation
	define("A_LANG_MNU_ORGANS","");
	define("A_LANG_MNU_RESEARCHES","");
	define("A_LANG_MNU_AUTHORS","");

        // Access request form
        define("A_LANG_REQUEST_ACCESS","Access Request");
        define("A_LANG_REQUEST_ACCESS_USER_TYPE","User Type");
        define("A_LANG_REQUEST_ACCESS_TYPE_STUDANT","Learner");
        define("A_LANG_REQUEST_ACCESS_TYPE_TEACHES","Teacher");
        define("A_LANG_REQUEST_ACCESS_NAME","Name");
        define("A_LANG_REQUEST_ACCESS_EMAIL","E-mail");
        define("A_LANG_REQUEST_ACCESS_PASS","Password");
        define("A_LANG_REQUEST_ACCESS_PASS2","Password Confirmation");
        define("A_LANG_REQUEST_ACCESS_INSTITUTION_EDUCATION","Education Institution");
        define("A_LANG_REQUEST_ACCESS_COMMENT","Comment");
        define("A_LANG_REQUEST_ACCESS_LANGUAGE","Language");
        define("A_LANG_REQUEST_ACCESS_MSG1","It was not possible to establish connection with the data base");
	define("A_LANG_REQUEST_ACCESS_MSG2","It was not possible to send your data (type another e-mail)");
        define("A_LANG_REQUEST_ACCESS_MSG3"," It was not possible to send your data");
        define("A_LANG_REQUEST_ACCESS_MSG4","Invalid Password (Type the same password in the PASSWORD and in the PASSWORD CONFIRMATION fields)");
        define("A_LANG_REQUEST_ACCESS_SAVE","Save");


        //Login Form
        define("A_LANG_LOGIN2","Login");
        define("A_LANG_EMAIL","E-mail");
	define("A_LANG_LOGIN_PASS","Password");
	define("A_LANG_LOGIN_START","Start");
	define("A_LANG_LOGIN_MSG1","Invalid Password");
        define("A_LANG_LOGIN_MSG2","Invalid E-mail");
        define("A_LANG_LOGIN_MSG3","Unauthorized user");
        define("A_LANG_LOGIN_MSG4","Wait for your access liberation by the AdaptWeb Environment administrator");
        define("A_LANG_LOGIN_MSG5","It was not possible to establish connection with the data base");


        // Course Register Form
        define("A_LANG_COURSE_REGISTER","Course Register");
        define("A_LANG_COURSE_RESGISTERED","Registered Courses");
        define("A_LANG_COURSE2","Course");
        define("A_LANG_COURSE_INSERT","Insert");
        define("A_LANG_COURSE_UPDATE","Update");
        define("A_LANG_COURSE_DELETE","Delete");
        define("A_LANG_COURSE_MSG1","It was not possible to establish connection with the data base");
	define("A_LANG_COURSE_MSG2","It was not possible to insert the course");
	define("A_LANG_COURSE_MSG3","It was not possible to update the course");
	define("A_LANG_COURSE_MSG4","It is not possible to delete the course, because it's related to some discipline. To remove the course, you must enter in 'discipline - course' and disable the selection of all the disciplines from the course you wish to delete.");
	define("A_LANG_COURSE_MSG5"," It was not possible to delete the course");


        // Discipline Register
        define("A_LANG_DISCIPLINES_REGISTER","Discipline Register");
        define("A_LANG_DISCIPLINES_REGISTERED","Registered Disciplines");
        define("A_LANG_DISCIPLINES2","Discipline");
        define("A_LANG_DISCIPLINES_INSERT","Insert");
        define("A_LANG_DISCIPLINES_UPDATE","Update");
        define("A_LANG_DISCIPLINES_DELETE","Delete");
        define("A_LANG_DISCIPLINES_MSG1","It was not possible to insert the discipline");
        define("A_LANG_DISCIPLINES_MSG2","It was not possible to update the discipline");
        define("A_LANG_DISCIPLINES_MSG3","It is not possible to delete the course, because it's related to some discipline. To remove the course, you must enter in 'discipline - course' and disable the selection of all the disciplines from the course you wish to delete.");
        define("A_LANG_DISCIPLINES_MSG4","It was not possible delete the discipline");


        // Discipline / Course Registration Form
        define("A_LANG_DISCIPLINES_COURSE1","Discipline/Course");
        define("A_LANG_DISCIPLINES_COURSE_DISCIPLINES","Disciplines");
        define("A_LANG_DISCIPLINES_COURSE2","Courses");
        define("A_LANG_DISCIPLINES_COURSE_SAVE","Save");
        define("A_LANG_DISCIPLINES_COURSE_MSG1","Register the DISCIPLINE(S) and COURSE(S) to have access to this item.");
        define("A_LANG_DISCIPLINES_COURSE_MSG2","It was not possible to establish bond of disciplines with the courses due the imperfection of connection with the data base.");
        define("A_LANG_DISCIPLINES_COURSE_MSG3","Inform the course's name");
        define("A_LANG_DISCIPLINES_COURSE_MSG4","The course already exists");
        define("A_LANG_DISCIPLINES_COURSE_MSG5","Select a course to update");

	// Access authorization form
	define("A_LANG_LIBERATE_USER1","Registration / Learner");   // to the teacher and administrator
        define("A_LANG_LIBERATE_USER2","Authorize teacher / Authorship"); // to the adiministrator
        define("A_LANG_LIBERATE_MSG1","Under development ... ");


        // Discipline liberation form
	define("A_LANG_LIBERATE_DISCIPLINES","Liberate Discipline");
        define("A_LANG_LIBERATE_DISCIPLINES_MSG1","Under development... ");

        // ======================= Topics ==========================

        // Entrance / topics
        define("A_LANG_ENTRANCE_TOPICS_DISCIPLINES","Discipline");
        // **** REVIEW "Structuralize topic"
        define("A_LANG_ENTRANCE_TOPICS_STRUTURALIZE","Structuralize Topics");
        define("A_LANG_ENTRANCE_TOPICS_MSG1","It was not possible to read the disciplines.");
        define("A_LANG_ENTRANCE_TOPICS_MSG2","* It will only be available in this form the disciplines that are related to at least one course.");


       // Guide - Topics List
        define("A_LANG_TOPIC_GENERATE_CONTENT","Generate Content");
        //ear
        define("A_LANG_TOPIC_LIST","Concepts");
        define("A_LANG_TOPIC_MAINTENANCE","Concept Maintenance");
        define("A_LANG_TOPIC_EXEMPLES","Examples");
        define("A_LANG_TOPIC_EXERCISES","Exercises");
        define("A_LANG_TOPIC_COMPLEMENTARY","Complementary Material");

        // Guide - topic maintenance
  	define("A_LANG_TOPIC_NUMBER","Concept Number");
  	define("A_LANG_TOPIC_DESCRIPTION","Concept Name");
  	define("A_LANG_TOPIC_SUMARIZED_TOPIC_DESCRIPTION","Sumarized concept description");
  	define("A_LANG_TOPIC_WORDS_KEY","Keywords");
  	define("A_LANG_TOPIC_PREREQUISIT","Prerequisite");
  	define("A_LANG_TOPIC_COURSE","Course");
	define("A_LANG_TOPIC_FILE1","File");
	define("A_LANG_TOPIC_FILE2","File(s)");
	define("A_LANG_TOPIC_MAIN_FILE","Main File");
	define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE","Associated File(s)");
	define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_STATUS","Status");
	define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_UPLOAD","");
        define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG1","load to the server");
	define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG2","loaded to the server");
	// buttons
	define("A_LANG_TOPIC_SAVE","Save");
        define("A_LANG_TOPIC_SEND","Send");
        define("A_LANG_TOPIC_SEARCH","Search");
	define("A_LANG_TOPIC_INSERT_TOPIC_SAME_LEVEL","Insert concept at the same level");
        define("A_LANG_TOPIC_INSERT_SUB_TOPIC","Insert subconcept");
	define("A_LANG_TOPIC_DELETE","Delete");
	define("A_LANG_TOPIC_PROMOTE","Promote");
	define("A_LANG_TOPIC_LOWER","Lower");

	// Guide - samples
	//define("A_LANG_TOPIC_EXEMPLES_NUMBER_TOPIC","Topic Number");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Example Description");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Complexity Level");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Course");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Delete");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Description");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Level");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Course");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","File");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Associated File(s)");
	// buttons
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Save");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Delete");


  	// Examples
 	// Complexity Level
	// Description
	// Level
	// Associated Files
        // Examples Description
  	// Exercises
        // Exercise Description
  	// Complementary Material
        // Menu
	// Authorize Access
	// Liberate disciplines
	// New user
        // Update registration
        // ************************************************************************ 	// * Form - Authorship                                                                     *
 	// ************************************************************************	// Liberated Disciplines

	// Register

	// Waiting Register

        // Authorize Access

        // Navegation

        // Courses Screen
        // ************************************************************************ 	// * Form - Demonstration
 	// ************************************************************************
     define("A_LANG_DEMO","AdaptWeb Environment demonstration discipline - it does not allow insertions and alterations");
	//
	// Regional Specific Date texts
	//
	// A little help for date manipulation, from PHP manual on function strftime():
	//
	// %a - abbreviated weekday name according to the current locale
	// %A - full weekday name according to the current locale
	// %b - abbreviated month name according to the current locale
	// %B - full month name according to the current locale
	// %c - preferred date and time representation for the current locale
	// %C - century number (the year divided by 100 and truncated to an integer, range 00 to 99)
	// %d - day of the month as a decimal number (range 01 to 31)
	// %D - same as %m/%d/%y
	// %e - day of the month as a decimal number, a single digit is preceded by a space
	//      (range ' 1' to '31')
	// %h - same as %b
	// %H - hour as a decimal number using a 24-hour clock (range 00 to 23)
	// %I - hour as a decimal number using a 12-hour clock (range 01 to 12)
	// %j - day of the year as a decimal number (range 001 to 366)
	// %m - month as a decimal number (range 01 to 12)
	// %M - minute as a decimal number
	// %n - newline character
	// %p - either `am' or `pm' according to the given time value, or the corresponding strings for
	//      the current locale
	// %r - time in a.m. and p.m. notation
	// %R - time in 24 hour notation
	// %S - second as a decimal number
	// %t - tab character
	// %T - current time, equal to %H:%M:%S
	// %u - weekday as a decimal number [1,7], with 1 representing Monday
	// %U - week number of the current year as a decimal number, starting with the first Sunday as
	//      the first day of the first week
	// %V - The ISO 8601:1988 week number of the current year as a decimal number, range 01 to 53,
	//      where week 1 is the first week that has at least 4 days in the current year, and with
	//      Monday as the first day of the week.
	// %W - week number of the current year as a decimal number, starting with the first Monday as
	//      the first day of the first week
	// %w - day of the week as a decimal, Sunday being 0
	// %x - preferred date representation for the current locale without the time
	// %X - preferred time representation for the current locale without the date
	// %y - year as a decimal number without a century (range 00 to 99)
	// %Y - year as a decimal number including the century
	// %Z - time zone or name or abbreviation
	// %% - a literal `%' character
	//
	// Note: A_LANG_DATESTRING is used for Articles and Comments Date
	//       A_LANG_LINKSDATESTRING is used for Web Links creation Date
	//       A_LANG_DATESTRING2 is used for Older Articles box on Home
	//

	define("A_LANG_CODE","pt-BR");
	define("A_LANG_CHACTERSET","ISO-8859-1");

	define("A_LANG_NAME_pt_BR","Brazilian Portuguese");
	define("A_LANG_NAME_en_US","English");
	define("A_LANG_NAME_es_ES","Spanish");
	define("A_LANG_NAME_fr_FR","French");
	define("A_LANG_NAME_ja_JP","Japanese");

	define('A_LANG_TIMEZONES','IDLW NT HST YST PST MST CST EST AST GMT-3:30 GMT-3 AT WAT GMT CET EET BT GMT+3:30 GMT+4 GMT+4:30 GMT+5 GMT+5:30 GMT+6 WAST CCT JST ACS GST GMT+11 NZST');
	define('A_LANG_TZOFFSETS','0 1 2 3 4 5 6 7 8 8.5 9 10 11 12 13 14 15 15.5 16 16.5 17 17.5 18 19 20 21 21.5 22 23 24');
	define('A_LANG_DATEBRIEF','%b %d, %Y');
	define('A_LANG_DATELONG','%A, %B %d, %Y');
	define('A_LANG_DATESTRING','%A, %B %d @ %H:%M:%S %Z');

	define('A_LANG_DATESTRING2','%A, %B %d');
	define('A_LANG_DATETIMEBRIEF','%b %d, %Y - %I:%M %p');
	define('A_LANG_DATETIMELONG','%A, %B %d, %Y - %I:%M %p %Z');

	define('A_LANG_LINKSDATESTRING','%d-%b-%Y');
	define('A_LANG_TIMEBRIEF','%I:%M %p');
	define('A_LANG_TIMELONG','%I:%M %p %Z');

	define('A_LANG_DAY_OF_WEEK_LONG','Sunday Monday Tuesday Wednesday Thursday Friday Saturday');
	define('A_LANG_DAY_OF_WEEK_SHORT','Sun Mon Tue Wed Thu Fri Sat');
	define('A_LANG_MONTH_LONG','January February March April May June July August September October November December');
	define('A_LANG_MONTH_SHORT','Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec');

	//************UFRGS******************
	//Index
	  //About
	define('A_LANG_INDEX_ABOUT_INTRO','The AdaptWeb Environment is turned to the authorship and adaptative presentation integranting disciplines of EAD courses in the Web. The objective of AdaptWeb is to allow to the adequacy of tactics and forms of contents presentation for learners of different courses of graduation and with different styles of learning, making possible different forms of presentation of each content, adequating the form to each course and to the individual preferences of the participant learners.<br> The AdaptWeb Environment was initially developed by UFRGS and UEL researchers, through the Electra e AdaptWeb projects, with support of CNPq. Visit the project\'s site: <a href=\'http://www.inf.ufrgs.br/adapt/adaptweb\' target=\'_blank\'>http://www.inf.ufrgs.br/adapt/adaptweb</a>');
	define('A_LANG_INDEX_ABOUT_ENVACCESS','Environment access');
	define('A_LANG_INDEX_ABOUT_TINFO','to make available the content of his disciplines, the teacher must effect one access request register in cadastre at the Authorship Environment');
	define('A_LANG_INDEX_ABOUT_LINFO','to have access to his discipline(s), the learner must effect a register in cadastre at the Navigation Environment and request the register at the discipline(s) related to his course');
	  //Demo
	define('A_LANG_INDEX_DEMO_AENVIRONMENT','Authorship Environment');
	define('A_LANG_INDEX_DEMO_FDESC1', 'Create discipline(s)');
	define('A_LANG_INDEX_DEMO_NENVIRNMENT','Navigation Environment');
	define('A_LANG_INDEX_DEMO_FDESC2', 'Watch discipline(s)');
	define('A_LANG_INDEX_DEMO_TOLOG','Access the environment and navigate through the discipline effecting login at the environment with the data below:');
	define('A_LANG_INDEX_DEMO_LOGIN','login: demo@inf.ufrgs.br');
	define('A_LANG_INDEX_DEMO_PASS', 'password: 123');
	define('A_LANG_INDEX_DEMO_OBSERVATION','Observation');
	define('A_LANG_INDEX_DEMO_OBSERVATIONTEXT', 'After effecting login, the access to the teacher environment and to the learner environment will be liberated by the "Teacher Environment" and the "Learner Environment" options at the home screen of AdaptWeb.');
	  //Apresenta��o
	define('A_LANG_INDEX_PRESENTATION_INFABOUT', 'Project informations:');
	define('A_LANG_DOWNLOAD', 'Download');
	define('A_LANG_PUBLICATIONS', 'Publications');
	define('A_LANG_INDEX_PRESENTATION_AVAILABLEAT','Available at:');
	

	
	//Ambiente do Aluno - Entrada
	  //Principal
	define('A_LANG_LENVIRONMENT','Learner environment');
	define('A_LANG_LENVIRONMENT_DESCRIPTION', 'This environment allows the learner to have access to the instrucional material of his teacher(s), adapted to his profile.');	
	define('A_LANG_LENVIRONMENT_WARNING', 'to have access to his discipline(s), the learner must intially effect his register in cadastre so that it will be possible login the environment to request the register of his discipline(s). Only after the access liberation by the responsible teacher, the learner will have access to the discipline(s).');

	define('A_LANG_LENVIRONMENT_WATCH_LIBERATED', 'Liberated Disciplines');
	define('A_LANG_LENVIRONMENT_WATCH_MINE', 'Disciplines of my authorship (navigate by course)');
	define('A_LANG_LENVIRONMENT_WATCH_MY', 'My Disciplines');
	define('A_LANG_LENVIRONMENT_WATCH_NOT', 'There are no registered disciplines');
	define('A_LANG_LENVIRONMENT_WATCH_WARNING', 'Request registration in cadastre to have access to the disciplines.');
	define('A_LANG_LENVIRONMENT_WATCH_VISUAL', 'Visualize discipline to each course');
	define('A_LANG_LENVIRONMENT_WATCH_CREATE', 'Create the disciplines and generate content (Generate Content) at the Teacher Environment (<b>Structuralize Content => Concept List </b> item) to have access to the navigation environment');
	define('A_LANG_LENVIRONMENT_WATCH_OBS', 'to visualize the disciplines of your authorship and test the navigation environment, it is not necessary to Liberate Discipline.');
	define('A_LANG_LENVIRONMENT_REQUEST', 'Request Registration');
	define('A_LANG_LENVIRONMENT_REQUEST_REQ', 'Request');
	define('A_LANG_LENVIRONMENT_REQUEST_NOTC', 'There is no registered course');
	define('A_LANG_LENVIRONMENT_REQUEST_NOTD', 'There is no registered discipline to the selected course');
	define('A_LANG_LENVIRONMENT_REQUEST_SELECT', 'Select to request registration:');
	define('A_LANG_LENVIRONMENT_REQUEST_MATR', 'Select to request registration:');
	define('A_LANG_LENVIRONMENT_WAITING', 'Waiting Registration');
	define('A_LANG_LENVIRONMENT_WAITING_NOT', 'There aro no registrations in cadastre');
	define('A_LANG_LENVIRONMENT_WAITING_WARNING', 'Request regsitration to have access to the disciplines:');
	define('A_LANG_LENVIRONMENT_NAVTYPE', 'Navigation Type');
	define('A_LANG_LENVIRONMENT_FORCOURSE', 'for the course');
	define('A_LANG_LENVIRONMENT_NETCONECTION', 'Net Conection');
	define('A_LANG_LENVIRONMENT_SPEED', 'The speed is');
	define('A_LANG_LENVIRONMENT_NAVIGATION', 'Navegation');
	define('A_LANG_LENVIRONMENT_TUTORIAL', 'Tutorial');
	define('A_LANG_LENVIRONMENT_FREE', 'Free');
	
	//Ambiente do professor
	define('A_LANG_TENVIRONMENT_DESCRIPTION', 'The authorship module allows the author to make available suitable contents to different learner profiles. Through the authorship process, the author will be able to make available the content of his lessons in an only structure adapted for the different courses. In this stage the author must register the discipline and for which courses he desires to make it available. After these cadastres, the topic archives of the content must be inserted, relating them with each topic of the discipline. Moreover, the author must inform the description of the topic, its prerequisite and for which courses he desires to make the content available. The author will be able to insert exercises, examples and adicional materials for each topic.');
	define('A_LANG_TENVIRONMENT_AUTHOR', 'Autorship');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR1', 'An error ocurred when registering the user in this discipline. This user must be already registered or ocurred an error at the server.');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR2', 'An error ocurred when deleting the user register from this discipline. This user must not be registered or ocurred an error at the server.');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_NOT', 'There is no registered course');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECT', 'Select the course');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_NOTD', 'There is no registered discipline to this course');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECTD', 'Select the discipline');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_LIST', 'Learners List:');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_MATR', 'Registered Learners:');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_BACK', 'Back');
	define('A_LANG_TENVIRONMENT_LIBERATE_DESCRIPTION', '* it will only be available to be liberated the disciplines that are with the corret topic. To verify the disciplines topic, access the <b>TOPIC</b>guide at the <b>STRUCTURALIZE CONTENT</b> item and use the <b>GENERATE CONTENT</b> button.');
	define('A_LANG_TENVIRONMENT_LIBERATE_LIB', 'Liberate<br> discipline:');
	define('A_LANG_TENVIRONMENT_LIBERATE_LIB_DIS', 'Liberated<br> disciplines:');
	define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE', 'Move Topic');
	define('A_LANG_TENVIRONMENT_ESTRUCT_EXCLUDE', 'Delete Topic');
	define('A_LANG_TENVIRONMENT_ESTRUCT_SELECT', 'Select the topic you wish to <b>MOVE</b> or <b>DELETE</b>');
	define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE_AFTER', '<B>MOVE</B> the selected topic after which topic?');
	define('A_LANG_TENVIRONMENT_ESTRUCT_CANCEL', 'The MOVE or DELETE operation will cause prerequisite cancellation');
	define('A_LANG_TENVIRONMENT_ESTRUCT_NOT_POSSIBLE', 'It was not possible to read the array from the Data Base');	
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND', 'Prerequisite Legend:');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND1', 'To select many prerequisite use the CTRL key');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND2', 'To remove the prerequisite from the selection use the CTRL key');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND3', 'The <font class=buttonselecionado>dark blue</font> prerequisites are selected');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND4', 'The <font class=buttonpre>blue</font> background indicates the current prerequisite (in case of losing the selection)');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND5', 'The <font class=buttonpreautomatico>yellow</font> background indicates the current automatic prerequisite');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NUMBER', 'Topic number:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NAME', 'Example name:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_DESCRIPTION', 'Example description:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_KEY', 'Example keywords:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_LEVEL', 'Complexity Level:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NOCLASS', 'No classification');
	define('A_LANG_TENVIRONMENT_EXAMPLES_EASY', 'Easy');
	define('A_LANG_TENVIRONMENT_EXAMPLES_MEDIUM', 'Medium');
	define('A_LANG_TENVIRONMENT_EXAMPLES_COMPLEX', 'Complex');
	define('A_LANG_TENVIRONMENT_EXAMPLES_DSC', 'Description');
	define('A_LANG_TENVIRONMENT_EXAMPLES_LVL', 'Level');
	define('A_LANG_TENVIRONMENT_EXERCISES_NAME', 'Exercise name:');
	define('A_LANG_TENVIRONMENT_EXERCISES_DESCRIPTION', 'Exercise description:');
	define('A_LANG_TENVIRONMENT_EXERCISES_KEY', 'Exercise keywords:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_NAME', 'Complementary Material <br> Name:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_DESCRIPTION', 'Complementary Material <br> Description:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_KEY', 'Complementary Material <br> Keywords:');
	define('A_LANG_TENVIRONMENT_ACCESS', 'Authorship Environment Access');
	define('A_LANG_TENVIRONMENT_WAIT', 'Wait for the environment\'s administrator to liberate the access to the authorship environment.');
	define('A_LANG_TENVIRONMENT_COMMENT', 'the access to the authorship environment is managed by the AdaptWeb\'s environment administrator. After effecting the cadastre in registre <br> it is necessary the administrator\'s authorization to have access to the authorship environment.');
	define('A_LANG_TENVIRONMENT_DEMO', 'Environment\'s Demonstration');
	define('A_LANG_TENVIRONMENT_ASSOCIATED_FILES','Obs.: Associated files must be send to the server through the "topic maintence" section.');
	
	//Op��es de Root
	define('A_LANG_ROOT_NOT_AUTH', 'Unauthorized<br> teachers:');
	define('A_LANG_ROOT_AUTH', 'Authorized<br> teachers:');
	define('A_LANG_ROOT_DEV', 'This option is under development.');

	//Outros
	define('A_LANG_DATA', 'Date');
	define('A_LANG_WARNING', 'Warning');
	define('A_LANG_COORDINATOR', 'COORDINATOR');
	define('A_LANG_DATABASE_PROBLEM', 'There are problems with the Data Base');
	define('A_LANG_FAQ', 'Frequently Asked Questions');
	define('A_LANG_WORKS', 'Works');
	define('A_LANG_NOT_DISC', 'There was not possible to present the registered disciplines');
	define('A_LANG_NO_ROOT', 'It was not possible to insert the Root user');
	define('A_LANG_MIN_VERSION', 'The minimum supported data base version is [');
	define('A_LANG_INSTALLED_VERSION', '], and the installed version is [');
	define('A_LANG_ERROR_BD', 'It was not possible to record the matrix in the Data Base');
	define('A_LANG_DISC_NOT_FOUND', 'Discipline\'s name not found or blank');
	define('A_LANG_SELECT_COURSE', 'select the course(s) in the topic <b>');
	define('A_LANG_TOPIC_REGISTER', 'Topic Register');
	define('A_LANG_SUMARIZED_DESCRIPTION', 'Sumarized description<br> of the topic');
	define('A_LANG_LOADED', 'loaded to the server');
	define('A_LANG_FILL_DESCRIPTION', 'Fill the description field of the material');
	define('A_LANG_WORKS_SEND_DATE', 'Sending date');
	define('A_LANG_SEARCH_RESULTS', 'Search Results');
	define('A_LANG_SEARCH_SYSTEM', 'Search System');
	define('A_LANG_SEARCH_WORD', 'Searched word');
	define('A_LANG_SEARCH_FOUND', 'Were found ');
	define('A_LANG_SEARCH_OCCURRANCES', ' occurrences of ');
	define('A_LANG_SEARCH_IN_TOPIC', ' in TOPIC');
	define('A_LANG_SEARCH_FOUND1', 'Was found ');
	define('A_LANG_SEARCH_OCCURRANCE', ' occurrence of ');
	define('A_LANG_SEARCH_NOT_FOUND', 'Were found no occurrences of ');
	define('A_LANG_SEARCH_CLOSE', 'Close');
	define('A_LANG_TOPIC', 'Topic');
	define('A_LANG_SHOW_MENU', 'Show Menu');
	define('A_LANG_CONFIG', 'Configurations');
	define('A_LANG_MAP', 'Map');
	define('A_LANG_PRINT', 'Print this page?');
	define('A_LANG_CONNECTION', 'It was not possible to establish a connection with the MySQL data base server. Please contact the administrator.');
	define('A_LANG_CONTACT', 'Please contact the administrator');
	define('A_LANG_HELP_SYSTEM', 'Help System');
	define('A_LANG_CONFIG_SYSTEM', 'System Configurations');
	define('A_LANG_CONFIG_COLOR', 'Configure background color');
	define('A_LANG_NOT_CONNECTED', 'Not connected!');
	define('A_LANG_DELETE_CADASTRE', 'Learner\'s Cadastres Exclusion');
	define('A_LANG_HOME', 'Home');
	define('A_LANG_YOU_HAVE', 'You have ');
	define('A_LANG_TO_CONFIRM', ' registration(s). To confirm this exclusion, click at ');
	define('A_LANG_CONFIRM', ' CONFIRM');
	define('A_LANG_EXAMPLE_LIST', 'Example List');
	define('A_LANG_COMPLEXITY_LEVEL_EASY', 'Easy Complexity Level');
	define('A_LANG_COMPLEXITY_LEVEL_MEDIUM', 'Medium Complexity Level');
	define('A_LANG_COMPLEXITY_LEVEL_COMPLEX', 'Hard Complexity Level');
	define('A_LANG_EXERCISES_LIST', 'Exercises List');
	define('A_LANG_REGISTER_MAINTENENCE', 'Registers Maintenence');
	define('A_LANG_REGISTER_RELEASE', 'Registers Liberation');
	define('A_LANG_SITE_MAP', 'Site Map');
	define('A_LANG_COMPLEMENTARY_LIST', 'Complementary Material List');
	define('A_LANG_DELETE_TOPIC', 'Delete topic ');
	define('A_LANG_AND', ' and ');
	define('A_LANG_SONS', ' sons');
	define('A_LANG_CONFIRM_EXCLUSION', 'Are you sure you want to delete the discipline');
	define('A_LANG_YES', '  Yes  ');
	define('A_LANG_NO', '  No  ');
	
	//Gera��o de conte�do
	define('A_LANG_GENERATION_END', 'End of the xml file of topic\'s elements generation');
	define('A_LANG_GENERATION_CREATED', '');
	define('A_LANG_GENERATION_XML', ' xml files were created.');
	define('A_LANG_GENERATION_END_STRUCT', 'End of the xml file of topic\'s structure generation');
/*	define('A_LANG_GENERATION_MISSING_FILE', 'Is needed the topic file of the ');
	define('A_LANG_GENERATION_TH_TOPIC', 'th topic');
	define('A_LANG_GENERATION_MISSING_COURSE', 'Is needed the course of the ');
	define('A_LANG_GENERATION_NO_ASOC', 'Topic with no associated course');
	define('A_LANG_GENERATION_MISSING_EXERC_ID', 'Is needed the exercise\'s identification of the ');
	define('A_LANG_GENERATION_TH_EXERCISE', 'th exercise and ');
	define('A_LANG_GENERATION_MISSING_EXERC_DESCRIPTION', 'Is needed the exercise\'s description of the ');
	define('A_LANG_GENERATION_MISSING_EXERC_FILE', 'Is needed the exercise\'s file of the ');
	define('A_LANG_GENERATION_MISSING_EXERC_COMPLEXITY', 'Is needed the exercise\'s complexity of the ');
	define('A_LANG_GENERATION_TH_EXAMPLE', 'th example and ');
	define('A_LANG_GENERATION_MISSING_EXAMP_ID', 'Is needed th example\'s identification of the ');
	define('A_LANG_GENERATION_MISSING_EXAMP_DESCRIPTION', 'Is needed the example\'s description of the ');
	define('A_LANG_GENERATION_MISSING_EXAMP_FILE', 'Is needed the example\'s file of the ');
	define('A_LANG_GENERATION_MISSING_EXAMP_COMPLEXITY' ,'Is needed the example\'s complexity of the ');
	define('A_LANG_GENERATION_TH_MATCOMP', 'th complementary material and ');
	define('A_LANG_GENERATION_MISSING_MATCO_ID', 'Is needed the complementary material\'s identification of the ');
	define('A_LANG_GENERATION_MISSING_MATCO_DESCRIPTION', 'Is needed the complementary material\'s description of the ');
	define('A_LANG_GENERATION_MISSING_MATCO_FILE', 'Is needed the complementary material\'s file of the ');
*/	define('A_LANG_GENERATION_VERIFY', 'Authorship data verification');
	define('A_LANG_GENERATION_PROBLEMS', 'Problems in the registering of discipline');
/*	define('A_LANG_GENERATION_MISSING_TOPIC_NUMBER', 'Is needed the topic number to the ');
	define('A_LANG_GENERATION_MISSING_TOPIC_DESCRIPTION', 'Is needed the topic description to the ');
	define('A_LANG_GENERATION_MISSING_TOPIC_ABREV', 'Is needed the topic abbreviation to the ');
	define('A_LANG_GENERATION_MISSING_TOPIC_KEYWORD', 'Is needed the topic keyword to the ');
*/	define('A_LANG_GENERATION_RESULT_XML', 'XML Generation Result');
	define('A_LANG_GENERATION_NO_ERROR', 'THERE IS NO ERROR');
	define('A_LANG_GENERATION_SUCCESS', 'THE FILES OF THE COURSE WERE SUCCESSFULLY GENERATED');
	define('A_LANG_GENERATION_DATA_LACK', 'XML files were not generated by data lack');
	define ('A_LANG_GENERATION_TOPIC','Topic');	
	define ('A_LANG_GENERATION_EXAMPLE','Example');	
	define ('A_LANG_GENERATION_EXERCISE','Exercise');	
	define ('A_LANG_GENERATION_COMPLEMENTARY','Complementary Material');	
	define ('A_LANG_GENERATION_TOPIC_NUMBER','Topic Number');	
	define ('A_LANG_GENERATION_TOPIC_NAME','Topic Name');	
	define ('A_LANG_GENERATION_DESCRIPTION','Description');	
	define ('A_LANG_GENERATION_KEY_WORD','Keyword');	
	define ('A_LANG_GENERATION_COURSE','Course');	
	define ('A_LANG_GENERATION_PRINC_FILE','Main File');	
	define ('A_LANG_GENERATION_ASSOC_FILE','Associated Files');	
	define ('A_LANG_GENERATION_MISSING','Missing');	
	define ('A_LANG_GENERATION_NO_PROBLEM','Ok');	
	define ('A_LANG_GENERATION_NUMBER','Number');	
	define ('A_LANG_GENERATION_COMPLEXITY','Complexity');	
	define ('A_LANG_GENERATION_NOT_EXIST','Do not have');	

	//Reposit�rio
	define ('A_LANG_MNU_REPOSITORY','Repository');	
	define ('A_LANG_PRESENTATION','Presentation');
	define ('A_LANG_EXPORT','Disciplines Exportation');
	define ('A_LANG_IMPORT','Disciplines Importation');
	define ('A_LANG_OBJECT_SEARCH','Search Learning Objects');
	define ('A_LANG_EXP_TEXT', 'Send your disciplines to the Repository, so the other teachers can use it too.');
	define ('A_LANG_IMP_TEXT', 'Use available disciplines in your AdaptWeb environment.');
	define ('A_LANG_SCH_TEXT', 'The AdaptWeb disciplines\' data were also indexed in the Learning Objects format, using metadata of the LOM (Learning Object Metadata) standard. Make searches by learning objects envolving disciplines, topics (concepts) and repository files.');
	define ('A_LANG_REP_TEXT', 'Welcome to the AdaptWeb\'s Disciplines Repository.<br>Through this repository, AdaptWeb users from different locations will be able to share data to create their disciplines. Here you can make the importation and exportation of disciplines, and search for learning objects.');
	define ('A_LANG_REP_SERVER', 'Repository Server');
	define ('A_LANG_REP_ADAPT_SERVER', 'AdaptWeb Server');
	define ('A_LANG_ADAPTWEB', 'AdaptWeb');
	define ('A_LANG_REP_DISC', 'Disciplines Repository');
	define ('A_LANG_REP_JUNE', 'June of 2004');
	define ('A_LANG_REP_SUGESTIONS', 'Doubts, sugestions, claimings');
	define ('A_LANG_REP_SELECT', 'Select a discipline');
	define ('A_LANG_REP_EXP_SUCCESS', 'Exportation succesfully done!');
	define ('A_LANG_REP_EXP_ERROR', 'Error exporting discipline!');
	define ('A_LANG_REP_EXP_TEXT', 'Through this interface you will be able to let you AdaptWeb disciplines available so that other people may use it.<br>Select the discipline in the list above, specify a description to her and click in &quot;Export&quot;.');
	define ('A_LANG_REP_EXPORT', 'Export');
	define ('A_LANG_REP_SELECT_COURSE', 'Select a Course');
	define ('A_LANG_REP_IMP_SUCCESS', 'Importation successfully done!');
	define ('A_LANG_REP_IMP_ERROR', 'Error importing discipline!');
	define ('A_LANG_REP_INVALID_PAR', 'Invalid Parameters!');
	define ('A_LANG_REP_IMP_TEXT', 'Through this interface you will be able to copy available disciplines of the repository to your authoship environment on AdaptWeb!<br>Select the discipline on the list above, specify the course which it will be associated, click in &quot;Import&quot;.');
	define ('A_LANG_REP_DISC_IN_REP', 'Disciplines in Repository');
	define ('A_LANG_REP_MY_COURSES', 'My Courses');
	define ('A_LANG_REP_OTHER_NAME', 'Import with another name');
	define ('A_LANG_REP_IMPORT', 'Import');
	define ('A_LANG_REP_DISC_DATA', 'Discipline Data');
	define ('A_LANG_REP_SEARCH', 'Digit a search string and choose the metadata that must be included in the search:');
	define ('A_LANG_REP_SCH_TITLE', 'Title');
	define ('A_LANG_REP_SCH_SELECT', 'Select...');
	define ('A_LANG_REP_AGGREG', 'Aggregation Level');
	define ('A_LANG_REP_SCH_RAW', 'Raw Data (files)');
	define ('A_LANG_REP_SCH_TOPIC', 'Topic');
	define ('A_LANG_REP_SCH_DISC', 'Discipline');
	define ('A_LANG_REP_SCH_COURSE', 'Course');
	define ('A_LANG_REP_SCH_SEARCH', 'Search');
	define ('A_LANG_REP_SCH_ERROR', 'Error');
	define ('A_LANG_REP_SCH_NO_REG', 'No register found!');
	define ('A_LANG_REP_SCH_REG', 'register found');
	define ('A_LANG_REP_SCH_REGS', 'registers found');
	define ('A_LANG_REP_DISC_NOT_FOUND', 'Discipline not found');
	define ('A_LANG_COURSES', 'Course(s)');
	define ('A_LANG_EXP_BY', 'Exported by');
	define ('A_LANG_ERROR_SELECT_BD', 'Error when selecting the data base');
?>