<?
/* -----------------------------------------------------------------------
*  AdaptWeb - Projeto de Pesquisa
*     UFRGS - Instituto de Inform�tica
*       UEL - Departamento de Computa��o
* -----------------------------------------------------------------------
*       @package AdaptWeb
*     @subpakage Linguagem
*          @file idioma/pt-BR/geral.php
*    @desciption Arquivo de tradu��o - Portugu�s/Brasil
*         @since 17/08/2003
*        @author Veronice de Freitas (veronice@jr.eti.br)
*   @Translation Veronice de Freitas
* -----------------------------------------------------------------------
*/


  // ************************************************************************
//Dados da interface da ferramenta de autoria
  define("A_LANG_VERSION","Version");
  define("A_LANG_SYSTEM","AdaptWeb System");
  define("A_LANG_ATHORING","Authoring Tool");
  define("A_LANG_USER","User Name: ");
  define("A_LANG_PASS","Password: ");
  define("A_LANG_NOTAUTH","<not autenticated>");
  define("A_LANG_HELP","Help");
  define("A_LANG_DISCIPLINE","Discipline");
  define("A_LANG_NAVEGATION","Where I am: ");

// Contexto
// observa��o - arquivo p_contexto_navegacao.php
       // ************************************************************************ // * Menu                                                                                 *
  // ************************************************************************
// Home
define("A_LANG_MNU_HOME","Home");
define("A_LANG_MNU_NAVIGATION","Student's Module");
define("A_LANG_MNU_AUTHORING","Teacher's Module");
define("A_LANG_MNU_UPDATE_USER","Update User");
define("A_LANG_MNU_EXIT","Exit");
define("A_LANG_MNU_LOGIN","Login");
define("A_LANG_MNU_ABOUT","About");
define("A_LANG_MNU_DEMO","Demonstration");
define("A_LANG_MNU_PROJECT","Project");
define("A_LANG_MNU_FAQ","FAQ");
define("A_LANG_MNU_RELEASE_AUTHORING","Release Authoring");
define("A_LANG_MNU_BACKUP","Backup");
define("A_LANG_MNU_NEW_USER","New User");
define("A_LANG_MNU_APRESENTATION","Presentation");
define("A_LANG_MNU_MY_ACCOUNT","My Account");
define("A_LANG_MNU_MANAGE","Management");

define("A_LANG_MNU_GRAPH","Access");
define("A_LANG_MNU_TODO","it's in developing");
// Alterado por Carla-estagio UDESC 2008/02 define("A_LANG_SYSTEM_NAME","AdaptWeb - Ambiente de Ensino-Aprendizagem Adaptativo na Web ");
define("A_LANG_SYSTEM_NAME","AdaptWeb");

define("A_LANG_SYSTEM_COPYRIGHT","Licensed under the GNU GENERAL PUBLIC LICENSE Version 2");
        // Autoria
        define("A_LANG_MNU_COURSE","Course");
        define("A_LANG_MNU_DISCIPLINES","Discipline");
        define("A_LANG_MNU_DISCIPLINES_COURSE","Discipline/Course");
        define("A_LANG_MNU_STRUTURALIZE_TOPICS","Estruturalize Topics");
        define("A_LANG_MNU_ACCESS_LIBERATE","Examine applications for registration");
        define("A_LANG_MNU_LIBERATE_DISCIPLINES","Liberate Disciplines");
        define("A_LANG_MNU_WORKS","Jobs of students");
     	 define("A_LANG_MNU_LOG","Log Analysis");
	 define("QUESTIONARIO","Questionnaire");
	 define("QS_ROOT","Results QS");


// Estudante
define("A_LANG_MNU_DISCIPLINES_RELEASED","Watch Discipline");
define("A_LANG_MNU_SUBSCRIBE","Request Registration");
define("A_LANG_MNU_WAIT","waiting for Registration");
define("A_LANG_MNU_UPLOAD_FILES","Submit Work");
define("A_LANG_MNU_MESSAGE","Notices");
// ************************************************************************ // * Formul�rio Principal                                                                          *
  // ************************************************************************
  // Apresenta��es
define("A_LANG_ORGANS","Participating Institutions");
define("A_LANG_RESEARCHES","Researchers");
define("A_LANG_AUTHORS","Authors");

  // ************************************************************************ // * Formul�rio - Autoria                                                                     *
  // ************************************************************************
// Apresenta��o
define("A_LANG_MNU_ORGANS","");
define("A_LANG_MNU_RESEARCHES","");
define("A_LANG_MNU_AUTHORS","");

        // Formul�rio de solicita��o de acesso
        define("A_LANG_REQUEST_ACCESS","Request Access");
        define("A_LANG_REQUEST_ACCESS_USER_TYPE","User Type *");
        define("A_LANG_REQUEST_ACCESS_TYPE_STUDANT","Student");
        define("A_LANG_REQUEST_ACCESS_TYPE_TEACHES","Teacher");
        define("A_LANG_REQUEST_ACCESS_NAME","Name *");
        define("A_LANG_REQUEST_ACCESS_EMAIL","E-mail *");
        define("A_LANG_REQUEST_ACCESS_PASS","Passaword *");
        define("A_LANG_REQUEST_ACCESS_PASS2","Password Confirmation *");
        define("A_LANG_REQUEST_ACCESS_INSTITUTION_EDUCATION","Institution of Education");
        define("A_LANG_REQUEST_ACCESS_COMMENT","Notice");
        define("A_LANG_REQUEST_ACCESS_LANGUAGE","Language");
        define("A_LANG_REQUEST_ACCESS_MSG1","Unable to connect to the database");
	 define("A_LANG_REQUEST_ACCESS_MSG2","Tell other e-mail, it's already registered");
        define("A_LANG_REQUEST_ACCESS_MSG3","Unable to send your datas ");
        define("A_LANG_REQUEST_ACCESS_MSG4","Senha inv�lida (Enter the same password in the fields PASSWORD  and CONFIRM PASSWORD)");
        define("A_LANG_REQUEST_ACCESS_SAVE","Save");
	// Acrescenta mensagem para este formulario - Carla -est�gio UDESC 2008/02
	define("A_LANG_REQUEST_ACCESS_MSG5","Fields that have * must be completed");
	define("A_LANG_REQUEST_ACCESS_MSG6","The Email field must be completed");
	define("A_LANG_REQUEST_ACCESS_MSG7","The Name field must be completed");
	define("A_LANG_REQUEST_ACCESS_MSG8","The password field is completed with a invalid value");
	define("A_LANG_REQUEST_ACCESS_MSG9","The fields Password and Confirmation Password must be complete with the same datas");
	define("A_LANG_REQUEST_ACCESS_MSG10","Your registration was made successfully");
	define("A_LANG_REQUEST_ACESS_OBSERVACAO", "Note: Fields that have * must be completed.");
	// Fim das alteracoes Carla


        //Formulario de login
        define("A_LANG_LOGIN2","Login");
        define("A_LANG_EMAIL","E-mail");
        define("A_LANG_LOGIN_PASS","Password");
       define("A_LANG_LOGIN_START","Enter");
        define("A_LANG_LOGIN_MSG1","Password invalid");
        define("A_LANG_LOGIN_MSG2","Email invalid");
        define("A_LANG_LOGIN_MSG3","Login unauthorized");
        define("A_LANG_LOGIN_MSG4","Wait for the release of your access by the administrator of the AdaptWeb");
        define("A_LANG_LOGIN_MSG5","Unable to connect to the database");


        // Formul�rio de cadastro de Curso
        define("A_LANG_COURSE_REGISTER","Register for Course");
        define("A_LANG_COURSE_RESGISTERED","Registered courses");
        define("A_LANG_COURSE2","Courso");
        define("A_LANG_COURSE_INSERT","ADD");
        define("A_LANG_COURSE_UPDATE","Change");
        define("A_LANG_COURSE_DELETE","Delete");
        define("A_LANG_COURSE_MSG1","Unable to connect to the database");
	 define("A_LANG_COURSE_MSG2","Unable to add the course");
	 define("A_LANG_COURSE_MSG3","Unable to change the course");
	 define("A_LANG_COURSE_MSG4","Unable to delete the course. To remove it you must enter Discipline / Course and remove the selection of this course in all subjects.");
	 define("A_LANG_COURSE_MSG5","Unable to delete the course");
	 // Incllus�o de mensagens para usabilidade - Carla -estagio UDESC-2008/02
	 define("A_LANG_COURSE_MSG6", "Course registered with Success");
	 define("A_LANG_COURSE_MSG7","Course deleted with Success");
	// Fim alteracoes Carla


        // Cadastro de disciplina
        define("A_LANG_DISCIPLINES_REGISTER","Register of Discipline");
        define("A_LANG_DISCIPLINES_REGISTERED","Disciplines registered");
        define("A_LANG_DISCIPLINES2","Discipline");
        define("A_LANG_DISCIPLINES_INSERT","Add");
        define("A_LANG_DISCIPLINES_UPDATE","Change");
        define("A_LANG_DISCIPLINES_DELETE","Delete");
        define("A_LANG_DISCIPLINES_MSG1","Unable to add the discipline");
        define("A_LANG_DISCIPLINES_MSG2","Unable to Change the course");
        define("A_LANG_DISCIPLINES_MSG3","It cannot delete this discipline. To remove it you must enter Discipline / Course and unmark the selection of all courses related.");
        define("A_LANG_DISCIPLINES_MSG4","Unable to delete the discipline");
        // Inclus�o de mensagens para usabilidade - Carla est�gio UDESC 2008/02
	 define("A_LANG_DISCIPLINES_MSG5","There's a discipline with this name");
	 define("A_LANG_DISCIPLINES_MSG6","A discipline cannot be completed blank");
	 define("A_LANG_DISCIPLINES_MSG7","To change a course you must select it and enter a new name");
	 define("A_LANG_DISCIPLINES_MSG8","the discipline was registered with Success");
	 // Final Carla


        // Formul�rio de cadastro de Disciplina / Curso
        define("A_LANG_DISCIPLINES_COURSE1","Discipline/Course");
        define("A_LANG_DISCIPLINES_COURSE_DISCIPLINES","Disciplines");
        define("A_LANG_DISCIPLINES_COURSE2","Courses");
        define("A_LANG_DISCIPLINES_COURSE_SAVE","Save");
        define("A_LANG_DISCIPLINES_COURSE_MSG1","Add DISCIPLINE and COURSE to access this item.");
        define("A_LANG_DISCIPLINES_COURSE_MSG2","Unable to link between Discipline and courses due to failure to connect to the database.");
        define("A_LANG_DISCIPLINES_COURSE_MSG3","Enter the name of the course to add");
        define("A_LANG_DISCIPLINES_COURSE_MSG4","Course already existing");
        define("A_LANG_DISCIPLINES_COURSE_MSG5","Select a course for a change");
        // Inclus�o de mensagens para usabilidade - Carla est�gio UDESC 2008/02
	 define("A_LANG_DISCIPLINES_COURSE_MSG6","The connection between subjects and courses has been successful.");
	 define("A_LANG_DISCIPLINES_COURSE_MSG7","There was no course selected for this Discipline");
	 define("A_LANG_DISCIPLINES_COURSE_MSG8","Course changed successfully");
	 define("A_LANG_DISCIPLINES_COURSE_OBS","*Note: To create the structure of the content of the discipline we must relate it to at least one course");
	 //Final Carla


	// Formul�rio de autoriza��o de acesso
	 define("A_LANG_LIBERATE_USER1","Number of Enrollment / Student");   // para o professor e administrador
        define("A_LANG_LIBERATE_USER2","Allow teachers / Author"); // para o adiministrador
        define("A_LANG_LIBERATE_MSG1","It's in development ... ");


        // Formul�rio para libera��o da disciplina
	 define("A_LANG_LIBERATE_DISCIPLINES","Release Discipline");
        define("A_LANG_LIBERATE_DISCIPLINES_MSG1","It's in development ... ");
	 // Inclus�o de mensagens para usabilidade - Carla est�gio UDESC 2008/02
	 define("A_LANG_TEXT_LIBERATE","To liberate the discipline is established must select it in the list \ 'Releasing Discipline \' and move to the list of \ 'Disciplines released \'.");
	 define("A_LANG_TEXT_LIBERATE_MAT","To liberate the registration is requested by the student must select it in the \ 'List of Students \ 'and move to the \' List of students registered \ .'");//carla incluiu para liberar matricula de aluno
	 // Final Carla

        // ======================= Topicos ==========================

        // Entrada / t�picos
        define("A_LANG_ENTRANCE_TOPICS_DISCIPLINES","Discipline");
        // **** REVER "Estruturar t�pico"
        define("A_LANG_ENTRANCE_TOPICS_STRUTURALIZE","Organizing Topics");
        define("A_LANG_ENTRANCE_TOPICS_MSG1","Couldnt read the disciplines");
        define("A_LANG_ENTRANCE_TOPICS_MSG2","Note: Only available in this form the Disciplines that are related to at least one course.");


       // Guia - Lista de t�picos
        define("A_LANG_TOPIC_GENERATE_CONTENT","Generate Content");
        //orelha
        define("A_LANG_TOPIC_LIST","Concepts");
        define("A_LANG_TOPIC_MAINTENANCE","Maintenance Concept");
        define("A_LANG_TOPIC_EXEMPLES","Examples");
        define("A_LANG_TOPIC_EXERCISES","Exercises");
        define("A_LANG_TOPIC_COMPLEMENTARY","Supplementary Material");
	  //Inclusao de mensagens de Usabilidade - Carla -estagio UDESC 2008/02
	   define("A_LANG_TOPIC_OBS","<b>*Note: </b> To <u> Creating Content </u> on the homepage of concepts you must properly fill out all fields on this page. <br> 
	   The file is being loaded must be <b>. Html </b>, if any image in this file, you need to upload the image. The <br> html file can be replaced by uploading another file. html. ");
			//Carla inseriu observa��o em pagina q cria topicos
	   define("A_LANG_TOPIC_ARQ_OBS","<b>* Note: </b> Extension of the html file (eg capitulo1.html)");//Carla inseriu observa��o para tipo de arquivo
	   define("A_LANG_TOPIC_GRAVAR_MSG1","Data saved successfully. If any change is made in this page you must write again.");
	 // Final Carla



        // Guia - manuten��o do t�pico
  	 define("A_LANG_TOPIC_NUMBER","Number of Concept");
	 define("A_LANG_TOPIC_DESCRIPTION","Name of Concept");
	 define("A_LANG_TOPIC_SUMARIZED_TOPIC_DESCRIPTION","Brief description of the concept");
	 define("A_LANG_TOPIC_WORDS_KEY","Keywords");
  	 define("A_LANG_TOPIC_PREREQUISIT","Prerequisite");
	 define("A_LANG_TOPIC_COURSE","Course");
	 define("A_LANG_TOPIC_FILE1","File");
	 define("A_LANG_TOPIC_FILE2","Files");
	 define("A_LANG_TOPIC_MAIN_FILE","Main File");
	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE","Associated Files");
	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_STATUS","Situation");
	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_UPLOAD","");
        define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG1","upload to the server");
  	 define("A_LANG_TOPIC_MAIN_ASSOCIATED_FILE_MSG2","upload to the server");

       // bot�es
        define("A_LANG_TOPIC_SAVE","Save");
        define("A_LANG_TOPIC_SEND","Send");
        define("A_LANG_TOPIC_SEARCH","search");
	 define("A_LANG_TOPIC_INSERT_TOPIC_SAME_LEVEL","Insert concept in the same level");
        define("A_LANG_TOPIC_INSERT_SUB_TOPIC","Insert sub-concept");
	 define("A_LANG_TOPIC_DELETE","Delete");
	 define("A_LANG_TOPIC_PROMOTE","Promote");
	 define("A_LANG_TOPIC_LOWER","Demote");
	 // Inclusao de mesanges de Usabilidade - Carla - estagio UDESC 2008/02
	 define("A_LANG_TOPIC_INSERT_MSG1","Complete all fields");//carla inseriu para colocar mensagem para usuario ao criar um topico
	 define("A_LANG_TOPIC_INSERT_MSG2","Complete the name field");//carla inseriu para colocar mensagem para usuario ao criar um topico
	 define("A_LANG_TOPIC_INSERT_MSG3","Complete the description field");//carla inseriu para colocar mensagem para usuario ao criar um topico
	 define("A_LANG_TOPIC_INSERT_MSG4","Complete the keyword field");//carla inseriu para colocar mensagem para usuario ao criar um t�pico
	 define("A_LANG_TOPIC_OBS2","<b>*Note:</b> All fields must be completed.");
	// Final Carla


	// Guia - samples
	//define("A_LANG_TOPIC_EXEMPLES_NUMBER_TOPIC","Number of topic");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Description of the example");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Level of Complexity");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Course");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Delete");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Description");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","level");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Course");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","File");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Associated files");
	// bot�es
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Save");
	//define("A_LANG_TOPIC_EXEMPLES_DESCRIPTION","Delete");


  // Exemplos
  // N�vel de Complexidade
// Descri��o
// N�vel
// Arquivos Associados
        // Descri��o dos Exemplos
  // Exerc�cios
        // Descri��o do exerc�cio
  // Material complementar
        // Menu
// Autorizar Acesso
// Liberar disciplinas
// Novo usu�rio
        // to modify I register in cadastre (alterar cadastro)
        // ************************************************************************ // * Formul�rio - Autoria
  // ************************************************************************ // Disciplinas Liberadas

// Matricula
	// Inclus�o de mesagens de usabilidade (para matricula) Carla - estagio UDESC 2008/02
	   define("A_LANG_LENVIRONMENT_REQUEST_SUCCESS","Request successfully performed. Wait for the release of its registration by the teacher");
	// Final Carla

// Aguardando Matricula
       // Inclus�o de mesagens de usabilidade (para aguardando matricula) Carla - estagio UDESC 2008/02
	   define("A_LANG_NO_DISC_RELEASED","There is no liberate discipline.");
	// Final Carla


        // Autorizar Acesso

        // Navega��o

        // Tela de Cursos
        // ************************************************************************ // * Formul�rio - Demo
  // ************************************************************************
  define("A_LANG_DEMO","Discipline is demo of AdaptWeb - does not allow insertions and changes");
//
// Regional Specific Date texts
//
// A little help for date manipulation, from PHP manual on function strftime():
//
// %a - abbreviated weekday name according to the current locale
// %A - full weekday name according to the current locale
// %b - abbreviated month name according to the current locale
// %B - full month name according to the current locale
// %c - preferred date and time representation for the current locale
// %C - century number (the year divided by 100 and truncated to an integer, range 00 to 99)
// %d - day of the month as a decimal number (range 01 to 31)
// %D - same as %m/%d/%y
// %e - day of the month as a decimal number, a single digit is preceded by a space
//      (range ' 1' to '31')
// %h - same as %b
// %H - hour as a decimal number using a 24-hour clock (range 00 to 23)
// %I - hour as a decimal number using a 12-hour clock (range 01 to 12)
// %j - day of the year as a decimal number (range 001 to 366)
// %m - month as a decimal number (range 01 to 12)
// %M - minute as a decimal number
// %n - newline character
// %p - either `am' or `pm' according to the given time value, or the corresponding strings for
//      the current locale
// %r - time in a.m. and p.m. notation
// %R - time in 24 hour notation
// %S - second as a decimal number
// %t - tab character
// %T - current time, equal to %H:%M:%S
// %u - weekday as a decimal number [1,7], with 1 representing Monday
// %U - week number of the current year as a decimal number, starting with the first Sunday as
//      the first day of the first week
// %V - The ISO 8601:1988 week number of the current year as a decimal number, range 01 to 53,
//      where week 1 is the first week that has at least 4 days in the current year, and with
//      Monday as the first day of the week.
// %W - week number of the current year as a decimal number, starting with the first Monday as
//      the first day of the first week
// %w - day of the week as a decimal, Sunday being 0
// %x - preferred date representation for the current locale without the time
// %X - preferred time representation for the current locale without the date
// %y - year as a decimal number without a century (range 00 to 99)
// %Y - year as a decimal number including the century
// %Z - time zone or name or abbreviation
// %% - a literal `%' character
//
// Note: A_LANG_DATESTRING is used for Articles and Comments Date
//       A_LANG_LINKSDATESTRING is used for Web Links creation Date
//       A_LANG_DATESTRING2 is used for Older Articles box on Home
//

define("A_LANG_CODE","pt-BR");
define("A_LANG_CHACTERSET","ISO-8859-1");

define("A_LANG_NAME_pt_BR","Brazilian Portuguese");
define("A_LANG_NAME_en_US","English");
define("A_LANG_NAME_es_ES","Spanish");
define("A_LANG_NAME_fr_FR","French");
define("A_LANG_NAME_ja_JP","Japanese");

define('A_LANG_TIMEZONES','IDLW NT HST YST PST MST CST EST AST GMT-3:30 GMT-3 AT WAT GMT CET EET BT GMT+3:30 GMT+4 GMT+4:30 GMT+5 GMT+5:30 GMT+6 WAST CCT JST ACS GST GMT+11 NZST');
define('A_LANG_TZOFFSETS','0 1 2 3 4 5 6 7 8 8.5 9 10 11 12 13 14 15 15.5 16 16.5 17 17.5 18 19 20 21 21.5 22 23 24');
define('A_LANG_DATEBRIEF','%b %d, %Y');
define('A_LANG_DATELONG','%A, %B %d, %Y');
define('A_LANG_DATESTRING','%A, %B %d @ %H:%M:%S %Z');

define('A_LANG_DATESTRING2','%A, %B %d');
define('A_LANG_DATETIMEBRIEF','%b %d, %Y - %I:%M %p');
define('A_LANG_DATETIMELONG','%A, %B %d, %Y - %I:%M %p %Z');

define('A_LANG_LINKSDATESTRING','%d-%b-%Y');
define('A_LANG_TIMEBRIEF','%I:%M %p');
define('A_LANG_TIMELONG','%I:%M %p %Z');

define('A_LANG_DAY_OF_WEEK_LONG','Monday Tuesday Wednesday Thursday Friday Saturday');
define('A_LANG_DAY_OF_WEEK_SHORT','Sun Mon Tue Wed Thu Fri Sat');
define('A_LANG_MONTH_LONG','January February March April May June July August September October November December');
define('A_LANG_MONTH_SHORT','Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec');

//************UFRGS******************
	//Index
	  //About
	// alterado por Carladefine('A_LANG_INDEX_ABOUT_INTRO',' The AdaptWeb is directed to the authors and adaptive presentation of disciplines members of ODL courses in Web AdaptWeb The goal is to allow the adequacy of tactics and ways of presenting content to students in various undergraduate courses and with different learning styles, allowing different forms of presentation of each content in a manner appropriate to each course and the individual preferences of the students participating. <br> Environment AdaptWeb was initially developed by researchers at UFRGS and the UEL, through projects and AdaptWeb Electra, with the support of CNPq. Visit the project page: <a href=\'http://www.inf.ufrgs.br/adapt/adaptweb\' target=\'_blank\'> http://www.inf.ufrgs.br/adapt / adaptweb </a>');
	// Alteracao mensagem Carla estagio UDESC 2008/02
	define('A_LANG_INDEX_ABOUT_INTRO',' The Environment AdaptWeb is directed to the authors and adaptive presentation of disciplines members of ODL courses in Web AdaptWeb The goal is to allow the adequacy of tactics and ways of presenting content to students in various undergraduate courses and with different learning styles, allowing different forms of presentation of each content in a manner appropriate to each course and the individual preferences of the students participating. <br> Environment AdaptWeb was initially developed by researchers at UFRGS and the UEL, through projects and AdaptWeb Electra, with the support of CNPq. Visit the project page: <a href=\'http://www.inf.ufrgs.br/adapt/adaptweb\' target=\'_blank\'> http://www.inf.ufrgs.br/adapt / adaptweb </a>');
	define('A_LANG_INDEX_ABOUT_ENVACCESS','Access to Adaptweb');
	define('A_LANG_INDEX_ABOUT_TINFO','To provide the content of your disciplines, the teacher must make a request to register for access to the environment and allow release of the Administrator Author');
	define('A_LANG_INDEX_ABOUT_LINFO','To access the courses the student must make up the Environment in the Navigation and require registration in the discipline related to their course');
	  //Demo
	define('A_LANG_INDEX_DEMO_AENVIRONMENT','Authoring of Environment');
	define('A_LANG_INDEX_DEMO_FDESC1', 'Create disciplines');
	define('A_LANG_INDEX_DEMO_NENVIRNMENT','Environment of navigation');
	define('A_LANG_INDEX_DEMO_FDESC2', 'Watch discipline');
	define('A_LANG_INDEX_DEMO_TOLOG','Access and navigate  by demonstrating discipline of logging on the environment with the data below:');
	define('A_LANG_INDEX_DEMO_LOGIN','login: demo@inf.ufrgs.br');
	define('A_LANG_INDEX_DEMO_PASS', 'senha: 123');
	define('A_LANG_INDEX_DEMO_OBSERVATION','Notes:');
	define('A_LANG_INDEX_DEMO_OBSERVATIONTEXT', 'after you perform the login access it will be released to the environment of the teacher and the students environment by the options "Environment of the teacher and the environment the student" on the initial screen of AdaptWeb.');
	  //Apresenta��o
	define('A_LANG_INDEX_PRESENTATION_INFABOUT', 'information about the project');
	define('A_LANG_DOWNLOAD', 'Download');
	define('A_LANG_PUBLICATIONS', 'Publications');
	define('A_LANG_INDEX_PRESENTATION_AVAILABLEAT','Available at:');



	//Ambiente do Aluno - Entrada
	  //Principal
	define('A_LANG_LENVIRONMENT',"Student's Environment");
	define('A_LANG_LENVIRONMENT_DESCRIPTION', 'This environment allows the student access to instructional material of your teacher (s) adapted to your profile.');
	define('A_LANG_LENVIRONMENT_WARNING', 'To have access to your discipline, the student must first make your registration in order to log into the environment to request registration of your discipline. Only after the release of access by the responsible teacher who will have access to the same discipline.');

	define('A_LANG_LENVIRONMENT_WATCH_LIBERATED', 'Courses released');
	define('A_LANG_LENVIRONMENT_WATCH_MINE', 'Disciplines own (browse by course)');
	define('A_LANG_LENVIRONMENT_WATCH_MY', 'My disciplines');
	define('A_LANG_LENVIRONMENT_WATCH_NOT', 'There is no disciplines registered ');
	define('A_LANG_LENVIRONMENT_WATCH_WARNING', 'Request registration for access to disciplines.');
	define('A_LANG_LENVIRONMENT_WATCH_VISUAL', 'Viw discipline for each course');
	define('A_LANG_LENVIRONMENT_WATCH_CREATE', 'Create and manage content disciplines (Generate Content) Professor of the Environment (item <b> Structuring Content => List of Concepts </b>) to access the navigation environment.');
	define('A_LANG_LENVIRONMENT_WATCH_OBS', 'To view disciplines own is not necessary to test the Discipline Release environment of navigation.');
	define('A_LANG_LENVIRONMENT_REQUEST', ' Request Registration');
	define('A_LANG_LENVIRONMENT_REQUEST_REQ', 'Request');
	define('A_LANG_LENVIRONMENT_REQUEST_NOTC', 'There are no course registered');
	define('A_LANG_LENVIRONMENT_REQUEST_NOTD', 'There no discipline registered for the course selected');
	define('A_LANG_LENVIRONMENT_REQUEST_SELECT', 'Select to request for registration:');
	define('A_LANG_LENVIRONMENT_REQUEST_MATR', 'Registered');
	define('A_LANG_LENVIRONMENT_WAITING', 'Waiting for release of teacher');
	define('A_LANG_LENVIRONMENT_WAITING_NOT', 'There no requests for registration');
	define('A_LANG_LENVIRONMENT_WAITING_WARNING', 'Request some registration for access disciplines:');
	define('A_LANG_LENVIRONMENT_NAVTYPE', 'Kind of Navigation');
	define('A_LANG_LENVIRONMENT_FORCOURSE', 'for the course of');
	define('A_LANG_LENVIRONMENT_NETCONECTION', 'Network Connection');
	define('A_LANG_LENVIRONMENT_SPEED', 'The speed is');
	define('A_LANG_LENVIRONMENT_NAVIGATION', 'Navegation');
	define('A_LANG_LENVIRONMENT_TUTORIAL', 'Tutorial');
	define('A_LANG_LENVIRONMENT_FREE', 'Free');
	// Inclus�o de mesagens de Usabilidade - Carla estagio UDESC 2008/02
	define('A_LANG_LENVIRONMENT_TUTORIAL_DESC', 'Navigation aided by the pre-requisites established by the teacher.'); // carla
       define('A_LANG_LENVIRONMENT_FREE_DESC', 'Navigation is open to all concepts (without taking into account the suggestions of the teacher).');//carla
       define('A_LANG_LENVIRONMENT_PLUS', 'Learn more about the types of navigation');//CARLA
	define('A_LANG_LENVIRONMENT_NAVIGATION_EXPLAIN', 'Actually Adaptweb offers two modes of navigation for a discipline (Guide tour and Free tour). The difference between them is like browsing by discipline. But the same contents are prepared for the two modes of navigation.'); //Carla- explicacao no modo de navegacao - a_navega e n_navega
	// Final Carla

	//Ambiente do professor
	define('A_LANG_TENVIRONMENT_DESCRIPTION', 'The module allows the author to author content tailored to provide different profiles of students. Through the process of authorship, the author can provide the content of their classes in a single structure adapted to the different courses. At this stage the author must register for the discipline and courses that want to make it available. After the registration insert the concept of the contents of files, relating them to each topic of discipline. Furthermore the author must inform the description of the topic, and its pre-requisite courses which want to provide content. The author can insert exercises, examples and additional materials for each topic.');
	define('A_LANG_TENVIRONMENT_AUTHOR', 'Authoring');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR1', 'There was an error when you enroll in this discipline. This user must already be registered or an error occurred on the server');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_ERR2', 'An error occurred while deleting the user is registration in this course. This user should not be registered or an error occurred on the server');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_NOT', 'There are no course registered');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECT', 'Select a course');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_NOTD', 'No discipline registered for this course');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_SELECTD', 'Select a discipline');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_LIST', 'List of students:');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_MATR', 'Student registered:');
	define('A_LANG_TENVIRONMENT_AUTHORIZE_BACK', 'back');
	define('A_LANG_TENVIRONMENT_LIBERATE_DESCRIPTION', '<b> Note: </b> Only available for the subjects that are released with correct content. To check the content of subjects to access the <b> CONCEPT </b> on item <b> STRUCTURE CONTENT </b> button and use the <b> generating content </b>.');
	define('A_LANG_TENVIRONMENT_LIBERATE_LIB', 'Release <br> discipline:');
	define('A_LANG_TENVIRONMENT_LIBERATE_LIB_DIS', 'Disciplines <br> released:');
	define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE', 'move concept');
	define('A_LANG_TENVIRONMENT_ESTRUCT_EXCLUDE', 'Delete concept');
	define('A_LANG_TENVIRONMENT_ESTRUCT_SELECT', 'Select the concept you want <b> MOVE </b> or <b> DELETE </b>');
	define('A_LANG_TENVIRONMENT_ESTRUCT_MOVE_AFTER', '<b> MOVE </b> concept selected after which concept?');
	define('A_LANG_TENVIRONMENT_ESTRUCT_CANCEL', 'to move or delete operation concept will result in cancellation of the prerequisites');
	define('A_LANG_TENVIRONMENT_ESTRUCT_NOT_POSSIBLE', 'nable to read the matrix of the Database');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND', 'Legend of pre-requisites:');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND1', 'To select more than one prerequisites use the CTRL key');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND2', 'to remove more than prerequistes use the CTRL key');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND3', 'The prerequisites in <font class=buttonselecionado> dark blue </font> are selected');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND4', 'The background <font class=buttonpre> blue </font> indicates the current pre-requisites (if losing the selection)');
	define('A_LANG_TENVIRONMENT_TOPICS_LEGEND5', 'The background <font class=buttonpreautomatico> yellow </font> indicates the prerequisites current automatic');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NUMBER', 'Number of concept:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NAME', 'Name of exemple');
	define('A_LANG_TENVIRONMENT_EXAMPLES_DESCRIPTION', 'Description of the example:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_KEY', 'Keywords of the example');
	define('A_LANG_TENVIRONMENT_EXAMPLES_LEVEL', 'Level of Complexity:');
	define('A_LANG_TENVIRONMENT_EXAMPLES_NOCLASS', 'Unranked');
	define('A_LANG_TENVIRONMENT_EXAMPLES_EASY', 'Easy');
	define('A_LANG_TENVIRONMENT_EXAMPLES_MEDIUM', 'Average');
	define('A_LANG_TENVIRONMENT_EXAMPLES_COMPLEX', 'Complex');
	define('A_LANG_TENVIRONMENT_EXAMPLES_DSC', 'Description');
	define('A_LANG_TENVIRONMENT_EXAMPLES_LVL', 'Level');
	define('A_LANG_TENVIRONMENT_EXERCISES_NAME', 'Name of exercises:');
	define('A_LANG_TENVIRONMENT_EXERCISES_DESCRIPTION', 'Description of exercise:');
	define('A_LANG_TENVIRONMENT_EXERCISES_KEY', 'keyword of exercise:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_NAME', 'Name of Complementary  <br> material:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_DESCRIPTION', 'Description of <br> Supplementary Material:');
	define('A_LANG_TENVIRONMENT_COMPLEMENTARY_KEY', 'Keywords <br> of Supplementary Material:');
	define('A_LANG_TENVIRONMENT_ACCESS', 'Access to the Environment of Author');
	define('A_LANG_TENVIRONMENT_WAIT', 'Wait for the administrator of the environment free access to the environment of authorship.');
	define('A_LANG_TENVIRONMENT_COMMENT', 'Access to the environment of authorship is managed by the administrator of the environment AdaptWeb. Once you register on the environment is necessary to permit the administrator to access the environment of authorship.');
	define('A_LANG_TENVIRONMENT_DEMO', 'Demonstration of Environments');
	define('A_LANG_TENVIRONMENT_ASSOCIATED_FILES','Note: Files associated should be sent to the server through the Maintenance Of');
	// Inclus�o de mensagens usabilidade - Carla estagio UDESC 2008/02
       define("A_LANG_TENVIRONMENT_EXAMPLES_OBS","<b>* Note: </b> To provide the discipline you must fill in all fields of this page correctly .");// Carla inserted to make a comment tab of examples. Maintenance of
       define("A_LANG_TENVIRONMENT_EXERC","To create one or more exercises is needed to fill all fields and upload the files in <b>. Html </b> (eg exerc1.html, exerc2.html). If this file has some image, you need to upload the same.");//Carla inseriu para colocar uma introdu��o na aba de material complementar");//carla
       define("A_LANG_TENVIRONMENT_EXAMPLES","To create one or more examples we must fill in all fields and upload the files in <b>. Html </b> (eg exemplo1.html, exemplo2.html). If this file has some image, you need to upload the same.");//Carla inseriu para colocar uma introdu��o na aba de exemplos.
       define("A_LANG_TENVIRONMENT_COMPLEMENTARY_DESC","To create one or more additional material is needed to fill all fields and upload the files in <b>. Html </b> (eg mat1.html, mat2.html). If this file has some image, you need to upload the same.");//Carla inseriu para colocar uma introdu��o na aba de material complementar.
	// Final Carla


	//Op��es de Root
	define('A_LANG_ROOT_NOT_AUTH', 'Teacher is <br> not allowed:');
	define('A_LANG_ROOT_AUTH', 'Teacher is <br> allowed:');
	define('A_LANG_ROOT_DEV', 'this option is in development.');

	//Outros
	define('A_LANG_DATA', 'Data');
	define('A_LANG_WARNING', 'Warning');
	define('A_LANG_COORDINATOR', 'COORDINATOR');
	define('A_LANG_DATABASE_PROBLEM', 'There are some problems with the Database');
	define('A_LANG_FAQ', 'FAQ');
	define('A_LANG_WORKS', 'Jobs');
	define('A_LANG_NOT_DISC', 'Unable to show the disciplines registered');
	define('A_LANG_NO_ROOT', 'Unable to insert the Root User');
	define('A_LANG_MIN_VERSION', 'The minimum supported version of the database is [');
	define('A_LANG_INSTALLED_VERSION', '] And a version is installed [');
	define('A_LANG_ERROR_BD', 'Unable to write the matrix in the Database');
	define('A_LANG_DISC_NOT_FOUND', 'Name of Course not found or blank');
	define('A_LANG_SELECT_COURSE', 'select the course on the concept <b>');
	define('A_LANG_TOPIC_REGISTER', 'Registration of Concepts');
	define('A_LANG_SUMARIZED_DESCRIPTION', 'Brief description of the concept');
	define('A_LANG_LOADED', 'uploaded to the server');
	define('A_LANG_FILL_DESCRIPTION', 'complete in the description field of the material');
	define('A_LANG_WORKS_SEND_DATE', 'File was sended on');
	define('A_LANG_SEARCH_RESULTS', 'Search Results');
	define('A_LANG_SEARCH_SYSTEM', 'Search System ');
	define('A_LANG_SEARCH_WORD', 'Search Keywords ');
	define('A_LANG_SEARCH_FOUND', 'Search Found ');
	define('A_LANG_SEARCH_OCCURRANCES', ' occurrences of ');
	define('A_LANG_SEARCH_IN_TOPIC', ' in concept');
	define('A_LANG_SEARCH_FOUND1', 'it was found ');
	define('A_LANG_SEARCH_OCCURRANCE', ' occurrences of ');
	define('A_LANG_SEARCH_NOT_FOUND', 'There were no instances of ');
	define('A_LANG_SEARCH_CLOSE', 'Close');
	define('A_LANG_TOPIC', 'Concept');
	define('A_LANG_SHOW_MENU', 'Show Menu');
	define('A_LANG_CONFIG', 'Configurations');
	define('A_LANG_MAP', 'Map');
	define('A_LANG_PRINT', 'Print this page?');
	define('A_LANG_CONNECTION', 'Unable to connect to the database server MySQL Please contact the administrator.');
	define('A_LANG_CONTACT', 'Please contact the administrator');
	define('A_LANG_HELP_SYSTEM', 'Help system');
	define('A_LANG_CONFIG_SYSTEM', 'Configuration of system');
	define('A_LANG_CONFIG_COLOR', 'Configuration color of background');
	define('A_LANG_NOT_CONNECTED', 'Not connected');
	define('A_LANG_DELETE_CADASTRE', 'Delete cadastre of students');
	define('A_LANG_HOME', 'Home');
	define('A_LANG_YOU_HAVE', 'you have got');
	define('A_LANG_TO_CONFIRM', ' registration. To confirm this exclusion click ');
	define('A_LANG_CONFIRM', ' Confirm');
	define('A_LANG_EXAMPLE_LIST', 'Example list');
	define('A_LANG_COMPLEXITY_LEVEL_EASY', 'Complexity level is Easy');
	define('A_LANG_COMPLEXITY_LEVEL_MEDIUM', 'Complexity level is Average');
	define('A_LANG_COMPLEXITY_LEVEL_COMPLEX', 'Complexity level is difficult');
	define('A_LANG_EXERCISES_LIST', 'Exercises list');
	define('A_LANG_REGISTER_MAINTENENCE', 'Maintenance of Registrations');
	define('A_LANG_REGISTER_RELEASE', 'Release of Enrollment');
	define('A_LANG_SITE_MAP', 'Site map');
	define('A_LANG_COMPLEMENTARY_LIST','List of Supplementary Material');
	define('A_LANG_DELETE_TOPIC', 'Deleted concept ');
	define('A_LANG_AND', ' and ');
	define('A_LANG_SONS', 'subconcepts');
	define('A_LANG_CONFIRM_EXCLUSION', 'Do you sure you want to delete the discipline');
	define('A_LANG_YES', '  yes  ');
	define('A_LANG_NO', '  no  ');
	// Inclusao mensagens usabilidade - Carla estagio UDESC 2008/02
	 define('A_LANG_CONFIRM_EXCLUSION_CURSO', 'Do you sure you want to delete the discipline');
	// Final Carla


	//Gera��o de conte�do
	define('A_LANG_GENERATION_END', 'End of the xml file generation of elements of the topic');
	define('A_LANG_GENERATION_CREATED', 'It was created ');
	define('A_LANG_GENERATION_XML', ' xml. filmes');
	define('A_LANG_GENERATION_END_STRUCT', 'End of the generation of the XML Structure of Topics');
/*	define('A_LANG_GENERATION_MISSING_FILE', 'add remaining file from the concept ');
	define('A_LANG_GENERATION_TH_TOPIC', 'concept ');
	define('A_LANG_GENERATION_MISSING_COURSE', 'Remains of course include ');
	define('A_LANG_GENERATION_NO_ASOC', 'Meaning no way associated');
	define('A_LANG_GENERATION_MISSING_EXERC_ID', 'Missing insert the identifier of exercise');
	define('A_LANG_GENERATION_TH_EXERCISE', '� exercise of ');
	define('A_LANG_GENERATION_MISSING_EXERC_DESCRIPTION', 'Missing insert exercise descriptions ');
	define('A_LANG_GENERATION_MISSING_EXERC_FILE', 'Add missing file from the exercise ');
	define('A_LANG_GENERATION_MISSING_EXERC_COMPLEXITY', 'Missing insert complexity of the exercise ');
	define('A_LANG_GENERATION_TH_EXAMPLE', '� exemplo do ');
	define('A_LANG_GENERATION_MISSING_EXAMP_ID', 'Missing insert identifier example');
	define('A_LANG_GENERATION_MISSING_EXAMP_DESCRIPTION', 'Missing insert description of the example ');
	define('A_LANG_GENERATION_MISSING_EXAMP_FILE', 'Add missing file from the example ');
	define('A_LANG_GENERATION_MISSING_EXAMP_COMPLEXITY' ,'Missing insert example of the complexity of ');
	define('A_LANG_GENERATION_TH_MATCOMP', '�supplementary material');
	define('A_LANG_GENERATION_MISSING_MATCO_ID', 'Missing insert the identifier for additional material ');
	define('A_LANG_GENERATION_MISSING_MATCO_DESCRIPTION', 'Missing insert description of the supplementary material');
	define('A_LANG_GENERATION_MISSING_MATCO_FILE', 'Add missing file from the supplementary material ');
*/	define('A_LANG_GENERATION_VERIFY', 'Check the data of authors');
	define('A_LANG_GENERATION_PROBLEMS', 'Problems with registration of the Discipline');
/*	define('A_LANG_GENERATION_MISSING_TOPIC_NUMBER', 'Missing insert number of topics for the ');
	define('A_LANG_GENERATION_MISSING_TOPIC_DESCRIPTION', 'Missing insert description of the topic ');
	define('A_LANG_GENERATION_MISSING_TOPIC_ABREV', 'Missing insert Abbreviation for the topic ');
	define('A_LANG_GENERATION_MISSING_TOPIC_KEYWORD', 'Missint insert Keyword for the topic');
*/	define('A_LANG_GENERATION_RESULT_XML', 'Result of generation of XML');
	define('A_LANG_GENERATION_NO_ERROR', 'NO ERROR');
	define('A_LANG_GENERATION_SUCCESS', 'COURSE FILES GENERATED WITH SUCCESS');
	define('A_LANG_GENERATION_DATA_LACK', 'XML files were not generated by lack of data');
	define ('A_LANG_GENERATION_TOPIC','Concept');
	define ('A_LANG_GENERATION_EXAMPLE','Example');
	define ('A_LANG_GENERATION_EXERCISE','Exercise');
	define ('A_LANG_GENERATION_COMPLEMENTARY','Supplementary Material');
	define ('A_LANG_GENERATION_TOPIC_NUMBER','number of topic');
	define ('A_LANG_GENERATION_TOPIC_NAME','name of topic');
	define ('A_LANG_GENERATION_DESCRIPTION','Description');
	define ('A_LANG_GENERATION_KEY_WORD','keyword');
	define ('A_LANG_GENERATION_COURSE','Course');
	define ('A_LANG_GENERATION_PRINC_FILE','Main file');
	define ('A_LANG_GENERATION_ASSOC_FILE','associated files');
	define ('A_LANG_GENERATION_MISSING','Missing');
	define ('A_LANG_GENERATION_NO_PROBLEM','Ok');
	define ('A_LANG_GENERATION_NUMBER','Number');
	define ('A_LANG_GENERATION_COMPLEXITY','Level');
	define ('A_LANG_GENERATION_NOT_EXIST','not exist');

	//Reposit�rio
	define ('A_LANG_MNU_REPOSITORY','Repository');
	define ('A_LANG_PRESENTATION','Presentation');
	define ('A_LANG_EXPORT','Export of Disciplines');
	define ('A_LANG_IMPORT','Import of Disciplines');
	define ('A_LANG_OBJECT_SEARCH','Search for Learning Objects');
	define ('A_LANG_EXP_TEXT', 'Submit your courses to the Repository so that other teachers can use it too.');
	define ('A_LANG_IMP_TEXT', 'Use disciplines available in your environment AdaptWeb');
	define ('A_LANG_SCH_TEXT', 'Datas of the subjects AdaptWeb were also indexed in the format of learning objects using the metadata standard LOM (Learning Object Metadata). Search for objects of learning covering subjects, topics (concepts) and the repository files.');
	define ('A_LANG_REP_TEXT', 'Welcome to the Repository of the Disciplines AdaptWeb. <br> Through this repository, users from different places AdaptWeb can share data to create their disciplines. Here you can import and export of disciplines, and seek objects of learning.');
	define ('A_LANG_REP_SELECT', 'Select a discipline');
	define ('A_LANG_REP_EXP_SUCCESS', 'Export was successful!');
	define ('A_LANG_REP_EXP_ERROR', 'Error exporting discipline!');
	define ('A_LANG_REP_EXP_TEXT', 'Through this interface you can make of their subjects AdaptWeb for others to use them. <br> Select the discipline from the list below, specify a description for it and click "Export."');
	define ('A_LANG_REP_EXPORT', 'Export');
	define ('A_LANG_REP_SELECT_COURSE', 'Select a course');
	define ('A_LANG_REP_IMP_SUCCESS', 'Import was successful!');
	define ('A_LANG_REP_IMP_ERROR', 'Error in importing discipline!');
	define ('A_LANG_REP_INVALID_PAR', 'Parameters are invalid!');
	define ('A_LANG_REP_IMP_TEXT', 'Through this interface you can copy disciplines available in the repository for your environment of authorship in AdaptWeb! <br> Select the discipline from the list below, select the course that it is linked, click "Import."');
	define ('A_LANG_REP_DISC_IN_REP', 'disciplines in the Repository');
	define ('A_LANG_REP_MY_COURSES', 'My courses');
	define ('A_LANG_REP_OTHER_NAME', 'Import with another name');
	define ('A_LANG_REP_IMPORT', 'Import');
	define ('A_LANG_REP_DISC_DATA', 'Data of the Discipline');
	define ('A_LANG_REP_SEARCH', 'Enter a string to search and select the metadata to be included in your search:');
	define ('A_LANG_REP_SCH_TITLE', 'Title');
	define ('A_LANG_REP_SCH_SELECT', 'Select');
	define ('A_LANG_REP_AGGREG', 'Aggregation Level');
	define ('A_LANG_REP_SCH_RAW', 'Datas (files)');
	define ('A_LANG_REP_SCH_TOPIC', 'Concept');
	define ('A_LANG_REP_SCH_DISC', 'Discipline');
	define ('A_LANG_REP_SCH_COURSE', 'Course');
	define ('A_LANG_REP_SCH_SEARCH', 'Search');
	define ('A_LANG_REP_SCH_ERROR', 'Error');
	define ('A_LANG_REP_SCH_NO_REG', 'No registry found!');
	define ('A_LANG_REP_SCH_REG', 'registry found');
	define ('A_LANG_REP_SCH_REGS', 'registries founds');
	define ('A_LANG_REP_DISC_NOT_FOUND', 'discipline not found');
	define ('A_LANG_COURSES', 'Courses');
	define ('A_LANG_EXP_BY', 'Export by');
	define ('A_LANG_ERROR_SELECT_BD', 'Error in the selecting the database');


/********************************************************************************************
 * Avalia��o (Definido por Claudiomar Desanti)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus&atilde;o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 */
define("A_LANG_MNU_AVS", "Evaluation Functions");
define("A_LANG_MNU_AVS_NAV", "Evaluation"); // menu na parte de navega&ccedil;&atilde;o
define("A_LANG_TOPIC_EVALUATION","Evaluation"); // orelha de avaliacao

define("A_LANG_AVS_STUDENTS_TOPIC_EVALUATION", "Evaluation released"); // t&oacute;pico da avalia&ccedil;&atilde;o
define("A_LANG_AVS_STUDENTS_TOPIC_RESULTS", "Table of results"); // t&oacute;pico de quadro de resultados

define("A_LANG_AVS_FUNCTION_INTRO","Introduction");
define("A_LANG_AVS_FUNCTION_AVERAGE","Average");
define("A_LANG_AVS_FUNCTION_ADJUSTMENT","Adjust Note");
define("A_LANG_AVS_FUNCTION_LIBERATE","Free Evaluation");
define("A_LANG_AVS_FUNCTION_DIVULGE","Disclosure </br> note");
define("A_LANG_AVS_FUNCTION_REPORT","Report");
define("A_LANG_AVS_FUNCTION_BACKUP","Backup");


define("A_LANG_AVS_TITLE_ADJUSTMENT_AVERAGE","Adjustment Average");
define("A_LANG_AVS_TITLE_ADJUSTMENT_QUESTION_CANCEL","Cancel Question");
define("A_LANG_AVS_TITLE_ADJUSTMENT","Adjust of Notes");
define("A_LANG_AVS_TITLE_ADJUSTMENT_QUESTION_WEIGHT","Distribute weight to evaluations");
define("A_LANG_AVS_TITLE_DIVULGE","Disclouse of notes of the evaluation");
define("A_LANG_AVS_TITLE_NEW_AVAL", "New evaluation");
define("A_LANG_AVS_TITLE_NEW_AVAL_PRES", "New Presential evaluation");
define("A_LANG_AVS_TITLE_GROUP","New group");
define("A_LANG_AVS_TITLE_LIBERATE","Free Evaluation");
define("A_LANG_AVS_TITLE_AVERAGE","distribution of averages");
define("A_LANG_AVS_TITLE_REPORT", "Report");
define("A_LANG_AVS_TITLE_BACKUP", "Backup");
define("A_LANG_AVS_TITLE_DISSERT","correction of expatiate questions");
define("A_LANG_AVS_TITLE_ADJUSTMENT_PRESENTIAL","adjustment notes of presential questions");
define("A_LANG_AVS_TITLE_ADD_QUESTION","Add existing questions");

// -------------- ERROS ------------------------------------------------- //
define("A_LANG_AVS_ERROR_DB_OBJECT","Boot failed, the object connection to acess database is invalid");
define("A_LANG_AVS_ERROR_DB_SAVE","Error in saving the informations");
define("A_LANG_AVS_ERROR_PARAM","<div class='erro'><p>Error in passing of parameters, reload the page</p></div>");

define("A_LANG_AVS_ERROR_VERIFY_AUTHO","<div class='erro'><p>you cannot perform this part of system=</p></div>");
define("A_LANG_AVS_ERROR_EVALUATION_DELETE","unable to delete the question of the evaluation");


define("A_LANG_AVS_ERROR_EVALUATION_AVERAGE","no evaluation has been completed or released");
define("A_LANG_AVS_ERROR_FUNCTION_EVALUATION_LIBERATE","No evaluation has been completed or released");
define("A_LANG_AVS_ERROR_QUESTION_OBJECT","unable loading the question");
define("A_LANG_AVS_ERROR_ANSWER_OBJECT","unable to load the answers of questions");
define("A_LANG_AVS_ERROR_LOGGED","you must log in the system first to acess this module");
define("A_LANG_AVS_ERROR_STUDENTS_LIBERATE","error in loading evaluation");
define("A_LANG_AVS_ERROR_STUDENTS_VIEW","That evaluation is not yours");
define("A_LANG_AVS_ERROR_EVALUATION_LIBERATE","It was not found the evaluation");
define("A_LANG_AVS_ERROR_EVALUATION_LIBERATE_STUDENTS","Student is not with the evaluation released");
define("A_LANG_AVS_ERROR_EVALUATION_LOGGED","You must be logged to acess the evaluation");
define("A_LANG_AVS_ERROR_EVALUATION_INIT","Error in acessing the evaluation");
define("A_LANG_AVS_ERROR_EVALUATION_SUBMIT","The evaluation has been closed by teacher");
define("A_LANG_AVS_ERROR_AUTHO","you do not have got permission to acess this area");
define("A_LANG_AVS_ERROR_TOPIC_EVALUATION","Nenhum conceito foi encontrado com avalia&ccedil;&otilde;es dispon&iacute;veis. Verifique se as quest&otilde;es das avalia&ccedil;&otilde;es est&atilde;o com os pesos definidos e se o peso da m&eacute;dia j&aacute; est&aacute; definida.");
define("A_LANG_AVS_ERROR_LIBERATE_NOT_STUDENTS", "no student is registered in this discipline to do the evaluations");
define("A_LANG_AVS_ERROR_DIVULGE_VALUE","unable disclouse the notes");
define("A_LANG_AVS_ERROR_DIVULGE_NOT_EVALUATION","no evaluation was completed to be disclosed");
define("A_LANG_AVS_ERROR_LIBERATE_EVALUATION","unable to complete the release of the evaluation");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT","no evaluation was found");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_SAVE","error in salving the question");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_AVERAGE","error in calculating the note");
define("A_LANG_AVS_ERROR_AVERAGE_CALCULE","erorr in calculate the average");
define("A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_STUDENT","student did not answer the question of this evaluation");
define("A_LANG_AVS_ERROR_ADJUSTMENT_PRESENTIAL","the notes was not disclosed, because some student have not any note");
define("A_LANG_AVS_ERROR_ADD_QUESTION_EXIST","Error in adding new questions in evaluation");
define("A_LANG_AVS_ERROR_QUESTION_NOT_FOUND","no results was founded");
define("A_LANG_AVS_ERROR_NOT_STUDENTS_LIBERATE","No students enrolled in that discipline");
define("A_LANG_AVS_ERROR_EVALUATION_FINISH","the evaluation already was delivered.");
define("A_LANG_AVS_ERROR_EVALUATION_FINISH2","the evaluation already was finished, you cannot render anymore");
define("A_LANG_AVS_ERROR_SESSION_BROWSER","the session that you started the evaluation is no longer the same<br/>this may have occurred by the exchange to another computer, or browser.");
define("A_LANG_AVS_ERROR_FUNCTION_CANCEL","N&atilde;o &eacute; cancelar as quest&otilde;es. &Eacute; necess&aacute;rio que haja ao menos uma quest&atilde;o n&atilde;o cancelada");


define("A_LANG_AVS_ERROR_QUESTION_LOGGED","you must be logged to acess this module");
define("A_LANG_AVS_ERROR_QUESTION_EVALUATION","the question cannot be added in this evaluation");
define("A_LANG_AVS_ERROR_QUESTION_LACUNA_DELETE","error in deleting the lacuna");
define("A_LANG_AVS_ERROR_QUESTION_ME_DELETE","unable delete the alternative");
// erros utilizados no ajax
define("A_LANG_AVS_ERROR_AJAX_AVERAGE","the evaluation was not defined for this course");
define("A_LANG_AVS_ERROR_AJAX_AVERAGE1","Student did not realize evaluations");
define("A_LANG_AVS_ERROR_AJAX_DIVULGE1","Os alunos assinalados com pendencia s�o alunos que ainda tem quest�es dissertativas a serem corrigidas");
define("A_LANG_AVS_ERROR_AJAX_DIVULGE2","Nobody realized this evaluation, Then this evaluation was cancelled. The student will do another evaluation");
define("A_LANG_AVS_ERROR_AJAX_LIBERATE","No evaluation is released to resolve");
define("A_LANG_AVS_ERROR_AJAX_LIBERATE2","No discipline was closed");
define("A_LANG_AVS_ERROR_AJAX_AVERAGE3","This discipline do not have no grupo of avaliation for the averages can be available,");
// ------------------- FIM DOS ERROS -----------------------------------------------------//

define("A_LANG_AVS_SUCESS", "Salve successfully!");
define("A_LANG_AVS_SUCESS_DIVULGE","The notes of evaluation was liberated to students");
define("A_LANG_AVS_SUCESS_LIBERATE","Evalutation was liberated successfully");
define("A_LANG_AVS_SUCESS_EVALUATION","The evaluation was delivered successfully");
define("A_LANG_AVS_STUDENTS","Students");

// -------------- BOTOES -----------------------------------------------------------------//
define("A_LANG_AVS_BUTTON_SAVE","Salve");
define("A_LANG_AVS_BUTTON_SAVE_COPY_DEFAULT","Copy default weight");
define("A_LANG_AVS_BUTTON_SAVE_COPY_MOD","Copy modified weight");
define("A_LANG_AVS_BUTTON_SUBMIT","Submit evaluation");
define("A_LANG_AVS_BUTTON_BACK",A_LANG_TENVIRONMENT_AUTHORIZE_BACK);
define("A_LANG_AVS_BUTTON_HELP",A_LANG_HELP);
define("A_LANG_AVS_BUTTON_NEW_GROUP","New group");
define("A_LANG_AVS_BUTTON_CHANGE_OPTION","Change parameter");
define("A_LANG_AVS_BUTTON_NEW_EVALUATION","New Evaluation");
define("A_LANG_AVS_BUTTON_WEIGHT","divide equally weights");
define("A_LANG_AVS_BUTTON_NEXT","Next");
define("A_LANG_AVS_BUTTON_LIBERATE_EVALUATION","Liberate new evaluation");
define("A_LANG_AVS_BUTTON_FINISH_EVALUATION","Finish evaluation");
define("A_LANG_AVS_BUTTON_LIST_STUDENTS","List students=");
define("A_LANG_AVS_BUTTON_CLOSE","Close");
define("A_LANG_AVS_BUTTON_SEARCH","search");
define("A_LANG_AVS_BUTTON_MARK","Mark all");
define("A_LANG_AVS_BUTTON_UNMARK","Unmarck all");

// bot&otilde;es do AVS_TOPICO.PHP
define("A_LANG_AVS_BUTTON_GROUP_NEW","New group");
define("A_LANG_AVS_BUTTON_GROUP_PARAM","Change parameter");
define("A_LANG_AVS_BUTTON_GROUP_NEW_EVALUATION","Add Evaluation");
define("A_LANG_AVS_BUTTON_DUPLICATE_AVAL","duplicate evaluation");
define("A_LANG_AVS_BUTTON_AVAL_PRESEN","Add presential evaluation");
// bot&otilde;es AVS_NOVA_AVAL.PHP
define("A_LANG_AVS_BUTTON_QUESTION_NEW","Add new question");
define("A_LANG_AVS_BUTTON_QUESTION_EXIST","add existing questions");
// bot&otilde;es utilizados na libera&ccedil;&atilde;o da avalia&ccedil;&atilde;o
define("A_LANG_AVS_BUTTON_EVALUATION_LIBERATE_NEW","Liberate new evaluation");
define("A_LANG_AVS_BUTTON_EVALUATION_LIBERATE_FINISH","Liberate evaluations finished");
define("A_LANG_AVS_BUTTON_BACK_LIBERATE","Turn Back to  Initial Page Liberate");
// bot&otilde;es utilizados nas telas de quest&otilde;es
define("A_LANG_AVS_BUTTON_QUESTION_ADD_LACUNA","Add new Lacuna");
define("A_LANG_AVS_BUTTON_QUESTION_DELETE_LACUNA","Delete lacuna");
define("A_LANG_AVS_BUTTON_QUESTION_ADD_ME","Add new alternative");

// bot&otilde;es utilizados nos ajax
define("A_LANG_AVS_BUTTON_AJAX_AVERAGE_LIST","list notes");
define("A_LANG_AVS_BUTTON_AJAX_DIVULGE","divulge notes");
define("A_LANG_AVS_BUTTON_AJAX_LIBERATE_END","end evaluation");
define("A_LANG_AVS_BUTTON_AJAX_LIST","list students");

// --------------- FIM DOS BOTOES ---------------------------------------------------- //
// --------------- LABELS - texto ao lado de combobox e spans ------------------------ //
define("A_LANG_AVS_LABEL_COURSE",A_LANG_COURSES);
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_VALUE","Note");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_EXPLANATION","Explication");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_WEIGHT","weight");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_CORRECT","right answer");
define("A_LANG_AVS_LABEL_STUDENT_QUESTION_OBS_TEACHER","comment of teacher");
define("A_LANG_AVS_LABEL_EVALUATION_MODEL","models of evaluation");
define("A_LANG_AVS_LABEL_EVALUATION_MODEL_AUTO","Models for Automated evaluation");
define("A_LANG_AVS_LABEL_EVALUATION_MODEL_PRESENTIAL","Models for presential evaluation");
define("A_LANG_AVS_LABEL_EVALUATION","Evaluation");
define("A_LANG_AVS_LABEL_TOPIC","conception");
define("A_LANG_AVS_LABEL_VIEW_EVALUATION","View of the evaluation by the teacher");
define("A_LANG_AVS_LABEL_YES","yes");
define("A_LANG_AVS_LABEL_NOT","No");
define("A_LANG_AVS_LABEL_INFO","Informations");
//

define("A_LANG_AVS_LABEL_QUESTION_DISSERT","question expatiate");
define("A_LANG_AVS_LABEL_QUESTION_ME","multiple Choice");
define("A_LANG_AVS_LABEL_QUESTION_LACUNAS","fill lacuna");
define("A_LANG_AVS_LABEL_QUESTION_VF","True or False");

define("A_LANG_AVS_LABEL_FUNCTION","Functions");
define("A_LANG_AVS_LABEL_QUESTION","Questions");

// labels do AVS_GRUPO.PHP
define("A_LANG_AVS_LABEL_QUESTION_RANDOM","Questions should be mixed?");
define("A_LANG_AVS_LABEL_EVALUATION_PRESENT","presential evaluation?");
define("A_LANG_AVS_LABEL_ANOTHER_GROUP","chosen by another group");
// labels do AVS_NOVA_AVAL.PHP
define("A_LANG_AVS_LABEL_DESCRIPTION","description");
define("A_LANG_AVS_LABEL_DIVULGE_NOTE","would you like to  disclose the note automatically?");
define("A_LANG_AVS_LABEL_DISSERT_QUESTION","There are some expatiate questions to rate");
// labels do AVS_TOPICO.PHP
define("A_LANG_AVS_LABEL_GROUP_DESCRIPTION","Group for evaluation concept");
define("A_LANG_AVS_LABEL_GROUP_NOT_WEIGHT","not yet defined");
define("A_LANG_AVS_LABEL_GROUP_PARAM","Parameters:");
define("A_LANG_AVS_LABEL_GROUP_QUESTION_RANDOM","mix questions:");
define("A_LANG_AVS_LABEL_GROUP_EVALUATION_PRESENT",A_LANG_AVS_LABEL_EVALUATION_PRESENT);
define("A_LANG_AVS_LABEL_GROUP_COURSE_WEIGHT","courses (weight %):");
define("A_LANG_AVS_LABEL_GROUP_NOT_EVALUATION","There is no evaluation to this groupo");
// labels utilizados no F_AJUSTE.PHP
define("A_LANG_AVS_LABEL_FUNCTION_AVERAGE","adjust average");
define("A_LANG_AVS_LABEL_FUNCTION_QUESTION_DISSERT","fix expatiate questions");
define("A_LANG_AVS_LABEL_FUNCTION_QUESTION_CANCEL","Cancel questions of the evaluation");
// labels utilizados no F_AJUSTE_ANULAR.PHP
define("A_LANG_AVS_LABEL_CHOOSE_EVALUATION","select the evaluation");
define("A_LANG_AVS_LABEL_CHOOSE_COURSE","Select the course");
define("A_LANG_AVS_LABEL_CHOOSE_TOPIC","select the concept");
define("A_LANG_AVS_LABEL_CHOOSE_STUDENTS","Choose students who will do the evaluation");

// labels utilizados no ambiente do aluno
define("A_LANG_AVS_LABEL_USER_NOT_EVALUATION","there are no released evaluations");
define("A_LANG_AVS_LABEL_USER_NOT_VALUE","there are no notes available");
// labels das telas de quest&otilde;es
define("A_LANG_AVS_LABEL_QUESTION_ENUNCIATE","Working of the question");
define("A_LANG_AVS_LABEL_QUESTION_EXPLANATION","explication=");
define("A_LANG_AVS_LABEL_QUESTION_OPTION_CORRECT","which one is the option correct?");
define("A_LANG_AVS_LABEL_QUESTION_VF_CORRECT","True");
define("A_LANG_AVS_LABEL_QUESTION_VF_FALSE","False");
define("A_LANG_AVS_LABEL_QUESTION_VF_NAME_CORRECT","True name");
define("A_LANG_AVS_LABEL_QUESTION_VF_NAME_FALSE","false name");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_DISSERT","Kind of question is expatiate");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_LACUNA","Kind of question is Lacuna");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_ME","kind of question is  multiple-choice");
define("A_LANG_AVS_LABEL_QUESTION_IDENTIFY_VF"," kind of questions is true or false");

define("A_LANG_AVS_LABEL_AJAX_DIVULGE1","Ok");
define("A_LANG_AVS_LABEL_AJAX_DIVULGE2","Pending");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE1","Pending");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE2","Making");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE3","delivered");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE4","not delivered");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE5","With zero");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION1","list of notes available");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION2","Topic");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION3","Description");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION4","list of notes that it was ended");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION5","Presential Evaluation");
define("A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION6","corrected");
define("A_LANG_AVS_LABEL_AJAX_AVERAGE","group of evaluation to the topic=");

define("A_LANG_AVS_LABEL_AJAX_ZERO","The note with zero will be deleted");

define("A_LANG_AVS_LABEL_SEARCH","Search by keyword");



// -------------- FIM DOS LABELS ------------------------------------------------------------ //
// -------------- COMBOBOX ----------------------------------------------------------------- //

define("A_LANG_AVS_COMBO_SELECT","Select...");
define("A_LANG_AVS_COMBO_DATE","Liberate");

// utilizado no F_AJUSTE_ANULAR.PHP
define("A_LANG_AVS_COMBO_TOPIC","Topic:");
define("A_LANG_AVS_COMBO_MARK","Mark");
define("A_LANG_AVS_COMBO_UNMARK","Unmark");


// -------------- TTULOS DE TABELAS -------------------------------------------------------- //
define("A_LANG_AVS_TABLE_MODIFY","Change");
define("A_LANG_AVS_TABLE_DELETE","Delete");
define("A_LANG_AVS_TABLE_DESCRIPTION","Description");
// t&iacute;tulo do AVS_NOVA_AVAL.PHP
define("A_LANG_AVS_TABLE_TYPE","type");
define("A_LANG_AVS_TABLE_WEIGHT","weight(%)");
define("A_LANG_AVS_TABLE_WEIGHT_DEFAULT","Default weight(%)");
define("A_LANG_AVS_TABLE_WEIGHT_MOD","wight was change(%)");
define("A_LANG_AVS_TABLE_SUM_WEIGHT","sunumber of questionsm of weights");
define("A_LANG_AVS_TABLE_REST_WEIGH","wight remaining");
// t&iacute;tulo do AVS_TOPICO.PHP
define("A_LANG_AVS_TABLE_DIVULGE","Disclosure");
define("A_LANG_AVS_TABLE_NUMBER_QUESTION","number of questions");
define("A_LANG_AVS_TABLE_DATE_START","liberation date");
define("A_LANG_AVS_TABLE_DATE_FINISH","ending date");
define("A_LANG_AVS_TABLE_DATE","Date");
define("A_LANG_AVS_TABLE_VALUE_CANCELLED","canceled notes");
define("A_LANG_AVS_TABLE_ACTION","Actions");
define("A_LANG_AVS_TABLE_DATE_CREATE","Creating");
define("A_LANG_AVS_TABLE_DATE_MODIFY","modifing");
define("A_LANG_AVS_TABLE_DIVULGE_AUTO","Automatic");
define("A_LANG_AVS_TABLE_DIVULGE_MANUAL","Manual");
// titulo dos scripts do ambiente do aluno (N_INDEX, N_MURAL)
define("A_LANG_AVS_TABLE_USER_EVALUATION","Evaluation");
define("A_LANG_AVS_TABLE_USER_DATE","Start Date");
define("A_LANG_AVS_TABLE_USER_REALIZE_EVALUATION","Realize evaluation");
define("A_LANG_AVS_TABLE_USER_DESCRIPTION_EVALUATION","Description");
define("A_LANG_AVS_TABLE_USER_DATE_REALIZE","Date realize");
define("A_LANG_AVS_TABLE_USER_VALUE","Notes");
define("A_LANG_AVS_TABLE_USER_VALUE2","note of test");
define("A_LANG_AVS_TABLE_USER_TOTAL_VALUE","total note");
// titulos de tabelas das telas de quest&otilde;es
define("A_LANG_AVS_TABLE_QUESTION_LACUNA1","Initial text");
define("A_LANG_AVS_TABLE_QUESTION_LACUNA2","Answer of Lacuna=");
define("A_LANG_AVS_TABLE_QUESTION_LACUNA3","Continuation of the text");
define("A_LANG_AVS_TABLE_QUESTION_ME1","Alternatives");
define("A_LANG_AVS_TABLE_QUESTION_ME2","Right?");
define("A_LANG_AVS_TABLE_QUESTION_ME3","Fixed");
define("A_LANG_AVS_TABLE_QUESTION_ME4","description of alternatives");
define("A_LANG_AVS_TABLE_QUESTION_ME5","Delete");
// titulos utilizados nas tabelas do ajuste de notas dissertativa
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT","Student");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT1","Pending questions");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT2","Student does not complete the evaluation");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT3","correcting evaluation");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT4","Change correcting");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT5","Weight");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT6","Note");
define("A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT7","Explication");

// titulos utilizados nos ajax
define("A_LANG_AVS_TABLE_AJAX_AVERAGE","Student");
define("A_LANG_AVS_TABLE_AJAX_AVERAGE1","Average");
define("A_LANG_AVS_TABLE_AJAX_AVERAGE2","Note of participation");
define("A_LANG_AVS_TABLE_AJAX_AVERAGE3","final average");
define("A_LANG_AVS_TABLE_AJAX_QUESTION_CANCEL","Cancel");
define("A_LANG_AVS_TABLE_AJAX_QUESTION_UNDO","undo");
define("A_LANG_AVS_TABLE_AJAX_QUESTION_CANCEL2","Cancelled");
define("A_LANG_AVS_TABLE_AJAX_DIVULGE1","Student");
define("A_LANG_AVS_TABLE_AJAX_DIVULGE2","Pending");

// hints - texto flutuante informativo
define("A_LANG_AVS_HINTS_EVALUATION_EDIT","edit evaluation. Add new questions");
define("A_LANG_AVS_HINTS_EVALUATION_DELETE","Delete evaluation");
define("A_LANG_AVS_HINTS_EVALUATION_VIEW","View Evaluation");
define("A_LANG_AVS_HINTS_USER_EVALUATION_VIEW","This note is correct in the test, excluding questions canceled by teacher");
define("A_LANG_AVS_HINTS_QUESTION_DELETE","Delete this alternative");
define("A_LANG_AVS_HINTS_AJAX_QUESTION_CANCEL","This question has been previously canceled");
define("A_LANG_AVS_HINTS_AJAX_DIVULGE1","Right the expatiate questions");
// textos
define("A_LANG_AVS_TEXT_LIBERATE","this is the number of available evaluation. Store it to consult in the future");
define("A_LANG_AVS_TEXT_FIELD_REQUIRE","The fields with <b>*</b> are required ");
define("A_LANG_AVS_TEXT_ADJUSTMENT","The fields <b> explication </b> will be presented to students as an explanation of the error or success. Its completion is not obligatory");
define("A_LANG_AVS_TEXT_LIBERATE_STUDENT","Students with * have already made this evaluatin.If they are selected again the old will be deleted.");

// ------------- TEXTOS UTILIZADOS EM JAVASCRIPT ---------------------------------------------//
// OBS: lembre-se em Javascript escreva sem htmlentities ( a = &aacute;)
define("A_LANG_AVS_JS_EVALUATION_CONFIRM","would you like to send this evaluation?");
define("A_LANG_AVS_JS_DELETE_PRESENT_CONFIRM","All evaluations will be deleted if the option is chosen presential");
define("A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT","There are some questions without weights defined");
define("A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT2","The sum o weights of the questions should not exceed one hundred percent (100%)
");
define("A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT3","The weight of questions must be one hundred percent");
define("A_LANG_AVS_JS_DELETE_QUESTION_CONFIRM","Would you like to delete this question?");
define("A_LANG_AVS_JS_ALERT_QUESTION","the weights was not save yet");
define("A_LANG_AVS_JS_ALERT_EVALUATION","you must choose one evaluation");
define("A_LANG_AVS_JS_ALERT_QUESTION_REQUIRE","The field ");
define("A_LANG_AVS_JS_ALERT_QUESTION_REQUIRE2"," must be completed!");
define("A_LANG_AVS_JS_ALERT_QUESTION_LACUNA_DELETE","would you like to delete this lacuna?");
define("A_LANG_AVS_JS_ALERT_QUESTION_ME_CORRECT","none of alternative was chose as right");
define("A_LANG_AVS_JS_ALERT_QUESTION_ME_REQUIRE","the question was not complete");
define("A_LANG_AVS_JS_ALERT_QUESTION_ME_DELETE","would you like to delete this alternative?");
define("A_LANG_AVS_JS_ALERT_QUESTION_VF_REQUIRE_CORRECT","you must be choose one of options to be the correct answer");
define("A_LANG_AVS_JS_ALERT_STUDENTS"," it is necessary to choose  students");
define("A_LANG_AVS_JS_ALERT_EVALUATION_DELETE_CONFIRM","Would you like to delete this evaluation?");
// ------------- FIM DO JAVASCRIPT ------------------------------------------------------------//
define("A_LANG_AVS_FORMAT_DATE","%d/%m/%Y"); // string da funcao date para modificar o estilo da data
define("A_LANG_AVS_FORMAT_DATE_TIME","%d/%m/%Y %H:%M:%S"); // string da funcao date para modificar o estilo da data

/**********************************************************************************************/
/*********************************** FIM DA AVALIACAO **************************************/

/********************************************************************************************
 * Mural de Recados (Definido por Laisi Corsani)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Estagio Curricular.
 * Orientadora: Isabela Gasparini
 * Supervisora: Avanilde Kemczinski
 */
define("A_LANG_MURAL_DISC","Mural of message of discipline ");
define("A_LANG_MURAL_ENVIARECADO_DISC","send message in the discipline");
define("A_LANG_MURAL_ENVIARECADO","Send message");
define("A_LANG_MURAL_ESCREVERECADO","Write message");
define("A_LANG_MURAL_OUTRORECADO","Write other message");
define("A_LANG_MURAL_ASS", "SUJECT:");
define("A_LANG_MURAL_REM", "SENDER");
define("A_LANG_MURAL_REC","MESSAGE:");
define("A_LANG_MURAL_DT","MESSAGE:");
define("A_LANG_MURAL_ALUNOS","Studentes:");
define("A_LANG_MURAL_DEST","Receiver: ");
define("A_LANG_MURAL_LISTREC","messages received on last seven days:");
define("A_LANG_MURAL_SENDEMAIL","send warning email");
define("A_LANG_MURAL_ASSMIN","Suject::");
define("A_LANG_MURAL_MSG","Message:");
define("A_LANG_MURAL_PROF","Teacher");
define("A_LANG_MURAL_GROUP","Group");
define("A_LANG_REC_SUSS","message was sent successfully!");
define("A_LANG_REC_INSUSS","unable to sned the message");
define("A_LANG_REC_INSUSS_ALL","unable to sned the message to all receiver");
define("A_LANG_REC_ALERT_DEST","select a receiver!");
define("A_LANG_REC_ALERT_MSG","unable send a message without content");
define("A_LANG_REC_ALERT_ALL","Unable to send a message! To post a message you must select a person and write a message!");
define("A_LANG_MURAL_BACK","turn back to Mural of Message");
define("A_LANG_LENVIRONMENT_MURAL", "Mural of Message");

/*********************************** FIM MURAL RECADOS **************************************/

/********************************************************************************************
 * Altera��es para Usabilidade (Definido por Carla Cristina Lui Dias)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Estagio Curricular.
 * Orientadora: Isabela Gasparini
 * Supervisora: Avanilde Kemczinski
 */

define("A_LANG_SYSTEM_NAME_DESC","Adaptive Web based learning Environment (AdaptWeb) ");
define("A_LANG_SYSTEM_WELCOME","Welcome to");
define("A_LANG_INTRO_DESC_1","This environment allows the teacher to provide materials for him students.");//calra
define("A_LANG_INTRO_DESC_2","To navigate the environment, the teacher should initially make your user account on the item <u> New User </u> in the menu of options. Made after the registration, the teacher must sign in to complete your item <u> Login </u> from the menu of options. If the teacher already has a register, only to complete your login.");//calra
define("A_LANG_INTRO_DESC_3","When the login is performed, the options menu is changed and the teacher by clicking the item <u> Environment Professor </u> can interact with the environment to create him disciplines. Clicking on this item, a submenu will appear with the title Teacher <b> Environment </b> at the end of the main menu, so the teacher can apply for and attend (s) subject (s) available (s) by teachers or by him same, you can create courses, students free, to review the log, apply evaluation, among others.");//calra
define("A_LANG_INTRO_DESC_4","This environment allows the students have access to (s) material (s) instructional (s) disclosed (s) by (s) your (s) teacher (s) according to your profile. <p align=justify> To navigate the environment, the student must first complete your user account on the item <u> New User </u> in the menu of options. Made after the registration, the student must sign in to complete your item <u> Login </u> from the menu of options. If the student already has a register, only to complete your login."); //carla
define("A_LANG_INTRO_DESC_5","When the login is performed, the options menu is changed and the student must click on the item <u> Student Environment </u>. Clicking on this item, a submenu will appear with the title <b> Environment Student </b> at the end of the main menu, so the student can apply for and attend (s) subject (s) available (s) by (s) him (s) teacher (s).");//Carla
// Itens de Autoria
define("A_LANG_AUTHOR","Authoring Environment");//carla
define("A_LANG_STUDENT", "Environments student"); //carla para n_entrada_navegacao
define("A_LANG_AUTHOR_INTRO_1","The module allows the author to author content tailored to provide different profiles of students. Through the process of authorship, the author can provide the content of their classes in a single structure adapted to the different courses. At this stage the author must register for the discipline and courses that want to make it available.");//carla
define("A_LANG_AUTHOR_INTRO_2","After the registration insert the concept of the contents of files, relating them to each topic of discipline. Furthermore the author must inform the description of the topic, and its pre-requisite courses which want to provide content. The author can insert exercises, examples and additional materials for each topic."); //Carla

// Estudantes
define("A_LANG_INTRO_STUD_DESC","This environment allows the students have access to (s) material (s) instructional (s) disclosed (s) by (s) your (s) teacher (s) according to your profile.
<p align=justify> To access (s) your (s) subject (s), the student must first click on the item
Student <u> Environment </u> from the menu of options. Clicking this item will appear a
  sub - menu with the title <b> Environment Student </b> at the end of the main menu, so the user must click
  Request the item <u> Registration </u> in the sub - menu for the current request which is related to the
  disciplines who want to ask him.
<p align=justify> Upon request of the course the user should mark the discipline they wish to request that
the teacher and click on request. After the discipline required the user must wait
the release of the teacher so he can attend the discipline required.");//Carla
define("A_LANG_REQUEST_ACCESS_MSG11","The field E-mail was filled with invalid value");//modificado por Carla, inseri * em Mar�o de 2009

/*********************************** FIM USABILIDADE **************************************/

/********************************************************************************************
 * F�rum de Discuss�o (Definido por Claudia da Rosa)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como TCC - Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora Avanilde Kemczinski
 */

define("A_LANG_TOPICO_INSUSS","Unable to create the topic!");
define("A_LANG_RESPOSTA_INSUSS","Unable to respond to the topic!");
define("A_LANG_TOPICO_SUSS","Topic created successfully!");
define("A_LANG_RESPOSTA_SUSS","Answer performed successfully !");
define("A_LANG_TOP_ALERT_TITULO","Unable to create your topic! Enter a title!");
define("A_LANG_TOP_ALERT_DESCR","Unable to create your topic! Enter your description!");
define("A_LANG_TOP_ALERT_RESP_DESCR","Unable to answer the topic! Enter your description!");
define("A_LANG_TOP_ALERT_AVAL","Select one of the options for evaluation!");
define("A_LANG_FORUM_OUTRO_TOPICO","Create another topic");
define("A_LANG_TOP_ALERT_ALL","Unable to create your topic! Enter a title and description!");
define("A_LANG_TOP_ALERT_ALL_PROF","Unable to create your topic! Enter the title, description and select the Evaluation!");
define("A_LANG_TOP_ALERT_ALL_AVAL_DESCR","Unable to create your topic! Enter the description and select evaluation!");
define("A_LANG_TOP_ALERT_ALL_AVAL_TITULO","Unable to create your topic! Enter a title and choose the evaluation!");
define("A_LANG_FORUM_DISC","Discussion Forum of discipline ");
define('A_LANG_LENVIRONMENT_FORUM', 'Discussion Forum');
define("A_LANG_FORUM_BACK","Back to Discussion Forum");
define("A_LANG_TOPICO_ESCOLHIDO_BACK","Back to Topic Chosen");
define("A_LANG_FORUM_LISTTOP","Topics raised in recent days:");
define("A_LANG_FORUM_TITULO","Title: ");
define("A_LANG_FORUM_TITULO_TOP","Title of topics: ");
define("A_LANG_FORUM_DESCR","Description");
define("A_LANG_FORUM_CRIARTOPICODISCUSSAO","Create Topic of Discussion");
define("A_LANG_FORUM_RESPONDERTOPICO","Reply Thread");
define("A_LANG_FORUM_AVAL","Evaluation");
define("A_LANG_FORUM_SIM","Yes");
define("A_LANG_FORUM_NAO","No");
define("A_LANG_FORUM_QUAL_INS","dissatisfactory!");
define("A_LANG_FORUM_QUAL_REG","Regular");
define("A_LANG_FORUM_QUAL_BOM","Good");
define("A_LANG_FORUM_QUAL_EXC","Excellent");
define("A_LANG_FORUM_AVAL_SUSS","Rating  of the share the student successfully performed!");
define("A_LANG_FORUM_AVAL_INSUSS","Unable to make evaluations of the shares!");
define("A_LANG_FORUM_LIST_ALUN_MAT","Students registered:");
define("A_LANG_FORUM_AVAL_ALUN_BACK","Back to evaluate Students");
define("A_LANG_FORUM_ALUNO","Student");
define("A_LANG_FORUM_AVAL_PARTIC","There is no participation of students for this topic!");
define("A_LANG_FORUM_AVAL_OBS","Note: To Realize a formative evaluation of students you must first evaluate the topics of discussion individually, via the link <u> Yes </u> column \"Evaluation \"");

/*********************************** FIM FORUM DISCUSSAO **************************************/

/********************************************************************************************
 * Altera��es para Usabilidade (Definido por Carla Cristina Lui Dias)
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Estagio Curricular.
 * Orientadora: Isabela Gasparini
 * Supervisora: Avanilde Kemczinski
 */
define("A_LANG_EMAIL_ERRO","The warning message was not sent the email to all recipients! <br> <br> Are invalid in email list.");
/*********************************** FIM ERRO EMAIL **************************************/

?>
