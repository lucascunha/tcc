		<form id="formCadastrar" method="post" action="cadastrar.php">
        	<input type="hidden" name="cTipoUsuario" value="aluno">        
            <p class="field-description">Nome <strong>*</strong></p>
        	<input type="text" name="cNome_Usuario" class="field" value="<?=$cAuxNomeUsuario?>" placeholder="Nome"/>
            <? if(!$logado){?>
            <p class="field-description">E-mail <strong>*</strong></p>
            <input type="email" name="cEmail_Usuario" class="field" value="<?=$cAuxEmail_Usuario?>" placeholder="email@domínio.com"/>
            <input type="hidden" name="alterar" value="true">        
            <? }else{?>
            <p class="field-description">E-mail</p>
            <?=$cAuxEmail_Usuario?>
            <input type="hidden" name="gravar" value="true">                    
            <? }?>
            <p class="field-description">Senha <? if(!$logado){?><strong>*</strong><? }?></p>
            <input type="password" name="cSenha_Usuario" class="field" value="" placeholder="Senha"/>
			<p class="field-description">Confirmar <? if(!$logado){?><strong>*</strong><? }?></p>
            <input type="password" name="cSenha_Usuario2" class="field" value="" placeholder="Repita a senha"/>
			<p class="field-description">Instituição de Ensino</p>
            <input type="text" name="cInstituicao_Ensino" class="field" value="<?=$cAuxInstituicao_Ensino?>" placeholder="Instituição de Ensino"/>
			<span class="buttonBig"><input type="submit" id="button" class="buttonBigYellow" value="Gravar"></span>
            <p class="field-description demo">Observação: Os campos que possuem <strong>*</strong> devem ser obrigatoriamente preenchidos.!</p>
		</form>  