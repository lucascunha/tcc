<?php

/*
 * Limpa os recados a mais de sete (7) dias no BD
 * @author Laisi Corsani <laisicorsani@gmail.com>
 * @version 1.0 <25/07/2008>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Estagio Curricular.
 * Orientadora: Isabela Gasparini
 * Suporvisora: Avanilde Kemczinski
 *
 */

global $id_aluno,$tipo,$logado,$num_disc,$num_curso,$id_prof,$disciplina, $curso;

$usuario=$A_DB_USER;
$senha=$A_DB_PASS;
$nomebd=$A_DB_DB;

include "include/conecta.php";

$dia = date(d) - 6;
if($dia <= 0){
	$resto = 0-$dia;
	$m = date(m)-1;
	if(($m==1) || ($m==3) || ($m==5) || ($m==7) || ($m==8) || ($m==10) || ($m==12)){
		$dia = 31-$resto;
		$data_lim = date(Y)."-".$m."-".$dia;
	}else if (($m==4)||($m==6)||($m==9)||($m==11)){
		$dia = 30-$resto;
		$data_lim = date(Y)."-".$m."-".$dia;
	}else{
		if(((date(Y)%400)==0) || (((date(Y)%4)==0)&&((date(Y)%100)!=0))){  
			$dia = 29-$resto;
			$data_lim = date(Y)."-".$m."-".$dia;
		}else{
			$dia = 28-$resto;
			$data_lim = date(Y)."-".$m."-".$dia;
		}
	}
}else{
	$data_lim = date(Y)."-".date(m)."-".$dia;
} 

$sql1 = "SELECT id_recado FROM destinatario WHERE dta<'$data_lim'";
$res = mysql_query($sql1,$id);
for($i=0;$i<mysql_num_rows($res);$i++){
	$row = mysql_fetch_row($res);
	$sql2 = "DELETE FROM mural WHERE id_recado='$row[0]'";
	$res2 = mysql_query($sql2,$id);
	$sql3 = "DELETE FROM destinatario WHERE id_recado='$row[0]'";
	$res3 = mysql_query($sql3,$id);
}

?>

