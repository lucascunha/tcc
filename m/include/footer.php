    <div data-theme="b" data-role="footer" id="footer">
        <div class="copyright">Licensed under the GNU GENERAL PUBLIC LICENSE Version 2</div>
        
        <? if($_SERVER['PHP_SELF'] != "/m/index.php"){ ?>
        <div data-role="navbar" data-iconpos="left">
            <ul>
                <? if($_SERVER['PHP_SELF'] != "/m/a_painel.php"){ ?>
                <li>
                    <a href="#" data-rel="back" data-icon="arrow-l" data-inline="true">
                        Voltar
                    </a>
                </li>
                <li>
                    <a href="index.php" data-icon="home">
                        Início
                    </a>
                </li>
                <? } ?>                
                <li>
                    <a href="#top" class="top" data-icon="arrow-u">
                        Topo
                    </a>
                </li>
            </ul>
        </div>    
        <? } ?>
            
    </div>
