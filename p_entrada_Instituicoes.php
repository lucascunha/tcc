<?
/** -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Informática  
 *       UEL - Departamento de Computação
 * -----------------------------------------------------------------------
 *         @name p_entrada_Instituicoes.php
 *     @abstract Informações sobre instituições participantes
 *        @since 25/06/2003
 *       @author Veronice de Freitas (veronice@jr.eti.br)
 *      @package AdaptWeb  
 * -----------------------------------------------------------------------
 */ 
?>

<script type="text/javascript"> 
	
	// $(".item img").css({"display":"none");
	
	// On window load. This waits until images have loaded which is essential
	$(window).load(function(){
		
		// Fade in images so there isn't a color "pop" document load and then on window load
		$(".item img").animate({opacity:1},100);
		
		// clone image
		$('.item img').each(function(){
			var el = $(this);
			el.css({"position":"absolute"}).wrap("<div class='img_wrapper' style='display: inline-block'>").clone().addClass('img_grayscale').css({"position":"absolute","z-index":"998","opacity":"0"}).insertBefore(el).queue(function(){
				var el = $(this);
				el.parent().css({"width":this.width,"height":this.height});
				el.dequeue();
			});
			this.src = grayscale(this.src);
		});
		
		// Fade image 
		$('.item img').mouseover(function(){
			$(this).parent().find('img:first').stop().animate({opacity:1}, 100);
		})
		$('.img_grayscale').mouseout(function(){
			$(this).stop().animate({opacity:0}, 100);
		});		
	});
	
	// Grayscale w canvas method
	function grayscale(src){
        var canvas = document.createElement('canvas');
		var ctx = canvas.getContext('2d');
        var imgObj = new Image();
		imgObj.src = src;
		canvas.width = imgObj.width;
		canvas.height = imgObj.height; 
		ctx.drawImage(imgObj, 0, 0); 
		var imgPixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
		for(var y = 0; y < imgPixels.height; y++){
			for(var x = 0; x < imgPixels.width; x++){
				var i = (y * 4) * imgPixels.width + x * 4;
				var avg = (imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3;
				imgPixels.data[i] = avg; 
				imgPixels.data[i + 1] = avg; 
				imgPixels.data[i + 2] = avg;
			}
		}
		ctx.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
		return canvas.toDataURL();
    }
	    
</script> 

<? 
  // cria matriz de orelhas
  $orelha = array(
  		array(   
   		   "LABEL" => A_LANG_SYSTEM , 
     		   "LINK" => "index.php?opcao=Apresentacao", 
     		   "ESTADO" =>"OFF"
   		   ), 		   
  		array(   
   		   "LABEL" => "O Projeto" , 
     		   "LINK" => "index.php?opcao=EntradaInstituicoes", 
     		   "ESTADO" =>"ON"
   		   ),   		 

  		array(   
   		   "LABEL" => "Demonstração" , 
     		   "LINK" => "index.php?opcao=Demo", 
     		   "ESTADO" =>"OFF"
   		   ),   
  	//	array(   
   	//	   "LABEL" => A_LANG_RESEARCHES , 
    // 		   "LINK" => "index.php?opcao=EntradaPesquisadores", 
    // 		   "ESTADO" =>"OFF"
   //		   ), 
  	//	array(   
  	//	   "LABEL" => A_LANG_AUTHORS , 
    //		   "LINK" => "index.php?opcao=EntradaAutores", 
     //		   "ESTADO" =>"OFF"
   	//	   )    		       		   		   
   		  ); 

  MontaOrelha($orelha);  

?>

<table CELLSPACING=5 CELLPADDING=5 border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?> height="80%" style="height:380px;" >

	<tr valign="top">
		<td>	
	<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; height:340px;" class="tabela_redonda" >
	<tr>
		<td valign=top>			
				<div class="bola" style='width:98%; margin:0 0 0 10px;'>
      			<h2 style="padding-top:10px; padding-left:15px;">
			 	   <?
				     echo A_LANG_SYSTEM_NAME_DESC;
				   ?> 	
  	      		</h2>
  	      		</div>
				<br>		
				<table border="0" width= "98%" style='margin:0 0 0 10px;'>
					<tr>
						<td colspan=3> 
						<p align="justify">  		
						<?
							echo A_LANG_PROJETO_INTRO;
						?>
						</p>
						</td>
					</tr>
			

					<tr >
						<td  >	
						<table border="0" width= "100%" cellpading=10 cellspacing=20>
						<tr>
						<td>	

						<div class="item"> 					
							<a href="http://www.ufrgs.br" target="_blank"><img border = "0" src="imagens/ufrgs_logo.gif"></a>
						</div>	
						</td>
						<td >
						<div class="item"> 													
						      <a href="http://www.inf.ufrgs.br" target="_blank"><img border="0" src="imagens/Infufrgs.gif"></a>
						</div>	        
						</td>		
			
						<td >		
						<div class="item"> 											
						      <a href="http://www.joinville.udesc.br" target="_blank"><img border="0" src="imagens/udescj.gif"></a>
						</div>	        
						</td>	

					</tr>
				

					<tr>
						<td >
						<div class="item"> 		
							<a href="http://www.uel.br" target="_blank"><img border = "0" src="imagens/uel_logo.gif" ></a>	
						</div>						
						</td>
						<td >
						<div class="item" > 																							
							<a href="http://www.cnpq.br" target="_blank"><img border="0" src="imagens/Cnpqx.gif" ></a>
						</div>	
						</td>
						<td >		
						<div class="item"> 											
						      <a href="http://sourceforge.net/projects/adaptweb/" target="_blank"><img border="0" src="imagens/sourceforge.png"></a>
						</div>	        
						</td>		
	
					</tr>
				
</table><br>
</td>
</tr>

				</table>

		</td>
	</tr>
</table><br>

		</td>
	</tr>
</table>