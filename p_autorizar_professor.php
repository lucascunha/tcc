<?
/* -----------------------------------------------------------------------
 *  AdaptWeb - Projeto de Pesquisa       
 *     UFRGS - Instituto de Inform�tica  
 *       UEL - Departamento de Computa��o
 * -----------------------------------------------------------------------
 *       @package Autorizar acesso 
 *     @subpakage Autorizar acesso - professor 
 *          @file p_autorizar_professor.php
 *    @desciption autoriza acesso para os professores para autoria (root) 
 *         @since 25/06/2003
 *        @author Veronice de Freitas (veronice@jr.eti.br)
 * -----------------------------------------------------------------------         
 */  


  global $NomeDisciplina, $conteudo, $tipo_usuario,$conteudo, $A_DB_TYPE, $A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB;
 
  $orelha = array();  
  
if ($tipo_usuario == "root")
{
  $orelha = array(
  		array(   
   		   "LABEL" => A_LANG_LIBERATE_USER2, 
     		   "LINK" => "",    
     		   "ESTADO" =>"ON"
   		   )		     		       		   		  
   		  ); 

}

$BloquearAcesso = $_POST['BloquearAcesso'];
$TirarAcesso = $_POST['TirarAcesso'];
$TirarAcesso1 = $_POST['TirarAcesso1'];
$PermitirAcesso = $_POST['PermitirAcesso'];
$ProfAut = $_POST['ProfAut'];
$ProfNaoAut = $_POST['ProfNaoAut'];
$ProfBloq = $_POST['ProfBloq'];



 
MontaOrelha($orelha);    

// atualiza status_usuario como 'nao autorizado'
if (isset($TirarAcesso1) and count($ProfBloq) > 0 )   
{		
	$conn = &ADONewConnection($A_DB_TYPE); 
	$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	
	for($j=0; $j < count($ProfBloq); $j++) 
	{		
		$sql = "UPDATE usuario SET status_usuario='nao autorizado' WHERE id_usuario=$ProfBloq[$j].";			
		$rs = $conn->Execute($sql);     
		if ($rs === false) die(A_LANG_DISCIPLINES_MSG2);  				     
	}
	$rs->Close(); 		
}

if (isset($TirarAcesso) and count($ProfAut) > 0 )   
{		
	$conn = &ADONewConnection($A_DB_TYPE); 
	$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	
	for($j=0; $j < count($ProfAut); $j++) 
	{		
		$sql = "UPDATE usuario SET status_usuario='nao autorizado' WHERE id_usuario=$ProfAut[$j].";			
		$rs = $conn->Execute($sql);     
		if ($rs === false) die(A_LANG_DISCIPLINES_MSG2);  				     
	}
	$rs->Close(); 		
}

if (isset($BloquearAcesso) and count($ProfNaoAut) > 0 )   
{		
	$conn = &ADONewConnection($A_DB_TYPE); 
	$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	
	for($j=0; $j < count($ProfNaoAut); $j++) 
	{		
		$sql = "UPDATE usuario SET status_usuario='bloqueado' WHERE id_usuario=$ProfNaoAut[$j].";			
		$rs = $conn->Execute($sql);     
		if ($rs === false) die(A_LANG_DISCIPLINES_MSG2);  				     
	}
	$rs->Close(); 		
}

// atualiza status_usuario como 'autorizado'
if (isset($PermitirAcesso) and count($ProfNaoAut) > 0)   
{	
	$conn = &ADONewConnection($A_DB_TYPE); 
	$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	
	for($j=0; $j < count($ProfNaoAut); $j++) 
	{		
		$sql = "UPDATE usuario SET status_usuario='autorizado' WHERE id_usuario=$ProfNaoAut[$j].";	
		$rs = $conn->Execute($sql);     		
		
		if ($rs === false) die(A_LANG_DISCIPLINES_MSG2);  				     
	}
	$rs->Close(); 		
}
?>


<form name="adicao" action="index.php?opcao=AutorizarAcessoAutor" method="post">
<table CELLSPACING=5 CELLPADDING=3 border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?>  style="height:350px;">
<tr valign="top">
	<td>      
		<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">
		<tr>
			<td>

				<table border ="0" CELLSPACING=15 CELLPADDING=0>	
					<tr>							
		    			<td valign = "top"  align="right" style="width:150px;" >
							<? echo "Professores Pendentes"; ?>:
	  	     			</td>

		     			<td valign = "top" >
							<?			
					  			$conn = &ADONewConnection($A_DB_TYPE); 
					  			$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
					  			$sql="select id_usuario,nome_usuario, instituicao_ensino, email_usuario from usuario where tipo_usuario='professor' and status_usuario='nao autorizado' order by nome_usuario" ;
					  			$rs = $conn->Execute($sql);	  
					  		?>
					  		<select class="button" NAME="ProfNaoAut[]" MULTIPLE style="width:550px; height:300px;"> 						 							 
					  		<? 
								while (!$rs->EOF)
								{					 	         									                								                							
								  	if (!($rs->fields[2]))
								  	{
								  		$rs->fields[2] = "Institui��o n�o foi informado";

								  	}
									echo "<optgroup label=\"".strtoupper($rs->fields[1])."\">";
								  	echo "	<option value=\"".$rs->fields[0]."\">".$rs->fields[3].' - ('.$rs->fields[2].')'."</option>";
									echo "</optgroup>";
							        $rs->MoveNext();
     							}
					    	    $rs->Close(); 
					    	?>      	
					    	</select>	
						</td>
						<td valign="middle" height="35">
							<input class=buttonBigBig type="submit" value="Autorizar Acesso  " name="PermitirAcesso">
							<br><br>
							<input class=buttonBigBig type="submit" value="Bloquear Acesso  " name="BloquearAcesso">


					 	</td>
	   				</tr>

		
				</table>
			</td>
		</tr>
		</table>

		
		<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">
		<tr>
			<td>

				<table border ="0" CELLSPACING=15 CELLPADDING=0>	
	
					<tr>
						<td valign = "top"  align="right" style="width:150px;" >
	     					<?  echo "Professores Autorizados";  ?>: 
	     				</td>
	
						<td valign = "top" >
							<?			
				  				$conn = &ADONewConnection($A_DB_TYPE); 
				  				$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
								$sql="select id_usuario,nome_usuario, instituicao_ensino, email_usuario from usuario where tipo_usuario='professor' and status_usuario='autorizado' order by nome_usuario";
				  				$rs = $conn->Execute($sql);	  
				  			?>
							<select class="button" name="ProfAut[]" multiple maxsize="15" style="width:550px; height:200px;"> 						 							 
				  			<? 
								while (!$rs->EOF)
								{					 	         									                								                												  
								  	if (!($rs->fields[2]))
								  	{
								  		$rs->fields[2] = "Institui��o n�o foi informado";

								  	}
									echo "<optgroup label=\"".strtoupper($rs->fields[1])."\">";
								  	echo "	<option value=\"".$rs->fields[0]."\">".$rs->fields[3].' - ('.$rs->fields[2].')'."</option>";
									echo "</optgroup>";

									$rs->MoveNext();
								}
								$rs->Close(); 
				  			?>      	
				  			</select>	
						</td>
						<td valign="middle" height="35">

							<input class=buttonBig type="submit" value="Voltar ao status Pendente" name="TirarAcesso">
					 	</td>						
					</tr>
				</table>
			</td>
		</tr>
		</table>


		<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">
		<tr>
			<td>

				<table border ="0" CELLSPACING=15 CELLPADDING=0>	
	
					<tr>
						<td valign = "top"  align="right" style="width:150px;" >
	     					<?  echo "Professores Bloqueados";  ?>: 
	     				</td>
	
						<td valign = "top" >
							<?			
				  				$conn = &ADONewConnection($A_DB_TYPE); 
				  				$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
								$sql="select id_usuario,nome_usuario, instituicao_ensino, email_usuario from usuario where tipo_usuario='professor' and status_usuario='bloqueado' order by nome_usuario";
				  				$rs = $conn->Execute($sql);	  
				  			?>
							<select class="button" name="ProfBloq[]" multiple maxsize="15" style="width:550px; height:200px;"> 						 							 
				  			<? 
								while (!$rs->EOF)
								{					 	         									                								                												  
								  	if (!($rs->fields[2]))
								  	{
								  		$rs->fields[2] = "Institui��o n�o foi informado";

								  	}
									echo "<optgroup label=\"".strtoupper($rs->fields[1])."\">";
								  	echo "	<option value=\"".$rs->fields[0]."\">".$rs->fields[3].' - ('.$rs->fields[2].')'."</option>";
									echo "</optgroup>";

									$rs->MoveNext();
								}
								$rs->Close(); 
				  			?>      	
				  			</select>	
						</td>
						<td valign="middle" height="35">

							<input class=buttonBig type="submit" value="Voltar ao status Pendente" name="TirarAcesso1">
					 	</td>						
					</tr>
				</table>
			</td>
		</tr>
		</table>

     <br>
    </td>
</tr>
</table>
</form>

