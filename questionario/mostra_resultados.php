<?php  
  $orelha = array();  
  $orelha = array(
  		array(   
   		   "LABEL" => QUESTIONARIO , 
     		   "LINK" => "a_index.php", 
     		   "ESTADO" =>"ON"
   		   )   		       		       		   		   
   ); 
  MontaOrelha($orelha);  
?>
<table CELLSPACING=5 CELLPADDING=5 border="0" width="100%" height="80%" valign="top" bgcolor=<?php echo $A_COR_FUNDO_ORELHA_ON ?> >
	<tr valign="top">
	  <td>	
		<TABLE cellpadding=0 cellspacing=0 BORDER="0" WIDTH = 92%  ALIGN = CENTER bgcolor="#BCD2EE">
			<TR></TD><TD>
				<TABLE cellpadding=0 cellspacing=0 BORDER="0" WIDTH = 100% ALIGN = CENTER bgcolor=<?php echo $A_COR_FUNDO_ORELHA_ON ?>>
					<TR><TD ALIGN = CENTER CLASS="bigtext">Question�rio Proposto - Resultados</TD></TR>
					<TR><TD ALIGN = CENTER>POR: Ana Paula Cristofolini</TD></TR>
					<TR><TD ALIGN = CENTER>ORIENTADORA: Isabela Gasparini</TD></TR>
					<TR><TD ALIGN = CENTER>CO-ORIENTADORA: Avanilde Kemczinski</TD></TR>
					<TR><TD height=10>&nbsp;</TD></TR>
				</TABLE>
			</TD></TR>
			<TR WIDTH = 100%><TD>
				<TABLE WIDTH = 92% ALIGN=CENTER>
					<TR>
						<TD>
					<?					
						$dbNome = "Adaptweb";
						$db = mysql_connect("localhost","adaptweb", "alebasi123");
						$comando1 = "select * from resultado";
						$busca1 = mysql_db_query($dbNome, $comando1);
						$numQuestionarios=mysql_num_rows($busca1);
						if(!$numQuestionarios){
							print ("<p><b><center>Nenhum registro at� o momento<br></center></b></p>");
						} else {
							print ("<p><b><center>".$numQuestionarios." resultados at� o momento<br></center></b></p>");
						}
					
			print ("</TD></TR>");
			print ("<TR><TD>");
					print ("<FONT CLASS=\"titles\"><B>DADOS DO PARTICIPANTE</B></FONT><br><br>");
					print ("<TABLE WIDTH=92%>");
					print ("<TR><TD><b>Idade:</b>");
					$comando2 = "select idade from resultado"; 
					$busca2 = mysql_db_query($dbNome, $comando2);
					$ate18 = 0;
					$de19a28 = 0;
					$de29a38 = 0;
					$de39a48 = 0;
					$de49a58 = 0;
					$acima59 = 0;
					while ($resultado = mysql_fetch_array($busca2)) {
						if ($resultado['idade'] < 18) $ate18++;
						if ($resultado['idade'] > 18 and $resultado['idade'] < 29) $de19a28++;
						if ($resultado['idade'] > 28 and $resultado['idade'] < 39) $de29a38++;
						if ($resultado['idade'] > 38 and $resultado['idade'] < 49) $de39a48++;
						if ($resultado['idade'] > 48 and $resultado['idade'] < 59) $de49a58++;
						if ($resultado['idade'] > 58) $acima59++;
					}
					mysql_free_result($busca1);
					mysql_free_result($busca2);
					if($ate18) { $ate18 = ($ate18 / $numQuestionarios) * 100;  } 
					if($de19a28) { $de19a28 = ($de19a28 / $numQuestionarios) * 100;  } 
					if($de29a38) { $de29a38 = ($de29a38 / $numQuestionarios) * 100;  } 
					if($de39a48) { $de39a48 = ($de39a48 / $numQuestionarios) * 100;  } 
					if($de49a58) { $de49a58 = ($de49a58 / $numQuestionarios) * 100;  } 
					if($acima59) { $acima59 = ($acima59 / $numQuestionarios) * 100;  } 
					print ("<ur>");
					print ("<li>at� 18 anos: ".number_format($ate18,2)."%</li>");
					print ("<li>de 19 a 28 anos: ".number_format($de19a28,2)."%</li>");
					print ("<li>de 29 a 38 anos: ".number_format($de29a38,2)."%</li>");
					print ("<li>de 39 a 48 anos: ".number_format($de39a48,2)."%</li>");
					print ("<li>de 49 a 58 anos: ".number_format($de49a58,2)."%</li>");
					print ("<li>acima de 59 anos: ".number_format($acima59,2)."%</li>");
					print ("</ur><br><br></TD></TR>");
					print ("<TR><TD><b>Profiss�o:</b>");
					$comando3 = "select profissao from resultado order by profissao";
					$busca3 = mysql_db_query($dbNome, $comando3);
					print ("<ur>");
					while ($resultado = mysql_fetch_array($busca3)) {
						if($resultado['profissao'])
							print ("<li>".$resultado['profissao']."</li>");
					}
					print ("</ur><br><br></TD></TR>");
					print ("<TR><TD><b>Grau de escolaridade:</b>");
					$comando4 = "select escolaridade from resultado";
					$busca4 = mysql_db_query($dbNome, $comando4);
					$graduando = 0;
					$graduacao = 0;
					$especializacao	= 0;
					$mestrado = 0;
					$doutorado = 0;
					while ($resultado = mysql_fetch_array($busca4)) {
						if ($resultado['escolaridade'] == "Graduando") $graduando++;
						if ($resultado['escolaridade'] == "Gradua��o") $graduacao++;
						if ($resultado['escolaridade'] == "Especializa��o") $especializacao++;
						if ($resultado['escolaridade'] == "Mestrado") $mestrado++;
						if ($resultado['escolaridade'] == "Doutorado") $doutorado++;
					}
					if($graduando) { $graduando = ($graduando / $numQuestionarios) * 100;  } 
					if($graduacao) { $graduacao = ($graduacao / $numQuestionarios) * 100;  } 
					if($especializacao) { $especializacao = ($especializacao / $numQuestionarios) * 100;  } 
					if($mestrado) { $mestrado = ($mestrado / $numQuestionarios) * 100;  } 
					if($doutorado) { $doutorado = ($doutorado / $numQuestionarios) * 100;  } 
					print ("<ur>");
					print ("<li>Graduando: ".number_format($graduando,2)."%</li>");
					print ("<li>Gradua��o: ".number_format($graduacao,2)."%</li>");
					print ("<li>Especializa��o: ".number_format($especializacao,2)."%</li>");
					print ("<li>Mestrado: ".number_format($mestrado,2)."%</li>");
					print ("<li>Doutorado: ".number_format($doutorado,2)."%</li>");
					print ("</ur><br><br></TD></TR>");
					print ("<TR><TD><b>N�vel de conhecimento sobre Inform�tica:</b>");
					$comando5 = "select informatica from resultado";
					$busca5 = mysql_db_query($dbNome, $comando5);
					$nenhum = 0;
					$baixo = 0;
					$medio	= 0;
					$alto = 0;
					while ($resultado = mysql_fetch_array($busca5)) {
						if ($resultado['informatica'] == "Nenhum") $nenhum++;
						if ($resultado['informatica'] == "Baixo") $baixo++;
						if ($resultado['informatica'] == "M�dio") $medio++;
						if ($resultado['informatica'] == "Alto") $alto++;
					}
					if($nenhum) { $nenhum = ($nenhum / $numQuestionarios) * 100;  } 
					if($baixo) { $baixo = ($baixo / $numQuestionarios) * 100;  } 
					if($medio) { $medio = ($medio / $numQuestionarios) * 100;  } 
					if($alto) { $alto = ($alto / $numQuestionarios) * 100;  } 
					print ("<ur>");
					print ("<li>Nenhum: ".number_format($nenhum,2)."%</li>");
					print ("<li>Baixo: ".number_format($baixo,2)."%</li>");
					print ("<li>M�dio: ".number_format($medio,2)."%</li>");
					print ("<li>Alto: ".number_format($alto,2)."%</li>");
					print ("</ur><br><br></TD></TR>");
					mysql_free_result($busca3);
					mysql_free_result($busca4);
					mysql_free_result($busca5);
					print ("<TR><TD><b>J� utilizou algum ambiente de Educa��o � Dist�ncia:</b>");
					$comando6 = "select jaUtilizou from resultado";
					$busca6 = mysql_db_query($dbNome, $comando6);
					$sim = 0;
					$nao = 0;
					while ($resultado = mysql_fetch_array($busca6)) {
						if ($resultado['jaUtilizou'] == "S") $sim++;
						if ($resultado['jaUtilizou'] == "N") $nao++;
					}
					if($sim) { $sim = ($sim / $numQuestionarios) * 100;  } 
					if($nao) { $nao = ($nao / $numQuestionarios) * 100;  } 
					print ("<ur>");
					print ("<li>N�o: ".number_format($nao,2)."%</li>");
					print ("<li>Sim: ".number_format($sim,2)."%</li>");
					print ("</ur>");
					$comando7 = "select ead from resultado";
					$busca7 = mysql_db_query($dbNome, $comando7);
					$num7 = mysql_num_rows($busca7);
					if(!$num7) 
						print ("<br><i>Nenhum ambiente de EAD foi citado.</i>");
					else {
						print ("<br><br><i>Os ambientes citados foram:<br></i>");
						print ("<ur>");
						while ($resultado = mysql_fetch_array($busca7)) {
							if($resultado['ead'])
								print ("<li><i>".$resultado['ead']."</i></li>");
						}
						print ("</ur><br><br></TD></TR>");
					}
					print ("<TR><TD><b>Qual a experi�ncia no ambiente AdaptWeb:</b>");
					$comando8 = "select experiencia from resultado";
					$busca8 = mysql_db_query($dbNome, $comando8);
					$novato = 0;
					$intermediario = 0;
					$experiente = 0;
					while ($resultado = mysql_fetch_array($busca8)) {
						if ($resultado['experiencia'] == "Novato") $novato++;
						if ($resultado['experiencia'] == "Intermedi�rio") $intermediario++;
						if ($resultado['experiencia'] == "Experiente") $experiente++;
					}
					if($novato) { $novato = ($novato / $numQuestionarios) * 100;  } 
					if($intermediario) { $intermediario = ($intermediario / $numQuestionarios) * 100;  } 
					if($experiente) { $experiente = ($experiente / $numQuestionarios) * 100;  } 
					print ("<ur>");
					print ("<li>Novato: ".number_format($novato,2)."%</li>");
					print ("<li>Intermedi�rio: ".number_format($intermediario,2)."%</li>");
					print ("<li>Experiente: ".number_format($experiente,2)."%</li>");
					print ("</ur><br><br></TD></TR>");
					print ("<TR><TD><b>A quanto tempo voc� utiliza o ambiente AdaptWeb:</b>");
					$comando9 = "select tempo from resultado";
					$busca9 = mysql_db_query($dbNome, $comando9);
					print ("<ur>");
					while ($resultado = mysql_fetch_array($busca9)) {
						if($resultado['tempo'])
							print ("<li>".$resultado['tempo']."</li>");
					}
					print ("</ur><br><br></TD></TR>");
					print ("</TABLE>");
					mysql_free_result($busca6);
					mysql_free_result($busca7);
					mysql_free_result($busca8);
					mysql_free_result($busca9);
					print ("<TABLE cellpadding=0 cellspacing=0 BORDER=\"0\" WIDTH = \"700\" ALIGN = CENTER>");
					$comando10 = "select * from heuristica";
					$busca10 = mysql_db_query($dbNome, $comando10);
					$num10=mysql_num_rows($busca10);
					if($num10){
						while($heuristica = mysql_fetch_array($busca10)){
							print ("<TR BGCOLOR = \"#93BEE2\"><TD></TD>");
								print ("<TD><B>".$heuristica['descHeuristica']."</B></TD>");
								print ("<TD WIDTH=8% ALIGN = CENTER><font class=\"newsdate\"><B>Concordo Totalmente</B></font></TD>");
								print ("<TD WIDTH=8% ALIGN = CENTER><font class=\"newsdate\"><B>Concordo</B></font></TD>");
								print ("<TD WIDTH=8% ALIGN = CENTER><font class=\"newsdate\"><B>Sem Opini�o</B></font></TD>");
								print ("<TD WIDTH=8% ALIGN = CENTER><font class=\"newsdate\"><B>Discordo</B></font></TD>");
								print ("<TD WIDTH=8% ALIGN = CENTER><font class=\"newsdate\"><B>Discordo Totalmente</B></font></TD>");
							print ("</TR>");
							$comando11 = "select idQuestao, desQuestao from fechadas where idHeuristica = '".$heuristica['idHeuristica']."'";
							$busca11 = mysql_db_query($dbNome, $comando11);
							$num11=mysql_num_rows($busca11);
							if($num11){
								$cor = "BCD2EE";
								while($questao = mysql_fetch_array($busca11)){
									$nomeQuestao = "questao".$questao['idQuestao'];
									$comando12 = "select ".$nomeQuestao." from resultado";
									$busca12 = mysql_db_query($dbNome, $comando12);
									$conc_total = 0;
									$conc = 0;
									$sem_opin = 0;
									$disc = 0;
									$disc_total = 0;
									while($resultado = mysql_fetch_array($busca12)) {
										 if($resultado[$nomeQuestao] == 1) $conc_total++;
										 if($resultado[$nomeQuestao] == 2) $conc++;
										 if($resultado[$nomeQuestao] == 3) $sem_opin++;
										 if($resultado[$nomeQuestao] == 4) $disc++;
										 if($resultado[$nomeQuestao] == 5) $disc_total++;
									}
									if($conc_total) { $conc_total = ($conc_total / $numQuestionarios) * 100;  } 
									if($conc) { $conc = ($conc / $numQuestionarios) * 100;  } 
									if($sem_opin) { $sem_opin = ($sem_opin / $numQuestionarios) * 100;  } 
									if($disc) { $disc = ($disc / $numQuestionarios) * 100;  } 
									if($disc_total) { $disc_total = ($disc_total / $numQuestionarios) * 100;  } 
									print ("<TR bgcolor=".$cor."><TD  WIDTH=5%>".$questao['idQuestao']."</TD>");
										print ("<TD WIDTH=55%>".$questao['desQuestao']."</TD>");
										print ("<TD WIDTH=8% ALIGN = CENTER>".number_format($conc_total,2)."%</TD>");
										print ("<TD WIDTH=8% ALIGN = CENTER>".number_format($conc,2)."%</TD>");
										print ("<TD WIDTH=8% ALIGN = CENTER>".number_format($sem_opin,2)."%</TD>");
										print ("<TD WIDTH=8% ALIGN = CENTER>".number_format($disc,2)."%</TD>");
										print ("<TD WIDTH=8% ALIGN = CENTER>".number_format($disc_total,2)."%</TD>");
									print ("</TR>");
									if($cor=="BCD2EE"){
										$cor="DDDDDD";
									}else{
										$cor="BCD2EE";
									}
								}
							}else{
								print ("<I>Nenhuma heuristica foi encontrada</I>");
							}
						}
					} else {
						print ("<I>Nenhuma heuristica foi encontrada</I>");
					}
					mysql_free_result($busca10);
					mysql_free_result($busca11);
					mysql_free_result($busca12);
					mysql_close($db);
					?>
				</TABLE>
			<BR><BR></TD></TR>
			<TR BGCOLOR = "#93BEE2"><TD><B>32. Qual o seu grau de satisfa��o em rela��o ao ambiente?</B></TD></TR>
			<TR><TD>
			<?
			$dbNome = "Adaptweb";
			$db = mysql_connect("localhost","adaptweb", "alebasi123");
			$comando13 = "select questao32 from resultado";
			$busca13 = mysql_db_query($dbNome, $comando13);
			print ("<ur>");
			while ($resultado = mysql_fetch_array($busca13)) {
				if($resultado['questao34'])
					print ("<li>".$resultado['questao32']."</li>");
			}
			mysql_free_result($busca13);
			mysql_close($db);
			?>
			</TD></TR>
			<TR BGCOLOR = "#93BEE2"><TD><B>33. Cite 3 pontos positivos do sistema.</B></TD></TR>
			<TR><TD>
			<?
			$dbNome = "Adaptweb";
			$db = mysql_connect("localhost","adaptweb", "alebasi123");
			$comando14 = "select questao33 from resultado";
			$busca14 = mysql_db_query($dbNome, $comando14);
			print ("<ur>");
			while ($resultado = mysql_fetch_array($busca14)) {
				if($resultado['questao34'])
					print ("<li>".$resultado['questao33']."</li>");
			}
			mysql_free_result($busca14);
			mysql_close($db);
			?>
			</TD></TR>
			<TR BGCOLOR = "#93BEE2"><TD><B>34. Cite 3 pontos negativos do sistema.</B></TD></TR>
			<TR><TD>
			<?
			$dbNome = "Adaptweb";
			$db = mysql_connect("localhost","adaptweb", "alebasi123");
			$comando15 = "select questao34 from resultado";
			$busca15 = mysql_db_query($dbNome, $comando15);
			print ("<ur>");
			while ($resultado = mysql_fetch_array($busca15)) {
				if($resultado['questao34'])
					print ("<li>".$resultado['questao34']."</li>");
			}
			mysql_free_result($busca15);
			mysql_close($db);
			?>
				</TD>
				</TR>
				</TABLE>
			</TD></TR>
		</TABLE>				
	</td>
	</tr>
</table>