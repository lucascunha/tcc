<?php
/**
 * Script para remover uma avalia��o de um grupo
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <21/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

  session_start();
  // verifica se a pessoa est� logada
  if(!isset($_SESSION['logado'])) die("Voc� deve efetuar primeiro o login no sistema para trabalhar com este m�dulo");
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die("<font color='red'>Voc� n�o tem autoriza��o para modificar este m�dulo</font>");


  $id_aval = isset($_REQUEST['aval']) ? $_REQUEST['aval'] : FALSE;

  if(!$id_aval) {
    echo "<script>self.close()</script>";
    exit();
  }

  require_once "../include/adodb/adodb.inc.php";
  require_once "../config/configuracoes.php";
  require_once "class/avaliacao.class.php";
	require_once "funcao/geral.func.php";

  // abrindo conex�o com a base de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

  $aval = new Avaliacao($conn, $id_aval);
  $aval->deletar();

  $conn->Close();

  echo "<script>";
  echo "window.opener.location.reload();";
  echo "self.close();";
  echo "</script>";


?>
