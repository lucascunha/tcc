<?php
/**
 * Faz o ajuste de pesos para o calculo da m�dia
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @subpackage Media
 * @version 1.0 <23/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */
 
	require_once "avaliacao/funcao/geral.func.php";
 
	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	
	if(!$CodigoDisciplina) {
		echo A_LANG_AVS_ERROR_PARAM;
		return;
	}	

  montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_AJUSTE);

?>

<table cellspacing='1' cellpadding='1' width='100%' border='0'  bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
	<tr>
		<td>
		<div class='funcao'>
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_ADJUSTMENT; ?></div>				
			
			<fieldset>
				<legend>Quest&otilde;es</legend>
				<p>
					<a href='<?php echo "a_index.php?opcao=FuncaoAjustarDissertAval&CodigoDisciplina=$CodigoDisciplina"; ?>'><?php echo A_LANG_AVS_LABEL_FUNCTION_QUESTION_DISSERT; ?></a>
				</p>			
			</fieldset>
			<br />			
			<fieldset>
				<legend>Avalia&ccedil;&otilde;es</legend>
				<p>
					<a href='<?php echo "a_index.php?opcao=FuncaoAjustarAnular&CodigoDisciplina=$CodigoDisciplina"; ?>'><?php echo A_LANG_AVS_LABEL_FUNCTION_QUESTION_CANCEL; ?></a>
				</p>
				<p>
					<a href='<?php echo "a_index.php?opcao=FuncaoAjustarAnularDividir&CodigoDisciplina=$CodigoDisciplina"; ?>'><?php echo A_LANG_AVS_LABEL_FUNCTION_QUESTION_CANCEL; ?></a>
				</p>
				<p>
					<a href='<?php echo "a_index.php?opcao=FuncaoAjustarAnularPeso&CodigoDisciplina=$CodigoDisciplina"; ?>'><?php echo A_LANG_AVS_LABEL_FUNCTION_QUESTION_CANCEL; ?></a>
				</p>
				<p>
					<a href='<?php echo "a_index.php?opcao=FuncaoAjustarPresencialGrupo&CodigoDisciplina=$CodigoDisciplina"; ?>'><?php echo A_LANG_AVS_TITLE_ADJUSTMENT_PRESENTIAL; ?></a>
				</p>			
			</fieldset>
			<br />			
			<fieldset>
				<legend>M&eacute;dia</legend>
				<p>
					<a href='<?php echo "a_index.php?opcao=FuncaoAjustarMedia&CodigoDisciplina=$CodigoDisciplina"; ?>'><?php echo A_LANG_AVS_LABEL_FUNCTION_AVERAGE; ?></a>
				</p>				
			</fieldset>
			<br />
			
		</div>		
		<div class="funcao"><input name='ajuda' type='button' class='button' value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onClick="<?php echo HELP_FUNCTION_ADJUSTMENT; ?>" /></div>
		</td>
	</tr>
</table>
