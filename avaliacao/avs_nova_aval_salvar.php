<?php

	@session_start();	
  require_once "../config/configuracoes.php";	
  // definio do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../idioma/".$A_LANG_IDIOMA_USER."/geral.php";

  // verifica se a pessoa est logada
  if(!isset($_SESSION['logado'])) die(A_LANG_AVS_ERROR_LOGGED);
  // verifica se quem est tentando acessar a pgina  um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die(A_LANG_AVS_ERROR_VERIFY_AUTHO);

  require_once "../include/adodb/adodb.inc.php";
  require_once "questao.const.php";
  require_once "funcao/geral.func.php";
  require_once "class/avaliacao.class.php";
  require_once "class/grupo.class.php";
  require_once "class/questao.class.php";
  require_once "class/dissertativa.class.php";
  require_once "class/multipla_escolha.class.php";
  require_once "class/lacunas.class.php";
  require_once "class/verdadeiro_falso.class.php";

  // abre conexo com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

  $id_aval = isset($_REQUEST['avaliacao']) ? $_REQUEST['avaliacao'] : FALSE;
  $id_grupo = isset($_REQUEST['grupo'])? $_REQUEST['grupo'] : FALSE;
	$CodigoDisciplina = isset($_REQUEST['CodigoDisciplina']) ? $_REQUEST['CodigoDisciplina'] : FALSE;	
	$pagina_volta = isset($_REQUEST['volta']) ? $_REQUEST['volta'] : FALSE;

	if(!$id_grupo || !$CodigoDisciplina) {
		die(A_LANG_AVS_ERROR_PARAM);
	}

	/* ********** passar codigo da disciplina *** */
	// SEGURANA, NO DEIXA OUTROS PROFESSORES ACESSAREM INFORMAES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina))
	  die(A_LANG_AVS_ERROR_VERIFY_AUTHO);
		
  $aval = new Avaliacao($conn, $id_aval);
	
	$erro = FALSE;
	// salvar novas informaes
  if(isset($_POST['enviar'])) {
    $aval->set_descricao($_POST['descricao']);
    $aval->set_grupo($id_grupo);
    $aval->set_divulgacao(isset($_POST['divul']) == 1 ? 1 : 0);
		$aval->set_presencial(isset($_POST['presencial']) == 1 ? 1 : 0);
		$aval->set_randomica(isset($_POST['randomica']) == 1 ? 1 : 0);
		// salvar o peso das questes
		if(isset($_POST['q'])) {
			foreach ($_POST['q'] as $k => $value) {
				if(!empty($value))	$aval->alterar_peso_questao($k, $value);
			}
		}

    if(!$aval->salvar()) $erro = 1;
		// Grava informa�ao das avalia�oes presenciais no grupo
		if($aval->get_presencial() != FALSE) {
			$grupo = new Grupo($conn, $id_grupo);
			$grupo->set_presencial($aval->get_avaliacao());
			if(!$grupo->salvar()) $erro = 1;
		}
  }

  // deleta questo da avaliao
	if(isset($_GET['delete'])) {
	  $aval->remover_questao($_GET['delete']);
		if(!$aval->salvar()) $erro = 2;
	}
	
	// verifica para qual p�gina dever� voltar, usando a op�ao
	switch ($pagina_volta) {
		case 2 : $opcao = "NovaAvalPresencial"; break;
		default: $opcao = "NovaAval"; break;	
	}
	
	if($erro) header("Location: ../a_index.php?opcao=$opcao&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina&avaliacao=".$aval->get_avaliacao()."&grupo=$id_grupo&erro=".$erro);
	else header("Location: ../a_index.php?opcao=$opcao&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina&avaliacao=".$aval->get_avaliacao()."&grupo=$id_grupo&erro=0");

?>