
// fun��o que verifica se o que est� sendo digitado � um n�mero
function numero(e) {
  if(window.event) {
  // for IE, e.keyCode or window.event.keyCode can be used
    key = e.keyCode;
  } else if(e.which) {
  // netscape
    key = e.which;
  }
  if (( (key > 47) && (key < 58) ) || (key==8) || (key==9) || (key > 95) && ((key < 106))) return true;
  else return false
}


// atualiza as cores das divs TOTAL e RESTO
function atualiza_divs() {
  var soma = calcular_peso();
  var total = document.getElementById('total');	
	var restante = document.getElementById('resto');
  total.innerHTML = soma;
  var resto = 100 - soma;
  restante.innerHTML = resto;
  if(resto < 0) restante.style.color = 'red';
  else restante.style.color = 'black';
}

// calcula o peso dos input
function calcular_peso() {
  var inputs = document.getElementsByTagName('input');
  var soma = 0;
  for(var x=0; x<inputs.length; x++){
    if(inputs[x].type == 'text') {
      valor = parseInt(inputs[x].value);
      if(!isNaN(valor)) soma += valor;
    }
  }
  return soma;
}

// verifica se o valor est� entre 0 e 100
function verifica(obj) {
  if(parseInt(obj.value) > 100) obj.value = 100;
  else if(parseInt(obj.value) < 0) obj.value = 0;
  atualiza_divs();
}

// distribui o peso das quest�es
function distribuir_peso_igual(valor, ultimo) {
  var inputs = document.getElementsByTagName('input');
  var text = 0;
  for(var x=0; x<inputs.length; x++){
    if(inputs[x].type == 'text') {
      inputs[x].value = valor;
      text = x;
    }
  }
  if (ultimo != 0) inputs[text].value = ultimo;

  atualiza_divs();
}
