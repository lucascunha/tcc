// fun��es para verificar os pesos das avalia��es

function salvar() {
  // verifica se h� valores para cada input
  var inputs = document.getElementsByTagName('input');
  for(var x=0; x<inputs.length; x++){
    if(inputs[x].type == 'text') {
      if(inputs[x].value == null || inputs[x].value=="") {
        alert("H� avalia��es que n�o tiveram seus pesos definidos");
        inputs[x].focus();
        return false;
      }
    }
  }
  soma = calcular_peso();
  if (soma > 100) {
    alert("A soma do peso das quest�es n�o deve ultrapassar a cem porcento (100%)");
    return false;
  }
  if(soma != 100) {
    alert("O peso das quest�es deve somar cem porcento (100%)");
    return false;
  }
  return true;
}

function carrega_media(curso, disc, id){
  var ajax = new AJAX();
  var div = document.getElementById(id);
  div.innerHTML = "Carregando <img src='imagens/carrega.gif' border='0'>";
  ajax.Texto("avaliacao/ajax/ajax_media.php?curso="+curso+"&disciplina="+disc,div);
}

function esconder_info() {
  var info = document.getElementById('info');
  if (info) {
    info.style.display="none";
  }
}