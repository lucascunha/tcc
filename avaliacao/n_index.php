<?php
/**
 * Descri��o
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <09/02/2008>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */


  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/liberar.class.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/funcao/aluno.func.php";

  montar_orelha_aluno_avaliacao(AVS_NAV_AVAL);

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

  $avaliacao = listar_avaliacao_liberada($conn, $_SESSION['id_aluno']);

?>
<script language="JavaScript">
function new_window(libera,libera_aluno)
{
  janela = window.open("avaliacao/a_avaliacao.php?libera="+libera+"&libera_aluno="+libera_aluno,"_fullscreen","width=800, height=600, scrollbars=yes");
  if (janela) janela.focus();
}
</script>

<table CELLSPACING=5 CELLPADDING=5 border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?>  height="80%" style="height:380px;" >
  <tr valign='top'>
    <td>
<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">
  <tr>
    <td>
    <?php
      if(count($avaliacao) > 0) {
        // imprime o cabe�alho da tabela
        echo "<table cellspacing='1' cellpadding='1' bgcolor='#009ACD' width='80%'  border='0' >\n";
        echo "<tr class='Titulo'>\n";
        echo "<td width='60%'>".A_LANG_AVS_TABLE_USER_EVALUATION."</td>\n";
        echo "<td width='20%'>".A_LANG_AVS_TABLE_USER_DATE."</td>\n";
        echo "<td width='20%'>".A_LANG_AVS_TABLE_USER_REALIZE_EVALUATION."</td>\n";
        echo "</tr>\n";

        // imprime cada avalia��o liberada
        $classe = "zebraA";

        foreach ($avaliacao as $k)
        {
          echo "<tr class='zebraA''>\n";
          $aval = new Avaliacao($conn,$k['id_avaliacao']);
          $libera = new Liberar($conn, $k['id_libera']);
          echo "<td>".$aval->get_descricao()."</td>\n";
          echo "<td>".$libera->get_data_inicio()."</td>";
          echo "<td><a href='javascript:new_window(".$libera->get_id().",".$libera->get_identificador_aluno($_SESSION['id_usuario'],$k['id_curso'],$k['id_disc']).")'>Realizar avalia��o</a></td>";
          echo "</tr>\n";
          $classe = $classe == "zebraA" ? "zebraB" : "zebraA";

        }
        echo "</table>";
      } else {
            ?>
                <table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0" style=padding-top:11px;>
                    <tr>
                      <td>  
                             <div class="Mensagem_Amarelo">
                               <?
                               echo "<p class=\"texto1\">\n";     
                               echo "<table cellspacing=\"0\" cellpadding=\"0\" width=80% > ";
                               echo "  <tr>";
                               echo "    <td width=\"50\">";
                               echo "      <img src=\"imagens/icones/warning.png\" alt=\"\" />";
                               echo "    </td>";
                               echo "    <td valign=\"center\">";
                               
                               echo A_LANG_AVS_LABEL_USER_NOT_EVALUATION;
                               echo "    </td>";
                               echo "  </tr>";
                               echo "</table>";  
                               echo "</p>\n";      
                               ?>    
                             </div>
                             <br>
                        </td>
                      </tr>       
                 </table>  
   
            <?php        
      }
    ?>

  </td>
</tr>
</table>  
</td>
</tr>
</table>
