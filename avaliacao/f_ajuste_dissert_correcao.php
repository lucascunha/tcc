<?php
/**
 * Faz o ajuste de pesos para o calculo da m�dia
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @subpackage Media
 * @version 1.0 <23/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */
  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/questao.class.php";
  require_once "avaliacao/class/dissertativa.class.php";
  require_once "avaliacao/funcao/divulgar.func.php";
  require_once "avaliacao/class/erro.class.php";
  require_once "avaliacao/class/liberar.class.php";
  require_once "avaliacao/class/resposta.class.php";
	require_once "avaliacao/funcao/calculo_media.func.php";


	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	$id_libera = isset($_GET['libera']) ? $_GET['libera'] : FALSE;
	$libera_aluno = isset($_GET['libera_aluno']) ? $_GET['libera_aluno'] : FALSE;
	
	if(!$CodigoDisciplina || !$libera || !$libera_aluno) {
		echo A_LANG_AVS_ERROR_PARAM;
		return;
	}	

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	
	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO;
		return;
	}	

    // faz a grava��o dos dados
	
  $erro = new Erro();
	$erro_critico = new Erro();
	
	$libera = new Liberar($conn, $id_libera);
	$resposta = new Resposta($conn, $libera_aluno);
	
	if($resposta->get_quantidade_resposta() == 0) $erro_critico->adicionar_erro(A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_STUDENT);
	
	if(!empty($_POST)) {
		$x = 1;
		foreach($_POST[QUESTAO_DISSERT] as $k => $value) {
			$v = $libera->questao_identificador($k);
			// apenas modifica as quest�es que foram corrigidas
			
			$value['nota'] = empty($value['nota']) ? 0 : $value['nota'];
			
			if(!$resposta->alterar_resposta($k,$value['nota'])) $erro->adicionar_erro(A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_SAVE." ".$x);	
			$resposta->adicionar_explicacao($k, $value['explicacao']);	
			
			$x++;
	  }
		if(!$resposta->calcular_nota(TRUE)) $erro->adicionar_erro(A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT_AVERAGE);
		if(!$resposta->salvar()) $erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE);
		$aluno = $libera->aluno_libera($libera_aluno);
		if(!calcular_media($conn, $aluno['aluno'] )) $erro->adicionar_erro(A_LANG_AVS_ERROR_AVERAGE_CALCULE);
	}
		
	montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_AJUSTE);
?>
<script language="JavaScript" type="text/javascript" src="avaliacao/ajax/ajax.js"></script>
<script language="JavaScript" type="text/javascript">
function peso(objeto, valor) {
	if(parseInt(objeto.value) > valor) {
		objeto.value = valor;		
	}	
}
	
	// fun��o que verifica se o que est� sendo digitado � um n�mero
function numero(e) {
  if(window.event) {
  // for IE, e.keyCode or window.event.keyCode can be used
    key = e.keyCode;
  } else if(e.which) {
  // netscape
    key = e.which;
  }
  if (( (key > 47) && (key < 58) ) || (key==8) || (key==9) || (key > 95) && ((key < 106))) return true;
  else return false
}
	
</script>

<form action="<?php echo "a_index.php?opcao=FuncaoAjustarDissertCorrecao&CodigoDisciplina=".$CodigoDisciplina."&libera_aluno=".$libera_aluno."&libera=".$id_libera; ?>" method="post">
<table cellspacing='1' cellpadding='1' width='100%'  border='0'  bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
	<tr>
		<td>
			<div class="funcao">
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_DISSERT; ?> (3/3)</div>	
			<br />
      <?php
        // imprime o erro ou o sucesso em salvar
				if(isset($_POST['enviar'])) {
					if ($erro->quantidade_erro() == 0) {
						echo "<div class='sucesso' id='info'>\n";
						echo "<p><span>".A_LANG_AVS_SUCESS."</span></p>\n";
						echo "</div>\n";
					} else {
						echo "<div class='erro' id='info'>\n";
						echo $erro->imprimir_erro();
						echo "</div>\n";
					}
				}				

				if($erro_critico->quantidade_erro() == 0) {		
					$aluno = $libera->aluno_libera($libera_aluno);
					echo "<div class='lista_aval'>";
					echo "<div class='info'>";
					echo "<p><span>".A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT.": </span>".$aluno['nome']."</p>";
					echo "<p><span>".A_LANG_AVS_LABEL_EVALUATION.": </span>".$resposta->get_descricao()."</p>";
					echo "</div>";
					
					// imprime as respostas do tipo dissertativas					
					$tabindex = 3;
					$x = 1;
					while($questao = $resposta->buscar_questao()) {
						if($questao['tipo'] == DISSERTATIVA) {
							$questao_libera = $libera->questao_identificador($questao['questao']);							
				
							$dissertativa = new Dissertativa($conn, $questao['questao']);						
							$alter = $resposta->resposta_identificador($questao['questao']);
							echo "<div class='ident'>\n";
							echo "<p><span class='t'>".$x." - ".A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT5.": </span>".$questao_libera['peso'];
							echo "&nbsp;&nbsp;<span class='t'>".A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT6.": </span><input type='text' name='".QUESTAO_DISSERT."[".$dissertativa->get_questao()."][nota]' value='".$alter['valor']."' class='button' onkeydown='return numero(event)' onkeyup='peso(this,".$questao_libera['peso'].")' tabindex='".$tabindex."'></p>\n";		
							echo "<p><span class='t'>".$dissertativa->get_pergunta()."</span></p>";
							echo "<p><span><textarea class='button' cols='100' rows='5' readonly='readonly'>".$alter['resposta']."</textarea></p>";
							$x++;
							$tabindex++;
							// imprime a explica��o da quest�o
							echo "<p><span class='t'>".A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT7.":</span></p>";
							echo "<p><textarea class='button' cols='100'  name='".QUESTAO_DISSERT."[".$dissertativa->get_questao()."][explicacao]' rows='5' tabindex='".$tabindex."'>".$alter['explicacao']."</textarea></p>";
	
							echo "</div>\n"; 					
							$tabindex++;
						}
					}
					echo "<div class='alerta'>";
					echo "<p>".A_LANG_AVS_TEXT_ADJUSTMENT."</p>";
					echo "</div>";
					echo "</div>";							
				}	else	{
				  echo "<div class='erro'>";
					echo $erro_critico->imprimir_erro();
					echo "</div>";
				}
      ?>			
			</div>
		</td>
	</tr>
  <tr>
    <td>
			<div class="funcao">
				<input type="button" name="voltar" value="<?php echo A_LANG_AVS_BUTTON_BACK; ?>" class="button" onclick="javascript:window.location='<?php echo "a_index.php?opcao=FuncaoAjustarDissertAluno&CodigoDisciplina=".$CodigoDisciplina."&libera=".$id_libera."&libera_aluno=".$libera_aluno; ?>'" />
				<?php
				  if($erro_critico->quantidade_erro() == 0) {
						echo "<input type='submit' name='enviar' value='".A_LANG_AVS_BUTTON_SAVE."' class='button' />";
					}
				?>
			</div>
		</td>
  </tr>
</table>
</form>
