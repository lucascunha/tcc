<?php
/**
 * Adiciona uma avaliao ao grupo de avaliao
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Scripts
 * @subpackage Nova_Aval
 * @version 1.0 16/10/2007
 * @since 10/04/2008 Modificao de cores e estrutura. Adicionado o boto de ajuda
 */

/*
 * Trabalho de Concluso de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

  require_once "avaliacao/questao.const.php";
	require_once "avaliacao/ajuda.const.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/questao.class.php";
  require_once "avaliacao/class/dissertativa.class.php";
  require_once "avaliacao/class/multipla_escolha.class.php";
  require_once "avaliacao/class/lacunas.class.php";
  require_once "avaliacao/class/verdadeiro_falso.class.php";
	require_once "avaliacao/class/erro.class.php";

  $numtopico = isset($_GET['numtopico']) ? $_GET['numtopico'] : FALSE;	
	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	$id_grupo = isset($_REQUEST['grupo'])? $_REQUEST['grupo'] : FALSE;
	
	if(!$numtopico || !$CodigoDisciplina || !$id_grupo) {
		echo A_LANG_AVS_ERROR_PARAM; // em caso de erro termina o script
		return;
	}

    // abre conexo com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURANA, NO DEIXA OUTROS PROFESSORES ACESSAREM INFORMAES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO;
		return;
	}	

  $id_aval = isset($_REQUEST['avaliacao']) ? $_REQUEST['avaliacao'] : FALSE;

  $aval = new Avaliacao($conn, $id_aval);
	
	$grupo = new Grupo($conn, $id_grupo);
		
	if(isset($_GET['erro'])) {
		$erro = new Erro();
		switch ($_GET['erro']) {
			case 1: $erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE); break;
			case 2: $erro->adicionar_erro(A_LANG_AVS_ERROR_EVALUATION_DELETE); break;
		}
	}

	montar_orelha_completa_avaliacao($numtopico, $CodigoDisciplina);
	
?>

<table cellspacing='1' cellpadding='1' width='100%' border='0' bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
  <tr>
    <td>
      <form name="form1" method="post" id="form" action="<?php echo "avaliacao/avs_nova_aval_salvar.php?opcao=NovaAvalPresencial&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina&avaliacao=$id_aval&grupo=$id_grupo&volta=2"; ?>">
			<input type="hidden" value="1" name="presencial" />
			<?php
				if(isset($_GET['erro']) && $_GET['erro'] != 0) {
				  echo "<div class='erro'>\n";
					echo $erro->imprimir_erro();
					echo "</div>";
				} elseif (isset($_GET['erro']) && $_GET['erro'] == 0) {
				  echo "<div class='sucesso'>\n";
					echo "<p><span>".A_LANG_AVS_SUCESS."</span></p>\n";
					echo "</div>";				
				}
			
			?>			
      <div class="funcao">				
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_NEW_AVAL_PRES; ?></div>
				<p><span><?php echo A_LANG_AVS_LABEL_DESCRIPTION; ?>: *</span><br />
				<textarea name="descricao" style="width:400px; height:100px" class="button"><?php echo $aval->get_descricao(); ?></textarea></p>
				<?php		
					echo "<div class='aval_funcao'>\n";
					echo "  <input type='button' value='".A_LANG_AVS_BUTTON_BACK."' class='button' onClick=\"javascript:window.location='a_index.php?opcao=TopicoAvaliacao&numtopico=1&CodigoDisciplina=1'\">";
					echo "  <input type='button' class='button' value='".A_LANG_AVS_BUTTON_HELP."' onClick=\"".HELP_AVAL."\" />";
					echo "	<input name='enviar' type='submit' id='salvar' value='".A_LANG_AVS_BUTTON_SAVE."' class='button'>\n";
					echo "</div>\n";
				?>				
			</div>
	
      </form>
    </td>
  </tr>
</table>
<?php
  $conn->Close();
?>
