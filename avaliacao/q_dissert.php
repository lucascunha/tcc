<?php
/**
 * Script para inser��o/altera��o de quest�es do tipo Dissertativa
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Scripts
 * @subpackage Verdadeiro_Falso
 * @version 1.0 21/10/2007
 * @since 08/04/2008 Modifica��o de cores e estrutura no CSS. Utiliza��o da classe Erro.
 * @since 09/04/2008 Retirado o bug de duplicar quest�es na avalia��o
 * @since 09/04/2008 Retirado o bug de criar duas quest�es com IDs diferentes a cada salvar
 */

/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

  @session_start();
  require_once "../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica se a pessoa est� logada
  if(!isset($_SESSION['logado'])) die(A_LANG_AVS_ERROR_QUESTION_LOGGED);
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>");

  require_once "../include/adodb/adodb.inc.php";
	require_once "ajuda.const.php";
	require_once "questao.const.php";
  require_once "class/questao.class.php";
  require_once "class/dissertativa.class.php";
  require_once "class/avaliacao.class.php";
	require_once "class/erro.class.php";
	require_once "funcao/geral.func.php";

  // pega as variaveis externas

  // id da questao, se for alterar
  $id_questao = isset($_REQUEST['questao']) ? $_REQUEST['questao'] : NULL;
  // id da avaliacao
  $id_aval = isset($_REQUEST['aval']) ? $_REQUEST['aval'] : FALSE;
    // codigo da disciplina
  $CodigoDisciplina = isset($_REQUEST['CodigoDisciplina']) ? $_REQUEST['CodigoDisciplina'] : FALSE;

  if(!($id_aval || $CodigoDisciplina)) die(A_LANG_AVS_ERROR_PARAM);

  // abrindo conex�o com a base de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));
		
	$erro = new Erro();

  // cria um objeto da questao dissertativa
  $q = new Dissertativa($conn, $id_questao);
  // se o formulario foi respondido far� a inser��o ou modifica��o
  if(isset($_POST['enviar'])) {
    // seta as vari�veis para o objeto da quest�o
    $q->set_pergunta($_POST['pergunta']);
		$q->set_explicacao($_POST['explicacao']);
    $q->set_disciplina($CodigoDisciplina);
    
		if (!$q->salvar()) {
			$erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE);
		}	else { //@since 05/08/2008 Adicionado para sair da popup se foi inserido com sucesso
			echo "<script language='javascript' type='text/javascript'>window.close()</script>";
		}				
		
		/**
		 * @since 09/04/2008 Bug de duplicar quest�es quando salva
		 */
		$id_questao = $q->get_questao(); 

		if($erro->quantidade_erro() == 0) {
			$aval = new Avaliacao($conn, $id_aval);
			/**
			 * @since 09/04/2008 Adiciona dentro da avalia��o apenas se n�o tiver a quest�o. Evitando quest�es duplicadas
			 */
			if(!$aval->questao_identificador($q->get_questao())) {
				$aval->atribuir_questao($q->get_questao());
				if(!$aval->salvar()) {
					$erro->adicionar_erro(A_LANG_AVS_ERROR_QUESTION_EVALUATION);
				}	
			}
		}
  }
  // fecha a conex�o com a base de dados
  $conn->Close();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv=Content-Type content="text/html; charset=ISO-8859-1">
	<meta http-equiv=expires content="0">
	<meta http-equiv=Content-Language content="pt-br">
	<meta name=KEYWORDS content="Adpta��o, Ensino, Distância">
	<meta name=DESCRIPTION content="Módulo de Avalia��o Somativa">
	<meta name=ROBOTS content="INDEX,FOLLOW">
	<meta name=resource-type content="document">
	<meta name=AUTHOR content="Claudiomar Desanti">
	<meta name=COPYRIGHT content="Copyright (c) 2007 by Universidade do Estado de Santa Catarina - UDESC">
	<meta name=revisit-after content="1 days">
	<meta name=distribution content="Global">
	<meta name=GENERATOR content="Easy Eclipse For PHP">
	<meta name=rating content="General">

	<link rel="StyleSheet" href="../css/geral.css" type="text/css">
	<link rel="stylesheet" href="css/ff.css" type="text/css">


<title>Quest�o Dissertativa</title>
<script language="javascript" type="text/javascript">
  function iniciar() {
    var pergunta = document.getElementById('pergunta');
    pergunta.focus();
  }

  // define as ids que tem os campos obrigat�rios
  var obrigatorio = new Array('pergunta');

  // fun��o para validar o formulario
  function valida() {
    for(var x=0 ; x < obrigatorio.length; x++) {
      obj = document.getElementById(obrigatorio[x]);
      if(obj.value == null || obj.value == "") {
        alert("<?php echo A_LANG_AVS_JS_ALERT_QUESTION_REQUIRE ?> "+ obj.id + "<?php echo A_LANG_AVS_JS_ALERT_QUESTION_REQUIRE2 ?>");
        obj.focus();
        return false;
      }
    }
    return true;
  }

  function recarrega() {
    window.opener.location.reload();
        self.close();
  }

</script>
</head>

<body style="background-color: <?php echo $A_COR_FUNDO_ORELHA_ON ?>;" onload="iniciar()">

<div class="identificacao">
  <h1><?php echo A_LANG_AVS_LABEL_QUESTION_IDENTIFY_DISSERT; ?></h1>
</div>
<?php
		if(isset($_POST['enviar'])) {
			if ($erro->quantidade_erro() == 0) {
				echo "<div class='sucesso' id='info'>\n";
				echo "<p><span>".A_LANG_AVS_SUCESS."</span></p>\n";
				echo "</div>\n";
			} else {
				echo "<div class='erro' id='info'>\n";
				echo $erro->imprimir_erro();
				echo "</div>\n";
			}
		}
?>
<form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF']."?CodigoDisciplina=".$CodigoDisciplina."&questao=".$id_questao."&aval=".$id_aval; ?>" onSubmit="return valida()">
    <table border="0" cellspacing="1" cellpadding="2" align="center">
    <tr>
      <td scope="col"><?php echo A_LANG_AVS_LABEL_QUESTION_ENUNCIATE; ?> *</td>
		</tr>
		<tr>	
      <td scope="col"><textarea name="pergunta" cols="100" rows="10" id="pergunta" class="button"><?php echo $q->get_pergunta(); ?></textarea></td>
    </tr>
    <tr>
      <td><?php echo A_LANG_AVS_LABEL_QUESTION_EXPLANATION ?></td>
    </tr>
    <tr>
      <td><textarea name="explicacao" cols="100" rows="5" id="explicacao" class="button"><?php echo $q->get_explicacao(); ?></textarea></td>
    </tr>
		<tr><td><div class="alerta"><p><?php echo A_LANG_AVS_TEXT_FIELD_REQUIRE; ?></p></div></td></tr>
    <tr>
      <td>
      <div class="funcao">
				<input type="button" name="ajuda" value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onclick="<?php echo HELP_DISSERT ?>" class="button" >
				<input type="button" name="fechar" value="<?php echo A_LANG_AVS_BUTTON_CLOSE; ?>" class="button" onClick="recarrega()">
        <input type="submit" class="button" value="<?php echo A_LANG_AVS_BUTTON_SAVE; ?>" name="enviar">
			</div>	
      </td>
    </tr>
  </table>
</form>
<p>&nbsp;</p>
</body>
</html>