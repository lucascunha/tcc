<?php
/**
 * P�gina de indice para as fun��es de avalia��o.
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <16/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

  // pega o codigo da disciplina
  $CodigoDisciplina = isset($_REQUEST['CodigoDisciplina']) ? $_REQUEST['CodigoDisciplina'] : header("Location: a_index.php?opcao=TopicosCadastro1&CodigoDisciplina=".$CodigoDisciplina."&numtopico=1");

  LeMatriz($CodigoDisciplina);

  require_once "avaliacao/questao.const.php";		
	require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/questao.class.php";
  require_once "avaliacao/class/dissertativa.class.php";
  require_once "avaliacao/class/multipla_escolha.class.php";
  require_once "avaliacao/class/lacunas.class.php";
  require_once "avaliacao/class/verdadeiro_falso.class.php";
		
	montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_INDEX);

?>

<table cellspacing='1' cellpadding='1' width='100%'  border ='0'  bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
  <tr>
    <td> 
			<div class='funcao'>
				<span><strong>As fun&ccedil;&otilde;es de avalia&ccedil;&atilde;o est&atilde;o divididas em abas, s&atilde;o elas:</strong></span>
				<p>&nbsp;</p>	
				<ul>
					<li>M&eacute;dia: </li>
				� <li>Ajustes de Nota:</li>
				� <li>Libera&ccedil;&atilde;o das avalia&ccedil;&otilde;es</li>
				� <li>Divulga&ccedil;&atilde;o das notas:</li>
				� <li>Relat&oacute;rios:</li>
				� <li>Backup:</li>
				</ul>
			</div>		
		</td>
  </tr>
</table>