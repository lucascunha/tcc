<?php
/**
 * Lista os cursos dispon�veis para prosseguir a configura��o da m�dia do semestre
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @subpackage Media
 * @version 1.0 <23/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */
  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/questao.class.php";
  require_once "avaliacao/class/dissertativa.class.php";
  require_once "avaliacao/class/multipla_escolha.class.php";
  require_once "avaliacao/class/lacunas.class.php";
  require_once "avaliacao/class/verdadeiro_falso.class.php";
  require_once "avaliacao/funcao/media.func.php";
  require_once "avaliacao/class/erro.class.php";
	require_once "avaliacao/funcao/calculo_media.func.php";

	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	
	if(!$CodigoDisciplina) {
		echo A_LANG_AVS_ERROR_PARAM; // em caso de erro termina o script
		return;
	}	

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO;
		return;
	}	

  // faz a grava��o dos dados
  if(isset($_POST['enviar_peso'])) {
    // adiciona os pesos nas provas
    // $k = id_grupo, $value = peso
    $erro = new Erro();
    foreach($_POST['peso'] as $k => $value) {
      $g = new Grupo($conn, $k);
      $g->alterar_peso($_POST['curso'], $value);
      if(!$g->salvar()) {
        $erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE);		// salva o grupo
        break;
      }
    }
		// ajusta a m�dia conforme foi passado
		$v_aluno = media_listar_aluno($conn, $_POST['curso'], $CodigoDisciplina);
		foreach($v_aluno as $k) {
			calcular_media($conn, $k['id']);
		}
		
  }

  $erro2 = new Erro();
  $vet_curso = media_listar_curso($conn, $CodigoDisciplina);
  if (count($vet_curso) == 0) $erro2->adicionar_erro(A_LANG_AVS_ERROR_EVALUATION_AVERAGE);

  montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_MEDIA);
?>
<script language="JavaScript" type="text/javascript" src="avaliacao/ajax/ajax.js"></script>
<script language="JavaScript" type="text/javascript" src="avaliacao/js/geral.js"></script>
<script language="JavaScript" type="text/javascript" src="avaliacao/js/media.js"></script>


<table cellspacing='1' cellpadding='1' width='100%' border='0' bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
  <tr>
    <td>
			<div class="funcao">
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_AVERAGE; ?></div>
			
      <?php
        // imprime o erro ou o sucesso em salvar
        if(isset($_POST['enviar_peso'])) {
          if ($erro->quantidade_erro() == 0) {
            echo "<div class='sucesso' id='info'>\n";
            echo "<p><span>".A_LANG_AVS_SUCESS."</span></p>\n";
            echo "</div>\n";
          } else {
            echo "<div class='erro' id='info'>\n";
            echo $erro2->imprimir_erro();
            echo $erro->imprimir_erro();
            echo "</div>\n";
          }
        }
        if($erro2->quantidade_erro() == 0) {
          echo "<br>";
          echo "<label>".A_LANG_AVS_LABEL_CHOOSE_COURSE.": &nbsp;&nbsp;</label>\n";
          echo "<select name='curso' class='select' onchange='carrega_media(this.value,$CodigoDisciplina ,\"conteudo\"); esconder_info()' style='width:300px'>\n";
          if(isset($_POST['curso'])) echo "<option value='' disabled='disabled'>".A_LANG_AVS_COMBO_SELECT."</option>\n";
          else echo "<option value='' disabled='disabled' selected='selected'>".A_LANG_AVS_COMBO_SELECT."</option>\n";
          foreach($vet_curso as $k) {
            if(isset($_POST['curso']) && $_POST['curso'] == $k['id']) {
              echo "<option value='".$k['id']."' selected='selected'>".$k['nome']."</option>\n";
            } else {
              echo "<option value='".$k['id']."'>".$k['nome']."</option>\n";
            }
          }
          echo "</select>\n";
        } elseif ($erro->quantidade_erro() > 0) {
          echo "<div class='erro' id='info'>\n";
          echo $erro2->imprimir_erro();
          echo "</div>\n";
        }
      ?>
		</div>	
    </td>
  </tr>
  <tr>
    <td>
      <div id='conteudo'></div>
    </td>
  </tr>
	<tr><td><div class="funcao"><input name='ajuda' type='button' class='button' value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onclick="<?php echo HELP_FUNCTION_AVERAGE; ?>" /></div></td></tr>
  <?php
    if(isset($_POST['curso'])) {
      echo "<script>carrega_media(".$_POST['curso'].",".$CodigoDisciplina.",\"conteudo\")</script>";
    }
  ?>
</table>

