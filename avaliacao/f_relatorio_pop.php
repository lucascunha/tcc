<?php
/**
 * Script para inser��o/altera��o de quest�es do tipo Dissertativa
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Scripts
 * @subpackage Verdadeiro_Falso
 * @version 1.0 21/10/2007
 * @since 08/04/2008 Modifica��o de cores e estrutura no CSS. Utiliza��o da classe Erro.
 * @since 09/04/2008 Retirado o bug de duplicar quest�es na avalia��o
 * @since 09/04/2008 Retirado o bug de criar duas quest�es com IDs diferentes a cada salvar
 */

/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

  @session_start();
  require_once "../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica se a pessoa est� logada
  if(!isset($_SESSION['logado'])) die(A_LANG_AVS_ERROR_QUESTION_LOGGED);
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>");

  require_once "../include/adodb/adodb.inc.php";
	require_once "ajuda.const.php";
	require_once "questao.const.php";
  require_once "class/questao.class.php";
  require_once "class/dissertativa.class.php";
  require_once "class/lacunas.class.php";
	require_once "class/multipla_escolha.class.php";
	require_once "class/verdadeiro_falso.class.php";	
  require_once "class/liberar.class.php";
	require_once "class/erro.class.php";
	require_once "funcao/geral.func.php";
	require_once "funcao/relatorio.func.php";
	require_once "class/resposta.class.php";

  // pega as variaveis externas

  // id da questao, se for alterar
  $id_questao = isset($_REQUEST['questao']) ? $_REQUEST['questao'] : NULL;
	
	$id_tipo = isset($_REQUEST['tipo']) ? $_REQUEST['tipo'] : NULL;
	
	$id_avaliacao = isset($_REQUEST['libera']) ? $_REQUEST['libera'] : NULL;
	
  // codigo da disciplina
  $CodigoDisciplina = isset($_REQUEST['CodigoDisciplina']) ? $_REQUEST['CodigoDisciplina'] : FALSE;

  if(!($id_avaliacao || $CodigoDisciplina)) die(A_LANG_AVS_ERROR_PARAM);

  // abrindo conex�o com a base de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));
		
	$erro = new Erro();

	$array = relatorio_busca_questao($conn, $id_avaliacao, $id_questao);



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv=Content-Type content="text/html; charset=ISO-8859-1">
	<meta http-equiv=expires content="0">
	<meta http-equiv=Content-Language content="pt-br">
	<meta name=KEYWORDS content="Adpta��o, Ensino, Distância">
	<meta name=DESCRIPTION content="Módulo de Avalia��o Somativa">
	<meta name=ROBOTS content="INDEX,FOLLOW">
	<meta name=resource-type content="document">
	<meta name=AUTHOR content="Claudiomar Desanti">
	<meta name=COPYRIGHT content="Copyright (c) 2007 by Universidade do Estado de Santa Catarina - UDESC">
	<meta name=revisit-after content="1 days">
	<meta name=distribution content="Global">
	<meta name=GENERATOR content="Easy Eclipse For PHP">
	<meta name=rating content="General">

	<link rel="StyleSheet" href="../css/geral.css" type="text/css">
	<link rel="stylesheet" href="css/ff.css" type="text/css">


	<title>Visualiza��o da quest�o</title>
	<style>
		body{
			background-color:#FFFFFF; !important
		}	
	</style>
</head>

<body>

<?php
		if(isset($_POST['enviar'])) {
			if ($erro->quantidade_erro() == 0) {
				echo "<div class='sucesso' id='info'>\n";
				echo "<p><span>".A_LANG_AVS_SUCESS."</span></p>\n";
				echo "</div>\n";
			} else {
				echo "<div class='erro' id='info'>\n";
				echo $erro->imprimir_erro();
				echo "</div>\n";
			}
		}
?>
<table border="0" cellspacing="1" cellpadding="2" align="center" width="400px">
	<tr>
	<td>
	<?php	

	switch ($id_tipo) {
		case DISSERTATIVA: {
			$q = new Dissertativa($conn, $id_questao);				
			monta_dissertativa($q, '#');
			break;
		}
		case LACUNAS: {
			$q = new Lacunas($conn, $id_questao);
			monta_lacunas($q, '#');
			break;
		}
		case MULTIPLA_ESCOLHA: {
			$q = new Multipla_Escolha($conn, $id_questao);
			monta_multipla_escolha($q, '#');
			break;
		}
		case VERDADEIRO_FALSO: {
			$q = new Verdadeiro_Falso($conn, $id_questao);
			monta_verdadeiro_falso($q, '#');
			break;
		}
	}
	 
	echo "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";	 
	echo "<tr class='Titulo'>";
	echo "<td width='70%'>Aluno</td>";
	echo "<td>Status</td>";
	echo "</tr>";
	
	$classe = 'zebraA';
	foreach($array as $k) {
		echo "<tr class='".$classe."'>";
		echo "<td>".$k['nome_usuario']."</td>";
		switch ($k['status_questao']) {
			case CORRETA: echo "<td>".IMG_CORRETA."</td>"; break;
			case ERRADA: echo "<td>".IMG_ERRADA."</td>"; break;
			case MEIO: echo "<td>".IMG_MEIA." (".($k['qt_acerto'] * 100)."%)</td>"; break;
			default: echo "<td>&nbsp;</td>"; break;
		}
		echo "</tr>";
		$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA' ;
	}	

	echo "</table>";
	 	
	?>		
	</td>
	</tr>
	<tr>
		<td>
		<div class="funcao">
			<input type="button" name="fechar" value="<?php echo A_LANG_AVS_BUTTON_CLOSE; ?>" class="button" onClick="recarrega()">
		</div>	
		</td>
	</tr>
</table>
<p>&nbsp;</p>
</body>
</html>