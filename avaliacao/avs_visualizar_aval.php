<?php
/**
 * Monta a avalia��o para visualiza��o do professor
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @subpackage Avalia��o
 * @version 1.0 <24/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

  // SEGURAN�A, N�O RETIRE ISTO, SEN�O ALUNOS PODER�O VISUALIZAR A AVALIA��O
  @session_start();

	require_once "../config/configuracoes.php";
	require_once "../include/adodb/adodb.inc.php";
  require_once "../config/configuracoes.php";
	require_once "questao.const.php";
  require_once "funcao/geral.func.php";
  require_once "class/avaliacao.class.php";
  require_once "class/grupo.class.php";
  require_once "class/questao.class.php";
  require_once "class/dissertativa.class.php";
  require_once "class/multipla_escolha.class.php";
  require_once "class/lacunas.class.php";
  require_once "class/verdadeiro_falso.class.php";
  
  if ($A_LANG_IDIOMA_USER == "") include "../idioma/".$A_LANG_IDIOMA."/geral.php";
	else include "../idioma/".$A_LANG_IDIOMA_USER."/geral.php";

	// verifica se a pessoa est� logada
  if(!isset($_SESSION['logado'])) die(A_LANG_AVS_ERROR_LOGGED);
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>");

	// fazer a verifica��o de seguran�a.
	$avaliacao = isset($_REQUEST['aval']) ? $_REQUEST['aval'] : FALSE;
	
	$CodigoDisciplina = isset($_REQUEST['CodigoDisciplina']) ? $_REQUEST['CodigoDisciplina'] : FALSE;
	
	if (!$avaliacao || !$CodigoDisciplina) {
		die(A_LANG_AVS_ERROR_STUDENTS_LIBERATE);	
	}

  // abrindo conex�o com a base de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));

  // cria um objeto da avaliacao
	$aval = new Avaliacao($conn, $avaliacao);
	$grupo = new Grupo($conn, $aval->get_grupo());
	
	if($grupo->get_presencial() == $aval->get_avaliacao()) exit;
	
//	if($grupo->get_randomica()) $aval->randomize();	
	
	if(!empty($conceito)) {
		die(A_LANG_AVS_ERROR_STUDENTS_LIBERATE);
	}	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Visualizar Avalia&ccedil;&atilde;o</title>
<meta http-equiv=expires content="0">
<meta name=KEYWORDS content="Adpta��o, Ensino, Dist�ncia">
<meta name=DESCRIPTION content="M�dulo de Avalia��o Somativa">
<meta name=ROBOTS content="INDEX,FOLLOW">
<meta name=resource-type content="document">
<meta name=AUTHOR content="Claudiomar Desanti">
<meta name=COPYRIGHT content="Copyright (c) 2007 by Universidade do Estado de Santa Catarina - UDESC">
<meta name=revisit-after content="1 days">
<meta name=distribution content="Global">
<meta name=GENERATOR content="Easy Eclipse For PHP">
<meta name=rating content="General">

<link rel="stylesheet" href="../css/geral.css" type="text/css">
<link rel="stylesheet" href="css/ff.css" type="text/css">
<!--[if lte IE 6]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->
<style>
  body {
		margin-left:1.0em;
		margin-top:1.0em;
		background-color:#FFF; !important	
	}
</style>
</head>
<body>
<div class='identificacao'>
	<h1><?php echo A_LANG_AVS_LABEL_VIEW_EVALUATION; ?></h1>
	<p><span><?php echo A_LANG_AVS_LABEL_TOPIC.": ".buscar_nome_conceito($conteudo,$grupo->get_topico()); ?></span></p>
	<p><span><?php echo A_LANG_AVS_LABEL_EVALUATION .": ".$aval->get_descricao(); ?></span></p>
</div>
<?php

	$numeracao = 1;
	// para cada quest�o, � verificado o tipo, e instanciado um objeto e passado como referencia para montar a quest�o
	
	while ($array = $aval->buscar_questao()) {
		// questao, tipo, peso
		 switch ($array['tipo']) {
				case DISSERTATIVA: {
					$q = new Dissertativa($conn, $array['questao']);
					monta_dissertativa($q, $numeracao);
					break;
				}
				case LACUNAS: {
					$q = new Lacunas($conn, $array['questao']);
					monta_lacunas($q, $numeracao);
					break;
				}
				case MULTIPLA_ESCOLHA: {
					$q = new Multipla_Escolha($conn, $array['questao']);
					monta_multipla_escolha($q, $numeracao);
					break;
				}
				case VERDADEIRO_FALSO: {
					$q = new Verdadeiro_Falso($conn, $array['questao']);
					monta_verdadeiro_falso($q, $numeracao);
					break;
				}
		 }
		 $numeracao++;		
	}
?>
<div align="center">
	<input type="button" value="<?php echo A_LANG_AVS_BUTTON_CLOSE; ?>" class="button" onclick="javascript:window.close()">
</div>
</body>
</html>

