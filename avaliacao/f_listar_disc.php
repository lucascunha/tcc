<?php
/**
 * Lista as Disciplinas para a escolha, para continuar as fun��es (semelhante a Listar Disciplina do Estruturar T�pico
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
  * @subpackage Fun��es da Avalia��o Somativa
 * @version 1.0 <23/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

  $orelha = array();
  $orelha = array(
    array(
        "LABEL" => "Fun��es de Avalia��o",
        "LINK" => "",
        "ESTADO" =>"ON"
        )
    );

  MontaOrelha($orelha);


if  ($id_usuario == 2)
{
      
      $mensagem = A_LANG_DEMO;
      $tipo_msg = "error";
      $icone = "<img src='imagens/icones/error.png' border='0' />";
      $status_botao = "disabled";
      ?>
    <script type="text/javascript">
        showNotification({
          message: "<table border = 0><tr><td width=50 align=right valign=middle><?php echo $icone; ?></td><td valign=middle class=mensagem_shadow>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $mensagem; ?></td></tr></table>",
          type: "<? echo $tipo_msg; ?>",
          autoClose: true,
          duration: 8                                        
        });
    </script>   
    <?      
} 
?>


<table border="0"  width="100%"  bgcolor=#ffffff  height="80%" style="height:380px;" >
	<form name="disc" action="<?php echo "a_index.php?opcao=FuncaoIndex" ?>" method="post">
	<tr valign="top">
		<td>
			<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">
 			<tr>
				<td>
					<table border="0" bgcolor="#ffffff" cellspacing=10 cellpadding=0>
						<tr> <td> &nbsp; </td> </tr>
 						<tr>
							<td width="170"  align="right" valign=top >
 				  				<? echo A_LANG_ENTRANCE_TOPICS_DISCIPLINES;  ?>:   
 							</td>
 							<td width="425">  						       							

								<?php
										$conn = &ADONewConnection($A_DB_TYPE);
										$conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
			
										$sql = "SELECT DISTINCT d.id_disc AS id, d.nome_disc AS nome FROM disciplina d, curso_disc c WHERE d.id_disc = c.id_disc AND d.id_usuario=".$id_usuario." ORDER BY nome_disc";
			
										$rs = $conn->Execute($sql);
										if ($rs === false) die(A_LANG_ENTRANCE_TOPICS_MSG1);
										echo "<div class=\"styled-select-big\">";
										echo "<select name='CodigoDisciplina'  style=\"width:548px;\"> ";
										while ($array = $rs->FetchRow()) {
											echo "<option value='".$array['id']."'>".$array['nome']."</option>";
										}
										$rs->Close();
										echo "</select>";
										echo "</div>";
								?>	
							</td>
						</tr>

	   					<tr>
              				<td>&nbsp;</td>
	      					<td align=right>
								<br>
								<input type="button" class="buttonBig" onClick="history.go(-1)" value="Voltar">
								&nbsp;&nbsp;	
								<input class="buttonBig" type="submit" value="Fun��es de Avalia��o" name="B1"  <? echo $status_botao; ?>><br><br>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			</table>

		</td>
  	</tr>
</table>
</td>
</tr>  			
</form>
</table>

