<?php
/**
 * Arquivo com as constantes utilizadas pelos scripts de quest�es
 *
 * @version 1.0 <22/09/2007>
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Quest�o
 */


/**
 * @global integer representa o tipo de quest�o que ser� utilizada (usada para
 * gravar no banco de dados na tabela questao e para buscas)
 */

  define('MULTIPLA_ESCOLHA',1);
  define('VERDADEIRO_FALSO',2);
  define('LACUNAS',3);
  define('DISSERTATIVA',4);
		
	$TIPO_QUESTAO = array(MULTIPLA_ESCOLHA, VERDADEIRO_FALSO, LACUNAS, DISSERTATIVA);
	
	/**
	 * Utilizada para montar a prova
	 */
	define('QUESTAO_ME', 'me');
	define('QUESTAO_VF', 'vf');
	define('QUESTAO_DISSERT', 'dissert');
	define('QUESTAO_LAC', 'lac');
		

?>
