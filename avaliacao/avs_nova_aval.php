<?php
/**
 * Adiciona uma avaliao ao grupo de avaliao
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Scripts
 * @subpackage Nova_Aval
 * @version 1.0 16/10/2007
 * @since 10/04/2008 Modificao de cores e estrutura. Adicionado o boto de ajuda
 */

/*
 * Trabalho de Concluso de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

  require_once "avaliacao/questao.const.php";
	require_once "avaliacao/ajuda.const.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/questao.class.php";
  require_once "avaliacao/class/dissertativa.class.php";
  require_once "avaliacao/class/multipla_escolha.class.php";
  require_once "avaliacao/class/lacunas.class.php";
  require_once "avaliacao/class/verdadeiro_falso.class.php";
	require_once "avaliacao/class/erro.class.php";

  $numtopico = isset($_GET['numtopico']) ? $_GET['numtopico'] : FALSE;	
	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	$id_grupo = isset($_REQUEST['grupo'])? $_REQUEST['grupo'] : FALSE;
	
	if(!$numtopico || !$CodigoDisciplina || !$id_grupo) {
		echo A_LANG_AVS_ERROR_PARAM; // em caso de erro termina o script
		return;
	}

    // abre conexo com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURANA, NO DEIXA OUTROS PROFESSORES ACESSAREM INFORMAES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO;
		return;
	}	

  $id_aval = isset($_REQUEST['avaliacao']) ? $_REQUEST['avaliacao'] : FALSE;

  $aval = new Avaliacao($conn, $id_aval);
	
	$grupo = new Grupo($conn, $id_grupo);
		
	if(isset($_GET['erro'])) {
		$erro = new Erro();
		switch ($_GET['erro']) {
			case 1: $erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE); break;
			case 2: $erro->adicionar_erro(A_LANG_AVS_ERROR_EVALUATION_DELETE); break;
		}
	}

	montar_orelha_completa_avaliacao($numtopico, $CodigoDisciplina);
	
?>
<script type="text/javascript" language="javascript">
  function abrir(url, largura, altura) {
    janela = window.open(url, 'questao', 'width='+largura+',height='+altura+',resizable=yes,scrollbars=yes,status=yes');
    janela.focus();
  }
	

  function nova_questao() {
     var sel = document.getElementById('tipo_questao');
     if (!sel) return;
     switch (sel.value) {
       case '<?php echo DISSERTATIVA ?>': abrir("avaliacao/q_dissert.php?aval=<?php echo $aval->get_avaliacao()."&CodigoDisciplina=".$CodigoDisciplina; ?>", 600, 400);break;
       case '<?php echo LACUNAS ?>': abrir("avaliacao/q_lacuna.php?aval=<?php echo $aval->get_avaliacao()."&CodigoDisciplina=".$CodigoDisciplina; ?>", 500, 400);break;
       case '<?php echo MULTIPLA_ESCOLHA ?>': abrir("avaliacao/q_me.php?aval=<?php echo $aval->get_avaliacao()."&CodigoDisciplina=".$CodigoDisciplina; ?>", 500, 600);break;
       case '<?php echo VERDADEIRO_FALSO ?>': abrir("avaliacao/q_vf.php?aval=<?php echo $aval->get_avaliacao()."&CodigoDisciplina=".$CodigoDisciplina; ?>", 450, 400);break;
     }
  }

  function alterar_questao(id, tipo) {
    switch (tipo) {
      case '<?php echo DISSERTATIVA ?>': abrir("avaliacao/q_dissert.php?aval=<?php echo $aval->get_avaliacao()."&CodigoDisciplina=".$CodigoDisciplina; ?>&questao="+id, 600, 400);break;
      case '<?php echo LACUNAS ?>': abrir("avaliacao/q_lacuna.php?aval=<?php echo $aval->get_avaliacao()."&CodigoDisciplina=".$CodigoDisciplina; ?>&questao="+id, 500, 400);break;
      case '<?php echo MULTIPLA_ESCOLHA ?>': abrir("avaliacao/q_me.php?aval=<?php echo $aval->get_avaliacao()."&CodigoDisciplina=".$CodigoDisciplina; ?>&questao="+id, 500, 600);break;
      case '<?php echo VERDADEIRO_FALSO ?>': abrir("avaliacao/q_vf.php?aval=<?php echo $aval->get_avaliacao()."&CodigoDisciplina=".$CodigoDisciplina; ?>&questao="+id, 400, 450);break;
    }
  }


  function questao_existente() {
    var sel = document.getElementById('tipo_questao');
    if (!sel) return;
    switch (sel.value) {
	    case '<?php echo DISSERTATIVA ?>': window.location='a_index.php?opcao=QuestaoExistente&avaliacao=<?php echo $aval->get_avaliacao()."&tipo=".DISSERTATIVA."&CodigoDisciplina=".$CodigoDisciplina."&numtopico=".$numtopico."&grupo=".$id_grupo; ?>'; break;
      case '<?php echo LACUNAS ?>': window.location='a_index.php?opcao=QuestaoExistente&avaliacao=<?php echo $aval->get_avaliacao()."&tipo=".LACUNAS."&CodigoDisciplina=".$CodigoDisciplina."&numtopico=".$numtopico."&grupo=".$id_grupo; ?>'; break;
      case '<?php echo MULTIPLA_ESCOLHA ?>': window.location='a_index.php?opcao=QuestaoExistente&avaliacao=<?php echo $aval->get_avaliacao()."&tipo=".MULTIPLA_ESCOLHA."&CodigoDisciplina=".$CodigoDisciplina."&numtopico=".$numtopico."&grupo=".$id_grupo; ?>'; break;
      case '<?php echo VERDADEIRO_FALSO ?>': window.location='a_index.php?opcao=QuestaoExistente&avaliacao=<?php echo $aval->get_avaliacao()."&tipo=".VERDADEIRO_FALSO."&CodigoDisciplina=".$CodigoDisciplina."&numtopico=".$numtopico."&grupo=".$id_grupo; ?>'; break;
	  }
  }

  function salvar() {
    // verifica se h valores para cada input
    var inputs = document.getElementsByTagName('input');
    for(var x=0; x<inputs.length; x++){
      if(inputs[x].type == 'text') {
        if(inputs[x].value == null || inputs[x].value=="") {
          alert("<?php echo A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT ?>");
          inputs[x].focus();
          return false;
        }
      }
    }
    soma = calcular_peso();
    if (soma > 100) {
      alert("<?php echo A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT2; ?>");
      return false;
    }
    if(soma != 100) {
      alert("<?php echo A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT3; ?>");
      return false;
    }
    return true;
  }

  function numero(e) {
    if(window.event) {
    // for IE, e.keyCode or window.event.keyCode can be used
      key = e.keyCode;
    } else if(e.which) {
    // netscape
      key = e.which;
    }
    if (( (key > 47) && (key < 58) ) || (key==8) || (key==9) || (key > 95) && ((key < 106))) return true;
    else return false
  }

  function calcular_peso() {
    var inputs = document.getElementsByTagName('input');
    var soma = 0;
    for(var x=0; x<inputs.length; x++){
      if(inputs[x].type == 'text') {
        valor = parseInt(inputs[x].value);
        if(!isNaN(valor)) soma += valor;
      }
    }
    return soma;
  }

  function verifica(obj) {
    if(parseInt(obj.value) > 100) obj.value = 100;
    else if(parseInt(obj.value) == 0) obj.value = 1;
    atualiza_divs();
  }
	
	function atualiza_divs() {
  	var soma = calcular_peso();
    var total = document.getElementById('total');
    var restante = document.getElementById('resto');
    total.innerHTML = soma;	
    var resto = 100 - soma;
    restante.innerHTML = resto;
    if(resto < 0) restante.style.color = 'red';
    else restante.style.color = 'black';
	}

  function distribuir_peso_igual() {
  <?php
    // calculo dos valores para cada questo
    $valor = @floor(100 / $aval->numero_questao());
		$ultimo = @((100 % $aval->numero_questao()) + $valor);
    echo "var valor = $valor;\n";
		echo "var ultimo = $ultimo;\n";
    ?>
    var inputs = document.getElementsByTagName('input');
    var text = 0;
    for(var x=0; x<inputs.length; x++){
      if(inputs[x].type == 'text') {
				inputs[x].value = valor;
				text = x;
      }
    }
		if (ultimo != 0) inputs[text].value = ultimo;
    atualiza_divs();
  }

  function remover(id) {
    var confirma = confirm("<?php echo A_LANG_AVS_JS_DELETE_QUESTION_CONFIRM; ?>");
    if(confirma) {
      var f = document.getElementById('form');
			f.action = f.action+"&delete="+id;
			f.submit();
    }
  }
	
	// funo para voltar a pgina anterior, faz verificao se todos os pesos foram definidos
	function voltar() {
	  var salvo = <?php echo isset($_GET['erro']) ? 'true' : 'false' ?>;
    // verifica se todos os inputs tem seu valor preenchido
		var inputs = document.getElementsByTagName('input');
    for(var x=0; x<inputs.length; x++){
      if(inputs[x].type == 'text') {
        if(inputs[x].value == null || inputs[x].value=="") {
          alert("<?php echo A_LANG_AVS_JS_ERROR_QUESTION_WEIGHT; ?>");
          inputs[x].focus();
          return false;
        }
      }
    }
		if(!salvo) {
			var escolha = confirm("<?php echo A_LANG_AVS_JS_ALERT_QUESTION; ?>");
			if(escolha == true)	window.location='a_index.php?opcao=TopicoAvaliacao&numtopico=<?php echo $numtopico ?>&CodigoDisciplina=<?php echo $CodigoDisciplina ?>';	
			else return false;
		}
		window.location='a_index.php?opcao=TopicoAvaliacao&numtopico=<?php echo $numtopico ?>&CodigoDisciplina=<?php echo $CodigoDisciplina ?>';
	}

  function alerta() {
	  var check = document.getElementById('presencial');
		if(check.checked) alert('<?php echo A_LANG_AVS_JS_DELETE_PRESENT_CONFIRM ?>')
	}
</script>


<table cellspacing='1' cellpadding='1' width='100%' border='0' bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
  <tr>
    <td>
      <form name="form1" method="post" id="form" action="<?php echo "avaliacao/avs_nova_aval_salvar.php?opcao=NovaAval&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina&avaliacao=$id_aval&grupo=$id_grupo&volta=1"; ?>">
			<?php
				if(isset($_GET['erro']) && $_GET['erro'] != 0) {
				  echo "<div class='erro'>\n";
					echo $erro->imprimir_erro();
					echo "</div>";
				} elseif (isset($_GET['erro']) && $_GET['erro'] == 0) {
				  echo "<div class='sucesso'>\n";
					echo "<p><span>".A_LANG_AVS_SUCESS."</span></p>\n";
					echo "</div>";				
				}
			
			?>			
      <div class="funcao">				
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_NEW_AVAL; ?></div>
				<p><span><?php echo A_LANG_AVS_LABEL_DESCRIPTION; ?>: *</span><br/>
				<textarea name="descricao" style="width:400px; height:100px" class="button"><?php echo $aval->get_descricao(); ?></textarea></p>
				<?php
					echo "<p>";
					// verifica se h questes dissertativas na prova para que possa deixar o professor escolher a opo de Divulgar as notas automaticamente.
					$checked = "";
					$nao_aceita = "";
					if ($aval->get_divulgacao() == 1) $checked = "checked='checked'";
					if (!$aval->aceita_divulgacao()) $nao_aceita = "disabled='disabled'";
					$input = "<input type='checkbox' name='divul' value='1' $checked $nao_aceita /> &nbsp;".A_LANG_AVS_LABEL_DIVULGE_NOTE;
					if ($nao_aceita) $input .= " (".A_LANG_AVS_LABEL_DISSERT_QUESTION.")";
					echo $input;
					echo "</p>\n";
					
					// randomica
					echo "<p>";
					$checked = "";
					if ($aval->get_randomica() == 1) $checked = "checked='checked'";
					$input = "<input type='checkbox' name='randomica' value='1' $checked /> &nbsp;".A_LANG_AVS_LABEL_GROUP_QUESTION_RANDOM;
					echo $input;
					echo "</p>\n";					
					
					// Adiciona os botões se ainda não foi salvo a avaliação pela primeira vez	
					if (!$aval->get_avaliacao()) 
					{
            echo "<div class='aval_funcao'>\n";
            echo "  <input type='button' value='".A_LANG_AVS_BUTTON_BACK."' class='button' onClick=\"javascript:window.location='a_index.php?opcao=TopicoAvaliacao&numtopico=1&CodigoDisciplina=1'\">";
						echo "  <input type='button' class='button' value='".A_LANG_AVS_BUTTON_HELP."' onClick=\"".HELP_AVAL."\" />";
						echo "	<input name='enviar' type='submit' id='salvar' value='".A_LANG_AVS_BUTTON_SAVE."' class='button'>\n";
						echo "</div>\n";
					}	
				?>				
			</div>
				
			<div class="funcao">
        <?php
          // a parte das questões
          if($aval->numero_questao() > 0) {
						echo "<p><span><strong>".A_LANG_AVS_LABEL_QUESTION.":</strong></span></p>\n";
              // cabealho
            echo "  <table border='0' cellspacing='1' cellpadding='1' width='100%'>\n";
            echo "    <tr align='center' class='Titulo'>\n";
            echo "      <td width='290'>".A_LANG_AVS_TABLE_DESCRIPTION."</td>\n";
            echo "      <td width='105'>".A_LANG_AVS_TABLE_TYPE."</td>\n";
            echo "      <td width='100'>".A_LANG_AVS_TABLE_WEIGHT."</td>\n";
            echo "      <td width='20'>".A_LANG_AVS_TABLE_MODIFY."</td>\n";
            echo "      <td width='20'>".A_LANG_AVS_TABLE_DELETE."</td>\n";
            echo "    </tr>\n";

            // imprime as questes
            $tab_control = 2; // controle do tabindex
						$classe = 'zebraA'; // controle de cor das linhas da tabela
            while($array = $aval->buscar_questao()) {
              // verifica o tipo da questao e busca a pergunta para ser colocada como descricao
              $q = new Questao($conn, $array['questao']);
              $descricao = $q->get_pergunta();
							switch ($array['tipo']) {
                case DISSERTATIVA: {
                  $tipo = A_LANG_AVS_LABEL_QUESTION_DISSERT;
                  break;
                }
                case LACUNAS: {
                  $tipo = A_LANG_AVS_LABEL_QUESTION_LACUNAS;
                  break;
                }
                case MULTIPLA_ESCOLHA: {
                  $tipo = A_LANG_AVS_LABEL_QUESTION_ME;
                  break;
                }
                case VERDADEIRO_FALSO: {
                  $tipo = A_LANG_AVS_LABEL_QUESTION_VF;
                  break;
                }
              }
              echo "  <tr align='center' class='".$classe."'>\n";
              echo "    <td align='left'><a title='".$descricao."' href='javascript:alterar_questao(\"".$array['questao']."\",\"".$array['tipo']."\")'>".quote($descricao, 50)."</a></td>\n";
              echo "    <td>".$tipo."</td>\n";
              echo "    <td><input name='q[".$array['questao']."]' value='".$array['peso']."' tabindex='".$tab_control."' type='text' size='5' class='text' onkeydown='return numero(event)' onkeyup='verifica(this)'></td>\n";
              echo "    <td><a href='javascript:alterar_questao(\"".$array['questao']."\",\"".$array['tipo']."\")'><img border='0' src='imagens/editar.png'></a></td>";
              echo "    <td><a href='javascript:remover(\"".$array['questao']."\")'><img border='0' src='imagens/deletar.gif'></a></td>";
              echo "  </tr>\n";
              $tab_control++;
							$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA';
            }						
            echo "  <tr>\n";
            echo "    <td align='right' colspan='2'>".A_LANG_AVS_TABLE_SUM_WEIGHT."</td>\n";
            echo "    <td align='center'><div id='total'>&nbsp;</div></td>\n";
            echo "    <td colspan='2'>&nbsp;</td>\n";
            echo "  </tr>\n";
            echo "  <tr>\n";
            echo "    <td align='right' colspan='2'>".A_LANG_AVS_TABLE_REST_WEIGH."</td>\n";
            echo "    <td align='center'><div id='resto'>&nbsp;</div></td>\n";
            echo "    <td colspan='2'>&nbsp;</td>\n";
            echo "  </tr>\n";
            echo "  <tr>\n";
						echo "  <script>atualiza_divs()</script>";
            echo "    <td align='right' colspan='5'>\n";
            echo "    </td>\n";
            echo "  </tr>\n";
            echo "  </table>";
          } 
				?>
				</div>
        <?php	
					// se for avaliao presencial
					if($aval->get_avaliacao()){
						echo "  	<div class='funcao'>\n";
						echo "      <input type='button' value='".A_LANG_AVS_BUTTON_BACK."' class='button' onClick='voltar()' />";
						echo "      <input type='button' class='button' value='".A_LANG_AVS_BUTTON_HELP."' onClick=\"".HELP_AVAL."\" />";
						if($aval->numero_questao() > 0 ) {
							echo "      <input type='button' value='".A_LANG_AVS_BUTTON_WEIGHT."' class='button' name='dist_pesos' onclick='distribuir_peso_igual()' />\n";
						}	
						echo "      <input type='submit' value='".A_LANG_AVS_BUTTON_SAVE."' class='button' name='enviar' />\n";
						echo "		</div>\n";					
					}
					
					// se for avaliao normal. Presencial no  inserido questes
					if($aval->get_avaliacao() != NULL) 
					{
						echo "<div class='funcao'>";
						echo "<select id='tipo_questao'>\n";
						echo "  <option value='".DISSERTATIVA."'>".A_LANG_AVS_LABEL_QUESTION_DISSERT."</option>\n";
						echo "  <option value='".MULTIPLA_ESCOLHA."'>".A_LANG_AVS_LABEL_QUESTION_ME."</option>\n";
						echo "  <option value='".LACUNAS."'>".A_LANG_AVS_LABEL_QUESTION_LACUNAS."</option>\n";
						echo "  <option value='".VERDADEIRO_FALSO."'>".A_LANG_AVS_LABEL_QUESTION_VF."</option>\n";
						echo "</select>\n";
						echo "<input type='button' value='".A_LANG_AVS_BUTTON_QUESTION_NEW."' class='button' name='nquestao' onclick='nova_questao()'>&nbsp;&nbsp;\n";
						echo "<input type='button' value='".A_LANG_AVS_BUTTON_QUESTION_EXIST."' class='button' name='equestao' onclick='questao_existente()'>\n";
						echo "</div>\n";
          }				
				?>
      </form>
    </td>
  </tr>
</table>
<?php
  $conn->Close();
?>
