<?php
/**
 * Faz o ajuste de pesos para o calculo da m�dia
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
  * @subpackage Media
 * @version 1.0 <23/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */
  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/questao.class.php";
  require_once "avaliacao/class/dissertativa.class.php";
  require_once "avaliacao/funcao/divulgar.func.php";
  require_once "avaliacao/class/erro.class.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/liberar.class.php";
  require_once "avaliacao/class/resposta.class.php";


	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	$libera = isset($_REQUEST['libera']) ? (int) $_REQUEST['libera'] : FALSE;
	
	if(!$CodigoDisciplina && !$libera) {
		echo A_LANG_AVS_ERROR_PARAM;
		return;
	}	

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO;
		return;
	}	

  $erro = new Erro();
	
	$libera = new Liberar($conn, $libera);
		
  montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_AJUSTE);
?>

<table cellspacing='1' cellpadding='1' width='100%'  border='0'  bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
	<tr>
		<td>
			<div class="funcao" align="center">
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_DISSERT; ?> (2/3)</div>	
			<br />
      <?php
				// mostra mensagem caso n�o haja mais avalia��es com notas a serem liberadas
        if($erro->quantidade_erro() == 0) {
					echo "<div class='lista_aval'>";					
					$alunos = $libera->alunos();					
					$_UASORT_CAMPO = "curso";					
					usort($alunos, 'ordena'); //ordena o vetor pelo curso
					
					$curso_atual = NULL;				
					
					foreach($alunos as $k) {
					
						if($curso_atual != $k['curso']) {
							if(is_null($curso_atual)) {
								echo "<br />";
								echo "<div class='lista_aluno'>";
								echo "<fieldset><legend>".$k['nome_curso']."</legend>";
								echo "<table width='95%' border='0' cellspacing='0' cellpadding='0'>";
								echo "<tr class='Titulo'>";
								echo "<td width='70%'>".A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT."</td>";
								echo "<td width='15%'>".A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT1."</td>";
								echo "<td width='15%'>".A_LANG_AVS_TABLE_ACTION."</td>";
								echo "</tr>";							
							} else {
								echo "</table>";
								echo "</fieldset>";
								echo "</div><br />";
								echo "<div class='lista_aluno'>";
								echo "<fieldset><legend>".$k['curso']."</legend>";
								echo "<table width='95%' border='0' cellspacing='0' cellpadding='0'>";
								echo "<tr class='Titulo'>";
								echo "<td width='70%'>".A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT."</td>";
								echo "<td width='15%'>".A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT1."</td>";
								echo "<td width='15%'>".A_LANG_AVS_TABLE_ACTION."</td>";
								echo "</tr>";							
							}
							$classe = "zebraA";
							$curso_atual = $k['curso'];
						}		
					
						$resposta = new Resposta($conn, $k['id']);						
						echo "<tr class='".$classe."'>";
						echo "<td>".$k['nome']."</td>";
						// verifica se o aluno entregou a avalia�ao	
						switch ($k['status']) {
							case NAOENTREGUE: 
								echo "<td class='naoentregue'>".A_LANG_AVS_LABEL_AJAX_LIBERATE4."</td>\n"; 
								echo "<td class='naoentregue'>&nbsp;</td>\n"; 
								break;
							case ZERADA: 
								echo "<td class='zerado'>".A_LANG_AVS_LABEL_AJAX_LIBERATE5."</td>\n"; 
								echo "<td class='zerado'>&nbsp;</td>\n"; 
								break;
							case ENTREGUE: {
								$corrigida = $resposta->verificar_questao_corrigida();
								if(!$corrigida) {
									echo "<td><a href='a_index.php?opcao=FuncaoAjustarDissertCorrecao&CodigoDisciplina=".$CodigoDisciplina."&libera_aluno=".$k['id']."&libera=".$libera->get_id()."'>".A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT3."</a></td>";														
									echo "<td>".$resposta->quantidade_questao_pendente()."</td>";
								}	else {
									echo "<td>".A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION6."</td>";
									echo "<td><a href='a_index.php?opcao=FuncaoAjustarDissertCorrecao&CodigoDisciplina=".$CodigoDisciplina."&libera_aluno=".$k['id']."&libera=".$libera->get_id()."'>".A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT4."</a></td>";								
								}
							} break;
						}									
						echo "</tr>";						
						$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA';
					}				
					echo "</table>";
					echo "</fieldset>";
					echo "</div>";
					echo "</div>";					
			  } 
      ?>			
			</div>
		</td>
	</tr>
  <tr>
    <td>
			<div class='funcao'>
				<input type="button" name="voltar" value="<?php echo A_LANG_AVS_BUTTON_BACK; ?>" class="button" onclick="javascript:window.location='a_index.php?opcao=FuncaoAjustarDissertAval&CodigoDisciplina=<?php echo $CodigoDisciplina; ?>'">
			</div>
		</td>
  </tr>
</table>

