<?php
/**
 * Lista os cursos dispon�veis para prosseguir a configura��o da m�dia do semestre
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
  * @subpackage Media
 * @version 1.0 <23/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
	require_once "avaliacao/funcao/ajuste.func.php";
	require_once "avaliacao/class/erro.class.php";
	require_once "avaliacao/funcao/calculo_media.func.php";

	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	
	if(!$CodigoDisciplina) {
		echo A_LANG_AVS_ERROR_PARAM;
		return;
	}	

	$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO;
		return;
	}	

	// salva a m�dia
	if(isset($_POST['media'])) {
	  $erro = new Erro();
		foreach ($_POST['media'] as $k => $value) {
			if(!ajuste_media_salvar($conn, $k, $_GET['CodigoDisciplina'], $_GET['curso'], $value)) {
				$erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE);
				break;
			}
		}
	}
	
	// busca os cursos
	$vet_cursos = ajuste_media_listar_curso($conn, $CodigoDisciplina);
	$erro_vetor = new Erro();
  if (count($vet_cursos) == 0) 
		$erro_vetor->adicionar_erro(A_LANG_AVS_ERROR_EVALUATION_AVERAGE);

  montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_AJUSTE);

?>
<script language="javascript" type="text/javascript" src="avaliacao/ajax/ajax.js"></script>
<script language="javascript" type="text/javascript">
	function listar_alunos(curso) {
		var ajax = new AJAX();
		var disc = <?php echo $CodigoDisciplina; ?>;
		var div = document.getElementById("conteudo");
		div.className='lista_aluno_load';
		div.innerHTML = "Carregando <img src='imagens/carrega.gif' border='0'>";
		ajax.Texto("avaliacao/ajax/ajax_ajuste_media.php?curso="+curso+"&disciplina="+disc,div);	
	}
	
	function listar_notas(aluno,curso,id) {
		var ajax = new AJAX();
		var disc = <?php echo $CodigoDisciplina; ?>;
		var div = document.getElementById(id);
		div.innerHTML = "Carregando <img src='imagens/carrega.gif' border='0'>";
		ajax.Texto("avaliacao/ajax/ajax_ajuste_media_nota.php?curso="+curso+"&disciplina="+disc+"&aluno="+aluno,div);		
	}

  // fun��es de verificacao
  function numero(e) {
    if(window.event) {
    // for IE, e.keyCode or window.event.keyCode can be used
      key = e.keyCode;
    } else if(e.which) {
    // netscape
      key = e.which;
    }
    if (( (key > 47) && (key < 58) ) || (key==8) || (key==9) || (key > 95) && ((key < 106))) return true;
    else return false
  }

  function calcular_media(media, obj, div) {
    if(parseInt(obj.value) > 100) obj.value = 100;
    else if(parseInt(obj.value) < 0) obj.value = 0; 
    var div = document.getElementById(div);
		calculada = parseInt(media) + parseInt(obj.value);
		if (isNaN(calculada)) calculada = 0;
		div.innerHTML = calculada;
  }

  function verifica(obj) {
    if(parseInt(obj.value) > 100) obj.value = 100;
    else if(parseInt(obj.value) == 0) obj.value = 1;    
		atualiza_divs();
  }  

</script>
<table cellspacing='1' cellpadding='1' width="100%" border='0' bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
  <tr>
    <td>
		<div class="funcao">
		<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_ADJUSTMENT_AVERAGE; ?></div>
      <?php
        // imprime o erro ou o sucesso em salvar
        if(isset($_POST['media'])) {
          if ($erro->quantidade_erro() == 0) {
            echo "<div class='sucesso' id='info'>\n";
            echo "<p><span>".A_LANG_AVS_SUCESS."</span></p>\n";
            echo "</div>\n";
          } else {
            echo "<div class='erro' id='info'>\n";
            echo $erro_vetor->imprimir_erro();
            echo $erro->imprimir_erro();
            echo "</div>\n";
          }
        }
        if($erro_vetor->quantidade_erro() == 0) {
          echo "<br>";
          echo "<label>".A_LANG_AVS_LABEL_COURSE.": &nbsp;&nbsp;</label>\n";

					echo "<select id='curso' class='button' name='curso' onchange='listar_alunos(this.value)' style='width:300px;'>\n";
          if(isset($_REQUEST['curso'])) echo "<option value='' disabled='disabled' >".A_LANG_AVS_COMBO_SELECT."</option>\n";
          else echo "<option value='' disabled='disabled' selected='selected' >".A_LANG_AVS_COMBO_SELECT."</option>\n";
          // impressao das avalia��es
          foreach ($vet_cursos as $k) {
            if(isset($_REQUEST['curso']) && $_REQUEST['curso'] == $k['id']) {
							echo "<option value='".$k['id']."' selected='selected'>".$k['nome']."</option>\n";
						} else 	 {
							echo "<option value='".$k['id']."'>".$k['nome']."</option>\n";
						}
          }
      		echo "</select>\n";
        } elseif ($erro_vetor->quantidade_erro() > 0) {
          echo "<div class='erro' id='info'>\n";
          echo $erro_vetor->imprimir_erro();
          echo "</div>\n";
        }
      ?>			
		</div>
    </td>
  </tr>
  <tr><td><div id="conteudo" align="center"></div></td></tr>
  <?php
    if(isset($_REQUEST['curso'])) {
      echo "<script>listar_alunos(".$_REQUEST['curso'].")</script>";
    }
  ?>
	<tr>
		<td>
		<div class="funcao">
		<input type="button" name="voltar" value="<?php echo A_LANG_AVS_BUTTON_BACK; ?>" class="button" onclick="javascript:window.location='a_index.php?opcao=FuncaoAjuste&CodigoDisciplina=<?php echo $CodigoDisciplina; ?>'">
		<input name='ajuda' type='button' class='button' value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onclick="<?php echo HELP_FUNCTION_ADJUSTMENT_AVERAGE; ?>" />
		</div>
		</td>
	</tr>
</table>