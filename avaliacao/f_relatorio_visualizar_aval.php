<?php
/**
 * Monta a avalia��o para visualiza��o do aluno
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @subpackage Avalia��o
 * @version 1.0 <24/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 */

  // SEGURAN�A, N�O RETIRE ISTO, SEN�O ALUNOS PODER�O VISUALIZAR A AVALIA��O
  @session_start();

	require_once "../config/configuracoes.php";
	// Troca de idioma
  if ($A_LANG_IDIOMA_USER == "") include "../idioma/".$A_LANG_IDIOMA."/geral.php";
	else include "../idioma/".$A_LANG_IDIOMA_USER."/geral.php";

	require_once "../include/adodb/adodb.inc.php";
	require_once "funcao/aluno.func.php";
	require_once "questao.const.php";
  require_once "funcao/geral.func.php";
  require_once "class/avaliacao.class.php";
  require_once "class/grupo.class.php";
  require_once "class/questao.class.php";
  require_once "class/dissertativa.class.php";
  require_once "class/multipla_escolha.class.php";
  require_once "class/lacunas.class.php";
  require_once "class/verdadeiro_falso.class.php";
	require_once "class/liberar.class.php";
	require_once "class/resposta.class.php";

		
  // verifica se a pessoa est� logada
  if(!isset($_SESSION['logado'])) die(A_LANG_AVS_ERROR_LOGGED);

	// fazer a verifica��o de seguran�a.
	$libera_aluno = isset($_GET['libera_aluno']) ? $_GET['libera_aluno'] : die(A_LANG_AVS_ERROR_STUDENTS_LIBERATE);
	
  // abrindo conex�o com a base de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);	
	
	$resposta = new Resposta($conn, $libera_aluno);
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Visualizar Avalia&ccedil;&atilde;o</title>
<meta http-equiv=expires content="0">
<meta name=keywords content="Adpta��o, Ensino, Dist�ncia">
<meta name=description content="M�dulo de Avalia��o Somativa">
<meta name=robots content="INDEX,FOLLOW">
<meta name=resource-type content="document">
<meta name=author content="Claudiomar Desanti">
<meta name=copyright content="Copyright (c) 2007 by Universidade do Estado de Santa Catarina - UDESC">
<meta name=revisit-after content="1 days">
<meta name=distribution content="Global">
<meta name=generator content="Easy Eclipse For PHP">
<meta name=rating content="General">

<link rel="stylesheet" href="../css/geral.css" type="text/css">
<link rel="stylesheet" href="css/ff.css" type="text/css">
<!--[if lte IE 6]>
	<link href="css/ie.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->
<style>
  body {
		margin-left:1.0em;
		margin-top:1.0em;
		background-color:#FFF; !important	
	}
</style>
</head>
<body>
<div class='identificacao'>	
	<p><span><?php echo A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT; ?>: <?php $aluno = $resposta->aluno_identificador($libera_aluno); echo $aluno['nome'] ?></span></p>
	<p><span><?php echo A_LANG_AVS_LABEL_EVALUATION; ?>: <?php echo $resposta->get_descricao(); ?></span></p>
</div>
<?php

	$numeracao = 1;
	// para cada quest�o, � verificado o tipo, e instanciado um objeto e passado como referencia para montar a quest�o
	$questao = $resposta->questao_vetor();
	$_UASORT_CAMPO = "tipo";	
	usort($questao, 'ordena'); //ordena o vetor pelo tipo
	
	foreach($questao as $array) {
		// questao, tipo, peso
		 switch ($array['tipo']) {
				case DISSERTATIVA: {
					$q = new Dissertativa($conn, $array['questao']);
					monta_dissertativa_aluno($q, $resposta ,$numeracao);
					break;
				}
				case LACUNAS: {
					$q = new Lacunas($conn, $array['questao']);
					monta_lacunas_aluno($q, $resposta, $numeracao);
					break;
				}
				case MULTIPLA_ESCOLHA: {
					$q = new Multipla_Escolha($conn, $array['questao']);
					monta_multipla_escolha_aluno($q, $resposta, $numeracao);
					break;
				}
				case VERDADEIRO_FALSO: {
					$q = new Verdadeiro_Falso($conn, $array['questao']);
					monta_verdadeiro_falso_aluno($q, $resposta, $numeracao);
					break;
				}
		 }
		 $numeracao++;		
	}
?>
<div align="center">
	<input name="enviar" type="button" value="<?php echo A_LANG_AVS_BUTTON_CLOSE;?>" class="button" onClick="javascript:window.close()">
</div>
</body>
</html>

