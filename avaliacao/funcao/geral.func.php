<?php
/**
 * Fun��es utilizadas para a implementa��o da Avalia��o Somativa
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <16/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 */

/**
 * Arquivo de fun��es destinadas a opera��es de ajuste de notas
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Funcao
 * @subpackage Ajuste
 * @filesource
 * @version 1.0
 */

/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

/**
 * Fun��o que retorna os cursos para ajustar a m�dia
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $disciplina ID da disciplina
 * @return array Contem o id e nome do curso array('id','nome')
 */


define('DIGITO_MAXIMO', 30);
define('IMG_CORRETA', "<img src='../imagens/carrega.gif'>");
define('IMG_ERRADA', "<img src='../imagens/deletar.gif'>");
define('IMG_MEIA', "<img src='../imagens/deletar.gif'>");

/**
 * Fun��o para montar a orelha da Avalia��o no Ambiente de Autoria
 */
function orelha_avaliacao($vet_orelha, $numtopico, $CodigoDisciplina, $estado = "OFF") {
	if( Pai($numtopico) == ""  ) {
		$aval = array (
				"LABEL" =>A_LANG_TOPIC_EVALUATION,
				"LINK" => "a_index.php?opcao=TopicoAvaliacao&numtopico=".$numtopico."&CodigoDisciplina=".$CodigoDisciplina,
				"ESTADO" =>$estado
				);
		array_push($vet_orelha, $aval);
	}
	return $vet_orelha;
}

function montar_orelha_completa_avaliacao($numtopico, $CodigoDisciplina) {

	// **************************************************************************
	// *  CRIA MATRIZ ORELHA                                                    *
	// **************************************************************************

	$orelha = array();
	$orelha = array(
			array(
					"LABEL" => A_LANG_TOPIC." ".$numtopico,
						"LINK" => "a_index.php?opcao=TopicosAltera&numtopico=".$numtopico."&CodigoDisciplina=".$CodigoDisciplina,
						"ESTADO" =>"OFF"
					),
			array(
					"LABEL" => A_LANG_TOPIC_EXEMPLES,
						"LINK" => "a_index.php?opcao=TopicosExemplos&numtopico=".$numtopico."&CodigoDisciplina=".$CodigoDisciplina,
						"ESTADO" =>"OFF"
					),
			array(
					"LABEL" => A_LANG_TOPIC_EXERCISES,
					"LINK" => "a_index.php?opcao=TopicosExercicios&numtopico=".$numtopico."&CodigoDisciplina=".$CodigoDisciplina,
						"ESTADO" =>"OFF"
					),
			array(
					"LABEL" => A_LANG_TOPIC_COMPLEMENTARY,
						"LINK" => "a_index.php?opcao=TopicosComplementar&numtopico=".$numtopico."&CodigoDisciplina=".$CodigoDisciplina,
						"ESTADO" =>"OFF"
					),
			array(
					"LABEL" => A_LANG_TOPIC_EVALUATION,
					"LINK" => "a_index.php?opcao=TopicosComplementar&numtopico=".$numtopico."&CodigoDisciplina=".$CodigoDisciplina,
					"ESTADO" =>"ON"
					),

				 );

		MontaOrelha($orelha);

}

define('AVS_FUNC_INDEX', 0);
define('AVS_FUNC_MEDIA', 1);
define('AVS_FUNC_LIBERAR', 2);
define('AVS_FUNC_AJUSTE', 3);
define('AVS_FUNC_DIVULGAR', 4);
define('AVS_FUNC_RELATORIO', 5);
define('AVS_FUNC_BACKUP', 6);

function montar_orelha_funcao_avaliacao($CodigoDisciplina, $on) {

	// **************************************************************************
	// *  CRIA MATRIZ ORELHA                                                    *
	// **************************************************************************

	$orelha = array();
	$orelha[0] = array(
						"LABEL" =>A_LANG_AVS_FUNCTION_INTRO,
						"LINK" => "a_index.php?opcao=FuncaoIndex&CodigoDisciplina=".$CodigoDisciplina,
						"ESTADO" =>"OFF"
						);
	$orelha[1] = array(
						"LABEL" =>A_LANG_AVS_FUNCTION_AVERAGE,
						"LINK" => "a_index.php?opcao=FuncaoMedia&CodigoDisciplina=".$CodigoDisciplina,
						"ESTADO" =>"OFF"
						);
	$orelha[2] = array(
						"LABEL" => A_LANG_AVS_FUNCTION_LIBERATE,
						"LINK" => "a_index.php?opcao=FuncaoLiberar&CodigoDisciplina=".$CodigoDisciplina,
						"ESTADO" =>"OFF"
						);
	$orelha[3] = array(
						"LABEL" => A_LANG_AVS_FUNCTION_ADJUSTMENT,
						"LINK" => "a_index.php?opcao=FuncaoAjuste&CodigoDisciplina=".$CodigoDisciplina,
						"ESTADO" =>"OFF"
						);						
	$orelha[4] = array(
						"LABEL" => A_LANG_AVS_FUNCTION_DIVULGE,
						"LINK" => "a_index.php?opcao=FuncaoDivulgar&CodigoDisciplina=".$CodigoDisciplina,
						"ESTADO" =>"OFF"
						);

	$orelha[5] = array(
						"LABEL" => A_LANG_AVS_FUNCTION_REPORT,
						"LINK" => "a_index.php?opcao=FuncaoRelatorio&CodigoDisciplina=".$CodigoDisciplina,
						"ESTADO" =>"OFF"
						);
	$orelha[6] = array(
						"LABEL" => A_LANG_AVS_FUNCTION_BACKUP,
						"LINK" => "a_index.php?opcao=FuncaoBackup&CodigoDisciplina=".$CodigoDisciplina,
						"ESTADO" =>"OFF"
						);
	// seta o estado para on;
			$orelha[$on]["ESTADO"] = "ON";

	MontaOrelha($orelha);

}


define('AVS_NAV_AVAL', 0);
define('AVS_NAV_MURAL', 1);


function montar_orelha_aluno_avaliacao($on) {

	// **************************************************************************
	// *  CRIA MATRIZ ORELHA                                                    *
	// **************************************************************************

	$orelha = array();
	$orelha[0] = array(
						"LABEL" =>A_LANG_AVS_STUDENTS_TOPIC_EVALUATION,
						"LINK" => "n_index_navegacao.php?opcao=Avaliacao",
						"ESTADO" =>"OFF"
						);
	$orelha[1] = array(
						"LABEL" =>A_LANG_AVS_STUDENTS_TOPIC_RESULTS,
						"LINK" => "n_index_navegacao.php?opcao=Mural",
						"ESTADO" =>"OFF"
						);
	// seta o estado para on;
			$orelha[$on]["ESTADO"] = "ON";

	MontaOrelha($orelha);

}
// -------- FUN��ES DE SEGURAN�A PARA O PROFESSOR --------------------------- //
function verifica_professor_disciplina($obj_banco, $usuario, $disciplina) {

  $id_disc = (integer) $disciplina;
	$id_usuario = (integer) $usuario;

  $query = "SELECT id_usuario FROM disciplina WHERE id_disc = ".$id_disc." AND id_usuario = ".$id_usuario;
	
  $rs = $obj_banco->Execute($query);
	if($rs->NumRows() > 0) return TRUE;
	else return FALSE;

}

// -------- FUN��ES PARA MONTAR AS QUEST�ES (HTML) PROVA -------------------- //
/**
 * Monta a quest�o dissertativa para a resposta do aluno
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @version 1.0 <24/10/2007>
 * @param object $obj Passado o objeto da quest�o dissertativa
 * @return bool False em caso de n�o encontrar a quest�o
 */
function monta_dissertativa($obj, $numero) {
  if (!is_a($obj,'Dissertativa')) {
    echo A_LANG_AVS_ERROR_QUESTION_OBJECT;
    return FALSE;
  }

  echo "<div class='ident'>\n";
  echo "<p><span class='numeracao'>".$numero.")</span> <span class='titulo'>".$obj->get_pergunta()."</span></p>";
  echo "<p><span><textarea class='button' name='".QUESTAO_DISSERT."[".$obj->get_questao()."]' cols='100' rows='5'></textarea></p>";
  echo "</div>\n";
}

/**
 * Monta a quest�o M�ltipla-Escolha para a resposta do aluno
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @version 1.0 <24/10/2007>
 * @param object $obj Passado o objeto da quest�o dissertativa
 * @return bool False em caso de n�o encontrar a quest�o
 *
 */

function monta_multipla_escolha($obj, $numero) {
  if (!is_a($obj,'Multipla_Escolha')) {
    echo A_LANG_AVS_ERROR_QUESTION_OBJECT;
    return FALSE;
  }

  echo "<div class='ident'>\n";
  echo "<p><span class='numeracao'>".$numero.")</span> <span class='titulo'>".$obj->get_pergunta()."</span></p>";
  while ($array = $obj->buscar_alternativa()) {
    echo "<p><span><input name='".QUESTAO_ME."[".$obj->get_questao()."]' type='radio' value='".$array['id_alternativa']."'>".$array['descricao']."</span></p>";
  }
  echo "</div>\n";
}

/**
 * Monta a quest�o Verdadeiro/Falso para a resposta do aluno
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @version 1.0 <24/10/2007>
 * @param object $obj Passado o objeto da quest�o dissertativa
 * @return bool False em caso de n�o encontrar a quest�o
 *
 */

function monta_verdadeiro_falso($obj, $numero) {
  if (!is_a($obj,'Verdadeiro_Falso')) {
    echo A_LANG_AVS_ERROR_QUESTION_OBJECT;
    return FALSE;
  }

  echo "<div class='ident'>\n";
  echo "<p><span class='numeracao'>".$numero.")</span> <span class='titulo'>".$obj->get_pergunta()."</span></p>";
  echo "<p><span><input name='".QUESTAO_VF."[".$obj->get_questao()."]' type='radio' value='v'>".$obj->get_label_verdade()."</span></p>";
  echo "<p><span><input name='".QUESTAO_VF."[".$obj->get_questao()."]' type='radio' value='f'>".$obj->get_label_falso()."</span></p>";
  echo "</div>\n";

}

/**
 * Monta a quest�o de Preenchimento de Lacunas para a resposta do aluno
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @version 1.0 <24/10/2007>
 * @param object $obj Passado o objeto da quest�o dissertativa
 * @return bool False em caso de n�o encontrar a quest�o
 *
 */

function monta_lacunas($obj, $numero) {
  if (!is_a($obj,'Lacunas')) {
    echo A_LANG_AVS_ERROR_QUESTION_OBJECT;
    return FALSE;
  }

  echo "<div class='ident'>\n";
  echo "<p><span class='numeracao'>".$numero.")</span> <span class='titulo'>".$obj->get_pergunta()."</span></p>";

  while ($array = $obj->buscar_lacuna()) {
    echo "<p><span>".$array['pergunta']."&nbsp;<input type='text' name='".QUESTAO_LAC."[".$obj->get_questao()."][".$array['id_lacuna']."]' class='text'>&nbsp;".$array['complemento']."</span></p>";
  }
  echo "</div>\n";
}

// ---------------------- MONTA AS RESPOSTAS PARA OS ALUNOS ----------------------- //
/**
 * Monta a quest�o dissertativa para a resposta do aluno
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @version 1.0 <24/10/2007>
 * @param object $obj Passado o objeto da quest�o dissertativa
 * @return bool False em caso de n�o encontrar a quest�o
 */
function monta_dissertativa_aluno($obj, $resp, $numero) {
  if (!is_a($obj,'Dissertativa')) {
    echo A_LANG_AVS_ERROR_QUESTION_OBJECT;
    return FALSE;
  }
	if(!is_a($resp, 'Resposta')) {
    echo A_LANG_AVS_ERROR_ANSWER_OBJECT;
    return FALSE;	
	}

	$alter = $resp->resposta_identificador($obj->get_questao());
	$questao = $resp->questao_identificador($obj->get_questao());

  echo "<div class='ident'>\n";
	echo "<p><span class='titulo'>".A_LANG_AVS_LABEL_STUDENT_QUESTION_VALUE.": </span>".formata_nota($alter['valor'])."  <span class='titulo'>".A_LANG_AVS_LABEL_STUDENT_QUESTION_WEIGHT.": </span>".formata_nota($questao['peso'])."</p>\n";		
  echo "<p><span class='numeracao'>".$numero.")</span> <span class='titulo'>".$obj->get_pergunta()."</span></p>";
  echo "<p><span><textarea class='button' cols='100' rows='5' readonly='readonly'>".$alter['resposta']."</textarea></p>";
  
	// imprime a explica��o da quest�o
	$explicacao = $alter['explicacao'];
	if(!empty($explicacao)) {
		echo "<p><span class='titulo'>".A_LANG_AVS_LABEL_QUESTION_EXPLANATION."</span></p>";
		echo "<p><code>".$explicacao."</code></p>";
	}
	echo "</div>\n";
}

/**
 * Monta a quest�o M�ltipla-Escolha para a resposta do aluno
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @version 1.0 <24/10/2007>
 * @param object $obj Passado o objeto da quest�o dissertativa
 * @return bool False em caso de n�o encontrar a quest�o
 */

function monta_multipla_escolha_aluno($obj, $resp, $numero) {
  if (!is_a($obj,'Multipla_Escolha')) {
    echo A_LANG_AVS_ERROR_QUESTION_OBJECT;
    return FALSE;
  }
	if(!is_a($resp, 'Resposta')) {
    echo A_LANG_AVS_ERROR_ANSWER_OBJECT;
    return FALSE;	
	}

	$alter = $resp->resposta_identificador($obj->get_questao());
	$questao = $resp->questao_identificador($obj->get_questao());	

	// imprime uma imagem se tiver a quest�o correta ou incorreta
	if($alter['status'] == CORRETA) $img = IMG_CORRETA;
	else $img = IMG_ERRADA;

  echo "<div class='ident'>\n";  
	echo "<p><span class='titulo'>".A_LANG_AVS_LABEL_STUDENT_QUESTION_VALUE.": </span>".formata_nota($alter['valor'])."  <span class='titulo'>".A_LANG_AVS_LABEL_STUDENT_QUESTION_WEIGHT.": </span>".formata_nota($questao['peso'])."</p>\n";		
	echo "<p><span class='numeracao'>".$numero.")</span> <span class='titulo'>".$obj->get_pergunta()."</span>  ".$img."</p>";
  // imprime as alternativas
	while ($array = $obj->buscar_alternativa()) {
    // alternativa correta e com acerto
		if($alter['resposta'] == $array['id_alternativa'] && $alter['status'] == CORRETA) {		
			echo "<p><span><input type='radio' value='".$array['id_alternativa']."'>".$array['descricao']."</span>  ".IMG_CORRETA."</p>";
		} elseif ($alter['status'] == ERRADA && $alter['resposta'] == $array['id_resposta']) {
		// alternativa correta sem acerto
			echo "<p><span><input type='radio' value='".$array['id_alternativa']."'>".$array['descricao']."</span>  ".IMG_ERRADA."</p>";
		} else {
		// outras alternativas
			echo "<p><span><input type='radio' value='".$array['id_alternativa']."'>".$array['descricao']."</span></p>";
		}	
  }
	// imprime a explica��o da quest�o
	$explicacao = $obj->get_explicacao();
	if(!empty($explicacao)) {
		echo "<p><span class='titulo'>".A_LANG_AVS_LABEL_QUESTION_EXPLANATION."</span></p>";
		echo "<p><code>".$obj->get_explicacao()."</code></p>";
	}
  echo "</div>\n";
}

/**
 * Monta a quest�o Verdadeiro/Falso para a resposta do aluno
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @version 1.0 <24/10/2007>
 * @param object $obj Passado o objeto da quest�o dissertativa
 * @return bool False em caso de n�o encontrar a quest�o
 *
 */

function monta_verdadeiro_falso_aluno($obj, $resp, $numero) {
  if (!is_a($obj,'Verdadeiro_Falso')) {
    echo A_LANG_AVS_ERROR_QUESTION_OBJECT;
    return FALSE;
  }
	if(!is_a($resp, 'Resposta')) {
    echo A_LANG_AVS_ERROR_ANSWER_OBJECT;
    return FALSE;	
	}

	$alter = $resp->resposta_identificador($obj->get_questao());
	$questao = $resp->questao_identificador($obj->get_questao());
		
	if($alter['status'] == CORRETA) $img = IMG_CORRETA;
	else $img = IMG_ERRADA;

  echo "<div class='ident'>\n";
	echo "<p><span class='titulo'>".A_LANG_AVS_LABEL_STUDENT_QUESTION_VALUE.": </span>".formata_nota($alter['valor'])."  <span class='titulo'>".A_LANG_AVS_LABEL_STUDENT_QUESTION_WEIGHT.": </span>".formata_nota($questao['peso'])."</p>\n";		
  echo "<p><span class='numeracao'>".$numero.")</span> <span class='titulo'>".$obj->get_pergunta()."</span>  ".$img."</p>";
  echo "<p><span><input type='radio' value='v'>".$obj->get_label_verdade()."</span></p>";
  echo "<p><span><input type='radio' value='f'>".$obj->get_label_falso()."</span></p>";

	// imprime a explica��o da quest�o
	$explicacao = $obj->get_explicacao();
	if(!empty($explicacao)) {
		echo "<p><span class='titulo'>".A_LANG_AVS_LABEL_QUESTION_EXPLANATION."</span></p>";
		echo "<p><code>".$obj->get_explicacao()."</code></p>";
	}
  echo "</div>\n";
}

/**
 * Monta a quest�o de Preenchimento de Lacunas para a resposta do aluno
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @version 1.0 <24/10/2007>
 * @param object $obj Passado o objeto da quest�o dissertativa
 * @return bool False em caso de n�o encontrar a quest�o
 *
 */
function monta_lacunas_aluno($obj, $resp, $numero) {
  if (!is_a($obj,'Lacunas')) {
    echo A_LANG_AVS_ERROR_QUESTION_OBJECT;
    return FALSE;
  }
	if(!is_a($resp, 'Resposta')) {
    echo A_LANG_AVS_ERROR_ANSWER_OBJECT;
    return FALSE;	
	}

	$alter = $resp->resposta_identificador($obj->get_questao());
	$questao = $resp->questao_identificador($obj->get_questao());

  echo "<div class='ident'>\n";
	echo "<p><span class='titulo'>".A_LANG_AVS_LABEL_STUDENT_QUESTION_VALUE.": </span>".formata_nota($alter['valor'])."  <span class='titulo'>".A_LANG_AVS_LABEL_STUDENT_QUESTION_WEIGHT.": </span>".formata_nota($questao['peso'])."</p>\n";		
  echo "<p><span class='numeracao'>".$numero.")</span> <span class='titulo'>".$resp->get_pergunta()."</span></p>";

  $resposta = unserialize($alter['resposta']); // desserializa a resposta
	$x = 0;
  while ($array = $obj->buscar_lacuna()) {
		if($array['resposta'] === $resposta[$x]) $img = IMG_CORRETA;
		else $img = IMG_ERRADA;
		echo "<p><span>".$array['pergunta']."&nbsp;<input type='text' class='text' value='".$array['resposta']."'>&nbsp;".$array['complemento']."</span><br />";
		echo "&nbsp;&nbsp;&nbsp;&nbsp;<span> ".A_LANG_AVS_LABEL_STUDENT_QUESTION_CORRECT.": </span>".$resposta[$x]."&nbsp;&nbsp;".$img."</p>\n";
		$x++;
  }

	// imprime a explica��o da quest�o
	$explicacao = $obj->get_explicacao();
	if(!empty($explicacao)) {
		echo "<p><span class='titulo'>".A_LANG_AVS_LABEL_QUESTION_EXPLANATION."</span></p>";
		echo "<p><code>".$obj->get_explicacao()."</code></p>";
	}
  echo "</div>\n";
}

/**
 * Formata a nota para base decimal
 */
function formata_nota($valor) {
  return number_format($valor/10, 2,",",".");
}


/**
 * Fun��o para modificar o formato da data
 */

function formata_data($data)
{
  return strftime(A_LANG_AVS_FORMAT_DATE,strtotime($data));
}


/**
 * Fun��o para modificar o formato da data
 */

function formata_data_hora($data_hora)
{
  return strftime(A_LANG_AVS_FORMAT_DATE_TIME,strtotime($data_hora));
}

/**
 * Pega o nome do conceito conforme o numero do t�pico passado
 */
function buscar_nome_conceito($conteudo,$topico)
{
  for($x = 0; $x < count($conteudo); $x++)
  {
    if ($conteudo[$x]['NUMTOP'] == $topico)
      return $conteudo[$x]['DESCTOP'];
  }
}

function checa_login($obj)
{
  global $A_DB_TYPE;
  if (!is_a($obj,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

//	@session_start();
  // se o usuario estiver logado
  if ($_SESSION['logado'])
  {
    // procura na tabela usuario a ultima sess�o gravada, com a atual
    $query = "SELECT * FROM usuario WHERE id_usuario = '".$_SESSION['id_usuario']."' ".
             "AND sessao_usuario = '".session_id()."' AND ip_usuario = '".$_SERVER['REMOTE_ADDR']."'";
    $resultado = $obj->Execute($query);

    // se a sess�o for igual faz atualiza��o na base
    if ($array = $obj->FetchArray($resultado))
    {
      $query = "UPDATE usuario SET sessao_usuario = '".session_id()."', ".
               "ip_usuario = '".$_SERVER['REMOTE_ADDR']."' ".
               "WHERE id_usuario = ".$_SESSION['id_usuario'];
      $sql = $obj->Execute($query);
    } else {
    // sen�o zera todas as vari�veis
      $_SESSION['logado'] = false;
      $_SESSION['id_usuario'] = "";
      return FALSE;
    }
  }
  return TRUE;
}


function quote($campo, $qtde_digito = DIGITO_MAXIMO){
	if(strlen($campo) <= $qtde_digito){
		return $campo;
	}
	$retorno = substr($campo, 0, $qtde_digito - 3);
	$retorno .= " ...";
	return $retorno;
}

$_UASORT_CAMPO = "";

// fun��o utilizada pelo uasort para ordena��o de vetores
function ordena($x, $y) {
	global $_UASORT_CAMPO;
	if (empty($_UASORT_CAMPO)) return -1;
	
	if ($x[$_UASORT_CAMPO] == $y[$_UASORT_CAMPO]) {
		return 0;
	}	
	elseif ($x[$_UASORT_CAMPO] < $y[$_UASORT_CAMPO])
		return 1;
	else
		return -1;
}


?>