<?php
/**
 * Descri��o
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <04/03/2008>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */


/**
 * Arquivo de fun��es destinadas a opera��es de ajuste de notas
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Funcao
 * @subpackage Ajuste
 * @filesource
 * @version 1.0
 */

/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

/**
 * Fun��o que retorna os cursos para ajustar a m�dia
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $disciplina ID da disciplina
 * @return array Contem o id e nome do curso array('id','nome')
 */
 
 
function divulgar_lista_aluno ($obj_banco, $libera) {

  global $A_DB_TYPE;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);
		
	$id_libera = (int) $libera;

  $query = "SELECT id_avaliacao AS avaliacao, id_libera_aluno AS id, id_usuario AS aluno ".
           "FROM liberar_aval INNER JOIN liberar_aluno USING (id_libera) ".
           "WHERE id_libera = ".$id_libera;

  $rs = $obj_banco->Execute($query);

  $retorno = array();

  while($vet = $rs->FetchRow()) {
    array_push($retorno, $vet);
  }

  return $retorno;
}

/**
 *
 * @since 10/04/2008 Modificado o SQL para mostrar apenas avalia��es com alunos que realizaram de fato a avalia��o
 */
function divulgar_lista_avaliacao($obj_banco, $disciplina) {

  global $A_DB_TYPE;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);
		
	$id_disciplina = (int) $disciplina;

  $query = "SELECT DISTINCT id_libera AS libera, ds_avaliacao AS descricao, topico AS topico, dt_liberar AS data, bl_presencial AS presencial" .
           " FROM liberar_aval INNER JOIN liberar_aluno USING (id_libera) INNER JOIN grupo_aval USING(id_grupo_aval)".
           " WHERE (divulgacao_nota = 0 OR divulgacao_nota IS NULL) AND id_disc = ".$id_disciplina.
					 " AND status_libera = 0";

	$rs = $obj_banco->Execute($query);

  $retorno = array();

  while($vet = $rs->FetchRow()) {
    array_push($retorno, $vet);
  }

  return $retorno;


}


?>
