<?php
/**
 * Arquivo de fun��es utilizadas no ambiente do aluno
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Funcao
 * @subpackage Aluno
 * @filesource
 * @version 1.0 09/02/2008
 */

/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

/**
 * Fun��o para listar todas as avalia��es liberadas para resolu��o do aluno
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $aluno ID da aluno
 * @return array com todas as informa��es do aluno e da avalia��o liberada
 */
function listar_avaliacao_liberada($obj_banco, $aluno)
{
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

  $id_aluno = (int) $aluno;
  $query = "SELECT * FROM liberar_aluno INNER JOIN liberar_aval USING (id_libera) " .
           "WHERE id_usuario = '".$id_aluno."' AND status_libera = '1' AND dt_envio IS NULL ".
           "AND status_aval = '0' " .
           "ORDER BY id_curso, id_disc, id_avaliacao";

  $rs = $obj_banco->Execute($query);

  $retorno = array();
  if($rs)
  {
     while($array = $rs->FetchRow()) {
       array_push($retorno, $array);
   }
  }
  return $retorno;
}

/**
 * Fun��o para listar todas as notas das avalia��es do aluno
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $aluno ID da aluno
 * @return array com todas as informa��es da nota do aluno
 */
function listar_avaliacao_nota($obj_banco, $aluno)
{
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
     die (A_LANG_AVS_ERROR_DB_OBJECT);

  $id_aluno = (int) $aluno;
  $query = "SELECT vl_nota, vl_nota_cancelada, id_libera, id_libera_aluno, id_disc, nome_disc, id_curso, nome_curso,".
						" ds_avaliacao, bl_presencial, dt_liberar ".
						" FROM liberar_aval INNER JOIN liberar_aluno USING ( id_libera ) INNER JOIN disciplina USING (id_disc) INNER JOIN curso USING (id_curso)".
						" WHERE id_usuario = '".$id_aluno."' AND divulgacao_nota = '1'";

  $rs = $obj_banco->Execute($query);

  $retorno = array();
  if($rs) {
    while($array = $rs->FetchRow()) {
      array_push($retorno, $array);
    }
  }
  return $retorno;
}

/**
 * Fun��o que busca todas as avalia��es que foram liberadas, para poder trancar o conte�do da disciplina
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $aluno ID da aluno
 * @return array Retorna num array todas as IDs da disciplina que h� avalia��es em andamento
 */
function avaliacao_liberada($obj_banco, $aluno)
{
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    return FALSE;

  $id_aluno = (int) $aluno;

  $query = "SELECT id_disc FROM liberar_aluno INNER JOIN liberar_aval USING(id_libera) " .
           "WHERE id_usuario = '".$id_aluno."' AND status_libera = 1";

  $rs = $obj_banco->Execute($query);
  $retorno = array();
  if($rs)
  {
    while($array = $rs->FetchRow()) {
      array_push($retorno, $array[0]);
   }
  }
  return $retorno;
}

/**
 * Fun��o que verifica se o aluno tem alguma avalia��o da disciplina em aberto, n�o podendo abrir o ambiente
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $aluno ID da aluno
 * @param integer $disciplina ID da disciplina
 * @return integer 1 = h� avalia��o aberta, 0 = para n�o h� avalia��o
 */
function verifica_avaliacao_liberada($obj_banco, $aluno, $disciplina) {
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    return 1;

  $aluno = (int) $aluno;
	$disciplina = (int) $disciplina;
	
	$sql = "SELECT id_libera FROM liberar_aluno INNER JOIN liberar_aval USING(id_liberar) " .
         "WHERE id_usuario='".$aluno."' AND id_disc='".$disciplina."' AND status_libera = 1";

  $rs = $obj_banco->Execute($sql);

  if($rs) return 1; // retorna 1 se haver avalia��o aberta
  else return 0; // retorna 0 se n�o houver avalia��o aberta
}	

/**
 * Verifica a sess�o do usu�rio para verificar se o mesmo n�o mudou de computador para acesso ao conte�do
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $aluno ID da aluno
 * @return array Formato: array('sessao'=>id da sessao, 'ip' => ip do usuario);
 */
function verificar_sessao($obj_banco, $aluno) {
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    return FALSE;

	$id_usuario = (int) $aluno;

  $query = "SELECT sessao_usuario AS sessao, ip_usuario AS ip FROM usuario ".
           "WHERE sessao_usuario = '".session_id()."' AND ip_usuario = '".$_SERVER['REMOTE_ADDR']."' ".
           "AND id_usuario ='".$id_usuario."'";
  $rs = $obj_banco->Execute($query);

  if($rs->NumRows() > 0) return TRUE;
  else return FALSE;

}

function verifica_resposta_aluno($obj_banco, $aluno, $libera_aluno) {
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    return FALSE;
	
	$query = "SELECT id_usuario FROM liberar_aluno WHERE id_libera_aluno =".$libera_aluno;

	$rs = $obj_banco->Execute($query);
	
	if($rs->NumRows() > 0) {
	  if($rs->Fields('id_usuario') == $aluno) return TRUE;
		else return FALSE;	
	} else return FALSE;
}


function aluno_filtro_disciplina($nota, $curso) {
	$array = array();
	foreach($nota as $k) {
		if($k['id_curso'] == $curso) {
				$array[] = $k;
		}
	}
	return $array;
}

?>
