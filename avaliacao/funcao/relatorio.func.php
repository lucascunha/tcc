<?php


/**
 * Fun��es utilizadas para a fun�ao de relat�rios do m�dulo de avalia�ao somativa
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <06/07/2008>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 */

function relatorio_aluno($obj_banco, $aluno, $disciplina, $curso) {
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_aluno = (int) $aluno; 
	$id_disciplina = (int) $disciplina; 
	$id_curso = (int) $curso; 
	
	$query = "SELECT DISTINCT liberar_aluno.*, liberar_aval.*, matricula.*, usuario.nome_usuario, curso.nome_curso, ".
					" grupo_curso.vl_peso ".
					" FROM liberar_aval INNER JOIN liberar_aluno USING(id_libera) ".
					" INNER JOIN grupo_curso USING(id_curso, id_grupo_aval) ".
					" INNER JOIN matricula USING(id_usuario, id_disc, id_curso)".
					" INNER JOIN usuario USING(id_usuario) INNER JOIN curso USING(id_curso)".
					" WHERE id_usuario=".$id_aluno." AND id_disc=".$id_disciplina." AND id_curso=".$id_curso;

  $rs = $obj_banco->Execute($query);
	
  $retorno = array();
  if($rs)
     while($array = $rs->FetchRow())  array_push($retorno, $array);
  return $retorno;
}

function relatorio_lista_curso($obj_banco, $disciplina){
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_disciplina = (int) $disciplina; 
	
	$query = "SELECT DISTINCT curso.id_curso AS id, curso.nome_curso AS nome".
					" FROM liberar_aluno INNER JOIN curso USING(id_curso) ".
					" WHERE id_disc=".$id_disciplina;
					
  $rs = $obj_banco->Execute($query);
	
  $retorno = array();
  if($rs)
     while($array = $rs->FetchRow())  array_push($retorno, $array);
  return $retorno;
}

function relatorio_lista_aluno($obj_banco, $disciplina, $curso){
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_disciplina = (int) $disciplina; 
	$id_curso = (int)$curso;
	
	$query = "SELECT DISTINCT usuario.id_usuario AS id, usuario.nome_usuario AS nome".
					" FROM liberar_aluno INNER JOIN usuario USING(id_usuario) ".
					" WHERE id_disc=".$id_disciplina." AND id_curso=".$id_curso;					
		
  $rs = $obj_banco->Execute($query);
	
  $retorno = array();
  if($rs)
     while($array = $rs->FetchRow())  array_push($retorno, $array);
  return $retorno;
}


function relatorio_lista_aval($obj_banco, $disciplina) {
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_disciplina = (int) $disciplina; 
	
	$query = "SELECT id_libera AS libera, ds_avaliacao AS descricao, dt_liberar AS data_liberar".
           " FROM liberar_aval INNER JOIN grupo_aval USING(id_grupo_aval)".
           " WHERE status_libera = 0 AND id_disc = '".$id_disciplina."' AND bl_presencial = 0";
			 
  $rs = $obj_banco->Execute($query);
	
  $retorno = array();
  if($rs)
     while($array = $rs->FetchRow())  array_push($retorno, $array);
  return $retorno;
}


function relatorio_resposta($obj_banco, $libera) {
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_libera = (int) $libera; 
	
	$query = "SELECT resposta.*, liberar_aluno.*, questao.*, matricula.* ".
					" FROM liberar_aval INNER JOIN liberar_aluno USING(id_libera) LEFT JOIN resposta USING (id_libera_aluno)".
					" INNER JOIN matricula USING(id_usuario, id_disc, id_curso) INNER JOIN questao USING(id_questao)".
					" WHERE id_libera=".$id_libera;
					
  $rs = $obj_banco->Execute($query);
	
  $retorno = array();
  if($rs)
     while($array = $rs->FetchRow())  array_push($retorno, $array);
  return $retorno;
}


function relatorio_busca_questao($obj_banco, $libera, $questao) {
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_libera = (int) $libera; 
	$id_questao = (int) $questao;
	
	$query = "SELECT resposta.*, usuario.nome_usuario ".
					" FROM liberar_aval INNER JOIN liberar_aluno USING(id_libera) LEFT JOIN resposta USING (id_libera_aluno)".
					" INNER JOIN usuario USING(id_usuario) ".
					" WHERE id_libera=".$id_libera." AND id_questao=".$id_questao;
					
  $rs = $obj_banco->Execute($query);
	
  $retorno = array();
  if($rs)
     while($array = $rs->FetchRow())  array_push($retorno, $array);
  return $retorno;	
}

function relatorio_filtro_aluno_questao($resposta, $aluno) {
	$array = array();
	foreach($resposta as $k) {
		if($k['id_libera_aluno'] == $aluno) {
			$array[] = $k;
		}
	}
	return $array;
}



function resposta_filtro_questao_correta($resposta, $questao) {
	$array = array();
	foreach($resposta as $k) {
		if($k['id_questao'] == $questao) {
			if($k['qt_acerto'] == 1) {
				$array[] = $k;
			}
		}
	}
	return $array;
}

function resposta_filtro_questao_incorreta($resposta, $questao) {
	$array = array();
	foreach($resposta as $k) {
		if($k['id_questao'] == $questao) {
			if($k['qt_acerto'] == 0) {
				$array[] = $k;
			}
		}
	}
	return $array;
}

function resposta_filtro_questao_meio($resposta, $questao) {
	$array = array();
	foreach($resposta as $k) {
		if($k['id_questao'] == $questao) {
			if($k['qt_acerto'] > 0 && $k['qt_acerto'] < 1) {
				$array[] = $k;
			}
		}
	}
	return $array;
}

?>