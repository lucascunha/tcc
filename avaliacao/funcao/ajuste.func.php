<?php
/**
 * Arquivo de fun��es destinadas a opera��es de ajuste de notas
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Funcao
 * @subpackage Ajuste
 * @filesource
 * @version 1.0
 */

/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

/**
 * Fun��o que retorna os cursos para ajustar a m�dia
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $disciplina ID da disciplina
 * @return array Contem o id e nome do curso array('id','nome')
 */
function ajuste_media_listar_curso($obj_banco, $disciplina) {
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_disciplina = (int) $disciplina; 
  $query = "SELECT DISTINCT g.id_curso AS id, c.nome_curso AS nome ".
           "FROM grupo_curso g NATURAL JOIN curso c NATURAL JOIN grupo_aval a " .
           "WHERE a.id_disc = ".$id_disciplina;

  $rs = $obj_banco->Execute($query);
	
  $retorno = array();
  if($rs)
     while($array = $rs->FetchRow())  array_push($retorno, $array);
  return $retorno;
}


/**
 * Fun��o que lista a media dos alunos
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $curso ID do curso
 * @param integer $disciplina ID da disciplina
 * @return array Contem o id, aluno, media, arredonda do aluno. Array('id','aluno','media','arredonda')
 */
function ajuste_media_listar_aluno($obj_banco, $curso, $disciplina) {

  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
		die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_disciplina = (int) $disciplina; 
	$id_curso = (int) $curso; 
			
	$query = "SELECT id_usuario AS id, u.nome_usuario AS aluno, m.vl_media_aval AS media, m.vl_arred AS arredonda ".
					 "FROM usuario u INNER JOIN matricula m USING (id_usuario) ".
					 "WHERE m.id_curso = ".$id_curso." AND m.id_disc = ".$id_disciplina;				 

  $rs = $obj_banco->Execute($query);
	
  $retorno = array();
  if($rs)
     while($array = $rs->FetchRow())  array_push($retorno, $array);
  return $retorno;
}

// lista notas do aluno

/**
 * Fun��o que lista as notas do aluno
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $aluno ID do aluno
 * @param integer $curso ID do curso
 * @param integer $disciplina ID da disciplina
 * @return array Contem o valor da nota e avalia��o do curso array('nota','avaliacao')
 */
function ajuste_media_listar_nota_aluno($obj_banco, $aluno, $curso, $disciplina) {
	global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_disciplina = (int) $disciplina; 
	$id_curso = (int) $curso; 
	$id_aluno = (int) $aluno;
			 
	$query = "SELECT vl_nota AS nota, vl_nota_cancelada AS cancelada, (vl_nota + vl_nota_cancelada) AS soma, ds_avaliacao AS avaliacao".
	         " FROM liberar_aluno INNER JOIN liberar_aval USING(id_libera)".
					 " WHERE id_curso = ".$id_curso." AND id_disc = ".$id_disciplina." AND id_usuario = ".$id_aluno;

  $rs = $obj_banco->Execute($query);
	
  $retorno = array();
  if($rs)
     while($array = $rs->FetchRow())  array_push($retorno, $array);
  return $retorno;

}

/**
 * Salva o ajuste na m�dia do aluno
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $aluno ID do aluno
 * @param integer $curso ID do curso
 * @param integer $disciplina ID da disciplina
 * @param integer $arredonda Valor da nota de participa��o (arredondamento) da m�dia final
 * @return boolean
 */
function ajuste_media_salvar($obj_banco, $aluno, $curso, $disciplina, $arredonda) {
	global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$aluno = (int) $aluno;
	$disciplina = (int) $disciplina;
	$curso = (int) $curso;
	$arredonda = (float) $arredonda;

	$sql = "UPDATE matricula SET vl_arred =".$arredonda." ".
				 "WHERE id_usuario = '".$aluno."' AND id_curso = ".$curso." AND id_disc = ".$disciplina;
				 
	$rs = $obj_banco->Execute($sql);
	if($rs) return TRUE;
	else return FALSE;
}

// -------------- FIM DAS FUN��ES DE AJUSTE DE M�DIA -------------------------------------//

// -------------- FUN��ES PARA ANULA��O DAS QUEST�ES -------------------------------------//

/**
 * Fun��o que lista todas as avalia��es que foram liberadas e j� conclu�das
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $disciplina ID da disciplina
 * @param integer $arredonda Valor da nota de participa��o (arredondamento) da m�dia final
 * @return array Array('descricao' =>descricao da avaliacao,'topico' =>n�mero do t�pico,'avaliacao'=>id,'data' => data da liberacao)
 */
function ajuste_questao_listar_libera($obj_banco, $disciplina) {
 
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_disciplina = (int) $disciplina; 
	
	$query = "SELECT id_libera AS libera, ds_avaliacao AS descricao, dt_liberar AS data_liberar".
           " FROM liberar_aval INNER JOIN grupo_aval USING(id_grupo_aval)".
           " WHERE status_libera = 0 AND id_disc = '".$id_disciplina."' AND bl_presencial = 0";
			 
  $rs = $obj_banco->Execute($query);
	
  $retorno = array();
  if($rs)
     while($array = $rs->FetchRow())  array_push($retorno, $array);
  return $retorno;

}

/**
 * Lista todas as quest�es de uma avalia��o que j� foi realizada
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $libera ID da avalia��o liberada
 * @param integer $arredonda Valor da nota de participa��o (arredondamento) da m�dia final
 * @return array Array('avaliacao' => id da avaliacao)
 */
function ajuste_questao_listar_questao($obj_banco, $libera) {

  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_libera = (int) $libera; 
	
	$query = "SELECT id_avaliacao AS avaliacao ".
				 	 "FROM liberar_aval INNER JOIN avaliacao USING (id_liberar) ".
					 "WHERE id_libera =".$id_libera;
			 
  $rs = $obj_banco->Execute($query);
	
  $retorno = array();
  if($rs)
     while($array = $rs->FetchRow())  array_push($retorno, $array);
  return $retorno;
}

function ajuste_questao_listar_aluno($obj_banco, $avaliacao) {

  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_aval = (int) $avaliacao; 
	
	$query = "SELECT id_libera_aluno AS id ".
	         "FROM liberar_aval INNER JOIN liberar_aluno USING (id_libera) ".
					 "INNER JOIN avaliacao USING (id_avaliacao) WHERE id_avaliacao = ".$id_aval;
  $rs = $obj_banco->Execute($query);
	
  $retorno = array();
  if($rs)
     while($array = $rs->FetchRow())  array_push($retorno, $array);
  return $retorno;
}


// ----------------- AJUSTE DE NOTAS PRESENCIAIS ----------------------------------
/**
 * Fun��o para listar os nomes dos conceitos que tenha avalia��es que possam ser liberadas
 * @param ADOdb_* $obj_banco Objeto de conex�o do banco de dados
 * @param integer $disciplina ID da disciplina
 * @return array Vetor contendo as informa��es: topico, grupo, curso
 */
function ajuste_presencial_listar_grupo($obj_banco, $disciplina) {

  global $A_DB_TYPE;
	
	$id_disciplina = (int) $disciplina;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);
  // seleciona todos os grupos que da disciplina indicada, retirando os resultado de provas presenciais
  // e grupos sem avalia��o.

  // o nome_curso � apenas para mostrar no combobox, retir�-lo n�o influenciar� na pesquisa
	$query = "SELECT id_libera AS libera, dt_liberar AS data, ds_avaliacao AS avaliacao, topico ".
	         " FROM liberar_aval INNER JOIN grupo_aval USING(id_grupo_aval) ".
					 " WHERE bl_presencial = 1 AND id_disc=".$id_disciplina;
  $rs = $obj_banco->Execute($query);
	
  $retorno = array();
	while($array = $rs->FetchRow()) {
		array_push($retorno, $array);
	}		
  return $retorno;
}


// -------------------------- AJUSTE PARA CORRE��O DE QUEST�ES DISSERTATIVAS --------------------------
function ajuste_dissert_lista_avaliacao($obj_banco, $disciplina) {
  global $A_DB_TYPE;
	
	$id_disciplina = (int) $disciplina;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);
		
	$query = "SELECT DISTINCT ds_avaliacao AS avaliacao, topico, id_libera AS id, dt_liberar AS data_liberar FROM liberar_aval".
					 " LEFT JOIN liberar_questao USING ( id_libera ) LEFT JOIN grupo_aval USING ( id_grupo_aval ) ".
					 " WHERE status_libera =0 ".
					 " AND id_questao IN (SELECT id_questao FROM liberar_questao INNER JOIN questao USING ( id_questao ) WHERE tipo_questao =4)".
					 " AND id_disc = ".$id_disciplina;					 
					 
  $rs = $obj_banco->Execute($query);
  $retorno = array();
  if($rs) {
    while($array = $rs->FetchRow()) {
			array_push($retorno, $array);
		}	
	}	 
  return $retorno;
}

?>