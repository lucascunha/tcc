<?php

/**
 * Fun��es utilizadas para a fun�ao de backup (PDF) do m�dulo de avalia�ao somativa
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <30/07/2008>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 */

function backup_lista_avaliacao($obj_banco, $disciplina) {
  global $A_DB_TYPE;
  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_disciplina = (int) $disciplina; 
	
	$query = "SELECT id_libera AS libera, ds_avaliacao AS descricao, dt_liberar AS data_liberar".
           " FROM liberar_aval INNER JOIN grupo_aval USING(id_grupo_aval)".
           " WHERE status_libera = 0 AND id_disc = '".$id_disciplina."'";

  $rs = $obj_banco->Execute($query);
	
  $retorno = array();
  if($rs)
     while($array = $rs->FetchRow())  array_push($retorno, $array);
  return $retorno;
}



?>