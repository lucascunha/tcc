<?php
/**
 * Descri��o
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <26/02/2008>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

/**
 * Arquivo de fun��es destinadas a opera��es de ajuste de notas
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Funcao
 * @subpackage Ajuste
 * @filesource
 * @version 1.0
 */

/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

/**
 * Fun��o que retorna os grupos de avalia��o junto com o peso para um determinado curso
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $curso ID da curso
 * @return array Formato: array('topico' => n�mero do t�pico,'grupo' => id do grupo,'peso' => peso definido para o grupo)
 */
function media_listar_topico ($obj_banco, $curso, $disciplina) {

  global $A_DB_TYPE;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_curso = (int) $curso;
	$id_disciplina = (int) $disciplina;

  $query = "SELECT a.topico AS topico, a.id_grupo_aval AS grupo, c.vl_peso AS peso".
           " FROM grupo_aval a INNER JOIN grupo_curso c USING(id_grupo_aval)".
           " WHERE c.id_curso=".$id_curso." AND id_disc =".$id_disciplina;

  $rs = $obj_banco->Execute($query);

  $retorno = array();

  while($vet = $rs->FetchRow()) {
    array_push($retorno, $vet);
  }

  return $retorno;
}

/**
 * Fun��o que lista os cursos de uma determinada disciplina para fazer atribuir o peso para o c�lculo da m�dia
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $disciplina ID da disciplina
 * @return array Contem o id e nome do curso array('id' => id do curso,'nome' => nome do curso)
 */
function media_listar_curso($obj_banco, $disciplina) {

  global $A_DB_TYPE;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_disciplina = (int) $disciplina;
	
  $query = "SELECT DISTINCT g.id_curso AS id, c.nome_curso AS nome".
	         " FROM grupo_aval a INNER JOIN grupo_curso g USING(id_grupo_aval) INNER JOIN curso c USING(id_curso)".
					 " WHERE a.id_disc = ".$id_disciplina;				 

  $rs = $obj_banco->Execute($query);

  $retorno = array();

  while($vet = $rs->FetchRow()) {
    array_push($retorno, $vet);
  }

  return $retorno;
}

function media_listar_aluno($obj_banco, $curso, $disciplina) {

 global $A_DB_TYPE;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_disciplina = (int) $disciplina;
	$id_curso = (int) $curso;
	
  $query = "SELECT DISTINCT id_usuario AS id FROM liberar_aluno INNER JOIN liberar_aval USING(id_libera) ".
					 "WHERE id_disc =".$id_disciplina." AND id_curso=".$id_curso;

  $rs = $obj_banco->Execute($query);

  $retorno = array();

  while($vet = $rs->FetchRow()) {
    array_push($retorno, $vet);
  }

  return $retorno;


}

?>
