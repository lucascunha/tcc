<?php
/**
 * Arquivo de fun��es destinadas a opera��es de ajuste de notas
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Funcao
 * @subpackage Calcular_Media
 * @filesource
 * @version 1.0 
 */

/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

/**
 * Fun��o que calcula a m�dia de um aluno
 * @param object $obj_banco Objeto ADODB para conex�o com o banco de dados
 * @param integer $aluno ID do aluno
 * @param integer $curso ID do curso
 * @param integer $disciplina ID da disciplina
 * @return boolean 
 */
function calcular_media($obj_banco, $aluno) {

	global $A_DB_TYPE;
	if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
		return FALSE;
	
	$aluno = (int) $aluno;

	// seleciona todas as notas do aluno
	$query = "SELECT sum((vl_nota_cancelada + vl_nota) * (vl_peso/100)) AS media".
					" FROM liberar_aval INNER JOIN liberar_aluno USING (id_libera)".
					" INNER JOIN grupo_aval USING (id_grupo_aval)".
					" INNER JOIN grupo_curso USING (id_grupo_aval, id_curso)".
					" WHERE id_usuario = ".$aluno;
					
	$rs = $obj_banco->Execute($query);
	
	if(!$rs->NumRows() > 0) return FALSE; // retorna falso se n�o haver nenhuma nota (Problema de tentar calcular a m�dia sem haver notas ainda)
	
	$media = $rs->Fields('media');
	
	$media = (int) $media;	
	
	if (!$media) $media = 0;

	// busca o valor da nota de participacao (vl_arred);
	$qarred = "SELECT vl_arred FROM liberar_aluno INNER JOIN matricula USING (id_usuario, id_disc, id_curso) ".
						"WHERE id_usuario = ".$aluno;

	$rs = $obj_banco->Execute($qarred);
	$arredonda = $rs->Fields('vl_arred');					
	
	if(!$arredonda) $arredonda = 0;
	
	if($media + $arredonda > 100) $arredonda = $media - 100; // controle para n�o ultrapassar a 10 na nota.
		
	$sql = "UPDATE matricula INNER JOIN liberar_aluno USING(id_usuario, id_curso, id_disc) ".
	       "SET vl_arred =".$arredonda.", vl_media_aval = ".$media." ".
				 "WHERE id_usuario = ".$aluno;
	unset($rs);

	$rs = $obj_banco->Execute($sql);
	
	if($rs) return TRUE;
	else return FALSE;
}  

?>