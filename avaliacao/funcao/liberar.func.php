<?php
/**
 * Fun��es utilizadas na aba de Libera��o de disciplina
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <14/12/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

/**
 * Fun��o para listar os nomes dos conceitos que tenha avalia��es que possam ser liberadas
 * @param ADOdb_* $obj_banco Objeto de conex�o do banco de dados
 * @param integer $disciplina ID da disciplina
 * @return array Vetor contendo as informa��es: topico, grupo, curso
 */
function liberar_listar_conceito($obj_banco, $disciplina) {

  global $A_DB_TYPE;
	
	$id_disciplina = (int) $disciplina;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);
  // seleciona todos os grupos que da disciplina indicada
	// e grupos sem avalia��o.

  // o nome_curso � apenas para mostrar no combobox, retir�-lo n�o influenciar� na pesquisa					 
	$query = "SELECT topico AS topico, id_grupo_aval AS grupo, nome_curso AS curso".
					 " FROM grupo_aval INNER JOIN grupo_curso USING(id_grupo_aval)".
					 " INNER JOIN curso USING(id_curso)".
					 " WHERE id_disc = ".$id_disciplina." AND id_grupo_aval NOT IN (". 
					 " SELECT id_grupo_aval FROM grupo_curso WHERE vl_peso IS NULL)". 
					 " ORDER BY topico ASC";				 
				 
  $rs = $obj_banco->Execute($query);
	
  $vet_conceito = array();
  if($rs->NumRows() > 0) {
    $vetor = $rs->FetchRow();
    $insert['topico'] = $vetor['topico'];
    $insert['grupo'] = $vetor['grupo'];
    $insert['curso'] = array();
  } 
	
	$rs->MoveFirst();
	while($vetor = $rs->FetchRow()) {
	  // adiciona o curso em um vetor separado se os outros dados forem iguais
		if ($insert['topico'] == $vetor['topico'] && $insert['grupo'] == $vetor['grupo']) {
      array_push($insert['curso'], $vetor['curso']);
    } else {
		// senao insere na primeira casa
      array_push($vet_conceito, $insert);
      $insert['topico'] = $vetor['topico'];
      $insert['grupo'] = $vetor['grupo'];
      $insert['curso'] = array();
      array_push($insert['curso'], $vetor['curso']);
    }
	}
	
  if(isset($insert)) array_push($vet_conceito, $insert);
  return $vet_conceito;
}


/**
 * Fun��o para listar os nomes das avalia��es de um grupo e disciplina espec�fico
 * @param ADOdb_* $obj_banco Objeto de conex�o do banco de dados
 * @param integer $grupo ID do grupo de avalia��o
 * @param integer $disciplina ID da disciplina
 * @return array Vetor contendo as informa��es: id (id_avaliacao), descricao
 */
function liberar_listar_avaliacao($obj_banco, $grupo, $disciplina) {

  $vet_avaliacao = array();
  global $A_DB_TYPE;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);
	
	$id_grupo = (int) $grupo;
	$id_disciplina = (int) $disciplina;

  $query = "SELECT id_avaliacao AS id, ds_avaliacao AS descricao, bl_presencial AS presencial".
						" FROM avaliacao INNER JOIN grupo_aval USING (id_grupo_aval)".
						" WHERE id_grupo_aval = ".$id_grupo." AND id_disc = ".$id_disciplina.
						" AND id_avaliacao NOT IN (".
						" SELECT id_avaliacao FROM avaliacao INNER JOIN aval_questao USING ( id_avaliacao ) WHERE vl_peso_questao IS NULL)". // impede avalia�oes sem defini�ao de pesos nas questoes
						" AND id_avaliacao NOT IN (" .
						" SELECT id_avaliacao FROM grupo_curso WHERE vl_peso IS NULL)". // impede grupo sem defini�ao de peso
						" ORDER BY bl_presencial ASC"; 

  $rs = $obj_banco->Execute($query);

  while($vetor = $rs->FetchRow(false)) {
    array_push($vet_avaliacao, $vetor);
  }

  return $vet_avaliacao;
}

/**
 * Fun��o para listar as avalia��es que j� foram realizadas na disciplina
 * @param ADOdb_* $obj_banco Objeto de conex�o do banco de dados
 * @param integer $disciplina ID da disciplina
 * @return array Vetor contendo as informa��es: topico, grupo, curso
 */
function liberar_listar_avaliacao_realizada($obj_banco, $disciplina) {

  global $A_DB_TYPE;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_disciplina = (int) $disciplina;

  $vet_libera = array();
  $query = "SELECT distinct id_libera AS libera, id_avaliacao AS avaliacao, ds_avaliacao AS descricao, topico " .
           "FROM liberar_aval INNER JOIN grupo_aval USING(id_grupo_aval)" .
           "WHERE id_disc = $id_disciplina AND status_libera=1";					 

  $rs = $obj_banco->Execute($query);

  while($obj = $rs->FetchRow(false)) {
    array_push($vet_libera, $obj);
  }

  return $vet_libera;
}

/**
 * Fun��o para listar as avalia��es que j� foram realizadas na disciplina
 * @param ADOdb_* $obj_banco Objeto de conex�o do banco de dados
 * @param integer $disciplina ID da disciplina
 * @return array Vetor contendo as informa��es: topico, grupo, curso
 */
function liberar_listar_avaliacao_encerrada($obj_banco, $disciplina) {

  global $A_DB_TYPE;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_disciplina = (int) $disciplina;

  $vet_libera = array();
  $query = "SELECT distinct id_libera AS libera, id_avaliacao AS avaliacao, ds_avaliacao AS descricao, topico, bl_presencial AS presencial".
					 " FROM liberar_aval INNER JOIN grupo_aval USING(id_grupo_aval)" .
           " WHERE id_disc = $id_disciplina AND status_libera=".ENCERRADA;
					 
  $rs = $obj_banco->Execute($query);

  while($obj = $rs->FetchRow(false)) {
    array_push($vet_libera, $obj);
  }

  return $vet_libera;
}

/**
 * Fun��o que lista os alunos que est�o liberados para realizar a avalia��o.
 * Retorna tamb�m o status da avalia��o
 */

function liberar_listar_aluno_realizada($obj_banco, $libera) {

  global $A_DB_TYPE;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_libera = (int) $libera;

	$query = "SELECT nome_usuario AS nome, dt_envio AS envio, status_aval AS status ".
	         "FROM liberar_aluno NATURAL JOIN matricula NATURAL JOIN usuario ".
					 "WHERE id_libera = ".$id_libera;
					 
	$rs = $obj_banco->Execute($query);

  $vet_aluno = array();	
  while($obj = $rs->FetchRow(false)) {
    array_push($vet_aluno, $obj);
  }
	
	return $vet_aluno;
}

  // listar os alunos que j� realizaram a avalia��o. 
function liberar_listar_alunos_avaliados($obj_banco, $grupo) {
  global $A_DB_TYPE;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);
		
	$id_grupo = (int) $grupo;
		
	$query = "SELECT id_usuario FROM liberar_aval INNER JOIN liberar_aluno USING (id_libera)".
					 " WHERE id_grupo_aval = $id_grupo";

  $rs_aluno = $obj_banco->Execute($query);

  $alunos_avaliados = array();
  while ($array = $rs_aluno->FetchRow()) {
    array_push($alunos_avaliados, $array['id_usuario']);
  }
	
	return $alunos_avaliados;
}

/**
 * Fun��o para limpar a base de dados de avalia�oes sem alunos
 * @param ADOdb_* $obj_banco Objeto de conex�o do banco de dados
 * @param integer $disciplina ID da disciplina
 */
function liberar_limpar_dados($obj_banco,$disciplina) {

  global $A_DB_TYPE;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_disciplina = (int) $disciplina;

  $query = "DELETE liberar_aval, liberar_questao".
	         " FROM liberar_aval INNER JOIN grupo_aval USING(id_grupo_aval) LEFT JOIN liberar_questao USING (id_libera)".
					 " WHERE id_libera NOT IN( SELECT id_libera FROM liberar_aluno WHERE  id_disc = ".$id_disciplina." )".
					 " AND id_disc = ".$id_disciplina;
	$rs = $obj_banco->Execute($query);
}	


function liberar_buscar_avaliacao($obj_banco, $avaliacao) {
  global $A_DB_TYPE;

  if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
    die (A_LANG_AVS_ERROR_DB_OBJECT);

	$id_avaliacao = (int) $avaliacao;

  $query = "SELECT id_libera FROM liberar_aval WHERE bl_presencial = 1 AND id_avaliacao=".$avaliacao;
	
  $rs = $obj_banco->Execute($query);
	if($rs) return $rs->Fields('id_libera');
	else return FALSE;
}

?>