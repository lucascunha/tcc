<?php
/**
 * Descri��o
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <09/02/2008>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/funcao/aluno.func.php";
  require_once "avaliacao/class/liberar.class.php";

  montar_orelha_aluno_avaliacao(AVS_NAV_MURAL);

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

  $nota = listar_avaliacao_nota($conn, $_SESSION['id_aluno']);

?>

<table CELLSPACING=5 CELLPADDING=5 border="0" width="100%"  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?>  height="80%" style="height:380px;" >
  <tr valign='top'>
    <td>
<table CELLSPACING=0 CELLPADDING=10 width="98%"  border = "0"  bgcolor="#ffffff" style="margin-left: 10px; margin-top: 10px; " class="tabela_redonda">
  <tr>
    <td>
				<?php
					if(count($nota) > 0) {		
					
						$_UASORT_CAMPO = "id_curso";	
						usort($nota, 'ordena'); //ordena o vetor pelo tipo
						
						echo "<div class='lista_aval'>";
						
						$curso = NULL;
						foreach($nota as $k) {
							if ($curso != $k['id_curso']) {
								if($curso != NULL) {								
									echo "</fieldset>";
									echo "</div>";
								}
								echo "<div class='lista_aluno'>";
								echo "<fieldset><legend>".$k['nome_curso']."</legend>";	
							}						
						
							echo "<br />";
							if ($curso == $k['id_curso']) {
							
								$disc = aluno_filtro_disciplina($nota, $curso);												
								$disc_anterior = NULL;
				
								foreach ($disc as $k) {										
									if ($disc_anterior != $k['id_disc']) {
										if ($disc_anterior != NULL) {
											echo "</table>";
											echo "</fieldset>";
											echo "<br />";
											echo "<fieldset><legend>".$k['nome_disc']."</legend>";
										}							
										// imprime o cabe�alho da tabela
										echo "<fieldset><legend>".$k['nome_disc']."</legend>";																				
										echo "<table cellspacing='0' cellpadding='0' bgcolor='#009ACD' width='95%' border='0' >\n";
										echo "<tr class='Titulo'>\n";
										echo "<td width='40%' rowspan='2'>".A_LANG_AVS_TABLE_USER_DESCRIPTION_EVALUATION."</td>\n";
										echo "<td width='20%' rowspan='2'>".A_LANG_AVS_TABLE_USER_DATE_REALIZE."</td>\n";				
										echo "<td width='40%' colspan='3' align='center'>".A_LANG_AVS_TABLE_USER_VALUE."</td>\n";
										echo "</tr><tr align='center'>";
										echo "<td><acronym title='".A_LANG_AVS_HINTS_USER_EVALUATION_VIEW."'>".A_LANG_AVS_TABLE_USER_VALUE2."</acronym></td>\n";
										echo "<td>Quest&otilde;es canceladas</td>\n";								
										echo "<td>".A_LANG_AVS_TABLE_USER_TOTAL_VALUE."</td>\n";
										echo "</tr>\n";
						
										// imprime cada avalia��o liberada
										$classe = "zebraA";
									}	
								
									echo "<tr class='".$classe."'>\n";
									echo "<td><a href='avaliacao/n_visualizar_aval.php?libera_aluno=".$k['id_libera_aluno']."' target='_blank' title='".A_LANG_AVS_HINTS_EVALUATION_VIEW."'>".quote($k['ds_avaliacao'], 70)."</a></td>\n";
									echo "<td>".formata_data_hora($k['dt_liberar'])."</td>";
									echo "<td align='right'>".formata_nota($k['vl_nota'])."</td>";
									echo "<td align='right'>".formata_nota($k['vl_nota_cancelada'])."</td>";									
									echo "<td align='right'>".formata_nota($k['vl_nota'] + $k['vl_nota_cancelada'])."</td>";
									echo "</tr>\n";
									$classe = $classe == "zebraA" ? "zebraB" : "zebraA";
									$disc_anterior = $k['id_disc'];
								}
								echo "</table>";
								echo "</fieldset>";								
							}
							$curso = $k['id_curso'];
						}
						echo "</fieldset>";
						echo "</div>";
						echo "</div>";
					} else {
            	?>
                <table CELLSPACING=0 CELLPADDING=0 width="100%"  border = "0" style=padding-top:11px;>
                    <tr>
                      <td>  
                             <div class="Mensagem_Amarelo">
                               <?
                               echo "<p class=\"texto1\">\n";     
                               echo "<table cellspacing=\"0\" cellpadding=\"0\" width=80% > ";
                               echo "  <tr>";
                               echo "    <td width=\"50\">";
                               echo "      <img src=\"imagens/icones/warning.png\" alt=\"\" />";
                               echo "    </td>";
                               echo "    <td valign=\"center\">";
                               
                               echo A_LANG_AVS_LABEL_USER_NOT_VALUE;
                               echo "    </td>";
                               echo "  </tr>";
                               echo "</table>";  
                               echo "</p>\n";      
                               ?>    
                             </div>
                             <br>
                        </td>
                      </tr>       
                 </table>  
            	<?php        						
				}		
				?>

  </td>
</tr>
</table>  
</td>
</tr>
</table>
