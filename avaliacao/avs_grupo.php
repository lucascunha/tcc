<?php
/**
 * Modifica o pesos das questes para a avaliao
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Scripts
 * @subpackage Grupo_Avaliacao
 * @version 1.0 16/10/2007
 */

/*
 * Trabalho de Concluso de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

  require_once "avaliacao/questao.const.php";
	require_once "avaliacao/ajuda.const.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/questao.class.php";
	require_once "avaliacao/class/erro.class.php";

  $numtopico = isset($_GET['numtopico']) ? $_GET['numtopico'] : NULL;	
	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : NULL;
	
	if(!$numtopico || !$CodigoDisciplina) {
		echo (A_LANG_AVS_ERROR_PARAM); // em caso de erro termina o script
		return;
	}	

	 // abre conexo com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

  $id_grupo = isset($_REQUEST['grupo']) ? $_REQUEST['grupo'] : NULL;
	
  $grupo = new Grupo($conn, $id_grupo);
	
	// SEGURANA, NO DEIXA OUTROS PROFESSORES ACESSAREM INFORMAES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo(A_LANG_AVS_ERROR_VERIFY_AUTHO);		
		return;
	}	

  $erro = new Erro();
  if(isset($_POST['enviar'])) {
    $grupo->set_disciplina($CodigoDisciplina);
    $grupo->set_topico($numtopico);
		$grupo->set_descricao($_POST['descricao']);
    
		//excluindo os cursos no selecionados
		$curso_banco = $grupo->cursos(); // pega os cursos gravados na base de dados
		$curso_sel = array();
		
		// pega os cursos Antigos (a) selecionados
		if (isset($_POST['acurso'])) {
			foreach ($_POST['acurso'] as $k => $value) {
				array_push($curso_sel, $k);
			}
		}
		//  realizada uma interseo para verificar qual dos cursos deve ser retirado
		$intersect = array();
		$intersect = array_diff($curso_banco,$curso_sel);

		// retira-se os cursos da base de dados
		if(count($intersect) > 0) {
			foreach ($intersect as $k) {
				$grupo->remover_curso($k);
			}
		}
		
		// inserindo novos cursos
		if(isset($_POST['ncurso'])) {
			foreach($_POST['ncurso'] as $k => $value) {
				$grupo->atribuir_curso($k);
			}
		}      
   	
		// salva a base de dados
		if (!$grupo->salvar()) {	
			$erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE);
		}			
  }

  montar_orelha_completa_avaliacao($numtopico, $CodigoDisciplina);

?>

<table cellspacing='1' cellpadding='1' width='100%' border='0' bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
  <tr>
    <td>
      <form name="frm_novo_grupo" method="post" action="<?php echo "a_index.php?opcao=EditarGrupo&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina&grupo=".$grupo->get_grupo(); ?>">
        <div class="funcao">
					<?php  
						if($erro->quantidade_erro() > 0) {
							echo "<div class='erro'>\n";
							echo $erro->imprimir_erro();
							echo "</div>\n";
						} elseif(isset($_POST['enviar'])) {
							echo "<div class='sucesso'>\n";
							echo "<p><span>".A_LANG_AVS_SUCESS."</span></p>\n";
							echo "</div>\n";				
						}
					?> 					
					<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_GROUP; ?></div>
					
					<p><?php echo A_LANG_AVS_LABEL_DESCRIPTION; ?>:</p>
					<p>&nbsp;<textarea name="descricao" id="descricao" class="button" style="width:400px; height:100px"><?php echo $grupo->get_descricao(); ?></textarea></p>
					<br />
					<p><span class="bold uppercase"><?php echo A_LANG_AVS_LABEL_COURSE; ?></span></p>
            <?php
              // encontra os cursos que j foram escolhidos em outro grupos do mesmo topico, para que no impedir a seleo novamente
              $query = "SELECT DISTINCT(id_curso) FROM grupo_aval av RIGHT JOIN grupo_curso c USING(id_grupo_aval) ".
                       "WHERE av.id_disc = $CodigoDisciplina AND av.topico = $numtopico AND c.id_grupo_aval <> '".$grupo->get_grupo()."'";
              $rs = $conn->Execute($query);

              $curso_escolhido = array();    // vetor que ir guardar os cursos que j foram escolhidos em outros grupos
              if ($rs) {
                while($id = $rs->FetchRow()){
                  array_push($curso_escolhido, $id[0]);
                }
              }

              for($t=0; $conteudo[$t]['NUMTOP']<>$numtopico; $t++);

              $total = count($conteudo[$t]['CURSO']);
              for($j=0; $j<$total; $j++)	{
                $curso_atual = $conteudo[$t]['CURSO'][$j]['ID_CURSO']; // pega a id do curso

                // se o curso ainda no tiver sido escolhido em outros grupos
                if(!in_array($curso_atual, $curso_escolhido)) {
                  // verifica se o curso j est selecionado
                  $curso_salvo = $grupo->cursos();
                  // se haver algum curso
                  if (!empty($curso_salvo)) {
                    if (in_array($curso_atual, $curso_salvo)) echo "<input class='checkbox' type='checkbox' checked='checked' name='acurso[".$curso_atual."]' value='1'>&nbsp;&nbsp;".ObterNomeCurso($curso_atual)."<br>\n";
                    else echo "<input class='checkbox' type='checkbox' name='ncurso[".$curso_atual."]' value='1'>&nbsp;&nbsp;".ObterNomeCurso($curso_atual)."<br>\n";
                  } else {
                    echo "<input class='checkbox' type='checkbox' name='ncurso[".$curso_atual."]' value='1'>&nbsp;&nbsp;".ObterNomeCurso($curso_atual)."<br>\n";
                  }
                } else {
                  // caso j tenha sido escolhido, deixa ele desabilitado para escolha
                  echo "<input class='checkbox' type='checkbox' disabled='disabled' />&nbsp;&nbsp;".ObterNomeCurso($curso_atual)." (".A_LANG_AVS_LABEL_ANOTHER_GROUP.")<br>\n";
                }
              }
            ?>					
				</div>              
				<div class="funcao">
					<input name="voltar" type="button" value="<?php echo A_LANG_AVS_BUTTON_BACK; ?>" class="button" onClick="javascript:window.location='<?php echo "a_index.php?opcao=TopicoAvaliacao&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina" ?>'" />
					<input type="button" name="ajuda" value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onclick="<?php echo HELP_GRUPO ?>" class="button" />
					<input name="enviar" type="submit" id="save" value="<?php echo A_LANG_AVS_BUTTON_SAVE; ?>" class="button" />
				</div>
      </form>
    </td>
  </tr>
</table>
<?php
  $conn->Close();
?>
