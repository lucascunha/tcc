<?php
/**
 * Descri��o
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <24/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/questao.class.php";
	require_once "avaliacao/class/dissertativa.class.php";
	require_once "avaliacao/class/multipla_escolha.class.php";
	require_once "avaliacao/class/verdadeiro_falso.class.php";
	require_once "avaliacao/class/lacunas.class.php";	
	require_once "avaliacao/funcao/ajuste.func.php";
	require_once "avaliacao/class/erro.class.php";
	require_once "avaliacao/funcao/ajuste.func.php";
	require_once "avaliacao/class/liberar.class.php";
	require_once "avaliacao/class/resposta.class.php";
	require_once "avaliacao/funcao/calculo_media.func.php";

	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	
	$id_libera = isset($_POST['avaliacao']) ? $_POST['avaliacao'] : FALSE;
	
	if(!$CodigoDisciplina) {
		echo A_LANG_AVS_ERROR_PARAM;
		return;
	}	

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO; 
		return;
	}	

	$erro = new Erro();	
	$erro_vetor = new Erro();
	
	if(isset($_POST['enviar'])) {
		if(!$id_libera) $erro->adicionar_erro(A_LANG_AVS_ERROR_PARAM);		
		$libera = new Liberar($conn, $id_libera);	
		
		$libera->set_nota_cancelada(0);
		
		$total=0;
		// modifica os pesos
		foreach($_POST['questao'] as $k => $v) {
			$libera->modificar_peso($k,$v);
			$total += $v;
		}
		
		if($total == 100) {
			if(!$libera->salvar()) $erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE);
		} else {
			$erro->$erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE);
		}
			
	} elseif (isset($_POST['default'])) {
		if(!$id_libera) $erro->adicionar_erro(A_LANG_AVS_ERROR_PARAM);		
		$libera = new Liberar($conn, $id_libera);	
		
		$libera->peso_default();
		if(!$libera->salvar()) $erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE);		

	} elseif (isset($_POST['modificado'])) {
		// Coloca os notas para o peso modificado
		if(!$id_libera) $erro->adicionar_erro(A_LANG_AVS_ERROR_PARAM);		
		$libera = new Liberar($conn, $id_libera);	
		
		$libera->peso_modificado();
		if(!$libera->salvar()) $erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE);
	}
	
	// atualiza as notas dos alunos
	if(isset($libera) && $erro->quantidade_erro() == 0 ) {
		$alunos = $libera->alunos();
		foreach($alunos as $k) {
			$resposta = new Resposta($conn, $k['id']);
			$resposta->corrigir_questao(TRUE);
			$resposta->calcular_nota();
			if(!$resposta->salvar()) $erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE);
			if(!calcular_media($conn, $k['aluno'])) $erro->adicionar_erro(A_LANG_AVS_ERROR_AVERAGE_CALCULE);		
		}		
	}
	
	$vetor = ajuste_questao_listar_libera($conn, $CodigoDisciplina);
	if(count($vetor) == 0) $erro_vetor->adicionar_erro(A_LANG_AVS_ERROR_FUNCTION_EVALUATION_LIBERATE);

	montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_AJUSTE);

?>

<script language="javascript" type="text/javascript" src="avaliacao/ajax/ajax.js"></script>
<script language="JavaScript" type="text/javascript" src="avaliacao/js/geral.js"></script>
<script language="JavaScript" type="text/javascript" src="avaliacao/js/media.js"></script>
<script language="javascript" type="text/javascript">

	function listar_questao(avaliacao) {
		var ajax = new AJAX();
		var disc = <?php echo $CodigoDisciplina; ?>;
		var div = document.getElementById("conteudo");
		div.className='lista_aluno_load';
		div.innerHTML = "Carregando <img src='imagens/carrega.gif' border='0'>";
		ajax.Texto("avaliacao/ajax/ajax_anular_peso.php?avaliacao="+avaliacao+"&disciplina="+<?php echo $CodigoDisciplina; ?>,div);	
	}		
</script>
<form action="<?php echo "a_index.php?opcao=FuncaoAjustarAnularPeso&CodigoDisciplina=$CodigoDisciplina"; ?>" method="post" name="nota" id="frm">
	<table cellspacing="1" cellpadding="1" width="100%" border="0" bgcolor="<?php echo $A_COR_FUNDO_ORELHA_ON ?>">
		<tr>
			<td>
				<div class="funcao">			
				<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_ADJUSTMENT_QUESTION_WEIGHT; ?></div>	
				<br />
				<?php
					if($erro_vetor->quantidade_erro() > 0) {
						echo "<div class='erro'>\n";
						echo $erro_vetor->imprimir_erro();
						echo "</div>";
					} else {			
						// mostra os erros ou acertos
						if($erro->quantidade_erro() > 0) {
							echo "<div class='erro'>\n";
							echo $erro->imprimir_erro();
							echo "</div>";
						}	elseif(isset($_POST['enviar'])) {
							echo "<div class='sucesso'>\n";
							echo "<p><span>".A_LANG_AVS_SUCESS."</span></p>";
							echo "</div>";					
						}
					}	 				
				
					// a parte de tratamento dos erros							
					if($erro_vetor->quantidade_erro() == 0) {					
						echo "<label>".A_LANG_AVS_LABEL_CHOOSE_EVALUATION." </label>&nbsp;\n";
						echo "<select name='avaliacao' onchange='listar_questao(this.value)' style='width:300px'>\n";
						if(empty($_REQUEST['avaliacao'])) echo "<option value='' disabled='disabled' selected='selected'>".A_LANG_AVS_COMBO_SELECT."</option>";
						else echo "<option value='' disabled='disabled' >".A_LANG_AVS_COMBO_SELECT."</option>";
						foreach ($vetor as $k) {						
							if(!empty($_REQUEST['avaliacao'])) {
								if($_REQUEST['avaliacao'] == $conceito['libera']) echo "<option value='".$k['libera']."' selected='selected'>".A_LANG_AVS_LABEL_EVALUATION.": ".$k['libera']." - ".quote($k['descricao']).". ".A_LANG_AVS_COMBO_DATE.": ".formata_data($k['data_liberar'])."</option>\n";
								else echo "<option value='".$k['libera']."'>".A_LANG_AVS_LABEL_EVALUATION.": ".$k['libera']." - ".quote($k['descricao']).". ".A_LANG_AVS_COMBO_DATE.": ".formata_data($k['data_liberar'])."</option>\n";
							} else {
								echo "<option value='".$k['libera']."'>".A_LANG_AVS_LABEL_EVALUATION.": ".$k['libera']." - ".quote($k['descricao']).". ".A_LANG_AVS_COMBO_DATE.": ".formata_data($k['data_liberar'])."</option>\n";
							}								
						}
						echo "</select>\n";
					} elseif ($erro->quantidade_erro() > 0) {
						echo "<div class='erro' id='info'>\n";
						echo $erro->imprimir_erro();
						echo "</div>\n";
					}	        
				?>

				</div>		
			</td>
		</tr>	
		<tr><td><div id="conteudo"></div></td></tr>
		<tr>
			<td>
				<div class="funcao">
					<input type="button" name="voltar" value="<?php echo A_LANG_AVS_BUTTON_BACK; ?>" class="button" onclick="javascript:window.location='a_index.php?opcao=FuncaoAjuste&CodigoDisciplina=<?php echo $CodigoDisciplina; ?>'">
					<input name='ajuda' type='button' class='button' value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onClick="<?php echo HELP_FUNCTION_ADJUSTMENT_CANCEL; ?>" />
				</div>
			</td>
		</tr>	
	</table>
	<?php
		if(isset($_REQUEST['avaliacao'])) {
			echo "<script>listar_questao(".$_REQUEST['avaliacao'].")</script>";
		}
	?>
</form>