<?php

  require_once "avaliacao/funcao/geral.func.php";
	require_once "avaliacao/ajuda.const.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/avaliacao.class.php";
	require_once "avaliacao/questao.const.php";

  $numtopico = isset($_GET['numtopico']) ? $_GET['numtopico'] : FALSE;
	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	
	if(!$numtopico || !$CodigoDisciplina) {
		echo(A_LANG_AVS_ERROR_PARAM);
		return;
	}	

	 // abre conexo com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURANA, NO DEIXA OUTROS PROFESSORES ACESSAREM INFORMAES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo(A_LANG_AVS_ERROR_VERIFY_AUTHO);
		return;
	}	

  montar_orelha_completa_avaliacao($numtopico, $CodigoDisciplina);

?>
<script language="JavaScript" type="text/javascript">
  function remover(id) {
    var confirma = confirm("<?php echo A_LANG_AVS_JS_ALERT_EVALUATION_DELETE_CONFIRM; ?>");
    if(confirma) {
      window.open("avaliacao/remove_aval.php?aval="+id, "remove");
    }
  }
	function visualizar(id) 
	{
    window.open("avaliacao/avs_visualizar_aval.php?aval="+id+"&CodigoDisciplina=<?php echo $CodigoDisciplina; ?>", "visualizar_avaliacao");
	}
</script>

<table cellspacing='0' cellpadding='0' width='100%' border='0' bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>' style="height:380px;">
  <tr>
    <td>
		  <div class='funcao'>
				<span><strong><?php echo A_LANG_AVS_LABEL_FUNCTION; ?>:</strong></span><p></p>
				<input type="button" value="<?php echo A_LANG_AVS_BUTTON_GROUP_NEW; ?>" class="buttonBig" name="novogrupo" onClick="javascript:window.location='<?php echo "a_index.php?opcao=EditarGrupo&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina" ?>'">
				<input type="button" name="ajuda" value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onclick="<?php echo HELP_TOPICO ?>" class="buttonBig" >				
  		</div>
      <?php
        // listar todos os grupos de avaliao para o tpico da disciplina
        $query = "SELECT id_grupo_aval AS grupo FROM grupo_aval WHERE id_disc = $CodigoDisciplina AND topico = $numtopico";
				
        $rs = $conn->Execute($query);

        if($rs) {
          while ($array = $rs->FetchRow()) {

            // abre um objeto grupo
            $grupo = new Grupo($conn, $array['grupo']);

            // monta a tabela para o grupo
            echo "<fieldset class='field'>\n";
            echo "<legend style='background-color:".$A_COR_FUNDO_ORELHA_ON."'>".A_LANG_AVS_LABEL_GROUP_DESCRIPTION." ".$numtopico."</legend>\n";
						
	          $string = "";
            while ($curso = $grupo->buscar_curso()) {
              //print_r($curso);
              $peso = $curso['peso'] ? $curso['peso']."%" : A_LANG_AVS_LABEL_GROUP_NOT_WEIGHT;
              $string .= ObterNomeCurso($curso['curso'])." (".$peso.")  -  ";
            }
            $string = substr_replace($string,"",-3,1); // retira o "-" do final da string
						
						echo "<div class='parametro'>\n";						
						echo "<p><span><strong>".A_LANG_AVS_LABEL_GROUP_PARAM."</strong></span></p>\n";
						
						echo "<table border='0' cellpadding='5'>\n";
						if( $grupo->get_descricao() ) {
							echo "<tr>";
							echo "<td>".A_LANG_AVS_LABEL_DESCRIPTION.":</td>";
							echo "<td>".$grupo->get_descricao()."</td>";
							echo "</tr>";
						}
						echo "<tr>";
						echo "<td>".A_LANG_AVS_LABEL_GROUP_COURSE_WEIGHT."</td><td>".$string."</td>";
						echo "</tr>";
						echo "</table>\n";
						echo "<br />";								
						echo "<div class='avaliacao'>\n";  // inicio da div para as avaliaes
            // se haver avaliaes junto aos grupos
            if($grupo->numero_avaliacao() > 0) {
						  echo "<p><span><strong>".A_LANG_AVS_LABEL_EVALUATION_MODEL.":</strong></span></p>\n";							

							// Informações sobre a avaliação presencial
							if($grupo->get_presencial()) {
                $aval = new Avaliacao($conn, $grupo->get_presencial());						
								echo "<table width='100%' border='0'>\n";
								echo "<caption>".A_LANG_AVS_LABEL_EVALUATION_MODEL_PRESENTIAL."</caption>";
								echo "<tr align='center' class='Titulo'>\n";
								echo "<td width='680' rowspan='2'>".A_LANG_AVS_TABLE_DESCRIPTION."</td>\n";
								echo "<td colspan='2'>".A_LANG_AVS_TABLE_DATE."</td>\n";
								echo "<td rowspan='2' width='80'>".A_LANG_AVS_TABLE_ACTION."</td>\n";
								echo "</tr>\n";
								echo "<tr align='center' class='Titulo'>\n";
								echo "<td width='80' align='center'>".A_LANG_AVS_TABLE_DATE_CREATE."</td>\n";
								echo "<td width='80' align='center'>".A_LANG_AVS_TABLE_DATE_MODIFY."</td>\n";
								echo "</tr>\n";

								$classe = "zebraA";
								echo "<tr align='center' class='".$classe."'>\n";
								echo "<td align='left'>".$aval->get_descricao()."</td>\n";
								echo "<td>".$aval->get_data_cadastro()."</td>\n";
								echo "<td>".$aval->get_data_alteracao()."</td>\n";
								echo "<td><a href='a_index.php?opcao=NovaAvalPresencial&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina&grupo=".$grupo->get_grupo()."&avaliacao=".$aval->get_avaliacao()."'><img border='0' src='imagens/editar.png' title='".A_LANG_AVS_HINTS_EVALUATION_EDIT."'></a></td>\n";
								echo "</tr>\n";
								echo "</table>";				
							} 
							
							echo "<br /><br />";
							
							if($grupo->numero_avaliacao() > ($grupo->get_presencial() ? 1 : 0 )) {
								// cabeçalho das avaliações não presenciais							
								echo "<table width='100%' border='0'>\n"; 			
								echo "<caption>".A_LANG_AVS_LABEL_EVALUATION_MODEL_AUTO."</caption>";			
								echo "<tr align='center' class='Titulo'>\n";
								echo "<td width='510' rowspan='2'>".A_LANG_AVS_TABLE_DESCRIPTION."</td>\n";
								echo "<td width='80' rowspan='2'>".A_LANG_AVS_TABLE_DIVULGE."</td>\n";
								echo "<td width='90' rowspan='2'>".A_LANG_AVS_TABLE_NUMBER_QUESTION."</td>\n";
								echo "<td colspan='2'>".A_LANG_AVS_TABLE_DATE."</td>\n";
								echo "<td rowspan='2' colspan='3' width='80'>".A_LANG_AVS_TABLE_ACTION."</td>\n";
								echo "</tr>\n";
								echo "<tr align='center' class='Titulo'>\n";
								echo "<td width='80' align='center'>".A_LANG_AVS_TABLE_DATE_CREATE."</td>\n";
								echo "<td width='80' align='center'>".A_LANG_AVS_TABLE_DATE_MODIFY."</td>\n";
								echo "</tr>\n";								
	
								$classe = "zebraA";	
								// informaes sobre as avaliações não presenciais
								while ($id_aval = $grupo->buscar_avaliacao()) {
									// imprime apenas as avaliações não presenciais
									if($grupo->get_presencial() != $id_aval) {		
										$aval = new Avaliacao($conn, $id_aval);						
										echo "<tr align='center' class='".$classe."'>\n";
										echo "<td align='left'>".$aval->get_descricao()."</td>\n";
										echo "<td>". $w = $aval->get_divulgacao() == 1 ? A_LANG_AVS_TABLE_DIVULGE_AUTO : A_LANG_AVS_TABLE_DIVULGE_MANUAL ."</td>\n";
										echo "<td>".$aval->numero_questao()."</td>\n";
										echo "<td>".$aval->get_data_cadastro()."</td>\n";
										echo "<td>".$aval->get_data_alteracao()."</td>\n";
										echo "<td><a href='a_index.php?opcao=NovaAval&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina&grupo=".$grupo->get_grupo()."&avaliacao=".$aval->get_avaliacao()."'><img border='0' src='imagens/editar.png' title='".A_LANG_AVS_HINTS_EVALUATION_EDIT."' /></a></td>\n";
										echo "<td><a href='javascript:remover(\"".$aval->get_avaliacao()."\")'><img border='0' src='imagens/deletar.gif' title='".A_LANG_AVS_HINTS_EVALUATION_DELETE."' /></a></td>\n";
										echo "<td><a href='javascript:visualizar(\"".$aval->get_avaliacao()."\")'><img border='0' src='imagens/visualizar.png' title='".A_LANG_AVS_HINTS_EVALUATION_VIEW."' /></a></td>\n";	
										echo "</tr>\n";
										$classe = $classe == "zebraA" ? "zebraB" : "zebraA";
									}
								}							
								echo "</table>";							
							}
            } else {
              echo "<p><span><strong>".A_LANG_AVS_LABEL_GROUP_NOT_EVALUATION."</strong></span></p>\n";
            }						
						echo "</div>\n"; // fim da div de avaliao

						// div de funes
						echo "<div class='funcao'>";
						echo "	<p><span><strong>".A_LANG_AVS_LABEL_FUNCTION.":</strong></span></p>\n";
							// botao adicionar avalição						
						echo "<p>";
						echo "<input name='param' type='submit' id='param' value='".A_LANG_AVS_BUTTON_GROUP_PARAM."' class='button' onclick=\"window.location='a_index.php?opcao=GrupoParam&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina&grupo=".$grupo->get_grupo()."'\" />\n";
						echo "<input name='sub_aval' type='submit' id='sub_aval' value='".A_LANG_AVS_BUTTON_GROUP_NEW_EVALUATION."' class='button' onclick=\"window.location='a_index.php?opcao=NovaAval&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina&grupo=".$grupo->get_grupo()."'\" />\n";
						echo "<input name='duplica' type='submit' id='duplicaaval' value='".A_LANG_AVS_BUTTON_DUPLICATE_AVAL."' class='button' onclick=\"window.location='a_index.php?opcao=DuplicarAvaliacao&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina&grupo=".$grupo->get_grupo()."'\" />\n";
	
						if($grupo->get_presencial() == FALSE) {
							echo "<input name='aval_presencial' type='submit' id='aval_presencial' value='".A_LANG_AVS_BUTTON_AVAL_PRESEN."' class='button' onclick=\"window.location='a_index.php?opcao=NovaAvalPresencial&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina&grupo=".$grupo->get_grupo()."'\" />\n";						
						}
						
						echo "</p>";
						echo "</div>\n";

						echo "</div>\n"; // fim da div do parametro				
            echo "</fieldset>\n";
						echo "<br />";
          }
        }
      ?>
    </td>
  </tr>
	<tr><td>&nbsp;</td></tr>
</table>
<?php
  $conn->Close();
?>
