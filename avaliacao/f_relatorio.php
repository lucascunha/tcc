<?php
/**
 * Faz o ajuste de pesos para o calculo da m�dia
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
	* @subpackage Media
 * @version 1.0 <23/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */
  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/questao.class.php";
  require_once "avaliacao/class/dissertativa.class.php";
  require_once "avaliacao/class/multipla_escolha.class.php";
  require_once "avaliacao/class/lacunas.class.php";
  require_once "avaliacao/class/verdadeiro_falso.class.php";
	require_once "avaliacao/class/erro.class.php";
	require_once "avaliacao/funcao/relatorio.func.php";

	$CodigoDisciplina = $_GET['CodigoDisciplina'];

  montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_RELATORIO);


?>

<table cellspacing='1' cellpadding='1' width='100%'  border='0'  bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
  <tr>
    <td>
		<div class='funcao'>
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_REPORT; ?></div>			
			<br />
			
			<p><span><a href="javascript:window.location='a_index.php?opcao=FuncaoRelatorioAval&CodigoDisciplina=<?php echo $CodigoDisciplina; ?>'">Relat&oacute;rio sobre avalia&ccedil;&otilde;es realizadas</a></span></p>
			<p><span><a href="javascript:window.location='a_index.php?opcao=FuncaoRelatorioAluno&CodigoDisciplina=<?php echo $CodigoDisciplina; ?>'">Relat&oacute;rio sobre alunos</a></span></p>
			
		</div>		
		<div class="funcao"><input name='ajuda' type='button' class='button' value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onClick="<?php echo HELP_FUNCTION_REPORT; ?>" /></div>
    </td>
  </tr>
</table>