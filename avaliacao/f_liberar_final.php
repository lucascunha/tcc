<?php
/**
 * Faz o ajuste de pesos para o calculo da m�dia
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
	* @subpackage Media
 * @version 1.0 <23/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/geral.func.php";	
	require_once "avaliacao/class/avaliacao.class.php";
	require_once "avaliacao/class/liberar.class.php";
	require_once "avaliacao/class/erro.class.php";
	require_once "avaliacao/funcao/liberar.func.php";

	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	$avaliacao = isset($_GET['aval']) ? $_GET['aval'] : FALSE;
	$aluno = isset($_POST['aluno']) ? $_POST['aluno'] : FALSE;
	
	if(!$CodigoDisciplina || !$avaliacao || !$aluno) {		
		echo A_LANG_AVS_ERROR_PARAM;
		return;
	}	

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO;
		return;
	}	
	
	$erro = new Erro();
	
	// verificar se a avalia�ao liberada nao � presencial, e j� foi liberada
	$liberada = liberar_buscar_avaliacao($conn, $avaliacao);
	
	if($liberada) {
		$lib = new Liberar($conn, $liberada);
		$lib->cancela_divulgacao();
		// percorre o vetor e busca o identificar do aluno, se nao houver � adicionado a avalia�ao
		foreach($aluno as $k) {
			$str = explode("_",$k); // $str[0] = id_usuario; $str[1] = id_curso
			if($lib->get_identificador_aluno($str[0], $str[1], $CodigoDisciplina) === FALSE) {
				$lib->aluno_adicionar($str[0], $str[1], $CodigoDisciplina);
			}
		}	
	} else {	
		$lib = new Liberar($conn);
		
		$lib->set_avaliacao($avaliacao);
		$lib->liberar_avaliacao(LIBERADA);
		
		// adicionar os alunos na libera��o da prova	
		foreach ($aluno as $k) {
			$str = explode("_",$k); // $str[0] = id_usuario; $str[1] = id_curso
			$lib->aluno_adicionar($str[0], $str[1], $CodigoDisciplina);
		}	
	}	
	
	if(!$lib->salvar()) $erro->adicionar_erro(A_LANG_AVS_ERROR_LIBERATE_EVALUATION);
	
	// limpa a base de dados se haver avalia�oes liberadas sem alunos
	liberar_limpar_dados($conn, $CodigoDisciplina);

  montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_LIBERAR);	
?>
<table cellspacing="1" cellpadding="1" width="100%" border ="0" bgcolor="<? echo $A_COR_FUNDO_ORELHA_ON ?>">
  <tr>
    <td>
			<div class="funcao">
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_LIBERATE; ?> (4/4)</div>
			<?php 
          if ($erro->quantidade_erro() == 0) {
            echo "<div class='sucesso' id='info'>\n";
            echo "<p><span>".A_LANG_AVS_SUCESS_LIBERATE."</span></p>\n";
						echo "<br />\n";
						echo "<p><span><strong>".$lib->get_id()."</strong>: ".A_LANG_AVS_TEXT_LIBERATE."</span></p>\n";
            echo "</div>\n";
          } else {
            echo "<div class='erro' id='info'>\n";
            echo $erro->imprimir_erro();
            echo "</div>\n";
          }
			?>
			</div>
			<div class="funcao">
				<input name="volta" type="button" class="button" value="<?php echo A_LANG_AVS_BUTTON_BACK_LIBERATE; ?>" onClick="javascript:window.location='a_index.php?opcao=FuncaoLiberar&CodigoDisciplina=<?php echo $CodigoDisciplina; ?>'">			
			</div>
    </td>
  </tr>
</table>