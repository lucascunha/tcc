<?php
/**
 * Faz o ajuste de pesos para o calculo da m�dia
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
  * @subpackage Media
 * @version 1.0 <23/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */
  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/funcao/divulgar.func.php";
  require_once "avaliacao/class/erro.class.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/liberar.class.php";
  require_once "avaliacao/class/resposta.class.php";


	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	
	if(!$CodigoDisciplina) {
		echo A_LANG_AVS_ERROR_PARAM;
		return;
	}	

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO;
		return;
	}	

  // faz a grava��o dos dados
  if(isset($_POST['enviar'])) {
		
    $erro = new Erro();

    $libera = new Liberar($conn, $_POST['libera']);
    if($libera->divulga_nota()) {
			if(!$libera->salvar()) $erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE);		
    } else {
      $erro->adicionar_erro(A_LANG_AVS_ERROR_DIVULGE_VALUE);
    }
  }

	$erro_vetor = new Erro();
  $vet_avaliacao = divulgar_lista_avaliacao($conn, $CodigoDisciplina);
	if(count($vet_avaliacao) == 0) $erro_vetor->adicionar_erro(A_LANG_AVS_ERROR_DIVULGE_NOT_EVALUATION);
 
  montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_DIVULGAR);
?>
<script language="JavaScript" type="text/javascript" src="avaliacao/ajax/ajax.js"></script>
<script language="JavaScript" type="text/javascript">
  function carregar (libera) {
    var ajax = new AJAX();
    var div = document.getElementById("conteudo");
    div.innerHTML = "Carregando <img src='imagens/carrega.gif' border='0'>";
    ajax.Texto("avaliacao/ajax/ajax_divulgar_nota.php?libera="+libera+"&disciplina="+<?php echo $CodigoDisciplina ?>, div);
  }
</script>
<table cellspacing='1' cellpadding='1' width='100%' border='0' bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
	<tr>
		<td>
			<div class="funcao">
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_DIVULGE; ?></div>
      <?php
        // imprime o erro ou o sucesso em salvar
        if(isset($_POST['enviar'])) {
          if ($erro->quantidade_erro() == 0) {
            echo "<div class='sucesso' id='info'>\n";
            echo "<p><span>".A_LANG_AVS_SUCESS_DIVULGE."</span></p>\n";
            echo "</div>\n";
          } else {
            echo "<div class='erro' id='info'>\n";
            echo $erro->imprimir_erro();
            echo "</div>\n";
          }
        }
				
				// mostra mensagem caso n�o haja mais avalia��es com notas a serem liberadas
        if($erro_vetor->quantidade_erro() == 0) {
          echo "<label>".A_LANG_AVS_LABEL_CHOOSE_EVALUATION.": &nbsp;&nbsp;</label>\n";
					echo "<select id='avaliacao' class='button' onchange='carregar(this.value)' style='width:300px;'>\n";
          if(isset($_POST['libera'])) echo "<option value='' disabled='disabled' >".A_LANG_AVS_COMBO_SELECT."</option>\n";
          else echo "<option value='' disabled='disabled' selected='selected' >".A_LANG_AVS_COMBO_SELECT."</option>\n";
          // impressao das avalia��es
          foreach ($vet_avaliacao as $k) {
						$presencial = $k['presencial'] == 0 ? "" : " - (Presencial)";
            if(isset($_POST['libera']) && $_POST['libera'] == $k['id']) {
							echo "<option value='".$k['libera']."' selected='selected'>".A_LANG_AVS_LABEL_EVALUATION.": ".$k['libera']." - ".quote($k['descricao']).$presencial.". ".A_LANG_AVS_COMBO_DATE.": ".formata_data($k['data'])."</option>\n";
						} else 	 {
							echo "<option value='".$k['libera']."'>".A_LANG_AVS_LABEL_EVALUATION.": ".$k['libera']." - ".quote($k['descricao']).$presencial.". ".A_LANG_AVS_COMBO_DATE.": ".formata_data($k['data'])."</option>\n";
						}
          }
      		echo "</select>\n";
        } elseif ($erro_vetor->quantidade_erro() > 0) {
          echo "<div class='erro' id='info'>\n";
          echo $erro_vetor->imprimir_erro();
          echo "</div>\n";
        }
      ?>			
			</div>
		</td>
	</tr>
  <tr>
    <td>
			<div id='conteudo'></div>
		</td>
  </tr>
	<tr>
		<td>
			<div class="funcao">
				<input name='ajuda' type='button' class='button' value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onclick="<?php echo HELP_FUNCTION_DIVULGATE; ?>" />
			</div>
		</td>
	</tr>
</table>

