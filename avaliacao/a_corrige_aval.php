<?php
/**
 * Script para a corre��o da avalia��o
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <16/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

  @session_start();
  require_once "../include/adodb/adodb.inc.php";
  require_once "../config/configuracoes.php";
  require_once "funcao/geral.func.php";
	require_once "funcao/calculo_media.func.php";
  require_once "questao.const.php";
  require_once "class/avaliacao.class.php";
  require_once "class/grupo.class.php";
  require_once "class/questao.class.php";
  require_once "class/dissertativa.class.php";
  require_once "class/multipla_escolha.class.php";
  require_once "class/lacunas.class.php";
  require_once "class/verdadeiro_falso.class.php";
  require_once "funcao/aluno.func.php";
  require_once "class/erro.class.php";
  require_once "class/liberar.class.php";
  require_once "class/resposta.class.php";

	// Troca de idioma
  if ($A_LANG_IDIOMA_USER == "")
		include "../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../idioma/".$A_LANG_IDIOMA_USER."/geral.php";

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

  $erro = new Erro(); // cria o objeto de erros

  // pega as vari�veis necess�rias para o funcionamento do programa
  $id_libera = isset($_POST['libera']) ? (int) $_POST['libera'] : $erro->adicionar_erro(A_LANG_AVS_ERROR_EVALUATION_LIBERATE);
	$id_libera_aluno = isset($_POST['libera_aluno']) ? (int) $_POST['libera_aluno'] : $erro->adicionar_erro(A_LANG_AVS_ERROR_EVALUATION_LIBERATE);
  $aluno = isset($_SESSION['id_aluno']) ? $_SESSION['id_aluno'] : $erro->adicionar_erro(A_LANG_AVS_ERROR_EVALUATION_LOGGED);

  if($erro->quantidade_erro() == 0 ) {
		$resposta = new Resposta($conn, $id_libera_aluno);
  }

  // ******* VERIFICA��ES QUE DEVEM SER FEITAS ********************* //

  // verificar a PHPIDSession
  if($erro->quantidade_erro() == 0) {  
    if(verificar_sessao($conn, $aluno) == FALSE) $erro->adicionar_erro(A_LANG_AVS_ERROR_SESSION_BROWSER);
  }

  // verifica se j� foi entregue e coloca a libera��o como entregue
  if($erro->quantidade_erro() == 0) {
    if($resposta->get_status_aluno($id_libera_aluno) == ENTREGUE) {
      $erro->adicionar_erro(A_LANG_AVS_ERROR_EVALUATION_FINISH);
    } else {
      $resposta->aluno_alterar($id_libera_aluno, ENTREGUE);
    }
  }

  // verifica se a liberacao como 1
  if($erro->quantidade_erro() == 0) {
    if($resposta->get_status() == ENCERRADA) {
      $erro->adicionar_erro(A_LANG_AVS_ERROR_EVALUATION_FINISH2);
      // adicionar a nota 0 ao aluno
      $resposta->zerar_avaliacao();
    }
  }

  // ******* FIM DAS VERIFICA��ES ******************************** //
  // ******* IN�CIO DAS CORRE��ES ******************************** //
  if($erro->quantidade_erro() == 0) {

    // cria o objeto que ir� colocar as respostas na base de dados
    // corre��o das verdadeiro-falso
    // $k = id_questao, $value = resposta
    if(isset($_POST[QUESTAO_VF])) {
      foreach ($_POST[QUESTAO_VF] as $k => $value) {
        $resposta->adicionar_resposta($k, $value);
      }
    }

    if(isset($_POST[QUESTAO_ME])) {
      foreach($_POST[QUESTAO_ME] as $k => $value) {
        $resposta->adicionar_resposta($k, $value);
      }
    }

    if(isset($_POST[QUESTAO_LAC])) {
      foreach($_POST[QUESTAO_LAC] as $k => $value) {
        $resposta->adicionar_resposta($k, $value);								
      }
    }

    // esta fica gravada dentro da base apenas esperando o professor atribuir nota
    // VERIFICAR COMO SER� GRAVADO NA BASE DE DADOS

    if(isset($_POST[QUESTAO_DISSERT])) {
      foreach($_POST[QUESTAO_DISSERT] as $k => $value) {
        $resposta->adicionar_resposta($k, $value);
      }
    }
		
		// faz a corre��o das quest�es
		$resposta->corrigir_questao();
		
		// calcula a nota
		if(!$resposta->get_presencial()) {
			$resposta->calcular_nota();
		}

		$resposta->divulga_nota(); 

	  // salva as quest�es
    if(!$resposta->salvar()) $erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE);
		
	
		// calcular a m�dia final
		if(!calcular_media($conn, $aluno)) $erro->adicionar_erro(A_LANG_AVS_ERROR_AVERAGE_CALCULE);
		
  }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Entregua da Avalia��o</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="css/ff.css">
<!--[if lte IE 6]>
	<link href="avaliacao/css/ie.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->
</head>

<body>
<?php
  if($erro->quantidade_erro() > 0) {
    echo "<div class='erro'>";		
		echo $erro->imprimir_erro();
		echo "</div>";
  } else {
		echo "<div class='sucesso'>";
    echo "<p><span>".A_LANG_AVS_SUCESS_EVALUATION."</span></p>";
		echo "</div>";
	}	
?>
<div align="center">
<input type="button" name="fechar" value="<?php echo A_LANG_AVS_BUTTON_CLOSE ?>" onclick="javascript:window.close()" class="button" />
</div>
</body>
</html>
<?php
  $conn->Close();
?>