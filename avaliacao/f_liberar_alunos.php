<?php
/**
 * Seleciona os alunos que ter�o a avalia��o liberada para resolu��o
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <24/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

  require_once "avaliacao/funcao/geral.func.php";
	require_once "avaliacao/funcao/liberar.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/questao.class.php";
	require_once "avaliacao/class/erro.class.php";
  require_once "avaliacao/questao.const.php";

  $grupo = isset($_REQUEST['grupo']) ? $_REQUEST['grupo'] : FALSE;
  $avaliacao = isset($_REQUEST['avaliacao']) ? $_REQUEST['avaliacao'] : FALSE;
	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	
	if(!$CodigoDisciplina || !$grupo || !$avaliacao) {
		echo A_LANG_AVS_ERROR_PARAM;
		return;
	}	

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO;
		return;
	}	

	$erro = new Erro();

  // seleciona todos os alunos com matricula liberada, que sejam do curso que a avaliacao escolhida pode ser aplicada
  $query = "SELECT id_usuario AS id, nome_usuario AS nome, nome_curso AS curso, id_curso FROM (matricula NATURAL JOIN usuario) LEFT JOIN curso USING (id_curso) WHERE status_mat = '1' ".
           "AND id_curso IN (SELECT id_curso FROM grupo_aval NATURAL JOIN grupo_curso WHERE id_disc = $CodigoDisciplina AND id_grupo_aval= $grupo) ORDER BY nome ASC";

	echo $query;

  $rs = $conn->Execute($query);

  $error = FALSE;
  if($rs->NumRows() == 0) $erro->adicionar_erro(A_LANG_AVS_ERROR_LIBERATE_NOT_STUDENTS);

  // monta a matriz de curso x alunos para utilizar no javascript
  $cursos = array();

  while ($array = $rs->FetchRow()) {
    $cursos[$array['id_curso']] = array();
  }
  $rs->MoveFirst();
  while ($array = $rs->FetchRow()) {
    array_push($cursos[$array['id_curso']], $array['id']."_".$array['id_curso']);
  }

  // listar os alunos que j� realizaram a avalia��o. Lembrando que os alunos que j� realizaram a avalia��o e forem
  // escolhidos novamente ter�o suas notas apagadas.
  $alunos_avaliados = liberar_listar_alunos_avaliados($conn, $grupo);
//	if(count($alunos_avaliados) == 0) $erro->adicionar_erro(A_LANG_AVS_ERROR_NOT_STUDENTS_LIBERATE);

	montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_LIBERAR);	
//	print_r($alunos_avaliados);
?>
<script language="javascript" type="text/javascript">

  <?php
    // monta o array em javascript para os cursos dispon�veis, para ser utilizados pelo combobox
    if ($rs->NumRows() > 0) {
      echo "var curso = new Array(".$rs->NumRows().");\n";
      foreach ($cursos as $k => $value) {
        // cria o vetor
        echo "curso[\"".$k."\"] = new Array();\n";
        for ($x=0; $x < count($value); $x++) {
          //adiciona os elementos
          echo "curso[\"".$k."\"][".$x."] = '".$value[$x]."';\n";
        }
      }
    } else {
      echo "var curso = new Array();\n";
    }
  ?>


  function acao_curso() {
    var sel_curso = document.getElementById('selecao');
    var sc = document.getElementById('selecao_curso');
    selecao = sc.value.substr(0,1) == 'm' ? true : false;
    value = sc.value.substr(2);

    for(y=0; y<curso[value].length;y++) {
      for(x=0; x<sel_curso.options.length;x++) {
         if(sel_curso.options[x].value == curso[value][y])
           sel_curso.options[x].selected = selecao;
      }
    }
  }

  // true para selecionar, false para deselecionar todos
  function acao_itens(selecao) {
    var sel = document.getElementById('selecao');
    for(var x=0; x < sel.options.length; x++) {
      sel.options[x].selected = selecao;
    }
  }

	function enviar_formulario() {
		var s = document.getElementById('selecao');		
		if(s.value == "") {	
			alert("<?php echo A_LANG_AVS_JS_ALERT_STUDENTS; ?>");
			return false;
		}
		return true;	
	}

</script>
<table cellspacing='1' cellpadding='1' width='100%'  border='0' bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
  <tr>
		<td>
			<div class="funcao">
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_LIBERATE; ?> (3/4)</div>
			<?php
				if($erro->quantidade_erro() > 0) {
					echo "<div class='erro'>";
					echo $erro->imprimir_erro();
					echo "</dvi>";				
				} else {
					echo "<p><span>".A_LANG_AVS_LABEL_CHOOSE_STUDENTS."</span></p>";
					echo "<form action='a_index.php?opcao=FuncaoLiberarFinal&CodigoDisciplina=$CodigoDisciplina&grupo=$grupo&aval=$avaliacao' method='post' name='form_alunos' onSubmit='return enviar_formulario()'>";
					
					// cria o select com os alunos
					echo "<select name='aluno[]' multiple='multiple' id='selecao' style='width: 300px; height: 100px'>\n";
					$rs->MoveFirst();
					while ($array = $rs->FetchRow()) {
						if(in_array($array['id'], $alunos_avaliados)) echo "<option value='".$array['id']."_".$array['id_curso']."'>".$array['nome']."(".$array['curso'].")*</option>\n";
						else echo "<option value='".$array['id']."_".$array['id_curso']."'>".$array['nome']." (".$array['curso'].")</option>\n";
					}
					echo "</select>\n";

					echo "<br />";
					echo "<div class='alerta' style='margin-left:0'>";
					echo "<p><span>".A_LANG_AVS_TEXT_LIBERATE_STUDENT."</span></p>";
					echo "</div>";
					// imprime os bot�es					
					echo "<div class='funcao' style='padding-left:0'>";
						echo "<input type='button' value='".A_LANG_AVS_BUTTON_MARK."' class='button' onclick='acao_itens(true)' /> &nbsp;";
						echo "<input name='button' type='button' class='button' onclick='acao_itens(false)' value='".A_LANG_AVS_BUTTON_UNMARK."' /> &nbsp;";
	
						echo "<select id='selecao_curso' onChange='acao_curso()'>\n";
						echo "  <option selected='selected' disabled='disabled'>".A_LANG_AVS_COMBO_SELECT."</option>\n";
						// monta o selecionar
						$rs->MoveFirst();
						$ultimo = NULL;
						echo "  <optgroup label='".A_LANG_AVS_COMBO_MARK."'>\n";
						while ($array = $rs->FetchRow()) {
							if($array['id_curso'] != $ultimo) {
								echo "    <option value='m_".$array['id_curso']."'>".$array['curso']."</option>\n";
								$ultimo = $array['id_curso'];
							}
						}
						echo "  </optgroup>\n";
						// monta o deslecionar
						$rs->MoveFirst();
						$ultimo = NULL;
						echo "  <optgroup label='".A_LANG_AVS_COMBO_UNMARK."'>\n";
						while ($array = $rs->FetchRow()) {
							if($array['id_curso'] != $ultimo) {
								echo "     <option value='d_".$array['id_curso']."'>".$array['curso']."</option>\n";
								$ultimo = $array['id_curso'];
							}
						}
						echo "   </optgroup>\n";	
						echo " </select>&nbsp;\n";					
						echo "<input name='enviar' value='".A_LANG_AVS_BUTTON_NEXT."' type='Submit' class='button'>";
					echo "</div>";
					
					echo "</form>";
				}			
			?>		
			</div>
		</td>
	</tr>
</table>

