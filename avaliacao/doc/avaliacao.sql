-- phpMyAdmin SQL Dump
-- version 2.7.0-pl2
-- http://www.phpmyadmin.net
-- 
-- Servidor: localhost
-- Tempo de Gera��o: Out 14, 2008 as 09:35 PM
-- Vers�o do Servidor: 5.0.18
-- Vers�o do PHP: 4.4.7
-- 
-- Banco de Dados: `adaptweb`
-- 

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `aval_questao`
-- 

DROP TABLE IF EXISTS aval_questao;
CREATE TABLE aval_questao (
  id_questao int(10) unsigned NOT NULL,
  id_avaliacao int(10) unsigned NOT NULL,
  vl_peso_questao int(11) default NULL,
  PRIMARY KEY  (id_questao,id_avaliacao)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `avaliacao`
-- 

DROP TABLE IF EXISTS avaliacao;
CREATE TABLE avaliacao (
  id_avaliacao int(10) unsigned NOT NULL auto_increment,
  id_grupo_aval int(10) unsigned NOT NULL,
  ds_avaliacao varchar(100) character set latin1 collate latin1_bin NOT NULL,
  bl_divulg_nota_auto tinyint(1) NOT NULL,
  dt_cadastro_aval datetime NOT NULL,
  dt_alteracao_aval datetime NOT NULL,
  bl_presencial tinyint(1) default '0',
  bl_randomica tinyint(1) default '0',
  PRIMARY KEY  (id_avaliacao)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `grupo_aval`
-- 

DROP TABLE IF EXISTS grupo_aval;
CREATE TABLE grupo_aval (
  id_grupo_aval int(10) unsigned NOT NULL auto_increment,
  id_disc int(10) unsigned NOT NULL,
  topico int(11) NOT NULL,
  ds_grupo varchar(256) character set latin1 collate latin1_bin default NULL COMMENT 'Descri��o do grupo de avalia��o',
  id_aval_presencial int(11) default NULL,
  PRIMARY KEY  (id_grupo_aval),
  KEY id_aval_presencial (id_aval_presencial)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `grupo_curso`
-- 

DROP TABLE IF EXISTS grupo_curso;
CREATE TABLE grupo_curso (
  id_grupo_aval int(11) NOT NULL,
  id_curso int(11) NOT NULL,
  vl_peso int(3) default NULL,
  vl_peso_mod int(3) default NULL,
  vl_peso_default int(3) default NULL,
  dt_modificado datetime default NULL,
  PRIMARY KEY  (id_grupo_aval,id_curso)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `lacuna`
-- 

DROP TABLE IF EXISTS lacuna;
CREATE TABLE lacuna (
  id_lacuna int(10) unsigned NOT NULL auto_increment,
  id_questao int(10) unsigned NOT NULL,
  ds_pergunta text character set latin1 collate latin1_bin NOT NULL,
  ds_compl_pergunta varchar(255) character set latin1 collate latin1_bin default NULL,
  ds_resposta varchar(40) character set latin1 collate latin1_bin NOT NULL,
  PRIMARY KEY  (id_lacuna),
  KEY questao_lacuna (id_questao)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `liberar_aluno`
-- 

DROP TABLE IF EXISTS liberar_aluno;
CREATE TABLE liberar_aluno (
  id_libera_aluno int(11) NOT NULL auto_increment,
  id_libera int(11) NOT NULL,
  id_usuario int(11) NOT NULL,
  id_curso int(11) NOT NULL,
  id_disc int(11) NOT NULL,
  dt_inicio datetime default NULL,
  dt_envio datetime default NULL,
  status_aval tinyint(1) default '0',
  vl_nota int(3) default NULL,
  PRIMARY KEY  (id_libera_aluno),
  UNIQUE KEY id_libera (id_libera,id_usuario,id_curso,id_disc),
  KEY dt_submisao (dt_envio)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `liberar_aval`
-- 

DROP TABLE IF EXISTS liberar_aval;
CREATE TABLE liberar_aval (
  id_libera int(11) NOT NULL auto_increment,
  id_grupo_aval int(11) NOT NULL,
  id_avaliacao int(11) NOT NULL,
  ds_avaliacao varchar(100) collate latin1_bin default NULL,
  bl_presencial tinyint(1) default '0',
  bl_randomica tinyint(1) default NULL,
  bl_divulg_nota_auto tinyint(1) NOT NULL default '0',
  dt_liberar datetime default NULL,
  dt_encerrar datetime default NULL,
  status_libera tinyint(1) default NULL,
  divulgacao_nota tinyint(1) default '0',
  dt_divulgacao datetime default NULL,
  vl_nota_cancelada int(3) default NULL,
  PRIMARY KEY  (id_libera)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `liberar_questao`
-- 

DROP TABLE IF EXISTS liberar_questao;
CREATE TABLE liberar_questao (
  id_questao int(10) unsigned NOT NULL,
  id_libera int(10) unsigned NOT NULL,
  vl_peso int(3) default NULL,
  vl_peso_mod int(3) default NULL,
  vl_peso_default int(3) default NULL,
  bl_cancelada tinyint(1) default NULL,
  dt_cancelada datetime default NULL,
  PRIMARY KEY  (id_questao,id_libera)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_bin;

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `multipla_escolha`
-- 

DROP TABLE IF EXISTS multipla_escolha;
CREATE TABLE multipla_escolha (
  id_alternativa int(10) unsigned NOT NULL auto_increment,
  id_questao int(10) unsigned NOT NULL,
  ds_alternativa varchar(256) character set latin1 collate latin1_bin NOT NULL,
  bl_fixa tinyint(1) default '0',
  bl_correta tinyint(1) default NULL,
  PRIMARY KEY  (id_alternativa),
  KEY questao_me (id_alternativa)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `questao`
-- 

DROP TABLE IF EXISTS questao;
CREATE TABLE questao (
  id_questao int(10) unsigned NOT NULL auto_increment,
  ds_enunciado text character set latin1 collate latin1_bin NOT NULL,
  ds_explicacao text character set latin1 collate latin1_bin NOT NULL,
  tipo_questao int(2) unsigned NOT NULL default '4',
  id_disc int(11) default NULL,
  PRIMARY KEY  (id_questao),
  KEY enunciado (ds_enunciado(256))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `resposta`
-- 

DROP TABLE IF EXISTS resposta;
CREATE TABLE resposta (
  id_resposta int(10) unsigned NOT NULL auto_increment,
  id_libera_aluno int(11) unsigned NOT NULL,
  id_questao int(10) unsigned NOT NULL,
  status_questao tinyint(1) NOT NULL default '-1',
  qt_acerto float default NULL,
  vl_questao int(3) default NULL,
  ds_explicacao text character set latin1 collate latin1_bin,
  ds_resposta text character set latin1 collate latin1_bin,
  PRIMARY KEY  (id_resposta),
  UNIQUE KEY resposta_aluno (id_libera_aluno,id_questao)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

-- 
-- Estrutura da tabela `verd_falso`
-- 

DROP TABLE IF EXISTS verd_falso;
CREATE TABLE verd_falso (
  id_questao int(10) unsigned NOT NULL,
  ds_verdadeiro varchar(256) character set latin1 collate latin1_bin default 'Verdadeiro',
  ds_falso varchar(256) character set latin1 collate latin1_bin default 'Falso',
  bl_alter_correta tinyint(1) default NULL,
  PRIMARY KEY  (id_questao)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ------------------------------------------------------------

-- Novos campos em tabelas existentes no AdaptWeb Original

ALTER TABLE usuario ADD sessao_usuario VARCHAR(50) NULL DEFAULT NULL;
ALTER TABLE usuario ADD ip_usuario CHAR(15) NULL DEFAULT NULL;

ALTER TABLE matricula ADD vl_arred INT(3) NULL DEFAULT NULL;
ALTER TABLE matricula ADD vl_arred INT(3) NULL DEFAULT NULL; 