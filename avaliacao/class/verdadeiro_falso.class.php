<?php

/**
 * Classe para as quest�es de Verdadeiro/Falso
 * @author Claudiomar Desanti <claudesanti@gmail.com
 * @package Questao
 * @subpackage Verdadeiro_Falso
 * @version 1.0 16/09/2007
 */

/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

 /**
 * Define a quest�o como verdadeiro
 */
define('VERDADEIRO', '1');

/**
 * Define a quest�o como falso
 */
define('FALSO','0');

class Verdadeiro_Falso extends Questao {

	/**
	 * @var string Descri��o do label verdadeiro
	 */
	var $label_verd;
 
	/**
	 * @var string Descri��o do label falso
	 */
	var $label_falso;
 
	/**
	 * @var boolean Indica TRUE para a resposta correta ser Verdade e FALSE para falso
	 */
	var $correta;

	/**
	 * Construtor da classe Verdadeiro_Falso, se for passado o parametro $id � feito a busca automaticamente na base de dados
	 * @param ADOdb_* $obj_banco Objeto de conex�o com a base de dados
	 * @param integer $id Identificador da quest�o (chave prim�ria)
	 */
	function Verdadeiro_Falso($obj_banco, $id = NULL) {
		global $A_DB_TYPE;
		if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
			die (A_LANG_AVS_ERROR_DB_OBJECT);
		
		$this->id_questao = empty($id) ? NULL : (int) $id; // seguran�a
		
		parent::Questao($obj_banco, $this->id_questao);

		if(empty($id)) {
			$this->tipo_questao = VERDADEIRO_FALSO;
			$this->label_falso  = 'Falso';
			$this->label_verd   = 'Verdadeiro';
			$this->correta = NULL;
		} else {
			Verdadeiro_Falso::buscar();
		}
	}

	/*
	-------------------------------------------------------------------------------------------------
	 * Fun��es SETTER
	 -------------------------------------------------------------------------------------------------
	 */

	/**
	 * Seta a descri��o do verdadeiro
	 * @param string $valor
	 * @access public
	 */
	function set_label_verdade($valor) {
		$this->label_verd = $valor;
	}

	/**
	 * Seta a descri��o do falso
	 * @param string $valor
	 * @access public
	 */
	function set_label_falso($valor) {
		$this->label_falso = $valor;
	}

	/**
	 * Define o valor da op��o correta
	 * @param string $valor '1' para a quest�o verdadeira e '0' para a quest�o falsa
	 * @access public
	 * @return boolean FALSE em caso de erro
	 */
	function set_correta($valor) {
		if ($valor == VERDADEIRO || $valor == FALSO)
			$this->correta = $valor;
		else return FALSE;
	}

	/*
	-------------------------------------------------------------------------------------------------
	* Fun��es GETTER
	-------------------------------------------------------------------------------------------------
	*/

	/**
	 * Retorna o label verdadeiro
	 * @access public
	 * @return string
	 */
	function get_label_verdade() {
		return $this->label_verd;
	}

	/**
	 * Retorna o label falso
	 * @access public
	 * @return string
	 */
	function get_label_falso() {
		return $this->label_falso;
	}

	/**
	 * Retorna a tipo correto para a questao
	 * @access public
	 * @return string
	 */
	function get_correta() {
		return $this->correta;
	}

	/**
	 * Confere a resposta correta
	 * @param integer $valor Valor para verificar se est� correto ou n�o
	 * @access public
	 
	 */
	function resposta_correta($valor) {
		if($this->correta == $valor) return 1;
		else return 0;			
	}

	/*
	-------------------------------------------------------------------------------------------------
	*		Base de dados
	-------------------------------------------------------------------------------------------------
	*/
	
	/**
	 * Busca as informa��es sobre a quest�o verdadeiro/falso
	 * @access public
	 * @return boolean Em caso de busca efetuada com sucesso retorna TRUE
	 */
	function buscar() {
		$query = "SELECT * FROM verd_falso WHERE id_questao = ".$this->id_questao;
		$rs = $this->banco->Execute($query);

		if ($rs) {
			$this->label_falso = $rs->Fields('ds_falso');
			$this->label_verd = $rs->Fields('ds_verdadeiro');
			$this->correta = $rs->Fields('bl_alter_correta');
			return TRUE;
		} else return FALSE;

	}

	/**
	 * Salva as informa��es no banco de dados
	 * @access public
	 * @return boolean Se a quest�o foi salva corretamente retorna TRUE, em caso de por falta de campos obrigat�rios ou erro no banco retorna FALSE
	 */
	function salvar() {
		// descri��o � um campo obrigat�rio se n�o for preenchido retorna falso.
		if (!($this->enunciado || $this->correta)) {
			return FALSE;
		}
		if ($this->id_questao) return Verdadeiro_Falso::_atualizar();
		else return Verdadeiro_Falso::_inserir();
	}

	/**
	 * Deleta a quest�o verdadeiro/falso do banco de dados
	 * @access public
	 */
	function deletar() {
		if($this->id_questao) {
			$query = "DELETE FROM verd_falso WHERE id_questao =".$this->id_questao;
			$this->banco->Execute($query);
			return parent::deletar();
		}
		return FALSE;
	}

	/*
	 * Fun��es Privadas
	 */

	/**
	 * Insere no banco de dados atrav�s da fun��o salva()
	 * @access private
	 * @see Verdadeiro_Falso::salva()
	 */
	function _inserir() {
		$this->banco->Execute("START TRANSACTION");
		// faz a inser��o da quest�o, e obtem o id_questao
		if (parent::salvar()) {
			// insere a questao dissertativa
			// coloca corretamente quote nas strings
			$verdade = $this->banco->qstr($this->label_verd);
			$falso = $this->banco->qstr($this->label_falso);
			
			$query = "INSERT INTO verd_falso(id_questao, ds_verdadeiro, ds_falso, bl_alter_correta) " .
					 "VALUES($this->id_questao, ".$verdade.", ".$falso.", '".$this->correta."')";

			$rs = $this->banco->Execute($query);
			// se tudo correr bem � feito real inser��o no banco
			if ($rs) {
				$this->banco->Execute("COMMIT");
				return TRUE;
			}
		}
		// em qualquer caso de erro retrocede as opera��es feitas
		$this->banco->Execute("ROLLBACK");
		return FALSE;
	}

  /**
	 * Atualiza as informa��es do objeto no banco de dados
	 * @access private
	 * @see Verdadeiro_Falso::salva()
	 */
	function _atualizar() {
		$this->banco->Execute("START TRANSACTION");
		if(parent::salvar()) {
			// coloca corretamente quote nas strings
			$verdade = $this->banco->qstr($this->label_verd);
			$falso = $this->banco->qstr($this->label_falso);

			$query = "UPDATE verd_falso SET " .
					 "ds_verdadeiro = ".$verdade.", " .
					 "ds_falso = ".$falso.", " .
					 "bl_alter_correta = '".$this->correta."' " .
					 "WHERE id_questao = $this->id_questao";

			$rs = $this->banco->Execute($query);
			// se tudo correr bem � feito real inser��o no banco
			if ($rs) {
				$this->banco->Execute("COMMIT");
				return TRUE;
			}
		}
		// em qualquer caso de erro retrocede as opera��es feitas
		$this->banco->Execute("ROLLBACK");
		return FALSE;
	}
}  // fim da classe Verdadeiro_Falso
?>