<?php
/**
 * Classe para trabalhar com os modelos de avaliao
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avaliacao_Somativa
 * @subpackage Avaliacao
 * @version 1.2 05/04/2008
 */

/*
 * Trabalho de Concluso de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

class Avaliacao {

	/**
	 * @var object Objeto ADODB para acesso ao banco de dados
	 */
  var $banco;

	/**
	 * @var integer Identificador do avaliacao (chave primria)
	 */
  var $id_aval;

	/**
	 * @var integer Identificador do grupo
	 */
	var $id_grupo;
  
	/**
	 * @var string Descrio da Avaliao
	 */
	var $descricao;

	/**
	 * @var boolean TRUE se a divulgao for automtica
	 */
  var $divulgacao;
	
	var $presencial;
	
	var $randomica;

	/**
	 * @var date Data de criao da Avaliao
	 */
  var $data_cadastro;

	/**
	 * @var date Data da ltima alterao feita na Avaliao
	 */
  var $data_alteracao;

	/**
	 * @var boolean TRUE se puder divulgar automaticamente, ou seja, no pode haver questes dissertativas
	 */
  var $pode_divulgar;

	/**
	 * Formato: array("questao", "tipo", "peso", "salvar")
	 * @var array Vetor que contm informaes sobre questes da Avaliao
	 */
  var $questao;

	/**
	 * @var integer Varivel de controle do vetor questao, indica a quantidade de questes na avaliaes
	 * @see Avaliacao::$questao
	 */
  var $num_questao;

	/**
	 * @var integer Varivel de controle do vetor questao, indica o fim do vetor
	 * @see Avaliacao::$questao
	 */	
  var $EOF;
	
	/**
	 * @var integer Varivel de controle do vetor questao, indica a posicao atual do cursor
	 * @see Avaliacao::$questao
	 */			
  var $pos_atual;

	/**
	 * Construtor da classe, se for passado o parametro $id  feito a busca automaticamente na base de dados
	 * @param ADOdb_* $obj_banco Objeto de conexo com a base de dados
	 * @param integer $id Identificador da Avaliao (chave primria)
	 */
  function Avaliacao($obj_banco, $id = NULL) {
    global $A_DB_TYPE;
    if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
      die (A_LANG_AVS_ERROR_DB_OBJECT);

    $this->banco = $obj_banco;
    $this->id_aval = empty($id) ? NULL : (int) $id; // parte de seguranca
    $this->questao = array();
    if (!empty($this->id_aval)) {
      Avaliacao::buscar();
    } else {
      $this->descricao = NULL;
      $this->id_grupo = NULL;
      $this->divulgacao = NULL;
      $this->data_cadastro = NULL;
      $this->data_alteracao = NULL;
      $this->pode_divulgar = TRUE;
			$this->presencial = FALSE;
			$this->randomica = TRUE;
    }
    $this->num_questao = count($this->questao);
    $this->pos_atual = 0;
    $this->EOF = ($this->pos_atual == ($this->num_questao));
  }

/*
------------------------------------------------------------------------------------
															SETTERS
------------------------------------------------------------------------------------
*/


	/**
	 * Seta o grupo em que o modelo de avaliao pertence
	 * @param integer $valor Cdigo (ID) do curso
	 * @access public
	 */
  function set_grupo($valor) {
    $this->id_grupo = (int) $valor;
  }

	/**
	 * Insere uma descrio para a avaliao para melhor organizao do professor
	 * @param string $valor Texto contendo a descrio da avaliao
	 * @access public
	 */
  function set_descricao($valor) {
    $this->descricao = (string) trim($valor);
  }

	/**
	 * A avaliao ter sua nota divulgada automaticamente pelo sistema
	 * @param integer $valor Cdigo (ID) da disciplina
	 * @access public
	 */
  function set_divulgacao($valor) {
    $this->divulgacao = $valor;
  }

	/**
	 * A avaliao ter sua nota divulgada automaticamente pelo sistema
	 * @param boolean $valor 
	 * @access public
	 */
  function set_presencial($valor) {
    $this->presencial = $valor;
  }

	/**
	 * A avaliao ter sua nota divulgada automaticamente pelo sistema
	 * @param boolean
	 * @access public
	 */
  function set_randomica($valor) {
    $this->randomica = $valor;
  }


/*
----------------------------------------------------------------------------
												GETTERS
----------------------------------------------------------------------------
*/

	/**
	 * Retorna o ID do grupo
	 * @access public
	 * @return integer
	 */
  function get_grupo() {
    return $this->id_grupo;
  }

	/**
	 * Retorna a descrio da avaliao
	 * @access public
	 * @return string 
	 */
  function get_descricao() {
    return $this->descricao;
  }

	/**
	 * Retorna o ID da avaliao
	 * @access public
	 * @return integer
	 */
  function get_avaliacao() {
    return $this->id_aval;
  }

	/**
	 * Retorna a data de criao da avaliao
	 * @access public
	 * @return date
	 */
  function get_data_cadastro() {
    return $this->data_cadastro;
  }

	/**
	 * Retorna a data da ltima alterao feita na avaliao
	 * @access public
	 * @return date
	 */
  function get_data_alteracao() {
    return $this->data_alteracao;
  }

	/**
	 * Retorna se a avaliao est sendo divulgada automaticamente
	 * @access public
	 * @return boolean TRUE para divulgao automatica
	 */
  function get_divulgacao() {
    return $this->divulgacao;
  }

	/**
	 * Verifica se a avaliao PODE ser divulgada automaticamente, ou seja, sem nenhuma questo dissertativa
	 * @access public
	 * @return boolean TRUE para a possibilidade de divulgao automtica
	 */
  function aceita_divulgacao() {
    return $this->pode_divulgar;
  }

	/**
	 * Retorna se a avaliao est sendo divulgada automaticamente
	 * @access public
	 * @return boolean TRUE para divulgao automatica
	 */
  function get_presencial() {
    return $this->presencial;
  }
	
		/**
	 * Retorna se a avaliao est sendo divulgada automaticamente
	 * @access public
	 * @return boolean TRUE para divulgao automatica
	 */
  function get_randomica() {
    return $this->randomica;
  }


/*
----------------------------------------------------------------------------------------
																	QUESTOES
----------------------------------------------------------------------------------------
*/


  /**
   * Atribui uma questao a avaliao
	 * @access public
   * @param integer $questao ID da questo que ser inserida na prova
	 * @param integer $peso Peso (entre 1 e 100) da questo para o clculo da nota da avaliao.
	 * @return integer | boolean Retorna a posio do vetor que foi inserida ou FALSE caso tenha algum erro
	 * @since 02/04/2008 Modificado a maneira de inserir as questes, diminuio do acesso a base de dados (nessrio utilizar a funo salvar())
   * @see Avaliacao::salvar()
   */
  function atribuir_questao($questao, $peso = NULL) {
    //  verifica o peso passado para a questo
    if($peso){
      if(!$peso = Avaliacao::verifica_peso($peso)) return FALSE;
    }
		// se a questo j foi inserida na avaliao ela no ser inserida novamente
    foreach ($this->questao as $k) {
      if($questao == $k['questao']) return FALSE;
    }
    // pega o tipo de questao, e tambm verifica se a questo est na base de dados
    $q = new Questao($this->banco, $questao);
    if(!($tipo = $q->get_tipo())) return FALSE;

    if($tipo == DISSERTATIVA) {
      $this->divulgacao = FALSE; // implica que no poder ser feito divulgao automtica
      $this->pode_divulgar = FALSE;
    }
	
		$vet_questao = array("questao" => $questao, 
										 "tipo"=> $tipo, 
										 "peso" => $peso,
										 "salvar" => 'i');

    $pos = array_push($this->questao, $vet_questao);    
    $this->num_questao++;
    $this->EOF = ($this->pos_atual == ($this->num_questao));
    return $pos;		
  }

  /**
   * Remove uma questo da avaliao
 	 * @access public
   * @param integer $questao ID da questo que ser inserida na prova
	 * @return boolean TRUE para removido do vetor com sucesso
	 * @since 02/04/2008 Modificado a maneira de deletar as questes, diminuio do acesso a base de dados (nessrio utilizar a funo salvar())
	 * @see Avaliacao::salvar()	 
   */
  function remover_questao($questao){
		$questao = (int)$questao;
    $pos = Avaliacao::procurar_posicao_questao($questao);
    if($pos === FALSE) return FALSE;
		
		$this->questao[$pos]['salvar'] = 'd';
		return TRUE;
  }

  /**
   * Altera o peso da questo
   * @access public
   * @param integer $questao ID da questo que ser inserida na prova
	 * @param integer $peso Peso (entre 1 e 100) da questo para o clculo da nota da avaliao.
	 * @return boolean TRUE para alterado o valor do vetor com sucesso
	 * @since 02/04/2008 Modificado a maneira de alterar as informaes das questes, diminuio do acesso a base de dados (nessrio utilizar a funo salvar())
	 * @see Avaliacao::salvar()	 
   */
  function alterar_peso_questao($questao, $peso) {
	  $questao = (int) $questao;
    $pos = Avaliacao::procurar_posicao_questao($questao);
    if($pos === FALSE) return FALSE;

    // o peso  um nmero inteiro de 0 a 100;
    if(!$peso = Avaliacao::verifica_peso($peso)) return FALSE;

    $this->questao[$pos]['peso'] = $peso;
		$this->questao[$pos]['salvar'] = 'u';
    return TRUE;
  }


	/**
	 * Retorna uma questo, esta questo  a qual o ponteiro interno est apontando. (Utilize com o comando foreach)
	 * @access public
	 * @return Array Retorn os valores da questo em focus
	 */
  function buscar_questao() {
    if ($this->num_questao == 0) return FALSE;
    if ($this->EOF) {
      $this->pos_atual = 0;
      $this->EOF = FALSE;
      return FALSE;
    }
    $retorno = $this->questao[$this->pos_atual];
    if(!$this->EOF) {
      // se estiver em qualquer outra casa do vetor
      $this->pos_atual++;
      $this->EOF = ($this->pos_atual == ($this->num_questao)); // verifica se no  fim de array
    }
    return $retorno;
  }

	/**
	 * Retorna a quantidade de questes inseridas na avaliao
	 * @access public
	 * @return integer Quantidade de questes
	 */
  function numero_questao() {
    return $this->num_questao;
  }

	/**
	 * Retorna as informaes de uma questo. (Utilize com o comando For)
	 * @access public
	 * @param integer $pos Posio do vetor
	 * @return Array Vetor com as informaes da questo
	 */
  function questao($pos) {
    return $this->questao[$pos];
  }

	/**
	 * Retorna as informaes de uma questo atravs do ID
	 * @access public
	 * @param integer $questao ID da questo
	 * @return Array Vetor com as informaes da questo
	 */
  function questao_identificador($questao) {
		$questao = (int) $questao;
    $pos = Avaliacao::procurar_posicao_questao($questao);
    if($pos === FALSE) return FALSE;
    return $this->questao[$pos];
  }


/*
------------------------------------------------------------------------------------------
															BANCO DE DADOS
------------------------------------------------------------------------------------------	 
*/

	/**
	 * Busca na base de dados a questo Multipla Escolha atravs do ID setado
	 * @access public
	 * @return boolean
	 */
  function buscar(){
    $this->questao = array();
		// selecionar as informacoes da avaliacao
    $query = "SELECT id_grupo_aval as grupo, ds_avaliacao as descricao, dt_cadastro_aval as cadastro, ".
             "dt_alteracao_aval as alteracao, bl_divulg_nota_auto as divulgacao, bl_presencial AS presencial, bl_randomica AS randomica ".
             "FROM avaliacao WHERE id_avaliacao =".$this->id_aval;

    $rs = $this->banco->Execute($query);
    if (!$rs) return FALSE;

    $this->id_grupo = $rs->Fields('grupo');
    $this->descricao = $rs->Fields('descricao');
    $this->data_alteracao = formata_data($rs->Fields('alteracao'));
    $this->data_cadastro = formata_data($rs->Fields('cadastro'));
    $this->divulgacao = $rs->Fields('divulgacao');
		$this->presencial = (int)$rs->Fields('presencial');
		$this->randomica = $rs->Fields('randomica');

    // selecionar as questoes pertencetes
    $qquestao = "SELECT id_questao AS questao, vl_peso_questao AS peso, tipo_questao AS tipo ".
                "FROM aval_questao LEFT JOIN questao USING(id_questao) WHERE id_avaliacao='".$this->id_aval."'";
    $rs_questao = $this->banco->Execute($qquestao);

    if (!$rs_questao) return FALSE;
    // montar o vetor com as questoes

    while($questao = $rs_questao->FetchRow()) {
      array_push($this->questao, array("questao" => $questao['questao'], "tipo" => $questao['tipo'], "peso" => $questao['peso'], "salvar"=>'x'));
      if ($questao['tipo'] == DISSERTATIVA) $this->pode_divulgar = FALSE;
    }
    if ($this->pode_divulgar === NULL) $this->pode_divulgar = TRUE;
    $this->num_questao = count($this->questao);
    $this->pos_atual = 0;
    $this->EOF = ($this->pos_atual == ($this->num_questao));
		return TRUE;
  }

	/**
	 * Deleta a questo da base de dados
	 * @access public
	 * @return boolean TRUE para sucesso.
	 */
  function deletar() {
    if($this->id_aval) {
      $query = "DELETE FROM aval_questao WHERE id_avaliacao ='".$this->id_aval."'";
      $this->banco->Execute($query);
      $query = "DELETE FROM avaliacao WHERE id_avaliacao ='".$this->id_aval."'";
      $this->banco->Execute($query);
    }
    return FALSE;
  }

	/**
	 * Salva as informaes na base de dados
	 * @access public
	 * @return boolean Retorna TRUE se a operao foi realizada com sucesso
	 */
  function salvar() {
		// Na hora de salvar verifica se a avaliação é presencial, se for deleta todas as questões.
		if ($this->presencial) {
			for($x=0; $x < $this->num_questao; $x++) {
				$this->questao[$x]['salvar'] = 'd';
			}		
			$this->randomica = 0;
			$this->divulgacao = 0;
		}
		if (empty($this->descricao)) return FALSE;
    if ($this->id_aval) return Avaliacao::_atualizar();
    else return Avaliacao::_inserir();
  }


	function clonar($avaliacao, $grupo) {
		if($this->id_aval) return FALSE;
		
		$avaliacao = (int) $avaliacao;
		$grupo = (int) $grupo;
		
		if(!$grupo || !$avaliacao) return FALSE;
		
		// Insere o parte principal da avaliacao
		$query = "INSERT INTO avaliacao ".
						 "SELECT null, ".$grupo.", ds_avaliacao, bl_divulg_nota_auto, NOW(), NOW(), bl_presencial, bl_randomica ".
						 "FROM avaliacao WHERE id_avaliacao=".$avaliacao;
		$rs = $this->banco->Execute($query);
		
		if($rs) { 
			$this->id_aval = $this->banco->Insert_ID(); // pega o id da avaliação
			// insere as questões
			$query = "INSERT INTO aval_questao ".
							 "SELECT id_questao, ".$this->id_aval.", vl_peso_questao FROM aval_questao WHERE id_avaliacao=".$avaliacao;
							 
			$rs = $this->banco->Execute($query);
			if($rs) return TRUE;
			else return FALSE;				 
		}
		return FALSE;
	}

	/**
	 * Insere na base de dados
	 * @see Avaliacao::salvar()
	 * @access private
	 * @return boolean
	 */
  function _inserir() {
    // insere na tabela avaliao
    $this->banco->Execute("START TRANSACTION");
		
		$descricao = $this->banco->qstr($this->descricao, get_magic_quotes_gpc()); // adiciona corretamente as quotes

    $query = "INSERT INTO avaliacao(id_avaliacao, id_grupo_aval, ds_avaliacao, bl_divulg_nota_auto ,dt_cadastro_aval, dt_alteracao_aval, bl_presencial, bl_randomica) ".
             "VALUES(NULL, ".$this->id_grupo.",".$descricao.",'".$this->divulgacao."',NOW(), NOW(), ".$this->presencial.",".$this->randomica.")";
    $rs = $this->banco->Execute($query);

    // inserir cada uma das questoes na tabela aval_questao
    if ($rs) {
      $this->id_aval = $this->banco->Insert_ID();
			for($x = 0; $x < $this->num_questao; $x++) {
        if(!Avaliacao::_salvar_questao($x)) {
          $this->banco->Execute("ROLLBACK");
          return FALSE;
        }
      }
			$this->banco->Execute("COMMIT");
			Avaliacao::buscar();
			return TRUE;	
    }
    $this->banco->Execute("ROLLBACK");
    return FALSE;
  }

	/**
	 * Atualiza (Update) as informaes na base de dados
	 * @access private
	 * @return boolean
	 * @see Avaliacao::salvar()
	 */
  function _atualizar() {
    $this->banco->Execute("START TRANSACTION");

    $query = "UPDATE avaliacao SET " .
             "ds_avaliacao ='".$this->descricao."', " .
             "id_grupo_aval = '".$this->id_grupo."', " .
             "dt_alteracao_aval = NOW(), ".
             "bl_divulg_nota_auto = '".$this->divulgacao."', ".
						 "bl_presencial = ".$this->presencial.", ".
						 "bl_randomica = ".$this->randomica." ".
             "WHERE id_avaliacao ='".$this->id_aval."'";

    $rs = $this->banco->Execute($query);
    if ($rs) {
			for($x = 0; $x < $this->num_questao; $x++) {
        if(!Avaliacao::_salvar_questao($x)) {
          $this->banco->Execute("ROLLBACK");
          return FALSE;
        }
      }
			$this->banco->Execute("COMMIT");
			Avaliacao::buscar();
			return TRUE;		
    }
    $this->banco->Execute("ROLLBACK");
    return FALSE;
  }

	/** 
	 * Salva (Insert, Update, Delete) as informaes das questes
	 * @access private
	 * @param integer $pos Posio do vetor aluno que ser trabalhado
	 * @return boolean TRUE para operao realizada com sucesso
	 */
  function _salvar_questao($pos) {
    
		$vetor = $this->questao[$pos]; // pega o vetor na posio passada
		
		if($vetor['salvar'] == 'i') {
		  // ----------- INSERE UMA NOVA QUESTO ------------------------- //
			$peso = !empty($vetor['peso']) ? $vetor['peso'] : 'NULL';
			$query = "INSERT INTO aval_questao(id_questao, id_avaliacao, vl_peso_questao) ".
								"VALUES(".$vetor['questao'].",".$this->id_aval.",".$peso.")";
			$rs = $this->banco->Execute($query);
			if($rs) return TRUE;
			else return FALSE;		
		} elseif ($vetor['salvar'] == 'u' ) {
		  // ---------- ATUALIZA A QUESTO ------------------------------- //
			$peso = $vetor['peso'] ? $vetor['peso'] : 'NULL';
			$query = "UPDATE aval_questao SET " .
								"vl_peso_questao='".$vetor['peso']."' " .
								"WHERE id_questao='".$vetor['questao']."' " .
								"AND id_avaliacao = '".$this->id_aval."'";
			$rs = $this->banco->Execute($query);
			if($rs) return TRUE;
			else return FALSE;
		} elseif ($vetor['salvar'] == 'd' ) {
      // ---------- DELETA A QUESTÃO DA AVALIÇÃO --------------------- //
			// ATENO: No  deletada a questo da base de dados, pois pode ser utilizada por outras avaliaes
      $query = "DELETE FROM aval_questao WHERE id_questao='".$vetor['questao']."' AND id_avaliacao=".$this->id_aval;
      $rs = $this->banco->Execute($query);
      if ($rs) return TRUE;		
			else return FALSE;
		}
		return TRUE;		
  }

	/**
	 * Procura a posio de uma questo no vetor questo atravs de uma ID
	 * @access private
	 * @param integer $id ID da questo que deve ser encontrada
	 * @return integer | boolean Retorna a posio do vetor que se encontra o ID procurado ou FALSE se no houver
	 */
  function procurar_posicao_questao($id) {
    for($x = 0; $x < $this->num_questao; $x++){
      if($this->questao[$x]['questao'] == $id) {
        return $x;
      }
    }
    return FALSE;
  }
  
	/**
	 * Funo que verifica se o peso est entre o intervalo de 0 a 100
	 * @access private
	 * @param integer $peso O peso que ser verificado
	 * @return boolean | integer Retorna FALSE se o peso no estiver no intervalo, seno retorna o prprio peso
	 */
  function verifica_peso($peso){
    // o peso  um nmero inteiro de 0 a 100;
    $peso = (integer)$peso;
    if($peso < 0 && $peso > 100) return FALSE;
    return $peso;
  }

}
?>