<?php
/**
 * Classe para a implementa��o das quest�es dissertativas
 * @author Claudiomar Desanti <claudesanti@gmail.com
 * @package Questao
 * @subpackage Dissertativa
 * @version 1.0 16/09/2007
 */
	
/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	
	
class Dissertativa extends Questao {

	/**
	 * Construtor da classe, se for passado o parametro $id � feito a busca automaticamente na base de dados
	 * @param ADOdb_* $obj_banco Objeto de conex�o com a base de dados
	 * @param integer $id Identificador da grupo (chave prim�ria)
	 */
	function Dissertativa($obj_banco, $id = NULL) {
		global $A_DB_TYPE;
		if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
			die (A_LANG_AVS_ERROR_DB_OBJECT);		
		
		$this->id_questao = empty($id) ? NULL : (int) $id; // seguran�a

		parent::Questao($obj_banco, $this->id_questao);

		if(empty($this->id_questao)) {
			$this->tipo_questao = DISSERTATIVA;
		} 
	}

	
}  // fim da classe Dissertativa

?>
