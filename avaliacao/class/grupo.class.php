<?php

/**
 * Classe para trabalhar com o Grupo de Avalição
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avaliacao_Somativa
 * @subpackage Grupo_Avaliacao
 * @version 1.2 24/09/2007
 * @since 02/04/2008 Modificação nas operacoes (insert, updade e delete) diminuindo o overhead na base de dados 
 * Necessário a utilização da função salvar() após a função remover_alternativa()
 */
 
/*
 * Trabalho de Conclusão de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

class Grupo {

	/**
	 * @var object Objeto ADODB para acesso ao banco de dados
	 */
	var $banco;

	/**
	 * @var integer Identificador do grupo (chave primria)
	 */
	var $id_grupo;

	/**
	 * @var integer Identificador da disciplina 
	 */
	var $id_disc;

	/**
	 * @var integer Define qual o topico que pertence a avaliao
	 */
	var $topico;

	/**
	 * @var string Descrição do grupo
	 */	
	var $descricao;

	/**
	 * @var integer Id da avaliação presencial
	 */
	var $presencial;

	/**
	 * Formato: array('id_avaliacao');
	 * @var array Vetor que contm o ID todas as avaliaes pertencentes ao grupo
	 */
	var $aval;

	/**
	 * @var integer Varivel de controle do vetor aval, indica a quantidade de avaliaes no grupo
	 * @see Grupo::$aval
	 */
	var $num_aval;

	/**
	 * @var integer Varivel de controle do vetor aval, indica o fim do vetor
	 * @see Grupo::$aval
	 */		
	var $EOF;

	/**
	 * @var integer Varivel de controle do vetor aval, indica a posicao atual do cursor
	 * @see Grupo::$aval
	 */		
	var $pos_atual_aval;

	/**
	 * Formato: array('curso','peso','salvar')
	 * @var array Vetor que contm informaes sobre os cursos pertencentes ao grupo
	 */
	var $curso;

	/**
	 * @var integer Varivel de controle do vetor curso, indica a quantidade de cursos para o grupo
	 * @see Grupo::$curso
	 */
	var $num_curso;

	/**
	 * @var integer Varivel de controle do vetor curso, indica o fim do vetor
	 * @see Grupo::$curso
	 */		
	var $EOFC;

	/**
	 * @var integer Varivel de controle do vetor curso, indica a posicao atual do cursor
	 * @see Grupo::$curso
	 */	
	var $pos_atual_curso;

	/**
	 * Construtor da classe, se for passado o parametro $id  feito a busca automaticamente na base de dados
	 * @param ADOdb_* $obj_banco Objeto de conexo com a base de dados
	 * @param integer $id Identificador da grupo (chave primria)
	 */
	function Grupo($obj_banco, $id = NULL) {
		// verifica o banco de dados
		global $A_DB_TYPE;
		if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
			die (A_LANG_AVS_ERROR_DB_OBJECT);

		$this->id_grupo = empty($id) ? NULL : (int) $id;

		$this->aval = array();
		$this->curso = array();
		$this->banco = $obj_banco;
		if ($this->id_grupo) {
			Grupo::buscar();
		} else {
			$this->id_disc = NULL;
			$this->presencial = 0;
			$this->descricao = "";
		}
		// para as avaliaes
		$this->num_aval = count($this->aval);
		$this->pos_atual_aval = 0;
		$this->EOF = ($this->pos_atual_aval == ($this->num_aval));
		// para os cursos
		$this->num_curso = count($this->curso);
		$this->pos_atual_curso = 0;
		$this->EOFC = ($this->pos_atual_curso == ($this->num_curso));
	}

	/*
	-----------------------------------------------------------------------
	*						Funções Setters
	----------------------------------------------------------------------- 
	*/
	 

	/**
	 * Seta a disciplina que pertence o grupo
	 * @param integer $valor Código (ID) da disciplina
	 * @access public
	 */
	function set_disciplina($valor) {
		$this->id_disc = (int) $valor;
	}

	/**
	 * Seta o número do tópico que pertence o grupo de avalição
	 * @param integer $valor Número do tópico
	 * @access public
	 */
	function set_topico($valor) {
		$this->topico = (int) $valor;
	}
	
	/**
	 * Seta o ID da avaliação presencial
	 * @param integer $valor ID da avaliação presencial
	 * @access public
	 */
	function set_presencial($valor) {
		$this->presencial = (int) $valor;
	}	
	
	/**
	 * Seta a descrição do grupo de avaliação
	 * @param String $valor Descrição
	 * @access public
	 */
	function set_descricao($valor) {
		$this->descricao = (string) trim($valor);
	}

/*
------------------------------------------------------------------------
					Funções Getters
------------------------------------------------------------------------
*/

	/**
	 * Devolve o ID do grupo
	 * @return integer
	 * @access public
	 */
	function get_grupo() {
		return $this->id_grupo;
	}

	/**
	 * Retorna se o grupo  constitudo por avaliao presencial
	 * @return boolean
	 * @access public
	 */
	function get_presencial() {
		return $this->presencial;
	}

	/**
	 * Retorna o ID da disciplina
	 * @return integer
	 * @access public
	 */
	function get_disciplina() {
		return $this->id_disc;
	}

	/**
	 * Devolve o nmero do tpico que pertence o grupo
	 * @return integer
	 * @access public
	 */
	function get_topico() {
		return $this->topico;
	}
	
	/**
	 * Devolve a descrição do grupo
	 * @return string
	 * @access public
	 */
	function get_descricao() {
		return $this->descricao;
	}	

	/*
	------------------------------------------------------------------------
	*						Funções para os cursos
	------------------------------------------------------------------------
	*/
	
	/**
	 * Adiciona um novo curso ao Grupo de Avaliao
	 * @access public
	 * @param integer $curso ID do curso
	 * @param integer $peso Valor do peso que o grupo ter para o curso na mdia final
	 * @return integer Posio do vetor que foi inserido o curso
	 * @since 04/04/2008 Modificado para gerar menos acessos ao banco de dados, agora  necessrio salvar
	 * @see Grupo::salvar()	 
	 */
	function atribuir_curso($curso, $peso = NULL) {		
		$curso = array("curso"	=> (int) $curso, 
									"peso" 		=> Grupo::verifica_peso($peso),
									"salvar" 	=> 'i'	 // insere um novo curso	
									);		
		$pos = array_push($this->curso, $curso) - 1;		

		$this->num_curso++;
		$this->EOFC = ($this->pos_atual_curso == ($this->num_curso));
		return $pos;
	}

	/**
	 * Remove o curso do Grupo de Avaliao. 
	 * @access public
	 * @param integet ID do curso que ser excludo do grupo
	 * @return boolean Retorna TRUE se foi excludo com sucesso
	 * @since 04/04/2008 Modificado para gerar menos acessos ao banco de dados, agora  necessrio salvar
	 * @see Grupo::salvar()
	 */
	function remover_curso($curso){
		$pos = Grupo::procurar_posicao_curso((int)$curso);
		if($pos === FALSE) return FALSE;

		if(!empty($this->id_grupo)) {
			$this->curso[$pos]['salvar'] = 'd';
			return TRUE;
		}	
		return FALSE;
	}

	/**
	 * Altera o peso de um curso para o grupo de avaliao
	 * @access public
	 * @param integer $curso ID do curso
	 * @param integer $peso O valor do peso que ser alterado
	 * @return boolean TRUE se foi alterado com sucesso
	 * @since 04/04/2008 Modificado para gerar menos acessos ao banco de dados, agora  necessrio salvar
	 * @see Grupo::salvar()
	 */
	function alterar_peso($curso, $peso) {
		$pos = Grupo::procurar_posicao_curso($curso);
		if($pos === FALSE) return FALSE;

		// o peso  um nmero inteiro de 0 a 100;
		if(!$peso = Grupo::verifica_peso($peso)) return FALSE;

		$this->curso[$pos]['peso'] = $peso;
		$this->curso[$pos]['salvar'] = 'u';
		return TRUE;
	}
	
	
	/**
	 * Altera o peso de um curso para o grupo de avalição para o peso default (valor do primeiro save)
	 * @access public
	 * @param integer $curso ID do curso
	 * @return boolean TRUE se foi alterado com sucesso
	 * @since 04/04/2008 Modificado para gerar menos acessos ao banco de dados, agora  necessrio salvar
	 * @see Grupo::salvar()
	 */	
	function peso_default($curso) {
		$pos = Grupo::procurar_posicao_curso($curso);
		if($pos === FALSE) return FALSE;
		
		$this->curso[$pos]['peso'] = $this->curso[$pos]['peso_default'];
		$this->curso[$pos]['salvar'] = 'u';
		return TRUE;		
	}
	
	/**
	 * Altera o peso de um curso para o grupo de avalição para o último peso (desfazer)
	 * @access public
	 * @param integer $curso ID do curso
	 * @return boolean TRUE se foi alterado com sucesso
	 * @since 04/04/2008 Modificado para gerar menos acessos ao banco de dados, agora  necessrio salvar
	 * @see Grupo::salvar()
	 */	
	function peso_anterior($curso) {
		$pos = Grupo::procurar_posicao_curso($curso);
		if($pos === FALSE) return FALSE;
		
		$this->curso[$pos]['peso'] = $this->curso[$pos]['peso_modificado'];
		$this->curso[$pos]['salvar'] = 'u';
		return TRUE;		
	}


	/**
	 * Quantidade de cursos dentro do Grupo
	 * @access public
	 * @return integer Quantidade de cursos
	 */
	function numero_curso() {
		return $this->num_curso;
	}

	/**
	 * Retorna uma curso, esta curso  a qual o ponteiro interno est apontando. (Utilize com o comando foreach)
	 * @access public
	 * @return Array Retorn os valores da alternativa em focus
	 */
	function buscar_curso() {
		if ($this->num_curso == 0) return FALSE;
		if ($this->EOFC) {
			$this->pos_atual_curso = 0;
			$this->EOFC = FALSE;
			return FALSE;
		}
		$retorno = $this->curso[$this->pos_atual_curso];
		if(!$this->EOFC) {
			// se estiver em qualquer outra casa do vetor
			$this->pos_atual_curso++;
			$this->EOFC = ($this->pos_atual_curso == ($this->num_curso)); // verifica se no  fim de array
		}
		return $retorno;
	}

	/**
	 * Retorna todos os IDs dos cursos pertencentes ao Grupo de Avaliao
	 * @access public
	 * @return array Vetor com os IDs dos cursos
	 */
	function cursos() {
		if(!empty($this->curso)) {
			$retorno = array();
			foreach ($this->curso as $k) {
				array_push($retorno, $k['curso']);
			}
			return $retorno;
		}
		else return array();
	}

	/*
	------------------------------------------------------------------------
					Funções para avaliação
	------------------------------------------------------------------------
	*/

	/**
	 * Quantidade de avaliaes dentro do Grupo de Avaliao
	 * @access public
	 * @return integer Quantidade de avaliaes
	 */
	function numero_avaliacao() {
		return $this->num_aval;
	}

	/**
	 * Retorna uma avaliao, esta avalição a qual o ponteiro interno est apontando. (Utilize com o comando foreach)
	 * @access public
	 * @return Array Retorn os valores da avaliao em focus
	 */
	function buscar_avaliacao() {
		if ($this->num_aval == 0) return FALSE;
		if ($this->EOF) {
			$this->pos_atual_aval = 0;
			$this->EOF = FALSE;
			return FALSE;
		}
		$retorno = $this->aval[$this->pos_atual_aval];
		if(!$this->EOF) {
			// se estiver em qualquer outra casa do vetor
			$this->pos_atual_aval++;
			$this->EOF = ($this->pos_atual_aval == ($this->num_aval)); // verifica se no  fim de array
		}
		return $retorno;
	}

	/**
	 * Retorna as informaes de uma avaliao. (Utilize com o comando For)
	 * @access public
	 * @param integer $pos Posio do vetor
	 * @return Array Vetor com as informaes da avaliao
	 */	
	function avaliacao($pos) {
		$pos = (int) $pos;
		return $this->aval[$pos];
	}

	/**
	 * Retorna as informaes de uma avaliao atravs do seu ID
	 * @access public
	 * @param integer $avaliacao ID da avaliao
	 * @return array | boolean Retorna o vetor com as infomaes da avaliao ou com FALSE se no encontrar
	 */
	function avaliacao_identificador($avaliacao) {
		$pos = Grupo::procurar_posicao_avaliacao($avaliacao);
		if($pos === FALSE) return FALSE;
		return $this->aval[$pos];
	}

	/*
	------------------------------------------------------------------------
					Funções Banco de Dados
	------------------------------------------------------------------------
	*/
	/**
	 * Faz a busca das informaes sobre o Grupo de Avaliao na base de dados
	 * @access public
	 */
	function buscar() {

		$this->aval = array();
		$this->curso = array();

		// busca de informações básicas	
		$query = "SELECT id_disc AS disciplina, topico as topico, ds_grupo as descricao, id_aval_presencial AS presencial".
				 		 " FROM grupo_aval WHERE id_grupo_aval='".$this->id_grupo."'";

		$rs = $this->banco->Execute($query);
		if($rs) {
			$this->id_disc = $rs->Fields('disciplina');
			$this->topico = $rs->Fields('topico');
			$this->descricao = $rs->Fields('descricao');
			$this->presencial = $rs->Fields('presencial');

			// buscar as avaliações
			$qaval = "SELECT id_avaliacao FROM avaliacao WHERE id_grupo_aval=".$this->id_grupo." ORDER BY bl_presencial, id_avaliacao";
			$rs_aval = $this->banco->Execute($qaval);
			
			if($rs_aval->NumRows() > 0) {
				while($aval = $rs_aval->FetchRow()){
					array_push($this->aval, $aval['id_avaliacao']);
				}
			}

			// busca os cursos
			$qcurso = "SELECT id_curso AS curso, vl_peso AS peso, vl_peso_mod AS peso_mod,".
					" vl_peso_default AS peso_default, dt_modificado AS data ".
					" FROM grupo_curso WHERE id_grupo_aval='".$this->id_grupo."'";
			
			$rs_curso = $this->banco->Execute($qcurso);
			if($rs_curso->NumRows() > 0) {
				while($curso = $rs_curso->FetchRow()){
					$c = array("curso" => $curso['curso'], 
											"peso" => $curso['peso'], 
											"peso_modificado" => $curso['peso_mod'],
											"peso_default" => $curso['peso_mod'],
											"data" => $curso['data'],
											"salvar" =>'x');
					array_push($this->curso, $c);
				}
			}
		}
		$this->num_aval = count($this->aval);
		$this->pos_atual_aval = 0;
		$this->EOF = ($this->pos_atual_aval == ($this->num_aval));

		$this->num_curso = count($this->curso);
		$this->pos_atual_curso = 0;
		$this->EOFC = ($this->pos_atual_curso == ($this->num_curso));
	}

	/**
	 * Exclui um Grupo de Avaliao com as informaes de seus respectivos cursos
	 * @access public
	 * @return boolean TRUE se excludo com sucesso
	 */
	function deletar() {
		$query = "DELETE FROM grupo_aval WHERE id_grupo_aval =".$this->id_grupo;
		$rs = $this->banco->Execute($query); //deleta o grupo
		// deleta cada uma das avaliaes no grupo.
		if($rs) {
			Grupo::_deletar_toda_avaliacao();
			$qcurso = "DELETE FROM grupo_curso WHERE id_grupo_aval =".$this->id_grupo;
			$rs = $this->banco->Execute($query); //deleta os cursos linkados com o grupo
			// deleta cada uma das avaliaes no grupo.
			if($rs) return TRUE;
		}
		return FALSE;
	}
  
	/**
	 * Salva as informaes sobre o Grupo de Avaliao
	 * @access public
	 * @return boolean TRUE em caso de sucesso
	 */
	function salvar() {
		if($this->id_grupo) return Grupo::_atualizar();
		else return Grupo::_inserir();
	}

	/**
	 * Atualiza (Update) as informaes na base de dados
	 * @access private
	 * @return boolean
	 * @see Grupo::salvar()
	 */		 
	function _atualizar() {
		$this->banco->Execute("START TRANSACTION");
		$descricao = $this->banco->qstr($this->descricao);
		$query = "UPDATE grupo_aval SET " .
				"id_disc = ".$this->id_disc.
				",topico = ".$this->topico.
				",ds_grupo = ".$descricao.
				",id_aval_presencial = ".$this->presencial.
				" WHERE id_grupo_aval = ".$this->id_grupo;
				
		$rs = $this->banco->Execute($query);
		if ($rs) {
			/*
			// verifica se a soma dos pesos não ultrapassa os valores
			if (!Grupo::soma_peso()) {
			$this->banco->Execute("ROLLBACK");
			return FALSE;	
			}	
			*/
			for($x = 0; $x < $this->num_curso; $x++) {
				if(!Grupo::_salvar_curso($x)) {
					$this->banco->Execute("ROLLBACK");
					return FALSE;
				}			
			}			
			$this->banco->Execute("COMMIT");
			Grupo::buscar();
			return TRUE;
		} else {
			$this->banco->Execute("ROLLBACK");
			return FALSE;
		}
	}

	/**
	 * Insere na base de dados
	 * @see Grupo::salvar()
	 * @access private
	 * @return boolean
	 */
	function _inserir() {
		// inicia a transacao
		$this->banco->Execute("START TRANSACTION");
		$descricao = $this->banco->qstr($this->descricao);

		$presencial = empty($this->presencial) ? $this->banco->qstr('NULL') : $this->presencial;
		
		$query = "INSERT INTO grupo_aval(id_grupo_aval, id_disc, topico, ds_grupo, id_aval_presencial)".
						" VALUES(NULL,".$this->id_disc.", ".$this->topico.", ".$descricao.", ".$presencial.")";
		// executa a query

		$rs = $this->banco->Execute($query);
		// se inserido com sucesso faz um commit e retorna true
		if ($rs) {      		
			$this->id_grupo = $this->banco->Insert_ID();
			/*
			// verifica se a soma dos pesos não ultrapassa os valores
			if (!Grupo::soma_peso()) {
			$this->banco->Execute("ROLLBACK");
			return FALSE;	
			}	
			*/
			for($x = 0; $x < $this->num_curso; $x++) {
				if(!Grupo::_salvar_curso($x)) {
					$this->banco->Execute("ROLLBACK");
					return FALSE;
				}					
			}			
			$this->banco->Execute("COMMIT");
			Grupo::buscar();
			return TRUE;
		} else {
			// seno ir dar rollback
			$this->banco->Execute("ROLLBACK");
			return FALSE;
		}
	}

	/**
	 * Salva as informaes pertinentes aos Cursos do Grupo de Avaliao
	 * @access private
	 * @param integer $num_alternativa Posio do vetor alternativa que ser utilizado nas operaes de inserir, atualizar e deletar alternativas
	 * @return boolean TRUE para sucesso.
	 * @since <04/04/2008> função criada no lugar da _insert_alternativa. O acesso ao banco de dados fica menor e mais rpido.
	 */	
	function _salvar_curso($num) {

		$vetor = $this->curso[$num];	
		$vetor['peso'] = empty($vetor['peso']) ? 'NULL' : $vetor['peso'] ;

		if($vetor['salvar'] == 'i') {
			// ----------- INSERIR NOVOS CURSOS	------------------- //
			$query = "INSERT INTO grupo_curso(id_grupo_aval, id_curso, 	vl_peso, vl_peso_mod, vl_peso_default, dt_modificado) ".
							" VALUES (".$this->id_grupo.", ".$vetor['curso'].", ".$vetor['peso'].", NULL, ".$vetor['peso'].", now())";
			$rs = $this->banco->Execute($query);
			if($rs) return TRUE;
			else return FALSE;		
		} elseif ($vetor['salvar'] == 'u') {
			// ------------- ATUALIZA OS CURSOS ------------ //		
			$query = "UPDATE grupo_curso SET vl_peso_mod = vl_peso, vl_peso = ".$vetor['peso'].", dt_modificado = now()".
							" WHERE id_grupo_aval = ".$this->id_grupo." AND id_curso = ".$vetor['curso'];
			$rs = $this->banco->Execute($query);
			if($rs) return TRUE;
			else return FALSE;				
		} elseif ($vetor['salvar'] == 'd') {
			$query = "DELETE FROM grupo_curso WHERE id_curso=".$vetor['curso']." AND id_grupo_aval=".$this->id_grupo;
			$rs = $this->banco->Execute($query);
			if($rs) return TRUE;
			else return FALSE;
		}
		return TRUE;
	}
	
	function _deletar_toda_avaliacao() {
		foreach ($this->aval as $k) {
			$aval = new Avaliacao($this->banco, $k);
			if(!$aval->deletar()) return FALSE;
		}	
		return TRUE;
	}

	/**
	 * Verifica se o peso dado a avaliacao est entre 0 e 100 em nmeros inteiros
	 * @param integer $peso Valor que ser comparado
	 * @access public
	 * @return integer | boolean Retorna FALSE se o valor no est entre 0 e 100, seno retorna o peso.
	 */
	function verifica_peso($peso){
		// o peso  um nmero inteiro de 0 a 100;
		$peso = (integer)$peso;
		if($peso < 0 && $peso > 100) return FALSE;
		return $peso;
	}
	
	/**
	 * Soma os pesos
	 */
	
	function soma_peso() {
		$soma = 0;
		if ($this->num_curso > 0) {
			for($x = 0; $x < $this->num_curso; $x++) {
				$soma += $this->curso['peso'];
			}
		}
		if($soma == 0 || $soma == 100) return TRUE;
		else return FALSE;		
	}

	/**
	 * Procura a posio de uma avaliao no vetor aval atravs de uma ID
	 * @access private
	 * @param integer $id ID da curso que deve ser encontrado
	 * @return integer | boolean Retorna a posio do vetor que se encontra o ID procurado ou FALSE se no houver
	 */  
	function procurar_posicao_avaliacao($id) {
		for($x = 0; $x < $this->num_aval; $x++){
			if($this->aval[$x] == $id) {
				return $x;
			}
		}
		return FALSE;
	}

	/**
	 * Procura a posio de um curso no vetor curso atravs de uma ID
	 * @access private
	 * @param integer $id ID da curso que deve ser encontrado
	 * @return integer | boolean Retorna a posio do vetor que se encontra o ID procurado ou FALSE se no houver
	 */
	function procurar_posicao_curso($id) {
		for($x = 0; $x < $this->num_curso; $x++){
			if($this->curso[$x]['curso'] == $id) {
				return $x;
			}
		}
		return FALSE;
  }

}
?>