

<?php


class Estatistica {

	var $populacao;
	var $num_populacao;
	
	var $media_aritmetica;
	var $desvio_padrao;
	var $variancia;
	
	var $amplitude_amostral;
	
	var $desvio_relacao_media;
	
	var $classe;
	
	var $frequencia_com_classe;

	
	function Estatistica() {
		Estatistica::inicializa_valores();
	}

	// populacao sem divisão de classes
	function set_populacao($vetor) {
		Estatistica::inicializa_valores();
		if(is_array($vetor)) $this->populacao = $vetor;
		else $this->populacao = array();
		$this->num_populacao = count($this->populacao);
	}
	
	function set_classe($vetor) {
		if(is_array($vetor)) {
			sort($vetor);
			$this->classe = $vetor;
		}	
		else $this->classe = array();	
	}


	/*
	----------------------------------------------------------------------------
	*									GETTERS
	----------------------------------------------------------------------------
	*/
	
	function get_media_aritmetica() {
		if (!$this->media_aritmetica) return Estatistica::media_aritmetica();
		else return $this->media_aritmetica;
	}
	
	function get_variancia() {
		if(!$this->variancia) return Estatistica::variancia();
		else return $this->variancia;
	}
	
	function get_desvio_padrao() {
		if(!$this->desvio_padrao) return Estatistica::desvio_padrao();
		else return $this->desvio_padrao;	
	}
	
	function get_maximo() {
		return max($this->populacao);
	}
	
	function get_minimo() {
		return min($this->populacao);
	}
	
	function get_frequencia_com_classe() {
		return $this->frequencia_com_classe;
	}
	
	function condicional($valor, $tipo = '>') {
		$cont = 0;
		switch ($tipo) {
			case '>':	
				foreach($this->populacao as $k) {
					if($k > $valor) $cont++;
				}
				break;
			case '<':
				foreach($this->populacao as $k) {
					if($k < $valor) $cont++;
				}			
				break;
			case '=':
				foreach($this->populacao as $k) {
					if($k == $valor) $cont++;
				}			
				break;
			case '>=':
				foreach($this->populacao as $k) {
					if($k >= $valor) $cont++;
				}			
				break;
			case '<=':
				foreach($this->populacao as $k) {
					if($k <= $valor) $cont++;
				}			
				break;		
		}
		return $cont;
	}
	

	function ordena_frequencia() {
		$aux = array();
		// coloca o limite inferior e limite superior
		for($x = 0; $x < count($this->classe) - 1; $x++) {
			array_push($aux, array('li' => $this->classe[$x], 'Li' => $this->classe[$x+1], 'frequencia' => 0));
		}		
		// coloca a populacao
		foreach($this->populacao as $k) {
			for($x = 0; $x < count($aux); $x++) {
				if($k >= $aux[$x]['li'] && $k <= $aux[$x]['Li']) {
					$aux[$x]['frequencia']++;
				}		
			}
		}		
		$this->frequencia_com_classe = $aux;
	}


	/**
	 * Freqüência sem intervalos de classe
	 */
	function frequencia_sem_intervalo_classe() {
		// ordena o vetor por ordem crescente
		asort($this->populacao);
		// cria um vetor com chave igual o valor da populacao 
		// e o valor é a soma dos valores da populacao
		$this->fsic = array_count_values($this->populacao);		
	}
	
	/**
	 * Freqüência com intervalos de classe
	 * @parm integer $k Número de classes que será dividida a amostra
	 */	
	function frequencia_ci($k) {
		// calcula o intervalo entre os valores
		$intervalo = round($this->populacao / $k); 
			
		$this->fcic = array();
		$aux = array_chunk($this->fsic, $intervalo, true); // quebra o vetor (fsic) em pedaços
		
		// buscar o li, Li e frequencia
		// array( 0 => array(0 => 'li', 1=> 'Li', 2=>frequencia)
		
			
		$frequencia = array();
		foreach($aux as $k) {
			array_push($frequencia, array_sum($k));
			foreach($k as $key){
				
			
			}			
		}
	}
	
	function media_aritmetica() {
		// Soma toda a amostra
		$soma = array_sum($this->populacao);
		// Divide pelo número de amostras	
		$this->media_aritmetica = $soma/$this->num_populacao;
	
		return $this->media_aritmetica;
	}
	
	function desvio_relacao_media() {
		// desvio em relacao a média
		$this->desvio_relacao_media = array();
		foreach($this->populacao as $pop) {
			array_push($this->desvio_relacao_media, $pop - $this->media_aritmetica);		
		}
		return $this->desvio_relacao_media;		
	}
	
	function desvio_padrao(){
		// somatório(populacao - media_aritmetica)²
		$soma = 0;
		
		if(!$this->desvio_relacao_media) Estatistica::desvio_relacao_media();
		
		foreach($this->desvio_relacao_media as $desvio) {
			$soma += pow($desvio, 2);
		}
		// somatório / número de população
		return $this->desvio_padrao = sqrt($soma/$this->num_populacao);	
	}
	
	function variancia() {
		if(!$this->desvio_padrao) Estatistica::desvio_padrao();
		return $this->variancia = pow($this->desvio_padrao, 2);
	}

	// calculo da amplitude amostral
	function amplitude_amostral() {
		$this->amplitude_amostral = max($this->fsic) - min($this->fsic);
	}
	
	function inicializa_valores() {
		$this->desvio_relacao_media = array();
		$this->num_populacao = 0;
		$this->media_aritmetica = 0;
		$this->desvio_padrao = 0;
		$this->variancia = 0;	
		$this->frequencia_com_classe = array();
		$this->populacao = array();
		$this->amplitude_amostral = 0;
	}


}



?>