<?php

/**
 * Classe para trabalhar com a Libera��o de Avalia��es
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avaliacao_Somativa
 * @subpackage Funcao_Avaliacao
 * @version 1.2 24/09/2007
 * @since 02/04/2008 Modifica��o nas opera��es (insert, updade e delete) diminuindo o overhead na base de dados. Necess�rio a utiliza��o da fun��o salvar() para qualquer fun��o de exclus�o
 */

/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	
 
class Liberar {

	/**
	 * @var object Objeto ADODB para acesso ao banco de dados
	 */
  var $banco;

	/**
	 * @var integer Identificador da libera��o (chave prim�ria)
	 */
  var $id;

	/**
	 * @var integer Identificador do grupo
	 */
	var $id_grupo;
	
	/**
	 * @var integer Identificador da avalia��o liberada
	 */
  var $id_aval;
	
	/**
	 * @var string Descri��o da Avalia��o
	 */
	var $descricao;
	
	/**
	 * @var boolean TRUE se a divulga��o for autom�tica
	 */
  var $divulga_auto;	
	
	/**
	 * @var boolean TRUE se a avalia�ao � presencial
	 */
  var $presencial;	
	
	
	var $randomica;
	
	var $nota_cancelada;
	
	/**
	 * @var date Data que foi realizada a libera��o
	 */
  var $data_liberacao;
	
	/**
	 * @var date Data que foi finalizada
	 */
  var $data_finalizacao;
	
	/**
	 * @var integer Status da libera��o
	 * @see PENDENTE, REALIZANDO, ENTREGUE
	 */
  var $status;
	
	/**
	 * @var date Data de divulga��o das notas da avalia��o realizada
	 */
  var $data_divulgacao;
	
	/**
	 * @var boolean Representa se a nota da avalia��o j� foi ou n�o divulgada
	 */
  var $divulgacao;

	/**
	 * Formato: array('aluno','curso','disciplina','envio','salvar');
	 * @var array Vetor dos alunos que participam da avalia��o liberada
	 */
  var $aluno;

	/**
	 * @var integer Vari�vel de controle do vetor aluno, indica a quantidade de alunos na avalia��o liberada
	 * @see Liberar::$aluno
	 */
  var $num_aluno;
	
	/**
	 * @var integer Vari�vel de controle do vetor aluno, indica o fim do vetor
	 * @see Liberar::$aluno
	 */		
  var $EOF;
	
	/**
	 * @var integer Vari�vel de controle do vetor aluno, indica a posicao atual do cursor
	 * @see Liberar::$aluno
	 */		
  var $pos_atual;


	/**
	 * Formato: array("questao", "tipo", "peso", "salvar")
	 * @var array Vetor que cont�m informa��es sobre quest�es da Avalia��o
	 */
  var $questao;

	/**
	 * @var integer Vari�vel de controle do vetor questao, indica a quantidade de quest�es na avalia��es
	 * @see Avaliacao::$questao
	 */
  var $num_questao;

	/**
	 * @var integer Vari�vel de controle do vetor questao, indica o fim do vetor
	 * @see Avaliacao::$questao
	 */	
  var $QEOF;
	
	/**
	 * @var integer Vari�vel de controle do vetor questao, indica a posicao atual do cursor
	 * @see Avaliacao::$questao
	 */			
  var $pos_questao_atual;


	/**
	 * Construtor da classe, se for passado o parametro $id � feito a busca automaticamente na base de dados
	 * @param ADOdb_* $obj_banco Objeto de conex�o com a base de dados
	 * @param integer $id Identificador da avalia��o liberada (chave prim�ria)
	 */
  function Liberar($obj_banco, $id=NULL) {
    global $A_DB_TYPE;
    if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE)) {
			die (A_LANG_AVS_ERROR_DB_OBJECT);
		}	

    $this->banco = $obj_banco; // seta o objeto para o banco de dados

    $this->aluno = array();
		$this->questao = array();
    // se haver um id passado j� realizar a busca
    $this->id = empty($id) ? NULL : (int)$id;
    if ($this->id) {
      Liberar::buscar();
    }else{
			$this->id_grupo = NULL;
      $this->id_aval = NULL;
			$this->descricao = NULL;
			$this->divulga_auto = NULL;
      $this->data_liberacao = NULL;
      $this->data_finalizacao = NULL;
      $this->status = ENCERRADA;
      $this->divulgacao = NULL;
      $this->data_divulgacao = NULL;
			$this->presencial = 0;
			$this->randomica = 0;
			$this->nota_cancelada = 0;
    }
    $this->num_aluno = count($this->aluno);
    $this->pos_atual = 0;
    $this->EOF = ($this->pos_atual == ($this->num_aluno));
		
		$this->num_questao = count($this->questao);
		$this->pos_questao_atual = 0;
		$this->QEOF = ($this->pos_questao_atual == ($this->num_questao));
  }

/*
--------------------------------------------------------------------------------
 																	SETTERS
--------------------------------------------------------------------------------	 
 */
  
	/**
	 * Seta o identificador da libera��o
	 * @param integer $valor ID da libera��o
	 * @access public
	 * @deprecated
	 */
	function set_id($valor) {
    $valor = (integer)$valor;
    $this->id = !empty($valor) ? $valor : NULL;
  }

	/**
	 * Seta o identificador da avalia��o
	 * @param integer $valor ID da avalia��o que ser� liberada
	 * @access public
	 */
	function set_avaliacao($valor) {
    $valor = (integer)$valor;
    $this->id_aval = !empty($valor) ? $valor : NULL;
  }

	/**
	 * Seta a data final de entrega da avalia��o pelos alunos
	 * @param date $valor Data de entrega
	 * @access public
	 */
  function set_data_final($valor) {
    $this->data_finalizacao = $valor;
  }

	/**
	 * Seta a data de in�cio da avalia��o
	 * @param date $valor Data de in�cio da avalia��o
	 * @access public
	 */
  function set_data_inicio($valor) {
    if($valor) $this->data_liberacao = $valor;
    else $this->data_liberacao = date('Y-m-d H:i:s');
  }
	
	function set_nota_cancelada($valor) {
		$valor = (int) $valor;
		$this->nota_cancelada = $valor;		
	}
	
	
	/**
	 * Libera ou encerra a avalia��o
	 * @param integer $valor Constante de libera��o ou encerramento
	 * @access public
	 * @see LIBERADA, ENCERRADA
	 */
  function liberar_avaliacao($valor) {
    $this->status = $valor == LIBERADA ? LIBERADA : ENCERRADA;
		if($this->status == ENCERRADA){
			for($x = 0; $x < $this->num_aluno; $x++) {
				if($this->aluno[$x]['status'] == PENDENTE) {
					$this->aluno[$x]['status'] = NAOENTREGUE;
					$this->aluno[$x]['nota']   = 0;
					$this->aluno[$x]['salvar'] = 'u';
				}	
			}
			if(is_null($this->data_finalizacao)) $this->data_finalizacao = date('Y-m-d H:i:s');	
		} else {
			if(is_null($this->data_liberacao)) $this->data_liberacao = date('Y-m-d H:i:s');
		}
  }

  /* -------------------------------------------------------
   *                      GETTER
	 * -------------------------------------------------------
   */
	 
 	/**
	 * Devolve o ID da avalia��o liberada
	 * @return integer
	 * @access public
	 */
  function get_id() {
    return $this->id;
  }

 	/**
	 * Devolve o ID da avalia��o
	 * @return integer
	 * @access public
	 */
  function get_avaliacao() {
    return $this->id_aval;
  }

 	/**
	 * Devolve a data de in�cio da avalia��o
	 * @return date
	 * @access public
	 */
  function get_data_inicio() {
    return $this->data_liberacao;
  }

 	/**
	 * Devolve a data de encerramento da avalia��o
	 * @return date
	 * @access public
	 */
  function get_data_final() {
    return $this->data_finalizacao;
  }

 	/**
	 * Devolve o status da avalia��o
	 * @return integer
	 * @access public
	 * @see LIBERADA, ENCERRADA
	 */
  function get_status(){
    return $this->status;
  }

 	/**
	 * Verifica se a nota da avalia��o j� foi divulgada
	 * @return boolean
	 * @access public
	 */
  function get_divulgacao() {
    return $this->divulgacao;
  }
 	/**
	 * Devolve a data de divulga��o da nota da avalia��o
	 * @return date
	 * @access public
	 */
  function get_data_divulgacao() {
    return $this->data_divulgacao;
  }
	
	/**
	 * Retorna se a avalia��o est� sendo divulgada automaticamente
	 * @access public
	 * @return boolean TRUE para divulga��o automatica
	 */
  function get_divulga_auto() {
    return $this->divulga_auto;
  }

	/**
	 * Retorna o ID do grupo
	 * @access public
	 * @return integer
	 */
  function get_grupo() {
    return $this->id_grupo;
  }

	/**
	 * Retorna a descri��o da avalia��o
	 * @access public
	 * @return string 
	 */
  function get_descricao() {
    return $this->descricao;
  }
	
	function get_randomica() {
		return $this->randomica;
	}
	
	function get_nota_cancelada() {
		return $this->nota_cancelada;
	}	

	function get_presencial() {
		return $this->presencial;
	}

/*
 ---------------------------------------------------------------------------
						Fun��es utilizadas para as quest�es
 ---------------------------------------------------------------------------
*/	
	
	/**
	 * Coloca as quest�es de modo aleat�rio na prova
	 * @access public
	 */
  function randomize() {
    // semelhante a fun��o randomize das alternativas
    srand((float)microtime()*1000000);
    shuffle($this->questao);
  }

	
  function buscar_questao() {
    if ($this->num_questao == 0) return FALSE;
    if ($this->QEOF) {
      $this->pos_questao_atual = 0;
      $this->QEOF = FALSE;
      return FALSE;
    }
    $retorno = $this->questao[$this->pos_questao_atual];
    if(!$this->QEOF) {
      // se estiver em qualquer outra casa do vetor
      $this->pos_questao_atual++;
      $this->QEOF = ($this->pos_questao_atual == ($this->num_questao)); // verifica se n�o � fim de array
    }
    return $retorno;
  }

	/**
	 * Retorna a quantidade de quest�es inseridas na avalia��o
	 * @access public
	 * @return integer Quantidade de quest�es
	 */
  function numero_questao() {
    return $this->num_questao;
  }

	/**
	 * Retorna as informa��es de uma quest�o. (Utilize com o comando For)
	 * @access public
	 * @param integer $pos Posi��o do vetor
	 * @return Array Vetor com as informa��es da quest�o
	 */
  function questao($pos) {
    return $this->questao[$pos];
  }
	
	function questao_vetor() {
		return $this->questao;
	}

	/**
	 * Retorna as informa��es de uma quest�o atrav�s do ID
	 * @access public
	 * @param integer $questao ID da quest�o
	 * @return Array Vetor com as informa��es da quest�o
	 */
  function questao_identificador($questao) {
		$questao = (int) $questao;
    $pos = Liberar::procurar_posicao_questao($questao);
    if($pos === FALSE) return FALSE;
    return $this->questao[$pos];
  }


	/**
	 * Fun�ao para cancelar uma questao adicionando o valor da questao como soma no final da prova
	 * @param integer $questao Identificador da questao
	 * @return boolean
	 */
	function cancelar_questao($questao) {
		$pos = Liberar::procurar_posicao_questao($questao);
		if($pos === FALSE) return FALSE;
		
		$this->nota_cancelada += $this->questao[$pos]['peso'];
		
		$this->questao[$pos]['cancelada'] = CANCELADA;
		$this->questao[$pos]['data_cancela'] = date('Y=m-d H:i:s');
		$this->questao[$pos]['modificada'] = $this->questao[$pos]['peso'];
		$this->questao[$pos]['peso'] = 0;	
		$this->questao[$pos]['salvar'] = 'u';
		
		return TRUE;	
	}

	/**
	 * Fun�ao para cancelar questoes, distribuindo o peso igualmente entre as questoes
	 * @param array $v_questao Vetor com os identificadores das questoes
	 * @return boolean
	 */
	function cancelar_questao_distribuir($v_questao) {	
		// $v_questao = vetor com as questoes que estao sendo canceladas
		// busca o valor total das questoes para serem distribu�das			
		for($x = 0; $x < count($v_questao); $x++) {
			$pos = Liberar::procurar_posicao_questao($v_questao[$x]);				
			
			if($pos !== FALSE) {
				$this->questao[$pos]['cancelada'] = CANCELADA;
				$this->questao[$pos]['data_cancela'] = date('Y=m-d H:i:s');
				$this->questao[$pos]['modificada'] = $this->questao[$pos]['peso'];				
				$this->questao[$pos]['peso'] = 0;				
				$this->questao[$pos]['salvar'] = 'u';
			}
		}

		// pega o n�mero de questoes canceladas
		$num_nao_cancel = 0;		
		$peso_restante  = 0;
		foreach($this->questao as $questao) {
			if($questao['cancelada'] == NAOCANCELADA) {
				$num_nao_cancel++;
				$peso_restante += $questao['peso'];
			}	
		}		
		
		// distribui o peso entre as outras questoes
		$peso_distribuir = floor( ( 100 - $this->nota_cancelada - $peso_restante )/$num_nao_cancel);
		// confere se nao houve perca de pontos
		$sobra = fmod(( 100 - $this->nota_cancelada - $peso_restante ), $num_nao_cancel); // a sobra		

		for($x = 0; $x < $this->num_questao; $x++) {
			if($this->questao[$x]['cancelada'] == NAOCANCELADA ) {
				$this->questao[$x]['peso'] += $peso_distribuir;
				$this->questao[$x]['salvar'] = 'u';				
			}	
		}
		
		// se houve sobra, adiciona na �ltima questao
		if($sobra > 0) {
			for($x = 0; $this->num_questao; $x++) {
					if($this->questao[$x]['cancelada'] == NAOCANCELADA) {
						$this->questao[$x]['peso'] += $sobra;
						$this->questao[$x]['salvar'] = 'u';
						break;
					}	
			}	
		}				
	
		return TRUE;	
	}
	
	function pode_cancelar($num_questao_cancelada) {
		$num = 0;
		for($x = 0; $x < $this->num_questao; $x++) {
			if($this->questao[$x]['cancelada'] == NAOCANCELADA) $num++;		
		}	
		if(($num - $num_questao_cancelada) > 0) return TRUE;
		else return FALSE;		
	}
	
	
	/**
	 * Modifica o peso de uma questao
	 * @param integer $questao Identificador da questao
	 * @param integer $peso Peso para a questao 
	 * @return boolean
	 */	
	function modificar_peso($questao, $peso) {
		$pos = Liberar::procurar_posicao_questao($questao);
		if($pos === FALSE) return FALSE;

		$peso = (int) $peso;
		
		$this->questao[$pos]['modificada'] = $this->questao[$pos]['peso'];
		$this->questao[$pos]['peso'] = $peso;
		$this->questao[$pos]['salvar'] = 'u';		
		if ($peso == 0) {
			$this->questao[$pos]['cancelada'] = CANCELADA;
			$this->questao[$pos]['data_cancelada'] = date('Y-m-d H:i:s');
		} else {	
			$this->questao[$pos]['cancelada'] = NAOCANCELADA;
			$this->questao[$pos]['data_cancelada'] = 'NULL';
		}	
	
		return TRUE;	
	}

	function desfazer_cancelar($questao) {
		$pos = Liberar::procurar_posicao_questao($questao);
		if($pos === FALSE) return FALSE;

		$peso = $this->questao[$pos]['modificada'];
		$this->nota_cancelada -= $peso;
		$this->questao[$pos]['peso'] = $peso;
		$this->questao[$pos]['modificada'] = $this->questao[$pos]['peso'];
		$this->questao[$pos]['cancelada'] = NAOCANCELADA;
		$this->questao[$pos]['data_cancelada'] = 'NULL';
		$this->questao[$pos]['salvar'] = 'u';	
		return TRUE;
	}


	/**
	 * Volta com os pesos default de todas as questoes 
	 * @return boolean
	 */		
	function peso_default() {
		for($x =0; $x < $this->num_questao; $x++) {
			$this->questao[$x]['modificada'] = $this->questao[$x]['peso'];
			$this->questao[$x]['peso'] = $this->questao[$x]['default'];
			$this->questao[$x]['salvar'] = 'u';
			$this->questao[$x]['cancelada'] = NAOCANCELADA;
			$this->questao[$x]['data_cancelada'] = 'NULL';
		}
		return TRUE;
	}

	function peso_modificado() {
		for($x =0; $x < $this->num_questao; $x++) {
			$peso = $this->questao[$x]['modificada'];
			$this->questao[$x]['modificada'] = $this->questao[$x]['peso'];
			$this->questao[$x]['peso'] = $peso;
			$this->questao[$x]['salvar'] = 'u';
			if ($peso == 0) {
				$this->questao[$x]['cancelada'] = CANCELADA;
				$this->questao[$x]['data_cancelada'] = date('Y-m-d H:i:s');
			} else {	
				$this->questao[$x]['cancelada'] = NAOCANCELADA;
				$this->questao[$x]['data_cancelada'] = 'NULL';
			}	
		}
		return TRUE;	
	}

	function verifica_nota_presencial() {
		if($this->presencial) {
			foreach($this->aluno as $aluno) {
				if(is_null($aluno['nota']) || $aluno['nota'] == 0) return FALSE;
			}	
			return TRUE;	
		}
		return FALSE;
	}
	
/* 
---------------------------------------------------------------------------
								Fun��es para trabalhar com os alunos
---------------------------------------------------------------------------
*/
  /**
   * Verifica se o aluno est� na avalia��o liberada
   * @access public
	 * @param integer $aluno ID do aluno
	 * @param integer $curso ID do curso
	 * @param integer $disciplina ID da disciplina
	 * @return boolen TRUE se o aluno estiver com a avalia��o liberada
   */
  function avaliacao_liberada_aluno($libera_aluno) {
    // se a avaliacao est� liberada		
    if($this->status == LIBERADA) {
      $pos = Liberar::procurar_posicao_libera_aluno($libera_aluno);
			if($pos === FALSE) return FALSE;
			else return TRUE;
    }
    return FALSE;
  }
	
	function get_identificador_aluno($aluno, $curso, $disc) {
		$pos = Liberar::procurar_posicao_aluno($aluno, $curso, $disc);
		if($pos === FALSE) return FALSE;
		else return $this->aluno[$pos]['id'];	
	}
	
	function aluno_identificador($libera_aluno) {
    $pos = Liberar::procurar_posicao_libera_aluno($libera_aluno);
    if($pos === FALSE) return FALSE;
    return $this->aluno[$pos];	
	}

	/**
	 * Retorna o status do aluno em rela��o a avalia��o
   * @access public
	 * @param integer $aluno ID do aluno
	 * @param integer $curso ID do curso
	 * @param integer $disciplina ID da disciplina
	 * @return integer Status do aluno em rela��o a avalia��o
	 * @see PENDENTE, ENTREGUE, REALIZANDO
	 */
  function get_status_aluno($libera_aluno) {
    $pos = Liberar::procurar_posicao_libera_aluno($libera_aluno);
    if($pos === FALSE) return FALSE;
    return $this->aluno[$pos]['status']	;
  }

  /**
   * Fun��o para adicionar alunos na lista da avalia��o liberada
   * @access public
	 * @param integer $aluno ID do aluno
	 * @param integer $curso ID do curso
	 * @param integer $disciplina ID da disciplina
	 * @param date $envio Data do envio da avalia��o pelo aluno
	 * @return integer Status do aluno em rela��o a avalia��o
	 * @since 07/04/2008 Modifica��o no modo de conex�o com a base de dados, agora � necess�rio salvar
	 * @see Liberar::salvar();
   */
  function aluno_adicionar($aluno, $curso, $disciplina,$envio='NULL') {
		$aluno = array('aluno'      =>$aluno,
									 'curso'      =>$curso, 
									 'disciplina' => $disciplina, 
									 'envio'      => $envio,
									 'salvar'     => 'i');
    $pos = array_push($this->aluno, $aluno) -1;
    $this->num_aluno++;
    $this->EOF = ($this->pos_atual == ($this->num_aluno));
		return $pos;
  }

	/**
   * Fun��o para alterar os dados do aluno
   * @access public
	 * @param integer $aluno ID do aluno
	 * @param integer $curso ID do curso
	 * @param integer $disciplina ID da disciplina
	 * @param integer $status Status da avalia��o do aluno
	 * @param date $envio Data da entrega da avalia��o pelo aluno
	 * @return integer Status do aluno em rela��o a avalia��o
	 * @since 07/04/2008 Modifica��o no modo de conex�o com a base de dados para melhorar o desempenho
	 * @see Liberar::salvar();
	 * @see PENDENTE, REALIZANDO, ENTREGUE
   */
  function aluno_alterar($libera_aluno, $status = FALSE) {
    $pos = Liberar::procurar_posicao_libera_aluno($libera_aluno);
    if($pos === FALSE) return FALSE;
    $this->aluno[$pos]['status'] = $status;
		$this->aluno[$pos]['salvar'] = 'u';
		switch ($status) {
			case REALIZANDO: 
				$this->aluno[$pos]['inicio'] = date('Y-m-d H:i:s');
				break;
			case ENTREGUE:	
    		$this->aluno[$pos]['envio'] = date('Y-m-d H:i:s'); // coloca a data atual	
				break;
		}
	
    return TRUE;
  }
	
	
	function aluno_nota($libera_aluno, $nota) {
    $pos = Liberar::procurar_posicao_libera_aluno($libera_aluno);
    if($pos === FALSE) return FALSE;
		
		$nota = (int) $nota;
		if($nota >= 0 && $nota <= 100) {		
			$this->aluno[$pos]['nota'] = $nota;
			$this->aluno[$pos]['salvar'] = 'u';
			return TRUE;
		} else {
			return FALSE;
		}				
	}
	
	
	/**
   * Deleta as informa��es do aluno da avalia��o liberada
   * @access public
	 * @param integer $aluno ID do aluno
	 * @param integer $curso ID do curso
	 * @param integer $disciplina ID da disciplina
	 * @param integer $status Status da avalia��o do aluno
	 * @return boolean TRUE se deletado com sucesso
	 * @since 07/04/20008 Cria��o da fun��o
	 * @see Liberar::salvar();
   */
	function aluno_deletar($libera_aluno) {
    $pos = Liberar::procurar_posicao_libera_aluno($libera_aluno);
    if($pos === FALSE) return FALSE;
		$this->aluno[$pos]['salvar'] = 'd';
		return TRUE;	
	}
	
	function alunos() {
		return $this->aluno;	
	}
	
	function aluno_libera($libera_aluno) {
    $pos = Liberar::procurar_posicao_libera_aluno($libera_aluno);
    if($pos === FALSE) return FALSE;
		return $this->aluno[$pos];	
	}


	function numero_alunos() {
		return count($this->aluno);
	}
	/**
	 * Divulga a nota da avalia��o
	 * @access public
	 * @return boolean TRUE se a divulga��o foi realizada com sucesso
	 */
  function divulga_nota(){
		if($this->status == ENCERRADA) { // verifica se a avalia�ao est� encerrada
			if($this->presencial)	{	 
				
				if(Liberar::verifica_nota_presencial()) {
					$this->divulgacao = 1;
					$this->data_divulgacao = date('Y-m-d H:i:s');
					return TRUE; // se for uma avalia�ao presencial, verifica se todos os alunos receberam notas				
				} else return FALSE;
								
			}	else {
				
				$corrigida = TRUE;
				foreach($this->aluno as $k) {	 // se for uma avalia�ao automatica, verifica se todas as questoes foram corrigidas para cada aluno
					$resposta = new Resposta($this->banco, $k['id']);
					$corrigida = $resposta->verificar_questao_corrigida();					
					unset($resposta);
					if(!$corrigida) return FALSE; // retorn false se nao foram corrigidas
				}
				$this->divulgacao = 1;
				$this->data_divulgacao = date('Y-m-d H:i:s');
				return TRUE; // retorn true se todas foram corrigidas			
			}		
		}
		
		if($this->divulga_auto) {
				$this->divulgacao = 1;
				$this->data_divulgacao = date('Y-m-d H:i:s');
				return TRUE; // retorn true se todas foram corrigidas						
		}		
		return FALSE; // retorna false se ainda nao foi encerrada
  }
	
	/**
	 * Cancela a divulga�ao da nota da avalia��o. � necess�rio salvar
	 * @access public
	 */
  function cancela_divulgacao(){
    $this->divulgacao = 0;
		$this->data_divulgacao = NULL;
  }

/*
--------------------------------------------------------------------------------------
													Banco de Dados
--------------------------------------------------------------------------------------
*/

	/**
	 * Faz a busca das informa��es sobre a Avalia��o Liberada na base de dados
	 * @access public
	 */
  function buscar() {
    $query = "SELECT * FROM liberar_aval WHERE id_libera=".$this->id;
		
    $rs = $this->banco->Execute($query);

    if($rs->NumRows() > 0) {
			$this->id               = $rs->Fields('id_libera');
      $this->id_aval          = $rs->Fields('id_avaliacao');
      $this->data_finalizacao = $rs->Fields('dt_encerrar');
      $this->data_liberacao   = $rs->Fields('dt_liberar');
      $this->status           = $rs->Fields('status_libera');
      $this->divulgacao       = $rs->Fields('divulgacao_nota');
      $this->data_divulgacao  = $rs->Fields('dt_divulgacao');
			$this->id_grupo         = $rs->Fields('id_grupo_aval');
			$this->descricao        = $rs->Fields('ds_avaliacao'); 
			$this->divulga_auto     = $rs->Fields('bl_divulg_nota_auto');
			$this->presencial       = $rs->Fields('bl_presencial');			
			$this->randomica        = $rs->Fields('bl_randomica');
			$this->nota_cancelada   = $rs->Fields('vl_nota_cancelada');
			
			// busca as informa��es sobre os alunos que pertencem a Avalia��o Liberada
      $qaluno = "SELECT id_libera_aluno AS id, id_usuario AS aluno, nome_usuario AS nome, id_disc AS disciplina, nome_disc AS nome_disciplina,".
								" id_curso AS curso, nome_curso, dt_inicio AS inicio, dt_envio AS envio, status_aval AS status, vl_nota AS nota" .
                " FROM liberar_aluno LEFT JOIN matricula USING(id_curso, id_disc, id_usuario) LEFT JOIN usuario USING (id_usuario)".
								" LEFT JOIN disciplina USING(id_disc) LEFT JOIN curso USING(id_curso)".
								" WHERE id_libera = ".$this->id;							
								
			$this->aluno = array();
			$this->questao = array();

      $rs_aluno = $this->banco->Execute($qaluno);
      // se encontrado algo
      if($rs_aluno->NumRows() > 0) {
        while($aluno = $rs_aluno->FetchRow()) {
					$a = array( "id"              => $aluno['id'],
											"aluno"           => $aluno['aluno'],
											"nome"            => $aluno['nome'],
											"disciplina"      => $aluno['disciplina'],
											"nome_disciplina" => $aluno['nome_disciplina'],
											"curso"           => $aluno['curso'],
											"nome_curso"      => $aluno['nome_curso'],
											"status"          => $aluno['status'],
											"inicio"					=> $aluno['inicio'],
											"envio"						=> $aluno['envio'],
											"nota"            => $aluno['nota'],
											"salvar"          => "x"
										);	
          array_push($this->aluno, $a);
        }
        $this->num_aluno = count($this->aluno);
        $this->EOF = ($this->pos_atual == ($this->num_aluno));
      } 
			
			// selecionar as questoes pertencetes
			$qquestao = "SELECT id_questao AS questao, vl_peso AS peso, tipo_questao AS tipo, vl_peso_mod AS modificada, vl_peso_default AS peso_default,". 
									" bl_cancelada AS cancelada, dt_cancelada AS data_cancelada".
									" FROM liberar_questao INNER JOIN questao USING(id_questao) WHERE id_libera=".$this->id;						
	
			$rs_questao = $this->banco->Execute($qquestao);
			
			if (!$rs_questao) return FALSE;
			// montar o vetor com as questoes
			
			while($questao = $rs_questao->FetchRow()) {
				$q = array( "questao"      => $questao['questao'], 
										"tipo"         => $questao['tipo'], 
										"peso"         => $questao['peso'], 
										"modificada"   => $questao['modificada'],
										"default"      => $questao['peso_default'],
										"cancelada"    => $questao['cancelada'],
										"data_cancela" => $questao['data_cancelada'],
										"salvar"=>'x');
				array_push($this->questao, $q);
				
//				if ($questao['tipo'] == DISSERTATIVA) $this->pode_divulgar = FALSE;
			}
			$this->num_questao = count($this->questao);
			$this->pos_questao_atual = 0;
			$this->QEOF = ($this->pos_questao_atual == ($this->num_questao));
			return TRUE;		
    } else {
      $this->id = NULL;
    }
  }

	/**
	 * Exclui uma Avalia��o Liberada
	 * @access public
	 * @return boolean TRUE se exclu�do com sucesso
	 */
  function deletar() {
    if($this->id) {
      $query = "DELETE liberar_aval WHERE id_libera =".$this->id;
      $rs1 = $this->banco->Execute($query);
			$query = "DELETE liberar_aluno WHERE id_libera =".$this->id;
      $rs2 = $this->banco->Execute($query);						
			$query = "DELETE liberar_questao WHERE id_libera =".$this->id;
      $rs3 = $this->banco->Execute($query);									
      if($rs1 && $rs2 && $rs3) return TRUE;
      else return FALSE;
    }
  }

	/**
	 * Salva as informa��es sobre a Avalia��o Liberada
	 * @access public
	 * @return boolean TRUE em caso de sucesso
	 */
  function salvar() {
    // id da avalia��o � obrigat�rio
    if(!empty($this->id_aval)) {
      if(!empty($this->id)) return Liberar::_atualizar();
      else return Liberar::_inserir();
    } else return FALSE;
  }

	/**
	 * Insere na base de dados
	 * @see Liberar::salvar()
	 * @access private
	 * @return boolean
	 */
  function _inserir() {
    
		$aval = new Avaliacao($this->banco, $this->id_aval); // busca a info das avalia��es
		
		if(!is_null($aval->get_avaliacao())) {					
			$this->banco->Execute("START TRANSACTION");			
			
			if($aval->get_presencial()) {
				$query = "INSERT INTO liberar_aval(id_libera, id_grupo_aval, id_avaliacao, ds_avaliacao, bl_divulg_nota_auto, bl_presencial, bl_randomica, dt_liberar, dt_encerrar, status_libera, dt_divulgacao, divulgacao_nota) " .
								 "VALUES(NULL, ".$aval->get_grupo().", ".$this->id_aval.", '".$aval->get_descricao()."', ".$aval->get_divulgacao().",".$aval->get_presencial().", ".$aval->get_randomica().", '".$this->data_liberacao."','NULL', ".ENCERRADA.", NULL,NULL)";			
			} else	{				
				// insere as info sobre a libera��o da avalia��o
				$query = "INSERT INTO liberar_aval(id_libera, id_grupo_aval, id_avaliacao, ds_avaliacao, bl_divulg_nota_auto, bl_presencial, bl_randomica, dt_liberar, dt_encerrar, status_libera, dt_divulgacao, divulgacao_nota) " .
								 "VALUES(NULL, ".$aval->get_grupo().", ".$this->id_aval.", '".$aval->get_descricao()."', ".$aval->get_divulgacao().",".$aval->get_presencial().", ".$aval->get_randomica().", '".$this->data_liberacao."','NULL', ".$this->status.", NULL,NULL)";

			}
			$rs = $this->banco->Execute($query);
			
			if($rs) {
				$this->id = $this->banco->Insert_ID();
				// faz a inser��o das quest�es
				$qquestao = "INSERT INTO liberar_questao (id_libera, id_questao, vl_peso, vl_peso_default,  bl_cancelada)".
										" SELECT ".$this->id.", id_questao, vl_peso_questao, vl_peso_questao AS peso_default, 0 FROM aval_questao WHERE id_avaliacao = ".$this->id_aval;
										
				$rs = $this->banco->Execute($qquestao);
				// cancela toda a opera��o se n�o conseguir inserir as quest�es
				
				if(!$rs) { 
						$this->banco->Execute("ROLLBACK");
						return FALSE;		
				}
										
				for($x = 0; $x < $this->num_aluno; $x++) {
					// insere as informa��es sobre os aluno liberados;
					if(!Liberar::_salvar_aluno($x)) {
						$this->banco->Execute("ROLLBACK");
						return FALSE;
					}
				}
				// se tudo correu como desejado ir� ser aplicada as altera��es
				$this->banco->Execute("COMMIT"); 
				Liberar::buscar();
				return TRUE;
			} else { // erro se n�o conseguir inserir na liberar_aval
				$this->banco->Execute("ROLLBACK");
				return FALSE;
			}
		} else { // se n�o encontrar a avalia��o
			return FALSE;		
		}	
  }

	/**
	 * Atualiza (Update) as informa��es na base de dados
	 * @access private
	 * @return boolean
	 * @see Liberar::salvar()
	 */		 
  function _atualizar() {
    
		$this->banco->Execute("START TRANSACTION");
		
		$data_divulgacao  = $this->banco->qstr(date('Y-m-d H:i:s'));
		$data_liberacao   = $this->banco->qstr(date('Y-m-d H:i:s'));
		$data_finalizacao = $this->banco->qstr(date('Y-m-d H:i:s'));
		
		$query = "UPDATE liberar_aval SET id_avaliacao=".$this->id_aval.
             ",dt_liberar=".$data_liberacao.
						 ",dt_encerrar=".$data_finalizacao.
             ",status_libera =".$this->status.
             ",divulgacao_nota=".($this->divulgacao == NULL || $this->divulgacao == 0 ? 'NULL' : '1').
             ",dt_divulgacao= ".$data_divulgacao.
						 ",vl_nota_cancelada = ".($this->nota_cancelada == NULL ? 0 : $this->nota_cancelada).						 
             " WHERE id_libera=".$this->id;
					 
    $rs = $this->banco->Execute($query);
    if($rs) {
      for($x = 0; $x < $this->num_aluno; $x++) {
        if(!Liberar::_salvar_aluno($x)) {
          $this->banco->Execute("ROLLBACK");
          return FALSE;
        }
      }
			
      for($x = 0; $x < $this->num_questao; $x++) {
        if(!Liberar::_salvar_questao($x)) {
          $this->banco->Execute("ROLLBACK");
          return FALSE;
        }
      }	
			
			$this->banco->Execute("COMMIT");
			Liberar::buscar();
			return TRUE;
    } else {
			$this->banco->Execute("ROLLBACK");
			return FALSE;
		}
  }

	function _salvar_questao($pos) {		
		$vetor = $this->questao[$pos]; // pega os dados dos alunos

		if($vetor['salvar'] == 'u') {
			// ---------------- ATUALIZA UM REGISTRO DA QUESTAO	 -------------------- //

			$vetor['data_cancela'] = is_null($vetor['data_cancela']) ? 'NULL' : $vetor['data_cancela'];
	
			$query = "UPDATE liberar_questao SET vl_peso = '".$vetor['peso']."'".
							" ,vl_peso_mod = '".$vetor['modificada']."'".
							" ,bl_cancelada = '".$vetor['cancelada']."'".
							" ,dt_cancelada = '".$vetor['data_cancela']."' ".
							" WHERE id_questao =".$vetor['questao']." AND id_libera =".$this->id;						 
			if($rs = $this->banco->Execute($query)) return TRUE;
			else return FALSE;
		}
		return TRUE;
	
	}

	/** 
	 * Salva (Insert, Update, Delete) as informa��es do aluno
	 * @access private
	 * @param integer $pos Posi��o do vetor aluno que ser� trabalhado
	 * @return boolean TRUE para opera��o realizada com sucesso
	 */
  function _salvar_aluno($pos) {
    
		$vetor = $this->aluno[$pos]; // pega os dados dos alunos
		
		if($vetor['salvar'] == 'i') {
			// ---------------- FAZ A INSER��O DE NOVOS ALUNOS --------------------- //			
			$del = Liberar::_deletar_aluno($pos); // retira-se as informa��es anteriores sobre o aluno e avalia��o
			// se deletado com sucesso
			if($del) {			
				$query = "INSERT INTO liberar_aluno(id_libera_aluno, id_libera, id_usuario, id_curso, id_disc, dt_envio, status_aval)".
								 " VALUES(NULL, ".$this->id.",".$vetor['aluno'].",".$vetor['curso'].",".$vetor['disciplina'].", ".$vetor['envio'].", 0)";
				if($this->banco->Execute($query)) return TRUE;
				else return FALSE;
			} else	return FALSE;		
		} elseif ($vetor['salvar'] == 'u') {
			// ---------------- ATUALIZA UM REGISTRO DO ALUNO	 -------------------- //
			$query = "UPDATE liberar_aluno SET dt_envio = ".($vetor['envio'] == NULL ? 'NULL' : "'".$vetor['envio']."'").	", status_aval=".$vetor['status'].
							 ", vl_nota=".(is_null($vetor['nota']) ? 0 : $vetor['nota']).
							 " WHERE id_libera='".$this->id."' AND id_usuario = ".$vetor['aluno']." AND id_curso = ".$vetor['curso'].
							 " AND id_disc = ".$vetor['disciplina'];
					 
			if($rs = $this->banco->Execute($query)) return TRUE;
			else return FALSE;
		
		} elseif($vetor['salvar'] == 'd') {
			// ---------------- DELETA AS INFORMA��ES DO ALUNO -------------------- //
			$del = Liberar::_deletar_aluno($pos); // retira-se as informa��es anteriores sobre o aluno e avalia��o
			if($del) {
				$query = "DELETE FROM libera_aluno".
								 " WHERE id_libera='".$this->id."' AND id_usuario = ".$vetor['aluno']." AND id_curso = ".$vetor['curso'].
		 						 " AND id_disc = ".$vetor['disciplina'];

				$rs = $this->banco->Execute($query);				 
				if($rs) return TRUE;
				else return FALSE;
			} else 	return FALSE;
		}
		return TRUE;		
  }

	/** 
	 * Retira os dados de avalia��es j� realizadas anteriormente pelo aluno.
	 * @access private
	 * @param integer $pos Posi��o do vetor aluno que ser� deletado
	 * @return boolean TRUE para opera��o realizada com sucesso
	 * @see Liberar::_salvar_aluno();
	 */
	function _deletar_aluno($pos) {
	
		$vetor = $this->aluno[$pos];
		  	
		// seleciona todas as avalia��es do grupo, para serem deletadas		
		$query = "SELECT id_avaliacao FROM avaliacao WHERE id_grupo_aval IN ( ".
						 "SELECT id_grupo_aval FROM grupo_aval INNER JOIN avaliacao USING ( id_grupo_aval ) ".
						 "WHERE id_avaliacao = '".$this->id_aval."' )";

		$rs = $this->banco->Execute($query);			
		
		if(!$rs) return FALSE;
		
		$vet_avaliacao = array();
		$avaliacao = "";
		
		while($array = $rs->FetchRow()) {
			$vet_avaliacao[] = $array['id_avaliacao'];
		}
		// coloca todo o vetor dentro de uma string com os id das avalia��es
		$avaliacao = implode(",", $vet_avaliacao);
   
	  // deleta todas as informa��es do aluno sobre a avaliacao	
		$query_del = "DELETE liberar_aluno, resposta".
								 " FROM liberar_aval INNER JOIN liberar_aluno USING (id_libera)".
								 " LEFT JOIN resposta USING (id_libera_aluno)".
								 " WHERE id_avaliacao IN (".$avaliacao.") AND id_usuario = ".$vetor['aluno'].
								 " AND id_curso = ".$vetor['curso']." AND id_disc = ".$vetor['disciplina'];
	 
		if($this->banco->Execute($query_del)) return TRUE;
		else return FALSE;						 
		
	}

	/**
	 * Procura a posi��o de um aluno no vetor aluno
	 * @access private
	 * @param integer $aluno ID do aluno
	 * @param integer $curso ID do curso
	 * @param integer $disciplina ID da disciplina
	 * @return integer | boolean Retorna a posi��o do vetor que se encontra o ID procurado ou FALSE se n�o houver
	 */  
  function procurar_posicao_aluno($aluno, $curso, $disciplina) {
     for($x = 0; $x < $this->num_aluno; $x++){
      if($this->aluno[$x]['aluno'] == $aluno && $this->aluno[$x]['curso'] == $curso && $this->aluno[$x]['disciplina'] == $disciplina) {
        return $x;
      }
    }
    return FALSE;
  }


  function procurar_posicao_libera_aluno($id) {
     for($x = 0; $x < $this->num_aluno; $x++){
      if($this->aluno[$x]['id'] == $id) {
        return $x;
      }
    }
    return FALSE;
  }



	/**
	 * Procura a posio de uma questo no vetor questo atravs de uma ID
	 * @access private
	 * @param integer $id ID da questo que deve ser encontrada
	 * @return integer | boolean Retorna a posio do vetor que se encontra o ID procurado ou FALSE se no houver
	 */
  function procurar_posicao_questao($id) {
    for($x = 0; $x < $this->num_questao; $x++){
      if($this->questao[$x]['questao'] == $id) {
        return $x;
      }
    }
    return FALSE;
  }
	
} // fim da classe Libera


/*
 * Constants
 */
 
/**
 * Constante que define se a avalia��o est� liberada
 */
define("LIBERADA", 1);

/**
 * Constante que define se a avalia��o est� encerrada
 */
define("ENCERRADA", 0);

/**
 * Constante que define se a avalia��o est� pendente (n�o submitida) pelo aluno
 */
define('PENDENTE',0);

/**
 * Constante que define se a avalia��o est� sendo realizada pelo aluno
 */
define('REALIZANDO',1);

/**
 * Constante que define se a avalia��o foi entregue pelo aluno
 */
define('ENTREGUE',2);

define('ZERADA',3);

define('NAOENTREGUE',4);


define("CANCELADA",1);

define("NAOCANCELADA",0);

?>
