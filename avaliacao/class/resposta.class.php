<?php

/**
 * Classe de Resposta a uma avalia��o, aluno, curso e disciplina espec�ficos
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avaliacao_Somativa
 * @subpackage Resposta
 * @version 1.2 05/04/2008
 */


/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	
 
 
define("NAOCORRIGIDA",-1);

define("ERRADA",0);

define("CORRETA",1);

define("MEIO",2);
 
class Resposta extends Liberar {

  // chaves
	var $id_resposta;
	
  // valor da nota
	var $nota_salva;
  var $nota; // nota da avalia��o considerando apenas os acertos
	
	var $questao_pendente;

  var $resposta;
  var $num_resposta;
  var $EOF_R;
  var $pos_atual_resposta;

  function Resposta($obj_banco, $id_libera_aluno) {
    global $A_DB_TYPE;
    if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
      die (A_LANG_AVS_ERROR_DB_OBJECT);
			
		$this->banco = $obj_banco;	

    $this->id_resposta = (int) $id_libera_aluno;
		
		if(!Resposta::buscar_libera()) die();
		else Liberar::buscar();
		
		// seta as chaves
		$this->resposta = array();
		$this->num_resposta = NULL;
		$this->EOF_R = NULL;
		$this->pos_atual_resposta = NULL;	
		$this->nota = 0;		
		$this->nota_salva = FALSE;
		$this->questao_pendente = array();
		
		Resposta::buscar();
  }

/*
--------------------------------------------------------------------------------------
																				GETTERS
--------------------------------------------------------------------------------------
 */

	function get_nota_total() {
		return ($this->nota + $this->nota_cancelada);
	}
	
	function get_nota() {
		return $this->nota;
	}
	
	function get_quantidade_resposta() {
	  return count($this->resposta);
	}

	// fun��o que retorna o vetor de quest�es ainda n�o corrigidas
	function get_questao_pendente() {
	  return $this->questao_pendente;	
	}


  function numero_resposta() {
    return $this->num_resposta;
  }
	
	function respostas($pos) {
    return $this->resposta[$pos];
  }

	
/*
---------------------------------------------------------------------------------------------
																					SETTERS
---------------------------------------------------------------------------------------------
*/	

	function set_nota($valor) {
		settype($valor, "integer");
		if($valor >= 0 && $valor <= 100) {
			$this->nota = $valor;
			$this->aluno[Liberar::procurar_posicao_libera_aluno($this->id_resposta)]['nota'] = $valor; 
			$this->aluno[Liberar::procurar_posicao_libera_aluno($this->id_resposta)]['salvar'] = 'u'; 
		} else {
			$this->nota = NULL;	
			$this->aluno[Liberar::procurar_posicao_libera_aluno($this->id_resposta)]['nota'] = 0; 
			$this->aluno[Liberar::procurar_posicao_libera_aluno($this->id_resposta)]['salvar'] = 'u'; 
		}	
	}
	
/*
-----------------------------------------------------------------------------------------
												QUESTOES
-----------------------------------------------------------------------------------------
*/


	// fun��o para zerar a avalia��o, por ter sido entregue ap�s o prazo
	function zerar_avaliacao() {
		foreach($this->questao as $k) {
			Resposta::adicionar_resposta($k['questao'], '');
		}
		Resposta::salvar();
		Resposta::calcular_nota();
	}
	
	/**
	 * Adiciona uma resposta a uma questao
	 * @param integer $questao Identificador da questao
	 * @param	string $resposta Resposta da questao 
	 */	
	function adicionar_resposta($questao, $resposta) {
    $resp = array('questao'=>(int)$questao,
									'resposta' => $resposta,
									'explicacao' => '',
                  'salvar' => "i"); // i => inserir novo
									
    $pos = array_push($this->resposta, $resp) -1;
    $this->num_resposta++;
    $this->EOF_R = ($this->pos_atual_resposta == ($this->num_resposta));
    return $pos;		
	}
	
	function alterar_resposta($questao, $valor) {
    $pos = Resposta::procurar_posicao_resposta($questao);
    // se n�o encontrar a quest�o � adicionada uma nova
		if($pos === FALSE) return FALSE;
		
		$valor = (int) $valor;		
		$peso = $this->questao[Liberar::procurar_posicao_questao($questao)]['peso'];
		
		if($valor > $peso) {
			return FALSE; // retorna falso se o valor for maior que o peso
		} 

		$this->resposta[$pos]['salvar'] = 'u';
		$this->resposta[$pos]['valor']  = $valor;
		$this->resposta[$pos]['acerto'] = $valor / $peso; // porcentagem de acerto
				
		if($valor == $peso) {
			$this->resposta[$pos]['status'] = CORRETA;	
		} elseif($valor == 0) {
			$this->resposta[$pos]['status'] = ERRADA;			
		} else {
			$this->resposta[$pos]['status'] = MEIO;
		}
		return TRUE;	
	}
	
	function adicionar_explicacao($questao, $explicacao) {
    $pos = Resposta::procurar_posicao_resposta($questao);
    // se n�o encontrar a quest�o � adicionada uma nova
		if($pos === FALSE) return FALSE;
		
		$this->resposta[$pos]['explicacao'] = $explicacao;
		$this->resposta[$pos]['salvar'] = 'u';
	}
	
	
	/**
	 * Faz a corre�ao de todas as questoes da avalia�ao
	 */
	function corrigir_questao($update = FALSE) {
		// faz a leitura do vetor de questao vindo do liberar.class.php
		
		for($x=0; $x < count($this->resposta); $x++) {
		
			$questao = Liberar::questao_identificador($this->resposta[$x]['questao']);			

			switch ($questao['tipo']) {
				// corre�ao da dissertativa
				case DISSERTATIVA:
					if(!isset($this->resposta[$x]['acerto'])) {						
						$this->resposta[$x]['status'] = NAOCORRIGIDA;
						$this->resposta[$x]['acerto'] = 'NULL';
					}
					break;
					
				// corre�ao da multipla escolha					
				case MULTIPLA_ESCOLHA:
					$q = new Multipla_Escolha($this->banco, $this->resposta[$x]['questao']);
					$this->resposta[$x]['status'] = $q->resposta_correta($this->resposta[$x]['resposta']);
					$this->resposta[$x]['acerto'] = $this->resposta[$x]['status'];	
					break;
					
				// corre�ao das lacunas	
				case LACUNAS:
					$q = new Lacunas($this->banco, $this->resposta[$x]['questao']);
					// ATEN�AO: a resposta das lacunas vem em um vetor
					
					$cont = 0; // quantidade de acertos
					foreach($this->resposta[$x]['resposta'] as $key => $value) {
						if($q->resposta_correta($key, $value)) $cont++;
					}
					
					$valor = round($cont/$q->numero_lacunas());
					$this->resposta[$x]['acerto'] = $valor; // seta a quantidade de acertos da questao	
					
					// coloca o status da questao
					if ($this->resposta[$x]['acerto'] == 1) {
						$this->resposta[$x]['status'] = CORRETA;
					} elseif ($this->resposta[$x]['acerto'] > 0 && $this->resposta[$x]['acerto'] < 1) {
						$this->resposta[$x]['status'] = MEIO; //$resposta->adicionar_resposta($k, MEIO, $valor, $r); // entra um percentual do acerto, que ser� multiplicado pelo peso
					}	else {
						$this->resposta[$x]['status'] = ERRADA; //$resposta->adicionar_resposta($k, ERRADA, $valor, $r);
					}	

					break;
					
				// corre�ao das questoes verdadeiro falso	
				case VERDADEIRO_FALSO:
					$q = new Verdadeiro_Falso($this->banco, $this->resposta[$x]['questao']);
					$this->resposta[$x]['status'] = $q->resposta_correta($r = $this->resposta[$x]['resposta'] =='v' ? VERDADEIRO : FALSO);
					$this->resposta[$x]['acerto'] = $this->resposta[$x]['status'];
					break;			
				default: 
					$this->resposta[$x]['status'] = ERRADA;
					$this->resposta[$x]['acerto'] = 0; 
					$this->resposta[$x]['resposta'] = '';
					$this->resposta[$x]['explicacao'] = '';
					break;	
			}
			unset($q);	
			
			// vai fazer o calculo dos valores
			$peso = $this->questao[Liberar::procurar_posicao_questao($this->resposta[$x]['questao'])]['peso'];
			
			switch ($this->resposta[$x]['status']) {
				case NAOCORRIGIDA:
					$this->resposta[$x]['valor'] = 'NULL'; // se nao foi corrigida entao o valor � nulo (USADO APENAS PARA QUEST�ES QUE O PROFESSOR TEM QUE ANALISAR A QUEST�O)
					break;
				case ERRADA:
					$this->resposta[$x]['valor'] = 0; // se estiver errada, a quest�o ter� valor 0
					break;
				case CORRETA:
					$this->resposta[$x]['valor'] = $peso; // se tiver TOTALMENTE CORRETA o ser� o pr�prio peso
					break;
				case MEIO:	
					$this->resposta[$x]['valor'] = $this->resposta[$x]['acerto'] * $peso; // se estiver meio correta ent�o � o valor (percentual do acerto) multiplicado pelo peso
					break;
				default:
					$this->resposta[$x]['valor'] = 0; // se for qualquer outro valor passado, TENTATIVA DE BULAR O SISTEMA, toler�ncia 0
					$this->resposta[$x]['status'] = ERRRADA;
					break;	
			}
			if($update == TRUE) $this->resposta[$x]['salvar'] = 'u';
		}		
	}



  function calcular_nota() {
		
		if($this->presencial) return TRUE; // nao � necess�rio calcular a nota
	
		if(!Resposta::verificar_questao_corrigida()) return FALSE;	
		
		// busca todas as respostas, e soma o valor da nota, menos das questoes canceladas. NAO ENTRA QUANDO � AVALIA��O PRESENCIAL
		if(count($this->resposta) > 0) {
			$nota = 0;
			foreach ($this->resposta as $resp) {
				// verifica se a questao est� cancelada
				if($this->questao[Liberar::procurar_posicao_questao($resp['questao'])]['cancelada'] == NAOCANCELADA)
					$nota += $resp['valor'];	
			}
			Resposta::set_nota($nota); // modifica a variavel da nota;
		}   
		return TRUE;
  }


	// fun��o que verifica se todas as quest�es j� foram corrigidas e cont�m suas respostas
	function verificar_questao_corrigida()	{
		$status_aval = $this->aluno[Liberar::procurar_posicao_libera_aluno($this->id_resposta)]['status'];	
		if($status_aval == NAOENTREGUE || $status_aval == ZERADA) return TRUE;
		Resposta::_verificar_correcao(); // verifica as corre�oes das questoes

		// se houver questoes pendentes retorna false
		if(count($this->questao_pendente) > 0) return FALSE;
		else return TRUE;
  }

	// fun��o que retorna a quantidade ainda de quest�es n�o corrigidas
	function quantidade_questao_pendente() {	
	  return count($this->questao_pendente);
	}
	

	function _verificar_correcao() {
		$this->questao_pendente = array();  // se for uma avalia�ao presencial retorna um vetor vazio
		if(!$this->presencial) {	// se for avalia�ao normal		
			$vet_comp_questao = array();
			$vet_comp_resp = array();
			// busca todas as questoes e coloca dentro de um vetor		
			foreach ($this->questao as $k) 
				array_push($vet_comp_questao, $k['questao']);
			// busca todas as respostas e coloca dentro de um vetor
			foreach ($this->resposta as $k) {
				if($k['status'] != NAOCORRIGIDA)
					array_push($vet_comp_resp, $k['questao']);
			}	
			// faz a diferen�a entre os vetores		
			$diferenca = array_diff($vet_comp_questao, $vet_comp_resp);			
			$this->questao_pendente = $diferenca;	 // quest�es pendentes
		} 
	}

  function buscar_resposta() {
    if ($this->num_resposta == 0) return FALSE;
    if ($this->EOF_R) {
      $this->pos_atual_resposta = 0;
      $this->EOF_R = FALSE;
      return FALSE;
    }
    $retorno = $this->resposta[$this->pos_atual_resposta];
    if(!$this->EOF_R) {
      // se estiver em qualquer outra casa do vetor
      $this->pos_atual_resposta++;
      $this->EOF_R = ($this->pos_atual_resposta == ($this->num_resposta)); // verifica se n�o � fim de array
    }
    return $retorno;
  }


  function resposta_identificador($id_resposta) {
		$id_resposta = (int) $id_resposta;
    $pos = Resposta::procurar_posicao_resposta($id_resposta);		
    if($pos === FALSE) return FALSE;
    return $this->resposta[$pos];
  }

/*
---------------------------------------------------------------------------------
														BANCO DE DADOS
---------------------------------------------------------------------------------
*/

	
  function buscar() {
//		if(!empty($this->id_aval)) Avaliacao::buscar();
		// faz a busca das respostas
		$query = "SELECT * FROM resposta ".
						 "WHERE id_libera_aluno = ".$this->id_resposta;

    $rs = $this->banco->Execute($query);
    if($rs) {  
	    $this->resposta = array(); // toda a busca deve ser colocado o vetor vazio
			while($questao = $rs->FetchRow()) {
		    $resp = array('id' => $questao['id_resposta'],
											'questao'=>$questao['id_questao'],
											'status'=>$questao['status_questao'],
											'acerto'=>$questao['qt_acerto'],
											'valor' => $questao['vl_questao'],
											'explicacao' => $questao['ds_explicacao'],											
											'resposta' => $questao['ds_resposta'],	
											'salvar' => "x"); // x => n�o faz nada

				// verifica o tipo da questao se necessita unserialize
				$tipo = $this->questao[Liberar::procurar_posicao_questao($resp['questao'])]['tipo'];
				if($tipo == LACUNAS) $resp['resposta'] = unserialize($resp['resposta']);
											
      	array_push($this->resposta, $resp);
    	}	 // fim do while para busca

      // faz o calculo para percorrer o vetor de resposta
			$this->num_resposta = count($this->resposta);
			$this->pos_atual_resposta = 0;
      $this->EOF_R = ($this->pos_atual_resposta == ($this->num_resposta));
			
			// nota
			$this->nota = $this->aluno[Liberar::procurar_posicao_libera_aluno($this->id_resposta)]['nota']; // busca a nota do aluno

			Resposta::_verificar_correcao();
    }
  }

  function salvar() {
    // verifica��o das chaves, � necess�rio que todas tenham sido preenchidas antes de salvar
    if(empty($this->id_resposta)) return FALSE;

		if(!Liberar::salvar()) return FALSE;

    $this->banco->Execute("START TRANSACTION"); // inicia uma transa��o
    // percorre o vetor e insere na base de dados apenas os dados atualizados
    foreach($this->resposta as $k)
    {
      if($k['salvar'] == "i") { // apenas ser�o inseridos novos
        if(!Resposta::_inserir_resposta($k)) {
          $this->banco->Execute("ROLLBACK"); // em caso de erro desfaz tudo
          return FALSE;
        }
      } elseif($k['salvar'] == "u") { // update
        if(!Resposta::_atualizar_resposta($k)) {
          $this->banco->Execute("ROLLBACK"); // em caso de erro desfaz tudo
          return FALSE;
        }
      }
    }
    $this->banco->Execute("COMMIT"); // se tiver sucesso
    Resposta::buscar(); // Faz a busca
    return TRUE;
  }

  /**
   * @access: private
   */
  function _inserir_resposta($vetor) {

		$vetor['explicacao'] = $this->banco->qstr($vetor['explicacao']);
		$tipo = $this->questao[Liberar::procurar_posicao_questao($vetor['questao'])]['tipo'];

		// se a questao for do tipo lacunas entao serializa a resposta		
		if($tipo == LACUNAS) $vetor['resposta'] = serialize($vetor['resposta']);

		$vetor['resposta'] = $this->banco->qstr($vetor['resposta']);

    // faz a inser��o na base
		$query = "INSERT INTO resposta(id_resposta,id_libera_aluno,id_questao,status_questao,qt_acerto,vl_questao,ds_explicacao,ds_resposta)".
						 "VALUES(NULL, ".$this->id_resposta.",".$vetor['questao'].", ".$vetor['status'].",".$vetor['acerto'].",".$vetor['valor'].",".$vetor['explicacao'].",".$vetor['resposta'].")";

    if($this->banco->Execute($query)) return TRUE;
    else return FALSE; // verificar como ser� feita a parte de controle de erro aqui
  }

	/**
	 *
	 */
	
  function _atualizar_resposta($vetor) {
    // se o peso da quest�o for false, buscar o valor na tabela liberar_questao.
		$peso = $this->questao[Liberar::procurar_posicao_questao($vetor['questao'])]['peso'];

		switch ($vetor['status']) {
		  case NAOCORRIGIDA:
				$vetor['valor'] = 'NULL'; // se nao foi corrigida entao o valor � nulo (USADO APENAS PARA QUEST�ES QUE O PROFESSOR TEM QUE ANALISAR A QUEST�O)
				break;
			case ERRADA:
				$vetor['valor'] = 0; // se estiver errada, a quest�o ter� valor 0
				break;
			case CORRETA:
				$vetor['valor'] = $peso; // se tiver TOTALMENTE CORRETA o ser� o pr�prio peso
				break;
			case MEIO:	
				$vetor['valor'] = $vetor['acerto'] * $peso; // se estiver meio correta ent�o � o valor (percentual do acerto) multiplicado pelo peso
				break;
			default:
				$vetor['valor'] = 0; // se for qualquer outro valor passado, TENTATIVA DE BULAR O SISTEMA, toler�ncia 0
				$vetor['status'] = ERRRADA;
				break;	
		}		
		
		$vetor['acerto'] = empty($vetor['acerto']) ? 0 : $vetor['acerto'];

		$vetor['explicacao'] = $this->banco->qstr($vetor['explicacao']);
		$vetor['resposta'] = $this->banco->qstr($vetor['resposta']);
		
    // faz a atualiza��o na base
		
    $query = "UPDATE resposta".
             " SET status_questao = ".$vetor['status'].", qt_acerto = ".$vetor['acerto'].
             " ,ds_explicacao = ".$vetor['explicacao'].
             " ,vl_questao = ".$vetor['valor'].", ds_resposta= ".$vetor['resposta'].
             " WHERE id_resposta = '".$vetor['id']."'";			 
 
    if($this->banco->Execute($query)) return TRUE;
    else return FALSE; // verificar como ser� feita a parte de controle de erro aqui
  }


  function procurar_posicao_resposta($id) {
		for($x = 0; $x < $this->num_resposta; $x++)
      if($this->resposta[$x]['questao'] == $id) return $x;
    return FALSE;
  }
	

	function buscar_libera() {
		$query = "SELECT id_libera FROM liberar_aluno WHERE id_libera_aluno =".$this->id_resposta." LIMIT 1";
		
		$rs = $this->banco->Execute($query);
		if($rs) {
			$this->id = $rs->Fields('id_libera'); // seta a variavel de libera�ao
			return TRUE;
		}	else return FALSE;		
	}

} // fim da classe resposta





?>
