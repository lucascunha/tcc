<?php
/**
 * Classe para busca de questões existentes que podem ser inseridas em uma avaliação
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Questao
 * @subpackage QuestaoExistente
 * @version 1.0 
 */
 
 /*
 * Trabalho de Conclusão de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	


class QuestaoExistente {

	/**
	 * @var object Objeto ADODB para acesso ao banco de dados
	 */
	var $banco;
	
	/**
	 * @var integer Identificador da disciplina que está sendo utilizada
	 */	
	var $disciplina;

	/**
	 * @var integer Identificador da avaliação que não poderá ser procurada
	 */	
	var $avaliacao;
	
	/**
	 * @var string Palavra que será utilizada para procurar na descrição da avaliação
	 */	
	var $palavra_chave;
	
	/**
	 * @var integer Tipo de questão que está sendo procurada
	 */
	var $tipo_questao;
	
	/**
	 * @var array Vetor de resultados
	 */
	var $resultado;
	
	/**
	 * @var integer Quantidade de resultados por página
	 */
	var $resultado_pagina;	
	
	/**
	 * @var integer Página que será retornada
	 */
	var $pagina;	
	
	/**
	 * @var integer Página que será retornada
	 */
	var $num_pagina;
	
	/**
	 * @var integer Total de questões encontradas
	 */
	var $total_questao;
	
	/*
	-------------------------------------------------------------------------------------------------
	* Funções públicas
	-------------------------------------------------------------------------------------------------
	*/
	
	/**
	 * Construtor da classe questão, se for passado o parametro $id  feito a busca automaticamente na base de dados
	 * @param ADOdb_* $obj_banco Objeto de conexo com a base de dados
	 * @param integer $disciplina Identificador da disciplina que estão as questões
	 * @param integer $avaliacao Identificador da avaliação que não será procurada (avaliação que será inserida as questões)	 
	 */
	function QuestaoExistente($obj_banco, $disciplina, $avaliacao) {
	    global $A_DB_TYPE;
	    if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE)) {
			die (A_LANG_AVS_ERROR_DB_OBJECT);
		}	

	    $this->banco = $obj_banco; // seta o objeto para o banco de dados
		
		// Coloca as variáveis com seus valores padrões
			
		$this->avaliacao = (int)$avaliacao;
		$this->disciplina = (int)$disciplina;
		$this->resultado = array();
		
		$this->palavra_chave = FALSE;
		$this->tipo_questao = FALSE;
		
		$this->pagina = 1;
		$this->num_pagina = 0;
		$this->resultado_pagina = 10;
	}

	/*
	-------------------------------------------------------------------------------------------------
	* Funções Setters
	-------------------------------------------------------------------------------------------------
	*/
	
	/**
	 * @access public
	 * @param integer $valor Página que será retornada a pesquisa
	 */	
	function set_pagina($valor) {
	  $this->pagina = (int) $valor;
	}

	/**
	 * @access public
	 * @param integer $valor Tipo de questão que será procurada
	 */	
	function set_tipo_questao($valor) {
	  $this->tipo_questao = (int) $valor;
	}

	/**
	 * @access public
	 * @param string $str Palavra contendo no enunciado da questão
	 */	
	function set_palavra_chave($str) {
	  $this->palavra_chave = (string) $str;	
	}

	/**
	 * @access public
	 * @param integer $valor Cdigo da disciplina a qual a questão pertence
	 */	
	function set_resultado_pagina($valor) {
	  $this->resultado_pagina = (int) $valor;	
	}

	/*
	-------------------------------------------------------------------------------------------------
	* Funções Getters
	-------------------------------------------------------------------------------------------------
	*/
	
	/**
	 * @access public
	 * @return integer Retorna o número da página
	 */	
	function get_pagina() {
		return $this->pagina;
	}
	
	/**
	 * @access public
	 * @return integer Retorn o tipo de questão
	 */		
	function get_tipo_questao() {
		return $this->tipo_questao;
	}

	/**
	 * @access public
	 * @return integer Retorna a quantidade de resultados por página
	 */		
	function get_resultado_pagina() {
		return $this->resultado_pagina;
	}

	/**
	 * @access public
	 * @return integer Retorna a palavra chave
	 */		
	function get_palavra_chave() {
	  return $this->palavra_chave;	
	}

	/**
	 * @access public
	 * @return integer Retorna o identificador da questão
	 */		
	function get_avaliacao() {
		return $this->avaliacao;
	}

	/**
	 * @access public
	 * @return integer Retorna o número de páginas
	 */		
	function numero_paginas() {
	  return $this->num_pagina;	
	}

	/**
	 * Faz a busca pelas questões
	 * @access public
	 * @return array Vetor com o resultado da busca
	 */	
	function buscar() {
	  
		$this->resultado = array(); // Zera o vetor
	
		$query = "SELECT id_questao AS questao, ds_enunciado AS pergunta, tipo_questao AS tipo".
				 " FROM questao LEFT JOIN aval_questao USING (id_questao)".
				 " WHERE id_disc = ".$this->disciplina.						 
				 " AND id_questao NOT IN (SELECT id_questao FROM aval_questao WHERE id_avaliacao = ".$this->avaliacao.")";

		$query .= QuestaoExistente::where();	// monta o where

		$rs = $this->banco->Execute($query);

		$this->total_questao = $rs->NumRows(); // pego o número total de registros 
		$this->num_pagina    =  ceil($this->total_questao / $this->resultado_pagina); // calculo o número de páginas e retorno na variável $num_pag
		$offset = ($this->pagina -1) * $this->resultado_pagina; // offset da busca
		$query .= " ORDER BY id_questao ASC LIMIT ".$offset.", ".$this->resultado_pagina;

		$rs = $this->banco->Execute($query);
		
		while($array = $rs->FetchRow()) {
			$this->resultado[] = $array;
		}
		
		return $this->resultado;
	}

	/**
	 * Monta a clausula WHERE do consulta
	 * @access private	  
	 * @return string Retorna a clausula where
	 */		
	function where() {
		$keyword = "";
		
		// se for escolhido algum tipo de questão
		if(!empty($this->tipo_questao)) {
			$keyword .= " AND tipo_questao = ".$this->tipo_questao;
		}
		
		// se for escrito alguma palavra chave
		if(!empty($this->palavra_chave)) {
			$keyword .= " AND UPPER(ds_enunciado) LIKE UPPER('%".($this->banco->qstr($this->palavra_chave))."%')";
		}		
		return $keyword;	
	}
	
} // Fim da questão


?>