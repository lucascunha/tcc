<?php
/**
 * Classe para impress�o de erros, tornando o c�digo fonte mais limpo
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Questao
 * @subpackage QuestaoExistente
 * @version 1.0 
 */
 
 /*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	
 
class Erro {

	/**
	 * @var array Vetor de mensagens de erros
	 */		
	var $msg_erro;

	/*
	-------------------------------------------------------------------------------------------------
	* Fun��es p�blicas
	-------------------------------------------------------------------------------------------------
	*/
	
	/**
	 * Construtor da classe quest�o, se for passado o parametro $id  feito a busca automaticamente na base de dados
	 */
	function Erro() {
		$this->msg_erro = array();	
	}

	/**
	 * @access public
	 * @param String $msg Mensagem de erro
	 */	
	function adicionar_erro($msg) {
		array_push($this->msg_erro, $msg);
	}
	
	/**
	 * @access public
	 * @return Array O vetor com todas as mensagens de erro
	 */	
	function get_vetor_erro() {
		return $this->msg_erro;
	}
	
	/**
	 * @access public
	 * @return Integer Retorna a quantidade de erros
	 */	
	function quantidade_erro() {
		return count($this->msg_erro);
	}
	
	/**
	 * @access public
	 * @return String Retorna todas as mensagens de erro em uma string dividida por par�grafos
	 */	
	function imprimir_erro() {
		$erro = "";
		foreach($this->msg_erro as $k)
		{
			$erro .= "<p><span>".$k."</span></p>";
		}
		return $erro;
	}
} // fim da classe de erros

?>