<?php
/**
 * Classe com funções requiridas pelas outras classes derivadas
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @abstract abstract
 * @package Questao
 * @subpackage Questao
 * @version 1.0 16/09/2007
 */
 
 /*
 * Trabalho de Conclusão de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

class Questao {
	/**
	 * @var object Objeto ADODB para acesso ao banco de dados
	 */
	var $banco;

	/**
	 * @var integer Identificador da questão (chave primria)
	 */
	var $id_questao;

	/**
	 * @var integer Tipo da questão, que facilita o tratamento nas sub-classes.
	 * @see questao.const.php
	 */
	var $tipo_questao;

	/**
	 * @var integer Identificador da disciplina que esta questão pertence
	 */
	var $disciplina;
	 
	/**
	 * @var string Explicao da questão
	 */
	var $explicacao;
	 
	/**
	 * @var integer Enunciado da questão
	 */
	var $enunciado;

	/*
	-------------------------------------------------------------------------------------------------
	* Funções públicas
	-------------------------------------------------------------------------------------------------
	*/
	
	/**
	 * Construtor da classe questão, se for passado o parametro $id  feito a busca automaticamente na base de dados
	 * @param ADOdb_* $obj_banco Objeto de conexo com a base de dados
	 * @param integer $id Identificador da questão (chave primria)
	 */
	function Questao($obj_banco, $id = NULL) {
		global $A_DB_TYPE;
		if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
			die (A_LANG_AVS_ERROR_DB_OBJECT);

		$this->banco = $obj_banco; // seta o objeto para o banco de dados

		// se haver um id passado já realizar a busca
		$this->id_questao = empty($id) ? NULL : (int)$id;
		if (!empty($this->id_questao)) {
			Questao::buscar();
		}else{
			$this->tipo_questao = DISSERTATIVA; // coloca como padro o tipo dissertativa
			$this->disciplina = NULL;
			$this->explicacao = NULL;
			$this->enunciado = NULL;
		}
	}
	
	/*
	-------------------------------------------------------------------------------------------------
	* Funções Setters
	-------------------------------------------------------------------------------------------------
	*/
	
	/**
	 * @access public
	 * @param integer $valor Cdigo da disciplina a qual a questão pertence
	 */
	function set_disciplina($valor) {
		$this->disciplina = (int)$valor;
	}

	/**
	 * @access public
	 * @param string $valor Enunciado da questão
	 */
	function set_pergunta($valor) {
		$this->enunciado = $valor;
	}

	/**
	 * @access public
	 * @param string $valor Explicacao para a questão
	 */
	function set_explicacao($valor) {
		$this->explicacao = $valor;
	}

	/*
	-------------------------------------------------------------------------------------------------
	* Funções Getters
	-------------------------------------------------------------------------------------------------
	*/
	
	/**
	 * @access public
	 * @return integer Retorna o identificador da questão no banco de dados
	 */
	function get_questao() {
		return $this->id_questao;
	}
	
	/**
	 * @access public
	 * @return string Enunciado da questão
	 */
	function get_pergunta() {
		return $this->enunciado;
	}
	
	/**
	 * @access public
	 * @return string Explicao da questão
	 */
	function get_explicacao() {
		return $this->explicacao;
	}

	/**
	 * @access public
	 * @return integer Retorna o tipo desta questão
	 */
	function get_tipo() {
		return $this->tipo_questao;
	}

	/**
	 * @access public
	 * @return integer Retorna o identificador da questão no banco de dados
	 */
	function get_disciplina() {
		return $this->disciplina;
	}

	/*
	-------------------------------------------------------------------------------------------------
	*     Base de dados
	-------------------------------------------------------------------------------------------------
	*/
	
	 /**
	 * Salva as alteraes no banco de dados
	 * @access public
	 * @return boolean TRUE se for salvo corretamente, e FALSE em caso de falha
	 */
	function salvar() {
		if (!empty($this->id_questao) && !empty($this->enunciado) && !empty($this->tipo_questao)) return Questao::_atualizar();
		else return Questao::_inserir();
	}

	/**
	 * Deleta a questão. Não é interessante utilizar está função, pois pode trazer erros as avaliações realizadas anteriormente
	 * @return boolean
	 * @access public
	 */
	function deletar() {
		if(!empty($this->id_questao)) {
			// deletar, na tabela questao e nas outras tabelas para nao ficar lixo no banco de dados, no deleta automatico porque  MyIsam
			$query = "DELETE FROM questao WHERE id_questao = ".$this->id_questao;
			$rs = $this->banco->Execute($query);
			if($rs) return TRUE;
			else return FALSE;
		}
		return FALSE;
	}

	/**
	 * Faz a busca da questão no banco de dados e carrega as variveis no objeto
	 * @acess public
	 * @return boolean
	 */
	function buscar() {
		if(empty($this->id_questao)) return FALSE;

		$query = "SELECT * FROM questao WHERE id_questao = ".$this->id_questao;

		$rs = $this->banco->Execute($query);
		if ($rs) {
			$this->id_questao   = $rs->Fields('id_questao');
			$this->tipo_questao = $rs->Fields('tipo_questao');
			$this->disciplina   = $rs->Fields('id_disc');
			$this->enunciado    = $rs->Fields('ds_enunciado');
			$this->explicacao   = $rs->Fields('ds_explicacao');
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/*
	-------------------------------------------------------------------------------------------------
	* Funções privadas
	-------------------------------------------------------------------------------------------------
	*/

	/**
	 * Insere a questão no banco de dados
	 * @access private
	 */
	function _inserir(){
		$enunciado = $this->banco->qstr($this->enunciado);
		$explicacao = $this->banco->qstr($this->explicacao);
		$query = "INSERT INTO questao (id_questao, ds_enunciado, ds_explicacao, tipo_questao, id_disc) ".
				 "VALUES(NULL, $enunciado, $explicacao, $this->tipo_questao, $this->disciplina)";
		$rs = $this->banco->Execute($query);
		if($rs) {
			$this->id_questao = $this->banco->Insert_ID();
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/**
	 * Atualiza a questão no banco de dados
	 * @access private
	 */
	function _atualizar() {
		$enunciado = $this->banco->qstr($this->enunciado);
		$explicacao = $this->banco->qstr($this->explicacao);

		$query = "UPDATE questao SET tipo_questao = $this->tipo_questao, id_disc = $this->disciplina, ".
				 "ds_explicacao = $explicacao, ds_enunciado = $enunciado ".
		         "WHERE id_questao = $this->id_questao";
		$rs = $this->banco->Execute($query);
		if ($rs) return TRUE;
		else return FALSE;
	}

} // fim da classe questão

?>
