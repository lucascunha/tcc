<?php

/**
 * Classe para as quest�es de Preenchimento de Lacunas
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Questao
 * @subpackage Lacunas
 * @version 1.2 24/09/2007
 * @since 02/04/2008 Modifica��o nas operacoes (insert, updade e delete) diminuindo o overhead na base de dados. Necess�rio a utiliza��o da fun��o salvar() ap�s a fun��o remover_alternativa()
 */

/*
* Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
* Orientadora: Isabela Gasparini
* Co-orientadora: Avanilde Kemczinski	
*/	

class Lacunas extends Questao {

	/**
	 * Formato: array('pergunta','resposta','complemento', 'id_lacuna','salvar'); 
	 * @var array Indica as lacunas na quest�o
	 */
	var $lacunas;

	/**
	 * Formato: array('id_lacuna') 
	 * @var Array Cont�m os IDs das lacunas
	 */
	var $id_lacuna;

	/**
	 * @var integer Vari�vel de controle do vetor lacunas, indica a quantidade de lacunas para a quest�o
	 * @see Lacunas::lacunas;
	 */
	var $num_lacuna;
	
	/**
	 * @var integer Vari�vel de controle do vetor lacunas, indica o fim do vetor lacunas
	 * @see Lacunas::lacunas;
	 */		 		
	var $EOF;
	
	/**
	 * @var integer Vari�vel de controle do vetor lacunas, indica a posicao atual do cursor
	 * @see Lacunas::lacunas;
	 */				 
	var $pos_atual;

	/**
	 * Construtor da classe Lacunas, se for passado o parametro $id � feito a busca automaticamente na base de dados
	 * @param ADOdb_* $obj_banco Objeto de conex�o com a base de dados
	 * @param integer $id Identificador da quest�o (chave prim�ria)
	 */
	function Lacunas($obj_banco, $id = NULL) {
		global $A_DB_TYPE;
		if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
      die (A_LANG_AVS_ERROR_DB_OBJECT);		
		$this->id_questao = empty($id) ? NULL : (int) $id; // seguranca

		parent::Questao($obj_banco, $this->id_questao);

		$this->lacunas = array();
		$this->id_lacuna = array();
		if(empty($id)) {
			$this->tipo_questao = LACUNAS;
		} else {
			Lacunas::buscar();
		}
		$this->num_lacuna = count($this->lacunas);
		$this->pos_atual = 0;
		$this->EOF = ($this->pos_atual == ($this->num_lacuna));
	}

	/**
	 * Retorna o n�mero de lacunas contidos na quest�o
	 * @access public
	 * @return integer Quantidade de lacunas
	 */
	function numero_lacunas() {
		return $this->num_lacuna;
	}

	/**
	 * Adiciona uma nova lacuna a quest�o
	 * @access public
	 * @param string $descricao In�cio da frase da lacuna
	 * @param string $resposta Resposta para a lacuna (Ela ser� convertida)
	 * @param string $complemento Final da frase da lacuna
	 * @return integer Posi��o que foi inserido no vetor lacunas
	 * @since 02/04/2008 Modificado a maneira de fazer o acesso a base de dados		 
	 */
	function adicionar_lacuna($descricao, $resposta, $complemento = NULL) {
		$nresposta = Lacunas::translate($resposta);
		$lacuna = array('pergunta'=>$descricao,
										'resposta'=>$nresposta, 
										'complemento' => $complemento,
										'salvar' => 'i');
		$pos = array_push($this->lacunas, $lacuna ) -1;
		$this->num_lacuna++;
		$this->EOF = ($this->pos_atual == ($this->num_lacuna));
		// caso j� tenha sido salvo faz a inser��o direto na base de dados
		return $pos;
	}

	/**
	 * Altera uma coluna j� existente
	 * @access public
	 * @param integer $lacuna ID da lacuna que ser� alterada
	 * @param string $descricao In�cio da frase da lacuna
	 * @param string $resposta Resposta para a lacuna (Ela ser� convertida)
	 * @param string $complemento Final da frase da lacuna
	 * @return boolean TRUE em caso de sucesso, FALSE em caso de erro e n�o achar a posi��o no vetor
	 * @since 02/04/2008 Modificado a maneira de fazer o acesso a base de dados		 
	 */
	function alterar_lacuna($lacuna, $descricao, $resposta, $complemento = NULL) {
		$lacuna = (int)$lacuna;
		$pos = Lacunas::procurar_posicao_lacuna($lacuna);
		if($pos === FALSE) return FALSE;
		$this->lacunas[$pos]['pergunta'] = $descricao;
		$this->lacunas[$pos]['complemento'] = $complemento;
		$this->lacunas[$pos]['resposta'] = $resposta;
		$this->lacunas[$pos]['salvar'] = 'u';
		return TRUE;
	}

	/**
	 * Remove uma lacuna da quest�o
	 * @access public
	 * @param integer $lacuna ID da lacuna que ser� removida
	 * @return boolean FALSE, em caso de falha em retirar da base de dados a questao, sen�o TRUE
	 * @since 02/04/2008 Modificado a maneira de deletar as quest�es, diminui��o do acesso a base de dados (ness�rio utilizar a fun��o salvar() );
	 * @see Lacunas::salvar();
	 */
	function remover_lacuna($lacuna) {
		$lacuna = (int) $lacuna;
		// primeiro encontra a id
		$pos = Lacunas::procurar_posicao_lacuna($lacuna);
		if($pos === FALSE) return FALSE;
		// deleta do banco de dados
		if(!empty($this->id_questao)) {
			$this->lacunas[$pos]['salvar'] = 'd';
		}
		return TRUE;
	}

	/**
	 * Retorna as informa��es de uma lacuna. (Utilize com o comando For)
	 * @access public
	 * @param integer $pos Posi��o do vetor
	 * @return Array Vetor com as informa��es da lacuna
	 */
	function lacuna($pos) {
		return $this->lacunas[$pos];
	}

	/**
	 * Retorna uma lacuna, esta lacuna � a qual o ponteiro interno est� apontando. (Utilize com o comando foreach)
	 * @access public
	 * @return Array Retorn os valores da lacuna em focus
	 */
	function buscar_lacuna() {
		if ($this->num_lacuna == 0) return FALSE;
		if ($this->EOF) {
			$this->pos_atual = 0;
			$this->EOF = FALSE;
			return FALSE;
		}
		$retorno = $this->lacunas[$this->pos_atual];
		if(!$this->EOF) {
			// se estiver em qualquer outra casa do vetor
			$this->pos_atual++;
			$this->EOF = ($this->pos_atual == ($this->num_lacuna)); // verifica se n�o � fim de array
		}
		return $retorno;
	}

	/**
	 * Verifica se a resposta est� correta
	 * @access public
	 * @param integer $lacuna O ID da lacuna que ser� testada se � a correta
	 * @param string $resposta String com a resposta que ser� comparada com a lacuna
	 * @return integer Retorna 1 se a resposta estiver correta e 0 se a resposta estiver errada ou n�o encontrada 
	 */		
	function resposta_correta($lacuna, $resposta) {
		$lacuna = (int) $lacuna;
		$pos = Lacunas::procurar_posicao_lacuna($lacuna);
		if($pos === FALSE) return 0;
		$nresposta = Lacunas::translate($resposta);
		if ($this->lacunas[$pos]['resposta'] == $nresposta) return 1;
		else return 0;
	}

	/**
	 * Busca na base de dados a quest�o Preenchimento de Lacunas atrav�s do ID setado
	 * @access public
	 * @return boolean
	 */
	function buscar() {
		$this->lacunas = array();
		$this->id_lacuna = array();

		$query = "SELECT id_lacuna AS id_lacuna, " .
						 "ds_pergunta AS pergunta, " .
						 "ds_compl_pergunta AS complemento, " .
						 "ds_resposta AS resposta " .
						 "FROM lacuna WHERE id_questao ='".$this->id_questao."' " .
						 "ORDER BY id_lacuna ASC";
		$rs = $this->banco->Execute($query);
		if ($rs) {
			$this->banco->SetFetchMode(ADODB_FETCH_ASSOC);
			while($lac = $rs->FetchRow()) {
				$lacuna = array("id_lacuna" => $lac['id_lacuna'], 
												"pergunta"=>$lac['pergunta'], 
												"complemento"=>$lac['complemento'], 
												"resposta"=>$lac['resposta'],
												"salvar"=>'x');
				array_push($this->lacunas, $lacuna);
			}
			$this->num_lacuna = count($this->lacunas);
			$this->EOF = ($this->pos_atual == ($this->num_lacuna));
			return TRUE;
		}
	}

	/**
	 * Salva as informa��es na base de dados
	 * @access public
	 * @return boolean Retorna TRUE se a opera��o foi realizada com sucesso
	 */
	function salvar() {
		// verificar as informa��es obrigat�ria
		// tem que haver pelo menos uma lacuna
		if($this->num_lacuna > 0) {
			if($this->id_questao) return Lacunas::_atualizar();
			else return Lacunas::_inserir();
		} else return FALSE;
	}

	/**
	 * Deleta a quest�o da base de dados
	 * @access public
	 * @return boolean TRUE para sucesso.
	 */
	function deletar() {
		if($this->id_questao) {
			$query = "DELETE FROM lacuna WHERE id_questao='".$this->id_questao."'";
			$this->banco->Execute($query);
			return parent::deletar();
		}
	}

	/**
	 * Insere na base de dados
	 * @see Lacunas::salvar()
	 * @access private
	 * @return boolean
	 */
	function _inserir() {
		$this->banco->Execute("START TRANSACTION");
		if(parent::salvar()) {
			for($x = 0; $x < $this->num_lacuna; $x++) {
				// se nao for inserida, retorna um erro
				if (!(Lacunas::_salvar_lacunas($x))) {
					$this->banco->Execute("ROLLBACK");
					return FALSE;
				} 
			}
			$this->banco->Execute("COMMIT");
			Lacunas::buscar();
			return TRUE;
		}
		$this->banco->Execute("ROLLBACK");
		return FALSE;
	}

	/**
	 * Atualiza (Update) as informa��es na base de dados
	 * @access private
	 * @return boolean
	 * @see Lacunas::salvar()
	 */
	function _atualizar() {
		$this->banco->Execute("START TRANSACTION");
		if(parent::salvar()) {
			for($x = 0; $x < $this->num_lacuna; $x++) {
				// se nao for inserida, retorna um erro
				if (!(Lacunas::_salvar_lacunas($x))) {
					$this->banco->Execute("ROLLBACK");
					return FALSE;
				} 
			}
			$this->banco->Execute("COMMIT");
			Lacunas::buscar();
			return TRUE;
		}
		$this->banco->Execute("ROLLBACK");
		return FALSE;
	}

	/**
	 * Salva as informa��es pertinentes as lacunas da quest�o
	 * @access private
	 * @param integer $lacuna Posi��o do vetor lacunas que ser� utilizado nas opera��es de inserir, atualizar e deletar lacunas
	 * @return boolean TRUE para sucesso.
	 * @since 02/04/2008 Fun��o criada no lugar da _insert_alternativa. O acesso ao banco de dados fica menor e mais r�pido.
	 */	
	function _salvar_lacunas($lacuna) {		

		$vetor = $this->lacunas[$lacuna];		
				
  	// formata��o das informa��es
		$pergunta = $this->banco->qstr($vetor['pergunta']);
		$complemento = $this->banco->qstr($vetor['complemento']);
		$resposta = $this->banco->qstr($vetor['resposta']);
		
		if($vetor['salvar'] == 'i') {
		 // ----------- INSER��O DE NOVAS LACUNAS ----------------// 
			$query = "INSERT INTO lacuna(id_lacuna,id_questao,ds_pergunta,ds_compl_pergunta,ds_resposta) " .
							 "VALUES (NULL, '".$this->id_questao."', ".$pergunta.", ".$complemento.", ".$resposta.")";
			$rs = $this->banco->Execute($query);
			if($rs) {
				array_push($this->id_lacuna, $this->banco->Insert_ID());
				$this->lacunas[$lacuna]['id_lacuna'] = $this->banco->Insert_ID();				
				return TRUE;
			} else return FALSE;
		} elseif($vetor['salvar'] == 'u') {
			// ----------- ATUALIZA��O DAS LACUNAS ------------------//
			$query = "UPDATE lacuna " .
							 "SET ds_pergunta = ".$pergunta.", " .
							 "ds_compl_pergunta = ".$complemento.", " .
							 "ds_resposta = ".$resposta." " .
							 "WHERE id_lacuna='".$vetor['id_lacuna']."'";
			$rs = $this->banco->Execute($query);
			if($rs) return TRUE;
			else return FALSE;			
		} elseif($vetor['salvar'] == 'd') {
			// ------- EXCLUS�O DE LACUNAS ------------------ //
			$query = "DELETE FROM lacuna WHERE id_lacuna = '".$vetor['id_lacuna']."'";
			$rs = $this->banco->Execute($query);
			if($rs) return TRUE;
			else return FALSE;
		}	
		return TRUE;
	}	

	/**
	 * Transforma os caracteres especiais em caracteres normais
	 * @access private
	 * @param string $str String com a resposta em forma de caracter especial
	 * @return string Retorna a string sem caracteres especiais
	 */
	function translate($str) {
		$caracter_especial = array("/�|�|�|�|�/i",
															 "/�|�|�|�/i",
															 "/�|�|�|�/i",
															 "/�|�|�|�|�/i",
															 "/�|�|�|�/i",
															 "/�/i");
		$caracter_simples = array("a","e","i","o","u","c");

		$nString = strtolower($str);
		$nString = preg_replace($caracter_especial, $caracter_simples, $nString);
		return $nString;
	}

	/**
	 * Procura a posi��o de uma lacuna no vetor lacuna atrav�s de uma ID
	 * @access private
	 * @param integer $id ID da lacuna que deve ser encontrada
	 * @return integer | boolean Retorna a posi��o do vetor que se encontra o ID procurado ou FALSE se n�o houver
	 */
	function procurar_posicao_lacuna($id) {
		 for($x = 0; $x < $this->num_lacuna; $x++){
			if($this->lacunas[$x]['id_lacuna'] == $id) {
				return $x;
			}
		}
		return FALSE;
	}

	/**
	 * Reseta as vari�veis de controle de loop
	 * @access public
	 */
	function reseta_variaveis() {
		$this->pos_atual = 0;
		$this->EOF = FALSE;
	}
} // fim da classe Lacunas
?>