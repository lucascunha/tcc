<?php

/**
 * Classe para as quest�es de Multipla Escolha
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Questao
 * @subpackage Multipla_Escolha
 * @version 1.2 24/09/2007
 * @since 02/04/2008 - Modifica��o nas operacoes (insert, updade e delete) diminuindo o overhead na base de dados. Necess�rio a utiliza��o da fun��o salvar() ap�s a fun��o remover_alternativa()
 */

/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	 
 
class Multipla_Escolha extends Questao {
	

	/**
	 * Formato: array('id_alternativa','descricao','correta','fixa','salvar')	
	 * @var array Vetor com as alternativas para a quest�o
	 */
	var $alternativa;
	
	/**
	 * @var boolean Indica se h� alguma alternativa correta
	 */
	var $correta;
	
	/**
	 * @var boolean Indica se h� alguma alternativa fixa
	 */
	var $fixa;
	
	/**
	 * @var integer Indica qual � o ID da alternativa correta
	 */
	var $id_correta;
	
	/**
	 * @var integer Indica qual � o ID da alternativa fixa
	 */
	var $id_fixa;
	
	/**
	 * @var array Vetor com todas as IDs das alternativas
	 */
	var $id_alternativa;

	/**
	 * @var integer Vari�vel de controle do vetor alternativa, indica a quantidade de alternativas para a quest�o
	 * @see Multipla_Escolha::$alternativa
	 */
	var $num_alternativa;
	
	/**
	 * @var integer Vari�vel de controle do vetor alternativa, indica o fim do vetor alternativa
	 * @see Multipla_Escolha::$alternativa
	 */		
	var $EOF;

	/**
	 * @var integer Vari�vel de controle do vetor alternativa, indica a posicao atual do cursor
	 * @see Multipla_Escolha::$alternativa
	 */		
	var $pos_atual;

	/**
	 * Construtor da classe, se for passado o parametro $id � feito a busca automaticamente na base de dados
	 * @param ADOdb_* $obj_banco Objeto de conex�o com a base de dados
	 * @param integer $id Identificador da quest�o (chave prim�ria)
	 */
	function Multipla_Escolha($obj_banco, $id = NULL) {
		global $A_DB_TYPE;
		if (!is_a($obj_banco,'adodb_'.$A_DB_TYPE))
      die (A_LANG_AVS_ERROR_DB_OBJECT);
		$this->id_questao = empty($id) ? NULL : (int) $id; // seguranca	

		parent::Questao($obj_banco, $this->id_questao);

		$this->id_alternativa = array();
		$this->alternativa = array();
		if(empty($this->id_questao)) {
			$this->tipo_questao = MULTIPLA_ESCOLHA;
			$this->correta = FALSE;
			$this->fixa = FALSE;
			$this->id_correta = FALSE;
			$this->id_fixa = FALSE;
		} else {
			Multipla_Escolha::buscar();
		}
		$this->num_alternativa = count($this->alternativa);
		$this->pos_atual = 0;
		$this->EOF = ($this->pos_atual == ($this->num_alternativa));

	}

	/**
	 * Adiciona uma alternativa a quest�o Multipla Escolha
	 * @param string $descricao Descri��o da alternativa
	 * @param boolean $correta Representa que esta � a alternativa correta
	 * @param boolean $fixa Deixa a alternativa fixa no final das alternativas
	 * @return integer A posi��o de inser��o no vetor $this->alternativa
	 * @since 02/04/2008 Modificado o modo de inser��o na base de dados
	 */
	function adicionar_alternativa($descricao, $correta = FALSE, $fixa = FALSE) {
		// se for passada a opcao de correta, verificar se j� n�o uma correta
		if ($correta && $this->correta) return FALSE;
		if ($fixa && $this->fixa) return FALSE;
		if (empty($descricao)) return FALSE;

		$fixa = !empty($fixa) ? 1 : 0;
		$correta = !empty($correta) ? 1 : 0;
		
		$alternativa = array('descricao'=>$descricao,
												 'correta'=>$correta, 
												 'fixa'=>$fixa,
												 'salvar' => 'i'); // insert		

		$pos = array_push($this->alternativa, $alternativa) -1;
		if ($correta) $this->correta = TRUE;
		if ($fixa) $this->fixa = TRUE;
		$this->num_alternativa++;
		$this->EOF = ($this->pos_atual == ($this->num_alternativa));
		return $pos;
	}

	/**
	 * Remove uma alternativa da quest�o Multipla Escolha
	 * @access public
	 * @param integer $alternativa ID da alternativa que ser� removida
	 * @return boolean FALSE, em caso de falha em retirar da base de dados a questao, sen�o TRUE
	 * @since 02/04/2008 Modificado a maneira de deletar as quest�es, diminui��o do acesso a base de dados (ness�rio utilizar a fun��o salvar()
	 * @see Multipla_Escolha::salvar()
	 */
	function remover_alternativa($alternativa) {
		$alternativa = (int)$alternativa;
	
		$pos = Multipla_Escolha::procurar_posicao_alternativa($alternativa);
		if($pos === FALSE) return FALSE;
		// a verificacao se h� uma alternativa correta fica a cargo de javascript ou php antes de executar esta fun��o
		// vale lembrar que a fun��o de salvar n�o ir� salvar se n�o haver uma op��o marcada como correta
		// se a questao j� estiver inserida no banco de dados ser� retirada do banco
		if (!empty($this->id_questao)) {
			$this->alternativa[$pos]['salvar'] = 'd'; // quando a funcao salvar() for executada a quest�o ser� deletada
			// verifica se a alternativa a ser deletada era correta e/fixa
			if($this->alternativa[$pos]['correta'] == 1) $this->correta = FALSE;
			if($this->alternativa[$pos]['fixa'] == 1) $this->fixa = FALSE;
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Atualiza uma alternativa
	 * @access public
	 * @param integer $alternativa ID da alternativa
	 * @param string $descricao Descri��o da alternativa
	 * @param boolean $correta Indica se a op��o � a correta
	 * @param boolean $fixa Indica se a op��o � fixa
	 * @return boolean FALSE em qualquer tipo de erro
	 * @since 02/04/2008 Modificado a maneira de fazer o acesso a base de dados
	 */
	function alterar_alternativa($alternativa, $descricao, $correta = FALSE, $fixa = FALSE) {
		// a verificacao se h� uma alternativa correta fica a cargo de javascript ou php antes de executar esta fun��o
		// vale lembrar que a fun��o de salvar n�o ir� salvar se n�o haver uma op��o marcada como correta
		// CUIDADO: com 2 op��es marcadas como correta, tratar com javascript
		// se a descri��o for branca tamb�m retorna falso					
		if (empty($descricao)) return FALSE;

		$alternativa = (int) $alternativa;
		$pos = Multipla_Escolha::procurar_posicao_alternativa($alternativa);
		if($pos === FALSE) return FALSE;

		// atualiza as variaveis
		if($this->alternativa[$pos]['correta'] && !$correta)  {
			$this->correta = FALSE;
			$this->id_correta = NULL;
		}
		if($this->alternativa[$pos]['fixa'] && !$fixa) {
			$this->fixa = FALSE;
			$this->id_fixa = NULL;
		}

		$fixa = !empty($fixa) ? 1 : 0;
		if($fixa) {
			$this->fixa = TRUE;
			$this->id_fixa = $alternativa;
		}
		$correta = !empty($correta) ? 1 : 0;

		if($correta) {
			$this->correta = TRUE;
			$this->id_correta = $alternativa;
		}
		// atualiza o vetor
		$this->alternativa[$pos]['descricao'] = $descricao;
		$this->alternativa[$pos]['correta'] = $correta;
		$this->alternativa[$pos]['fixa'] = $fixa;
		$this->alternativa[$pos]['salvar'] = 'u';

		return TRUE;
	}


	/**
	 * Retorna uma alternativa, esta alternativa � a qual o ponteiro interno est� apontando. (Utilize com o comando foreach)
	 * @access public
	 * @return Array Retorn os valores da alternativa em focus
	 */
	function buscar_alternativa() {
		if ($this->num_alternativa == 0) return FALSE;
		// se for j� estiver no fim do vetor, � resetado as vari�veis para a pr�xima utiliza��o da fun��o
		if ($this->EOF) {
			$this->pos_atual = 0;
			$this->EOF = FALSE;
			return FALSE;
		}
		$retorno = $this->alternativa[$this->pos_atual];
		if(!$this->EOF) {
			// se estiver em qualquer outra casa do vetor
			$this->pos_atual++;
			$this->EOF = ($this->pos_atual == ($this->num_alternativa)); // verifica se n�o � fim de array

		}
		return $retorno;
	}

	/**
	 * Retorna as informa��es de uma alternativa. (Utilize com o comando For)
	 * @access public
	 * @param integer $pos Posi��o do vetor
	 * @return Array Vetor com as informa��es da alternativa
	 */
	function alternativa($pos) {
		return $this->alternativa[$pos];
	}

	/**
	 * Retorna a quantidade de alternativas inseridas na quest�o Multipla Escolha
	 * @access public
	 * @return integer Quantidade de alternativas
	 */
	function numero_alternativas(){
		return $this->num_alternativa;
	}

	/**
	 * Retorna o vetor com as id das alternativas
	 * @access public
	 * @return array Vetor com as id das alternativas
	 */
	function id_alternativas() {
		return $this->id_alternativa;
	}

	/**
	 * Monta as quest�es em ordem aleat�ria
	 * @access public
	 */
	function randomize() {
		srand((float)microtime()*1000000);
		$aux = $this->alternativa; // vetor auxiliar
		// se haver uma alternativa fixa. A alternativa fixa sempre � a �ltima ap�s uma busca.
		if($this->alternativa[$this->num_alternativa - 1]['fixa']) {
			array_splice($aux, ($this->num_alternativa-1), 1);
		}
		shuffle($aux);
		array_push($aux, $this->alternativa[$this->num_alternativa-1]);
		$this->alternativa = $aux;
	}

	/**
	 * Verifica se h� uma op��o marcada como correta, e devolve seu id
	 * @access public
	 * @return integer | boolean
	 */
	function correta() {
		foreach ($this->alternativa as $k) {
			if ($k['correta'] == true) return $k['id_alternativa'];
		}
		return FALSE;
	}

	/**
	 * Verifica se a resposta est� correta
	 * @access public
	 * @param integer $alternativa O ID da alternativa que ser� testada se � a correta
	 * @return integer Retorna 1 se a resposta estiver correta e 0 se a resposta estiver errada ou n�o encontrada 
	 */		
	function resposta_correta($alternativa) {
		$alternativa = (int) $alternativa;
		$pos = Multipla_Escolha::procurar_posicao_alternativa($alternativa);
		if($pos === FALSE) return 0;
		if ($this->alternativa[$pos]['correta'] == TRUE ) return 1;
		else return 0;
	}

	/*
	 * Parte de Banco de Dados
	 */

	/**
	 * Busca na base de dados a quest�o Multipla Escolha atrav�s do ID setado
	 * @access public
	 * @return boolean
	 */
	function buscar() {
		$this->id_alternativa = array();
		$this->alternativa = array();
			
		// busca as alternativas da questao m�ltipla-escolha
		$qbusca = "SELECT id_alternativa as id_alternativa, " .
							"ds_alternativa as descricao, " .
							"bl_fixa as fixa, " .
							"bl_correta as correta " .
							"FROM multipla_escolha WHERE id_questao =".$this->id_questao." ".
							"ORDER BY bl_fixa ASC, id_alternativa ASC";

		$rs = $this->banco->Execute($qbusca);
		if ($rs) {
			while($alt = $rs->FetchRow()) {
				$alternativa = array("id_alternativa" => $alt['id_alternativa'], 
														 "descricao"=>$alt['descricao'], 
														 "correta"=>$alt['correta'], 
														 "fixa"=>$alt['fixa'], 
														 "salvar"=>'x');
				array_push($this->alternativa, $alternativa);
			}
			// verifica se h� alguma alternativa fixa e o seu id, assim como na correta
			foreach ($this->alternativa as $k) {
				if ($k['fixa']) {
					$this->fixa = TRUE;
					$this->id_fixa = $k['id_alternativa'];
				}
				if ($k['correta']) {
					$this->correta = TRUE;
					$this->id_correta = $k['id_alternativa'];
				}
				array_push($this->id_alternativa, $k['id_alternativa']);
			}
			$this->num_alternativa = count($this->alternativa);
			$this->EOF = ($this->pos_atual == ($this->num_alternativa));
			return TRUE;
		} else return FALSE;
	}

	/**
	 * Salva as informa��es na base de dados
	 * @access public
	 * @return boolean Retorna TRUE se a opera��o foi realizada com sucesso
	 */
	function salvar() {
		// descri��o � um campo obrigat�rio se n�o for preenchido retorna falso.
		if (empty($this->correta)) {
			return FALSE;
		}
		if (!empty($this->id_questao)) return Multipla_Escolha::_atualizar();
		else return Multipla_Escolha::_inserir();
	}

	/**
	 * Deleta a quest�o da base de dados
	 * @access public
	 * @return boolean TRUE para sucesso.
	 */
	function deletar() {
		if(!empty($this->id_questao)) {
			$query = "DELETE FROM multipla_escolha WHERE id_questao =".$this->id_questao;
			$this->banco->Execute($query);
			return parent::deletar();
		}
		return FALSE;
	}

	/*
	 * Fun��es privadas
	 */

	/**
	 * Insere na base de dados
	 * @see Multipla_Escolha::salvar()
	 * @access private
	 * @return boolean
	 */
	function _inserir() {
		$this->banco->Execute("START TRANSACTION");
		// faz a inser��o da quest�o, e obtem o id_questao
		if (parent::salvar()) {
			for($x = 0; $x < $this->num_alternativa; $x++) {
				// se nao for inserida, retorna um erro
				if (!Multipla_Escolha::_salvar_alternativa($x)) {
					$this->banco->Execute("ROLLBACK");
					return FALSE;
				}
			}
			$this->banco->Execute("COMMIT");
			Multipla_Escolha::buscar();
			return TRUE;	
		}
		// em qualquer caso de erro retrocede as opera��es feitas
		$this->banco->Execute("ROLLBACK");
		return FALSE;
	}

	/**
	 * Atualiza (Update) as informa��es na base de dados
	 * @access private
	 * @return boolean
	 * @see Multipla_Escolha::salvar()
	 */
	function _atualizar() {
		$this->banco->Execute("START TRANSACTION");
		if(parent::salvar()) {
			for($x = 0; $x < $this->num_alternativa; $x++) {
				// se nao for inserida, retorna um erro
				if (!Multipla_Escolha::_salvar_alternativa($x)) {
					$this->banco->Execute("ROLLBACK");
					return FALSE;
				}
			}
			$this->banco->Execute("COMMIT");
			Multipla_Escolha::buscar();
			return TRUE;
		}
		// em qualquer caso de erro retrocede as opera��es feitas
		$this->banco->Execute("ROLLBACK");
		return FALSE;
	}

	/**
	 * Salva as informa��es pertinentes as alternativas da quest�o
	 * @access private
	 * @param integer $num_alternativa Posi��o do vetor alternativa que ser� utilizado nas opera��es de inserir, atualizar e deletar alternativas
	 * @return boolean TRUE para sucesso.
	 * @since 02/04/2008 Fun��o criada no lugar da _insert_alternativa. O acesso ao banco de dados fica menor e mais r�pido.
	 */	
	function _salvar_alternativa($num_alternativa) {
		
		$vetor = $this->alternativa[$num_alternativa];
		
		// Formata as vari�veis
		$fixa = !empty($vetor['fixa']) ? 'TRUE' : 'FALSE';
		$correta = !empty($vetor['correta']) ? 'TRUE' : 'FALSE';
		$descricao = $this->banco->qstr($vetor['descricao']); // quote			
		
		if ($vetor['salvar'] == 'i') { // insere
			// ----------- INSERIR NOVAS ALTERNATIVAS	------------------- //
			$query  = "INSERT INTO multipla_escolha(id_alternativa, id_questao, ds_alternativa, bl_fixa, bl_correta)" .
								"VALUES(NULL,".$this->id_questao.",".$descricao.", ".$fixa.", ".$correta.")";
			$rs = $this->banco->Execute($query);
			// adiciona ao vetor de alternativas
			if($rs) {
				array_push($this->id_alternativa, $this->banco->Insert_ID());
				$this->alternativa[$num_alternativa]['id_alternativa'] = $this->banco->Insert_ID();				
				return TRUE;
			} else return FALSE;
			
		} elseif ($vetor['salvar'] == 'u')	{ // faz update
			// ------------- ATUALIZA AS ALTERNATIVAS ------------ //
			$query = "UPDATE multipla_escolha SET " .
							 "ds_alternativa = ".$descricao.", " .
							 "bl_fixa = ".$fixa.", " .
							 "bl_correta = ".$correta." " .
							 "WHERE id_alternativa =".$vetor['id_alternativa'];
//							 echo $query;
			$rs = $this->banco->Execute($query);			
			if($rs) return TRUE;
			else return FALSE;
		} elseif ($vetor['salvar'] == 'd') { // deleta
			// ------------ EXCLUI ALTERNATIVAS ------------------ // 
			$query = "DELETE FROM multipla_escolha WHERE id_alternativa=".$vetor['id_alternativa'];
			$rs = $this->banco->Execute($query);
			if ($rs) return TRUE;
			else return FALSE;				
		}
		return TRUE;
	}

	/**
	 * Procura a posi��o de uma alternativa no vetor alternativa atrav�s de uma ID
	 * @access private
	 * @param integer $id ID da alternativa que deve ser encontrada
	 * @return integer | boolean Retorna a posi��o do vetor que se encontra o ID procurado ou FALSE se n�o houver
	 */
	function procurar_posicao_alternativa($id) {
		 for($x = 0; $x < $this->num_alternativa; $x++){
			if($this->alternativa[$x]['id_alternativa'] == $id) {
				return $x;
			}
		}
		return FALSE;
	}

} // fim da classe Multipla Escolha


?>