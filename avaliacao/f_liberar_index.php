<?php
/**
 * 
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @subpackage Liberar Avalia��o
 * @version 1.0 <05/12/2007>
 * @since <17/12/2007> Inclus�o do Ajax
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */
  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
	require_once "avaliacao/ajuda.const.php";

	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	
	if(!$CodigoDisciplina) {
		echo A_LANG_AVS_ERROR_PARAM;
		return;
	}	

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO;
		return;
	}	

  montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_LIBERAR);
?>
<script language="JavaScript" type="text/javascript" src="avaliacao/ajax/ajax.js"></script>
<script language="javascript" type="text/javascript">
	//lista os alunos de uma avaliacao
	function listar_alunos(libera, id) {
		var ajax = new AJAX();
		var div = document.getElementById(id);
		div.className='lista_aluno';
		div.innerHTML = "Carregando <img src='imagens/carrega.gif' border='0'>";
		ajax.Texto("avaliacao/ajax/ajax_liberar_lista_aluno.php?libera="+libera+"&disciplina="+<?php echo $CodigoDisciplina; ?>,div);	
	}
	
  // encerra uma avaliacao
	function encerrar_avaliacao(libera) {
		var ajax = new AJAX();
		var r = confirm('Gostaria de encerrar a avalia�ao?');
		if(r == true) {
			ajax.TextoSemRetorno("avaliacao/ajax/ajax_liberar_encerrar_aval.php?libera="+libera+"&disciplina="+<?php echo $CodigoDisciplina; ?>, 'listar_avaliacao_aberta()');		
		}		
	}
	
	// quando inicializa a p�gina busca as avalia��es liberadas
	function listar_avaliacao_aberta() {
		var ajax = new AJAX();
		var div_lista = document.getElementById('lista_aval');
		div_lista.innerHTML = "Carregando <img src='imagens/carrega.gif' border='0'>";
		ajax.Texto("avaliacao/ajax/ajax_liberar_lista_aval.php?disciplina=<?php echo $CodigoDisciplina; ?>",div_lista);
	}
	
	function lista_aval_finalizada() {
		var ajax = new AJAX();
		var div_lista = document.getElementById('aval_finalizada');
		div_lista.innerHTML = "Carregando <img src='imagens/carrega.gif' border='0'>";
		ajax.Texto("avaliacao/ajax/ajax_liberar_lista_aval_enc.php?disciplina=<?php echo $CodigoDisciplina; ?>",div_lista);	
	}	
	
</script>

<table cellspacing='1' cellpadding='1' width='100%' border='0' bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
  <tr>
    <td>
		<div class="funcao">
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_LIBERATE; ?> (1/4)</div>	
			<p><span><strong><?php echo A_LANG_AVS_LABEL_FUNCTION; ?></strong></span></p>
			<p>
					<input type='button' name='liberar' class='button' value="<?php echo A_LANG_AVS_BUTTON_EVALUATION_LIBERATE_NEW; ?>"  onclick="javascript:window.location='<?php echo "a_index.php?opcao=FuncaoLiberarConceito&CodigoDisciplina=$CodigoDisciplina" ?>'" />
					<input type='button' name='listar' class='button' value="<?php echo A_LANG_AVS_BUTTON_EVALUATION_LIBERATE_FINISH; ?>"  onclick="lista_aval_finalizada()" />
					<input name='ajuda' type='button' class='button' value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onclick="<?php echo HELP_FUNCTION_LIBERATE; ?>" />
			</p>	
		</div>
		<div id="lista_aval">
		
		</div>
		<br />
		<div id="aval_finalizada">
		
		</div>
		</td>		
	</tr>
</table>
<script>
<!--
listar_avaliacao_aberta();
-->
</script>