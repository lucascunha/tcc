<?php
/**
 * Monta a avalia��o para que o aluno possa responder e enviar as respostas
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @subpackage Avalia��o
 * @version 1.0 <24/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

  require_once "../include/adodb/adodb.inc.php";
  require_once "../config/configuracoes.php";
  require_once "questao.const.php";
  require_once "funcao/geral.func.php";
  require_once "class/avaliacao.class.php";
  require_once "class/grupo.class.php";
  require_once "class/questao.class.php";
  require_once "class/dissertativa.class.php";
  require_once "class/multipla_escolha.class.php";
  require_once "class/lacunas.class.php";
  require_once "class/verdadeiro_falso.class.php";
  require_once "class/erro.class.php";
  require_once "class/liberar.class.php";

  $erro = new Erro();

  @session_start();

	// Troca de idioma
  if ($A_LANG_IDIOMA_USER == "")
		include "../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../idioma/".$A_LANG_IDIOMA_USER."/geral.php";

  // fazer a verifica��o de seguran�a.
//  $avaliacao = isset($_GET['aval']) ? (int) $_GET['aval'] :$erro->adicionar_erro("Avalia��o n�o encontrada");
  $id_libera = isset($_GET['libera']) ? (int) $_GET['libera'] : $erro->adicionar_erro(A_LANG_AVS_ERROR_EVALUATION_LIBERATE);
	$id_libera_aluno = isset($_GET['libera_aluno']) ? (int) $_GET['libera_aluno'] : $erro->adicionar_erro(A_LANG_AVS_ERROR_EVALUATION_LIBERATE_STUDENTS);
  $aluno = isset($_SESSION['id_aluno']) ? $_SESSION['id_aluno'] : $erro->adicionar_erro(A_LANG_AVS_ERROR_EVALUATION_LOGGED);

  // caso n�o haja erros
  if($erro->quantidade_erro() == 0) {
    // abrindo conex�o com a base de dados
    $conn = &ADONewConnection($A_DB_TYPE);
    $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

    $liberar = new Liberar($conn, $id_libera);

    // verifica se a avaliacao est� liberada para o aluno
    if($liberar->avaliacao_liberada_aluno($id_libera_aluno))
    {
		  // grava no sistema que o aluno est� realizando a prova
      $liberar->aluno_alterar($id_libera_aluno, REALIZANDO);
      if(!$liberar->salvar()) $erro->adicionar_erro(A_LANG_AVS_ERROR_EVALUATION_INIT);

      if($erro->quantidade_erro() == 0)
      {
        if($liberar->get_randomica()) $liberar->randomize();
      }
    } else {
			$erro->adicionar_erro(A_LANG_AVS_ERROR_EVALUATION_SUBMIT);
		}		
  }



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <title>Avalia��o</title>
  <meta http-equiv=expires content="0">
  <meta name=keywords content="Adpta��o, Ensino, Dist�ncia">
  <meta name=description content="M�dulo de Avalia��o Somativa">
  <meta name=robots content="INDEX,FOLLOW">
  <meta name=resource-type content="document">
  <meta name=author content="Claudiomar Desanti">
  <meta name=copyright content="Copyright (c) 2007 by Universidade do Estado de Santa Catarina - UDESC">
  <meta name=revisit-after content="1 days">
  <meta name=distribution content="Global">
  <meta name=generator content="Easy Eclipse For PHP">
  <meta name=rating content="General">

  <link rel="StyleSheet" href="../css/geral.css" type="text/css">
	<link href="css/ff.css" rel="stylesheet" type="text/css" media="screen" />
	<!--[if lte IE 6]>
		<link href="css/ie.css" rel="stylesheet" type="text/css" media="screen" />
	<![endif]-->
  <style>
    body {
      margin-left:1.0em;
      margin-top:1.0em;
      background-color:#FFF; !important;
    }
  </style>
  <script language="Javascript">
    function confirma() {
      var c = confirm(<?php echo A_LANG_AVS_JS_EVALUATION_CONFIRM ?>);
      return c;
    }
  </script>
</head>
<body>
<?php
  // se houver algum erro, apresenta o erro e termina o script
  if($erro->quantidade_erro() > 0)
  {
    echo "<div class='erro'>";
    echo $erro->imprimir_erro();
    echo "</div>";
    exit();
  }

?>
<form name="frm_avaliacao" method="post" action="a_corrige_aval.php" target="_self">
<?php
  $numeracao = 1;
  // para cada quest�o, � verificado o tipo, e instanciado um objeto e passado como referencia para montar a quest�o	
  while ($array = $liberar->buscar_questao()) {
    // questao, tipo, peso
     switch ($array['tipo']) {
        case DISSERTATIVA: {
          $q = new Dissertativa($conn, $array['questao']);
          monta_dissertativa($q, $numeracao);
          break;
        }
        case LACUNAS: {
          $q = new Lacunas($conn, $array['questao']);
          monta_lacunas($q, $numeracao);
          break;
        }
        case MULTIPLA_ESCOLHA: {
          $q = new Multipla_Escolha($conn, $array['questao']);
          monta_multipla_escolha($q, $numeracao);
          break;
        }
        case VERDADEIRO_FALSO: {
          $q = new Verdadeiro_Falso($conn, $array['questao']);
          monta_verdadeiro_falso($q, $numeracao);
          break;
        }
     }
     $numeracao++;
  }
?>
<div align="center">
  <input type="submit" value="<?php echo A_LANG_AVS_BUTTON_SUBMIT ?>" class="button" onClick="return confirma()" />
	<input type="hidden" value="<?php echo $liberar->get_avaliacao(); ?>" name="avaliacao" />
	<input type="hidden" value="<?php echo $id_libera; ?>" name="libera" />
	<input type="hidden" value="<?php echo $id_libera_aluno; ?>" name="libera_aluno" />
</div>
</form>
<p>&nbsp;</p>
</body>
</html>
