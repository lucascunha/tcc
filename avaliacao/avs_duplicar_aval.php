<?php

  require_once "avaliacao/questao.const.php";
  require_once "include/adodb/adodb.inc.php";
  require_once "config/configuracoes.php";
	require_once "avaliacao/questao.const.php";
  require_once "avaliacao/class/questao.class.php";
  require_once "avaliacao/class/lacunas.class.php";
  require_once "avaliacao/class/verdadeiro_falso.class.php";
  require_once "avaliacao/class/multipla_escolha.class.php";
  require_once "avaliacao/class/dissertativa.class.php";
  require_once "avaliacao/class/avaliacao.class.php";
	require_once "avaliacao/class/questaoexistente.class.php";
	require_once "avaliacao/funcao/geral.func.php";
	require_once "avaliacao/class/erro.class.php";
	
	require_once "avaliacao/class/avaliacaobusca.class.php";

  // id da questao, se for alterar
  $CodigoDisciplina = isset($_REQUEST['CodigoDisciplina']) ? $_REQUEST['CodigoDisciplina'] : FALSE;
	// numero do topico
	$numtopico = isset($_GET['numtopico']) ? $_GET['numtopico'] : FALSE;
	// grupo
	$grupo = isset($_REQUEST['grupo']) ? $_REQUEST['grupo'] : FALSE;
	// p�gina
	$pagina = !empty($_POST['pagina']) ? (int)$_POST['pagina'] : 1;

  if (!($CodigoDisciplina && $grupo && $numtopico)) {
		echo A_LANG_AVS_ERROR_PARAM;
		return;
	}	
		
  // abrindo conex�o com a base de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)){
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO;
		return;
	}	
	
	$erro = new Erro();
	
	if (isset($_POST['salvar'])) {
		if(isset($_POST['aval'])) {
			foreach ($_POST['aval'] as $k => $value) {
				if($value == 1) {
					$aval = new Avaliacao($conn);
					if(!$aval->clonar($k, $grupo)) $erro->adicionar_erro(A_LANG_AVS_ERROR_ADD_QUESTION_EXIST);
				}
			}
		}
	}	

	
	$avaliacao = new AvaliacaoBusca($conn, $CodigoDisciplina);
	$avaliacao->set_pagina($pagina);
	if(!empty($_POST['palavra'])) $avaliacao->set_palavra_chave(trim($_POST['palavra']));
	
	$rs = $avaliacao->buscar();

  montar_orelha_completa_avaliacao($numtopico, $CodigoDisciplina);
?>
<script type="text/javascript" language="javascript">
	function paginacao(pg) {
		var formulario = document.getElementById('form');
		var pagina = document.getElementById('pagina');
		pagina.value = pg;
		formulario.submit();
	}
</script>
<form action="<?php echo "a_index.php?opcao=DuplicarAvaliacao&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina&grupo=$grupo"; ?>" method="post" name="form" id="form">
<input type="hidden" value="" id="pagina" name="pagina">
<table cellspacing='1' cellpadding='1' width='100%' border='0' bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
  <tr>
		<td>
			<div class="funcao" align="center">
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_ADD_QUESTION; ?></div>
				<?php
				 // alerta de erros ou de sucesso em salvar as quest�es
				if(isset($_POST['salvar'])) {
					if($erro->quantidade_erro() > 0) {
						echo "<div class='erro'>\n";
						echo $erro->imprimir_erro();
						echo "</div>\n";				
					} else {
						echo "<div class='sucesso'>\n";
						echo "<p><span>".A_LANG_AVS_SUCESS."</span></p>\n";
						echo "</div>\n";				
					}			 
				}			 
				?>
				<span><?php echo A_LANG_AVS_LABEL_SEARCH ?>:</span>	
				<input type="text" name="palavra" id="palavra" value="<?php echo $avaliacao->get_palavra_chave() ?>" class="text" style="width:300px"/>
				<input type="submit" name="enviar" value="<?php echo A_LANG_AVS_BUTTON_SEARCH; ?>" class="button" />		
			</div>		
		</td>	
	</tr>
	<tr>
		<td>
			<div class="paginacao" align="center">
			 <?php
				if($avaliacao->numero_paginas() > 1) {
					echo "<table border='0' cellspacing='0' cellpadding='0'>\n";
					echo "<tr>\n";
					echo "  <td style='height:15px;'>\n";
					$paginacao = "";
					// link para o anterior
					$paginacao .= " <a href='javascript:paginacao(1)' title='".A_LANG_AVS_BUTTON_BACK."'><img src='imagens/nav_titulo/irmao_ant.gif' border='0'></a>";
					$paginacao .= "&nbsp;";					
					// cria os links para as p�ginas
					// cria os links para as p�ginas					
					$t =  ceil($pagina/10)*10+1;
					// cria os bot�es para pular 10 p�ginas
					if ($pagina > 10) $paginacao .= "| <a href='javascript:paginacao(".($pagina-10 < 1 ? 1 : $pagina-10).")'>&nbsp;...&nbsp;</a>";
					// coloca o n�mero das p�ginas
					for ($i = ($t - 10); $i < ( $t < $avaliacao->numero_paginas() + 1 ? $t : $avaliacao->numero_paginas() + 1); $i++) {
						if ($i == $pagina) $paginacao .= "| <strong>".($i)."</strong> ";
						else $paginacao .= "| <a href='javascript:paginacao(".($i).")'>".($i)."</a> ";
					}
					// bot�o pula 10 p�ginas para frente
					if ($pagina+ 10 <= $avaliacao->numero_paginas()) $paginacao .= "| <a href='javascript:paginacao(".($pagina+10 > $avaliacao->numero_paginas() ? $avaliacao->numero_paginas() : $pagina+10).")' title='Pular 5 p�ginas para frente'>&nbsp;...&nbsp;</a> ";
					$paginacao .= "|&nbsp;";
					
					// bot�o pr�ximo			
					if($pagina < $avaliacao->numero_paginas()) {
						$paginacao .= "<a href='javascript:paginacao(".($pagina + 1).")' title='".A_LANG_AVS_BUTTON_NEXT."'><img src='imagens/nav_titulo/irmao_pos.gif' border='0'></a>";
					} else {
						$paginacao .= "<a href='#' title='".A_LANG_AVS_BUTTON_NEXT."'><img src='imagens/nav_titulo/irmao_pos.gif' border='0'></a>";
					}	
					echo $paginacao;
					echo "	</td>";
					echo "</tr>";
					echo "</table>";			 
				}
			 ?>
			</div>
		</td>	
	</tr>
	<tr>
		<td align="center">
		<div class="funcao avaliacao">
		<?php
		  // impress�o dos resultados
			if (count($rs) > 0) {
				echo "<table width='90%'>\n";
				echo "<tr class='Titulo'>";
				echo "<td>&nbsp;</td>";
				echo "<td>".A_LANG_AVS_LABEL_QUESTION_ENUNCIATE."</td>";
				echo "<td>".A_LANG_AVS_TABLE_TYPE."</td>";
				echo "</tr>";
				$classe = "zebraA";
				foreach ($rs as $item) {				
      	  echo "<tr class='".$classe."'>\n";
        	echo "<td align='center' width='70px'><input name='aval[".$item['avaliacao']."]' type='checkbox' class='checkbox' value='1'></td>\n";
	        echo "<td>".quote($item['descricao'], 100)."</td>\n";

					echo "<td width='50px'>".$item['topico']."</td>";					
					
  	      echo "</tr>\n";
				  $classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA';
				}
				echo "</table>\n";		
			} else {
				echo A_LANG_AVS_ERROR_QUESTION_NOT_FOUND;
			}
		
		?>
		</div>
		</td>
	</tr>
	<tr>
		<td>
			<div class="funcao">
				<input type="button" class="button" value="<?php echo A_LANG_AVS_BUTTON_BACK; ?>" onClick="<?php echo "javascript:window.location='a_index.php?opcao=TopicoAvaliacao&numtopico=$numtopico&CodigoDisciplina=$CodigoDisciplina&grupo=$grupo'"; ?>">
				<?php 
					if(count($rs) > 0) {
						echo "<input type='submit' class='button' value='".A_LANG_AVS_BUTTON_SAVE."' name='salvar'>";
					}
				?>	
			</div>			
		</td>
	</tr>
</table>
</form>