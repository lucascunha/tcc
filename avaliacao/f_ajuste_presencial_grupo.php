<?php
/**
 * P�gina que ir� listar os t�picos e os cursos para liberar a avalia��o.
 * Lembrando que esta � a primeira das p�ginas em um total de 3 p�ginas.
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @subpackage Liberar Avalia��o
 * @version 1.0 <24/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/questao.class.php";
  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/ajuste.func.php";
	require_once "avaliacao/class/erro.class.php";
	require_once "avaliacao/funcao/calculo_media.func.php";
	require_once "avaliacao/class/liberar.class.php";
	require_once "avaliacao/class/resposta.class.php";

  $CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	$id_libera = isset($_POST['libera']) ? $_POST['libera'] : FALSE;
	global $conteudo;
	
	if(!$CodigoDisciplina || empty($conteudo)) {
		echo A_LANG_AVS_ERROR_PARAM;
		return;
	}

	 // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO;
		return;
	}	

  $vet_conceito = ajuste_presencial_listar_grupo($conn, $CodigoDisciplina);

  $erro_vetor = new Erro(); // erro se n�o haver o disciplinas
	$erro = new Erro(); // erro em salvar

	if(count($vet_conceito) == 0)
		$erro_vetor->adicionar_erro(A_LANG_AVS_ERROR_TOPIC_EVALUATION);

	$divulgar = FALSE; // se pode ser divulgada as notas
	if($id_libera) {
		$libera = new Liberar($conn, $id_libera);
		// verificar o vetor se existe
		if(isset($_POST['aluno'])) {
			foreach ($_POST['aluno'] as $k => $v) {				
				$libera->aluno_alterar($k, ENTREGUE);
				$libera->aluno_nota($k, $v);
				$alunos = $libera->aluno_libera($k);
				if(!calcular_media($conn, $aluno['aluno'])) $erro->adicionar_erro(A_LANG_AVS_ERROR_AVERAGE_CALCULE);
			}
			if($divulgar) {
				$libera->divulga_nota();
			}
			if(!$libera->salvar()) $erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE); 
		}
	}	

	 montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_AJUSTE);
	
?>

<script language="JavaScript" type="text/javascript" src="avaliacao/ajax/ajax.js"></script>
<script language="JavaScript" type="text/javascript">	
  // fun��es de verificacao
  function numero(e) {
    if(window.event) {
    // for IE, e.keyCode or window.event.keyCode can be used
      key = e.keyCode;
    } else if(e.which) {
    // netscape
      key = e.which;
    }
    if (( (key > 47) && (key < 58) ) || (key==8) || (key==9) || (key > 95) && ((key < 106))) return true;
    else return false
  }

  function verifica(obj) {
    if(parseInt(obj.value) > 100) obj.value = 100;
    else if(parseInt(obj.value) == 0) obj.value = 0;    
  }
	
	function divulgacao() {
		var r = confirm('Gostaria de divulgar as notas?');
		if(r == true) {
			var d = document.getElementById('divulgar');
			d.value = 1;
		}
		return true;	
	}
	  
  function carregar(libera){
    var destino = document.getElementById('conteudo');
    destino.innerHTML = "";
    var disc = <?php echo $CodigoDisciplina; ?>;
    var ajax = new AJAX();
    ajax.Texto("avaliacao/ajax/ajax_ajuste_presencial.php?disciplina="+disc+"&libera="+libera,destino);
  } 		
</script>

<table cellspacing='1' cellpadding='1' width='100%'  border='0'  bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
  <tr>
    <td>
		<div class='funcao'>
		<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_ADJUSTMENT_PRESENTIAL; ?></div>
		<?php
			// em caso de erro
			if($erro_vetor->quantidade_erro() > 0) {
				echo "<div class='erro'>\n";
				echo $erro_vetor->imprimir_erro();
				echo "</div>";
			} else {			
				// mostra os erros ou acertos
				if($erro->quantidade_erro() > 0) {
					echo "<div class='erro'>\n";
					echo $erro->imprimir_erro();
					echo "</div>";
				}	elseif(isset($_POST['enviar_nota'])) {
					echo "<div class='sucesso'>\n";
					echo "<p><span>".A_LANG_AVS_SUCESS."</span></p>";
					echo "</div>";					
				} 
				
				// a parte abaixo s� ser� executada se n�o forem encontrado os erros		
				echo "<br />";
				echo "<p>";
				echo "<span>".A_LANG_AVS_LABEL_CHOOSE_EVALUATION.": </span>";
				echo "<select name='libera' id='id' style='width:300px' onchange='carregar(this.value)'>";
				if(empty($_REQUEST['libera'])) echo "<option value='' disabled='disabled' selected='selected'>".A_LANG_AVS_COMBO_SELECT."</option>";
				else echo "<option value='' disabled='disabled' >".A_LANG_AVS_COMBO_SELECT."</option>";

				foreach ($vet_conceito as $conceito) {
					if(!empty($_REQUEST['libera'])) {
						if($_REQUEST['libera'] == $conceito['libera']) echo "<option value='".$conceito['libera']."' selected='selected'>".A_LANG_AVS_LABEL_EVALUATION.": ".$conceito['libera']." - ".quote($conceito['avaliacao']).". ".A_LANG_AVS_COMBO_DATE.": ".formata_data($conceito['data'])."</option>\n";
						else echo "<option value='".$conceito['libera']."' >".A_LANG_AVS_LABEL_EVALUATION.": ".$conceito['libera']." - ".quote($conceito['avaliacao']).". ".A_LANG_AVS_COMBO_DATE.": ".formata_data($conceito['data'])."</option>\n";
					} else {
						echo "<option value='".$conceito['libera']."' >".A_LANG_AVS_LABEL_EVALUATION.": ".$conceito['libera']." - ".quote($conceito['avaliacao']).". ".A_LANG_AVS_COMBO_DATE.": ".formata_data($conceito['data'])."</option>\n";
					}	
				}
				echo "</select>";
				echo "</p>";
			}	
		?>
		</div>
    </td>
  </tr>
	<tr>
		<td>
			<div class="funcao" id="conteudo"></div>
			<?php
				if(isset($_REQUEST['libera'])) {
					echo "<script>carregar(".$_REQUEST['libera'].")</script>";
				}
			?>
		</td>
	</tr>
	<tr>
		<td>
		<div class="funcao">
			<input type="button" name="voltar" value="<?php echo A_LANG_AVS_BUTTON_BACK; ?>" class="button" onclick="javascript:window.location='a_index.php?opcao=FuncaoAjuste&CodigoDisciplina=<?php echo $CodigoDisciplina; ?>'">
			<input name='ajuda' type='button' class='button' value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onclick="<?php echo HELP_FUNCTION_ADJUSTMENT_PRESENTIAL; ?>" />
		</div>
		</td>
	</tr>
</table>
