<?php

/**
 * Script que lista as avalia�oes de um t�pico para ser liberada para resolu�ao
 * Exemplo de URL para a chamada da p�gina:
 * <code>
 *   avaliacao/ajax/ajax_liberar_aval.php?disciplina=$CodigoDisciplina&grupo=$id_grupo
 * </code>   
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Ajax
 * @subpackage Liberar
 * @filesource
 * @version 1.0 16/12/2007
 */
 
/*
 * Trabalho de Conclusao de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

	@session_start();	
  require_once "../config/configuracoes.php";	
  // defini�ao do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica�ao de seguran�a
  if(!isset($_SESSION['logado'])) die(utf8_encode(A_LANG_AVS_ERROR_LOGGED));
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die(utf8_encode("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>"));

	/**
	 * ID da disciplina que � passado via GET (URL)
	 * @global integer $_GET['disciplina']
	 * @name $disciplina
	 */ 	
  $disciplina = isset($_GET['disciplina']) ? $_GET['disciplina']: FALSE;

	/**
	 * ID do grupo que � passado via GET (URL)
	 * @global integer $_GET['grupo']
	 * @name $grupo
	 */ 	
  $id_libera = isset($_GET['libera']) ? $_GET['libera']: FALSE;

  if(!$id_libera || !$disciplina) die(utf8_encode(A_LANG_AVS_ERROR_PARAM));

  define('FPDF_FONTPATH', '../log/pdf/font/');
	require_once "funcao/geral.func.php";
  require_once "questao.const.php";
  require_once "funcao/media.func.php";
  require_once "../include/adodb/adodb.inc.php";
  require_once "funcao/relatorio.func.php";	
	require_once "class/liberar.class.php";
  require_once "class/questao.class.php";
  require_once "class/dissertativa.class.php";
  require_once "class/multipla_escolha.class.php";
  require_once "class/lacunas.class.php";
  require_once "class/verdadeiro_falso.class.php";	
	require_once "class/resposta.class.php";
	require('../log/pdf/fpdf.php');

  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $disciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));	
	
	$libera = new Liberar($conn, $id_libera);
	
	class AvaliacaoPDF extends FPDF {
		var $numero_questao;
		//Page header
		function Header()
		{
			$this->Image('../imagens/logo.jpg',10,10,50);
			$this->SetFont('times','B', 16, 'iso-8859-1');
			$this->Cell(200,20,'Backup',0,1,'C'); 
		}
		
		
		function cabecalho_questao($obj) {
			switch ($obj->get_tipo()) {
				case DISSERTATIVA: $tipo = 'Dissertativa'; break;				
				case MULTIPLA_ESCOLHA: $tipo = 'M�ltipla-Escolha'; break;				
				case VERDADEIRO_FALSO: $tipo = 'Verdadeiro-Falso'; break;				
				case LACUNAS: $tipo = 'Preenchimento de lacunas'; break;
			}		
			$this->Cell(0,5,$tipo, 0, 1);			
			$linha = ++$this->numero_questao . ") ". $obj->get_pergunta();
			$this->MultiCell(0,5, html_entity_decode($linha), 0, 1);		
		}
		
		function rodape_questao($obj, $questao) {
			if($obj->get_explicacao()) {
				$this->SetFont('times', 'I', 9);	
				$this->Cell(0,5,'Explica��o:', 0, 1);
				$this->SetFont('times', '', 9);
				$this->SetX(15);$this->MultiCell(0,5, html_entity_decode($obj->get_explicacao()), 0, 1);			
			}
			$linha = "Peso: ".$questao['peso']."     Cancelada: ".($c = $questao['cancelada'] == 1 ? 'Sim' : 'N�o');
			$this->SetFont('times', 'I', 9);	
			$this->Cell(0,5,$linha, 0, 1);
			$this->SetFont('times', '', 9);
			$this->Line($this->GetX(), $this->GetY(), 200, $this->GetY());			
		}

		
		function cabecalho_questao_aluno($obj, $questao, $alter) {
			switch ($obj->get_tipo()) {
				case DISSERTATIVA: $tipo = 'Dissertativa'; break;				
				case MULTIPLA_ESCOLHA: $tipo = 'M�ltipla-Escolha'; break;				
				case VERDADEIRO_FALSO: $tipo = 'Verdadeiro-Falso'; break;				
				case LACUNAS: $tipo = 'Preenchimento de lacunas'; break;
			}	
			if (is_null($alter['valor'])) $alter['valor'] = 0;
			$linha = $tipo ."     Valor da quest�o: ". $alter['valor'] ."     Peso: ".$questao['peso']. "     Cancelada: ".($c = $questao['cancelada'] == 1 ? 'Sim' : 'N�o');
			$this->SetFont('times', '', 9);
			$this->Cell(0,5,$linha, 0, 1);			
			$linha = ++$this->numero_questao . ") ". $obj->get_pergunta();
			$this->MultiCell(0,5, html_entity_decode($linha), 0, 1);		
		}

		function rodape_questao_aluno($obj, $resposta) {
			$this->SetFont('times', 'I', 9);
			$this->SetX(15);$this->MultiCell(0,5, "Resposta dada: ".html_entity_decode($resposta['resposta']), 0, 1);	
			$this->SetFont('times', '', 9);			
			if($obj->get_explicacao()) {
				$this->SetFont('times', 'I', 9);	
				$this->Cell(0,5,'Explica��o:', 0, 1);
				$this->SetFont('times', '', 9);
				$this->SetX(15);$this->MultiCell(0,5, html_entity_decode($obj->get_explicacao()), 0, 1);			
			}
			$this->Line($this->GetX(), $this->GetY(), 200, $this->GetY());			
		}
		
		function correta($numero) {
			$this->SetFont('times', 'B', 9);
			$this->Cell(0,5,"Resposta Correta: ".$numero,0,1);
			$this->SetFont('times', '', 9);			
		}
		
		function nota($valor, $peso, $cancelada) {
			$this->SetFont('times', 'I', 9);
			if (is_null($valor)) $valor = 0;
			$linha = "Valor da quest�o: ". $valor ."     Peso: ".$peso. "     Cancelada: ".($c = $cancelada == 1 ? 'Sim' : 'N�o');
			$this->Cell(0,5, $linha, 0, 1);
			$this->SetFont('times', '', 9);
		}
		
		// monta questoes da avalia�ao, sem respostas dos alunos
		function monta_dissertativa($obj, $questao) {
			if (!is_a($obj,'Dissertativa')) {
				return FALSE;
			}	
			$this->cabecalho_questao($obj);
			$this->rodape_questao($obj, $questao);
		}	
		
		function monta_multipla_escolha($obj, $questao) {
			if (!is_a($obj,'Multipla_Escolha')) {
				return FALSE;
			}	
			$this->cabecalho_questao($obj);			
			
			$x = 0;
			$correta = NULL;
			while ($array = $obj->buscar_alternativa()) {
				$descricao = ++$x." - ". $array['descricao'];
				if($array['correta']) $correta = $x;
				$this->SetX(15);$this->Cell(0,5, html_entity_decode($descricao),0,1); 				
			}
			$this->correta($correta);
			$this->rodape_questao($obj, $questao);
		}	

		function monta_lacunas($obj, $questao) {
			if (!is_a($obj,'Lacunas')) {
				return FALSE;
			}	
			$this->cabecalho_questao($obj);
			while ($array = $obj->buscar_lacuna()) {
				$descricao = $array['pergunta']." ___________________ ".$array['complemento'];
				$this->SetX(15);$this->Cell(0,5,html_entity_decode($descricao),0,1);
				$this->correta($array['resposta']);
			}
									
			$this->rodape_questao($obj, $questao);
		}	
		
		function monta_verdadeiro_falso($obj, $questao) {
			if (!is_a($obj,'Verdadeiro_Falso')) {
				return FALSE;
			}	
			$this->cabecalho_questao($obj);		
			$this->SetX(15);$this->Cell(0,5,"1 - ".$obj->get_label_falso(),0,1);			
			$this->SetX(15);$this->Cell(0,5,"2 - ".$obj->get_label_verdade(),0,1);
			$this->correta($obj->get_correta() + 1); 
			$this->rodape_questao($obj, $questao);
		}			

		// ------------ PARTE DOS ALUNOS ----------------------
		
		function monta_dissertativa_aluno($obj, $resp) {
			if (!is_a($obj,'Dissertativa')) {
				return FALSE;
			}
			
			if(!is_a($resp, 'Resposta')) {
				return FALSE;	
			}
		
			$alter = $resp->resposta_identificador($obj->get_questao());
			$questao = $resp->questao_identificador($obj->get_questao());			
			$this->cabecalho_questao_aluno($obj, $questao, $alter);
			$this->rodape_questao_aluno($obj, $alter);
		}
		
		function monta_multipla_escolha_aluno($obj, $resp) {
			if (!is_a($obj,'Multipla_Escolha')) {
				return FALSE;
			}
			
			if(!is_a($resp, 'Resposta')) {
				return FALSE;	
			}
		
			$alter = $resp->resposta_identificador($obj->get_questao());
			$questao = $resp->questao_identificador($obj->get_questao());			
			$this->cabecalho_questao_aluno($obj, $questao, $alter);
			$x = 0;
			$correta = NULL;
			while ($array = $obj->buscar_alternativa()) {
				$descricao = ++$x." - ". $array['descricao'];
				if($array['correta']) $correta = $x;
				$this->SetX(15);$this->Cell(0,5, html_entity_decode($descricao),0,1); 				
			}
			$this->correta($correta);
			$this->rodape_questao_aluno($obj, $alter);
		}

		function monta_verdadeiro_falso_aluno($obj, $resp) {
			if (!is_a($obj,'Verdadeiro_Falso')) {
				return FALSE;
			}
			
			if(!is_a($resp, 'Resposta')) {
				return FALSE;	
			}
		
			$alter = $resp->resposta_identificador($obj->get_questao());
			$questao = $resp->questao_identificador($obj->get_questao());			
			$this->cabecalho_questao_aluno($obj, $questao, $alter);
			$this->SetX(15);$this->Cell(0,5,"1 - ".$obj->get_label_falso(),0,1);			
			$this->SetX(15);$this->Cell(0,5,"2 - ".$obj->get_label_verdade(),0,1);
			$this->correta($obj->get_correta() + 1); 
			$this->rodape_questao_aluno($obj, $alter);
		}

		function monta_lacunas_aluno($obj, $resp) {
			if (!is_a($obj,'Lacunas')) {
				return FALSE;
			}
			
			if(!is_a($resp, 'Resposta')) {
				return FALSE;	
			}
		
			$alter = $resp->resposta_identificador($obj->get_questao());
			$questao = $resp->questao_identificador($obj->get_questao());			
			$this->cabecalho_questao_aluno($obj, $questao, $alter);
			$x = 0;
			$resposta = unserialize($alter['resposta']);
			while ($array = $obj->buscar_lacuna()) {
				$descricao = $array['pergunta']." ___________________ ".$array['complemento'];
				$this->SetX(15);$this->Cell(0,5,html_entity_decode($descricao),0,1);
				$this->correta($array['resposta']);
				
			}
			$this->rodape_questao_aluno($obj, $alter);
		}		
			
	} // Fim da classe de PDF
	
	$pdf = new AvaliacaoPDF();
	$pdf->Open();
	$pdf->AddPage();
	$pdf->SetFont('times', 'BI', 12);	
	$pdf->Cell(40,10,'1. Informa��es',0,1);	
	$pdf->SetFont('times', 'B', 9);		
	// Informa�ao da avalia�ao
	$y = $pdf->GetY();
	$pdf->Cell(40,5,'Avalia��o: ',0,1);
	$pdf->Cell(40,5,'Grupo: ',0,1);
	$pdf->Cell(40,5,'Data libera��o: ',0,1);
	$pdf->Cell(40,5,'Data finaliza��o: ',0,1);
	$pdf->Cell(40,5,'Data divulga��o: ',0,1);
	$pdf->Cell(40,5,'Valor das quest�es canceladas: ',0,1);	

	$pdf->SetY($y);
	$pdf->SetFont('times', '', 9);
	$pdf->SetX(70);	$pdf->Cell(60,5, $libera->get_id()." - ".$libera->get_descricao(), 0, 1);	
	$pdf->SetX(70);	$pdf->Cell(60,5, $libera->get_grupo(), 0, 1);
	$pdf->SetX(70);	$pdf->Cell(60,5, formata_data_hora($libera->get_data_inicio()), 0, 1);
	$pdf->SetX(70);	$pdf->Cell(60,5, formata_data_hora($libera->get_data_final()), 0, 1);
	$pdf->SetX(70);	$pdf->Cell(60,5, formata_data_hora($libera->get_data_divulgacao()), 0, 1);
	$pdf->SetX(70);	$pdf->Cell(60,5, $libera->get_nota_cancelada() ."%", 0, 1);
	

	// Impressao das questoes
	$pdf->AddPage();
	$pdf->SetFont('times', 'BI', 12);	
	$pdf->Cell(40,10,'2. Quest�es',0,1);
	$pdf->SetFont('times', '', 9);	
	while($questao = $libera->buscar_questao()) {
		switch($questao['tipo']) {
			case DISSERTATIVA: 
				$q = new Dissertativa($conn, $questao['questao']);
				$pdf->monta_dissertativa($q, $questao);
				break;
			case MULTIPLA_ESCOLHA: 
				$q = new Multipla_Escolha($conn, $questao['questao']);
				$pdf->monta_multipla_escolha($q, $questao);				
				break;
			case VERDADEIRO_FALSO: 
				$q = new Verdadeiro_Falso($conn, $questao['questao']);
				$pdf->monta_verdadeiro_falso($q, $questao);	
				break;
			case LACUNAS: 
				$q = new Lacunas($conn, $questao['questao']);
				$pdf->monta_lacunas($q, $questao);	
				break;
		}
		$pdf->ln(5);	
	}

	// Informa�oes para os alunos
	$alunos = $libera->alunos();

	$_UASORT_CAMPO = "curso";	
	usort($alunos, 'ordena'); //ordena o vetor pelo curso	

	$pdf->AddPage();
	$pdf->SetFont('times', 'BI', 12);	
	$pdf->Cell(40,10,'3. Alunos',0,1);
	$x = 1;
	
	foreach($alunos as $k) {
		$pdf->SetFont('times', 'I', 10);	
		$pdf->Cell(40,10,'3.'.$x.' - '.$k['nome'],0,1);
		$x++;
		$pdf->SetFont('times', '', 9);			
		$pdf->Cell(40,3,'Curso: '.$k['nome_curso'],0,1);
		$pdf->Cell(40,10,'Nota: '.$k['nota'],0,1);
		$pdf->SetFont('times', 'BU', 10);
		$pdf->Cell(40,10,'Respostas',0,1);
		$pdf->SetFont('times', '', 9);
		$resposta = new Resposta($conn, $k['id']);
		$y = 1;
		while($r = $resposta->buscar_questao()) {
			switch ($r['tipo']) {
				case DISSERTATIVA: 					
					$q = new Dissertativa($conn, $r['questao']);
					$pdf->monta_dissertativa_aluno($q, $resposta);
					break; 
				case MULTIPLA_ESCOLHA:	
					$q = new Multipla_Escolha($conn, $r['questao']);
					$pdf->monta_multipla_escolha_aluno($q, $resposta);
					break; 
				case VERDADEIRO_FALSO:	
					$q = new Verdadeiro_Falso($conn, $r['questao']);
					$pdf->monta_verdadeiro_falso_aluno($q, $resposta);
					break;
				case LACUNAS:	
					$q = new Lacunas($conn, $r['questao']);
					$pdf->monta_lacunas_aluno($q, $resposta);
					break; 	
			}
		}		
		$pdf->SetFont('times', 'I', 9);		
		if($x < count($alunos)+1) $pdf->AddPage();
	}
	
	$pdf->Output();


?>
