<?php
/**
 * Faz o ajuste de pesos para o calculo da m�dia
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
	* @subpackage Media
 * @version 1.0 <23/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */
  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/questao.class.php";
  require_once "avaliacao/class/dissertativa.class.php";
  require_once "avaliacao/class/multipla_escolha.class.php";
  require_once "avaliacao/class/lacunas.class.php";
  require_once "avaliacao/class/verdadeiro_falso.class.php";
	require_once "avaliacao/class/erro.class.php";
	require_once "avaliacao/funcao/relatorio.func.php";

	$CodigoDisciplina = $_GET['CodigoDisciplina'];

  montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_RELATORIO);

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	
	$erro_vetor = new Erro();
	
	$vetor = relatorio_lista_curso($conn, $CodigoDisciplina);
	if(count($vetor) == 0) $erro_vetor->adicionar_erro(A_LANG_AVS_ERROR_FUNCTION_EVALUATION_LIBERATE);

?>
<script language="javascript" type="text/javascript" src="avaliacao/ajax/ajax.js"></script>
<script type="text/javascript" language="javascript">
	function listar_relatorio() {
		var ajax = new AJAX();
		var disc = <?php echo $CodigoDisciplina; ?>;
		var aluno = document.getElementById("aluno").value;
		var curso = document.getElementById("curso").value
		var div = document.getElementById("conteudo");
		div.innerHTML = "Carregando <img src='imagens/carrega.gif' border='0'>";
		ajax.Texto("avaliacao/ajax/ajax_relatorio_aluno.php?aluno="+aluno+"&curso="+curso+"&disciplina="+<?php echo $CodigoDisciplina; ?>,div);	
	}	
  
	function visualizar_avaliacao(id) 
	{
    window.open("avaliacao/f_relatorio_visualizar_aval.php?libera_aluno="+id+"&CodigoDisciplina=<?php echo $CodigoDisciplina; ?>", "visualizar_avaliacao");
	}
	
	function carregar_aluno(){
    var destino = document.getElementById('aluno');
    destino.innerHTML = "";
    var disc = <?php echo $CodigoDisciplina; ?>;
    var curso = document.getElementById('curso').value;
    var ajax = new AJAX();
    ajax.Combo("avaliacao/ajax/ajax_relatorio_listar_aluno?disciplina="+disc+"&curso="+curso,destino);
  }
</script>
<table cellspacing='1' cellpadding='1' width='100%'  border='0'  bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
  <tr>
    <td>
		<div class='funcao'>
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_REPORT; ?></div>				
			<br />
			<?php
				if($erro_vetor->quantidade_erro() > 0) {
					echo "<div class='erro'>\n";
					echo $erro_vetor->imprimir_erro();
					echo "</div>";
				} else {								
					echo "<label>Selecione o curso </label>&nbsp;\n";
					echo "<select name='curso' onchange='carregar_aluno()' style='width:300px' id='curso'>\n";
					echo "<option value='' disabled='disabled' selected='selected'>".A_LANG_AVS_COMBO_SELECT."</option>";
					foreach ($vetor as $k) {						
							echo "<option value='".$k['id']."'>".$k['nome']."</option>\n";
					}
					echo "</select>\n";
					echo "<br />";
					echo "<br />";					
					echo "<label>Selecione o aluno </label>&nbsp;\n";
					echo "<select name='aluno' onchange='listar_relatorio()' style='width:300px' id='aluno'>\n";
					echo "<option value='' disabled='disabled' selected='selected'>Selecione primeiro o curso</option>";
					echo "</select>\n";

				}
			?>			
			<div id='conteudo'></div>						
			
		</div>		
		<div class="funcao">
			<input name="voltar" type="button" value="<?php echo A_LANG_AVS_BUTTON_BACK; ?>" class="button" onClick="javascript:window.location='<?php echo "a_index.php?opcao=FuncaoRelatorio&CodigoDisciplina=$CodigoDisciplina" ?>'" />		
			<input name='ajuda' type='button' class='button' value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onClick="<?php echo HELP_FUNCTION_REPORT_STUDENT; ?>" />	
		</div>
    </td>
  </tr>
</table>