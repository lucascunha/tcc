<?php
/**
 * P�gina que ir� listar os t�picos e os cursos para liberar a avalia��o.
 * Lembrando que esta � a primeira das p�ginas em um total de 3 p�ginas.
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @subpackage Liberar Avalia��o
 * @version 1.0 <24/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/questao.class.php";
  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/liberar.func.php";
	require_once "avaliacao/class/erro.class.php";
	
	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	
	if(!$CodigoDisciplina) {
		echo A_LANG_AVS_ERROR_PARAM;
		return;
	}	

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO;
		return;
	}	

  $vet_conceito = liberar_listar_conceito($conn, $CodigoDisciplina);

  $erro_vetor = new Erro();
	if(count($vet_conceito) == 0)
		$erro_vetor->adicionar_erro(A_LANG_AVS_ERROR_TOPIC_EVALUATION);

  montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_LIBERAR);
	
?>

<script language="JavaScript" type="text/javascript" src="avaliacao/ajax/ajax.js"></script>
<script language="JavaScript" type="text/javascript">
  function carregar_avaliacao(){
    var destino = document.getElementById('aval');
    destino.innerHTML = "";
    var disc = <?php echo $CodigoDisciplina; ?>;
    var grupo = document.getElementById('grupo').value;
    var ajax = new AJAX();
    ajax.Combo("avaliacao/ajax/ajax_liberar_aval?disciplina="+disc+"&grupo="+grupo,destino);
  }
	
	function valida() {
		var avaliacao = document.getElementById('aval');
		if(avaliacao.value == "") {
			alert('<?php echo A_LANG_AVS_JS_ALERT_EVALUATION; ?>');
			return false;	
		}	else return true;
	}
</script>

<table cellspacing='1' cellpadding='1' width='100%' border='0' bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
  <tr>
    <td>
		<div class="funcao">
   		<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_LIBERATE; ?> (2/4)</div>
			<?php
				// em caso de erro
				if($erro_vetor->quantidade_erro() > 0) {
					echo "<div class='erro'>\n";
					echo $erro_vetor->imprimir_erro();
					echo "</div>";
				} else {
				// a parte abaixo s� ser� executada se n�o forem encontrado os erros
			?>
      <form action="<?php echo "a_index.php?opcao=FuncaoLiberarAluno&CodigoDisciplina=$CodigoDisciplina"; ?>" method="post" name="form_peso" onSubmit="return valida();">
        <table>
          <tr>					
            <td><?php echo A_LANG_AVS_LABEL_CHOOSE_TOPIC; ?>:</td>
            <td>
              <select name='grupo' id='grupo' onChange='carregar_avaliacao()' style="width:300px">
              <option value="" disabled='disabled' selected='selected'><?php echo A_LANG_AVS_COMBO_SELECT; ?></option>
              <?php
                foreach ($vet_conceito as $conceito) {
                  for($x = 0; $x < count($conteudo); $x++) {
                    if ($conteudo[$x]['NUMTOP'] == $conceito['topico']) {
                      // se o topico tiver um grupo, adicionar o nome dos cursos no select
                      $nome_curso = "";
                      foreach($conceito['curso'] as $curso) {
                        $nome_curso .= "(".$curso.") ";
                      }
                      echo "<option value='".$conceito['grupo']."'>".$conteudo[$x]['DESCTOP']." ".$nome_curso."</option>\n";
                    }
                  }
                }
               ?>
              </select>
            </td>
          </tr>
					<tr><td>&nbsp;</td></tr>
          <tr>
            <td><?php echo A_LANG_AVS_LABEL_CHOOSE_EVALUATION; ?>:</td>
            <td>
              <select name='avaliacao' id='aval' style="width:300px">
                <option value=""><?php echo A_LANG_AVS_COMBO_SELECT; ?></option>
              </select>
            </td>
          </tr>
        </table>
				<div class="funcao"><input name='enviar' value='<?php echo A_LANG_AVS_BUTTON_NEXT; ?>' type='Submit' class='button'></div>
      </form>
		</div>
		<?php } // fim do else ?>			
    </td>
  </tr>
</table>
