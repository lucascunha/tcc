<?php
 /**
 * Script para inser��o/altera��o de quest�es do tipo Preechimento de Lacunas
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Scripts
 * @subpackage Verdadeiro_Falso
 * @version 1.0 21/10/2007
 * @since 08/04/2008 Modifica��o de cores e estrutura no CSS. Utiliza��o da classe Erro.
 * @since 09/04/2008 Retirado o bug de duplicar quest�es na avalia��o
 * @since 09/04/2008 Retirado o bug de criar duas quest�es com IDs diferentes a cada salvar
 */

/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

  @session_start();
  require_once "../../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../../idioma/".$A_LANG_IDIOMA_USER."/geral.php";	
	// verifica se a pessoa est� logada
  if(!isset($_SESSION['logado'])) die(A_LANG_AVS_ERROR_QUESTION_LOGGED);
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>");

  require_once "../include/adodb/adodb.inc.php";
  require_once "../config/configuracoes.php";
	require_once "questao.const.php";
  require_once "class/questao.class.php";
  require_once "class/lacunas.class.php";
  require_once "class/avaliacao.class.php";
	require_once "class/erro.class.php";
	require_once "funcao/geral.func.php";	
	require_once "ajuda.const.php";	

  // id da questao, se for alterar
  $id_questao = isset($_REQUEST['questao']) ? $_REQUEST['questao'] : NULL;
  // id da avaliacao
  $id_aval = isset($_REQUEST['aval']) ? $_REQUEST['aval'] : FALSE;
    // codigo da disciplina
  $CodigoDisciplina = isset($_REQUEST['CodigoDisciplina']) ? $_REQUEST['CodigoDisciplina'] : FALSE;

  if(!($id_aval || $CodigoDisciplina)) die(A_LANG_AVS_ERROR_PARAM);

  // abrindo conex�o com a base de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));

	$erro = new Erro();

  // cria um objeto da questao dissertativa
  $q = new Lacunas($conn, $id_questao);
  // se o formulario foi respondido far� a inser��o ou modifica��o

  if(isset($_POST['enviar'])) {
		// seta as vari�veis para o objeto da quest�o
		$q->set_explicacao($_POST['explicacao']);
		$q->set_disciplina($CodigoDisciplina);
		$q->set_pergunta($_POST['pergunta']);

		// perguntas j� inseridas
		// ATEN��O: as pergunta antigas (j� inseridas) tem o A como prefixo do nome das vari�veis
		if(isset($_POST['aperg'])) {
			foreach ($_POST['aperg'] as $k => $value) {
				if($value && !empty($_POST['aresp'][$k])) {
					$q->alterar_lacuna($k, $value, $_POST['aresp'][$k], $_POST['acompl'][$k]);
				}
			}
		}

		// para as novas perguntas
		// ATEN��O: as novas pergunta tem o N como prefixo do nome das vari�veis
		if(isset($_POST['nperg'])) {
			foreach ($_POST['nperg'] as $k => $value) {
				if($value && !empty($_POST['nresp'][$k])){
					$q->adicionar_lacuna($value, $_POST['nresp'][$k], $_POST['ncompl'][$k]);
				}
			}
		}

		if (!$q->salvar()) {
			$erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE);
		}	else { //@since 05/08/2008 Adicionado para sair da popup se foi inserido com sucesso
			echo "<script language='javascript' type='text/javascript'>window.close()</script>";
		}		
		
		/**
		 * @since 09/04/2008 Bug de duplicar quest�es quando salva
		 */
		$id_questao = $q->get_questao(); 

		if($erro->quantidade_erro() == 0) {
			$aval = new Avaliacao($conn, $id_aval);
			/**
			 * @since 09/04/2008 Adiciona dentro da avalia��o apenas se n�o tiver a quest�o. Evitando quest�es duplicadas
			 */
			if(!$aval->questao_identificador($q->get_questao())) {
				$aval->atribuir_questao($q->get_questao());
				if(!$aval->salvar()) {
					$erro->adicionar_erro(A_LANG_AVS_ERROR_QUESTION_EVALUATION);
				}	
			}
		}
  }
	
	if(isset($_POST['deletar'])) {
		$q->remover_lacuna($_POST['deletar']);
		if(!$q->salvar()) $erro->adicionar_erro(A_LANG_AVS_ERROR_QUESTION_LACUNA_DELETE);
	}

  // fecha a conex�o com a base de dados
  $conn->Close();


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv=Content-Type content="text/html; charset=ISO-8859-1">
	<meta http-equiv=expires content="0">
	<meta http-equiv=Content-Language content="pt-br">
	<meta name=KEYWORDS content="Adpta��o, Ensino, Distância">
	<meta name=DESCRIPTION content="Módulo de Avalia��o Somativa">
	<meta name=ROBOTS content="INDEX,FOLLOW">
	<meta name=resource-type content="document">
	<meta name=AUTHOR content="Claudiomar Desanti">
	<meta name=COPYRIGHT content="Copyright (c) 2007 by Universidade do Estado de Santa Catarina - UDESC">
	<meta name=revisit-after content="1 days">
	<meta name=distribution content="Global">
	<meta name=GENERATOR content="Easy Eclipse For PHP">
	<meta name=rating content="General">

	<link rel="StyleSheet" href="../css/geral.css" type="text/css">
	<link rel="stylesheet" href="css/ff.css" type="text/css">

<title>Quest�o Preechimento de Lacunas</title>
<style>
 .tamanho {
    width:250px;
  }
</style>
<script language="javascript" type="text/javascript">

var contador = 1;

function add() {
  var navegador = navigator.userAgent.toLowerCase();
  var ie = navegador.indexOf('msie'); // verifica o tipo de navegador

  var td1_1 = document.createElement('td'); // cria��o das TD
  var td1_2 = document.createElement('td'); // cria��o das TD
  var td2_1 = document.createElement('td'); // cria��o das TD
  var td2_2 = document.createElement('td'); // cria��o das TD
  var td3_1 = document.createElement('td'); // cria��o das TD
  var td3_2 = document.createElement('td'); // cria��o das TD
  var td4 =  document.createElement('td'); // cria��o das TD

  contador++;

  if(ie != -1) {
    // c�digo para o Internet Explorer
    var tr1 = document.getElementById('lacuna').insertRow(-1);

    var tr2 = document.getElementById('lacuna').insertRow(-1);
    var tr3 = document.getElementById('lacuna').insertRow(-1);
    var tr4 = document.getElementById('lacuna').insertRow(-1);
  } else	{
    // c�digo para os outros navegadores (os que funcionam)
    var tb_lacuna = document.getElementById('lacuna');

    var tr1 = document.createElement('tr');
    var tr2 = document.createElement('tr');
    var tr3 = document.createElement('tr');
    var tr4 = document.createElement('tr');

    tb_lacuna.appendChild(tr1);
    tb_lacuna.appendChild(tr2);
    tb_lacuna.appendChild(tr3);
    tb_lacuna.appendChild(tr4);
  }


  td1_1.innerHTML = "<?php echo A_LANG_AVS_TABLE_QUESTION_LACUNA1 ?>: *";
  td1_1.width = '130';
  td1_2.innerHTML = "<input type='text' name='nperg["+contador+"]' class='text tamanho'>";
  td2_1.innerHTML = "<?php echo A_LANG_AVS_TABLE_QUESTION_LACUNA2 ?>: *";
  td2_2.innerHTML = "<input type='text' name='nresp["+contador+"]' class='text tamanho'>";
  td3_1.innerHTML = "<?php echo A_LANG_AVS_TABLE_QUESTION_LACUNA3 ?>:";
  td3_2.innerHTML = "<input type='text' name='ncompl["+contador+"]' class='text tamanho'>";
  td4.colSpan = "2";
  td4.innerHTML = "&nbsp;";

  tr1.appendChild(td1_1);
  tr1.appendChild(td1_2);
  tr2.appendChild(td2_1);
  tr2.appendChild(td2_2);
  tr3.appendChild(td3_1);
  tr3.appendChild(td3_2);
  tr4.appendChild(td4);
}


function remover(id) {
  var confirma = confirm("<?php echo A_LANG_AVS_JS_ALERT_QUESTION_LACUNA_DELETE ?>");
  if(confirma) {
    var del = document.getElementById('deletar');
		var f = document.getElementById('form');
		del.value = id;
		f.submit();		
		//window.open("remove.php?tipo=l&id="+id+"&questao=<?php echo $q->get_questao(); ?>", "remove");
  }
}

function recarrega() {
  window.opener.location.reload();
  self.close();
}

function iniciar() {
	var pergunta = document.getElementById('pergunta');
	pergunta.focus();
}

</script>
</head>

<body style="background-color: <?php echo $A_COR_FUNDO_ORELHA_ON ?>;">

<div class="identificacao">
  <h1><?php echo A_LANG_AVS_LABEL_QUESTION_IDENTIFY_LACUNA ?></h1>
</div>
<?php
		if(isset($_POST['enviar'])) {
			if ($erro->quantidade_erro() == 0) {
				echo "<div class='sucesso' id='info'>\n";
				echo "<p><span>".A_LANG_AVS_SUCESS."</span></p>\n";
				echo "</div>\n";
			} else {
				echo "<div class='erro' id='info'>\n";
				echo $erro->imprimir_erro();
				echo "</div>\n";
			}
		}
?>
<form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF']."?CodigoDisciplia=".$CodigoDisciplina."&questao=".$id_questao."&aval=".$id_aval; ?>" id="form">
  <input type="hidden" name="deletar" id="deletar" value="">
<table align="center">
	<tr><td>
  <table border="0" cellpadding="1" cellspacing="2" id="lacuna" align="center">
    <?php
      // vem toda a parte de colocar as pergunta j� feitas
    if($q->numero_lacunas() > 0) {
      while($array = $q->buscar_lacuna()){
        echo "    <tr>\n";
        echo "      <td width='130'>".A_LANG_AVS_TABLE_QUESTION_LACUNA1." *</td>\n";
        echo "      <td width='330'><input type='text' name='aperg[".$array['id_lacuna']."]' class='text tamanho' value='".$array['pergunta']."'></td>\n";
        echo "    </tr>\n";
        echo "    <tr>\n";
        echo "      <td>".A_LANG_AVS_TABLE_QUESTION_LACUNA2.": *</td>\n";
        echo "      <td><input type='text' name='aresp[".$array['id_lacuna']."]' class='text tamanho' value='".$array['resposta']."'></td>\n";
        echo "    </tr>\n";
        echo "    <tr>\n";
        echo "      <td>".A_LANG_AVS_TABLE_QUESTION_LACUNA3.": *</td>\n";
        echo "      <td><input type='text' name='acompl[".$array['id_lacuna']."]' class='text tamanho' value='".$array['complemento']."'></td>\n";
        echo "    </tr>\n";
        echo "    <tr>\n";
        echo "      <td></td><td>";
        // para n�o deixar excluir a �ltima lacuna
        if($q->numero_lacunas() > 1) {
          echo "<input name='excluir' type='button' class='button' value='".A_LANG_AVS_BUTTON_QUESTION_DELETE_LACUNA."' onClick='remover(".$array['id_lacuna'].")'>";
        } else {
          echo "&nbsp;";
        }
        echo "</td>\n";
        echo "    </tr>\n";
      }
    } else {
        echo "    <tr>\n";
        echo "      <td width='130'>".A_LANG_AVS_TABLE_QUESTION_LACUNA1.": *</td>\n";
        echo "      <td width='330'><input type='text' name='nperg[1]' class='text tamanho'></td>\n";
        echo "    </tr>\n";
        echo "    <tr>\n";
        echo "      <td>".A_LANG_AVS_TABLE_QUESTION_LACUNA2.": *</td>\n";
        echo "      <td><input type='text' name='nresp[1]' class='text tamanho'></td>\n";
        echo "    </tr>\n";
        echo "    <tr>\n";
        echo "       <td>".A_LANG_AVS_TABLE_QUESTION_LACUNA3.":</td>\n";
        echo "       <td><input type='text' name='ncompl[1]' class='text tamanho'></td>\n";
        echo "    </tr>\n";
        echo "    <tr>\n";
        echo "       <td colspan='2' align='right>&nbsp;</td>\n";
        echo "    </tr>\n";
    }
  ?>
  </table>		
</td></tr>
<tr><td>
  <table border="0">
    <tr>
      <td><?php echo A_LANG_AVS_LABEL_QUESTION_ENUNCIATE ?> * </td>
      <td><textarea rows="5" name="pergunta" class="button tamanho" id="pergunta"><?php echo $q->get_pergunta(); ?></textarea></td>
    </tr>
    <tr>
			<td width="130"><?php echo A_LANG_AVS_LABEL_QUESTION_EXPLANATION ?></td>
      <td width="330"><textarea rows="5" name="explicacao" class="button tamanho"><?php echo $q->get_explicacao(); ?></textarea></td>
    </tr>
		<tr><td><div class="alerta"><p><?php echo A_LANG_AVS_TEXT_FIELD_REQUIRE; ?></p></div></td></tr>
    <tr>
      <td colspan="2">
      <div class="funcao">
				<input type="button" name="ajuda" value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onclick="<?php echo HELP_LAC ?>" class="button" >
				<input type="button" name="fechar" value="<?php echo A_LANG_AVS_BUTTON_CLOSE; ?>" class="button" onClick="recarrega()">
	      <input name="novo" type="button" class="button" id="novo2" value="<?php echo A_LANG_AVS_BUTTON_QUESTION_ADD_LACUNA; ?>" onClick="add()">
        <input type="submit" class="button" value="<?php echo A_LANG_AVS_BUTTON_SAVE; ?>" name="enviar">
			</div>	
      </td>
    </tr>
  </table>
	</td></tr>
</table>	
</form>
</body>
</html>
