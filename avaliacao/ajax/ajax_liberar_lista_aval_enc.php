<?php
/**
 * Script que lista todas as avalia��es liberadas e que ainda n�o foram encerradas para uma determinada disciplina
 * Exemplo de URL para a chamada da p�gina:
 * <code>
 *   avaliacao/ajax/ajax_liberar_lista_aval.php?disciplina=$CodigoDisciplina
 * </code>   
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Ajax
 * @subpackage Liberar
 * @filesource
 * @version 1.0 16/12/2007
 */
 
/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

	@session_start();	
  require_once "../../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica��o de seguran�a
  if(!isset($_SESSION['logado'])) die(utf8_encode(A_LANG_AVS_ERROR_LOGGED));
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die(utf8_encode("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>"));

	require_once "../funcao/geral.func.php";
  require_once "../funcao/liberar.func.php";
	require_once "../class/liberar.class.php";
  require_once "../../include/adodb/adodb.inc.php";
	
	/**
	 * ID da disciplina que � passado via GET (URL)
	 * @global integer $_GET['disciplina']
	 * @name $disciplina
	 */ 	
	$disciplina = isset($_GET['disciplina']) ? $_GET['disciplina']: FALSE;
	
	if(!$disciplina) die(utf8_encode(A_LANG_AVS_ERROR_PARAM));
	
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $disciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));

	$vet_lista = liberar_listar_avaliacao_encerrada($conn, $disciplina);
	
	$saida = "";
	
	if(count($vet_lista) > 0) {
		$saida .= "<fieldset class='field'>\n";
		$saida .= "<legend style='background-color:".$A_COR_FUNDO_ORELHA_ON."'>".A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION4."</legend>";
		foreach ($vet_lista as $lista) {
			$saida .= "<div class='lista_aval'>\n";
			
			// lista de info
			$saida .= "<div class='lista'>\n";
			$saida .= "<ul>";
			$saida .= "<li><span class='titulo'>".A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION2.": </span><span class='texto'>".$lista['topico']."</span></li>";
			$saida .= "<li><span class='titulo'>".A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION3.": </span><span class='texto'>".$lista['descricao']."</span></li>";
			if($lista['presencial'] == 1) {
				$saida .= "<li><span class='texto'>".A_LANG_AVS_LABEL_AJAX_LIBERATE_EVALUATION5."</span></li>\n";
			}
			$saida .= "</ul>";			
			$saida .= "</div>";
			// botoes
			$saida .= "<div class='lista_button'>";
			$saida .= "<ul>";			
			$saida .= "<li><input type='button' name='aluno' value='".A_LANG_AVS_BUTTON_AJAX_LIST."' class='button' onClick='javascript:listar_alunos(".$lista['libera'].",\"lista_aberta_".$lista['libera']."\")'></li>";
			$saida .= "</ul>";
			$saida .= "</div>";
			$saida .= "<br /><br />";
			// alunos
			$saida .= "<div id='lista_aberta_".$lista['libera']."' class='lista_aluno hidden'>";
			$saida .= "</div>";

			$saida .= "</div>";
			$saida .= "<br />";
		}		
	} else {
		$saida .= "<div class='erro'>\n";
		$saida .= "  <p><span>".A_LANG_AVS_ERROR_AJAX_LIBERATE2."</span></p>\n";
		$saida .= "</div>";		
	}
	
	echo utf8_encode($saida);

?>