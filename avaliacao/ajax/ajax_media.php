<?php
 /**
 * Script que lista os grupos de avalia��o para uma disciplina para que possa ser feita a m�dia
 * Exemplo de URL para a chamada da p�gina:
 * <code>
 *   avaliacao/ajax/ajax_media.php?disciplina=$CodigoDisciplina
 * </code>   
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Ajax
 * @subpackage Media
 * @version 1.0 26/02/2008
 * @since 10/04/2008 Arrumado para mostrar mensagem se n�o houver grupos para a disciplina escolhida
 */
 
/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	
  
	@session_start();	
  require_once "../../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica��o de seguran�a
  if(!isset($_SESSION['logado'])) die(utf8_encode(A_LANG_AVS_ERROR_LOGGED));
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die(utf8_encode("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>"));

  require_once "../funcao/geral.func.php";
  require_once "../questao.const.php";
  require_once "../funcao/media.func.php";
  require_once "../../include/adodb/adodb.inc.php";

	/**
	 * ID do curso que � passado via GET (URL)
	 * @global integer $_GET['curso']
	 * @name $curso
	 */ 		
  $curso = isset($_GET['curso']) ? $_GET['curso']: FALSE;
  $disciplina = isset($_GET['disciplina']) ? $_GET['disciplina']: FALSE;

  if(!$curso || !$disciplina) die(utf8_encode(A_LANG_AVS_ERROR_PARAM));

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	
	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $disciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));

  $vet_topico = media_listar_topico($conn, $curso, $disciplina);

  $saida = "";
  if (count($vet_topico) > 0)  {
		$saida .= "<form action='a_index.php?opcao=FuncaoMedia&CodigoDisciplina=$disciplina' method='post' name='formCurso'>";
		$saida .= "<input type='hidden' value='".$curso."' name='curso'>";
		$saida .= "<table border='0' width='90%' align='center'>";
		$saida .= "	<tr class='Titulo' align='center'>";
		$saida .= "		<td width='90%'>".A_LANG_AVS_LABEL_TOPIC."</td>";
		$saida .= "		<td>".A_LANG_AVS_TABLE_WEIGHT."</td>";
		$saida .= "	</tr>";
		$tab_control = 1;
	
		$classe = 'zebraA'; // classe css
	
		$total = 0; // total de pontos
	
		foreach($vet_topico as $k) {
			$saida .= "<tr class='".$classe."'>";
			$saida .= "  <td>".A_LANG_AVS_LABEL_AJAX_AVERAGE." ".$k['topico']." </td>";
			$saida .= "	 <td><input type='text' value='".$k['peso']."' name='peso[".$k['grupo']."]' class='text' size='10' tabindex='".$tab_control."' onkeydown='return numero(event)' onkeyup='verifica(this)'></td>";
			$saida .= "</tr>";
			$total += $k['peso'];
			$tab_control++;
			$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA';
		}
	
		$saida .= "	<tr>";
		$saida .= "		<td align='right'>".A_LANG_AVS_TABLE_SUM_WEIGHT.":</td>";
		$saida .= "		<td align='center'><div id='total'>".$total."</div></td>";
		$saida .= "	</tr>";
		$saida .= "	<tr>";
		$saida .= "		<td align='right'>".A_LANG_AVS_TABLE_REST_WEIGH.":</td>";
		$saida .= "		<td align='center'><div id='resto'>".(100 - $total)."</div></td>";
		$saida .= "	</tr>";
		$saida .= "	<script>atualiza_divs()</script>";
		$saida .= "</table>";
		$saida .= "		<div class='funcao'>\n";
		// calculo dos valores para cada quest�o
		$valor = @floor(100 / count($vet_topico));
		$ultimo = 100 % count($vet_topico) + $valor;
		$saida .= "			<input type='button' name='dist' value='".A_LANG_AVS_BUTTON_WEIGHT."' class='button' onclick='distribuir_peso_igual(".$valor.",".$ultimo.")'>";
		$saida .= "			<input name='enviar_peso' type='submit' class='button' value='".A_LANG_AVS_BUTTON_SAVE."' onclick='return salvar()'>";
		$saida .= "		</div>\n";	
		$saida .= "</form>";
	} else {
		$saida .= A_LANG_AVS_ERROR_AJAX_AVERAGE3;
	}	

  echo utf8_encode($saida);

?>
