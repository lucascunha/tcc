<?php
/**
 * Script que lista os alunos de uma avalia��o que est� em andamento, mostrando se a avalia��o foi entregue
 * Exemplo de URL para a chamada da p�gina:
 * <code>
 *   avaliacao/ajax/ajax_liberar_lista_aluno.php?libera=$liberada
 * </code>   
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Ajax
 * @subpackage Liberar
 * @filesource
 * @version 1.0 16/12/2007
 */
 
/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

	@session_start();	
  require_once "../../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica��o de seguran�a
  if(!isset($_SESSION['logado'])) die(utf8_encode(A_LANG_AVS_ERROR_LOGGED));
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die(utf8_encode("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>"));

  require_once "../funcao/geral.func.php";
	require_once "../funcao/liberar.func.php";
  require_once "../../include/adodb/adodb.inc.php";
	require_once "../class/liberar.class.php";

	/**
	 * ID da avalia��o liberada que � passado via GET (URL)
	 * @global integer $_GET['libera']
	 * @name $libera
	 */ 		
	$id_libera = isset($_GET['libera']) ? $_GET['libera'] : FALSE;	
	$disciplina = isset($_GET['disciplina']) ? $_GET['disciplina']: FALSE;
	
	if(!$id_libera || !$disciplina) die(utf8_encode(A_LANG_AVS_ERROR_PARAM));

  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	
	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $disciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));
	
	$vet_aluno = liberar_listar_aluno_realizada($conn, $id_libera);
	
	$saida = "<fieldset><legend>".A_LANG_AVS_STUDENTS."</legend>";
		
	$saida .= "<table width='90%' border='0' cellspacing='0' cellpadding='0'>\n";
	$classe = "zebraA";
	foreach($vet_aluno as $aluno) {
		$saida .= "<tr class='".$classe."'>\n";
		$saida .= "<td width='85%'>".$aluno['nome']."</td>\n";
		switch ($aluno['status']) {
			case PENDENTE: $saida .= "<td width='15%' class='pendente'>".A_LANG_AVS_LABEL_AJAX_LIBERATE1."</td>\n"; break;
			case REALIZANDO: $saida .= "<td width='15%' class='pendente'>".A_LANG_AVS_LABEL_AJAX_LIBERATE2."</td>\n"; break;
			case ENTREGUE: $saida .= "<td width='15%' class='ok'>".A_LANG_AVS_LABEL_AJAX_LIBERATE3."</td>\n"; break;
			case NAOENTREGUE: $saida .= "<td width='15%' class='naoentregue'>".A_LANG_AVS_LABEL_AJAX_LIBERATE4."</td>\n"; break;
			case ZERADA: $saida .= "<td width='15%' class='zerado'>".A_LANG_AVS_LABEL_AJAX_LIBERATE5."</td>\n"; break;
		}
		$saida .= "</tr>\n";
		$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA';
	}
	$saida .= "</table>\n";
	$saida .= "</fieldset>";
	
	echo utf8_encode($saida);
?>