<?php
/**
 * Script utilizado para divulgar a nota dos alunos
 * Exemplo de URL para a chamada da p�gina:
 * <code>
 *   avaliacao/ajax/ajax_divulgar_nota.php?libera=$liberada&disciplina=$CodigoDisciplina
 * </code> 
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Ajax
 * @subpackage Divulgar_Nota
 * @filesource
 * @version 1.0 05/01/2008
 * @since 10/04/2008 Arrumado bug de aparecer avalia��es sem aluno.
 */
 
/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	


	@session_start();	
  require_once "../../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica��o de seguran�a
  if(!isset($_SESSION['logado'])) die(utf8_encode(A_LANG_AVS_ERROR_LOGGED));
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die(utf8_encode("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>"));

  require_once "../funcao/divulgar.func.php";
  require_once "../../include/adodb/adodb.inc.php";
  require_once "../class/liberar.class.php";
  require_once "../class/avaliacao.class.php";
  require_once "../class/resposta.class.php";
  require_once "../funcao/geral.func.php";
  require_once "../questao.const.php";

	/**
	 * ID da avalia��o liberada que � passado via GET (URL)
	 * @global integer $_GET['libera']
	 * @name $libera
	 */ 		
  $id_libera = isset($_GET['libera']) ? $_GET['libera'] : FALSE;

	/**
	 * ID da disciplina que � passado via GET (URL)
	 * @global integer $_GET['disciplina']
	 * @name $disciplina
	 */ 			
  $disciplina = isset($_GET['disciplina']) ? $_GET['disciplina'] : FALSE;
	
	if(!$id_libera || !$disciplina) die(utf8_encode(A_LANG_AVS_ERROR_PARAM));

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $disciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));

	$libera = new Liberar($conn, $id_libera);
	
  $saida = "";	
	if($libera->get_status() == ENCERRADA) {
		$saida .= "<div class='lista_aval'>\n";
		$saida .= "<form action='a_index.php?opcao=FuncaoDivulgar&CodigoDisciplina=$disciplina' method='post' name='frmdivulgar'>\n";
		$saida .= "<input type='hidden' value='".$id_libera."' name='libera'>\n";

		$pode_liberar = TRUE;
		
		// se for avalia�ao presencial
		if ($libera->get_presencial()) {
			$alunos = $libera->alunos();
			
			$_UASORT_CAMPO = "curso";	
			usort($alunos, 'ordena'); //ordena o vetor pelo curso 
			
			$curso_atual = NULL;

			foreach ($alunos as $k) {
				$aluno = $libera->aluno_libera($k['id']);				

				if($aluno['curso'] != $curso_atual) {
					if($curso_atual != NULL) {
							$saida .= "</table>";
							$saida .= "</fieldset>";
							$saida .= "</div>";
					}
					// um fieldset para cada grupo
					$saida .= "<div class='lista_aluno'>\n";
					$saida .= "<fieldset class='field'>\n";
					$saida .= "<legend>".$aluno['nome_curso']."</legend>";
					$saida .= "<table width='90%' border='0' cellspacing='0' cellpadding='0'>";
					$saida .= "<tr class='Titulo' align='center'>\n";
					$saida .= "<td width='80%'><strong>".A_LANG_AVS_TABLE_AJAX_DIVULGE1."</strong></td>\n";
					$saida .= "<td width='20%'><strong>".A_LANG_AVS_TABLE_AJAX_DIVULGE2."</strong></td>\n";
					$saida .= "</tr>\n";
					$classe = "zebraA";				
				}
			
				$curso_atual = $aluno['curso'];
				
				if(!is_null($aluno['nota'])) {
					$saida .= "<tr class='".$classe."'>\n";
					$saida .= "<td>".$aluno['aluno']." - ".$aluno['nome']."</td>\n";
					$saida .= "<td align='center' class='ok'>".A_LANG_AVS_LABEL_AJAX_DIVULGE1."</td>\n";
					$saida .= "</tr>\n";
				} else {
					$saida .= "<tr class='".$classe."'>\n";
					$saida .= "<td><a href='a_index.php?opcao=FuncaoAjustarDissertCorrecao&CodigoDisciplina=$disciplina&libera=$id_libera&libera_aluno=".$k['id']."'>".$aluno['aluno']." - ".$aluno['nome']."</a></td>\n";
					$saida .= "<td align='center' class='pendente'>".A_LANG_AVS_LABEL_AJAX_DIVULGE2."</td>\n";
					$saida .= "</tr>\n";
				}
				$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA';				
			}		
			$pode_liberar = $libera->verifica_nota_presencial();
			$saida .= "</table>";
			$saida .= "</fieldset>";
			$saida .= "</div>";	
		
		} else { // para avalia�oes automaticas
		
			$alunos = $libera->alunos();			
			$_UASORT_CAMPO = "curso";	
			usort($alunos, 'ordena'); //ordena o vetor pelo curso 
			
			$curso_atual = NULL;
		
			foreach ($alunos as $k) {			
				$aluno = $libera->aluno_libera($k['id']);	
				if($aluno['curso'] != $curso_atual) {
					if($curso_atual != NULL) {
							$saida .= "</table>";
							$saida .= "</fieldset>";
							$saida .= "</div>";
					}
					// um fieldset para cada grupo
					$saida .= "<div class='lista_aluno'>\n";
					$saida .= "<fieldset class='field'>\n";
					$saida .= "<legend>".$aluno['nome_curso']."</legend>";
					$saida .= "<table width='90%' border='0' cellspacing='0' cellpadding='0'>";
					$saida .= "<tr class='Titulo' align='center'>\n";
					$saida .= "<td width='80%'><strong>".A_LANG_AVS_TABLE_AJAX_DIVULGE1."</strong></td>\n";
					$saida .= "<td width='20%'><strong>".A_LANG_AVS_TABLE_AJAX_DIVULGE2."</strong></td>\n";
					$saida .= "</tr>\n";
					$classe = "zebraA";				
				}
			
				$curso_atual = $aluno['curso'];

				$resposta = new Resposta($conn, $k['id']);
				$corrigida = $resposta->verificar_questao_corrigida();
				$aluno = $libera->aluno_libera($k['id']);			
			
				if($corrigida) {
					$saida .= "<tr class='".$classe."'>\n";
					$saida .= "<td>".$aluno['aluno']." - ".$aluno['nome']."</td>\n";
					$saida .= "<td align='center' class='ok'>".A_LANG_AVS_LABEL_AJAX_DIVULGE1."</td>\n";
					$saida .= "</tr>\n";					
				} else {
					$saida .= "<tr class='".$classe."'>\n";
					$saida .= "<td><a href='a_index.php?opcao=FuncaoAjustarDissertCorrecao&CodigoDisciplina=$disciplina&libera=$id_libera&libera_aluno=".$k['id']."'><span class='bold'>".$aluno['aluno']." - ".$aluno['nome']."</span></a></td>\n";
					$saida .= "<td align='center' class='pendente'>".A_LANG_AVS_LABEL_AJAX_DIVULGE2." (".$resposta->quantidade_questao_pendente()." ".strtolower(A_LANG_AVS_LABEL_QUESTION).")</td>\n";
					$saida .= "</tr>\n";
					$pode_liberar = FALSE;
				}
				$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA';
				unset($resposta);				
			}
			$saida .= "</table>";
			$saida .= "</fieldset>";
			$saida .= "</div>";				
		}	
		
		if($pode_liberar) {
			$saida .= "<input type='submit' class='button' name='enviar' value='".A_LANG_AVS_BUTTON_AJAX_DIVULGE."' />\n";
		} else {
			$saida .= "<div class='alerta' style='margin-left:0px; margin-right:0px'>";
			$saida .= "<p><span>".A_LANG_AVS_ERROR_AJAX_DIVULGE1."</span></p>\n";
			$saida .= "</div>";
		}
		$saida .= "</form>";
		$saida .= "</div>";
	} else {
	  $saida = "<div class='erro'><p>".A_LANG_AVS_ERROR_AJAX_DIVULGE2."</p></div>";
	}	

  echo utf8_encode($saida);
?>