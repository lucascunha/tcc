<?php
 /**
 * Script que lista as quest�es de uma avalia��o realizada para que as quest�es possam ser anuladas
 * A partir desta lista o professor pode escolher qual quest�o ser� anulada
 * Exemplo de URL para a chamada da p�gina:
 * <code>
 *   avaliacao/ajax/ajax_anular_lista.php?avaliacao=$id_avaliacao
 * </code>   
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Ajax
 * @subpackage Anular_Questao
 * @filesource 
 * @version 1.0 05/01/2008
 */
 
/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

	@session_start();	
  require_once "../../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica��o de seguran�a
  if(!isset($_SESSION['logado'])) die(utf8_encode(A_LANG_AVS_ERROR_LOGGED));
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die(utf8_encode("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>"));
 
  require_once "../funcao/ajuste.func.php";
  require_once "../../include/adodb/adodb.inc.php";
	require_once "../questao.const.php";
	require_once "../funcao/ajuste.func.php";
	require_once "../funcao/geral.func.php";
	require_once "../class/liberar.class.php";
	require_once "../class/resposta.class.php";
	require_once "../class/questao.class.php";
	require_once "../class/multipla_escolha.class.php";
	require_once "../class/lacunas.class.php";
	require_once "../class/verdadeiro_falso.class.php";
	require_once "../class/dissertativa.class.php";
	require_once "../class/estatistica.class.php";
	require_once "../funcao/relatorio.func.php";

	/**
	 * ID da avalia��o que � passado via GET (URL)
	 * @global integer $_GET['avaliacao']
	 * @name $avaliacao
	 */ 		
	$id_libera = isset($_GET['avaliacao']) ? $_GET['avaliacao'] : FALSE;
	
  $disciplina = isset($_GET['disciplina']) ? $_GET['disciplina']: FALSE;

  if(!$id_libera || !$disciplina) die(utf8_encode(A_LANG_AVS_ERROR_PARAM));

  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $disciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));
	
	$libera = new Liberar($conn, $id_libera);
	$saida = "";
	$saida .= "<div class='lista_aval'>";
	
	// Informa�oes b�sicas
	$saida .= "<div class='info'>";
	$saida .= "<fieldset>";
	$saida .= "<legend>Informa&ccedil;&otilde;es: </span></legend>";
	$saida .= "<p><span>Avalia&ccedil;&atilde;o: </span>".$libera->get_id()."</p>";
	$saida .= "<p><span>Descri&ccedil;&atilde;o: </span>".$libera->get_descricao()."</p>";
	$saida .= "<p><span>Data de libera&ccedil;&atilde;o: </span>".formata_data_hora($libera->get_data_inicio())."</p>";
	$saida .= "<p><span>Data de entregua: </span>".formata_data_hora($libera->get_data_final())."</p>";
	$saida .= "<p><span>Data de divulga&ccedil;&atilde;o das notas: </span>".formata_data_hora($libera->get_data_divulgacao())."</p>";
	$saida .= "</fieldset>";
	$saida .= "</div>";
	$saida .= "<br />";
	
	
	$alunos = $libera->alunos();
	$notas = array();	

	foreach($alunos as $k) {
		$notas[$k['aluno']] = $k['nota'];		
	}	

	$est = new Estatistica();
	$est->set_populacao($notas);
	
	$est->frequencia_sem_intervalo_classe();
	
	// Parte de estat�stica
	$saida .= "<div class='info'>";	
	$saida .= "<fieldset><legend>Estat&iacute;stica</legend>";
	$saida .= "<p><span>M&eacute;dia da aritm&eacute;tica turma: </span>".round($est->get_media_aritmetica(),2)."</p>";
	$saida .= "<p><span>Desvio padr&atilde;o: </span>".round($est->get_desvio_padrao(),2)."</p>";
	$saida .= "<p><span>Vari&acirc;ncia: </span>".round($est->get_variancia(),2)."</p>";
	$saida .= "<p><span>Maior Nota: </span>".$est->get_maximo()."</p>";
	$saida .= "<p><span>Menor Nota: </span>".$est->get_minimo()."</p>";
	
	$nao_entregue = 0;
	$entregue = 0;
	$zerada = 0;
	
	foreach($alunos as $k) {
		switch ($k['status']) {
			case ZERADA: $zerada++; break;
			case ENTREGUE: $entregue++; break;
			case NAOENTREGUE: $nao_entregue++; break;
		}
	}
	
	$saida .= "<p><span>Quantidade de avalia&ccedil;&otilde;es n&atilde;o entregues: </span>".$nao_entregue." </p>";
	$saida .= "<p><span>Quantidade de avalia&ccedil;&otilde;es entregues: </span>".$entregue." </p>";
	$saida .= "<p><span>Quantidade de avalia&ccedil;&otilde;es zeradas: </span>".$zerada." </p>";
	
	$saida .= "<p><span>Aprovados: </span>".$est->condicional(70, '>=')."</p>";
	$saida .= "<p><span>Reprovados: </span>".$est->condicional(70, '<')."</p>";
	
	$questao_array = $libera->questao_vetor();
	$cancelada = 0;
	foreach($questao_array as $k){
		if ($k['cancelada'] == CANCELADA) $cancelada++;
	}
	
	$saida .= "<p><span>Quest&otilde;es Canceladas: </span>".$cancelada."</p>";
	$saida .= "<p><span>Nota canceladas: </span>".$libera->get_nota_cancelada()."</p>";
	
	// frequencia de notas dividos em classes
	$saida .= "<fieldset>";
	$saida .= "<legend>Classe de Notas (%)</legend>";		
	$est->set_classe(array(0,20,40,60,80,100));	
	$css = array('#990000','#FF0000','#000000','#6699FF','#3333FF');
	$est->ordena_frequencia();		

	$saida .= "<ul class='lista'>";
	$x = 0;
	foreach($est->get_frequencia_com_classe() as $k) {
		$saida .= "<li><span style='color:".$css[$x].";background-color:".$css[$x]."'>H</span> ".$k['li']." - ".$k['Li'].": ".$k['frequencia']." alunos</li>";
		$x++;
	}

	$saida .= "</ul>";	
	$saida .= "</fieldset>";
	// --------------- fim da divisao das notas ----------------------------	
	$saida .= "</fieldset>";
	$saida .= "</div>";
	// ---------------- fim das estatisticas -------------------------------
	$saida .= "<br />";
	
	// Lista de questoes da prova
	$saida .= "<div class='lista_aluno'>";
	$saida .= "<fieldset><legend>Lista de quest&otilde;es</legend>";
	$saida .= "<br />";
	$saida .= "<table width='95%' border='0' cellspacing='0' cellpadding='0'>";
	$saida .= "<tr class='Titulo'>";
	$saida .= "<td>N&ordm;</td>";
	$saida .= "<td width='50%'>Descri&ccedil;&atilde;o</td>";
	$saida .= "<td>Tipo</td>";
	$saida .= "<td>Peso(%)</td>";
	$saida .= "<td>Acertos (%)</td>";
	$saida .= "<td>Erros(%)</td>";
	$saida .= "<td>Meia(%)</td>";
	$saida .= "</tr>";

	$questao_array = $libera->questao_vetor();
	$classe = 'zebraA';

	$_UASORT_CAMPO = "tipo";	
	usort($questao_array, 'ordena'); //ordena o vetor pelo tipo	

	$respostas = relatorio_resposta($conn, $libera->get_id()); // vetor com todas as respostas dos alunos, media	
	$_UASORT_CAMPO = "tipo_questao";	
	usort($respostas, 'ordena'); //ordena o vetor pelo tipo
	
	$num = 1;
	foreach($questao_array as $questao) {
		$saida .= "<tr class='".$classe."'>";		
		// verifica o tipo da questao e busca a pergunta para ser colocada como descricao
		$q = new Questao($conn, $questao['questao']);
		switch ($questao['tipo']) {
			case DISSERTATIVA: {
				$tipo = A_LANG_AVS_LABEL_QUESTION_DISSERT;
				break;
			}
			case LACUNAS: {
				$tipo = A_LANG_AVS_LABEL_QUESTION_LACUNAS;
				break;
			}
			case MULTIPLA_ESCOLHA: {
				$tipo = A_LANG_AVS_LABEL_QUESTION_ME;
				break;
			}
			case VERDADEIRO_FALSO: {
				$tipo = A_LANG_AVS_LABEL_QUESTION_VF;
				break;
			}
		}			
		
		// buscar quantidade de alunos que acertaram e erraram a questao
		$acerto = resposta_filtro_questao_correta($respostas, $questao['questao']);		
		$incorreta = resposta_filtro_questao_incorreta($respostas, $questao['questao']);
		$meia = resposta_filtro_questao_meio($respostas, $questao['questao']);

		$saida .= "<td>".$num."</td>";
		$saida .= "<td><acronym title='".$q->get_pergunta()."'><a href='avaliacao/f_relatorio_pop.php?CodigoDisciplina=".$disciplina."&questao=".$questao['questao']."&libera=".$id_libera."&tipo=".$questao['tipo']."' target='novo'>".quote($q->get_pergunta(), 80)."</a></acronym></td>";
		$saida .= "<td>".$tipo."</td>";
		$saida .= "<td>".$questao['peso']."</td>";
		$saida .= "<td>".count($acerto)." (".round((count($acerto) / $libera->numero_alunos()), 2)."%)</td>";
		$saida .= "<td>".count($incorreta)." (".round((count($incorreta) / $libera->numero_alunos()), 2)."%)</td>";
		$saida .= "<td>".count($meia)." (".round((count($meia) / $libera->numero_alunos()), 2)."%)</td>";		
		$saida .= "</tr>";
		$num++;
		$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA' ;
	}
	$saida .= "</table>";
	$saida .= "</fieldset>�";
	$saida .= "</div>";
	// ------------------------ fim da lista de questoes ------------------------------------

	// Alunos
	$saida .= "<div class='lista_aluno'>";
	$saida .= "<fieldset><legend>Alunos</legend><br />";	
	$saida .= "<table width='95%' border='0' cellspacing='0' cellpadding='0'>";
	$saida .= "<tr class='Titulo'>";
	$saida .= "<td width='30%' rowspan='2'>Alunos</td>";
	$saida .= "<td colspan='3' style='text-align:center'>Notas</td>";
	$saida .= "<td rowspan='2'>In�cio</td>";	
	$saida .= "<td rowspan='2'>Entrega</td>";	
	$saida .= "<td rowspan='2'>Tempo</td>";
	$saida .= "<td rowspan='2'>Tempo/Quest&atilde;o</td>";
	$saida .= "</tr>";
	$saida .= "<tr class='Titulo'>";
	$saida .= "<td>Prova</td>";
	$saida .= "<td>Canceladas</td>";
	$saida .= "<td>Final</td>";
	$saida .= "</tr>";

	$alunos = $libera->alunos();
	$classe = 'zebraA';
	
	$_UASORT_CAMPO = "curso";	
	usort($alunos, 'ordena'); //ordena o vetor pelo curso
	
	$_UASORT_CAMPO = "id_usuario";	
	usort($respostas, 'ordena'); //ordena o vetor pelo curso
	
	foreach($alunos as $k) {	
		$segundos = strtotime($k['envio']) - strtotime($k['inicio']);
		$tempo_questao = $segundos / $libera->numero_questao();
		$saida .= "<tr class='".$classe."'>";
		$saida .= "<td><a href='javascript:visualizar_avaliacao(\"".$k['id']."\")'>".$k['nome']."</a></td>";
		$saida .= "<td>".$k['nota']."</td>";
		$saida .= "<td>".$libera->get_nota_cancelada()."</td>";
		$saida .= "<td>".($k['nota'] + $libera->get_nota_cancelada())."</td>";
		$saida .= "<td>".formata_data_hora($k['inicio'])."</td>";
		$saida .= "<td>".formata_data_hora($k['envio'])."</td>";
		$saida .= "<td>".date('H:i:s', mktime(0,0, $segundos))."</td>";
		$saida .= "<td>". date('H:i:s', mktime(0,0, $tempo_questao))."</td>";
		$saida .= "</tr>";
		$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA' ;
	}			
	$saida .= "</table>";
	$saida .= "</fieldset>";
	$saida .= "</div>";	
  
	// ------------------------ fim dos alunos -------------------------------------------  

	// ALUNOS X QUESTAO	
	$num_questao_tabela = 15;
	$num_tabelas = floor($libera->numero_questao() / $num_questao_tabela);
	
	$num_questao_restante = $libera->numero_questao() % $num_questao_tabela;
	
	$saida .= "<div class='lista_aluno'>";
	$saida .= "<fieldset><legend>Alunos x Quest&atilde;o</legend><br />";
	$num = 1;
	for($y =0; $y < $num_tabelas; $y++) {		
		$saida .= "<table width='95%' border='0' cellspacing='0' cellpadding='0'>";
		$saida .= "<tr class='Titulo'>";
		$saida .= "<td width='30%' rowspan='2'>Alunos</td>";
		$saida .= "<td colspan='".$num_questao_tabela."' style='text-align:center'>Quest&otilde;es</td>";
		$saida .= "</tr>";

		$saida .= "<tr class='Titulo'>";		
		for($x = 0; $x < $num_questao_tabela; $x++)	{
			$saida .= "<td>".$num."</td>";
			$num++;
		}		
		$saida .= "</tr>";	

		$alunos = $libera->alunos();
		$classe = 'zebraA';
		
		$_UASORT_CAMPO = "curso";	
		usort($alunos, 'ordena'); //ordena o vetor pelo curso
		
		$_UASORT_CAMPO = "id_usuario";	
		usort($respostas, 'ordena'); //ordena o vetor pelo curso
		
		foreach($alunos as $k) {	
			$saida .= "<tr class='".$classe."'>";
			$saida .= "<td><a href='javascript:visualizar_avaliacao(\"".$k['id']."\")'>".$k['nome']."</a></td>";
			$questao_aluno = relatorio_filtro_aluno_questao($respostas, $k['id']);			

			$_UASORT_CAMPO = "tipo_questao";	
			usort($questao_aluno, 'ordena'); //ordena o vetor pelo tipo_questao	
			
			for($x=0; $x < ($y * $num_questao_tabela); $x++);  // coloca o cursor do vetor no lugar para impressao		
	
			if(count($questao_aluno) > 0) {				
				for($x; $x < $num_questao_tabela + ($y*$num_questao_tabela); $x++) {
					switch($questao_aluno[$x]['status_questao']) {
						case CORRETA: $saida .= "<td>C</td>"; break;
						case ERRADA: $saida .= "<td>E</td>";break;
						case MEIO: $saida .= "<td>M</td>"; break;
						default: $saida .= "<td></td>"; break;
					}
				}
			}
			$saida .= "</tr>";
			$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA' ;
		}
		$saida .= "</table><br />";
	}
	// para as questoes restantes
	if($num_questao_restante > 0) {
		$saida .= "<table width='95%' border='0' cellspacing='0' cellpadding='0'>";
		$saida .= "<tr class='Titulo'>";
		$saida .= "<td width='30%' rowspan='2'>Alunos</td>";
		$saida .= "<td colspan='".$num_questao_restante."' style='text-align:center'>Quest&otilde;es</td>";
		$saida .= "</tr>";

		$saida .= "<tr class='Titulo'>";		
		for($x = 0; $x < $num_questao_restante; $x++)	{
			$saida .= "<td>".$num."</td>";
			$num++;
		}		
		$saida .= "</tr>";	

		$alunos = $libera->alunos();
		$classe = 'zebraA';
		
		$_UASORT_CAMPO = "curso";	
		usort($alunos, 'ordena'); //ordena o vetor pelo curso
		
		$_UASORT_CAMPO = "id_usuario";	
		usort($respostas, 'ordena'); //ordena o vetor pelo curso
		
		foreach($alunos as $k) {	
			$saida .= "<tr class='".$classe."'>";
			$saida .= "<td><a href='javascript:visualizar_avaliacao(\"".$k['id']."\")'>".$k['nome']."</a></td>";
			$questao_aluno = relatorio_filtro_aluno_questao($respostas, $k['id']);			

			$_UASORT_CAMPO = "tipo_questao";	
			usort($questao_aluno, 'ordena'); //ordena o vetor pelo curso						
			
			$x = ($num_tabelas * $num_questao_tabela);		
			
			for($x; $x < count($questao_aluno); $x++) {
				switch($questao_aluno[$x]['status_questao']) {
					case CORRETA: $saida .= "<td>C</td>"; break;
					case ERRADA: $saida .= "<td>E</td>";break;
					case MEIO: $saida .= "<td>M</td>"; break;
					default: $saida .= "<td></td>"; break;
				}
			}
			$saida .= "</tr>";
			$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA' ;
		}
		$saida .= "</table><br />";
	}	
	
	$saida .= "</fieldset>";
	$saida .= "</div>";		

	// fim da tabela de alunos x questoes
		
	$saida .= "<br />";	
	$saida .= "</div>";
	echo utf8_encode($saida);
?>

