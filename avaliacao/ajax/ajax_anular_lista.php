<?php
 /**
 * Script que lista as quest�es de uma avalia��o realizada para que as quest�es possam ser anuladas
 * A partir desta lista o professor pode escolher qual quest�o ser� anulada
 * Exemplo de URL para a chamada da p�gina:
 * <code>
 *   avaliacao/ajax/ajax_anular_lista.php?avaliacao=$id_avaliacao
 * </code>   
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Ajax
 * @subpackage Anular_Questao
 * @filesource 
 * @version 1.0 05/01/2008
 */
 
/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

	@session_start();	
  require_once "../../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica��o de seguran�a
  if(!isset($_SESSION['logado'])) die(utf8_encode(A_LANG_AVS_ERROR_LOGGED));
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die(utf8_encode("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>"));
 
  require_once "../funcao/ajuste.func.php";
  require_once "../../include/adodb/adodb.inc.php";
	require_once "../class/liberar.class.php";
	require_once "../questao.const.php";
	require_once "../funcao/ajuste.func.php";
	require_once "../funcao/geral.func.php";
	require_once "../class/Questao.class.php";
	require_once "../class/Multipla_Escolha.class.php";
	require_once "../class/Lacunas.class.php";
	require_once "../class/Verdadeiro_Falso.class.php";
	require_once "../class/Dissertativa.class.php";

	/**
	 * ID da avalia��o que � passado via GET (URL)
	 * @global integer $_GET['avaliacao']
	 * @name $avaliacao
	 */ 		
	$id_libera = isset($_GET['avaliacao']) ? $_GET['avaliacao'] : FALSE;
	
  $disciplina = isset($_GET['disciplina']) ? $_GET['disciplina']: FALSE;

  if(!$id_libera || !$disciplina) die(utf8_encode(A_LANG_AVS_ERROR_PARAM));

  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $disciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));
	
	$libera = new Liberar($conn, $id_libera);
	$saida = "";
	if($libera->numero_questao() > 0 ) {	
		$classe = "zebraA";	
		$saida .= "<div class='lista_aval'>";		
		// info da avalia�ao
		$saida .= "<div class='info'>";
		$saida .= "<p><span>".A_LANG_AVS_TABLE_USER_EVALUATION.": </span>".$libera->get_descricao()."</p>"; 
		$saida .= "<p><span>".A_LANG_AVS_TABLE_DATE_START.": </span>".formata_data_hora($libera->get_data_inicio())."</p>"; 
		$saida .= "<p><span>".A_LANG_AVS_TABLE_DATE_FINISH.": </span>".formata_data_hora($libera->get_data_final())."</p>"; 
		$saida .= "<p><span>".A_LANG_AVS_TABLE_VALUE_CANCELLED." (%): </span>".$libera->get_nota_cancelada()."</p>"; 
		$saida .= "</div>";
		$saida .= "<div class='lista_aluno'>";
		$saida .= "<fieldset><legend>".A_LANG_AVS_LABEL_QUESTION."</legend>";
		$saida .= "<table width='95%' border='0' cellspacing='0' cellpadding='0'>\n";
		$saida .= "  <tr class='Titulo' align='center'>\n";
		$saida .= "    <td width='60%'>".A_LANG_AVS_LABEL_DESCRIPTION."</td>\n";
		$saida .= "    <td>".A_LANG_AVS_TABLE_TYPE."</td>\n";
		$saida .= "    <td>".A_LANG_AVS_TABLE_WEIGHT."</td>\n";
		$saida .= "    <td>".A_LANG_AVS_TABLE_AJAX_QUESTION_CANCEL."</td>\n";
		$saida .= "    <td>".A_LANG_AVS_TABLE_AJAX_QUESTION_UNDO."</td>\n";
		$saida .= "  </tr>\n";
		
		while($array = $libera->buscar_questao()) {
			// verifica o tipo da questao e busca a pergunta para ser colocada como descricao
			switch ($array['tipo']) {
				case DISSERTATIVA: {
					$tipo = A_LANG_AVS_LABEL_QUESTION_DISSERT;
					$q = new Dissertativa($conn, $array['questao']);
					$descricao = $q->get_pergunta();
					break;
				}
				case LACUNAS: {
					$tipo = A_LANG_AVS_LABEL_QUESTION_LACUNAS;
					$q = new Lacunas($conn, $array['questao']);
					$lac = $q->get_pergunta();
					$descricao = $lac['pergunta'];
					break;
					}
				case MULTIPLA_ESCOLHA: {
					$tipo = A_LANG_AVS_LABEL_QUESTION_ME;
					$q = new Multipla_Escolha($conn, $array['questao']);
					$descricao = $q->get_pergunta();
					break;
				}
				case VERDADEIRO_FALSO: {
					$tipo = A_LANG_AVS_LABEL_QUESTION_VF;
					$q = new Verdadeiro_Falso($conn, $array['questao']);
					$descricao = $q->get_pergunta();
					break;
				}
			}
			$saida .= "  <tr align='center' class='".$classe."'>\n";
			$saida .= "    <td align='left'>".$descricao."</td>\n";
			$saida .= "    <td>".$tipo."</td>\n";
			$saida .= "    <td>".$array['peso']."</td>\n";
			if($array['cancelada'] == CANCELADA) { 
				$saida .= "<td> -- </td>\n";
				$saida .= "<td><input type='checkbox' value='".$array['questao']."' name='desfazer[]' /></td>\n";
			} else {
				$saida .= "<td><input type='checkbox' value='".$array['questao']."' name='cancelar[]' /></td>";
				$saida .= "<td> -- </td>\n";				
			}				
			$saida .= "  </tr>\n";
			$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA';						
		}
		$saida .= "<tr><td><br/><input type='submit' name='enviar' value='".A_LANG_AVS_BUTTON_SAVE."' class='button' /></td></tr>";	
		$saida .= "</table>\n";		
		$saida .= "</fieldset>";
		$saida .= "</div>";	
		$saida .= "</div>";			
	}	else {
		$saida = A_LANG_AVS_ERROR_EVALUATION_LIBERATE;
	}

	echo utf8_encode($saida);
?>

