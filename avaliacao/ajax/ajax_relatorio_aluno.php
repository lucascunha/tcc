<?php
 /**
 * Script que lista as quest�es de uma avalia��o realizada para que as quest�es possam ser anuladas
 * A partir desta lista o professor pode escolher qual quest�o ser� anulada
 * Exemplo de URL para a chamada da p�gina:
 * <code>
 *   avaliacao/ajax/ajax_anular_lista.php?avaliacao=$id_avaliacao
 * </code>   
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Ajax
 * @subpackage Anular_Questao
 * @filesource 
 * @version 1.0 05/01/2008
 */
 
/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

	@session_start();	
  require_once "../../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica��o de seguran�a
  if(!isset($_SESSION['logado'])) die(utf8_encode(A_LANG_AVS_ERROR_LOGGED));
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die(utf8_encode("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>"));
 
  require_once "../funcao/ajuste.func.php";
  require_once "../../include/adodb/adodb.inc.php";
	require_once "../questao.const.php";
	require_once "../funcao/ajuste.func.php";
	require_once "../funcao/geral.func.php";
	require_once "../class/liberar.class.php";
	require_once "../class/resposta.class.php";
	require_once "../class/questao.class.php";
	require_once "../class/multipla_escolha.class.php";
	require_once "../class/lacunas.class.php";
	require_once "../class/verdadeiro_falso.class.php";
	require_once "../class/dissertativa.class.php";
	require_once "../class/estatistica.class.php";
	require_once "../funcao/relatorio.func.php";

	/**
	 * ID da avalia��o que � passado via GET (URL)
	 * @global integer $_GET['avaliacao']
	 * @name $avaliacao
	 */ 		
	$aluno = isset($_GET['aluno']) ? $_GET['aluno'] : FALSE;
	
	$curso = isset($_GET['curso']) ? $_GET['curso'] : FALSE;
	
  $disciplina = isset($_GET['disciplina']) ? $_GET['disciplina']: FALSE;

  if(!$curso || !$disciplina || !$aluno) die(utf8_encode(A_LANG_AVS_ERROR_PARAM));

  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $disciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));
	
	$vetor =  relatorio_aluno($conn, $aluno, $disciplina, $curso);
	$saida = "";
	$situacao = $vetor[0]['vl_media_aval']+$vetor[0]['vl_arred'] > 70 ? "Aprovado" : "Reprovado";
	$saida .= "<div class='lista_aval'>";
	$saida .= "<div class='info'>";
	$saida .= "<p><span>Aluno:</span> ".$vetor[0]['nome_usuario']." </p>";
	$saida .= "<p><span>Curso:</span> ".$vetor[0]['nome_curso'] ."</p>";
	$saida .= "<p><span>Situa&ccedil;&atilde;o:</span> ".$situacao."</p> <br />";
	$saida .= "</div>";
	
	$saida .= "<div class='lista_aluno'>";	
	$saida .= "<fieldset><legend>Avalia&ccedil;&otilde;es</legend>";
	$saida .= "<table width='95%' border='0' cellspacing='0' cellpadding='0'>";
	$saida .= "  <tr class='Titulo'>";
	$saida .= "    <td rowspan='2'>Avalia&ccedil;&otilde;es</td>";
	$saida .= "    <td colspan='3' width='30%'>Notas</td>";
	$saida .= "    <td rowspan='2' width='10%'>Peso</td>";
	$saida .= "    <td rowspan='2' width='10%'>M&eacute;dia</td>";
	$saida .= "  </tr>";
	$saida .= "  <tr class='Titulo'>";
	$saida .= "    <td width='10%'>Prova</td>";
	$saida .= "    <td width='10%'>Cancelada</td>";
	$saida .= "    <td width='10%'>Final</td>";
	$saida .= "  </tr>";
	$classe = 'zebraA';	

	foreach($vetor as $k) {
		$saida .= "<tr class='".$classe."'>";
		if($k['bl_presencial'] == 1)
			$saida .= "<td>".$k['ds_avaliacao']." - Presencial</td>";
		else
			$saida .= "<td><a href='javascript:visualizar_avaliacao(".$k['id_libera_aluno'].")' >".$k['ds_avaliacao']."</a></td>";
		$saida .= "<td>".$k['vl_nota']."</td>";
		$saida .= "<td>".$k['vl_nota_cancelada']."</td>";
		$saida .= "<td>".($k['vl_nota_cancelada'] + $k['vl_nota'])."</td>";
		$saida .= "<td>".$k['vl_peso']."</td>";
		$media = ($k['vl_nota_cancelada'] + $k['vl_nota']) * $k['vl_peso'] / 100;
		$saida .= "<td>".$media."</td>";
		$saida .= "</tr>";
		$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA' ;				
	}
	
	$saida .= "  <tr class='total'>";
	$saida .= "    <td colspan='5'><div align='right'>Total</div></td>";
	$saida .= "    <td><div style='padding-left:5px'>".$vetor[0]['vl_media_aval']."</div></td>";
	$saida .= "  </tr>";
	$saida .= "  <tr class='total'>";
	$saida .= "    <td colspan='5'><div align='right'>Nota Participativa </div></td>";
	$saida .= "    <td><div style='padding-left:5px'> ".$vetor[0]['vl_arred']."</div></td>";
	$saida .= "  </tr>";
	if(strcasecmp($situacao,'Reprovado') == 0)
		$saida .= "  <tr class='total_reprovado'>";
	else
		$saida .= "  <tr class='total_aprovado'>";
	$saida .= "    <td colspan='5'><div align='right'>M&eacute;dia Final </div></td>";
	$saida .= "    <td><div style='padding-left:5px'>".($vetor[0]['vl_arred'] + $vetor[0]['vl_media_aval'])."</div></td>";
	$saida .= "  </tr>";
	$saida .= "</table>";
	$saida .= "</fieldset>";
	$saida .= "</div>";
	$saida .= "</div>";

	echo utf8_encode($saida);
?>

