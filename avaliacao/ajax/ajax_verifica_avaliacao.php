<?php
/**
 * Verifica se o aluno est� realizando uma avalia��o
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <14/02/2008>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

/**
 * Script que listar� todos os alunos de um curso e disciplina para realizar o ajuste de m�dia
 * Exemplo de URL para a chamada da p�gina:
 * <code>
 *   avaliacao/ajax/ajax_ajuste_media.php?disciplina=1&curso=2
 * </code>   
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Ajax
 * @subpackage Ajuste_Media
 * @version 1.0 05/01/2008
 */
 
/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	


	@session_start();	
  require_once "../../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica��o de seguran�a
  if(!isset($_SESSION['logado'])) die(utf8_encode(A_LANG_AVS_ERROR_LOGGED));
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die(utf8_encode("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>"));
 
  require_once "../../include/adodb/adodb.inc.php";
	require_once "../questao.const.php";
	require_once "../funcao/geral.func.php";
	require_once "../funcao/aluno.func.php";


	$aluno = $_SESSION['id_usuario'];
  $disciplina = isset($_GET['disciplina']) ? $_GET['disciplina'] : FALSE;
	
	if (!$aluno || !$disciplina ) {
		die(1); // retorna como havendo avalia��o aberta
	}
	
	$conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	
	$retorno = verifica_avaliacao_liberada($conn, $aluno, $disciplina);
	
	die($retorno); // retorna 0 ou 1

?>
