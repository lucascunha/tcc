<?php
/**
 * Script que lista as notas de um aluno para uma determinada disciplina e curso
 * Exemplo de URL para a chamada da p�gina:
 * <code>
 *   avaliacao/ajax/ajax_ajuste_media_nota.php?aluno=$id_aluno&disciplina=$CodigoDisciplina&curso=$curso
 * </code>   
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Ajax
 * @subpackage Ajuste
 * @filesource
 * @version 1.0 06/01/2008
 */
 
/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

	@session_start();	
  require_once "../../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica��o de seguran�a
  if(!isset($_SESSION['logado'])) die(utf8_encode(A_LANG_AVS_ERROR_LOGGED));
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die(utf8_encode("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>"));

  require_once "../funcao/ajuste.func.php";
  require_once "../../include/adodb/adodb.inc.php";
  require_once "../../config/configuracoes.php";
	require_once "../class/liberar.class.php";
	require_once "../funcao/geral.func.php";

	/**
	 * ID do curso que � passado via GET (URL)
	 * @global integer $_GET['curso']
	 * @name $curso
	 */ 	
	$curso = isset($_GET['curso']) ? $_GET['curso'] : FALSE;

	/**
	 * ID da disciplina que � passado via GET (URL)
	 * @global integer $_GET['disciplina']
	 * @name $disciplina
	 */
	$disciplina = isset($_GET['disciplina']) ? $_GET['disciplina'] : FALSE;

	/**
	 * ID do aluno que � passado via GET (URL)
	 * @global integer $_GET['aluno']
	 * @name $aluno
	 */
	$aluno = isset($_GET['aluno']) ? $_GET['aluno'] : FALSE;

	if(!$curso || !$disciplina || !$aluno) die(utf8_encode(A_LANG_AVS_ERROR_PARAM));

  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $disciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));
	
	$vet_aluno = ajuste_media_listar_nota_aluno($conn, $aluno, $curso, $disciplina);
	
	if(count($vet_aluno) > 0 ) {	
		$classe = "zA";	
		$saida = "<div class='aval'>";
		$saida .= "<table width='90%' border='0' cellspacing='0' cellpadding='2'>\n";
		$saida .= "<tr class='Titulo'>";
		$saida .= "<td width='70%'>Avalia&ccedil;&atilde;o</td>";
		$saida .= "<td align='right'>Nota</td>";
		$saida .= "</tr>";
		foreach($vet_aluno as $aluno) {
			$saida .= "	<tr class='".$classe."'>\n";
			$saida .= "		<td>".$aluno['avaliacao']."</td>\n";
			$saida .= "   <td align='right'>".formata_nota(($aluno['soma']))."</td>\n";
			$saida .= " </tr>";
			$classe = $classe == 'zA' ? 'zB' : 'zA';
		}
		$saida .= "		</table>\n";
		$saida .= "</div>";
	}	else {
		$saida = A_LANG_AVS_ERROR_AJAX_AVERAGE1;
	}

	echo utf8_encode($saida);
?>
