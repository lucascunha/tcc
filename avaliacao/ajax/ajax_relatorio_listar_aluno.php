<?php
/**
 * Script que lista as avalia�oes de um t�pico para ser liberada para resolu�ao
 * Exemplo de URL para a chamada da p�gina:
 * <code>
 *   avaliacao/ajax/ajax_liberar_aval.php?disciplina=$CodigoDisciplina&grupo=$id_grupo
 * </code>   
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Ajax
 * @subpackage Liberar
 * @filesource
 * @version 1.0 16/12/2007
 */
 
/*
 * Trabalho de Conclusao de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

	@session_start();	
  require_once "../../config/configuracoes.php";	
  // defini�ao do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica�ao de seguran�a
  if(!isset($_SESSION['logado'])) die(utf8_encode(A_LANG_AVS_ERROR_LOGGED));
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die(utf8_encode("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>"));

  require_once "../funcao/geral.func.php";
  require_once "../questao.const.php";
  require_once "../funcao/media.func.php";
  require_once "../../include/adodb/adodb.inc.php";
  require_once "../funcao/relatorio.func.php";


	/**
	 * ID da disciplina que � passado via GET (URL)
	 * @global integer $_GET['disciplina']
	 * @name $disciplina
	 */ 	
  $disciplina = isset($_GET['disciplina']) ? $_GET['disciplina']: FALSE;

	/**
	 * ID do grupo que � passado via GET (URL)
	 * @global integer $_GET['grupo']
	 * @name $grupo
	 */ 	
  $curso = isset($_GET['curso']) ? $_GET['curso']: FALSE;

  if(!$curso || !$disciplina) die(utf8_encode(A_LANG_AVS_ERROR_PARAM));

  header("Content-type: text/xml; charset=ISO-8859-1");
  echo "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";

  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $disciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));

  $aluno = relatorio_lista_aluno($conn, $disciplina, $curso);

  echo "<registro>\n";

  foreach ($aluno as $k) {
			printf ("<nome id=\"%d\">%s</nome>\n", $k['id'], $k['nome']);
  }

  echo "</registro>\n";

?>
