<?php
/**
 * Seleciona os alunos que ter�o a avalia��o liberada para resolu��o
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @version 1.0 <24/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

	@session_start();	
  require_once "../../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica��o de seguran�a
  if(!isset($_SESSION['logado'])) die(utf8_encode(A_LANG_AVS_ERROR_LOGGED));
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die(utf8_encode("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>"));

  $id_disc = isset($_GET['disciplina']) ? $_GET['disciplina'] : FALSE;
  $id_libera = isset($_REQUEST['libera']) ? $_REQUEST['libera'] : FALSE;
	
	if(!$id_libera || !$id_disc ) die(utf8_encode(A_LANG_AVS_ERROR_PARAM));
	
  require_once "../funcao/geral.func.php";
	require_once "../funcao/liberar.func.php";
  require_once "../class/avaliacao.class.php";
  require_once "../class/grupo.class.php";
  require_once "../class/questao.class.php";
  require_once "../questao.const.php";
	require_once "../class/liberar.class.php";
	require_once "../class/erro.class.php";
	require_once "../class/resposta.class.php";
  require_once "../../include/adodb/adodb.inc.php";
  require_once "../../config/configuracoes.php";

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $disciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));
		
	$erro = new Erro();

	$libera = new Liberar($conn, $id_libera);
	$array_aluno = $libera->alunos();	
	
	$_UASORT_CAMPO = "curso";
	
	usort($array_aluno, 'ordena'); //ordena o vetor pelo curso
	
	$saida = "";
	$saida .= "<div class='lista_aval'>";	
	if($libera->get_divulgacao()) {
		$saida .= "<form action='a_index.php?opcao=FuncaoAjustarPresencialGrupo&CodigoDisciplina=".$id_disc."&libera=".$id_libera."' method='post' name='form_alunos' >";	
	}else{
		$saida .= "<form action='a_index.php?opcao=FuncaoAjustarPresencialGrupo&CodigoDisciplina=".$id_disc."&libera=".$id_libera."' method='post' name='form_alunos' onsubmit='return divulgacao()' >";
	}
	$saida .= "<input type='hidden' value='".$libera->get_id()."' name='libera' />";
	$saida .= "<input type='hidden' value='0' name='divulgar' id='divulgar' />";
	$saida .= "<div class='info'>";
	$saida .= "<p><span>".A_LANG_AVS_TABLE_USER_EVALUATION.": </span>".$libera->get_descricao()."</p>"; 
	$saida .= "<p><span>".A_LANG_AVS_TABLE_DATE_START.": </span>".formata_data_hora($libera->get_data_inicio())."</p>"; 
	$saida .= "<p><span>".A_LANG_AVS_TABLE_DATE_FINISH.": </span>".formata_data_hora($libera->get_data_final())."</p>"; 
	$saida .= "</div>";	
	$curso_atual = NULL;
	$tabindex = 1;
	$classe = "zebraA";
	foreach ($array_aluno as $k) {					
		if($curso_atual != $k['curso']) {
			if(is_null($curso_atual)) {
				$saida .= "<br />";
				$saida .= "<div class='lista_aluno'>";
				$saida .= "<fieldset><legend>".$k['nome_curso']."</legend>";
				$saida .= "<table width='95%' border='0' cellspacing='0' cellpadding='0'>";
				$saida .= "<tr class='Titulo'>";
				$saida .= "<td width='85%'>".A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT."</td>";
				$saida .= "<td width='15%'>".A_LANG_AVS_TABLE_WEIGHT."</td>";
				$saida .= "</tr>";							
			} else {
				$saida .= "</table>";
				$saida .= "</fieldset>";
				$saida .= "</div><br />";
				$saida .= "<div class='lista_aluno'>";
				$saida .= "<fieldset><legend>".$k['curso']."</legend>";
				$saida .= "<table width='95%' border='0' cellspacing='0' cellpadding='0'>";
				$saida .= "<tr class='Titulo'>";
				$saida .= "<td width='85%'>".A_LANG_AVS_TABLE_ADJUSTMENT_DISSERT_STUDENT."</td>";
				$saida .= "<td width='15%'>".A_LANG_AVS_TABLE_WEIGHT."</td>";
				$saida .= "</tr>";							
			}
			$classe = "zebraA";
			$curso_atual = $k['curso'];
		}							
		$saida .=  "<tr class='".$classe."'>"; 
		$saida .=  "  <td width='80%'>".$k['nome']."</td>";
		$saida .=  "  <td width='20%'><input type='text' name='aluno[".$k['id']."]' value='".$k['nota']."' class='button' onKeyDown='return numero(event)' onKeyUp='verifica(this)' maxlenght='3' tabindex='".$tabindex."' size='5' /></td>";
		$saida .=  "</tr>";
		$tabindex++;
		if(!$curso_atual) $curso_atual = $k['curso'];
		$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA';
	}
	$saida .= "</table>";	
	$saida .= "</fieldset>";
	$saida .= "</div>";
	$saida .= "<div class='funcao'>";			
	$saida .= "<input name='enviar_nota' value='".A_LANG_AVS_BUTTON_SAVE."' type='Submit' class='button'>";	
	$saida .= "</div>";
	$saida .= "</form>";
	
	echo utf8_encode($saida);

?>