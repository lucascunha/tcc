<?php
/**
 * Script que listar� todos os alunos de um curso e disciplina para realizar o ajuste de m�dia
 * Exemplo de URL para a chamada da p�gina:
 * <code>
 *   avaliacao/ajax/ajax_ajuste_media.php?disciplina=$CodigoDisciplina&curso=$curso
 * </code>    
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Ajax
 * @subpackage Ajuste
 * @filesource
 * @version 1.0 05/01/2008
 */
 
/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

	@session_start();	
  require_once "../../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../../idioma/".$A_LANG_IDIOMA_USER."/geral.php";
  // verifica��o de seguran�a
  if(!isset($_SESSION['logado'])) die(utf8_encode(A_LANG_AVS_ERROR_LOGGED));
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die(utf8_encode("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>"));

  require_once "../funcao/ajuste.func.php";
  require_once "../../include/adodb/adodb.inc.php";
  require_once "../../config/configuracoes.php";
	require_once "../class/liberar.class.php";
	require_once "../funcao/geral.func.php";

	/**
	 * ID do curso que � passado via GET (URL)
	 * @global integer $_GET['curso']
	 * @name $curso
	 */ 
	$id_curso =  isset($_GET['curso']) ? $_GET['curso'] : FALSE;

	/**
	 * ID da disciplina que � passado via GET (URL)
	 * @global integer $_GET['disciplina']
	 * @name $disciplina
	 */ 
	$id_disciplina = isset($_GET['disciplina']) ? $_GET['disciplina'] : FALSE;
	
  if(!$id_curso || !$id_disciplina) die(utf8_encode(A_LANG_AVS_ERROR_PARAM));	

  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	
	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $disciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));	
	
	$vet_aluno = ajuste_media_listar_aluno($conn, $id_curso, $id_disciplina);	
	
	/**
	 * Vari�vel que retorna toda a string HTML contendo as informa��es do script
 	 * @global string $saida
	 */
	$saida = "";	
	
	if(count($vet_aluno) > 0 ) {		
		$classe = "zebraA";	
		$saida .= "<form action='a_index.php?opcao=FuncaoAjustarMedia&CodigoDisciplina=".$id_disciplina."&curso=".$id_curso."' method='post' name='frm_media'>\n";
		$saida .= "	<table width='95%' border='0' cellspacing='0' cellpadding='0'>\n";
		$saida .= "	<tr class='Titulo'>\n";
		$saida .= "		<td width='50%'>".A_LANG_AVS_TABLE_AJAX_AVERAGE."</td>\n";
		$saida .= "   <td width='15%' align='center'>".A_LANG_AVS_TABLE_AJAX_AVERAGE1." (%)</td>\n";
		$saida .= "   <td width='15%' align='center'>".A_LANG_AVS_TABLE_AJAX_AVERAGE2." (%)</td>\n";
		$saida .= "   <td width='15%' align='center'>".A_LANG_AVS_TABLE_AJAX_AVERAGE3." (%)</td>\n";
		$saida .= "   <td width='20%' align='center'></td>\n";
		$saida .= " </tr>\n";
		foreach($vet_aluno as $aluno) {
			$saida .= "	<tr class='".$classe."'>\n";
			$saida .= "		<td>".$aluno['aluno']."</td>\n";
			$media = !empty($aluno['media']) ? $aluno['media'] : 0;
			$arredonda = !empty($aluno['arredonda']) ? $aluno['arredonda'] : 0;
			$calculada = $media + $arredonda;
			$saida .= "   <td align='center'>".$media."</td>\n";
			$saida .= "   <td align='center'><input type='text' value='".$arredonda."' class='button' name='media[".$aluno['id']."]' id='media_".$aluno['id']."'  size='5' onkeyup='calcular_media(".$media.",this, \"calcula_".$aluno['id']."\")' onKeyDown='return numero(event)' ></td>\n";
			$saida .= "   <td align='center'><div id='calcula_".$aluno['id']."'>".$calculada."</div></td>\n";			
			$saida .= "   <td><input type='button' class='button' value='".A_LANG_AVS_BUTTON_AJAX_AVERAGE_LIST."' onclick='listar_notas(".$aluno['id'].",".$id_curso.",\"nota_".$aluno['id']."\",\"t_".$aluno['id']."\")'></td>\n";			
			$saida .= " </tr>\n";
			$saida .= " <tr class='".$classe."' ><td colspan='5'><div id='nota_".$aluno['id']."' align='right'></div></td></tr>\n";
			$classe = $classe == 'zebraA' ? 'zebraB' : 'zebraA';
		}
		$saida .= "		<tr><td><br><input type='submit' value='".A_LANG_AVS_BUTTON_SAVE."' name='enviar_media' class='button'></td></tr>\n";
		$saida .= "	</table>\n";
		$saida .= "</form>\n";
	}	else {
		$saida = A_LANG_AVS_ERROR_AJAX_AVERAGE;
	}

	echo utf8_encode($saida);
?>
