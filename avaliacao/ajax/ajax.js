function AJAX() {

  var xmlhttp = getXmlHttp();

  this.Texto=carregarDados;
  this.Combo=carregaCombo;
	this.TextoSemRetorno=carregarDadosSemRetorno;
	this.Alerta=fechaJanela;

  function fechaJanela(url,idResposta) {
    //Abre a url
    xmlhttp.open("GET", url, true);
    //Executada quando o navegador obtiver o c�digo
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
        //L� o texto
    	  var texto=xmlhttp.responseText;
				var liberada = parseInt(texto);				
      	if(liberada == 1) {
					idResposta.innerHTML = "Ambiente trancado!";
					alert("H� uma avalia��o pendente que vo�e deve responder");					
				}
      }
    }
    xmlhttp.send(null);
		
	}
	
	/*
   * Fun��o que completa o combobox
   * @param idResposta Objeto que corresponde onde ser� colocado o dados
   */

  function carregaCombo(url,idResposta) {
    //Abre a url
    xmlhttp.open("GET", url, true);
    //Executada quando o navegador obtiver o c�digo
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
        var result = xmlhttp.responseXML; // busca o arquivo xml retornado
        var no = result.getElementsByTagName("nome"); // procura a tag
        idResposta.innerHTML = "";
        for (var i = 0; i < no.length; i++) {
          novo = criar_opcao(no[i]);
          idResposta.appendChild(novo);
        }
      }
    }
    xmlhttp.send(null);
  }

  function criar_opcao(no) {
    var nova_opcao = document.createElement("option");
    var texto = document.createTextNode(no.childNodes[0].data);
    nova_opcao.setAttribute("value",no.getAttribute("id"));
    nova_opcao.appendChild(texto); //Adiciona o texto a OPTION.
    return nova_opcao; // Retorna a nova OPTION.
  }

  /*
   * @param idResposta Objeto que corresponde onde ser� colocado o dados
   */
  function carregarDados(url,idResposta) {
    //Abre a url
    xmlhttp.open("GET", url,true);
    //Executada quando o navegador obtiver o c�digo
    xmlhttp.onreadystatechange=function() {
      if (xmlhttp.readyState==4 && xmlhttp.status == 200){
  	    //L� o texto
    	  var texto=xmlhttp.responseText;
      	//Desfaz o urlencode
				texto=texto.replace(/\+/g," ");
  	    texto=unescape(texto);
	      //Exibe o texto no div conte�do
 		    idResposta.innerHTML=texto;
      }
    }
    xmlhttp.send(null);
  }
	
	function carregarDadosSemRetorno(url, funcao) {
    //Abre a url
    xmlhttp.open("GET", url,true);
    //Executada quando o navegador obtiver o c�digo
    xmlhttp.onreadystatechange=function() {
      if (xmlhttp.readyState==4 && xmlhttp.status == 200){
      	//L� o texto
	      var texto=xmlhttp.responseText;				
				
				eval(funcao);
				
			}
    }
    xmlhttp.send(null);		
	}

  /**
   * Fun��o que define o objeto XMLHttpRequest
   */

  function getXmlHttp() {
    var xmlhttp;
    try{
      xmlhttp = new XMLHttpRequest();
    }catch(ee){
      try{
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
      }catch(e){
        try{
          xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }catch(E){
          xmlhttp = false;
        }
      }
    }
    return xmlhttp;
  }
}
