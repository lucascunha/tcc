<?php
/**
 * Script para inser��o/altera��o de quest�es do tipo m�ltipla-escolha
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Scripts
 * @subpackage Verdadeiro_Falso
 * @version 1.0 21/10/2007
 * @since 08/04/2008 Modifica��o de cores e estrutura no CSS. Utiliza��o da classe Erro.
 * @since 09/04/2008 Retirado o bug de duplicar quest�es na avalia��o
 * @since 09/04/2008 Retirado o bug de criar duas quest�es com IDs diferentes a cada salvar
 */

/*
 * Trabalho de Conclus�o de Curso Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski	
 */	

  @session_start();
  require_once "../config/configuracoes.php";	
  // defini��o do idioma
	if (!empty($A_LANG_IDIOMA_USER))
		include "../idioma/".$A_LANG_IDIOMA."/geral.php";
	else
		include "../idioma/".$A_LANG_IDIOMA_USER."/geral.php";	
  // verifica se a pessoa est� logada
  if(!isset($_SESSION['logado'])) die(A_LANG_AVS_ERROR_QUESTION_LOGGED);
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die("<font color='red'>".A_LANG_AVS_ERROR_AUTHO."</font>");

  require_once "questao.const.php";
  require_once "../include/adodb/adodb.inc.php";
  require_once "../config/configuracoes.php";
  require_once "class/questao.class.php";
  require_once "class/multipla_escolha.class.php";
  require_once "class/avaliacao.class.php";
	require_once "class/erro.class.php";
	require_once "funcao/geral.func.php";
	require_once "ajuda.const.php";	

  // pega as variaveis externas

  // id da questao, se for alterar
  $id_questao = isset($_REQUEST['questao']) ? $_REQUEST['questao'] : NULL;
  // id da avaliacao
  $id_aval = isset($_REQUEST['aval']) ? $_REQUEST['aval'] : FALSE;
    // codigo da disciplina
  $CodigoDisciplina = isset($_REQUEST['CodigoDisciplina']) ? $_REQUEST['CodigoDisciplina'] : FALSE;

  // se n�o for enviado nenhuma avalia��o o script ser� encerrado
  if(!($id_aval || $CodigoDisciplina)) die(A_LANG_AVS_ERROR_PARAM);

  // abrindo conex�o com a base de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina))
	  die(utf8_encode(A_LANG_AVS_ERROR_VERIFY_AUTHO));

	$erro = new Erro();

  // cria um objeto da questao dissertativa
  $q = new Multipla_Escolha($conn, $id_questao);
	
  // se o formulario foi respondido far� a inser��o ou modifica��o
  if(isset($_POST['enviar'])) {
    $q->set_explicacao($_POST['explicacao']);
    $q->set_pergunta($_POST['pergunta']);
    $q->set_disciplina($CodigoDisciplina);
    // perguntas j� inseridas
    // ATEN��O: as pergunta antigas (j� inseridas) tem o A como prefixo do nome das vari�veis

    if(isset($_POST['aalter'])) {
      foreach ($_POST['aalter'] as $k => $value) {
        if($value) {
          $correta = $_POST['correta'] == $k ? TRUE : FALSE;
          $fixa = FALSE;
          if(isset($_POST['fixa'])) {
            $fixa = ($_POST['fixa'] == $k) ? TRUE : FALSE;
          }
          $q->alterar_alternativa($k, $value, $correta, $fixa);
        }
      }
    }

    // para as novas perguntas
    // ATEN��O: as novas pergunta tem o N como prefixo do nome das vari�veis
    if(isset($_POST['nalter'])) {
      foreach ($_POST['nalter'] as $k => $value) {
        if($value){
          $correta = $_POST['correta'] == $k ? TRUE : FALSE;
          $fixa = FALSE;
          if (isset($_POST['fixa'])) {
            $fixa = $_POST['fixa'] == $k ? TRUE : FALSE;
          }
          $q->adicionar_alternativa($value, $correta, $fixa);
        }
      }
    }

		if (!$q->salvar()) {
			$erro->adicionar_erro(A_LANG_AVS_ERROR_DB_SAVE);
		}	else { //@since 05/08/2008 Adicionado para sair da popup se foi inserido com sucesso
			echo "<script language='javascript' type='text/javascript'>window.close()</script>";
		}		
		/**
		 * @since 09/04/2008 Bug de duplicar quest�es quando salva
		 */
		$id_questao = $q->get_questao(); 

		if($erro->quantidade_erro() == 0) {
			$aval = new Avaliacao($conn, $id_aval);
			/**
			 * @since 09/04/2008 Adiciona dentro da avalia��o apenas se n�o tiver a quest�o. Evitando quest�es duplicadas
			 */
			if(!$aval->questao_identificador($q->get_questao())) {
				$aval->atribuir_questao($q->get_questao());
				if(!$aval->salvar()) {
					$erro->adicionar_erro(A_LANG_AVS_ERROR_QUESTION_EVALUATION);
				}	
			}
		}
  }

	// exclui a alternativa
	if(isset($_POST['delete'])) {
    $q->remover_alternativa($_POST['delete']);
		if(!$q->salvar()) $erro->adicionar_erro(A_LANG_AVS_ERROR_QUESTION_ME_DELETE);	
	}
	
  // fecha a conex�o com a base de dados
  $conn->Close();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv=Content-Type content="text/html; charset=ISO-8859-1">
	<meta http-equiv=expires content="0">
	<meta http-equiv=Content-Language content="pt-br">
	<meta name=KEYWORDS content="Adpta��o, Ensino, Distância">
	<meta name=DESCRIPTION content="Módulo de Avalia��o Somativa">
	<meta name=ROBOTS content="INDEX,FOLLOW">
	<meta name=resource-type content="document">
	<meta name=AUTHOR content="Claudiomar Desanti">
	<meta name=COPYRIGHT content="Copyright (c) 2007 by Universidade do Estado de Santa Catarina - UDESC">
	<meta name=revisit-after content="1 days">
	<meta name=distribution content="Global">
	<meta name=GENERATOR content="Easy Eclipse For PHP">
	<meta name=rating content="General">

	<link rel="StyleSheet" href="../css/geral.css" type="text/css">
	<link rel="stylesheet" href="css/ff.css" type="text/css">

<script language="javascript" type="text/javascript">

var contador = 4;

function add() {
  var navegador = navigator.userAgent.toLowerCase();
  var ie = navegador.indexOf('msie'); // verifica o tipo de navegador
  contador++;

  var td1 = document.createElement('td');
  var td2 = document.createElement('td');
  var td3 = document.createElement('td');

  td1.innerHTML = "<input name='correta' type='radio' value='"+contador+"'>";
  td2.innerHTML = "<input name='fixa' type='radio' value='"+contador+"'>";
  td3.innerHTML = "<input name='nalter["+contador+"]' type='text' class='text tamanho'>";
  td3.colSpan = '2';
  td3.align='left';

  if(ie != -1) {
    var tr = document.getElementById('alternativa').insertRow(-1);
  } else {
    var tabela = document.getElementById('alternativa');
    var tr = document.createElement('tr');
    tabela.appendChild(tr);
  }
  tr.align = 'center';
  tr.appendChild(td1);
  tr.appendChild(td2);
  tr.appendChild(td3);
}


function remover(id) {
  var confirma = confirm("<?php echo A_LANG_AVS_JS_ALERT_QUESTION_ME_DELETE; ?>");
  if(confirma) {
		var f = document.getElementById('frm');	
		var del = document.getElementById('delete');
		del.value=id;
		f.submit();
  }
}

function valida() {
  var pergunta = document.getElementById('pergunta');
  if (!pergunta.value || pergunta.value == "") {
    alert("<?php echo A_LANG_AVS_JS_ALERT_QUESTION_ME_REQUIRE; ?>");
    pergunta.focus();
    return false;
  }
  check = false;
  var botoes = document.getElementsByName('correta');
  for (i=0;i<botoes.length;i++) {
    if (botoes[i].checked) {
      check = true;
      break;
    }
  }
  if (!check) {
    alert("<?php echo A_LANG_AVS_JS_ALERT_QUESTION_ME_CORRECT; ?>");
    return false;
  }
  return true;
}

function recarrega() {
  window.opener.location.reload();
  self.close();
}

function iniciar() {
  var texto = document.getElementById('pergunta');
	texto.focus();
}

</script>
<style>
.tamanho {
  width:300px;
}
</style>

</head>

<body style="background-color: <?php echo $A_COR_FUNDO_ORELHA_ON ?>;" onload="iniciar()">

<div class="identificacao">
  <h1><?php echo A_LANG_AVS_LABEL_QUESTION_IDENTIFY_ME; ?></h1>
</div>
<?php
		if(isset($_POST['enviar'])) {
			if ($erro->quantidade_erro() == 0) {
				echo "<div class='sucesso' id='info'>\n";
				echo "<p><span>".A_LANG_AVS_SUCESS."</span></p>\n";
				echo "</div>\n";
			} else {
				echo "<div class='erro' id='info'>\n";
				echo $erro->imprimir_erro();
				echo "</div>\n";
			}
		}
?>
<form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF']."?CodigoDisciplina=".$CodigoDisciplina."&questao=".$id_questao."&aval=".$id_aval; ?>" onSubmit="return valida()" id="frm">
<input type="hidden" id="delete" name="delete" value="">
  <table width="100%" border="0" align="center">
    <tr>
      <td width="80"><?php echo A_LANG_AVS_LABEL_QUESTION_ENUNCIATE; ?> * </td>
      <td width=""><textarea name="pergunta" rows="5" class="tamanho button" id='pergunta'><?php echo $q->get_pergunta(); ?></textarea></td>
    </tr>
    <tr>
      <td><?php echo A_LANG_AVS_LABEL_QUESTION_EXPLANATION?></td>
      <td><textarea name="explicacao" rows="5" class="tamanho button"><?php echo $q->get_explicacao(); ?></textarea></td>
    </tr>
    <tr>
      <td colspan="2">
        <table width="100%" border="0" id="alternativa">
          <tr>
            <th colspan="4"><?php echo A_LANG_AVS_TABLE_QUESTION_ME1; ?></th>
          </tr>
          <tr align="center">
            <td width="47"><?php echo A_LANG_AVS_TABLE_QUESTION_ME2; ?></td>
            <td width="40"><?php echo A_LANG_AVS_TABLE_QUESTION_ME3; ?></td>
            <td width="319"><?php echo A_LANG_AVS_TABLE_QUESTION_ME4; ?></td>
            <td width="43"><?php echo A_LANG_AVS_TABLE_QUESTION_ME5; ?></td>
          </tr>
          <?php
            if($q->numero_alternativas() > 0) {
              while($array = $q->buscar_alternativa()) {
                echo "<tr align='center'>\n";
                if($array['correta']) echo "<td><input name='correta' type='radio' value='".$array['id_alternativa']."' checked='checked'></td>\n";
                else echo "<td><input name='correta' type='radio' value='".$array['id_alternativa']."'></td>\n";
                if($array['fixa']) echo "<td><input name='fixa' type='radio' value='".$array['id_alternativa']."' checked='checked'></td>\n";
                else echo "<td><input name='fixa' type='radio' value='".$array['id_alternativa']."'></td>\n";
                echo "<td align='left'><input name='aalter[".$array['id_alternativa']."]' type='text' class='text tamanho' value='".$array['descricao']."'></td>\n";
                echo "<td><a href='javascript:remover(".$array['id_alternativa'].")' alt='".A_LANG_AVS_HINTS_QUESTION_DELETE."' title='".A_LANG_AVS_HINTS_QUESTION_DELETE."'><img src='../imagens/deletar.gif' width='21' height='20' border='0'></a></td>\n";
                echo "</tr>";
              }
            }

            for($x = 0; $x < (4 - $q->numero_alternativas()); $x++) {
              echo "<tr align='center'>\n";
              echo "<td><input name='correta' type='radio' value='".$x."'></td>\n";
              echo "<td><input name='fixa' type='radio' value='".$x."'></td>\n";
              echo "<td align='left'><input name='nalter[".$x."]' type='text' class='text tamanho'></td>\n";
              echo "<td>&nbsp;</td>\n";
              echo "</tr>";
            }
          ?>
        </table>
      </td>
    </tr>
		<tr><td colspan="2"><div class="alerta"><p><?php echo A_LANG_AVS_TEXT_FIELD_REQUIRE; ?></p></div></td></tr>
    <tr>
      <td colspan="2">
      <div class="funcao">
				<input type="button" name="ajuda" value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onclick="<?php echo HELP_ME ?>" class="button" >				
				<input type="button" name="fechar" value="<?php echo A_LANG_AVS_BUTTON_CLOSE; ?>" class="button" onClick="recarrega()">
        <input name="novo" type="button" class="button" id="novo" value="<?php echo A_LANG_AVS_BUTTON_QUESTION_ADD_ME; ?>" onclick="add()">				
        <input type="submit" class="button" value="<?php echo A_LANG_AVS_BUTTON_SAVE; ?>" name="enviar">
			</div>	
      </td>
    </tr>
  </table>
</form>
</body>
</html>
