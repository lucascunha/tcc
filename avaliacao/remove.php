<?php
/**
 * Script para remover as lacunas e alternativas
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @version 1.0 <14/10/2007>
 */

  session_start();
  // verifica se a pessoa est� logada
  if(!isset($_SESSION['logado'])) die("Voc� deve efetuar primeiro o login no sistema para trabalhar com este m�dulo");
  // verifica se quem est� tentando acessar a p�gina � um professor. Impede que alunos tentem acessar e ver a resposta correta.
  if($_SESSION['tipo_usuario'] != 'professor') die("<font color='red'>Voc� n�o tem autoriza��o para modificar este m�dulo</font>");


?>

<script type="text/javascript" language="javascript">
  function voltar() {
    window.opener.location.reload();
    self.close();
  }
</script>

<?php
  require_once "../include/adodb/adodb.inc.php";
  require_once "../config/configuracoes.php";
	require_once "questao.const.php";
  require_once "class/questao.class.php";
  require_once "class/lacunas.class.php";
  require_once "class/avaliacao.class.php";
  require_once "class/multipla_escolha.class.php";

  // deleta a lacuna (l) ou alternativa (a)
  $tipo = $_REQUEST['tipo'] == 'l' ? 'l' : 'a';
  $questao = $_REQUEST['questao'] ? $_REQUEST['questao'] : FALSE;
  $id = $_REQUEST['id'] ? $_REQUEST['id'] : FALSE;

  if(!$id) {
    echo "<script>voltar()</script>";
  }
  if(!$questao) {
    echo "<script>voltar()</script>";
  }

  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

  if($tipo == 'l') {
    $q = new Lacunas($conn, $questao);
    $q->remover_lacuna($id);
		$q->salvar();
  } else {
    $q = new Multipla_Escolha($conn, $questao);
    $q->remover_alternativa($id);
		$q->salvar();
  }
  echo "<script>voltar()</script>";

  $conn->Close();
?>

