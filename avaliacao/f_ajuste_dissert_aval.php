<?php
/**
 * P�gina que ir� listar os t�picos e os cursos para liberar a avalia��o.
 * Lembrando que esta � a primeira das p�ginas em um total de 3 p�ginas.
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
 * @subpackage Liberar Avalia��o
 * @version 1.0 <01/12/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */

	$CodigoDisciplina = isset($_GET['CodigoDisciplina']) ? $_GET['CodigoDisciplina'] : FALSE;
	
	if(!$CodigoDisciplina) {
		echo A_LANG_AVS_ERROR_PARAM;
		return;
	}	

  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/questao.class.php";
	require_once "avaliacao/funcao/ajuste.func.php";
	require_once "avaliacao/class/erro.class.php";
	require_once "avaliacao/ajuda.const.php";

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);

	// SEGURAN�A, N�O DEIXA OUTROS PROFESSORES ACESSAREM INFORMA��ES DO PROFESSOR
	if(!verifica_professor_disciplina($conn, $_SESSION['id_usuario'], $CodigoDisciplina)) {
	  echo A_LANG_AVS_ERROR_VERIFY_AUTHO;
		return;
	}	

  montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_AJUSTE);	

	$vetor = ajuste_dissert_lista_avaliacao($conn, $CodigoDisciplina);
	
	$erro = new Erro();
	if(count($vetor) == 0) $erro->adicionar_erro(A_LANG_AVS_ERROR_ADJUSTMENT_DISSERT);

?>
<script type="text/javascript" language="javascript">
function info(id) {
  var div = document.getElementsByTagName('div');
	for(x=0; x < div.length; x++) {
	  if(div[x].className.search("hidden") != -1) {
		  div[x].style.display="none";
		}	
	}
  var x = document.getElementById(id);
	x.style.display="block";	
}

function enviar() {
  var combo = document.getElementById('libera');
	if(combo.value == 0) {
	  var erro = document.getElementById('erro');
		erro.style.display="block";
		return false;
	}	else {
	  var form = document.getElementById('frm');
		form.submit();
	}
}
</script>
<form action="<?php echo "a_index.php?opcao=FuncaoAjustarDissertAluno&CodigoDisciplina=$CodigoDisciplina"; ?>" method="post" id='frm' name="form_peso">
<table cellspacing='1' cellpadding='1' width='100%' border='0' bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
	<tr>
    <td>
      <div class="funcao">
				<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_DISSERT; ?> (1/3)</div>	
				<div class="hidden erro" id='erro'>
				  <p><span><?php echo A_LANG_AVS_LABEL_CHOOSE_EVALUATION; ?></span></p>
				</div>
				<br />				
				<?php
					if($erro->quantidade_erro() > 0) {
						echo "<div class='erro'>\n";
						echo $erro->imprimir_erro();
						echo "</div>\n";
					} else {
						
						echo "<p><span>".A_LANG_AVS_LABEL_TOPIC.": </span>";
												
						echo "<select name='libera' id='libera' class='button' style='width:400px' onchange='info(this.value)'>\n";
						echo "<option value='' disabled='disabled' selected='selected'>".A_LANG_AVS_COMBO_SELECT."</option>\n";
						foreach ($vetor as $k) {
						  echo "<option value='".$k['id']."'>".A_LANG_AVS_LABEL_EVALUATION.": ".$k['id']." - ".quote($k['avaliacao']).". ".A_LANG_AVS_COMBO_DATE.": ".formata_data($k['data'])."</option>\n";
						}						
						echo "</select>";						
						echo "</p>";
						
						// escreve as div ocultas com as informa��es.
						foreach ($vetor as $k) {
						  echo "<div class='hidden avaliacao' id='".$k['id']."'>\n";
							echo "<h2 class='uppercase'>".A_LANG_AVS_LABEL_INFO."</h2>\n";
							echo "<p><span>".A_LANG_AVS_LABEL_TOPIC.":</span> ".$k['topico']." - ".buscar_nome_conceito($conteudo,$k['topico'])."</p>\n";
							echo "<p><span>".A_LANG_AVS_TABLE_DATE.":</span> ".formata_data($k['data_liberar'])."</p>\n";
							echo "<p><span>".A_LANG_AVS_LABEL_EVALUATION.":</span> ".$k['avaliacao']."</p>\n";
							echo "</div>\n";						
						}						
					}			
				?>			
			</div>
    </td>
  </tr>
	<tr>
		<td>
		<div class='funcao'>
		<?php 
		  if($erro->quantidade_erro() == 0) {
				// div do botao
				echo "<input type='button' name='voltar' value='".A_LANG_AVS_BUTTON_BACK."' class='button' onclick=\"javascript:window.location='a_index.php?opcao=FuncaoAjuste&CodigoDisciplina=$CodigoDisciplina'\" />";
				echo "<input name='ajuda' type='button' class='button' value='".A_LANG_AVS_BUTTON_HELP."' onClick='".HELP_FUNCTION_ADJUSTMENT_DISSERT."' />";
				echo "<input type='submit' value='".A_LANG_AVS_BUTTON_NEXT."' class='button' onclick='return enviar();'/>";
			}			
		?>	
		</div>	
		</td>
	</tr>
</table>
</form>