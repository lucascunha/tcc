<ul id='nav'>
  <li><a href="index.php">Introdu&ccedil;&atilde;o</a></li>
	<li><a href="passo.php">Passo a passo</a></li>
	<li><a href="index_estrut.php">Estruturar avalia&ccedil;&atilde;o</a>
		<ul>
			<li><a href="topico.php">T&oacute;picos</a></li>
			<li> <a href="grupos.php">Grupos de avalia&ccedil;&atilde;o</a> </li>
			<li><a href="index_aval.php">Avalia&ccedil;&otilde;es</a>	  
				<ul>
					<li><a href="nova_questao.php">Novas quest&otilde;es</a> </li>
					<li><a href="questao_existe.php">Quest&otilde;es existentes</a></li>
				</ul>	
			</li>	  
	</ul>
	<li>Fun&ccedil;&otilde;es de avalia&ccedil;&atilde;o
	  <ul>
	    <li><a href="media.php">Distribui&ccedil;&atilde;o de m&eacute;dia</a></li>
    	<li><a href="index_ajuste.php">Ajuste de notas</a>      
				<ul>
					<li><a href="ajuste_media.php">Ajuste de m&eacute;dia</a></li>
					<li><a href="ajuste_questao.php">Corre&ccedil;&atilde;o de quest&otilde;es dissertativas</a></li>
					<li><a href="ajuste_anular.php">Anular quest&otilde;es da avalia&ccedil;&atilde;o</a></li>
					<li><a href="ajuste_presencial.php">Ajuste de notas das avalia&ccedil;&otilde;es presenciais</a></li>
				</ul>
    	</li>
			<li><a href="liberar.php">Liberar avalia&ccedil;&atilde;o</a></li>
			<li><a href="divulgar.php">Divulgar notas</a></li>
			<li><a href="relatorio.php">Relat&oacute;rios</a></li>
			<li><a href="backup.php">Backup</a></li>
	  </ul>
	</li>
	<li>Quest&otilde;es
  	<ul>
			<li>Dissertativa</li>
			<li>M&uacute;ltipla-escolha</li>
			<li>Verdadeiro/Falso</li>
			<li>Lacunas</li>
		</ul>
	</li>
</ul>
