<?php
/**
 * Faz o ajuste de pesos para o calculo da m�dia
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
	* @subpackage Media
 * @version 1.0 <23/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */
  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/geral.func.php";
	require_once "avaliacao/funcao/backup.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/questao.class.php";
  require_once "avaliacao/class/dissertativa.class.php";
  require_once "avaliacao/class/multipla_escolha.class.php";
  require_once "avaliacao/class/lacunas.class.php";
  require_once "avaliacao/class/verdadeiro_falso.class.php";
	require_once "avaliacao/class/erro.class.php";
	
	$CodigoDisciplina = $_GET['CodigoDisciplina'];

  montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_BACKUP);

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	
	$vetor = backup_lista_avaliacao($conn, $CodigoDisciplina);
		
	$erro = new Erro();	
	
	if(count($vetor) == 0) $erro->adicionar_erro(A_LANG_AVS_ERROR_FUNCTION_EVALUATION_LIBERATE);

?>
<script type="text/javascript" language="javascript">
function backup() {
	var id = document.getElementById('avaliacao').value;
	if (id)
		window.open("avaliacao/f_backup_pdf.php?disciplina=<?php echo $CodigoDisciplina; ?>&libera="+id, "backup_avaliacao");
}
</script>

<table cellspacing=1 cellpadding=1 width="100%"  border =0  bgcolor=<? echo $A_COR_FUNDO_ORELHA_ON ?>>
  <tr>
    <td>
		<div class='funcao'>
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_BACKUP; ?></div>				
			<p>
      <?php
        // imprime o erro ou o sucesso em salvar
        if($erro->quantidade_erro() == 0) {
          echo "<br>";
          echo "<label>".A_LANG_AVS_LABEL_EVALUATION.": &nbsp;&nbsp;</label>\n";

					echo "<select id='avaliacao' class='button' style='width:300px;'>\n";
          echo "<option value='' disabled='disabled' selected='selected' >".A_LANG_AVS_COMBO_SELECT."</option>\n";
          // impressao das avalia��es
          foreach ($vetor as $k) {
						echo "<option value='".$k['libera']."' >".A_LANG_AVS_LABEL_EVALUATION.": ".$k['libera']." - ".quote($k['descricao']).". ".A_LANG_AVS_COMBO_DATE.": ".formata_data($k['data_liberar'])."</option>\n";
          }
      		echo "</select>\n";
					echo "<input type='button' name='pdf' class='button' value='Gerar pdf' onclick='backup()' />";
        } elseif ($erro->quantidade_erro() > 0) {
          echo "<div class='erro' id='info'>\n";
          echo $erro_vetor->imprimir_erro();
          echo "</div>\n";
        }
      ?>				

			</p>	
		</div>		
		<div class="funcao"><input name='ajuda' type='button' class='button' value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onClick="<?php echo HELP_FUNCTION_BACKUP; ?>" /></div>


    </td>
  </tr>
</table>
