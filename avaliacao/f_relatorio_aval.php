<?php
/**
 * Faz o ajuste de pesos para o calculo da m�dia
 * @author Claudiomar Desanti <claudesanti@gmail.com>
 * @package Avalia��o Somativa
	* @subpackage Media
 * @version 1.0 <23/10/2007>
 *
 * Desenvolvido na Universidade do Estado de Santa Catarina - UDESC.
 * Como Trabalho de Conclus�o de Curso.
 * Orientadora: Isabela Gasparini
 * Co-orientadora: Avanilde Kemczinski
 *
 */
  require_once "avaliacao/questao.const.php";
  require_once "avaliacao/funcao/geral.func.php";
  require_once "avaliacao/class/avaliacao.class.php";
  require_once "avaliacao/class/grupo.class.php";
  require_once "avaliacao/class/questao.class.php";
  require_once "avaliacao/class/dissertativa.class.php";
  require_once "avaliacao/class/multipla_escolha.class.php";
  require_once "avaliacao/class/lacunas.class.php";
  require_once "avaliacao/class/verdadeiro_falso.class.php";
	require_once "avaliacao/class/erro.class.php";
	require_once "avaliacao/funcao/relatorio.func.php";

	$CodigoDisciplina = $_GET['CodigoDisciplina'];

  montar_orelha_funcao_avaliacao($CodigoDisciplina, AVS_FUNC_RELATORIO);

  // abre conex�o com o banco de dados
  $conn = &ADONewConnection($A_DB_TYPE);
  $conn->PConnect($A_DB_HOST,$A_DB_USER,$A_DB_PASS,$A_DB_DB);
	
	$erro_vetor = new Erro();
	
	$vetor = relatorio_lista_aval($conn, $CodigoDisciplina);
	if(count($vetor) == 0) $erro_vetor->adicionar_erro(A_LANG_AVS_ERROR_FUNCTION_EVALUATION_LIBERATE);

?>
<script language="javascript" type="text/javascript" src="avaliacao/ajax/ajax.js"></script>
<script type="text/javascript" language="javascript">
	function listar_questao(avaliacao) {
		var ajax = new AJAX();
		var disc = <?php echo $CodigoDisciplina; ?>;
		var div = document.getElementById("conteudo");
		div.innerHTML = "Carregando <img src='imagens/carrega.gif' border='0'>";
		ajax.Texto("avaliacao/ajax/ajax_relatorio_aval.php?avaliacao="+avaliacao+"&disciplina="+<?php echo $CodigoDisciplina; ?>,div);	
	}	
  
	function visualizar_avaliacao(id) 
	{
    window.open("avaliacao/f_relatorio_visualizar_aval.php?libera_aluno="+id+"&CodigoDisciplina=<?php echo $CodigoDisciplina; ?>", "visualizar_avaliacao");
	}
</script>
<table cellspacing='1' cellpadding='1' width='100%'  border='0'  bgcolor='<?php echo $A_COR_FUNDO_ORELHA_ON ?>'>
  <tr>
    <td>
		<div class='funcao'>
			<div class="bold uppercase logo"><?php echo A_LANG_AVS_TITLE_REPORT; ?></div>				
			<br />
			<?php
				if($erro_vetor->quantidade_erro() > 0) {
					echo "<div class='erro'>\n";
					echo $erro_vetor->imprimir_erro();
					echo "</div>";
				} else {								
					echo "<label>".A_LANG_AVS_LABEL_CHOOSE_EVALUATION." </label>&nbsp;\n";
					echo "<select name='avaliacao' onchange='listar_questao(this.value)' style='width:300px'>\n";
					if(empty($_REQUEST['avaliacao'])) echo "<option value='' disabled='disabled' selected='selected'>".A_LANG_AVS_COMBO_SELECT."</option>";
					else echo "<option value='' disabled='disabled' >".A_LANG_AVS_COMBO_SELECT."</option>";
					foreach ($vetor as $k) {						
						if(!empty($_REQUEST['avaliacao'])) {
							if($_REQUEST['avaliacao'] == $conceito['libera']) echo "<option value='".$k['libera']."' selected='selected'>".A_LANG_AVS_LABEL_EVALUATION.": ".$k['libera']." - ".quote($k['descricao']).". ".A_LANG_AVS_COMBO_DATE.": ".formata_data($k['data_liberar'])."</option>\n";
							else echo "<option value='".$k['libera']."'>".A_LANG_AVS_LABEL_EVALUATION.": ".$k['libera']." - ".quote($k['descricao']).". ".A_LANG_AVS_COMBO_DATE.": ".formata_data($k['data_liberar'])."</option>\n";
						} else {
							echo "<option value='".$k['libera']."'>".A_LANG_AVS_LABEL_EVALUATION.": ".$k['libera']." - ".quote($k['descricao']).". ".A_LANG_AVS_COMBO_DATE.": ".formata_data($k['data_liberar'])."</option>\n";
						}								
					}
					echo "</select>\n";
				}
			?>			
			<div id='conteudo'></div>						
			
		</div>		
		<div class="funcao">
			<input name="voltar" type="button" value="<?php echo A_LANG_AVS_BUTTON_BACK; ?>" class="button" onClick="javascript:window.location='<?php echo "a_index.php?opcao=FuncaoRelatorio&CodigoDisciplina=$CodigoDisciplina" ?>'" />
			<input name='ajuda' type='button' class='button' value="<?php echo A_LANG_AVS_BUTTON_HELP; ?>" onClick="<?php echo HELP_FUNCTION_REPORT_AVAL; ?>" />	
		</div>			

    </td>
  </tr>
</table>